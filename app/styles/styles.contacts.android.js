'use strict';

module.exports = {
  listViewRecent: {
    paddingLeft:15,
    paddingRight:15,
    backgroundColor: '#fff'
  },
  text:{
    fontSize:12,
    color: 'gray',
  },
  textBold:{
    fontSize:12,
    color: 'gray',
    fontWeight: '800',
    marginBottom:5,
  },
  thumbnail:{
    width:50,
    height:50,
    borderRadius:25,
    borderWidth:2,
    borderColor: 'gray',
  },
  rightContainer: {
    flex: 1,
    paddingLeft:20,
  },
  li: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    //marginBottom:10,
    //marginTop:10,
    padding: 10,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  centerText: {
    alignItems: 'center',
  },
  noContactsText: {
    marginTop: 80,
    color: '#888888',
  },
  separator: {
    height: 1,
    backgroundColor: '#eeeeee',
  },
  borderBottom:{
    height:1,
    flex:2,
    backgroundColor:"#29b6f6"
  },
  actionButtonIcon: {
    fontSize: 25,
    color: 'white',
  },
};
