'use strict';

module.exports = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#ffffff'
  },
  listView: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 30
  },
  messageContainer: {
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: .8,
    borderBottomColor: '#29b6f6'
  },
  textBold:{
    fontSize: 15,
    color: 'gray',
    fontWeight: '800',
    marginBottom: 5,
  },
  base: {
    width: 55,
    height: 55,
  },
  actionButtonIcon: {
    fontSize: 25,
    color: 'white',
  },

  loadingMessage:{
    flex: 1,
    backgroundColor: 'white',
    fontSize: 12,
    textAlign: 'center',
    alignItems: 'center',
  }
};
