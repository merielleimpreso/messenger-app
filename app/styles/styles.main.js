'use strict';

import React, {
} from 'react';

import {
  Dimensions
} from 'react-native';

var SCREEN = Dimensions.get("window");

module.exports = {
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  loaderMessage: {
    alignSelf: 'center',
  },
  actionButtonIcon: {
    fontSize: 25,
  },

  // BEGIN LOGIN
  appName: {
    height: 160,
    width: 170,
    marginBottom:20
  },

  inputLoginContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    width: SCREEN.width * 0.75,
    borderBottomWidth: 0.5
  },
  searchBoxContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    borderRadius: 3,
  },
  input: {
    // width: 250,
    height: 35,
    padding: 5,
    textAlign: 'center',
  },
  buttonLoginContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    borderRadius: 3,
    width: 200,
  },
  smallButtonLoginContainer: {
    marginTop: 30,
    paddingTop: 5,
    paddingLeft: 15,
    paddingBottom: 5,
    paddingRight: 15,
    borderRadius: 2,
  },
  buttonSignupToKliklabs: {
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonLogin: {
    height: 45,
    width: 250,
    flex: 0,
    flexDirection: 'row',
    borderRadius: 8,
    marginBottom: 10,
    justifyContent: 'center',
    //backgroundColor: COLOR.BUTTON
  },
  buttonSignup: {
    height: 45,
    width: 250,
    flex: 0,
    flexDirection: 'row',
    borderRadius: 8,
    marginBottom: 10,
    justifyContent: 'center',
    //backgroundColor: COLOR.THEME
  },
  loginButtonText: {
    fontSize: 18,
    alignSelf: 'center',
    //color: COLOR.BUTTON_TEXT
  },
  error: {
    marginBottom: 10,
    fontSize: 12,
    textAlign: 'center',
    //color: '#FF0000'
  },
  hint: {
    marginBottom: 10,
    fontSize: 12,
    textAlign: 'center',
    //color: COLOR.BUTTON
  },
  btnBottom: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: COLOR.THEME
  },
  loginContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  // END LOGIN

  // MESSENGER TABS
  tabView: {
    flex: 1,
    padding: 0,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  card: {
    height: SCREEN.height - 117,
  },
  icon: {
    width: 300,
    height: 300,
    alignSelf: 'center',
  },
  toolbar: {
    height: 55,
    //backgroundColor: COLOR.BAR,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  logoContainer: {
    flex: 1
  },
  logo: {
    height: 40,
    width: 120,
    marginLeft: 10
  },
  // END MESSENGER TABS

  // recent
  listView: {
    marginTop: 10
  },
  li: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:5,
    marginTop:5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  rightContainer: {
    flex: 1,
    paddingRight:20,
    justifyContent: 'flex-start'
  },
  textBold:{
    fontSize:15,
    fontWeight: '800',
    marginBottom:5,
    //color: COLOR.TEXT_DARK
  },
  text:{
    fontSize:12,
    fontWeight: '300',
    //color: COLOR.TEXT_LIGHT
  },
  textNotSeen:{
    fontSize:12,
    //color: COLOR.TEXT_LIGHT,
    fontWeight: '900',
  },
  thumbnail:{
    width:50,
    height:50,
    borderRadius:25,
    borderWidth:2,
    //borderColor: COLOR.CONTRAST,
  },
  borderBottom:{
    height:1,
    flex:2,
    //backgroundColor:"#bbdefb"
  },
  buttonContainer: {
    flex: 1,
    paddingLeft:3,
    flexDirection: 'row',
  },
  button: {
    height: 30,
    width: 80,
    flex: 0,
    flexDirection: 'row',
    borderColor: '#1F5490',
    borderRadius: 8,
    marginRight: 10,
    justifyContent: 'center',
    //backgroundColor: COLOR.BUTTON
  },
  buttonText: {
    fontSize: 15,
    fontWeight: '500',
    alignSelf: 'center',
    //color: COLOR.BUTTON_TEXT
  },

  chatRecentRow: {
    flexDirection: 'row',
    // margin: 10,
    // alignItems: 'stretch'
  },
  chatRecentImage:{
    width: 50,
    height: 50,
    borderRadius: 25,
    margin: 10
  },
  chatRecentDetailsContainer: {
    flex: 1,
    height: 70,
    justifyContent: 'center',
    marginRight: 10
  },
  chatRecentTextBold:{
    fontSize: 15,
    // fontWeight: '800',
    marginBottom: 5,
    //color: COLOR.TEXT_DARK
  },
  chatRecentButtonContainer: {
    // flex: 1,
    paddingLeft:3,
    flexDirection: 'row',
  },
  chatRecentButton: {
    height: 30,
    width: 80,
    flex: 0,
    flexDirection: 'row',
    borderColor: '#1F5490',
    borderRadius: 8,
    marginRight: 10,
    justifyContent: 'center',
    //backgroundColor: COLOR.BUTTON
  },
  chatRecentButtonText: {
    fontSize: 15,
    fontWeight: '500',
    alignSelf: 'center',
    //color: COLOR.BUTTON_TEXT
  },

  // CONTACTS
  iconButton: {
    padding: 10
  },
  noContactsText: {
    marginTop: 80,
    //color: '#888888',
  },
  searchName: {
    height: 40,
    padding: 5,
    backgroundColor: 'transparent',
    //color: COLOR.TEXT_DARK
  },
  borderRounded: {
    borderWidth: 1,
    borderRadius: 5,
    margin: 3
    //borderColor: COLOR.BUTTON,
  },
  // END CONTACTS

  // CHAT
  media: {
    height: 150,
    width: 150
  },
  messageContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  messageThread: {
    flex: 1,
    height: SCREEN.height,
  },
  messages: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding: 10
  },
  messageWithMedia: {
    marginLeft: 10,
    marginRight: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius : 10,
    padding: 10,
    width: 154
  },
  imageContainer: {
    width: 154,
    borderWidth: 2,
    //borderColor: COLOR.TEXT_LIGHT,
    borderRadius: 10,
    marginLeft: 12,
    marginRight: 12,
  },
  mapContainer: {
    width: 154,
    borderWidth: 2,
    //borderColor: COLOR.TEXT_LIGHT,
    marginLeft: 12,
    marginRight: 12,
  },
  mediaWithMessage: {
    width: 154,
    borderWidth: 2,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius : 10,
    marginLeft: 10,
    marginRight: 12,
  },
  base: {
    width: 55,
    height: 55,
    borderRadius: 27.5
  },
  hyperlink: {
    //color: COLOR.THEME,
    textDecorationLine:'underline'
  },
  chatMessageContainer: {
    //flex: 1,
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 10,
    paddingTop: 1.5,
    paddingBottom: 1.5
  },

  chatDateContainer: {
    //flex: 1,
    borderRadius: 3,
    alignSelf: 'center'
  },
  chatDateText: {
    fontSize: 11
  },
  chatDetailsText: {
    fontSize: 9,
    // marginLeft: -7,
    // marginRight: -7
  },
  chatTextContainer: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 10,
    padding: 10,
    width: SCREEN.width * 0.7
  },
  chatContainer: {
    flexWrap: 'wrap',
    maxWidth: SCREEN.width * 0.65,
    // paddingLeft: 10,
    // paddingRight: 10
  },
  chatTextWrapContainer: {
    //alignSelf: 'flex-start',
    flexWrap: 'wrap',
    paddingTop: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5,
    borderRadius: 5
  },
  chatTextContainerOpaque: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  triangleSent : {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 15,
    borderTopWidth: 15,
    borderRightColor: 'transparent',
    marginRight: 5,
    zIndex: -1
  },
  triangleReceived: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 15,
    borderTopWidth: 15,
    borderLeftColor: 'transparent',
    marginLeft: 5,
    zIndex: -1
  },
  chatUserImage: {
    width: 28,
    height: 28,
    borderRadius: (28) / 2
  },
  chatUserImageSmall: {
    width: 30,
    height: 30,
    borderRadius: 15,
    //backgroundColor: 'black'
  },
  chatImage: {
    height: 150,
    width: 150,
    // borderRadius: 8
  },
  chatImageContainer: {
    width: 150,
    // borderRadius: 10,
    // marginLeft: 15,
    // marginRight: 15,
  },
  messageIcon: {
    fontSize: 100,
    marginLeft: 10,
    marginRight: 10
  },
  // END CHAT

  // MESSAGE DIRECT
  msgContainer: {
    flex: 1,
    alignItems: 'stretch',
    //backgroundColor: COLOR.CHAT_BG
  },
  topContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    //backgroundColor: COLOR.THEME,
  },
  backTouchable: {
    paddingLeft: 5,
    width: 30,
    height: 50,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  backText: {
    //color: COLOR.BUTTON_TEXT,
    fontSize: 40
  },
  headerName: {
    //color: COLOR.BUTTON_TEXT,
    fontSize: 20,
  },
  active: {
    //color: COLOR.BUTTON,
    fontSize: 12
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 5,
  },
  // END MESSAGE DIRECT

  // TYPING AREA
  footer: {
    height: 25,
    //backgroundColor: COLOR.THEME,
  },
  sendingContainer: {
    flexDirection: 'row',
    position: 'absolute',
    top: -40,
    right: 8
  },
  typingArea: {
    // width: SCREEN.width,
    flex: 1,
    position: 'relative',
  },
  inputSeparator: {
    height: 1,
    //backgroundColor: COLOR.THEME,
  },
  inputContainer: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  textContainer: {
    flex:1,
    justifyContent:'center',
  },
  sendContainer: {
    padding: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  messageInput: {
    width: SCREEN.width - 122,
    alignSelf: 'center',
    fontSize:15,
    padding: 5,
    backgroundColor: 'transparent',
  },
  messageInputIOS: {
    width: SCREEN.width - 122,
    height: 33,
    alignSelf: 'center',
    fontSize:15,
    backgroundColor: 'transparent',
    padding: 5
  },
  sendButton: {
    fontSize: 25,
    margin: 3
    // marginTop: 3,
    // marginLeft: 7,
    // marginBottom: 3,
    // marginRight: 2
  },
  buttonsContainer: {
    height: SCREEN.height * .4,
    flexDirection: 'column',
    //alignItems: 'center',
    justifyContent: 'space-around',
  },
  buttonWrap: {
    width: SCREEN.width * 0.335,
    height: SCREEN.height * 0.12,
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center'
  },
  optionTouchable: {
    height: 100,
    width: 100,
    borderRadius: 50,
    justifyContent: 'center'
  },
  optionButtonIcon: {
    alignSelf: 'center',
    fontSize: 45
  },
  optionButtonText: {
    fontSize: 12,
    // marginTop: 5,
    // alignSelf: 'center'
    textAlign: 'center'
  },
  recordButton: {
    alignSelf: 'center',
    height: 100,
    width: 100,
    flex: 0,
    borderRadius: 50,
    marginBottom: 15,
    justifyContent: 'center',
    //backgroundColor: COLOR.ERROR
  },
  recordContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  // END TYPING

  // GROUP MESSAGES
  details: {
    marginRight: 5,
    //color: COLOR.BUTTON_TEXT
  },
  contactContainer: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  addContactContainer: {
    flexDirection: 'row',
    marginTop: 14,
    marginLeft: 25,
    marginRight: 20,
    marginBottom: 10,
    alignItems: 'center'
  },
  contactListContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:10,
  },
  // END GROUP MESSAGES

  // ADD NEW MESSAGE
  barInput: {
    height: 55,
    //backgroundColor: COLOR.THEME,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  typeContainer: {
    width: SCREEN.width,
    height: 40,
    justifyContent: 'center',
    paddingLeft: 15,
  },
  typeName: {
    padding: 0,
    height: 40,
    fontSize: 18,
    backgroundColor: 'transparent',
    //color: COLOR.BUTTON_TEXT
  },
  doneIcon: {
    fontSize: 45,
    //color: COLOR.BUTTON_TEXT,
  },
  contactIcon: {
    fontSize: 45,
    //color: COLOR.THEME,
  },
  // END ADD NEW MESSAGE

  // ADD GROUP CHAT
  addContainer: {
    height: 50,
    //backgroundColor: COLOR.THEME,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  buttonCreate: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  txtCreateContainer: {
    paddingRight: 10
  },
  txtCreate: {
    //color: COLOR.BUTTON_TEXT
  },
  createIcon: {
    fontSize: 30,
    //color: COLOR.BUTTON_TEXT
  },
  mainContainer: {
    flex: 1,
    // paddingRight: 20,
    // paddingLeft: 20,
    // alignItems: 'center',
    // justifyContent: 'center',
    // height: SCREEN.height - 135
  },
  pictureNameContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10
    // height: 115,
  },
  pictureContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 75,
    width: 75,
    borderRadius: 50,
    borderWidth: 1,
    // borderColor: COLOR.BUTTON,
    // padding: 20
  },
  cameraIcon: {
    fontSize: 40,
    // color: COLOR.HIGHLIGHT
  },
  nameContainer: {
    width: Dimensions.get('window').width - 110,
    justifyContent: 'center',
    paddingTop: 15,
    paddingLeft: 15,
    height: 50
  },
  txtName: {
    // justifyContent: 'space-around',
    // height: 40,
    // paddingLeft: 20,
    fontSize: 18,
  },
  searchContainer: {
    height: 50,
    paddingRight: 25,
    paddingLeft: 10,
    paddingBottom: 15,
    flexDirection: 'row'
  },
  checkboxIconContainer: {
    paddingRight: 5,
  },
  checkBoxIcon: {
    fontSize: 35,
    // color: COLOR.TEXT_LIGHT
  },
  personIcon: {
    fontSize: 40,
    //color: COLOR.THEME
  },
  addPeople: {
    width: Dimensions.get('window').width * 0.84,
    height: 50
  },
  hidden: {
    height: 0,
    width: 0
  },

  // PROFILE
  changeAvatarIcon: {
   alignSelf: 'flex-end',
   borderRadius: 50,
   borderWidth: 0.5,
   width: 25,
   height: 25,
   alignItems: 'center',
   justifyContent: 'center'
 },
  nameText: {
    fontSize: 20,
    marginLeft: 20
  },
  contactImage: {
    height: 130,
    width: 130,
    borderRadius: 70,
  },
  profilePicContainer: {
    height: 77,
    width: 77,
    borderRadius: 50,
    //borderColor: COLOR.HIGHLIGHT,
    borderWidth: 1,
  },
  borderBottomContrast: {
    alignItems: 'center',
    borderBottomWidth: 1,
    //borderBottomColor: COLOR.CONTRAST,
  },
  profileDetailesContainer: {
    //flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'column',
  },
  detailContainer: {
    flexDirection: 'row',
    //padding: 5,
  },
  detailTitle: {
    //color: COLOR.CONTRAST,
    fontSize: 18,
    width: 75
  },
  detailValue: {
    //color: COLOR.HIGHLIGHT,
    fontSize: 18,
  },
  passwordContainer: {
    //flex: 1,
    //flexDirection: 'column',
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorPassword: {
    fontSize: 12,
    textAlign: 'center',
    //color: '#FF0000'
  },
  passwordButton: {
    height: 35,
    width: 120,
    borderRadius: 8,
    justifyContent: 'center',
    //backgroundColor: COLOR.BUTTON,
    marginTop: 5
  },
  passwordText: {
    justifyContent: 'space-around',
    fontSize: 15,
    height: 45,
    paddingBottom: 0
    //color: COLOR.TEXT_DARK,
    //paddingBottom: 5
  },
  buttonUpload: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
    borderRadius: 5,
    margin: 5
  },

  timelineLoggedInUserImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },

  timelinePostUserName: {
    fontSize: 15,
    fontWeight:'800',
    marginTop: 2
  },

  // timelinePostDate: {
  //   fontSize: 9,
  //   paddingBottom: 5,
  // },

  timelinePostUserImage: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },

  timelinePostText: {
    flex: 1,
    fontWeight: '100',
    marginTop: 10,
    marginBottom: 10,
    padding: 10
  },

  // New TABS
  toolbarAndroid: {
    height: 40
  },

  // New CONTACTS
  contactsListViewRow: {
    flex: 0,
    flexDirection: 'row',
    // margin:5,
    marginLeft: 15,
    alignItems: 'center'
  },
  listViewRowContainer: {
    flex: 1,
    height: 60,
    paddingLeft: 10,
    justifyContent: 'center',
  },
  listViewImage:{
    width: 40,
    height: 40,
    borderRadius: 20
  },
  listViewTitle: {
    fontSize: 12,
    marginBottom: 2,
  },

  // Add Friends
  iconMenuContainer: {
    width: SCREEN.width * 0.25,
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconMenuLabel: {
    fontSize: 9,
    width: 60,
    textAlign: 'center',
    height: 55,
  },

  // Others
  pageTitle: {
    marginLeft: 10,
    marginTop: 10,
    fontSize: 13
  },

  containerNoResults: {
    flex: 1,
    alignItems: 'stretch'
  },

  contactsCreateGroupImage: {
    alignSelf: 'flex-end',
    marginLeft: -20,
    borderRadius: 13,
    width: 26,
    height: 26,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5
  },

  contactsCreateGroupMemberTitle: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },

  contactsCreateGroupList: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin:10
  },

  contactsCreateGroupListRow: {
    justifyContent: 'center',
    width: ((SCREEN.width - 20)) / 4,
    height: ((SCREEN.width - 20)) / 4,
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },

  contactsCreateGroupListRowItem: {
    width: 60,//((SCREEN.width - 20) - (5*2*4)) / 4,
    height: 60, //((SCREEN.width - 20) - (5*2*4)) / 4,
    borderRadius: 50,//(((SCREEN.width - 20) - (5*2*4)) / 4) / 2,
    alignItems:'center',
    justifyContent:'center',
    margin:5
  },

  contactsCreateGroupListRowItemRemove: {
    alignSelf: 'flex-start',
    marginLeft: -25,
    borderRadius: 10,
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5
  },

  optionsMenuContainer: {
    flex: 1,
    height: 90,
    justifyContent: 'center'
  },

  optionsMenuMiddle: {
    borderLeftWidth: 0.5,
    borderRightWidth: 0.5,
  },

  addPhotoRegister: {
   borderRadius: 50,
   borderWidth: 0.5,
   width: 30,
   height: 30,
   alignItems: 'center',
   justifyContent: 'center',
   bottom: 8,
   right: 8
 },

 audioPlayer: {
    width: 150,
    height: 40,
    borderRadius: 13,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },

  audioPlayerProgress: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    height: 40,
    justifyContent: 'space-around',
    alignSelf: 'center',
    alignItems: 'center',
    borderLeftWidth: 0.5,
    borderRightWidth: 0.5,
  },

  audioPlayerProgressView: {
    height: 40,
    alignSelf: 'center'
  },

  audioPlayerTouch: {
    borderRadius: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    // marginLeft: 10,
    // marginRight: 10
  },

  timelineProfile: {
    width: 35,
    height: 35,
    // borderRadius: 20
  },
}
