'use strict';

import React, {
} from 'react';

import {
  AsyncStorage,
  Dimensions
} from 'react-native';

var SCREEN = Dimensions.get('window');

module.exports = {
  audioPlayer: {
    width: 210,
    height: 45,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },

  audioPlayerProgress: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    height: 45,
    justifyContent: 'space-around',
    alignSelf: 'center',
    alignItems: 'center',
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },

  audioPlayerProgressView: {
    height: 45,
    alignSelf: 'center'
  },

  audioPlayerTouch: {
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10
  },

  chatRecentRow: {
    flex: 0,
    flexDirection: 'row',
    margin: 10,
    alignItems: 'stretch'
  },
  chatRecentImage:{
    width: 50,
    height: 50,
    borderRadius: 25,
    margin: 10
  },
  chatRecentDetailsContainer: {
    flex: 1,
    paddingLeft:10,
    justifyContent: 'center',
  },

  chatRecentTextName: {
    fontSize: 14,
  },

  chatRecentTextNameBold: {
    fontSize: 14,
    fontWeight: '700',
  },

  chatRecentTextMessage: {
    fontSize: 12,
  },

  chatRecentTextMessageBold: {
    fontSize: 12,
    fontWeight: '700',
  },

  chatRecentButtonContainer: {
    flex: 1,
    paddingLeft:3,
    flexDirection: 'row',
  },

  chatRecentButton: {
    height: 30,
    width: 80,
    flex: 0,
    flexDirection: 'row',
    borderColor: '#1F5490',
    borderRadius: 8,
    marginRight: 10,
    justifyContent: 'center'
  },

  chatRecentButtonText: {
    fontSize: 15,
    fontWeight: '500',
    alignSelf: 'center'
  },

  chatDateContainer: {
    borderRadius: 3,
    alignSelf: 'center'
  },

  chatDateText: {
    fontSize: 12,
    fontWeight:'500'
  },

  chatDetailsText: {
    fontSize: 10,
    paddingLeft: 10,
    paddingRight: 10
  },

  chatImage: {
    height: 150,
    width: 150,
    borderRadius: 10,
    justifyContent:'center',
    alignItems:'center'
  },

  chatHyperlink: {
    textDecorationLine: 'underline'
  },

  chatImageContainer: {
    borderRadius: 10,
    marginLeft: 5,
    marginRight: 5,
  },

  chatMessageContainer: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10
  },

  chatTextContainer: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding: 10,
    width: SCREEN.width * 0.7,
  },

  chatTextContainerOpaque: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  chatTextWrapContainer:{
    flexDirection: 'column',
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 10,
    padding: 10
  },
  chatUserImage: {
    width: 40,
    height: 40,
    borderRadius: 20
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch'
  },

  containerNoResults: {
    flex: 1,
    alignItems: 'stretch'
  },

  contactsCreateGroupImage: {
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    right: 0,
    borderRadius: 10,
    borderWidth: 0.5, 
  },

  contactsCreateGroupMemberTitle: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },

  contactsCreateGroupList: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin:10
  },

  contactsCreateGroupListRow: {
    justifyContent: 'center',
    width: ((SCREEN.width - 20)) / 4,
    height: ((SCREEN.width - 20)) / 4,
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },

  contactsCreateGroupListRowItem: {
    width:((SCREEN.width - 20) - (5*2*4)) / 4,
    height:((SCREEN.width - 20) - (5*2*4)) / 4,
    borderRadius:(((SCREEN.width - 20) - (5*2*4)) / 4) / 2,
    alignItems:'center',
    justifyContent:'center',
    margin:5
  },

  contactsCreateGroupListRowItemRemove: {
    alignSelf: 'flex-start',
    marginLeft: -25,
    borderRadius: 10,
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5
  },

  contactsListViewRow: {
    flex: 0,
    flexDirection: 'row',
    margin:5,
    marginLeft:10,
    alignItems: 'stretch'
  },

  contactsSectionHeader: {
    borderTopWidth:1,
    marginTop:5,
    marginLeft:10,
    marginRight:10,
    flexDirection:'row',
    justifyContent:'center',
  },

  groupModalTitle: {
    flexDirection:'row',
    height:50,
    justifyContent:'center',
    alignItems:'center'
  },

  groupModalTitleText: {
    alignSelf:'center',
    marginLeft:60,
    marginRight:60,
    fontSize:16,
    fontWeight:'800',
  },

  groupModalImage: {
    alignSelf:'stretch',
    width:50,
    height:50,
    borderRadius:25,
    marginLeft:5,
    marginRight:5
  },

  groupNewContactContainer: {
    flexDirection:'row',
    alignItems:'center',
     marginTop:20,
     paddingLeft:15,
     paddingRight:15
  },

  groupNewContactImage: {
    width:40,
    height:40,
    borderRadius:20,
    marginLeft:12.5
  },

  groupNewContactName: {
    paddingLeft:7.5,
    fontSize: 17,
  },

  groupNewImageContainer: {
    alignSelf:'center',
    borderWidth:2,
    width:83,
    height:83,
    borderRadius:50
  },

  groupNewInputName: {
    height:40,
    fontSize:15,
    paddingLeft:10,
    alignSelf: 'stretch',
  },

  groupNewInputEdge: {
    width: 1,
    height: 5,
    marginTop: -5,
    alignSelf: 'flex-end'
  },

  groupNewInputSeparatorName: {
    height: 1,
    width: (SCREEN.width - (SCREEN.width/3)) - 30
  },

  groupNewInputSeparatorPeople: {
    height: 1,
    width:(SCREEN.width) - (SCREEN.width * 0.20),
  },

  hidden: {
    flex: 0,
    height: 0,
    width: 0
  },

  inputContainer: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor:'#fff',

  },

  listViewRowContainer: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    marginLeft: 10,
    alignSelf: 'center'
  },

  listViewDetail: {
    fontSize: 10,
    fontWeight: '400',
  },

  listViewImage:{
    width: 40,
    height: 40,
    borderRadius: 20
  },

  listViewTitle: {
    fontSize: 12,
  },

  loginActivityIndicator: {
    alignSelf: 'center',
    flex: 0,
  },

  loginAppName: {
    alignSelf: 'center',
    marginTop: 0,
    marginBottom: 20,
    height: 140,
    width: 150,
  },

  loginButton: {
    alignSelf: 'stretch',
    height: 50,
    flex: 1,
    borderRadius: 8,
    marginBottom: 15,
    justifyContent: 'center'
  },

  loginButtonText: {
    alignSelf: 'center',
    fontSize: 18,
    textAlign: 'center'
  },

  loginButtonSignup: {
    alignSelf: 'center',
    height: 50,
    width: 200,
    flex: 0,
    borderRadius: 8,
    marginBottom: 10,
    justifyContent: 'center'
  },

  loginError: {
    marginBottom: 10,
    fontSize: 12,
    textAlign: 'center',
  },

  loginSeparator: {
    height:1,
    marginTop:-10
  },

  map: {
    flex: 1
  },
  messageInputContainer: {
    flex:6,
    paddingTop:8,
  },
  messageInput:{
    flex:1,
    paddingLeft:6,
    paddingRight:6,
    fontSize:11,
    flexDirection:'row',
  },
  messageNewContactImage: {
    width:40,
    height:40,
    borderRadius:20
  },

  messageNewContactName: {
    paddingLeft:7.5,
    paddingRight:7.5,
    alignSelf:'center',
    fontSize: 17,
  },

  messageNewInputSeparator: {
    alignSelf:'stretch',
    height: 1,
  },

  messageNewToText: {
    width: 50,
    paddingTop: 9,
    height: 37,
    textAlign: 'center',
    fontSize: 17
  },

  messageNewRecipientInput: {
    flex:1,
    width: SCREEN.width - 70,
    height:37,
    paddingTop:5,
    fontSize:17,
    alignSelf: 'center',
  },

  navbarButton: {
    alignSelf:'center',
    margin:5
  },

  profileImageHome: {
    alignSelf: 'flex-end',
    marginRight: -20,
    borderRadius: 13,
    width: 26,
    height: 26,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    zIndex: 1,
  },

  profileContainerLeftAlign: {
    flex: 1,
    flexDirection: 'row',
    justifyContent:'flex-start',
    alignItems: 'center',
    marginLeft: 30,
    marginTop: 20
  },

  profileContainerTopAlign: {
    flex: 1,
    flexDirection: 'column',
    justifyContent:'flex-start',
    marginLeft: 33,
  },

  profileContainerDetails: {
    flex: 1,
    flexDirection:'row',
    alignItems: 'flex-start',
    marginLeft: 32
  },

  profileContainerDetailsContent: {
    marginLeft: 10,
    marginTop: 15,
  },

  profileDetailsText: {
    fontSize: 16,
    textAlign: 'left',
    margin: 7
  },

  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40
  },

  profilePasswordTextBox: {
    height: 45,
    flex: 1,
    textAlign: 'center',
  },

  profilePasswordTextBoxSeparator: {
    height: 1,
    marginTop: -10
  },

  profileName: {
    fontSize: 20,
    textAlign: 'center',
    marginLeft:20,
  },

  profileSeparator: {
    height: 1,
    marginTop: 20,
    marginRight: 30,
    marginLeft: 30
  },

  profileStatus: {
    fontSize: 16,
    textAlign: 'left',
    margin: 7
  },

  recordButton: {
    alignSelf: 'center',
    height: 100,
    width: 100,
    flex: 0,
    borderRadius: 50,
    marginBottom: 15,
    justifyContent: 'center',
  },

  radioStyle: {
    paddingRight: 15
  },

  radioButtonWrap: {
    marginRight: 3
  },

  reload: {
    alignSelf: 'center',
    height: 50,
    marginTop: 10,
    flex: 0,
  },

  searchBar: {
    marginLeft: 7,
    marginRight: 7,
    flexDirection: 'row',
    alignItems: 'center'
  },

  searchBarInput: {
    fontSize: 13,
    height: 25,
    flex: 1,
    alignSelf:'center',
    margin: 5
  },

  searchBarSpinner: {
    width: 25,
    margin: 5
  },

  textInput: {
    textAlign: 'center',
    height: 45,
    padding: 10,
    fontSize: 14,
  },

  textNoResults: {
    alignSelf: 'center',
    fontSize: 14,
    margin: 10,
    textAlign: 'center'
  },

  themeModal: {
    alignSelf:'center',
    marginLeft:50,
    marginRight:50,
    fontSize:16,
    fontWeight:'800',
  },

  timelineLoggedInUserImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },

  timelinePostUserName: {
    fontSize: 15,
    fontWeight:'800',
  },

  timelinePostDate: {
    fontSize: 9,
  },

  timelinePostUserImage: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },

  timelinePostText: {
    flex: 1,
    fontWeight: '100',
    marginTop: 10,
    marginBottom: 10,
    padding: 10
  },
  
  timelinePost: {
    backgroundColor:'#fff',
    padding:5,
    marginTop:8,
    marginBottom:8,
  },

  timelinePostComment: {
    fontSize: 13,
    lineHeight:24,
  },

  timelinePostInfo: {
    marginLeft:10,
  },

  timelinePosterName: {
    fontWeight:'bold',
  },

  // timelinePostDate: {
  //   fontSize:11,
  //   color:'#888'
  // },



  timelineProfile: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 20,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#000',
    opacity:0.5
  },
  typingArea: {
    height: 40,
    width: SCREEN.width,
    position: 'relative',
    alignItems: 'center',
  },

  typingAreaTextInput: {
    fontSize: 13,
    height: 30,
    margin: 5,
    textAlign: 'left',
  },

  typingAreaButtons: {
    width: 30,
    height: 30,
    margin: 5
  },

  video: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  }
};
