'use strict';

module.exports = {
  listView: {
    paddingLeft:20,
    paddingRight:20,
    marginBottom:50
  },
  text:{
    fontSize:12,
    color: 'gray',
  },
  textBold:{
    fontSize:12,
    color: 'gray',
    fontWeight: '800',
    marginBottom:5,
  },
  thumbnail:{
    width:50,
    height:50,
    borderRadius:25,
    borderWidth:2,
    borderColor: 'gray',
  },
  rightContainer: {
    flex: 1,
    paddingLeft:20,
  },
  li: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom:10,
    marginTop:10,
  },
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  centerText: {
    alignItems: 'center',
  },
  noContactsText: {
    marginTop: 80,
    color: '#888888',
  },
  separator: {
    height: 1,
    backgroundColor: '#eeeeee',
  },
  borderBottom:{
    height:1,
    flex:2,
    backgroundColor:"#bbdefb"
  },
  searchBar:
  {
    justifyContent: 'space-around',
    height: 20,
    fontSize: 15
  },

  searchBarContainer:
  {

    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom:10,
    marginTop:10

  }
};
