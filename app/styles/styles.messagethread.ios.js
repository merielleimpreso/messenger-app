'use strict';

import React, {
} from 'react';

import {
  Dimensions
} from 'react-native';

var windowSize = Dimensions.get('window');
module.exports = {
  container: {
    flex: 1,
  },
  buttons: {
    width: 30,
    height: 30,
    alignSelf: 'center'
  },
  buttonWrap: {
    width: 40,
    padding:5,
    height: 40
  },
  sendButton: {
    width: 30,
    height: 30,
    alignSelf: 'flex-start'
  },
  input: {
    width: windowSize.width - 70,
    color: '#212121',
    height:40,
    marginTop:10,
    fontSize:15,
    alignSelf: 'center',
  },
  buttonsContainer: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  inputContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  textContainer: {
    flex:1,
    justifyContent:'center'
  },
  sendContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  typingArea: {
    width: windowSize.width,
  },
  inputSeparator: {
    alignSelf:'stretch',
    height: 1,
    backgroundColor: '#eeeeee'
  },
  base: {
    width: 55,
    height: 55,
    borderRadius: 27.5
  },
  topContainer:{
  },
  messageContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 25
  },
  messageThread: {
    flex: 1,
    height: windowSize.height,
  },
  messages: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding: 10
  },
  recipientInput: {
    flex:1,
    width: windowSize.width - 70,
    color: '#212121',
    height:37,
    paddingTop:5,
    fontSize:17,
    alignSelf: 'center',
  },
  disabledText:{
    width:50,
    paddingTop:9,
    height:37,
    textAlign: 'center',
    fontSize:17,
    color:'#888',
  },
  contentBox:{
    height:windowSize.height - 195,
  },
  messageBox:{
    height:windowSize.height - 160,
  }
};
