'use strict';

import React, {
} from 'react';

import {
  Dimensions
} from 'react-native';

var GLOBAL = require('./../modules/config/Globals.js');
let SCREEN = Dimensions.get("window");

module.exports = {
  buttons: {
    width: 30,
    height: 30,
    margin: 5,
    alignSelf: 'center'
  },

  buttonsContainer: {
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
  },

  inputContainer: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  inputSeparator: {
    height: 1,
  },

  messageInputContainer: {
    flex:6,
    paddingBottom:8,
  },
  messageInput:{
    flex:1,
    paddingLeft:6,
    paddingRight:6,
    fontSize:11,
    flexDirection:'row',
  },
  textContainer: {
    flex:1,
    justifyContent:'center',
  },

  typingArea: {
    flex:1,
  },
}
