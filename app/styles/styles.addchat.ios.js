'use strict';

import React, {
} from 'react';

import React, {
  Dimensions
} from 'react-native';

module.exports = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#ffffff'
  },

  // Top Container Create button
  addContainer: {
    height: 45,
    backgroundColor: '#9e9e9e',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  buttonCreate: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingTop: 5
  },
  doneIcon: {
    fontSize: 40,
    color: 'white',
  },
  txtCreateContainer: {
    paddingTop: 10,
    paddingRight: 10
  },
  txtCreate: {
    color: 'white',
  },

  // main
  mainContainer: {
    flex: 10,
    paddingRight: 15,
    paddingLeft: 15
  },

  // Add picture and name
  pictureNameContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 105,
    paddingTop: 15,
    paddingBottom: 15
  },
  pictureContainer: {
    height: 75,
    width: 75,
    borderRadius: 50,
    borderColor: '#dbdbdb',
    borderWidth: 1,
    padding: 20
  },
  cameraIcon: {
    fontSize: 40,
  },
  nameContainer: {
    width: Dimensions.get('window').width - 100,
    paddingTop: 15,
    paddingLeft: 15,
    height: 40
  },
  txtName: {
    justifyContent: 'space-around',
    height: 40,
    fontSize: 15,
  },
  txtSearchName: {
    justifyContent: 'space-around',
    height: 40,
    fontSize: 15,
  },
  underline: {
    height: 3,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
  },
  searchContainer: {
    height: 40,
    paddingBottom: 15,
  },
  contactContainer: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  checkboxIconContainer: {
    paddingRight: 5,
  },
  checkboxIcon: {
    fontSize: 25,
  },
  contactName: {
    fontSize: 15,
    fontWeight: '800'
  },
  base: {
    width: 55,
    height: 55,
  },
  thumbnail: {
    width:55,
    height:55,
    borderRadius:50,
    borderColor: 'gray',
    marginLeft: 10
  },
  li: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 10,
  },
  rightContainer: {
    flex: 1,
    paddingLeft:20,
  },
  text:{
    fontSize:12,
    color: 'gray',
  },
  textBold:{
    fontSize:12,
    color: 'gray',
    fontWeight: '800',
    marginBottom:5,
  },

  /**
   * Additional styles for AddNewMessage.android.js
   */
  inputContainer: {
    height: 50,
    backgroundColor: '#039be5',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  typeContainer: {
    width: Dimensions.get('window').width - 50,
    height: 40,
    paddingTop: 8
  },
  typeName: {
    justifyContent: 'space-around',
    height: 40,
    fontSize: 18,
    backgroundColor: 'transparent',
    color: '#fff'
  }
};
