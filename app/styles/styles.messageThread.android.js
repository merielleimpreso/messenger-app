'use strict';

import React, {
} from 'react';

import {
  Dimensions
} from 'react-native';

var windowSize = Dimensions.get('window');

module.exports = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#ffffff'
  },

  // HEADER
  topContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eeeeee',
    borderBottomWidth: .8,
    borderBottomColor: '#29b6f6'
  },
  backTouchable: {
    paddingLeft: 5,
    width: 30,
    height: 50,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  divider: {
    height: 35,
    width: 1,
    backgroundColor: '#039be5'
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    //flexDirection: 'row',
    marginLeft: 10,
    marginBottom:5
  },
  headerSeparator: {
    height: 1,
    backgroundColor: '#29b6f6'
  },

  // THREAD
  chatContainer: {
    flex: 10,
  },
  messageThread: {
    flex: 1,
    height: windowSize.height - 175
  },
  messageContainer: {
    flexDirection: 'row',
    padding: 8,
    //marginBottom: 10,
  },
  messages: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    padding: 10
  },

  // INPUT AREA
  typingArea: {
    width: windowSize.width,
    position: 'relative'
  },
  inputSeparator: {
    height: 1,
    backgroundColor: '#eeeeee',
  },
  inputContainer: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'space-around',
    //alignItems: 'center',
    backgroundColor: '#fff'
  },
  textContainer: {
    flex:1,
    justifyContent:'center',
  },
  sendContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 10
  },
  sendLabel: {
    color: '#e0e0e0',
    fontSize: 15
  },
  input: {
    width: windowSize.width - 40,
    color: '#212121',
    paddingTop: 10,
    //marginLeft: 5,
    alignSelf: 'center',
    backgroundColor: '#ffffff'
  },
  sendButton: {
    width: 30,
    height: 30,
    marginTop: 10,
    alignSelf: 'center'
  },
  buttonsContainer: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  buttons: {
    width: 30,
    height: 30,
    marginTop: 10,
    alignSelf: 'center'
  },
  base: {
    width: 55,
    height: 55,
  },
  footer: {
    height: 25,
    backgroundColor: '#039be5',
  },
  buttonWrap: {
    width: 50,
    height: 50,
  },
  actionButtonIcon: {
    fontSize: 25,
    color: '#BDBDBD',
    alignSelf: 'center'
  },
  messagePhoto: {
    height: 100,
    width: 100
  },
  loaderContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  }
};
