'use strict';

module.exports = {
  listView: {
    paddingLeft:20,
    paddingRight:20,
    marginBottom:50
  },
  text:{
    fontSize:12,
    color: 'gray',
    fontWeight: '300',
  },
  textBold:{
    fontSize:12,
    color: 'gray',
    fontWeight: '800',
    marginBottom:5,
  },
  thumbnail:{
    width:50,
    height:50,
    borderRadius:25,
    borderWidth:2,
    borderColor: 'gray',
  },
  noMessagesText: {
    marginTop: 80,
    color: '#888888',
  },
  li: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginBottom:10,
    marginTop:10,
  },
  rightContainer: {
    flex: 1,
    paddingRight:20,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  borderBottom:{
    height:1,
    flex:2,
    backgroundColor:"#bbdefb"
  },
  actionButtonIcon: {
    fontSize: 25,
    color: 'white',
  },
  button: {
    height: 30,
    width: 80,
    flex: 0,
    flexDirection: 'row',
    backgroundColor: '#1F5490',
    borderColor: '#1F5490',
    borderRadius: 8,
    marginRight: 10,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 15,
    color: 'white',
    fontWeight: '500',
    alignSelf: 'center'
  },
  buttonContainer: {
    flex: 1,
    paddingLeft:3,
    flexDirection: 'row',
  },
};
