import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  connnectionStatus: {
    alignItems:'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  loading: {
    flex:1
  }
});
