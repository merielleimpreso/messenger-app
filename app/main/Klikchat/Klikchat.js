'use strict';

import React, { Component } from 'react';
import {
  AsyncStorage,
  ActivityIndicator,
  AppState,
  BackAndroid,
  Keyboard,
  LayoutAnimation,
  Platform,
  NetInfo,
  Navigator,
  Text
} from 'react-native';
import _ from 'underscore';
import {AudioPlayer} from 'react-native-audio';
import AddFriends from '../../modules/AddFriends';
import AddGroupChat from '../../modules/AddGroupChat';
import AddUserToGroup from '../../modules/AddUserToGroup';
import ChangeMobileNumber from '../../modules/ChangeMobileNumber';
import Chat from '../../modules/config/db/Chat';
import ConnectionStatus from '../../modules/common/ConnectionStatus';
import ChangePassword from '../../modules/ChangePassword';
import ChangeWallpaper from '../../modules/scenes/ChangeWallpaper';
import ContactsInvite from '../../modules/ContactsInvite';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import dpp from '../../modules/config/ddp.js';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import FriendList from '../../modules/scenes/FriendList';
import GLOBAL from '../../modules/config/Globals.js';
import GroupDetails from '../../modules/scenes/GroupDetails';
import InputCountryCode from '../../modules/InputCountryCode';
import KliklabsCongratulations from '../../modules/KliklabsCongratulations';
import KliklabsPortal from '../../modules/scenes/KliklabsPortal';
import KliklabsConfirmCode from '../../modules/KliklabsConfirmCode';
import KliklabsRegister from '../../modules/scenes/KliklabsRegister';
import KliklabsSignUp from '../../modules/scenes/KliklabsSignUp';
import Login from '../../modules/Login';
import Map from '../../modules/Map';
import Message from '../../modules/config/db/Message';
import MessageInfo from '../../modules/MessageInfo';
import MessagesGiftedChat from '../../modules/scenes/MessagesGiftedChat'
import PhotoInfo from '../../modules/PhotoInfo';
import DataUsage from '../../modules/DataUsage';
import Post from '../../modules/config/db/Post';
import Profile from '../../modules/Profile';
import RNRestart from 'react-native-restart';
import SearchOtherMember from '../../modules/SearchOtherMember';
import Settings from '../../modules/scenes/Settings';
import ShareMedia from '../../modules/ShareMedia';
import SoundEffects from '../../modules/actions/SoundEffects';
import StoreCache from '../../modules/config/db/StoreCache';
import styles from './styles';
import SystemAccessor from '../../modules/SystemAccessor';
import Tabs from '../../modules/Tabs';
import TextView from '../../modules/GiftedChat/TextView';
import Timeline from '../../modules/Timeline';
import TimelinePost from '../../modules/TimelinePost';
import TimelinePoster from '../../modules/TimelinePoster';
import TimelinePostComments from '../../modules/TimelinePostComments';
import Users from '../../modules/config/db/Users';
import VideoPlayer from '../../modules/VideoPlayer';
import ViewPhoto from '../../modules/ViewPhoto';

process.nextTick = setImmediate;

let navigator;
let isSafeToBack = true;
let goToSignUp = null;
var ddpWasClosed = false;
var previousNotif;
var iosHasInitFCM = false; //for ios.

class Klikchat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hasInternet: true, // states for connecting to ddp
      isDDPConnecting: true,
      isDDPConnected: false,

      isCheckingLogin: false, // states for login
      isLoggingIn: false,
      isLoggedIn: false,
      isLoggedInDDP: false,
      loggedInUser: null,
      loggedInUserIsVerified: false,
      shouldLogin: false,
      shouldRenderConnection: false,

      hasPreviousSession: false, // for cache
      isCheckingPreviousSession: true,

      audio: null,
      backgroundImageSrc: null,
      hasNewFriendRequest: false,
      theme: GLOBAL.THEMES[0],
      timeFormat: '12',
      isWaitingToRemoveNotif: false, //To cancel notif upon toFront from background.
      isAutoSaveMedia: true
    }
    this.handleAppState = this.handleAppState.bind(this);
    this.handleConnectivityChange = this.handleConnectivityChange.bind(this);
    this.initializeDDP = this.initializeDDP.bind(this);
    this.removeConnectivityListener = this.removeConnectivityListener.bind(this);

    // Functions passed as props
    this.checkLoginToken = this.checkLoginToken.bind(this);
    this.goToSignUp = this.goToSignUp.bind(this);
    this.subscribeContacts = this.subscribeContacts.bind(this);
    this.subscribeToLoggedInUser = this.subscribeToLoggedInUser.bind(this);

    // Getter methods
    this.audio = this.audio.bind(this);
    this.backgroundImageSrc = this.backgroundImageSrc.bind(this);
    this.hasInternet = this.hasInternet.bind(this);
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.loggedInUser = this.loggedInUser.bind(this);
    this.logout = this.logout.bind(this);
    this.theme = this.theme.bind(this);
    this.timeFormat = this.timeFormat.bind(this);
    this.getMessageWallpaper = this.getMessageWallpaper.bind(this);
    this.hasNewFriendRequest = this.hasNewFriendRequest.bind(this);

    // Setter methods
    this.setAudio = this.setAudio.bind(this);
    this.setIsSafeToBack = this.setIsSafeToBack.bind(this);
    this.setLoggedInUser = this.setLoggedInUser.bind(this);
    this.setSignUpCallback = this.setSignUpCallback.bind(this);
    this.setIsLoggingOut = this.setIsLoggingOut.bind(this);

    this.render = this.render.bind(this);
    this.renderConnectionStatus = this.renderConnectionStatus.bind(this);
    this.renderLoading = this.renderLoading.bind(this);
    this.renderNavigatorWithLogin = this.renderNavigatorWithLogin.bind(this);
    this.renderNavigatorWithTab = this.renderNavigatorWithTab.bind(this);

    this.ddpClose = this.ddpClose.bind(this);
    this.setInitialState = this.setInitialState.bind(this);
    this.isAutoSaveMedia = this.isAutoSaveMedia.bind(this);
    this.forceRestart = this.forceRestart.bind(this);
    this.initFCMMethods = this.initFCMMethods.bind(this);
  }

  componentDidMount() {
    console.log('Klikchat componentDidMount');
    this._isMounted = true;
    this.initFCMMethods();
    if (Platform.OS === 'android') {
      //this.initFCM();
      this.setTimeFormat();
      SystemAccessor.initializeFFmpeg();//Video conversion
      SystemAccessor.shouldPlaySound((err, shouldPlaySound)=>{
        console.log("Should play sound: ",shouldPlaySound );
      });

      SystemAccessor.getStartingIntent((err ,notif)=>{

        console.log("err: ", err);
        console.log("Notif: ", notif);
        if(err == null) {
          var data = {
              _id: notif.id,
              name: notif.title,
              image: notif.image
          };
          console.log("Data before redirect ", data);
          console.log("Redirecting to message");
          this.redirectToChatMessage(data, notif.chatType);
        } else {
          console.log("Not redirecting to chat message");
        }

      })
    } else {
      //this.initFCM();
    }

    SoundEffects.initAudio();
    this.addListenerForKeyboard();
    this.getMessageWallpaper();
    this.loadCache();
    this.addConnectivityListener();
    AppState.addEventListener('change', this.handleAppState);

    this.initializeDDP();

    ddp.isLoggedIn = false;
    this.subscribeContacts();
  }

  componentWillUnmount() {
    this._isMounted = false;
    console.log('Klikchat componentWillUnmount');
    AppState.removeEventListener('change', this.handleAppState);
    this.removeConnectivityListener();
    this.refreshUnsubscribe();
  }

  handleAppState(appState) {
    if (appState == 'background') {
      console.log("Is background");
      dismissKeyboard();
      if (this.state.audio) {
        this.setState({audio: null});
        AudioPlayer.stop();
        console.log('STOP')
      }
    } else if(appState == 'active') {
      console.log("Is Active");
      NetInfo.isConnected.fetch().then( isConnected =>{
        //initializeDDP();
        //this.handleConnectivityChange(isConnected);
        console.log("Netinfo isconnected: ", isConnected , " ddp.connected: ",ddp.connected, " ddpWasClosed: ", ddpWasClosed);
        if (isConnected && ddpWasClosed) {
          this.forceRestart();
        }
        this.setState({
          shouldRenderConnection: !isConnected,
        });
      });
      this.setState({
        isWaitingToRemoveNotif: false //only purpose is to toggle to setState, to
                                      //trigger open message thread's didUpdate to
                                      //delete notifs on open thread.
      })
    }
  }

  initFCM() {
    console.log("Init FCM");
    AsyncStorage.getItem('iosHasInitFCM').then((hasInit)=>{

      if(Platform.OS == 'ios' && !hasInit) {
        FCM.requestPermissions();
        AsyncStorage.setItem('iosHasInitFCM', JSON.stringify(true));
      }

      FCM.getFCMToken().then(token => {
        console.log('Klikchat: FCM device token = ' + token);
        GLOBAL.FCM_USER_TOKEN = token;


        //FCM token is bound to every login to every device (1 unique for every login in device)
        //ie when user1 logs in a device, that has separate fcm than user1's log in another device
        //we subscribe to a topic with format /topics/<id>_<Platform>
        //this is because android and ios require different message formats for our purposes. tuod. -Tere
        var newTopic = '/topics/' + this.loggedInUser()._id +"_"+ Platform.OS;
        FCM.subscribeToTopic(newTopic);


      });
    });





  }

  initFCMMethods() {
    // Triggered when notif is clicked
    // There are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    this.notificationUnsubscribe = FCM.on('notification', (notif) => {
      console.log('Klikchat: FCM notification = ', notif);
      // console.log('Klikchat: FCM initial data = ', FCM.initialData);
      if (notif.isInvitation === 'true') {
        console.log('TRUE')
        this.setState({
          hasNewFriendRequest: true
        })
      }

      if (notif.fcm != null) {
        console.log("Notif.fcm.action: "+ notif.fcm.action);
      }

      this.state.previousNotifID = this.state.newNotifID;
      this.state.newNotifID = notif.id;

      if (Platform.OS == "android") {
        console.log("Notif. ddpWasClosed: ", ddpWasClosed);
          // Will only trigger if pressed from tray
        if (notif.fcm != null && (notif.fcm.action === 'clicked' || notif.fcm.action === 'invitation') && !ddpWasClosed) {
          if (notif.fcm.action == 'reply') {
            console.log('Klikchat: FCM: Reply command received');
          } else if (notif.isInvitation === 'true') {
            this.goToAddFriends();
          } else {
            var data = {
              _id: notif.notificationID,
              name: notif.title,
              image: notif.image
            };

            this.redirectToChatMessage(data, notif.chatType);
          }
        } else {
          console.log('Klikchat: FCM: Notif received not tapped');
        }
      } else {//ios

        console.log("Klikchat: FCM opened_from_tray: ", notif.opened_from_tray);
          if(notif.opened_from_tray == 1 ) {
            if (notif.isInvitation === 'true') {
              this.goToAddFriends();
            } else {
              var data = {
                _id: notif.notificationID,
                name: notif.name,
                image: notif.image
              };

              this.redirectToChatMessage(data, notif.chatType);
            }
          } else {
            console.log('Klikchat: FCM: Notif received not tapped');
          }
          }





      if (_.isEmpty(notif)) {
        console.log('observed');
        this.setState({isObserved: true});
      }

      this.setState({
        hasNewFriendRequest: false
      })
    });

    this.refreshUnsubscribe = FCM.on('refreshToken', (token) => {
      console.log('Klikchat: FCM refreshed token = ' + token)
      GLOBAL.FCM_USER_TOKEN = token;

      // FCM token may not be available on first load, catch it here
      ddp.call('storeFCMToken', [token]).catch(error => {
        console.log('Klikchat: storeFCMToken error: ', error);
      });
    });
  }

  subscribeContacts() {
    if (this.isLoggedIn()) {
      Users.subscribeContacts().then(() => {
        console.log('Klikchat: contacts subscribed');
        Users.observeContacts(this.loggedInUser(), (contacts) => {
          console.log('Klikchat: contacts observed');
        });
      }).catch((e) => console.log('Klikchat: error subscribeContacts', e));
    }
  }

  goToAddFriends() {
    let navigatorStack = navigator.getCurrentRoutes();
    let currentRoute = navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'AddFriends') {
      requestAnimationFrame(() => {
        navigator.push({
          name:'AddFriends',
          audio: this.audio,
          backgroundImageSrc: this.backgroundImageSrc,
          hasInternet: this.hasInternet,
          hasNewFriendRequest: this.hasNewFriendRequest,
          isLoggedIn: this.isLoggedIn,
          isSafeToBack: this.isSafeToBack,
          loggedInUser: this.loggedInUser,
          logout: this.logout,
          renderConnectionStatus: this.renderConnectionStatus,
          setAudio: this.setAudio,
          setIsSafeToBack: this.setIsSafeToBack,
          subscribeContacts: this.subscribeContacts,
          subscribeToLoggedInUser: this.subscribeToLoggedInUser,
          theme: this.theme,
          timeFormat: this.timeFormat
        });
      });
    }
  }

  goToMessageChat(data, chatType) {
    StoreCache.getCache(StoreCache.keys.isAutoSaveMedia, (result) => {
      result = (result == undefined) ? false : JSON.parse(result);
      if (this._isMounted) {
        this.setState({isAutoSaveMedia: result});
      }

      let groupData = null;
      let directData = null;
      if (chatType == 'group') {
        groupData = {
          _id: data._id,
          name: data.name,
          image: data.image
        };
        directData = groupData;
      } else {
        directData = data;
      }

      requestAnimationFrame(() => {
        navigator.push({
          name:'MessagesGiftedChat',
          audio: this.audio,
          backgroundImageSrc: this.backgroundImageSrc,
          group: groupData,
          getMessageWallpaper: this.getMessageWallpaper,
          hasInternet: this.hasInternet,
          isLoggedIn: this.isLoggedIn,
          isSafeToBack: false,
          loggedInUser: this.loggedInUser,
          logout: this.logout,
          renderConnectionStatus: this.renderConnectionStatus,
          setAudio: this.setAudio,
          setIsSafeToBack: this.setIsSafeToBack,
          setLoggedInUser: this.setLoggedInUser,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          timeFormat: this.timeFormat,
          theme: this.theme,
          user: directData,
          users: data.userGroups,
          isAutoSaveMedia: this.isAutoSaveMedia
        });
      });
    });
  }

  goToSignUp() {
    let navigatorStack = navigator.getCurrentRoutes();
    let currentRoute = navigator.getCurrentRoutes().length - 1;
    let goToRoute = (navigatorStack[currentRoute].name != 'KliklabsSignUp') ?  'KliklabsSignUp' : 'KliklabsPortal';

    requestAnimationFrame(() => {
      navigator.push({
        name: goToRoute,
        audio: this.audio,
        backgroundImageSrc: this.backgroundImageSrc,
        goToSignUp: this.goToSignUp,
        hasInternet: this.hasInternet,
        hasNewFriendRequest: this.hasNewFriendRequest,
        isLoggedIn: this.isLoggedIn,
        isSafeToBack: this.isSafeToBack,
        loggedInUser: this.loggedInUser,
        logout: this.logout,
        renderConnectionStatus: this.renderConnectionStatus,
        setAudio: this.setAudio,
        setIsSafeToBack: this.setIsSafeToBack,
        setSignUpCallback: this.setSignUpCallback,
        subscribeToLoggedInUser: this.subscribeToLoggedInUser,
        theme: this.theme,
        timeFormat: this.timeFormat,
        userConnectionStatus: this.userConnectionStatus
      });
    });
  }

  redirectToChatMessage(data, chatType) {
    if(this.state.loggedInUser != null) {
      this.goToMessageChat(data, chatType);
    } else {
      setTimeout(()=>{
        this.redirectToChatMessage(data,chatType)
      }, 1500)
    }
  }

  addListenerForKeyboard() {
    const updateListener = Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow';
    const resetListener = Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide';

    this._listeners = [
      Keyboard.addListener(updateListener, this.updateKeyboardSpace),
    ];
    AsyncStorage.getItem('keyboardHeight').then((result) =>{
        ddp.keyboardSpace = parseInt(result);
    });
  }

  //handle keyboardspace throughout
  updateKeyboardSpace(frames) {
    if (!frames.endCoordinates) {
      return;
    }
    const keyboardSpace = frames.endCoordinates.height;
    if (ddp.keyboardSpace !== keyboardSpace || !ddp.keyboardSpace) {
      console.log('UPDATING keyboard');
      ddp.keyboardSpace = keyboardSpace;
      AsyncStorage.setItem('keyboardHeight', JSON.stringify(keyboardSpace));
      console.log('keyboard UPDATED');
    }
  }

  loadCache() {
    StoreCache.storeCache(StoreCache.keys.isAutoSaveMedia, this.state.isAutoSaveMedia);
    StoreCache.hasPreviousSession((hasPreviousSession) => {
      if (hasPreviousSession) {
        StoreCache.getCache(StoreCache.keys.loggedInUser, (result) => {
          if (this._isMounted) {
            this.setState({
              loggedInUser: result,
              hasPreviousSession: hasPreviousSession,
              isCheckingPreviousSession: false
            });
          }
        });
      } else {
        if (this._isMounted) {
          this.setState({
            hasPreviousSession: hasPreviousSession,
            isCheckingPreviousSession: false
          });
        }
      }
    });
  }

  setTimeFormat() {
    SystemAccessor.getTimeFormat((error, format) => {
      if (format.timeFormat != this.state.timeFormat) {
        if (this._isMounted) {
          console.log('Klikchat: time format = ' + format.timeFormat);
          this.setState({
            timeFormat: format.timeFormat
          });
        }
      }
    });
  }

  getMessageWallpaper() {

    AsyncStorage.getItem('backgroundImageSrc')
    .then(bgImage => {
      console.log('Klikchat: Set the saved wallpaper');

      if (this._isMounted) {
        this.setState({backgroundImageSrc: bgImage});
      }
    });
  }

  addConnectivityListener() {
    console.log("Added Connectivity Listener.");
    NetInfo.isConnected.addEventListener(
      'change',
      this.handleConnectivityChange
    );
  }

  removeConnectivityListener() {
    NetInfo.isConnected.removeEventListener(
        'change',
        this.handleConnectivityChange
    );
  }

  handleConnectivityChange(isConnected) {
    console.log('Klikchat.js: Connectivity change, hasInternet:' + isConnected);
    //ddp.connected = isConnected;

    console.log("ddp connected set to: "+ ddp.connected);
    // Re-initializing DDP
    if (!isConnected && !this.state.isLoggedIn) {
      if (this._isMounted) {
        console.log("Entered Re-init ddp");
        this.setState({
          isDDPConnecting: false,
          isDDPConnected: false,
          isCheckingLogin: true,
        });
      }
    }

    // Set if there is a connection
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({
        hasInternet: isConnected,
      });
    }

    if (!isConnected) {
      if (this._isMounted) {
        this.setState({
          isLoggedIn: false,
          shouldRenderConnection: true
        });
      }
      ddp.isLoggedIn = false;
    }

    console.log("HandleAppState. isDDPConnecting:  ",this.state.isDDPConnecting, " isConnected: ", isConnected, " ddp.connected: ", ddp.connected );
    // Only re-initialize ddp if it is already initialized
    if (!this.state.isDDPConnecting && isConnected && !ddp.connected) {
      ddp.connected = isConnected;
      this.initializeDDP();
    }
  }

  initializeDDP() {
    Chat.stopObserving();
    Message.stopObserving();
    Post.stopObserving();
    Users.stopObserving();
    ddp.close();
    console.log('initialize DDP');

    if (this._isMounted) {
      this.setState({
        isLoggedInDDP: false,
        isLoggedIn: false
      })
      ddp.isLoggedIn = false;
    }
    setTimeout(()=>{

          ddp.initialize().then((result) => {
            // Initialization of DDP is success.
            console.log("DDP result: ", result);
            ddp.setCloseCallBack(this.ddpClose);
            ddp.setForceCloseCallback(this.forceRestart);

            if (result) {
              console.log(`Klikchat.js: DDP initialization success, connected to ${ddp.host}:${ddp.port}`);
              if (this._isMounted) {
                this.setState({
                  isDDPConnected: true,
                  isDDPConnecting: false,
                  hasInternet: true,
                })
              }
              if (!this.state.isLoggedIn) {
                if (this._isMounted) {
                  this.setState({
                    isCheckingLogin: true
                  });
                }
              }
              this.checkLoginToken();
            }
          }).catch(error => { // Initialization of DDP failed.
            console.log('Klikchat: DDP initialization failed: ' + error);

            if (this._isMounted) {
              this.setState({
                isDDPConnected: false,
                isDDPConnecting: false,
                isCheckingLogin: true,
                hasInternet: false,
                shouldRenderConnection: true
              })
            }

            // if(error.indexOf("Bad Gateway")){



            //   setTimeout(()=>{
            //     if(this.state.hasInternet) {
            //         this.forceRestart();
            //     }

            //   }, 15000);

            // }


            NetInfo.isConnected.fetch().then( isConnected =>{
              //console.log("Netinfo isconnected: ", isConnected , " ddp.connected: ",ddp.connected, " ddpWasClosed: ", ddpWasClosed);
              if (isConnected && ddpWasClosed) {

                this.forceRestart();
              }
            });

          })
    }, 1500);

  }

  checkLoginToken() {
    console.log('Klikchat: Checking if there is a login token saved...')

    // Get login token saved through AsyncStorage.
    AsyncStorage.getItem('loginToken').then((res) => {
      if (res) { // A login token is saved.
        console.log('Klikchat: A login token is saved');
        if (!this.state.isLoggedIn) {
          if (this._isMounted) {
            this.setState({
              isCheckingLogin: false,
              shouldLogin: true,
              isLoggingIn: true
            });
          }
        }
        if (this.state.isDDPConnected) {
          // Log in the user with the saved token.
          this.loginWithToken(res);
        }
        // Load saved data from last login while logging in
        //this.loadOffline();
      } else {
        // No login token saved.
        console.log('Klikchat: No login token saved');
        if (this._isMounted) {
          this.setState({
            isCheckingLogin: false,
            shouldLogin: false
          });
        }
      }
    });
  }

  // Call loginWithToken function
  loginWithToken(res) {
    console.log('Klikchat]: Logging in with token...');

    ddp.loginWithToken(res, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        if (this._isMounted) {
          this.setState({
            isLoggedInDDP: true
          });
        }
        this.subscribeToLoggedInUser();
      }
    });
  }

  subscribeToLoggedInUser(isLoginWithUserName, setIsLoggedIn) {
    //if(this.state.isLoggedIn) {
      Users.subscribeToCurrentUser().then(() => {
        this.observeCurrentUser();
        this.subscribeContacts();
      }).catch((error) => {
        console.log('Klikchat: Subscribe to current user error = ' + error);
        this.checkLoginToken();
      });
    //}


  }

  observeCurrentUser() {
    Users.observeCurrentUser(result => {
      this.checkIfUserIsVerified(result);
    });
  }

  // Call currentUser function from server to check if user is verified
  checkIfUserIsVerified(loggedInUser) {
    if (loggedInUser) {
      let loggedInUser = Users.getCurrentUser();

      if (loggedInUser.profile.isVerified) { // User is verified.
        console.log('Klikchat: User is verified and logged in');
        ddp.isLoggedIn = true;

        StoreCache.save(StoreCache.keys.loggedInUser);
        if (this._isMounted) {


          this.setState({
            isCheckingLogin: false,
            isLoggingIn: false,
            isLoggedIn: true,
            loggedInUser: loggedInUser,
            loggedInUserIsVerified: true
          });

          this.initFCM();
        }
      } else { // User is not verified.
        console.log('Klikchat: User needs to be verified')
        if (this._isMounted) {
          this.setState({
            isCheckingLogin: false,
            isLoggingIn: false,
            isLoggedIn: false,
            loggedInUserIsVerified: false
          })
        }
      }
    }
  }

  render() {
    if (this.state.isCheckingPreviousSession) {
      return this.renderLoading();
    } else {
      return (this.state.hasPreviousSession) ? this.renderNavigatorWithTab() : this.renderNavigatorWithLogin();

      // return <Timeline
      //   {...this.props}
      //   navigator={navigator}
      //     addContactToLoggedInUser={this.addContactToLoggedInUser}
      //     audio={this.audio}
      //     backgroundImageSrc={this.backgroundImageSrc}
      //     debugText={this.debugText}
      //     changeTheme={this.changeTheme}
      //     getMessageWallpaper={this.getMessageWallpaper}
      //     hasInternet={this.hasInternet}
      //     isDebug={this.isDebug}
      //     isLoggedIn={this.isLoggedIn}
      //     isSafeToBack={this.isSafeToBack}
      //     loggedInUser={this.loggedInUser}
      //     logout={this.logout}
      //     renderConnectionStatus={this.renderConnectionStatus}
      //     setAudio={this.setAudio}
      //     setForcedGoToThreadCallback={this.setForcedGoToThreadCallback}
      //     setIsSafeToBack={this.setIsSafeToBack}
      //     setLoggedInUser={this.setLoggedInUser}
      //     setSignUpCallback={this.setSignUpCallback}
      //     shouldReloadData={this.shouldReloadData}
      //     subscribeToLoggedInUser={this.subscribeToLoggedInUser}
      //     theme={this.theme}
      //     timeFormat={this.timeFormat}
      //     checkLoginToken={this.checkLoginToken}
      //     setIsLoggingOut={this.setIsLoggingOut}
      //     isLoggingOut={this.isLoggingOut}
      //
      // />
    }
  }

  renderLoading() {
    let color = GLOBAL.COLOR_THEME[this.state.theme];
    return (
      <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} style={styles.loading} />
    );
  }

  renderNavigatorWithLogin() {
    let color = GLOBAL.COLOR_THEME[this.state.theme];
    return (
      <Navigator style={styles.container}
        renderScene={this.renderScene.bind(this)}
        initialRoute={{
          name: 'KliklabsPortal',
          audio: this.audio,
          backgroundImageSrc: this.backgroundImageSrc,
          getMessageWallpaper: this.getMessageWallpaper,
          goToSignUp: this.goToSignUp,
          hasInternet: this.hasInternet,
          hasNewFriendRequest: this.hasNewFriendRequest,
          isSafeToBack: this.isSafeToBack,
          isLoggedIn: this.isLoggedIn,
          loggedInUser: this.loggedInUser,
          logout: this.logout,
          renderConnectionStatus: this.renderConnectionStatus,
          setAudio: this.setAudio,
          setIsSafeToBack: this.setIsSafeToBack,
          setLoggedInUser: this.setLoggedInUser,
          setSignUpCallback: this.setSignUpCallback,
          subscribeToLoggedInUser: this.subscribeToLoggedInUser,
          theme: this.theme,
          timeFormat: this.timeFormat,
          userConnectionStatus: this.userConnectionStatus
        }}
        ref={(nav) => { navigator = nav; }}
        configureScene={() => ({...Navigator.SceneConfigs.HorizontalSwipeJump, gestures: {} })}
      />
    );
  }

  renderNavigatorWithTab() {
    let color = GLOBAL.COLOR_THEME[this.state.theme];
    return (
      <Navigator style={styles.container}
        renderScene={this.renderScene.bind(this)}
        initialRoute={{
          name: 'Tabs',
          audio: this.audio,
          backgroundImageSrc: this.backgroundImageSrc,
          hasInternet: this.hasInternet,
          goToSignUp: this.goToSignUp,
          isLoggedIn: this.isLoggedIn,
          isSafeToBack: this.isSafeToBack,
          hasNewFriendRequest: this.hasNewFriendRequest,
          loggedInUser: this.loggedInUser,
          logout: this.logout,
          renderConnectionStatus: this.renderConnectionStatus,
          setAudio: this.setAudio,
          setIsSafeToBack: this.setIsSafeToBack,
          setSignUpCallback: this.setSignUpCallback,
          setLoggedInUser: this.setLoggedInUser,
          subscribeToLoggedInUser: this.subscribeToLoggedInUser,
          theme: this.theme,
          timeFormat: this.timeFormat,
          userConnectionStatus: this.userConnectionStatus,
          getMessageWallpaper: this.getMessageWallpaper
        }}
        ref={(nav) => { navigator = nav; }}
        configureScene={() => ({...Navigator.SceneConfigs.HorizontalSwipeJump, gestures: {} })}
      />
    );
  }

  renderScene(route, navigator) {
    switch (route.name) {
      case 'AddNewMessage':
        return (
          <AddNewMessage navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            data={route.data}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            hasInternet={route.hasInternet}
            isAutoSaveMedia={route.isAutoSaveMedia}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
          />
        );
      case 'AddFriends':
        return (
          <AddFriends navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isAutoSaveMedia={route.isAutoSaveMedia}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            shouldReloadData={route.shouldReloadData}
            subscribeContacts={route.subscribeContacts}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            setSubscribeContacts={route.setSubscribeContacts}
            theme={route.theme}
            timeFormat={route.timeFormat}
            users={route.users}
          />
        );
        case 'AddGroupChat':
          return (
            <AddGroupChat navigator={navigator}
              audio={route.audio}
              backgroundImageSrc={route.backgroundImageSrc}
              data={route.data}
              hasInternet={route.hasInternet}
              isAutoSaveMedia={route.isAutoSaveMedia}
              isLoggedIn={route.isLoggedIn}
              isSafeToBack={route.isSafeToBack}
              loggedInUser={route.loggedInUser}
              refreshGroups={route.refreshGroups}
              renderConnectionStatus={route.renderConnectionStatus}
              setAudio={route.setAudio}
              setIsSafeToBack={route.setIsSafeToBack}
              shouldReloadData={route.shouldReloadData}
              theme={route.theme}
              timeFormat={route.timeFormat}
            />
          );
        case 'AddUserToGroup':
          return (
            <AddUserToGroup navigator={navigator}
              audio={route.audio}
              backgroundImageSrc={route.backgroundImageSrc}
              chatType={route.chatType}
              currentUsers={route.currentUsers}
              groupUsers={route.groupUsers}
              group={route.group}
              hasInternet={route.hasInternet}
              isLoggedIn={route.isLoggedIn}
              loggedInUser={route.loggedInUser}
              refreshGroups={route.refreshGroups}
              renderConnectionStatus={route.renderConnectionStatus}
              shouldReloadData={route.shouldReloadData}
              theme={route.theme}
              timeFormat={route.timeFormat}

              addContactToLoggedInUser={route.addContactToLoggedInUser}
              changeBgImage={route.changeBgImage}
              changeTheme={route.changeTheme}
              getMessageWallpaper={route.getMessageWallpaper}
              isSafeToBack={route.isSafeToBack}
              logout={route.logout}
              setLoggedInUser={route.setLoggedInUser}
              setIsViewVisible={route.setIsViewVisible}
              setIsSafeToBack={route.setIsSafeToBack}
              subscribeToLoggedInUser={route.subscribeToLoggedInUser}
              user={route.user}
              users={route.users}
              addOrRemove={route.addOrRemove}
              setGroupUsers={route.setGroupUsers}
            />
        );
      case 'ChangeMobileNumber':
        return (
          <ChangeMobileNumber navigator={navigator}
            hasInternet={route.hasInternet}
            isLoggedIn={route.isLoggedIn}
            loggedInUser={route.loggedInUser}
            renderConnectionStatus={route.renderConnectionStatus}
            setMobileNumber={route.setMobileNumber}
            theme={route.theme}
          />
        );
      case 'ChangePassword':
        return (
          <ChangePassword navigator={navigator}
            theme={route.theme}
            isLoggedIn={route.isLoggedIn}
            hasInternet={route.hasInternet}
            renderConnectionStatus={route.renderConnectionStatus}
          />
        );
      case 'ChangeWallpaper':
        return (
          <ChangeWallpaper navigator={navigator}
            getMessageWallpaper={route.getMessageWallpaper}
            theme={route.theme}
            renderConnectionStatus={route.renderConnectionStatus}
          />
        );
      case 'ContactsInvite':
        return (
          <ContactsInvite navigator={navigator}
            hasInternet={route.hasInternet}
            loggedInUser={route.loggedInUser}
            renderConnectionStatus={route.renderConnectionStatus}
            renderBackgroundImage={route.renderBackgroundImage}
            theme={route.theme}
            users={route.users}
          />
        );
      case 'DataUsage':
        return (
          <DataUsage navigator={navigator}
            hasInternet={route.hasInternet}
            isAutoSaveMedia={route.isAutoSaveMedia}
            renderConnectionStatus={route.renderConnectionStatus}
            setIsAutoSaveMedia={route.setIsAutoSaveMedia}
            theme={route.theme}
          />
        );
      case 'GroupDetails':
        return (
          <GroupDetails navigator={navigator}
            backgroundImageSrc={route.backgroundImageSrc}
            data={route.data}
            loggedInUser={route.loggedInUser}
            hasInternet={route.hasInternet}
            getGroupUsers={route.getGroupUsers}
            group={route.group}
            groupInfo={route.groupInfo}
            groupUsers={route.groupUsers}
            refreshGroups={route.refreshGroups}
            renderConnectionStatus={route.renderConnectionStatus}
            shouldReloadData={route.shouldReloadData}
            timeFormat={route.timeFormat}
            theme={route.theme}

            addContactToLoggedInUser={route.addContactToLoggedInUser}
            changeBgImage={route.changeBgImage}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            isSafeToBack={route.isSafeToBack}
            logout={route.logout}
            setLoggedInUser={route.setLoggedInUser}
            setIsViewVisible={route.setIsViewVisible}
            setIsSafeToBack={route.setIsSafeToBack}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            user={route.user}
            users={route.users}
            isFromAddUserToGroup={route.isFromAddUserToGroup}
            newGroup={route.newGroup}
          />
        );
      case 'InputCountryCode':
        return (
          <InputCountryCode navigator={navigator}
           selectedCountry={route.selectedCountry}
           setSelectedCountry={route.setSelectedCountry}
           theme={route.theme}
          />
        );
      case 'KliklabsPortal':
        return (
          <KliklabsPortal navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isLoggedIn={route.isLoggedIn}
            isDebug={route.isDebug}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
          />
        );
      case 'KliklabsConfirmCode':
        return (
          <KliklabsConfirmCode navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            mobile={route.mobile}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            token={route.token}
            urlstring={route.urlstring}
            username={route.username}
            apiLoginOptions={route.apiLoginOptions}
          />
        );
      case 'KliklabsCongratulations':
        return (
          <KliklabsCongratulations navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            apiMobile={route.apiMobile}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            image={route.image}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            password={route.password}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            username={route.username}
          />
        );
      case 'KliklabsRegister':
        return (
          <KliklabsRegister navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            mobile={route.mobile}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            token={route.token}
            username={route.username}
          />
        );
      case 'KliklabsSignUp':
        return (
          <KliklabsSignUp navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            apiLoginOptions={route.apiLoginOptions}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            signupKliklabUser={route.signupKliklabUser}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            token={route.token}
          />
        );
      case 'Login':
        return (
          <Login navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsLoggedIn={route.isLoggedIn}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            checkLoginToken={this.loginWithToken}
          />
        );
      case 'Map':
        return (
          <Map navigator={navigator}
           color={route.color}
           from={route.from}
           sendLocation={route.sendLocation}
           to={route.to}
          />
        );
      case 'MessageInfo':
        return (
          <MessageInfo navigator={navigator}
            loggedInUser={route.loggedInUser}
            message={route.message}
            color={route.color}
            otherUsers={route.otherUsers}
            timeFormat={route.timeFormat}
          />
        );
      case 'MessagesGiftedChat':
        return (
          <MessagesGiftedChat navigator={navigator}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            group={route.group}
            hasInternet={route.hasInternet}
            isAutoSaveMedia={route.isAutoSaveMedia}
            isSafeToBack={route.isSafeToBack}
            isLoggedIn={route.isLoggedIn}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setIsSafeToBack={route.setIsSafeToBack}
            setIsLoggedIn={route.setIsLoggedIn}
            setShouldResubscribe={route.setShouldResubscribe}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            user={route.user}
            users={route.users}
            newGroup={route.newGroup}
            messages={route.messages}
            getMessageWallpaper={route.getMessageWallpaper}
          />
        );
      case 'PhotoInfo':
        return <PhotoInfo navigator={navigator} color={route.color} imageInfo={route.imageInfo}/>;
      case 'Profile':
        return (
          <Profile navigator={navigator}
           changeTheme={route.changeTheme}
           color={route.color}
           hasInternet={route.hasInternet}
           isAutoLocate={route.isAutoLocate}
           locationError={route.locationError}
           loggedInUser={route.loggedInUser}
           renderConnectionStatus={route.renderConnectionStatus}
           setIsAutoLocate={route.setIsAutoLocate}
           shouldReloadData={route.shouldReloadData}
           theme={route.theme}
           user={route.user}
          />
        );
      case 'FriendList':
        return (
          <FriendList navigator={navigator}
            action={route.action}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            changeBgImage={route.changeBgImage}
            changeTheme={route.changeTheme}
            chatType={route.chatType}
            getMessageWallpaper={route.getMessageWallpaper}
            hasInternet={route.hasInternet}
            isAutoSaveMedia={route.isAutoSaveMedia}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            sendContact={route.sendContact}
            sendNewMessage={route.sendNewMessage}
            setAudio={route.setAudio}
            setIsViewVisible={route.setIsViewVisible}
            setIsSafeToBack={route.setIsSafeToBack}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
          />
        );
      case 'Settings':
        return (
          <Settings navigator={navigator}
           addContactToLoggedInUser={route.addContactToLoggedInUser}
           backgroundImageSrc={route.backgroundImageSrc}
           debugText={route.debugText}
           changeTheme={route.changeTheme}
           getMessageWallpaper={route.getMessageWallpaper}
           goToSignUp={route.goToSignUp}
           hasInternet={route.hasInternet}
           isAutoSaveMedia={route.isAutoSaveMedia}
           isDebug={route.isDebug}
           isSafeToBack={route.isSafeToBack}
           isLoggedIn={this.props.isLoggedIn}
           loggedInUser={route.loggedInUser}
           logout={route.logout}
           renderConnectionStatus={route.renderConnectionStatus}
           setIsAutoSaveMedia={route.setIsAutoSaveMedia}
           setIsSafeToBack={route.setIsSafeToBack}
           setLoggedInUser={route.setLoggedInUser}
           setSignUpCallback={route.setSignUpCallback}
           shouldReloadData={route.shouldReloadData}
           subscribeToLoggedInUser={route.subscribeToLoggedInUser}
           theme={route.theme}
           timeFormat={route.timeFormat}
           checkLoginToken={route.checkLoginToken}
           setIsLoggingOut={route.setIsLoggingOut}
          />
        );
      case 'ShareMedia':
        return (
          <ShareMedia navigator={navigator}
           color={route.color}
           loggedInUser={route.loggedInUser}
           image={route.image}
          />
        );
      case 'TextView':
        return (
          <TextView
            navigator={navigator}
            theme={route.theme}
            linkStyle={route.linkStyle}
            currentMessage={route.currentMessage}
            textStyle={route.textStyle}
            onEmailPress={route.onEmailPress}
            onPhonePress={route.onPhonePress}
            onUrlPress={route.onUrlPress}
          />
        );
      case 'Tabs':
        return (
          <Tabs navigator={navigator}
            addContactToLoggedInUser={route.addContactToLoggedInUser}
            audio={route.audio}
            backgroundImageSrc={route.backgroundImageSrc}
            debugText={route.debugText}
            changeTheme={route.changeTheme}
            getMessageWallpaper={route.getMessageWallpaper}
            goToSignUp={route.goToSignUp}
            hasInternet={route.hasInternet}
            hasNewFriendRequest={route.hasNewFriendRequest}
            isDebug={route.isDebug}
            isLoggedIn={route.isLoggedIn}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            logout={route.logout}
            renderConnectionStatus={route.renderConnectionStatus}
            setAudio={route.setAudio}
            setForcedGoToThreadCallback={this.setForcedGoToThreadCallback}
            setIsSafeToBack={route.setIsSafeToBack}
            setLoggedInUser={route.setLoggedInUser}
            setSignUpCallback={route.setSignUpCallback}
            shouldReloadData={route.shouldReloadData}
            subscribeToLoggedInUser={route.subscribeToLoggedInUser}
            theme={route.theme}
            timeFormat={route.timeFormat}
            checkLoginToken={route.checkLoginToken}
            setIsLoggingOut={this.setIsLoggingOut}
            isLoggingOut={this.isLoggingOut}
          />
        );
      case 'TimelinePoster':
        return (
          <TimelinePoster navigator={navigator}
            commentActive={route.commentActive}
            hasInternet={route.hasInternet}
            isLoggedIn={route.isLoggedIn}
            isOnComment={route.isOnComment}
            isSafeToBack={route.isSafeToBack}
            loggedInUser={route.loggedInUser}
            renderConnectionStatus={route.renderConnectionStatus}
            setIsSafeToBack={route.setIsSafeToBack}
            theme={route.theme}
          />
        );
      case 'TimelinePostComments':
        return (
          <TimelinePostComments navigator={navigator}
            hasInternet={route.hasInternet}
            isSafeToBack={route.isSafeToBack}
            isOnComment={route.isOnComment}
            post={route.post}
            color={route.color}
            theme={route.theme}
            renderConnectionStatus={route.renderConnectionStatus}
            setIsSafeToBack={route.setIsSafeToBack}
            setIsShowReactions={route.setIsShowReactions}
          />
        );
      case 'ViewPhoto':
        return (
          <ViewPhoto navigator={navigator}
            data={route.data}
            color={route.color}
            imageGallery={route.imageGallery}
            loggedInUser={route.loggedInUser}
            setIsSafeToBack={route.setIsSafeToBack}
            users={route.users}
          />
        );
      case 'VideoPlayer':
        return <VideoPlayer navigator={navigator} color={route.color} data={route.data} hasInternet={route.hasInternet} />;
    }
  }

  backgroundImageSrc() {
    return this.state.backgroundImageSrc;
  }

  hasInternet() {
    return this.state.hasInternet;
  }

  isLoggedIn() {
    return this.state.isLoggedIn
  }

  isSafeToBack() {
    return isSafeToBack;
  }

  logout() {

    Chat.stopObserving();
    Message.stopObserving();
    Post.stopObserving();
    Users.stopObserving();

    FCM.removeAllDeliveredNotifications();
    console.log("Unsubscribing from: " + "/topic/"+this.loggedInUser()._id+"_"+Platform.OS);
    FCM.unsubscribeFromTopic("/topics/"+this.loggedInUser()._id+"_"+Platform.OS);
    //this.removeConnectivityListener();

    // if (this._isMounted) {
    //   this.setState({
    //     hasPreviousSession: false,
    //     isCheckingPreviousSession: false,
    //     loggedInUser: null
    //   });
    // }
  }

  hasNewFriendRequest() {
    return this.state.hasNewFriendRequest;
  }

  loggedInUser() {
    return this.state.loggedInUser;
  }

  renderConnectionStatus() {
    let color = GLOBAL.COLOR_THEME[this.state.theme];

    if (this.state.shouldRenderConnection) {
      if (this.state.hasInternet) {
        if (this.state.isLoggedIn) {
          return null;
        } else {
          if (!this.state.isDDPConnected) {
            if (this.state.isDDPConnecting) {
              return <ConnectionStatus text={'Connecting...'} color={color} />;
            }
            return <ConnectionStatus text={'Failed to connect to server'} color={color} isError />;
          }
          return <ConnectionStatus text={'Connecting...'} color={color} />;
        }
      }
      return <ConnectionStatus text={'No internet connection'} color={color} isError />;
    } else {
      return null;
    }
  }

  setLoggedInUser(loggedInUser) {
    // console.warn("Called setLoggedInUser");
    loggedInUser.profile.image = getMediaURL(loggedInUser.profile.image);
    this.setState({loggedInUser: loggedInUser});
  }

  setIsSafeToBack(isSafe) {
    isSafeToBack = isSafe;
  }

  setSignUpCallback(callback = null) {
    if (callback) {
      goToSignUp = callback;
    } else {
      goToSignUp = this.goToSignUp;
    }
  }

  setIsLoggingOut(isLoggingOut) {
    console.log("Called isLoggingOut");
    this.setState({
      isLoggingOut: true
    });

  }

  theme() {
    return this.state.theme;
  }

  timeFormat() {
    return this.state.timeFormat;
  }

  ddpClose() {
    console.log("DDPClose called");
    if(!this.state.isLoggingOut) {
      console.log("isLoggingOut is false");
      ddpWasClosed = true;
      ddp.connected = false;
      // if(this._isMounted && this.hasInternet()) {
      //   RNRestart.Restart();
      // }

    } else {
      console.log("isLogging out is true");
    }

  }

  setInitialState() {
    console.log('setInitialState');
    this.setState({
      hasInternet: true, // states for connecting to ddp
      isDDPConnecting: true,
      isDDPConnected: false,

      isCheckingLogin: false, // states for login
      isLoggingIn: false,
      isLoggedIn: false,
      isLoggedInDDP: false,
      loggedInUser: null,
      loggedInUserIsVerified: false,
      shouldLogin: false,
    });
  }

  setIsAutoSaveMedia(isAutoSaveMedia) {
    if (this._isMounted) {
      this.setState({isAutoSaveMedia: isAutoSaveMedia});
    }
  }

  isAutoSaveMedia() {
    return this.state.isAutoSaveMedia;
  }

  forceRestart() {
    ddp.setForceCloseCallback(null);

    this.handleConnectivityChange(false);
    setTimeout(()=>{
      ddp.isConnected = false;
      ddp.isLoggedIn = false;
      ddp.connected = false;
      ddp.setCloseCallBack(null);
        this.handleConnectivityChange(true);
    }, 1500);

    //RNRestart.Restart();
  }

  audio() {
    return this.state.audio;
  }

  setAudio(audio) {
    if (this._isMounted) {
      console.log('[setAudio]')
      this.setState({audio: audio});
    }
  }

}

BackAndroid.addEventListener('hardwareBackPress', () => {
  var currentRoutes = navigator.getCurrentRoutes();
  var currentRouteIndex = currentRoutes.length-1;
  var currentRoute = currentRoutes[currentRouteIndex];
  var routeToLoad = currentRoutes[currentRouteIndex-1];
  let kliklabsRoutes = ['KliklabsSignUp', 'KliklabsConfirmCode', 'KliklabsRegister', 'KliklabsCongratulations'];
  let typingRoutes = ['MessagesGiftedChat', 'TimelinePoster', 'TimelinePostComments'];

  SoundEffects.playSound('tick');

  if (routeToLoad == null
      || currentRoute.name ==='Messenger'
      || currentRoute.name ==='KliklabsPortal') {
      BackAndroid.exitApp(0);
      return false;
  } else if (_.contains(typingRoutes, currentRoute.name)) {
    if (isSafeToBack) {
      //requestAnimationFrame(() => {
        navigator.pop();
      //});
    }
    return true;
  } else if (_.contains(kliklabsRoutes, currentRoute.name)) {
    goToSignUp();
    return true;
  } else if(currentRoute.name == 'KliklabsCongratulations') {
      return true;
    } else if (navigator && currentRoutes.length > 1) {
      requestAnimationFrame(() => {
      navigator.pop();
    });
    return true;
  }

  return false;
});

module.exports = Klikchat;
