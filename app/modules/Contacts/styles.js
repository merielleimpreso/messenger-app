import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  actionButtonIcon: {
    fontSize: 25,
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  input: {
    height: 35,
    padding: 5,
    textAlign: 'center',
  },
  searchBoxContainer: {
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 3,
  },
});
