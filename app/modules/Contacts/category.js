module.exports = {
  profile: {
    name: 'Profile',
    order: 0
  },
  friendRequest: {
    name: 'Friend Request',
    order: 1
  },
  group : {
    name: 'Group',
    order: 2
  },
  friend: {
    name: 'Friend',
    order: 3
  },
  block: {
   name: 'Blocked User',
   order: 4
 }
}
