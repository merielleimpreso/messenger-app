'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  Image,
  InteractionManager,
  LayoutAnimation,
  ListView,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import _ from 'underscore';
import {logTime, toNameCase, getMediaURL} from './Helpers';
import ActivityView from 'react-native-activity-view';
import ButtonIcon from './common/ButtonIcon';
import ContactsManager from 'react-native-contacts';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './common/ListRow';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import ScreenNoItemFound from './ScreenNoItemFound';
import SearchBar from './SearchBar';
import Send from './config/db/Send';
import Users from './config/db/Users';
import UserGroups from './config/db/UserGroups';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const CATEGORY_PROFILE = {name: 'Profile', order: 0};
const CATEGORY_FRIENDREQUEST = {name: 'Friend Request', order: 1};
const CATEGORY_GROUP = {name: 'Group', order: 2};
const CATEGORY_FRIEND = {name: 'Friend', order: 3};
const tempprofpic = '/assets/avatar/tempprofpic.jpg';
const tempgrouppic = '/assets/avatar/tempgrouppic.jpg';

const animation = {
  linear: {
    duration: 300,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity
    },
    update: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity
    }
  },
}

class Contacts extends Component {

  // Initial state and bind functions
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      contactsCategoryMap: {},
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      }),
      isLoading: true,
      isLoadingSearch: false,
      isOpenDetails: false,
      detailsDisplayed: {},
      shownCategories: [CATEGORY_PROFILE.name, CATEGORY_FRIENDREQUEST.name, CATEGORY_GROUP.name, CATEGORY_FRIEND.name],
      theme: this.props.theme(),
      loading: true,
      isLoaded: false,
      isShowSearchOnContacts: this.props.isShowSearchOnContacts()
    }

    this.goToContactsAdd = this.goToContactsAdd.bind(this);
    this.getAllContacts = this.getAllContacts.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.renderDetails = this.renderDetails.bind(this);
    this.renderDetailsGroup = this.renderDetailsGroup.bind(this);
    this.renderDetailsFriend = this.renderDetailsFriend.bind(this);
    this.goToContactsGroup = this.goToContactsGroup.bind(this);
    this.goToMessagesChat = this.goToMessagesChat.bind(this);
    this.goToGroupChat = this.goToGroupChat.bind(this);
    this.hideDetails = this.hideDetails.bind(this);
    this.getFriendRequestIds = this.getFriendRequestIds.bind(this);
  }

  // Put a flag to check if component is mounted
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getAllContacts();
    });
  }

  componentWillUpdate(){

  }

  // Reset view if theme updates
  componentDidUpdate(prevProps, prevState) {
    if (this.props.theme() != this.state.theme) {
      this.reRenderList();
    }

    if (prevState.shownCategories != this.state.shownCategories) {
      this.reRenderList();
    }
  }

  // Put a flag to check if component is unmounted
  componentWillUnmount() {
    this._isMounted = false;
  }

  reRenderList() {
    this.state.dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    });
    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRowsAndSections(this.state.contactsCategoryMap),
        theme: this.props.theme()
      });
    }
  }

  // Get contacts from phone and database current
  getAllContacts() {
    let loggedInUser = this.props.loggedInUser();
    let user = this.getContactsByType([loggedInUser._id], 'Profile');
    let friends = this.getContactsByType(loggedInUser.contacts, 'Friend');
    let groups = this.getContactsByType(_.pluck(this.props.groups(), '_id'), 'Group');
    let friendRequest = this.getContactsByType(this.getFriendRequestIds(), 'Friend Request');
    let contacts = _.union(user,friends, groups, friendRequest);

    contacts = _.sortBy(contacts, contact => contact.name.toLowerCase()).reverse();
    contacts = _.sortBy(contacts, 'category').reverse();

    if (this._isMounted) {
      this.setState({
        contacts: contacts,
        contactsCategoryMap: this.getContactsCategoryMap(contacts),
        dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getContactsCategoryMap(contacts)),
        isLoading: false
      });
    }
  }

  getFriendRequestIds() {
    let loggedInUser = this.props.loggedInUser();
    let friendRequestList = _.sortBy(loggedInUser.friendRequestReceived, 'date');
    let friendRequestIds = [];
    for (let i = 0, friendRequestListLen = friendRequestList.length; i < friendRequestListLen; i++) {
      if (!(_.contains(loggedInUser.contacts, friendRequestList[i].userId))) {
        friendRequestIds.push(friendRequestList[i].userId);
      }
    }
    return friendRequestIds;
  }

  // Get contacts: Friend or Group
  getContactsByType(ids, category) {
    if (!ids.length) return;

    let items = (category === 'Group') ? this.props.groups() : this.props.users(),
        contacts = [],
        name = '',
        statusMessage = '',
        image = '',
        friendRequestNames = '';

    if (category === 'Friend Request') {
      for (let i = 0, idsLength = ids.length; i < idsLength; i++) {
        let c = _.findWhere(items, {_id:ids[i]});
        if (c) {
          friendRequestNames += `${toNameCase(c.profile.firstName)} ${toNameCase(c.profile.lastName)}, `;
          if (i == 0) {
            image = (c.profile.photoUrl) ? getMediaURL(c.profile.photoUrl) : getMediaURL(tempprofpic);
          }
        } else {
          Users.subscribeById(ids[i]);
        }
      }
      name = friendRequestNames.replace(/,\s*$/, "");
      let contact = {
        _id: null,
        name: name,
        image: image,
        statusMessage: statusMessage,
        category: category
      };
      contacts.push(contact);
    } else {
      for ( let i = 0, idsLength = ids.length; i < idsLength; i++) {
        let c = _.findWhere(items, {_id:ids[i]});

        if (!c) continue;

        if (category !== 'Group') {
          name = !c.profile.hasOwnProperty('displayName')
            ? `${toNameCase(c.profile.firstName)} ${toNameCase(c.profile.lastName)}`
            : c.profile.displayName;

          statusMessage = !c.profile.hasOwnProperty('statusMessage')
            ? statusMessage
            : !c.profile.statusMessage
              ? statusMessage
              : c.profile.statusMessage;

          image = !c.profile.photoUrl.startsWith('data:image')
            ? getMediaURL(c.profile.photoUrl)
            : getMediaURL(tempprofpic);
        } else {
          name = toNameCase(c.Name);
          image = !c.hasOwnProperty('GroupImage')
            ? getMediaURL(tempgrouppic)
            : getMediaURL(c.GroupImage);
        }

        contacts.push({
          _id: c._id,
          name: name,
          image: image,
          statusMessage: statusMessage,
          category: category
        })

      }
    }

    contacts = _.sortBy(contacts, 'name').reverse();
    return contacts;
  }

  // Map contacts according to category
  getContactsCategoryMap(contacts) {
    var contactsCategoryMap = {};
    contacts.forEach(function(contactItem) {
      if (!contactsCategoryMap[contactItem.category]) {
        contactsCategoryMap[contactItem.category] = [];
      }
      contactsCategoryMap[contactItem.category].push(contactItem);
    });
    return contactsCategoryMap;
  }

  // Render display
  render() {
    console.log('searchText : ', this.props.getSearchText())
    console.log('this.state.isShowSearchOnContacts(): ', this.props.isShowSearchOnContacts())
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Contact', tintColor: color.BUTTON_TEXT}}
         rightButton={<ButtonIcon icon='addcontact' color={color} onPress={this.goToContactsAdd} size={25}/>} />
        {this.props.renderConnectionStatus()}
        {this.renderSearchBar()}
        {this.renderContent()}
        {this.renderDetails()}
        <View style={{height: 50}} />
      </View>
    )
  }

  // Render search bar
  renderSearchBar() {
    if (!this.props.isShowSearchOnContacts()) return;
    return (
      <SearchBar
       theme={this.props.theme}
       onSearchChange={this.onSearchChange}
       isLoading={this.state.isLoadingSearch}
       placeholder='Search'
       onFocus={() =>
        this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
     />
    );
  }

  // Render the main content
  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (!this.state.isLoading && this.state.dataSource.getRowCount() === 0) {
      return <ScreenNoItemFound isLoading={false} text={'No contact'} color={color}/>;
    } else {
      return (
        <ListView ref="listview"
         automaticallyAdjustContentInsets={false}
         dataSource={this.state.dataSource}
         enableEmptySections={true}
         initialListSize={this.state.dataSource.getRowCount()}
         keyboardShouldPersistTaps={true}
         renderRow={this.renderRow}
         renderSectionHeader={this.renderSectionHeader}
         showsVerticalScrollIndicator={false}
         scrollEventThrottle={100}
         onScroll={() => this.props.setShowSearchOnContacts()}
        />

      );
    }
  }

  /**
   * Render data in a row
   * @date: Nov.28.2016
   */
  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    if (_.contains(this.state.shownCategories, data.category)) {
      return (
        <ListRow onPress={() => this.onPressRow(data)}
         labelText={data.name}
         detailText={data.statusMessage}
         image={data.image}
         color={color}
         addSeparator={false}
         />
      );
    } else {
      return <View />;
    }
  }

  // Render section headers
  renderSectionHeader(sectionData, category) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let items = [];
    if (category == CATEGORY_FRIEND.name) {
      items = this.props.loggedInUser().contacts;
    } else if (category == CATEGORY_GROUP.name) {
      items =  this.props.groups();
    } else if (category == CATEGORY_FRIENDREQUEST.name) {
      items = this.getFriendRequestIds();
    }

    var header = category;
    if (category != CATEGORY_PROFILE.name) {
      header += (items.length > 1) ? 's ' : ' ';
      header += '(' + items.length + ')';
    }

    let icon = _.contains(this.state.shownCategories, category) ? 'up' : 'dropdown';
    return (
      <View style={[styles.contactsSectionHeader, {borderTopColor:color.HIGHLIGHT, backgroundColor:color.CHAT_BG, marginTop:0}]}>
        <Text style={{flex:1, fontSize:13, color:color.TEXT_LIGHT, fontWeight:'500', padding:5, marginRight:10, alignSelf:'center'}}>{header}</Text>
        <TouchableHighlight onPress={() => this.onPressSectionHeaderIcon(category)} underlayColor='transparent' style={{alignSelf:'center'}}>
          <KlikFonts name={icon} color={color.TEXT_LIGHT} size={20}/>
        </TouchableHighlight>
      </View>
    );
  }

  // When a search is made
  onSearchChange(searchText) {
    this.setState({isLoadingSearch: true});
    var contacts = _.filter(this.state.contacts, function(contact){
      return (contact.name).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });

    if (this._isMounted) {
      this.props.setSearchText(searchText);
      this.setState({
        dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getContactsCategoryMap(contacts)),
        isLoadingSearch: false
      });
    }
  }

  onPressRow(data) {
    if (data.category === "Friend Request") {
      return this.goToContactsAdd();
    }
    return this.displayDetails(data);
  }

  onPressSectionHeaderIcon(category) {
    var shownCategories = this.state.shownCategories;
    if (_.contains(shownCategories, category)) {
      shownCategories = _.without(shownCategories, category);
    } else {
      shownCategories.push(category);
    }
    if (this._isMounted) {
      LayoutAnimation.configureNext(animation.linear);
      this.setState({
        shownCategories: shownCategories
      });
    }
  }

  displayDetails(data) {
    if (this._isMounted) {
      this.setState({
        isOpenDetails: true,
        detailsDisplayed: data
      });
    }
  }

  hideDetails(){
    if (this._isMounted) {
      this.setState({
        isOpenDetails: false,
        detailsDisplayed: {}
      });
    }
  }

  renderDetails() {
    var modalContent = <View />;
    if (this.state.detailsDisplayed.category === 'Group') {
      modalContent = this.renderDetailsGroup();
    } else if (this.state.detailsDisplayed.category === 'Friend') {
      modalContent = this.renderDetailsFriend();
    } else if (this.state.detailsDisplayed.category === 'Profile') {
      modalContent = this.renderDetailsProfile();
    }

    return (
      <Modal open={this.state.isOpenDetails}
       overlayBackground={'rgba(0, 0, 0, 0.4)'}
       closeOnTouchOutside={true}
       modalDidClose={() => this.setState({isOpenDetails:false, detailsDisplayed:{}})}
       containerStyle={{justifyContent: 'center'}}
       modalStyle={{
         borderRadius: 2,
         margin: 10,
         padding: 0
       }}>
        {modalContent}
      </Modal>
    );
  }

  renderDetailsProfile() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let name = this.state.detailsDisplayed.name;
    let image = this.state.detailsDisplayed.image;

    return (
      <View>
        <Image source={require('./../images/backgroundImage/chatbg1.jpg')} style={{height: 150, width:null}} resizeMode={'cover'}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded)
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:150, width:null}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
        <TouchableHighlight onPress={this.hideDetails}
         underlayColor='transparent'
         style={{alignSelf:'flex-end', marginTop:5, marginRight:5}}>
          <KlikFonts name='close' color={color.TEXT_LIGHT} size={25}/>
        </TouchableHighlight>
        <Image source={{uri: image}} style={{height: 80, width: 80, borderRadius: 40, alignSelf: 'center', marginTop: -70 }}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded)
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:80, width:80}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
        <Text style={{alignSelf:'center', fontWeight:'700', fontSize:14, margin:5}}>{name}</Text>
        <View style={{flexDirection: 'row', borderTopWidth: 1, borderTopColor: color.HIGHLIGHT, marginTop:30}}>
          <TouchableHighlight underlayColor={color.HIGHLIGHT}
            onPress={() => this.goToProfile()}
            style={{flex: 1, padding: 5}}>
            <View style={{alignItems: 'center'}}>
              <KlikFonts name="recent" style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT}}>Edit Profile</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderDetailsGroup() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let profileData = this.state.detailsDisplayed;
    let groupId = profileData._id;
    let group = _.findWhere(this.props.groups(), {_id: groupId});
    let groupName = this.state.detailsDisplayed.name;
    let groupImage = profileData.image;
    let userIds = _.pluck(UserGroups.getItemByGroupId(groupId), 'UserFID'),
        users = _.map(userIds, (userId) => { return Users.getItemById(userId); }),
        usergroups = UserGroups.getItemByGroupId(groupId);

    return (
      <View>
        <Image source={{uri:groupImage}} style={{height: 150, width:null}} resizeMode={'cover'}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded)
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:150, width:null}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
        <TouchableHighlight onPress={this.hideDetails}
         underlayColor='transparent'
         style={{alignSelf:'flex-end', marginTop:5, marginRight:5}}>
          <KlikFonts name='close' color={color.TEXT_LIGHT} size={25}/>
        </TouchableHighlight>
        <Text style={{alignSelf:'center', fontWeight:'700', fontSize:14}}>{groupName}</Text>
        <View style={{flexDirection:'row', alignSelf:'center', margin:10}}>
          {usergroups.map((member, i) => {
            if (i < 4) {
              let user = _.findWhere(users, {_id: member.UserFID});
              let photoUrl = getMediaURL(user.profile.photoUrl);
              return <Image key={member.UserFID} source={{uri:photoUrl}} style={{width:26, height:26, borderRadius:13,margin:1}}
                onLoadStart={(e) => this.setState({loading: true})}
                onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
                onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
                {(this.state.loading && !this.state.isLoaded)
                  ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:26, width:26}}>
                      <ActivityIndicator size="small" color={color.BUTTON} />
                    </View>
                  : null
                }
              </Image>;
            }
          })}
          <TouchableHighlight onPress={() => this.goToContactsGroup()} underlayColor='transparent'>
            <View style={{backgroundColor:color.BUTTON, margin:1, marginLeft:2, borderRadius:5, flexDirection:'row', alignItems:'center'}}>
              <Text style={{color:color.BUTTON_TEXT, fontSize:12, fontWeight:'600', margin:5, marginLeft:5, marginRight:0}}>{usergroups.length}</Text>
              <KlikFonts name='next' color={color.BUTTON_TEXT} size={15}/>
            </View>
          </TouchableHighlight>
        </View>

        <View style={{flexDirection: 'row', borderTopWidth: 1, borderTopColor: color.HIGHLIGHT, marginTop:30}}>
          <TouchableHighlight underlayColor={color.HIGHLIGHT}
            onPress={() => this.goToGroupChat(group, users)}
            style={{flex: 1, padding: 5}}>
            <View style={{alignItems: 'center'}}>
              <KlikFonts name="recent" style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT}}>Group Chat</Text>
            </View>
          </TouchableHighlight>
        </View>

      </View>
    );
  }

  renderDetailsFriend() {
    let userId = this.state.detailsDisplayed._id;
    let user = _.findWhere(this.props.users(), {_id : userId});
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let name = this.state.detailsDisplayed.name;
    let image = this.state.detailsDisplayed.image;

    return (
      <View>
        <Image source={require('./../images/backgroundImage/chatbg1.jpg')} style={{height: 150, width:null}} resizeMode={'cover'}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded)
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:150, width:null}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
        <TouchableHighlight onPress={this.hideDetails}
         underlayColor='transparent'
         style={{alignSelf:'flex-end', marginTop:5, marginRight:5}}>
          <KlikFonts name='close' color={color.TEXT_LIGHT} size={25}/>
        </TouchableHighlight>
        <Image source={{uri: image}} style={{height: 80, width: 80, borderRadius: 40, alignSelf: 'center', marginTop: -70 }}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded)
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:80, width:80}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
        <Text style={{alignSelf:'center', fontWeight:'700', fontSize:14, margin:5}}>{name}</Text>
        <View style={{flexDirection: 'row', borderTopWidth: 1, borderTopColor: color.HIGHLIGHT, marginTop:30}}>
          <TouchableHighlight underlayColor={color.HIGHLIGHT}
            onPress={() => this.goToMessagesChat(user)}
            style={{flex: 1, padding: 5}}>
            <View style={{alignItems: 'center'}}>
              <KlikFonts name="recent" style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT}}>Chat</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  goToContactsGroup() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'contactsgroup',
        renderConnectionStatus: this.props.renderConnectionStatus,
        hasInternet: this.props.hasInternet,
        theme: this.props.theme,
        loggedInUser: this.props.loggedInUser,
        users: this.props.users,
        usergroups: this.props.usergroups,
        groupId: this.state.detailsDisplayed._id
      });
      this.hideDetails();
    });
  }

  goToContactsAdd() {
    this.props.navigator.push({
      id: 'contactsadd',
      backgroundImageSrc:this.props.backgroundImageSrc,
      getMessageWallpaper: this.props.getMessageWallpaper,
      hasInternet: this.props.hasInternet,
      loggedInUser: this.props.loggedInUser,
      renderConnectionStatus: this.props.renderConnectionStatus,
      setLoggedInUser: this.props.setLoggedInUser,
      shouldReloadData: this.props.shouldReloadData,
      theme: this.props.theme,
      timeFormat:this.props.timeFormat,
      users: this.props.users
    });
  }

  goToMessagesChat(user) {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'messageschat',
        backgroundImageSrc:this.props.backgroundImageSrc,
        bgImage: this.props.bgImage,
        changeBgImage: this.props.changeBgImage,
        hasInternet: this.props.hasInternet,
        loggedInUser: this.props.loggedInUser,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setImage: this.props.setImage,
        shouldReloadData: this.props.shouldReloadData,
        theme: this.props.theme,
        timeFormat:this.props.timeFormat,
        user: user
      });
      this.hideDetails();
    });
  }

  goToGroupChat(group, users) {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'messageschat',
        backgroundImageSrc:this.props.backgroundImageSrc,
        bgImage:this.props.bgImage,
        changeBgImage:this.props.changeBgImage,
        group: group,
        hasInternet:this.props.hasInternet,
        loggedInUser:this.props.loggedInUser,
        renderConnectionStatus:this.props.renderConnectionStatus,
        setImage:this.props.setImage,
        shouldReloadData:this.props.shouldReloadData,
        theme:this.props.theme,
        timeFormat:this.props.timeFormat,
        users:users
      });
      this.hideDetails();
    });
  }

  goToProfile() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'profile',
        addContactToLoggedInUser:this.props.addContactToLoggedInUser,
        bgImage:this.props.bgImage,
        changeBgImage:this.props.changeBgImage,
        changeTheme:this.props.changeTheme,
        hasInternet:this.props.hasInternet,
        loggedInUser:this.props.loggedInUser,
        logout:this.props.logout,
        renderConnectionStatus:this.props.renderConnectionStatus,
        setImage:this.props.setImage,
        subscribeToLoggedInUser:this.props.subscribeToLoggedInUser,
        shouldReloadData:this.props.shouldReloadData,
        theme:this.props.theme
      });
      this.hideDetails();
    });
  }

}

// Export module.
module.exports = Contacts;
