'use strict';

import _ from 'underscore';
import React, {
  Component,
} from 'react';
import {
  Dimensions,
  InteractionManager,
  LayoutAnimation,
  ListView
} from 'react-native';
import {logTime, toLowerCase} from '../Helpers';
import Animation from '../../styles/animation';
import {CACHE_KEYS} from '../config/db/StoreCache';
import Category from './category'
import ContactsRender from './ContactsRender';
import GLOBAL from '../config/Globals.js';
import StoreCache from '../config/db/StoreCache';
import SoundEffects from '../actions/SoundEffects';
import Users from '../config/db/Users';

class Contacts extends Component {

  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      }),
      displayContacts: [],      
      isAllLoaded: false,
      isCacheLoaded: false,
      isShowContactProfile: false,
      loggedInUser: this.props.loggedInUser(),
      profileData: null,
      shownCategories: [Category.profile.name, Category.friendRequest.name, Category.group.name, Category.friend.name, Category.block.name],
      searchContact: ''
    }
    this.getGroupUsers = this.getGroupUsers.bind(this);
    this.goToAddFriends = this.goToAddFriends.bind(this);
    this.goToChat = this.goToChat.bind(this);
    this.goToGroupDetails = this.goToGroupDetails.bind(this);
    this.goToProfile = this.goToProfile.bind(this);
    this.observeContacts = this.observeContacts.bind(this);
    this.onPressSectionHeaderIcon = this.onPressSectionHeaderIcon.bind(this);
    this.onPressRow = this.onPressRow.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.setProfileData = this.setProfileData.bind(this);
    this.setIsShowContactProfile = this.setIsShowContactProfile.bind(this);
    this.setShowSearchOnScroll = this.setShowSearchOnScroll.bind(this);
    this.subscribe = this.subscribe.bind(this);
  }

  componentDidMount() {
    this.props.setSubscribeContacts(this.subscribe);
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      StoreCache.getCache(StoreCache.keys.contacts, (contacts) => {
        if (!contacts) {
          contacts = [];
        }
        contacts = _.sortBy(contacts, 'categoryOrder');
        let contactList = this.getContactsCategoryMap(contacts);
        console.log('Contacts.js: Loaded ' + contacts.length + ' contacts from cache');
        
        if (this._isMounted) {
          this.setState({
            contacts: contactList,
            dataSource: this.state.dataSource.cloneWithRowsAndSections(contactList),
            displayContacts: contactList,
            isCacheLoaded: true
          });
        }
        this.subscribe();
      });
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state.loggedInUser) != JSON.stringify(this.props.loggedInUser())) {
      console.log('updated')
      this.subscribe();
      if (this._isMounted) {

        this.setState({
          loggedInUser: this.props.loggedInUser()
        });
      }
    }
  }

  subscribe() {
    if (this.props.isLoggedIn()) {
      console.log('Contacts: subscribe');
      Users.subscribeContacts().then(() => {
        this.observeContacts();
      }).catch(error => {
        console.log(error);
      });
    }
  }

  observeContacts() {

    Users.observeContacts( this.props.loggedInUser(), (contacts) => {
      let contactList = _.sortBy(contacts, 'categoryOrder');
      let contactsMapCategory = this.getContactsCategoryMap(contactList);
      if (JSON.stringify(contactsMapCategory) != JSON.stringify(this.state.contacts)) {
        console.log('Contacts.js: Loaded ' + contactList.length + ' contact/s from observer');
        // StoreCache.save(StoreCache.keys.contacts);
        StoreCache.storeCache('contacts', contacts);
        Users.updateChat(contactList);
        if (this._isMounted) {
          this.setState({
            contacts: contactsMapCategory,
            isAllLoaded: true,
            dataSource: this.state.dataSource.cloneWithRowsAndSections(contactsMapCategory),
            displayContacts: contactsMapCategory,
          });
        }
      } else {
        if (this._isMounted) {
          if (this.state.isCacheLoaded) {
            this.setState({
              isAllLoaded: true
            });
          }
        }
      }
    });
  }

  getContactsCategoryMap(contacts) {
    let user = this.props.loggedInUser();
    let blocked = _.reject(user.friendRequestReceived, function(fr) { return fr.date != null });
    
    let blockedIds =  _.pluck(blocked, 'userId');

    var contactsCategoryMap = []
    let categories = this.state.shownCategories
    for (let i = 0; i < categories.length; i++) {
      let data = [];
      contacts.map(function(contactItem) {
        if (contactItem.category == Category.friendRequest.name) {
          if (_.contains(blockedIds, contactItem._id)) {
            contactItem.categoryOrder = Category.block.order;
            contactItem.category = Category.block.name;
          } 
        } else if (contactItem.category == Category.block.name && !_.contains(blockedIds, contactItem._id)) {
          contactItem.categoryOrder = Category.friendRequest.order;
          contactItem.category = Category.friendRequest.name;
        }

        if (categories[i] == contactItem.category) {
          data.push(contactItem);
        }
      });
      !_.isEmpty(data)&& contactsCategoryMap.push({category: categories[i], data: data});      
    }
    return contactsCategoryMap;
  }

  render() {
    return (
      <ContactsRender
        contacts={this.state.displayContacts}
        dataSource={this.state.dataSource}
        goToGroupDetails={this.goToGroupDetails}
        // hasInternet={this.props.hasInternet}
        isAllLoaded={this.state.isAllLoaded}
        isShowContactProfile={this.state.isShowContactProfile}
        // isShowSearchOnContacts={this.props.isShowSearchOnContacts}
        onPressRow={this.onPressRow}
        onPressSectionHeaderIcon={this.onPressSectionHeaderIcon}
        onSearchChange={this.onSearchChange}
        profileData={this.state.profileData}
        // renderConnectionStatus={this.props.renderConnectionStatus}
        setProfileData={this.setProfileData}
        setShowSearchOnScroll={this.setShowSearchOnScroll}
        shownCategories={this.state.shownCategories}
        // theme={this.props.theme}
        {...this.props}
      />
    )
  }

  setShowSearchOnScroll() {
    if (this.props.isShowSearchOnContacts() && _.isEmpty(this.state.searchContact)) {
      this.props.setShowSearchOnContacts();
    }
  }

  onSearchChange(searchText) {
    if (searchText != '') {
      this.setState({
        isLoadingSearch: true,
        searchContact: searchText
      });
      const contacts = this.state.displayContacts;
      let search = [];
      for (let i = 0; i < contacts.length; i++) {
        _.filter(contacts[i].data, function(contact){
          if ((contact.name).toLowerCase().indexOf(searchText.toLowerCase()) != -1) {
            search.push(contact);
          };
        });
      }
      
      if (this._isMounted) {
        this.setState({
          displayContacts: this.getContactsCategoryMap(search),
          dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getContactsCategoryMap(search)),
          isLoadingSearch: false
        });
      }
    } else {
      if (this._isMounted) {
        this.setState({
          displayContacts: this.state.contacts,
          dataSource: this.state.dataSource.cloneWithRowsAndSections(this.state.contacts),
          isLoadingSearch: false
        });
      }
    }
  }

  onPressSectionHeaderIcon(category) {
    var shownCategories = this.state.shownCategories;
    console.log('shownCategories');
    if (_.contains(shownCategories, category)) {
      shownCategories = _.without(shownCategories, category);
    } else {
      shownCategories.push(category);
    }
    if (this._isMounted) {
      LayoutAnimation.configureNext(Animation.linear);
      SoundEffects.playSound("tick")
      this.setState({
        shownCategories: shownCategories
      });
    }
  }

  onPressRow(data) {
    let category = data.category;
    if (category == Category.friendRequest.name) {
      this.goToAddFriends(data);
    } else {
      this.setIsShowContactProfile(data);
    }
  }

  unblockUser(data) {
    this.setProfileData(null, null);
    ddp.call('addFriendRequest', [data._id, true]).then(result => {
      console.log('Unblock result', result);
    });
  }

  setIsShowContactProfile(data) {
    if (data) {
      let category = data.category;
      let actions = [];
      if (category == Category.friend.name) {
        actions = [{icon: 'recent', iconLabel: 'Chat', onPressIcon: () => this.goToChat(data)}];
      } else if (category == Category.group.name) {
        actions = [{icon: 'recent', iconLabel: 'Chat', onPressIcon: () => this.goToChat(data)}];
      } else if (category == Category.profile.name) {
        actions = [{icon: 'settings', iconLabel: 'Edit Profile', onPressIcon: () => this.goToProfile(data)}];
      } else if (category == Category.block.name) {
        actions = [{icon: 'block', iconLabel: 'Unblock', onPressIcon: () => this.unblockUser(data)}];        
      }

      if (category == Category.group.name) {
        if (this.props.isLoggedIn()) {
          ddp.call('getGroupUsersById', [data._id]).then(users => {
            data.users = users;
            this.setProfileData(data, actions);
          }).catch(error=>{
            console.log('Contacts ['+ logTime() +']: ', error);
          });
        } else {
          //console.warn();('Show no Internet');
        }
      } else {
        this.setProfileData(data, actions);
      }
    }
  }

  setProfileData(data, actions) {
    let profileData = {
      data: data,
      actions: actions,
      relStatus: null
    }
    if (this._isMounted) {
      this.setState({
        isShowContactProfile: !this.state.isShowContactProfile,
        profileData: profileData
      });
    }
  }

  goToAddFriends(data) {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name:'AddFriends',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeBgImage: this.props.changeBgImage,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        hasInternet: this.props.hasInternet,
        hasNewFriendRequest: this.props.hasNewFriendRequest,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setLoggedInUser: this.props.setLoggedInUser,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        subscribeContacts: this.props.subscribeContacts,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        users: this.props.users,
        setIsContactsChanged: this.props.setIsContactsChanged,
      });
    });
  }

  goToChat(data) {
    this.setProfileData(null);
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'messagesgiftedchat',
        name:'MessagesGiftedChat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeBgImage: this.props.changeBgImage,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        // group: null,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setLoggedInUser: this.props.setLoggedInUser,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        user: data,
        // users: null,
        // messages: this.messages
      });
    });
  }

  goToGroupDetails(data) {
    this.setProfileData(null);
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name: 'GroupDetails',
        backgroundImageSrc: this.props.backgroundImageSrc,
        getGroupUsers: this.getGroupUsers,
        groupUsers: data.users,
        theme: this.props.theme,
        group: data,
        loggedInUser: this.props.loggedInUser,
        shouldReloadData: this.props.shouldReloadData,
        renderConnectionStatus: this.props.renderConnectionStatus,
        hasInternet: this.props.hasInternet,
        refreshGroups: this.props.refreshGroups,
        timeFormat: this.props.timeFormat
      });
    });
  }

  getGroupUsers() {
    // This is for MessagesGiftedChat: to update message list after adding or removing user.
    return null;
  }

  goToProfile(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    this.setProfileData(null);

    requestAnimationFrame(() => {
      data['mobile'] = this.props.loggedInUser().mobile;
      data['username'] = this.props.loggedInUser().username;
      this.props.navigator.push({
        name: 'Profile',
        color: color,
        renderConnectionStatus: this.props.renderConnectionStatus,
        locationError: this.props.locationError,
        loggedInUser: this.props.loggedInUser,
        hasInternet: this.props.hasInternet,
        isAutoLocate: this.props.isAutoLocate,
        identifyCurrentLocation: this.props.identifyCurrentLocation,
        setIsAutoLocate: this.props.setIsAutoLocate,
        shouldReloadData: this.props.shouldReloadData,
        theme: this.props.theme,
        user: data
      });
    });
  }
}

// Export module.
module.exports = Contacts;
