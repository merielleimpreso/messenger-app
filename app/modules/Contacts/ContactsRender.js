import React, {
  Component,
} from 'react';
import {
  Dimensions,
  ListView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  View,
  StyleSheet
} from 'react-native';
import _ from 'underscore';
import {getMediaURL, renderFooter} from '../Helpers';
import Accordion from '../common/Accordion';
import Category from './category';
import GLOBAL from'../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from '../common/ListRow';
import Navigation from '../actions/Navigation';
import NavigationBar from 'react-native-navbar';
import RenderModal from '../common/RenderModal';
import Toolbar from '../common/Toolbar'
import styles from './styles';

const SCREEN = Dimensions.get('window');

class ContactsRender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSection: true
    }
    this.renderContent = this.renderContent.bind(this);
    this.renderContentCategory = this.renderContentCategory.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
  }

  render () {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        {(Platform.OS == 'ios')
          ? <View>
              <Toolbar title='Contacts'
                theme={this.props.theme}
                rightIcon='addcontact'
                onPressRightButton={() => Navigation.onPress(() => this.props.onActionSelected(1))}
              />
              {this.props.renderConnectionStatus()}
            </View>
          : null}
        {this.renderSearch()}
        {this.renderContent()}
        {this.renderModal()}
      </View>
    );
  }

  renderSearch() {
    let width = SCREEN.width - 20;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let isShowSearchOnContacts =  this.props.isShowSearchOnContacts();

    if (isShowSearchOnContacts || Platform.OS == 'ios') {
      return (
        <View style={[styles.searchBoxContainer, {backgroundColor: color.HIGHLIGHT, width: width, margin: 10}]}>
          <TextInput
            placeholder='Search'
            placeholderTextColor={color.TEXT_LIGHT}
            style={[styles.input, {backgroundColor: 'transparent'}]}
            onChangeText={(text) => this.props.onSearchChange(text)} />
        </View>
      );
    } else {
      return null;
    }
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ScrollView onScroll={() => { this.props.setShowSearchOnScroll(); }}
        automaticallyAdjustContentInsets={false}
        contentInset={{bottom:50}}
        scrollEventThrottle={200}>
        <Accordion 
          activeSection={this.props.shownCategories}
          sections={this.props.contacts}
          renderHeader={this.renderHeader}
          renderContent={this.renderContentCategory}
          underlayColor={color.HIGHLIGHT}
        /> 
        {this.renderFooter()}
      </ScrollView>
    )
  }

  renderHeader(content, index, isActive) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let borderTop = (content.category != Category.profile.name) ? {borderTopWidth: 1, borderTopColor: color.HIGHLIGHT} : null;    
    let icon = isActive ? 'up' : 'dropdown';
    let iconColor = isActive ? color.TEXT_LIGHT : color.INACTIVE_BUTTON;
    let label = (content.data.length > 1) ? `${content.category}s (${content.data.length})` : content.category;
    return (
      <View style={[borderTop, {paddingLeft: 10, paddingRight: 10, flexDirection: 'row', backgroundColor: color.CHAT_BG}]}>
        <Text style={{fontSize:12, color:color.TEXT_DARK, marginTop:3, marginBottom:3, flex: 1}}>{label}</Text>
        <KlikFonts name={icon} style={[styles.actionButtonIcon, {color: iconColor}]} />
      </View>
    );
  }

  renderContentCategory(content, index, isActive) {
    return (
      content.data.map(item => this.renderRow(item))
    );
  }

  renderSectionHeader(sectionData, category) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let items = sectionData;
    var header = category;
    let borderTop = null;
    if (category != Category.profile.name) {
      header += (items.length > 1) ? 's ' : ' ';
      header += '(' + items.length + ')';
      borderTop = {borderTopWidth: 1, borderTopColor: color.HIGHLIGHT};
    }
    let icon = _.contains(this.props.shownCategories, category) ? 'up' : 'dropdown';
    let iconColor = _.contains(this.props.shownCategories, category) ? color.TEXT_LIGHT : color.INACTIVE_BUTTON;
    return (
      <View style={[borderTop, {marginLeft: 10, marginRight: 10, flexDirection: 'row', backgroundColor: color.CHAT_BG}]}>
        <Text style={{fontSize:12, color:color.TEXT_DARK, marginTop:3, marginBottom:3, flex: 1}}>{header}</Text>
        <KlikFonts name={icon}
          style={[styles.actionButtonIcon, {color: iconColor}]}
          onPress={() => this.props.onPressSectionHeaderIcon(category)} />
      </View>
    );
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let image = (data.image) ? data.image
                  : (data._id.includes('group')) ? GLOBAL.DEFAULT_GROUP_IMAGE
                    : GLOBAL.DEFAULT_PROFILE_IMAGE;
    image = getMediaURL(image);
    return (
      <ListRow key={data._id}
        onPress={() => this.props.onPressRow(data)}
        labelText={data.name}
        detailText={data.statusMessage}
        image={image}
        color={color}
        addSeparator={false}
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return renderFooter(this.props.isAllLoaded, color, this.props.hasInternet());
  }

  renderModal() {
    if (this.props.isShowContactProfile) {
      return (
        <RenderModal
          theme={this.props.theme}
          profileData={this.props.profileData}
          setIsShowModal={() => this.props.setProfileData(null)}
          goToGroupDetails={this.props.goToGroupDetails}
          modalType={'viewProfile'}
        />
      );
    }
  }
}

// Export module.
module.exports = ContactsRender;
