'use strict'

import React, {
  Component
} from 'react';
import {
  AsyncStorage,
  ActivityIndicator,
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';
import {toNameCase, getMediaURL} from './Helpers.js';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ImageWithLoader from './common/ImageWithLoader';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
const styles = StyleSheet.create(require('./../styles/styles_common'));
const GLOBAL = require('./config/Globals.js');
const ImagePickerManager = require('NativeModules').ImagePickerManager;
const tempprofpic = '/assets/avatar/tempprofpic.jpg';
var timeout;

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading:true,
      isLoaded:false,
      isLoggingOut: false,
      newPassword: '',
      oldPassword: '',
      status: 0,
      loggedInUser: this.props.loggedInUser(),
      name: '',
      statusMessage: 'Not set',
      mobile: this.props.loggedInUser().mobile,
      isShareUpdate: true,
      isUploading: false,
      photoOptions: {
        cameraType: 'back',
        mediaType: 'photo',
        maxWidth: 800,
        maxHeight: 800,
        quality: 1,
        allowsEditing: false,
        noData: false
      }
    }

    this.changeAvatar = this.changeAvatar.bind(this);
    this.onButtonPressIn = this.onButtonPressIn.bind(this);
    this.onButtonPressOut = this.onButtonPressOut.bind(this);
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.renderUploading = this.renderUploading.bind(this);
    this.setIsUploading = this.setIsUploading.bind(this);
    this.shareUpdate = this.shareUpdate.bind(this);
    this.setMobileNumber = this.setMobileNumber.bind(this);
    this.setDisplayName = this.setDisplayName.bind(this);
    this.setStatusMessage = this.setStatusMessage.bind(this);

    // Get access token saved through AsyncStorage.
    // AsyncStorage.getItem('accessToken').then((accessToken) => {
    //   if (accessToken) { // A login token is saved.
    //     console.log('accessToken: ', accessToken);
    //   } else { // No login token saved.
    //     console.log('accessToken: ', accessToken);
    //   }
    // });

  }

  componentWillMount() {
    // console.log('@componentWillMount');
  }

  componentDidMount() {
    // console.log('@componentDidMount');
    this._isMounted = true;
    let loggedInUser = this.state.loggedInUser;

    if (this._isMounted) {
      
      if (!loggedInUser.profile.hasOwnProperty('isShareUpdate')) {
        this.setState({isShareUpdate: true});
      } else {
        this.setState({
          isShareUpdate: JSON.parse(loggedInUser.profile.isShareUpdate)
        });
      }

      if (!loggedInUser.profile.hasOwnProperty('displayName')) {
        this.setState({
          name: `${toNameCase(loggedInUser.profile.firstName)} ${toNameCase(loggedInUser.profile.lastName)}`});
      } else {
        this.setState({
          name: loggedInUser.profile.displayName
        });
      }

      if (loggedInUser.profile.hasOwnProperty('statusMessage')) {
        if (loggedInUser.profile.statusMessage &&
            loggedInUser.profile.statusMessage !== 'empty') {
          this.setState({
            statusMessage: loggedInUser.profile.statusMessage
          });
        }
      } 
    }
  }

  componentWillReceiveProps() {
    // console.log('@componentWillReceiveProps');
  }


  shouldComponentUpdate() {
    // console.log('@shouldComponentUpdate');
    return true; // should return boolean true|false
  }

  componentWillUpdate() {
    // console.log('@componentWillUpdate');
  }

  componentDidUpdate() {
    // console.log('@componentDidUpdate');
  }

  componentWillUnmount() {
    // console.log('@componentWillUnmount');
    this._isMounted = false;
  }

  render() {
    let loggedInUser = this.state.loggedInUser;
    let theme = this.props.theme();
    let color = GLOBAL.COLOR_THEME[theme];
    let photoUrl = !loggedInUser.profile.photoUrl.startsWith('data:image')
        ? getMediaURL(loggedInUser.profile.photoUrl) 
        : getMediaURL(tempprofpic);
    let user_id = loggedInUser.apiUser;
    let statusMessageColor = this.state.statusMessage ? color.TEXT_DARK : color.TEXT_INPUT_PLACEHOLDER;
    let email = loggedInUser.emails[0].address;

    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.HIGHLIGHT}]}>
        <NavigationBar tintColor={color.THEME}
         title={{ title: 'Profile', tintColor: color.BUTTON_TEXT}}
         rightButton={
           <TouchableHighlight onPress={this.onPressButtonClose} underlayColor='transparent' style={{alignSelf:'center'}}>
             <KlikFonts name='close' color={color.BUTTON_TEXT} style={styles.navbarButton} size={30}/>
           </TouchableHighlight>
         }/>

          <ScrollView ref='sm' scrollPadding ={10} automaticallyAdjustContentInsets={false}>
            
            <View style={{flex:1, flexDirection:'row', marginTop:15, marginBottom:15}}>
              <View style={{flex:1, alignItems:'center'}}>
                <TouchableHighlight onPress={this.changeAvatar} underlayColor='transparent'>
                  <View>
                    <ImageWithLoader image={photoUrl} color={color} style={{width:70,height:70,borderRadius:35}} />
                    <View style={[styles.contactsCreateGroupImage, {backgroundColor:color.CHAT_BG, borderColor:color.CHAT_SENT}]}>
                      <KlikFonts name='photo' color={color.TEXT_LIGHT} size={15}/>
                    </View>
                  </View>
                </TouchableHighlight>
              </View>
              <View style={{marginLeft:5, flex: 2, justifyContent:'center'}}>
                <Text style={[styles.chatRecentTextMessage, {color:color.THEME}]}>Phone Number</Text>
                <TouchableHighlight style={{marginBottom:8}} onPress={() => this.onPressMobileNumber()} underlayColor='transparent'>
                  <View style={{flexDirection:'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                    <Text style={{ fontSize: 16, color:color.TEXT_DARK, fontWeight:'500'}}>{this.state.mobile}</Text>
                    <KlikFonts name='next' color={color.TEXT_LIGHT} size={20} style={{backgroundColor:'transparent'}}/>
                  </View>
                </TouchableHighlight>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  {/*
                  <TouchableHighlight onPress={() => {}} underlayColor='transparent'>
                    <View style={{marginRight:5, padding:5, borderRadius:3, backgroundColor:color.CHAT_BG, flexDirection:'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                      <KlikFonts name='home' color={color.TEXT_LIGHT} size={15} style={{backgroundColor:'transparent'}}/>
                      <Text style={{marginLeft:2, fontSize: 10, color:color.TEXT_DARK, fontWeight:'500'}}>Home</Text>
                    </View>
                  </TouchableHighlight>

                  
                  <TouchableHighlight onPress={() => {}} underlayColor='transparent'>
                    <View style={{marginRight:5, padding:5, borderRadius:3, backgroundColor:color.CHAT_BG, flexDirection:'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                      <KlikFonts name='home' color={color.TEXT_LIGHT} size={15} style={{backgroundColor:'transparent'}}/>
                      <Text style={{marginLeft:2, fontSize: 10, color:color.TEXT_DARK, fontWeight:'500'}}>Keep</Text>
                    </View>
                  </TouchableHighlight>
                  */}
                </View>
              </View>
            </View>
            <View style={{backgroundColor:color.CHAT_BG, borderTopWidth:.5, borderBottomWidth:1, borderTopColor:color.TEXT_LIGHT, borderBottomColor:color.TEXT_LIGHT}}>
               <View style={{flexDirection:'row', margin:5, marginLeft:10, marginRight:10}}>
                 <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                    <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_DARK}]}>Share Profile Media Updates</Text>
                 </View>
                 <Switch onValueChange={(value) => this.shareUpdate(value)} value={this.state.isShareUpdate} />
               </View>
            </View>
            <Text style={{color:color.TEXT_INPUT_PLACEHOLDER, fontSize:10, marginTop:8, marginRight:10,marginBottom:20, marginLeft:10 }}>Share profile media updates on Timeline</Text>
            <TouchableHighlight onPress={() => this.onPressDisplayName()} underlayColor='transparent'>
              <View style={{backgroundColor:color.CHAT_BG, borderTopWidth:.5, borderBottomWidth:1, borderTopColor:color.TEXT_LIGHT, borderBottomColor:color.TEXT_LIGHT}}>
                 <View style={{flexDirection:'row', margin:10, marginLeft:10, marginRight:0}}>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                      <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_DARK}]}>Display Name</Text>
                   </View>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'flex-end'}}>
                     <Text style={{fontSize:12, color:color.TEXT_LIGHT, fontWeight:'500'}}>{this.state.name}</Text>
                     <KlikFonts name='next' color={color.TEXT_LIGHT} size={20}/>
                   </View>
                 </View>
              </View>
            </TouchableHighlight>

            <Text style={{color:color.TEXT_INPUT_PLACEHOLDER, fontSize:10, marginTop:20, marginRight:10, marginLeft:10, marginBottom:8}}>Status Message</Text>
            
            <TouchableHighlight onPress={() => this.onPressStatusMessage()} underlayColor='transparent'>
              <View style={{backgroundColor:color.CHAT_BG, borderTopWidth:.5, borderBottomWidth:1, borderTopColor:color.TEXT_LIGHT, borderBottomColor:color.TEXT_LIGHT}}>
                 <View style={{flexDirection:'row', margin:14, marginLeft:10, marginRight:0}}>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                      <Text style={[styles.chatRecentTextMessage, {color:statusMessageColor}]}>{this.state.statusMessage}</Text>
                   </View>
                   <KlikFonts name='next' color={color.TEXT_LIGHT} size={20} style={{backgroundColor:'transparent'}}/>
                 </View>
              </View>
            </TouchableHighlight>

            <View style={{backgroundColor:color.CHAT_BG,  borderTopWidth:0.5, borderBottomWidth:0.5, borderColor:color.TEXT_LIGHT, marginTop:25, paddingLeft:10}}>
              <View style={{flexDirection:'row', margin:10, marginLeft:0}}>
                <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_DARK, flex:1}]}>User ID</Text>
                <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_LIGHT,fontWeight:'500'}]}>{user_id}</Text>
             </View>
              {/*
              <View style={{backgroundColor:color.CHAT_BG}}>
                 <View style={{flexDirection:'row', margin:5, marginLeft:10, marginRight:10}}>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                      <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_DARK, fontWeight:'500'}]}>Add by ID</Text>
                   </View>
                   <Switch value={true} />
                 </View>
              </View>
              */}
              
            </View>

            {/*
            <Text style={{color:statusMessageColor, fontSize:11, marginTop:10, marginRight:10,marginBottom:20, marginLeft:10 }}>People can add you as a friend by searching for your ID.</Text>
            <TouchableHighlight onPress={() => {}} underlayColor='transparent'>
              <View style={{backgroundColor:color.CHAT_BG, borderTopWidth:.5, borderBottomWidth:1, borderTopColor:color.TEXT_LIGHT, borderBottomColor:color.TEXT_LIGHT}}>
                 <View style={{flexDirection:'row', margin:5, marginLeft:10, marginRight:0}}>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                      <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_DARK}]}>My QR Code</Text>
                   </View>
                   <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'flex-end'}}>
                     <KlikFonts name='next' color={color.TEXT_LIGHT} size={20}/>
                   </View>
                 </View>
              </View>
            </TouchableHighlight>

            */}
            
           
          </ScrollView>

        {this.renderUploading()}


      </View>

    );

  }


  renderUploading() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this._isMounted) {
      return (
        <Modal open={this.state.isUploading}
        closeOnTouchOutside={false}
        containerStyle={{
          justifyContent: 'center'
        }}
        modalStyle={{
          borderRadius: 2,
          margin: 20,
          padding: 10,
          backgroundColor: color.CHAT_BG
        }}
        >
        <ActivityIndicator styleAttr="Inverse" color={color.THEME} />
        <Text style={{textAlign: 'center', color: color.THEME}}>Uploading Photo...</Text>
        </Modal>
      )
    }
  }

  setIsUploading(isUploading) {
    if (this._isMounted) {
      this.setState({isUploading: isUploading});
    }
  }

  setMobileNumber(newMobileNumber) {
    this.setState({
      mobile: newMobileNumber
    });
  }

  setDisplayName(newDisplayName) {
    this.setState({
      name: newDisplayName
    });
  }

  setStatusMessage(newStatusMessage) {
    newStatusMessage = newStatusMessage !== 'empty' ? newStatusMessage : 'Not set';
    this.setState({
      statusMessage: newStatusMessage
    });
  }

  shareUpdate(value) {
    ddp.call('isShareUpdate', [value]).then((response) => {
      this.setState({isShareUpdate: JSON.parse(response)});
    }).catch(() => console.log('Something went wrong!.') );
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPressMobileNumber() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id: 'changemobilenumber',
        loggedInUser: this.props.loggedInUser,
        theme: this.props.theme,
        setMobileNumber: this.setMobileNumber
      });
    });
  }

  onPressDisplayName() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id: 'changedisplayname',
        loggedInUser: this.props.loggedInUser,
        theme: this.props.theme,
        setDisplayName: this.setDisplayName
      });
    });
  }

  onPressStatusMessage() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id: 'changestatusmessage',
        loggedInUser: this.props.loggedInUser,
        theme: this.props.theme,
        setStatusMessage: this.setStatusMessage
      });
    });
  }

  

  onButtonPressIn() {
    console.log('in press');
    this.setState({pressStatus: true});
    // timeout = setTimeout(() => this.goToDebug(), 5000);
    timeout = setTimeout(() => Alert.alert(ddp.host + ':' + ddp.port),
      3000);
  }

  onButtonPressOut() {
    this.setState({pressStatus: false});
    clearInterval(timeout)
  }

  renderLoggingOut() {
    let color = this.props.color;
    return (
      <Modal open={this.state.isLoggingOut}
       closeOnTouchOutside={false}
       containerStyle={{
         justifyContent: 'center'
       }}
       modalStyle={{
         borderRadius: 2,
         margin: 20,
         padding: 10,
       }}
      >
       <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
       <Text style={{textAlign: 'center', color: color.BUTTON}}>Logging you out...</Text>
      </Modal>
    )
  }

  changeAvatar() {
    Alert.alert(
      'Upload from',
      null,
      [
        {text: 'Gallery', onPress: () => this.launchGallery()},
        {text: 'Camera', onPress: () => this.launchCamera()},
        {text: 'CANCEL', onPress: () => {}}
      ]
    )
  }

  launchGallery() {
    ImagePickerManager.launchImageLibrary(this.state.photoOptions, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePickerManager Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setIsUploading(true);
        const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
        var loggedInUser = this.state.loggedInUser;
        loggedInUser.profile.photoUrl = source.photoUrl
        ddp.call('changePhoto', [source.photoUrl]).then(result => {
          if (result) {
            if (this._isMounted) {
              loggedInUser.profile.photoUrl = result;
              this.setState({
                loggedInUser: loggedInUser
              });
            }
            if (this.state.isShareUpdate) {
              this.postChangePhoto(response);
            }
          }
          this.setIsUploading(false);
        }).catch(function(e){
          console.log('[Profile] changeAvatar Error: ', e)
        });
      }
    });
  }

  launchCamera(){
    ImagePickerManager.launchCamera(this.state.photoOptions, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePickerManager Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setIsUploading(true);
        const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
        var loggedInUser = this.state.loggedInUser;
        ddp.call('changePhoto', [source.photoUrl]).then(result => {
          if (result) {
            loggedInUser.profile.photoUrl = result;
            this.setState({
              loggedInUser: loggedInUser
            });
            if (this.state.isShareUpdate) {
              this.postChangePhoto(response);
            }
          }
          this.setIsUploading(false);
        })
        .catch(function(e){
          console.log('[Profile] changeAvatar Error: ', e)
        });
      }
    });
  }

  postChangePhoto(image) {
    var content = {
      Image: image,
      Text: 'Changed profile photo',
      isProfilePicture: 1
    };
    Send.post(content, this.state.loggedInUser._id, () => {});
  }

  // Change password
  changePass() {
    var oldPass = this.state.oldPassword;
    var newPass = this.state.newPassword;
    var confirmPass = this.state.confirmPassword;
    var newPassError,confirmPassError,notifySuccess = <View/>;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    if (newPass.length < 6) {
      newPassError = <Text style={[styles.loginError, {color:color.ERROR}]}>Password should at least be 6 characters.</Text>;
    }
    else if (newPass !== confirmPass) {
      confirmPassError = <Text style={[styles.loginError, {color:color.ERROR}]}>Password does not match.</Text>;
    }
    else{
      var change = ddp.call('changePassword', [oldPass, newPass]).then(function(value) {
          // console.log(value); // "Success!"
          throw {reason:'Password changed'};

        }).catch(function(e) {
          confirmPassError = <Text style={[styles.loginError, {color:color.ERROR}]}>{e.reason}</Text>;

            console.log(this.refs.sm.input);
          this.refs.sm.input_oldpass.setNativeProps({text: ''});
          this.refs.sm.input_newpass.setNativeProps({text: ''});
          this.refs.sm.input_confirmpass.setNativeProps({text: ''});
          this.refs.sm.input_oldpassword.focus();
        }.bind(this));
        // if (change._65.passwordChanged) {
        //   notifySuccess = <Text>Successfully changed password.</Text>;
        // }
        // else{
        //   notifySuccess = <Text>{change._65.reason}</Text>;
        // }
      // });
    }
    if (this._isMounted) {
      this.setState({
        newPassError: newPassError,
        confirmPassError:confirmPassError,
      });
    }
  }

}

module.exports = Profile;






// /*

// logout() {
//   let theme = this.props.theme();
//   ddp.logout((err, res) => {
//     if (this._isMounted) {
//       this.setState({
//         isLoggingOut: true
//       });
//     }
//     this.props.navigator.replace({
//       // id: 'login',
//       id: 'kliklabsportal',
//       loggedInUser: this.props.loggedInUser,
//       hasInternet: this.props.hasInternet,
//       renderConnectionStatus: this.props.renderConnectionStatus,
//       shouldReloadData: this.props.shouldReloadData,
//       theme: this.props.theme,
//       changeTheme: this.props.changeTheme,
//       addContactToLoggedInUser: this.addContactToLoggedInUser,
//       changeBgImage: this.props.changeBgImage,
//       bgImage: this.props.bgImage,
//       setImage: this.props.setImage,
//       subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
//       logout: this.props.logout
//     });
//     this.props.logout();
//   });
// }
//
// Author: Merielle Impreso
// Date Created: 2016-04-14
// Date Updated: 2016-05-20
// Description: This file displays the profile of the user.
// Used In: Tabs.ios
//
// Changelog:
// update: 2016-05-10 John Rezan B.
//   - change password
//   - login status details
// update: 2016-05-12 (by Merielle I.)
//   - implement color and UI scheme
//   - implement profile image
// update: 2016-05-17 (by Merielle I.)
//   - receive sign out props from Tabs, Logging Out screen on Tabs
//   - check if ddp.user has value before displaying it
// update: 2016-05-19 John Rezan B.
//     - added SmartScrollView support
// update: 2016-05-20 by Merielle I.
//   - remove logout call method here, put it on Tabs
// update: 2016-06-17 by Merielle I.
//   - added support for customize UI
// update: 2016-07-12 by Merielle I.
//   - removed warning message when scrolling
// update: 2016-07-21 (Merielle I.)
//   - Change ES5 code format to ES6
//
// */
//
//
// 'use strict'
//
// import _ from 'underscore';
// import { toNameCase, getSettings } from './Helpers.js';
// import LoggedOut from './LoggedOut';
// import NavigationBar from 'react-native-navbar';
// import SmartScrollView from './SmartScrollView';
// import KlikFonts from 'react-native-vector-icons/KlikFonts';
// var ImagePickerManager = require('NativeModules').ImagePickerManager;
//
// import React, {
//   Component
// } from 'react';
// import ReactNative, {
//   Alert,
//   AsyncStorage,
//   Image,
//   ScrollView,
//   StyleSheet,
//   Text,
//   TouchableWithoutFeedback,
//   TextInput,
//   TouchableOpacity,
//   View,
// } from 'react-native';
// var styles = StyleSheet.create(require('./../styles/styles_common'));
// var GLOBAL = require('./config/Globals.js');
// var Progress = require('react-native-progress');
// var timeout;
//
// class Profile extends Component {
//
//   // Initialize state and bind functions
//   constructor(props) {
//     super(props);
//     this.state = {
//       firstName: '',
//       lastName: '',
//       image: GLOBAL.DEFAULT_IMAGE,
//       newPassword: '',
//       status: 0,
//       theme: 0,
//     }
//     this.getTheme = this.getTheme.bind(this);
//     this.changeStatus = this.changeStatus.bind(this);
//     this.changeTheme = this.changeTheme.bind(this);
//     this.onButtonPressIn = this.onButtonPressIn.bind(this);
//     this.onButtonPressOut = this.onButtonPressOut.bind(this);
//     this.goToDebug = this.goToDebug.bind(this);
//     this.changePass = this.changePass.bind(this);
//     this.changeAvatar = this.changeAvatar.bind(this);
//     this.launchGallery = this.launchGallery.bind(this);
//     this.launchCamera = this.launchCamera.bind(this);
//     this.logOut = this.logOut.bind(this);
//   }
//
//   /*
//   Put a flag to check if component is mounted.
//   Get currentUser.
//   */
//   componentDidMount() {
//     this._isMounted = true;
//     this.getTheme();
//     getSettings().then((result)=>{
//       this.setState({
//         settings: JSON.parse(result)
//       });
//     });
//     AsyncStorage.getItem('currentUser')
//     .then(result => {
//       var user = JSON.parse(result);
//       if (this._isMounted) {
//         this.setState({
//           user: user
//         });
//       }
//     });
//   }
//
//   // Put a flag to check if component will unmount.
//   componentWillUnmount() {
//     this._isMounted = false;
//   }
//
//   // Get saved theme
//   getTheme() {
//     var theme = this.props.theme;
//
//     for (var i=0; i<GLOBAL.THEMES.length; i++) {
//       if (GLOBAL.THEMES[i] == theme) {
//         if (this._isMounted) {
//           this.setState({theme: i})
//         }
//       }
//     }
//   }
//
//   // Put a flag to check if component will unmount.
//   changeStatus(statuses) {
//     var status = this.state.status + 1;
//     if (this.state.status === (statuses.length - 1)) {
//       status = 0;
//     }
//     if (this._isMounted) {
//       this.setState({
//         status: status
//       });
//     }
//   }
//
//   changeTheme(themes) {
//     var theme = this.state.theme + 1;
//     if (this.state.theme === (themes.length - 1)) {
//       theme = 0;
//     }
//     var themeName = GLOBAL.THEMES[theme];
//     AsyncStorage.setItem('theme', themeName);
//     this.props.getTheme();
//
//     if (this._isMounted) {
//       this.setState({
//         theme: theme
//       });
//     }
//   }
//
//   onButtonPressIn() {
//     console.log('in press');
//     this.setState({pressStatus: true});
//     // timeout = setTimeout(() => this.goToDebug(), 5000);
//     timeout = setTimeout(() => Alert.alert(ddp.host + ':' + ddp.port),
//       3000);
//   }
//
//   onButtonPressOut(){
//     this.setState({pressStatus: false});
//     clearInterval(timeout)
//   }
//
//   goToDebug(){
//     console.log('going to debug');
//     this.props.navigator.push({
//       id: 'debug',
//       title: 'Debug',
//       navigationBarHidden: false,
//       color: this.props.color,
//       settings: this.state.settings
//     });
//   }
//
//   // Change password
//   changePass(){
//     var oldPass = this.state.oldPassword;
//     var newPass = this.state.newPassword;
//     var confirmPass = this.state.confirmPassword;
//     var newPassError,confirmPassError,notifySuccess = <View/>;
//     if (newPass.length < 6) {
//       newPassError = <Text style={styles.loginError}>Password should at least be 6 characters.</Text>;
//     }
//     else if (newPass !== confirmPass) {
//       confirmPassError = <Text style={styles.loginError}>Password does not match.</Text>;
//     }
//     else{
//       var change = ddp.call('changePassword', [oldPass, newPass]).then(function(value) {
//           // console.log(value); // "Success!"
//           throw {reason:'Password changed'};
//
//         }).catch(function(e) {
//           notifySuccess = <Text style={styles.loginError}>{e.reason}</Text>;
//           this.setState({
//             notifySuccess: notifySuccess
//           });
//
//             console.log(this.refs.sm.input);
//           this.refs.sm.input_oldpass.setNativeProps({text: ''});
//           this.refs.sm.input_newpass.setNativeProps({text: ''});
//           this.refs.sm.input_confirmpass.setNativeProps({text: ''});
//           this.refs.sm.input_oldpassword.focus();
//         }.bind(this));
//         // if (change._65.passwordChanged) {
//         //   notifySuccess = <Text>Successfully changed password.</Text>;
//         // }
//         // else{
//         //   notifySuccess = <Text>{change._65.reason}</Text>;
//         // }
//       // });
//     }
//     this.setState({
//       newPassError: newPassError,
//       confirmPassError:confirmPassError,
//     });
//   }
//
//   changeAvatar(){
//     Alert.alert(
//       'Upload from',
//       null,
//       [
//         {text: 'Gallery', onPress: () => this.launchGallery()},
//         {text: 'Camera', onPress: () => this.launchCamera()},
//       ]
//     )
//   }
//
//   launchGallery(){
//     var photoOptions = {
//       cameraType: 'back',
//       mediaType: 'photo',
//       maxWidth: 100,
//       maxHeight: 100,
//       quality: 1,
//       aspectX: 1,
//       aspectY: 1,
//       allowsEditing: true,
//       noData: false,
//       viewLoader: 0
//     }
//     ImagePickerManager.launchImageLibrary(photoOptions, (response) => {
//
//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePickerManager Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//       } else {
//         console.log(response);
//         const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
//         var user = this.state.user;
//         user.profile.photoUrl = source.photoUrl
//         ddp.call('changePhoto', [source.photoUrl]).then(result => {
//           console.log(result);
//           if (result) {
//             this.setState({
//               user: user
//             });
//           }
//         });
//       }
//     });
//   }
//
//   launchCamera(){
//     var photoOptions = {
//       cameraType: 'back',
//       mediaType: 'photo',
//       maxWidth: 800,
//       maxHeight: 800,
//       quality: 1,
//       allowsEditing: true,
//       noData: false,
//       viewLoader: 0
//     }
//     ImagePickerManager.launchCamera(photoOptions, (response) => {
//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePickerManager Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//       } else {
//         console.log(response); // uri (on android)
//         const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
//         ddp.call('changeAvatar', [source.photoUrl]).then(result => {
//           console.log(result);
//           if (result) {
//             this.setState({
//               user: user
//             });
//           }
//         });
//       }
//     });
//   }
//
//   // Display info
//   render() {
//     var name = '';
//     var email = '';
//     var userImage = '';
//     var number = '';
//     if (this.state.user) {
//       name = toNameCase(this.state.user.profile.firstName) + " " + toNameCase(this.state.user.profile.lastName);
//       email = this.state.user.emails[0].address;
//       userImage = this.state.user.profile.photoUrl;
//       number = this.state.user.username;
//     }
//     var image = (userImage == '') ? GLOBAL.DEFAULT_IMAGE : userImage;
//     var color = this.props.color;
//
//     var statuses = [<Text style={[styles.profileStatus, {color:color.THEME}]}>Available</Text>,
//                     <Text style={[styles.profileStatus, {color:color.THEME}]}>Idle</Text>,
//                     <Text style={[styles.profileStatus, {color:color.THEME}]}>Busy</Text>,
//                     <Text style={[styles.profileStatus, {color:color.THEME}]}>Offline</Text>];
//
//     var themes = [<Text style={[styles.profileStatus, {color:color.THEME}]}>{GLOBAL.THEMES[0]}</Text>,
//                   <Text style={[styles.profileStatus, {color:color.THEME}]}>{GLOBAL.THEMES[1]}</Text>,
//                   <Text style={[styles.profileStatus, {color:color.THEME}]}>{GLOBAL.THEMES[2]}</Text>,
//                   <Text style={[styles.profileStatus, {color:color.THEME}]}>{GLOBAL.THEMES[3]}</Text>];
//
//     return (
//       <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
//         <NavigationBar
//           tintColor={color.THEME}
//           title={{ title: 'Profile', tintColor: color.BUTTON_TEXT}} />
//
//           <ScrollView ref='sm'
//             scrollPadding         = { 10 }
//             automaticallyAdjustContentInsets={false}
//           >
//           <View>
//           <View style={styles.profileContainerLeftAlign}>
//             <Image ref='image' style={styles.profileImage} source={{uri: image}} />
//             <KlikFonts name="camera" onPress={() => {this.changeAvatar()}} color={color.THEME} style={{backgroundColor:'transparent', alignSelf:'flex-end', marginLeft:-15}} size={28}/>
//
//             <TouchableOpacity onPressIn={() => {this.onButtonPressIn()}} onPressOut={() => {this.onButtonPressOut()}}>
//               <Text  onPressIn={this.onButtonPressIn} style={[styles.profileName, {color:color.TEXT_DARK}]}> {name} </Text>
//             </TouchableOpacity>
//           </View>
//
//           </View>
//
//           <View style={styles.profileSeparator} />
//
//           <View>
//           <View style={styles.profileContainerDetails}>
//             <View style={styles.profileContainerDetailsContent}>
//               <Text style={[styles.profileDetailsText, {color:color.TEXT_DARK}]}> Theme </Text>
//               <Text style={[styles.profileDetailsText, {color:color.TEXT_DARK}]}> Phone </Text>
//               <Text style={[styles.profileDetailsText, {color:color.TEXT_DARK}]}> Email </Text>
//             </View>
//
//             <View style={styles.profileContainerDetailsContent}>
//               <TouchableOpacity onPress={() => {this.changeTheme(themes)}}>
//                 {themes[this.state.theme]}
//               </TouchableOpacity>
//               <Text style={[styles.profileDetailsText, {color:color.TEXT_DARK}]}>
//                 {number}
//               </Text>
//               <Text style={[styles.profileDetailsText, {color:color.TEXT_DARK}]}>
//                 {email}
//               </Text>
//             </View>
//           </View>
//
//           <View style={{height: 30}} />
//
//           <View style={{marginRight:60, marginLeft:60}}>
//             <TextInput
//             smartScrollOptions = {{
//                 moveToNext: true,
//                 scrollRef:'oldpass',
//                 type:'text'
//               }}
//               autoCapitalize='none'
//               placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
//               autoCorrect={false}
//               onChangeText={(oldPassword) => this.setState({oldPassword})}
//               placeholder='Old Password'
//               returnKeyType='next'
//               secureTextEntry
//               style={styles.profilePasswordTextBox}
//               rejectResponderTermination={false}
//             />
//             <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
//           </View>
//
//           <View style={{marginTop:10, marginRight:60, marginLeft:60}}>
//             <TextInput smartScrollOptions = {{
//                 moveToNext: true,
//                 scrollRef:'newpass',
//                 type:'text'
//               }}
//               autoCapitalize='none'
//               autoCorrect={false}
//               onChangeText={(newPassword) => this.setState({newPassword})}
//               placeholder='New Password'
//               placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
//               returnKeyType='next'
//               secureTextEntry
//               style={styles.profilePasswordTextBox}
//               rejectResponderTermination={false}
//             />
//             <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
//           </View>
//
//           {this.state.newPassError}
//
//           <View style={{marginTop:10, marginRight:60, marginLeft:60}}>
//             <TextInput smartScrollOptions = {{
//                 type:       'text',
//                 scrollRef:'confirmpass'
//               }}
//               autoCapitalize='none'
//               autoCorrect={false}
//               onSubmitEditing={(event) => this.changePass()}
//               onChangeText={(confirmPassword) => this.setState({confirmPassword})}
//               secureTextEntry
//               style={styles.profilePasswordTextBox}
//               placeholder='Confirm Password'
//               placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
//               rejectResponderTermination={false}
//             />
//             <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
//           </View>
//
//           <View style={{height: 10}} />
//           {this.state.confirmPassError}
//           {this.state.notifySuccess}
//           <View style={{height: 10}} />
//
//           <TouchableOpacity onPress={() => this.changePass()}
//             style={[styles.loginButtonSignup, {backgroundColor:color.BUTTON}]}
//             underlayColor={color.HIGHLIGHT}>
//             <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}>
//               Change Password
//             </Text>
//           </TouchableOpacity>
//
//           <TouchableOpacity onPress={() => this.logOut()}
//             style={[styles.loginButton, {backgroundColor:color.BUTTON}]}
//             underlayColor={color.HIGHLIGHT}>
//             <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}>
//               Logout
//             </Text>
//           </TouchableOpacity>
//           <View style={{height:50}}/>
//         </View>
//         </ScrollView>
//       </View>
//     );
//   }
//
//   // Log out user, call props from Tabs
//   logOut() {
//     this.props.setIsLoggingOut(true);
//   }
//
// }
//
// module.exports = Profile;
