'use strict';

import React, {
  Component
} from 'react';
import ReactNative, {
  Alert,
  AsyncStorage,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import _ from 'underscore';
import moment from 'moment';
import {logTime, toNameCase} from './Helpers';
import ButtonIcon from './common/ButtonIcon';
import Chat from './Chat';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Message from './config/db/Message';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
import TypingArea from './TypingArea';

const ADD_TO_LIMIT = 10;
const GLOBAL = require('./config/Globals.js');
const NAVBAR_HEIGHT = 64;
const SCREEN_SIZE = Dimensions.get('window');
const styles = StyleSheet.create(require('./../styles/styles_common'));

class MessagesChat extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      connectionStatusHeight: 0,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isFirstLoad: true,
      isLoading: true,
      isViewVisible: true,
      limit: 10,
      messages: null,
      typeAreaHeight: 0,
      shouldReloadData: this.props.shouldReloadData(),
    }
    
    this.goToChatWallpaper = this.goToChatWallpaper.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.onPressedBack = this.onPressedBack.bind(this);
    this.renderChat = this.renderChat.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.shouldReloadData() != this.state.shouldReloadData) {
      if (this._isMounted) {
        this.setState({
          shouldReloadData: this.props.shouldReloadData()
        })
      }
      if (this.props.shouldReloadData()) {
        console.log('MessagesChat ['+ logTime() +']: Reloading...');
        InteractionManager.runAfterInteractions(() => {
          this.subscribe();
        });
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  subscribe() {
    console.log('MessagesChat ['+ logTime() +']: Loading messages...');
    var id = (this.props.group) ? this.props.group._id : this.props.user._id;
    if (this.props.hasInternet()) {
      Message.subscribeById(id).then(() => {
        this.observe();
      });
    } else {
      this.observe();
    }
  }

  observe() {
    var id = (this.props.group) ? this.props.group._id : this.props.user._id;
    Message.observeById(this.props.loggedInUser()._id, id, (messages) => {
      Message.checkTempAndDeleteSentFiles(messages, (newMessages) => {
        if(JSON.stringify(newMessages) != JSON.stringify(this.state.messages)) {
          if (this.props.loggedInUser()) {
            if (this._isMounted) {
              console.log('MessagesChat ['+ logTime() +']: Loaded ' + newMessages.length + ' message/s');
              var imageGallery = []
              for (let i = 0; i < newMessages.length; i++) {
                let r = newMessages[i];
                if (_.has(r.Message, "Media")) {
                  imageGallery.push(r);
                }
                if (this.props.hasInternet()) {
                  Send.updateSeenMessages(newMessages, this.props.loggedInUser()._id);
                  if (r.isFailed) {
                    Send.resend(r);
                  }
                }
              }

              this.setState({
                dataSource: this.state.dataSource.cloneWithRows(this.generateMessages(newMessages)),
                isLoading: false,
                isAllLoaded: true,
                messages: newMessages,
                imageGallery: imageGallery,
              });
            }
          }
        }
      });
    });
  }

  generateMessages(result) {
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: '2-digit'
    };
    var createDates = [];
    _.each(_.values(result), function(r) {
      var date = new Date(r.CreateDate);
      var formattedDate = moment(date).format('LL');
      r['FormattedDate'] = formattedDate;
      createDates.push(formattedDate);
    });
    createDates = _.uniq(createDates);
    var messages = [];
    _.each(_.values(createDates), function(date) {
      _.each(_.values(result), function(r) {
        if (r.FormattedDate == date) {
          messages.push(r);
        }
      });
      messages.push({date: date});
    });
    return messages;
  }


  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var name = '';
    if (this.props.group) { // Group name
      name = toNameCase(this.props.group.Name);
    } else {
      name = toNameCase(this.props.user.profile.firstName) + ' ' + toNameCase(this.props.user.profile.lastName);
    }

    return (
      <View style={styles.containerNoResults}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: name, tintColor: color.BUTTON_TEXT }}
         leftButton={<ButtonIcon icon='back' color={color} onPress={this.onPressedBack}/>}
         rightButton={<ButtonIcon icon='theme' color={color} onPress={this.goToChatWallpaper} size={20}/>} />
        <View onLayout={(event) => {this.setState({connectionStatusHeight: event.nativeEvent.layout.height});}}>
          {this.props.renderConnectionStatus()}
        </View>
        <Image style={{width: null, height: null, flex: 1, flexDirection: 'column'}} resizeMode={'cover'} source={this.renderBackgroundImage()}>
          {this.renderChat()}
          {this.renderTypingArea()}
        </Image>

      </View>
    );
  }

  renderBackgroundImage() {
    var bgRetriever = this.props.backgroundImageSrc;
    var bgImage = bgRetriever();
      if (bgImage == null || bgImage == '') {
        if (this.props.theme() == "ORIGINAL") {
          return require('./../images/chatbgdefault.jpg');
        } else if (this.props.theme() == "BLUE") {
          return require('./../images/backgroundImage/chatbg1.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "LAVENDER") {
          return require('./../images/backgroundImage/themebg2b.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "ORIGINAL") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else {
          return null;
        }
      } else {
        return {uri: bgImage};
      }
  }

  renderChat() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var chatHeight = SCREEN_SIZE.height - this.state.typeAreaHeight - NAVBAR_HEIGHT - this.state.connectionStatusHeight;
    if (chatHeight < 165) {
      chatHeight = 165;
    }
    var users = null;
    if (this.props.group) {
      users = this.props.users;
    } else {
      users = [this.props.loggedInUser(), this.props.user]
    }
    return (
      <Chat backgroundImageSrc={this.props.backgroundImageSrc}
       changeTheme={this.props.changeTheme}
       chatType={GLOBAL.CHAT.DIRECT}
       color={color}
       dataSource={this.state.dataSource}
       getMessageWallpaper={this.props.getMessageWallpaper}
       hasInternet={this.props.hasInternet}
       height={chatHeight}
       isLoading={this.state.isLoading}
       isAllLoaded={this.state.isAllLoaded}
       loadMore={this.loadMore}
       loggedInUser={this.props.loggedInUser}
       navigator={this.props.navigator}
       renderConnectionStatus={this.props.renderConnectionStatus}
       shouldReloadData={this.props.shouldReloadData}
       theme={this.props.theme}
       timeFormat={this.props.timeFormat}
       users={users}
      />
    );
  }

  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('MessagesChat ['+ logTime() +']: Checking if there are more messages to load...');
          var id = (this.props.group) ? this.props.group._id : this.props.user._id;
          ddp.call('shouldLoadMessageChat', [id, this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('MessagesChat ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              console.log(error);
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  renderTypingArea() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let recipientId = (this.props.group) ? this.props.group._id : this.props.user._id;
    return (
      <View onLayout={(event) => {
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <TypingArea navigator={this.props.navigator}
         color={color}
         theme={this.props.theme}
         isViewVisible={this.state.isViewVisible}
         loggedInUser={this.props.loggedInUser}
         recipientId={recipientId}
         chatType={GLOBAL.CHAT.DIRECT}
        />
      </View>
    )
  }

  // Go back
  onPressedBack() {
    if (this._isMounted) {
      this.setState({
        isViewVisible: false
      });
    }
    requestAnimationFrame(() => {
      var id = (this.props.group) ? this.props.group._id : this.props.user._id;
      this.props.navigator.pop(0);
    });
  }

  goToChatWallpaper() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id: 'changewallpaper',
        theme: this.props.theme,
        getMessageWallpaper: this.props.getMessageWallpaper
      });
    });
  }

}

module.exports = MessagesChat;
