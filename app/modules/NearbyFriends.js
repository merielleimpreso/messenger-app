'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Alert
} from 'react-native';
import {getDisplayName, getMediaURL} from './Helpers';
import Button from './common/Button';
import Geocoder from 'react-native-geocoder';
import ListRow from './common/ListRow';
import ScreenNoItemFound from './ScreenNoItemFound';
import SystemAccessor from './SystemAccessor';
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');
const dpp = require('./config/ddp.js');

class NearbyFriends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonHeight: 0,
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      resultLength: 0,
      nothingToDisplay: false,
      isLoading: true,
      isOpenModalProfile: false,
      checkInButtonText: "Try again."
    }
    this.renderRow = this.renderRow.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.identifyCurrentLocation = this.identifyCurrentLocation.bind(this);
    this.updateMyLocationToServer = this.updateMyLocationToServer.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      console.log("Calling findNearbyContacts");
      this.identifyCurrentLocation();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var content = <View/>;

    if (this.state.resultLength <= 0 && !this.state.isLoading) {
      if (this.state.nothingToDisplay) {
        console.log("Rendered No Contact");
        content = <View style={{marginTop: 10}}>
                    <ScreenNoItemFound isLoading={false} text={'No friend is near you'} color={color}/>
                  </View>;
      } else {
        console.log("Rendered CurrentLocation Not Set");
        content = <View style={{marginTop: 10}}>
                    <ScreenNoItemFound isLoading={false} text={'Location settings are turned off!'} color={color}/>
                    <Button text={this.state.checkInButtonText}
                      color={color.THEME}
                      underlayColor={color.HIGHLIGHT}
                      onPress={() => this.identifyCurrentLocation()}
                      style={{alignSelf:'center', width: windowSize.width*0.25}}
                      isDefaultWidth={false}
                    />
                  </View>;
      }
    } else if(this.state.isLoading) {
      content = <ActivityIndicator size="small" color={color.BUTTON} style={{marginTop: 10}}/>
      //<ScreenNoItemFound isLoading={false} text={'Loading...'} color={color}/>;
    } else {
      // console.log("Rendered List");
      content =   <ListView ref="listview"
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          renderSectionHeader={this.renderSectionHeader}
          onEndReached={this.onEndReached}
          automaticallyAdjustContentInsets={false}
          keyboardDismissMode="on-drag"
          keyboardShouldPersistTaps={true}
          showsVerticalScrollIndicator={false}
        />
    }

    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        {content}
      </View>
    )
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let image = getMediaURL(data.profile.image);
    return (
      <ListRow
        iconPress={() => this.goToMessageChat(data)}
        labelText={getDisplayName(data)}
        detailText={data.location.locality + "  "+ data.distance}
        button={"Chat"}
        image={image}
        color={color}
        addSeparator={true}
      />
    );
  }

  goToMessageChat(data) {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    let directData = {
    _id: data.id,
    image: data.profile.image,
    name: data.profile.name};

    console.log("Data: ", data);
    let groupData = null;
    // console.log("Tried to go to: ", data);
    if (navigatorStack[currentRoute].name !== 'MessagesGiftedChat') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id:'messagesgiftedchat',
          name:'MessagesGiftedChat',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          isAutoSaveMedia: this.props.isAutoSaveMedia,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          changeBgImage: this.props.changeBgImage,
          timeFormat: this.props.timeFormat,
          user: directData,
          isLoggedIn: this.props.isLoggedIn
        });
      });
    }
  }

  identifyCurrentLocation() {
    if (this._isMounted) {
      this.setState({
        checkInButtonText: "Finding..."
      });
    }
    console.log("Identifying current location...");
    navigator.geolocation.getCurrentPosition((position) => {
      if (this._isMounted) {
        this.setState({
          checkInButtonText: "Try again"
        });
      }
      //Identify location name using react-native-geocoder
      Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(res => {
        //console.log("Geocoder result: ",res);
        console.log("Identified. Currently in: ", res[0].locality);
        this.updateMyLocationToServer(position, res[0].locality);
      }).catch((error) => {
          Alert.alert("Oops!", "Unable to identify current location at this time: \n\nReason: "+ error + ". \n\nTry again? ", [
            {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
            {text: "No", onPress: () => {}}
          ]);
      });
    },(error) => {
      if (error === "No available location provider.") {
        Alert.alert("Oops!", "Unable to identify current location at this time: \n\nReason: Location might be turned off. \n\nTry again? ", [
          {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
          {text: "No", onPress: () => {}},
          {text: "Settings", onPress: () => {this.goToLocationSettings()}}
        ]);
      } else {
          Alert.alert("Oops!", "Unable to identify current location at this time. \n\nReason: "+ error + " \n\nTry again? ", [
            {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
            {text: "No", onPress: () => {}}
         ]);
      }
      if (this._isMounted) {
        this.setState({
          nothingToDisplay: false,
          isLoading: false,
          checkInButtonText: "Try again"
        });
      }
    },{timeout: 50000, maximumAge: 1000});
  }

  updateMyLocationToServer(position, locality) {
    var location = {latitude: position.coords.latitude, longitude: position.coords.longitude, locality: locality};
    console.log("Updating location in server... ");

    ddp.call("updateUserLocation", [location]).then(()=>{
      console.log("Successfully updated location in server. ");
      console.log("Finding nearby contacts... ");
      ddp.call("findNearbyContacts", []).then((result)=>{
        this.state.resultLength = result.length;

        if (result!= "No Coordinates found!") {
          console.log("Found "+ result.length +" nearby contacts.");
          if (result.length <= 0) {
            this.state.nothingToDisplay = true;
          }

          if (this._isMounted) {
            this.setState({
              dataSource: this.state.dataSource.cloneWithRows(result),
              isLoading: false
            });
          }
        } else {
          if (this._isMounted) {
            this.setState({
              isLoading: false,
              nothingToDisplay: false
            });
          }
        }
      });
    }).catch(() => {
      Alert.alert("Oops! ", "Failed to check in to "+ locality+"! Try again?",
      [
        {text: "Yes", onPress: () => this.updateMyLocationToServer(position, locality)},
        {text: "No", onPress: () =>{}}
      ]);
   });
  }

  goToLocationSettings() {
    SystemAccessor.goTo("locationSettings", (error, result) => {
      console.log("goToLocationSettings result: "+ result);
    });
  }


  // renderModalProfile(profileData) {
  //   let color = GLOBAL.COLOR_THEME[this.props.theme()];
  //   if (this.state.isOpenModalProfile && !(_.isEmpty(profileData))) {
  //     return (
  //       <RenderModal
  //         theme={this.props.theme}
  //         profileData={profileData}
  //         setIsShowModal={() => this.setIsOpenProfile(null)}
  //         goToGroupDetails={this.goToGroupDetails}
  //         modalType={'viewProfile'}
  //       />
  //     );
  //   }
  // }
}

// Export module.
module.exports = NearbyFriends;
