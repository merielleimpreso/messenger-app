import React, { Component } from 'react';
import { View } from 'react-native';
import ContactsInvite from './../ContactsInvite';
import FacebookTabBar from './../FacebookTabBar';
import FriendRequest from './../FriendRequest';
import GLOBAL from './../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from './../actions/Navigation';
import NearbyFriends from './../scenes/NearbyFriends';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import SearchOtherMember from './../SearchOtherMember';
import styles from './styles';
import Toolbar from './../common/Toolbar';

export default class AddFriendsRender extends Component {
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={styles.container}>
        <Toolbar title={'Add Friends'}
          toolbarFrom={'Tabs'}
          withBackButton={true}
          onPressLeftButton={() => Navigation.back(this.props.navigator)}
          {...this.props}
        />
        {this.renderTabs()}
      </View>
    );
  }

  renderTabs() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ScrollableTabView
        ref="scrollableTabView"
        initialPage={0}
        renderTabBar={() => <FacebookTabBar color={color} tabFor="AddFriends"/>}
        onChangeTab={() => this.props.onChangeTab()}
        style={{backgroundColor: color.CHAT_BG}}>

          <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="recommended" label="Request" >
            {this.props.renderConnectionStatus()}
            <FriendRequest {...this.props} />
          </View>

          <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="contact" label="Contact" >
            {this.props.renderConnectionStatus()}
            <ContactsInvite {...this.props} />
          </View>

          <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="searchidphone" label="ID/Phone No." >
            {this.props.renderConnectionStatus()}
            <SearchOtherMember {...this.props} />
          </View>

          <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="nearby" label="Nearby" >
            {this.props.renderConnectionStatus()}
            <NearbyFriends {...this.props} />
          </View>

      </ScrollableTabView>
    );
  }
}
