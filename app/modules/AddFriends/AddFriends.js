'use strict';

import React, { Component } from 'react';
import AddFriendsRender from './AddFriendsRender';
import SoundEffects from './../actions/SoundEffects';

export default class AddFriends extends Component {
  render() {
    return (
      <AddFriendsRender
        onChangeTab={this.onChangeTab.bind(this)}
        {...this.props}
      />
    );
  }

  onChangeTab() {
    SoundEffects.playSound('tick');
  }
}
