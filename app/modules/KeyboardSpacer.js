/**
 * Created by andrewhurst on 10/5/15.
 */
import React, { Component, PropTypes } from 'react';
import {
  InteractionManager,
  Keyboard,
  LayoutAnimation,
  View,
  Platform,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default class KeyboardSpacer extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      keyboardSpace: 0
    };
    this.updateKeyboardSpace = this.updateKeyboardSpace.bind(this);
    this.resetKeyboardSpace = this.resetKeyboardSpace.bind(this);
  }

  componentDidMount() {
    const updateListener = Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow';
    const resetListener = Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide';
    this._listeners = [
      Keyboard.addListener(updateListener, this.updateKeyboardSpace),
      Keyboard.addListener(resetListener, this.resetKeyboardSpace)
    ];
  }

  componentWillUnmount() {
    this._listeners.forEach(listener => listener.remove());
  }

  updateKeyboardSpace(frames) {
    const keyboardSpace = frames.endCoordinates.height;
    this.setState({
      keyboardSpace: keyboardSpace,
    });
  }

  resetKeyboardSpace() {
    this.setState({
      keyboardSpace: 0,
      isViewVisible: false
    });
  }

  render() {
    return (
      <View style={[styles.container, { height: this.state.keyboardSpace }]} />
    );
  }
}
