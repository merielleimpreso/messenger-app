/*

Author: Merielle Impreso
Date Created: 2016-04-19
Date Updated: 2016-05-05
Description: Row display for recent chat
  Passes chatInfo object as this.props.config when clicked.
  For direct chat: chatInfo have the following properties:
    user = logged in user
    otherUser = a specific user
    users = users included in the conversation: [user, otherUser]
  For group chat: chatInfo have the following properties:
    user = logged in user
    groupId = id of the group
    users = users included in the group
Props: An item in the dataSource from Messages or Groups
  data = a chat item from a collection
  data.chatType = GLOBAL.CHAT.DIRECT or GLOBAL.CHAT.GROUP
  data.user = logged in user
Used In: Messages.ios, Groups.ios

Changelog:
update: 2016-04-21 (by Merielle I.)
  - used latest ddp-client
  - added DirectChat and GroupChat for database subscription and observe
update: 2016-04-22 (by Merielle I.)
  - added isMounted flag to fix setState warnings, put it inside the method callback
update: 2016-05-05 (by Ida)
  - Modified UI color scheme
update: 2016-05-06 (by Merielle I.)
  - Check if data is loaded and modify the count in Messages/Groups
update: 2016-05-12 (by Merielle I.)
  - Added message when image or sticker was sent.
update: 2016-05-12 (By Terence)
  -Added condition for Android to ignore onClick if data not yet loaded.
update: 2016-05-18 (By Terence)
  -Fixed Android's Group threads unclickable. Changed if condition.
  -Added Catch to MethodNotFound warning for getUsersGroupById.
update: 2016-05-19 (By Terence)
  -Fixed Group Thread group thread causing crashes when clicked. Changed if condition.
update: 2016-06-01 (by Merielle I.)
  - Added message when a location is shared.
  - Added seen indicator.
update: 2016-06-17 (by Merielle I.)
  - Added
update: 2016-06-28 (by John Rezan B.)
  - Added time
  - Added Seen
update: 2016-07-07 (by Merielle I.)
  - Addded consoleLog from Helpers for performance
update: 2016-07-19 (by Merielle I.)
  - UI for pending invites
update: 2016-07-21 (by Merielle I.)
  - Change ES5 code format to ES6
update: 2016-08-17 (by John Rezan B.)
  - Changed ddp.user to data.user

*/

'use strict';

import _ from 'underscore';
import {consoleLog, dateToTime} from './Helpers';
import DirectChat from './config/db/DirectChat';
import GroupChat from './config/db/GroupChat';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import UserGroups from './config/db/UserGroups';
import React, {
  Component
} from 'react';
import ReactNative, {
  ActivityIndicator,
  AsyncStorage,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

import moment from 'moment';

const styles = StyleSheet.create(require('./../styles/styles_common'));
const GLOBAL = require('./config/Globals.js');

class ChatRecentRenderRow extends Component {

  // Declare initial state and bind functions
  constructor(props) {
    super(props);
    this.state = {
      chatInfo: '',
      image: 'http://www.sessionlogs.com/media/icons/defaultIcon.png',
      isLoading: true,
      message: '',
      name: '',
      loading: true,
      isLoaded: false
    };

    this.getRecentChat = this.getRecentChat.bind(this);
    this.getRecentDirectChat = this.getRecentDirectChat.bind(this);
    this.getRecentGroupChat = this.getRecentGroupChat.bind(this);
    this.renderLoading = this.renderLoading.bind(this);
    this.renderNormal = this.renderNormal.bind(this);
    this.renderPendingInvite = this.renderPendingInvite.bind(this);
    this.onClickedView = this.onClickedView.bind(this);
    this.acceptInvite = this.acceptInvite.bind(this);
  }

  /*
  Get recent direct or group chat.
  */
  componentDidMount() {
    if (GLOBAL.SHOULD_SHOW_LOGS) {
      console.log('[ChatRecentRenderRow] componentDidMount');
    }
    this._isMounted = true;
    requestAnimationFrame(() => {
      if (GLOBAL.SHOULD_SHOW_LOGS) {
        console.log('[ChatRecentRenderRow] componentDidMount: call getRecentChat()');
      }
      this.getRecentChat();
    });
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Observe and get the recent chat.
  getRecentChat() {
    if (GLOBAL.SHOULD_SHOW_LOGS) {
      console.log('[ChatRecentRenderRow] getRecentChat');
      console.log('[ChatRecentRenderRow] getRecentChat: observe directChat');
    }
    let data = this.props.data;
    if (this.props.data.chatType == GLOBAL.CHAT.DIRECT) {
      let user = this.props.data.user;
      var id = (user._id == data.FromUserFID) ? data.ToUserFID : data.FromUserFID;
       if (this._isMounted) {
        this.setState({
          targetId:id
        });
       }

      DirectChat.observeRecent(user, (results) => {
        if (JSON.stringify(results) != JSON.stringify(this.state.chat)) {
          if (this._isMounted) {
            if (GLOBAL.SHOULD_SHOW_LOGS) {
              console.log('[ChatRecentRenderRow] getRecentChat: call getRecentDirectChat()');
            }
            this.getRecentDirectChat();
            this.setState({
              chat: results
            })
          }
        }
      });

    } else {
      GroupChat.observeRecent((results) => {
        if (this._isMounted) {
          this.setState({
            targetId: this.props.data.GroupFID
          });
          this.getRecentGroupChat();
        }
      });
    }
  }

  /*
  Get the recent direct chat items by getting the values of this.props.data from Messages.
  The conversationName for this function is from the getUserById method call.
  */
  getRecentDirectChat() {
    var data = this.props.data;
    var chatType = data.chatType;
    var message = data.Message.Text;
    var user = data.user;
    var id = (user._id == data.FromUserFID) ? data.ToUserFID : data.FromUserFID;
    var isContact = data.isContact;
    var props = this.props;

    var chatInfo = {};
    var callTime = moment();
    if (data.chatInfo) {
      var image = data.chatInfo.otherUser.profile.photoUrl;
      image = (image == '') ? GLOBAL.DEFAULT_IMAGE : image;
      var name = data.chatInfo.otherUser.profile.firstName + " " + data.chatInfo.otherUser.profile.lastName;
      var nameSender = (data.user._id == data.FromUserFID) ? "You" : data.chatInfo.otherUser.profile.firstName;

      if (data.Message.Media && data.Message.Media[0] != null) {
        message = nameSender + " sent an image."
      } else if (data.Message.Sticker) {
        message = nameSender + " sent a sticker."
      } else if (data.Message.Like) {
        message = nameSender + " sent a like."
      } else if (data.Message.Location) {
        message = nameSender + " shared a location."
      } else if (data.Message.Audio) {
        message = nameSender + " sent an audio clip."
      } else if (data.Message.Video) {
        message = nameSender + " sent a video clip."
      } else if (data.Message.Text) {
        if (nameSender == "You") {
          message = nameSender + ": " + message;
        }
      } else if (data.Message.Attachment) {
        message = nameSender + " sent an attachment."
      }
      if (this._isMounted) {
        if (GLOBAL.SHOULD_SHOW_LOGS) {
          var now = moment();
          var diff = (now.diff(callTime) / 1000) + 's';
          console.log('[ChatRecentRenderRow] getRecentDirectChat: server method getUserById responds after ' + diff + ' <' + name + '>');
        }
        this.setState({
          chatInfo: data.chatInfo,
          image: image,
          isLoading: false,
          message: message,
          name: name
        });
        this.props.setIsLoadedRowsCount();
      }
    }
    else
    ddp.call('getUserById', [id])
    .then(result => {
      var image = result.profile.photoUrl;
      image = (image == '') ? GLOBAL.DEFAULT_IMAGE : image;
      var name = result.profile.firstName + " " + result.profile.lastName;
      var nameSender = (data.user._id == data.FromUserFID) ? "You" : result.profile.firstName;

      if (data.Message.Media && data.Message.Media[0] != null) {
        message = nameSender + " sent an image."
      } else if (data.Message.Sticker) {
        message = nameSender + " sent a sticker."
      } else if (data.Message.Like) {
        message = nameSender + " sent a like."
      } else if (data.Message.Location) {
        message = nameSender + " shared a location."
      } else if (data.Message.Audio) {
        message = nameSender + " sent an audio clip."
      } else if (data.Message.Video) {
        message = nameSender + " sent a video clip."
      } else if (data.Message.Text) {
        if (nameSender == "You") {
          message = nameSender + ": " + message;
        }
      } else if (data.Message.Attachment) {
        message = nameSender + " sent an attachment."
      }

      chatInfo["user"] = user;
      chatInfo["otherUser"] = result;
      chatInfo["users"] = [user, result];
      chatInfo["isContact"] = isContact;
      data.chatInfo = chatInfo;
      data.message = message;
      // console.log(data);
      AsyncStorage.getItem('directMessages').then((result) => {
        var chatRecentMessages = [];
        if(result){
          chatRecentMessages = JSON.parse(result);
          //console.log('current array in async: ', chatRecentMessages);
        }
        chatRecentMessages.push(data);
        AsyncStorage.setItem('directMessages', JSON.stringify(chatRecentMessages));
        //console.log('store data to async');
      }).
      catch((error)=>{
        //console.warn("directMessages does not exist in AsyncStorage. Raw Error: "+error);
      });

      if (this._isMounted) {
        if (GLOBAL.SHOULD_SHOW_LOGS) {
          var now = moment();
          var diff = (now.diff(callTime) / 1000) + 's';
          console.log('[ChatRecentRenderRow] getRecentDirectChat: server method getUserById responds after ' + diff + ' <' + name + '>');
        }
        this.setState({
          chatInfo: chatInfo,
          image: image,
          isLoading: false,
          message: message,
          name: name
        });
        this.props.setIsLoadedRowsCount();
      }
    })
    .catch(e => {
      if((e.reason && e.reason == "Disconnected from DDP server") || e.reason == "Disconnected from DDP server")
        props.ddpDisconnected("Tere: DDPCnxn fail. ChatRecentRenderRow, getUserById(). Reloaded. Raw Error: "+error);

      if (GLOBAL.SHOULD_SHOW_LOGS) {
        console.log(error);
      }
    });
  }

  /*
  Get the recent group chat items by getting the values of this.props.data from Groups.
  The conversationName is from the getGroupById server method call.
  Save the users of the group in chatInfo by calling the server method call getUsersByGroupId.
  */
  getRecentGroupChat() {
    var data = this.props.data;
    var chatType = data.chatType;
    var message = data.Message.Text;
    var props = this.props;
    ddp.call('getGroupById', [data.GroupFID])
    .then(result => {
      var name = result.Name;
      data['name'] = name;

      if (this._isMounted) {
        this.setState({ name: name });
      }

      var chatInfo = {};
      chatInfo["groupName"] = name;
      ddp.call('getUsersByGroupId', [data.GroupFID])
      .then(result => {
        chatInfo["user"] = data.user;
        chatInfo["groupId"] = data.GroupFID;
        chatInfo["users"] = result;
        this.props.setIsLoadedRowsCount();

        if (this._isMounted) {
          this.setState({ chatInfo: chatInfo });
        }

        var sender = "";
        for(var x = 0; x < result.length; x++) {
          if (result[x]._id == data.FromUserFID) {
            sender = result[x].profile.firstName;
          }
        }
        var nameSender = (data.user._id == data.FromUserFID) ? "You" : sender;
        if (data.Message.Media && data.Message.Media[0].URL != null) {
          message = nameSender + " sent an image."
        } else if (data.Message.Sticker) {
          message = nameSender + " sent a sticker."
        } else if (data.Message.Like) {
          message = nameSender + " sent a like."
        } else if (data.Message.Location) {
          message = nameSender + " shared a location."
        } else if (data.Message.Audio) {
          message = nameSender + " sent an audio clip."
        } else if (data.Message.Video) {
          message = nameSender + " sent a video clip."
        } else if (data.Message.Text) {
          message = nameSender + ": " + message;
        }
        if (this._isMounted) {
          this.setState({
            message: message,
            isLoading: false
          });
        }
      })
      .catch(error => {
        if((error.reason && error.reason == "Disconnected from DDP server") || error.reason == "Disconnected from DDP server")
          props.ddpDisconnected("Tere: DDPCnxn fail. ChatRecentRenderRow, getUsersByGroupId(). Reloaded. Raw Error: "+error);
        if (GLOBAL.SHOULD_SHOW_LOGS) {
          console.log(error);
        }
      });
    })
    .catch(error => {
      if((error.reason && error.reason == "Disconnected from DDP server") || error.reason == "Disconnected from DDP server")
        props.ddpDisconnected("Tere: DDPCnxn fail. ChatRecentRenderRow, getGroupById(). Reloaded. Raw Error: "+error);
      if (GLOBAL.SHOULD_SHOW_LOGS) {
        console.log(error);
      }
    });
  }

  // Render loading screen.
  renderLoading() {
    var color = this.props.color;
    return (
      <View>
        <View style={styles.chatRecentRow}>
          <Image source={{uri:this.state.image}} style={styles.chatRecentImage}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
            {(this.state.loading && !this.state.isLoaded) 
              ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:50, width:50}}>
                  <ActivityIndicator size="small" color={color.BUTTON} />
                </View>
              : null
            }
          </Image>
          <View style={styles.chatRecentDetailsContainer}>
            <View style={{flexDirection:'row'}}>
                  <Text numberOfLines={1} style={{color:color.TEXT_LIGHT, flex:4, fontSize:12}}> Loading... </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  // Render latest message.
  renderNormal() {
    var data = this.props.data;
    var chatType = data.chatType;
    var user = data.user;
    var textStyle = styles.chatRecentText;
    var color = this.props.color;
    if (chatType == GLOBAL.CHAT.DIRECT) {
      if (data.user._id == data.ToUserFID) {
        if (data.Seen == null || data.Seen == '') {
          textStyle = styles.chatRecentTextNotSeen;
        }
      }
    } else {
      var id = user._id;
      if (data.user._id != data.FromUserFID) {
        var seenUsers = data.Seen;
        var seen = false;
        _.each(_.values(seenUsers), function(s) {
          if (data.user._id == s.userId) {
            seen = true;
          }
        });
        if(!seen) {
          textStyle = styles.chatRecentTextNotSeen;
        }
      }
    }

    var time =  <Text numberOfLines={1} style={[styles.chatRecentText, {color:color.THEME}]}>{dateToTime(data.CreateDate)}</Text>;

    //Check if seen
    var badge = <View/>;

    if (data.FromUserFID === data.user._id) {
      if(data.Seen)
      badge = <Text style={{fontSize:11, color:color.TEXT_LIGHT}}>Seen <KlikFonts name="check" color={color.THEME}  size={11}/></Text>;
    }
    else{
      if(!data.Seen)
      badge =  <Text style={[styles.chatRecentText,{fontSize:11,color:color.TEXT_LIGHT}]}>New</Text>;
    }

    return (
      <TouchableHighlight onPress={() => this.onClickedView()} underlayColor={color.HIGHLIGHT}>
        <View>
          <View style={styles.chatRecentRow}>
            <Image source={{uri:this.state.image}} style={styles.chatRecentImage}
              onLoadStart={(e) => this.setState({loading: true})}
              onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
              onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
              onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
              {(this.state.loading && !this.state.isLoaded) 
                ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:50, width:50}}>
                    <ActivityIndicator size="small" color={color.BUTTON} />
                  </View>
                : null
              }
            </Image>
            <View style={styles.chatRecentDetailsContainer}>
              <View style={{flexDirection:'row'}}>
                    <Text numberOfLines={1} style={[styles.chatRecentTextBold, {color:color.TEXT_DARK, flex:4}]}> {this.state.name} </Text>
                    <View style={{flexDirection:'column', alignItems:'flex-end', flex:1}}>
                      {time}
                    </View>
              </View>

              <View style={{flexDirection:'row'}}>
                    <Text numberOfLines={1} style={[textStyle, {color:color.TEXT_LIGHT,flex:4}]}> {this.state.message} </Text>
                    <View style={{flexDirection:'column', alignItems:'flex-end',flex:1}}>
                      {badge}
                    </View>
              </View>
            </View>
          </View>
        </View>
     </TouchableHighlight>
    );
  }

  // Render invite buttons.
  renderInvite() {
    var color = this.props.color;
    return (
      <View>
        <View style={styles.chatRecentRow}>
          <Image source={{uri: this.state.image}} style={styles.chatRecentImage}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
            {(this.state.loading && !this.state.isLoaded) 
              ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:50, width:50}}>
                  <ActivityIndicator size="small" color={color.BUTTON} />
                </View>
              : null
            }
          </Image>
          <View style={styles.chatRecentDetailsContainer}>
              <Text numberOfLines={1} style={[styles.chatRecentTextBold, {color:color.TEXT_DARK}]} > {this.state.name} wants to be your friend </Text>
              <View style={styles.chatRecentButtonContainer}>
                <TouchableHighlight onPress={() => this.acceptInvite()}
                  style={[styles.chatRecentButton, {backgroundColor: color.BUTTON}]} underlayColor={color.HIGHLIGHT}>
                  <Text style={[styles.chatRecentButtonText, {color: color.BUTTON_TEXT}]}>
                    Accept
                  </Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.ignoreInvite()}
                  style={[styles.chatRecentButton, {backgroundColor: color.BUTTON}]} underlayColor={color.HIGHLIGHT}>
                  <Text style={[styles.chatRecentButtonText, {color: color.BUTTON_TEXT}]}>
                    Ignore
                  </Text>
                </TouchableHighlight>
              </View>
          </View>
        </View>
      </View>
    );
  }

  // Render message showing invite is pending for acceptance
  renderPendingInvite() {
    var color = this.props.color;
    var textStyle = styles.chatRecentText;
    var data = this.props.data;
    var time =  <Text numberOfLines={1} style={[styles.chatRecentText, {color:color.THEME}]}>{dateToTime(data.CreateDate)}</Text>;
    return (
      <TouchableHighlight onPress={() => this.onClickedView()} underlayColor={color.HIGHLIGHT}>
        <View>
          <View style={styles.chatRecentRow}>
            <Image source={{uri:this.state.image}} style={styles.chatRecentImage}
              onLoadStart={(e) => this.setState({loading: true})}
              onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
              onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
              onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
              {(this.state.loading && !this.state.isLoaded) 
                ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:50, width:50}}>
                    <ActivityIndicator size="small" color={color.BUTTON} />
                  </View>
                : null
              }
            </Image>
            <View style={styles.chatRecentDetailsContainer}>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[styles.chatRecentTextBold, {color:color.TEXT_DARK, flex:4}]}> {this.state.name} </Text>
                <View style={{flexDirection:'column', alignItems:'flex-end', flex:1}}>
                  {time}
                </View>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[textStyle, {color:color.BUTTON,flex:4}]}>has been invited</Text>
              </View>
            </View>
          </View>
        </View>
     </TouchableHighlight>
    );
  }

  // Display name, recent message and image
  render() {
    var render = <View />;


    if (this.props.data.chatType == GLOBAL.CHAT.GROUP) {
      render = this.renderNormal();
    } else {
      if (this.props.data.isContact) {
        render = this.renderNormal();
      } else {
        if (this.props.data.FromUserFID == this.props.data.user._id) {
          render = this.renderPendingInvite();
        } else {
          render = this.renderInvite();
        }
      }
    }


    return render;
 }

 setIsLoadingRecentRow() {
   if (this._isMounted) {
     this.setState({isLoading: true});
   }
 }

 // Redirect to MessageDirect or GroupMessages
 onClickedView() {
   if (this.props.data.chatType == GLOBAL.CHAT.DIRECT) {
     if (Platform.OS === 'ios') {
       console.log(this.state.targetId);
       requestAnimationFrame(() => {
         this.props.navigator.push({
           id:'messagedirect',
           title: 'Messenger',
           navigationBarHidden:false,
           config: this.state.chatInfo,
           color: this.props.color,
           targetId: this.state.targetId,
           setIsLoadingRecentRow: this.setIsLoadingRecentRow
         });
       });
     } else {
       if(this.state.name!='')
       {
         requestAnimationFrame(() => {
           this.props.navigator.push({
             name: 'MessageDirect',
             config: this.state.chatInfo,
             color: this.props.color,
             ddpDisconnected: this.props.ddpDisconnected

           });
         });

       }

     }
   } else {
     if (Platform.OS === 'ios') {
       requestAnimationFrame(() => {
         this.props.navigator.push({
           id:'groupmessages',
           title: 'Messenger',
           navigationBarHidden:false,
           config: this.state.chatInfo,
           color: this.props.color,
           targetId: this.state.targetId
         });
       });
     } else {


       if(this.state.chatInfo.users!= null)
       {
         requestAnimationFrame(() => {
           this.props.navigator.push({
             name: 'GroupMessages',
             config: this.state.chatInfo,
             color: this.props.color,
             targetId: this.state.targetId,
             ddpDisconnected: this.props.ddpDisconnected,
             group: this.props.group,

           });
         });

        }
     }
   }
 }

 // Accept invite, add to contacts
 acceptInvite() {
   //Use this when method in server is ready
   var props = this.props;
   ddp.call("acceptInvitation", [this.props.data.FromUserFID])
   .then(()=> {
     if (GLOBAL.SHOULD_SHOW_LOGS) {
       console.log("acceptInvitation: successful");
     }
   })
   .catch(function(error) {
     if((error.reason && error.reason == "Disconnected from DDP server") || error.reason == "Disconnected from DDP server")
      props.ddpDisconnected("Tere: DDPCnxn fail. ChatRecentRenderRow, acceptInvite(). Reloaded. Raw Error: "+error);
     if (GLOBAL.SHOULD_SHOW_LOGS) {
       console.log(e);
     }
   });
 }

 // Ignore invite
  ignoreInvite() {
    ddp.call("isIgnored", [this.props.data.FromUserFID])
    .then(() => {
      console.log('ignored')
    })
    .catch((err) => {
      console.log('isIgnored Error: ', err)
    });

  }

}

// Export module
module.exports = ChatRecentRenderRow;
