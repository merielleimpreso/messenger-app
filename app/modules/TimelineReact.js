'use strict';

import React, { Component } from 'react';

import {
  Dimensions,
  Image,
  Platform,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';
var Helpers = require('./Helpers.js');
var GLOBAL = require('./config/Globals.js');
var styles = StyleSheet.create(require('./../styles/styles.main'));
var windowSize = Dimensions.get('window');
import Navigation from './actions/Navigation';

class TimelineReact extends Component {
  constructor(props) {
    super(props);
  }

  reactOnPress(reaction) {
    if (!this.props.hasInternet()) {
      this.props.closeReact();
    } else {
      ddp.call('reactionOnPost', [this.props.postId, reaction]).then((result) => {
        console.log(result);
      });
      this.props.closeReact();
    }
  }

  render(){
    let color = this.props.color;
    let staticDecrement = (Platform.OS === 'ios') ? 85 : ((this.props.isOnComment) ? 12 : 175);
    let reactBubbleDirection = (this.props.isOnComment) ? {borderBottomWidth: 8,borderBottomColor: color.THEME} : {borderTopWidth: 8,borderTopColor: color.THEME};
    let reactionStyle = [reactBubbleDirection, {
      width: 0,
      height: 0,
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderLeftWidth: 8,
      borderLeftColor: 'transparent'
    }];
    let reactTopPosition = this.props.reactTopPosition - staticDecrement;
    let reactTrianglePosition = (this.props.isOnComment) ? {borderTopRightRadius:0} : {borderBottomRightRadius:0};
    return (
      <View style={{position:'absolute', top:reactTopPosition, alignItems: 'flex-end', right: 60}}>
        {(this.props.isOnComment) ? <View style={reactionStyle}/> : null}
        <View style={[reactTrianglePosition, {padding:5, flexDirection:'row', backgroundColor:color.THEME, borderRadius:3}]}>
           <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':love'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/love.png')}/>
           </TouchableOpacity>

           <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':happy'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/happy.png')}/>
           </TouchableOpacity>

            <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':confused'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/confused.png')}/>
           </TouchableOpacity>

            <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':supportive'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/supportive.png')}/>
           </TouchableOpacity>

            <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':blank'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/blank.png')}/>
           </TouchableOpacity>

            <TouchableOpacity onPress={() => Navigation.onPress(() => this.reactOnPress(':sad'))}>
              <Image style={styles.timelineProfile} source={require('./../images/reactions/sad.png')}/>
           </TouchableOpacity>
        </View>
        {(this.props.isOnComment) ? null : <View style={reactionStyle}/>}
      </View>
    )
  }
}
// Export module
module.exports = TimelineReact;
