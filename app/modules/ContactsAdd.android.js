/**
Author:           Terence John Lampasa
Date Created:     2016-04-29
Date Updated:     2016-05-19
Descrption:       This file is used to add new Contact, via new message.
Return:
File Dependency:  ddp.js, ContactRetriever.js, Helpers.js, Globals.js

Changelog:

update: 2016-05-18 (By Terence)
  -Fixed warning when no match is found for typed name
  -Modified to display NoContacts Found when no matches found
  -Utilized filter to show which search item was there no results found.
update: 2016-05-19 (by Ida)
  - added keyboardShouldPersistTaps={true} in <ListView />
update: 2016-06-22 (ida)
  - Implement color themes
update: 2016-06-27 (ida)
  - changed icons to klikfonts
update: 2016-07-12 (tere)
  -Fixed loading/searching/no contacts messages to display correctly.

*/
'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Image,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TextInput,
  View
} from 'react-native';
import {jsonToArray, toNameCase, toPhoneNumber, toLowerCase} from './Helpers.js';
import ContactRetriever from './ContactRetriever';
import ddp from './config/ddp';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
var ProgressBar = require('ActivityIndicator');

const GLOBAL = require('./../modules/config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));

var AddContact = React.createClass({

  // Set initial state
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      rawDataSource: [],
      text:"",
      currentUser: '',
      userImage: '',
      isLoading: true,
      filter:'',
      loading: true,
      isLoaded: false
    }
  },

  // Bind data to ListView
  getDataSource: function(contacts: Array<any>): ListView.DataSource {
    return this.state.dataSource.cloneWithRows(contacts);
  },

  //get the data before rendering
  componentDidMount: function(){
    this.isMounted = true;
    this.getContacts();
    this.setState({isLoading: true});
    ddp.call('currentUser', []).then((result) => {
      this.setState({currentUser: result, isLoading: false});
    });
  },

  // Put flag if component is unmounted.
  componentWillUnmount: function() {
    this.isMounted = false;
  },

  // get contact that are already registered
  getContacts: function(){
    if (this.isMounted) {

    this.filterWithSearchText("");


    }
  },

  //render each contact
  renderContacts: function(contact){
    let color = this.props.color;
      return (
        <TouchableHighlight
          onPress={() => this.getUserDetails(contact)}
          underlayColor={color.HIGHLIGHT}>
          <View style={styles.li}>

            {this.renderUserProfile(contact)}

            <View style={[styles.rightContainer, {paddingLeft: 10}]}>
              <Text style={[styles.textBold, {color: color.TEXT_DARK}]} >{contact.profile.firstName} {contact.profile.lastName}</Text>
            </View>

          </View>
        </TouchableHighlight>
      );
    //<Text style={styles.text} >{toPhoneNumber(contact.phoneNumbers[0])}</Text>
  },

  // Set photo of the user
  renderUserProfile: function(contact) {
    let color = this.props.color;
    if(contact.profile.photoUrl) {
      return (
        <Image style={styles.thumbnail} source={{uri: contact.profile.photoUrl}}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded) 
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:50, width:50}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
        </Image>
      );
    } else {
      return(<KlikFonts name="profile" style={[styles.doneIcon, {margin: 8, color: color.TEXT_LIGHT}]}/>);
    }
  },

  // Display
  render: function() {
    let color = this.props.color;
    var content;
      if(this.state.dataSource.getRowCount() === 0 && !this.state.isLoading) {
        content = <NoContacts
                    filter={this.state.filter}
                    isLoading={this.state.isLoading}
                    color={color}
                  />;
      }
      else if(this.state.isLoading)
      {
        var searchText;

        if(this.state.filter == "")
        {
          searchText = "Retrieving contact list.";
        }
        else {
          searchText= "Searching for \""+this.state.filter+"\"";
        }
        content = <LoadBar
                  color={color}
                  text={searchText}/>

      }
      else {
        content = <ListView
                    ref="listview"
                    keyboardShouldPersistTaps={true}
                    style={styles.listView}
                    dataSource={this.state.dataSource}
                    renderRow={this.renderContacts}
                    onEndReached={this.onEndReached}
                  />;
      }
    return (
      <View style={styles.container}>

        <View style={[styles.topContainer, {backgroundColor: color.THEME}]}>
          <TouchableHighlight
            onPress={() => this.onBackPress()}
            underlayColor={color.HIGHLIGHT}
            style={{padding: 8}}>
            <KlikFonts name='back' style={{color: color.BUTTON_TEXT, fontSize: 25}}/>
          </TouchableHighlight>

          <View style={styles.typeContainer}>
            <TextInput
              placeholder='Type a name or group'
              placeholderTextColor={color.BUTTON_TEXT}
              style={[styles.typeName, {color: color.BUTTON_TEXT}]}
              onChangeText={(text) => this.filterWithSearchText(text)}
              autoFocus={true} />
          </View>
        </View>

        <View style={{flex: 10, backgroundColor: color.CHAT_BG}}>
          {content}
        </View>
      </View>
    );
  },

  getUserDetails: function(contact) {
    var chatInfo = {};
    var data = this.props.data;
    var chatType = data.chatType;
    var user = data.user;

    ddp.call('getUserById', [contact._id])
      .then(result => {

        chatInfo["user"] = user;
        chatInfo["otherUser"] = result;
        chatInfo["users"] = [user, result];
        chatInfo["isContact"] = false;
        this.goToChat(chatInfo);

    });
  },

  // navigates to MessageDirect
  goToChat: function(chatInfo) {
    console.log(chatInfo);
    this.props.navigator.replace({
        name: 'MessageDirect',
        config: chatInfo,
        color: this.props.color
    });
  },

  // go back to previous screen
  onBackPress: function() {
    //console.log('pressed back');
    this.props.navigator.pop();
  },

  // Search contacts
  filterWithSearchText: function(text) {
    var contact;
    //var tempRawDataSource = this.state.rawDataSource.slice();

    var tempStr = text.replace(/\W\D*/g, "");
      this.setState({isLoading: true, filter: text});
      ddp.call('searchUser', [text])
      .then(result => {
        if(this.isMounted)
        {
            this.setState({dataSource: this.getDataSource(result), filter: text, isLoading: false})
        }

          console.log("dataSource");
          console.log(this.state.dataSource);

      }).catch(function(e) {
        console.warn("ContactsAdd searchUser Fail");
        if(this.isMounted)
        {
          this.setState({dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
          }), filter: text})
        }


      }.bind(this));






  //  this.setState({text: text, dataSource: this.getDataSource(tempRawDataSource._65)});

  }
});

// Display for empty records
var NoContacts = React.createClass({
  render: function() {
    let color = this.props.color;
    var text = '';
    if (this.props.filter) {
      text = `No results for "${this.props.filter}"`;
    } else if (!this.props.isLoading) {

      text = 'No contacts found';
    }

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={{marginTop: 20, color: color.TEXT_LIGHT}}>{text}</Text>
      </View>
    );
  }
});

var LoadBar = React.createClass({
  render: function() {
    let color = this.props.color;
    var text = this.props.text;
    var progressBar =
      <View>
        <View style={{justifyContent: 'center'}}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
        </View>
        <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
      </View>;
    return (
      <View style={styles.loaderContainer}>

          <View style={{justifyContent: 'center'}}>
            <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
          </View>
          <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
      </View>
    );
  }
});

module.exports = AddContact;
