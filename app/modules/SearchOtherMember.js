/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-30
 * Date Updated: N/A
 * Description: Component used for searching other member
 */
'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  Text,
  View,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  ActivityIndicator,
  Picker,
  Platform,
  Image
} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import _ from 'underscore';
import {getDisplayName, getMediaURL, toNameCase} from './Helpers';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import country from './../modules/config/db/Countries';
import ImageWithLoader from './common/ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';
import ScreenNoItemFound from './ScreenNoItemFound';
import Send from './config/db/Send';
import SoundEffects from './actions/SoundEffects';
import SystemAccessor from './SystemAccessor';
import Users from './config/db/Users';

const GLOBAL = require('./config/Globals.js');
const SCREEN = Dimensions.get("window");
const styles = StyleSheet.create(require('./../styles/styles.main'));

class SearchOtherMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false,
      isSearching: false,      
      isShowMessageModal: false,
      newContactSearchOptionIndex: 0,
      noResultSearchMember: false,
      searchedMember: [],
      searchText: '',
      searchType: 'name',
      selectedCode: '+62',
      selectedCountry: 'Indonesia'
    }
    this.searchOtherMember = this.searchOtherMember.bind(this);
    this.inviteSearchedMember = this.inviteSearchedMember.bind(this);
    this.renderResultButton = this.renderResultButton.bind(this);
    this.setSelectedCountry = this.setSelectedCountry.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowContactProfile = this. setIsShowContactProfile.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillMount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let searchedMember = this.state.searchedMember;
    let countryList = country.country;
    let content = <View />;
    let searchPlaceholder = (this.state.searchType === 'name') ? "Enter your friend's username" : "Enter your friend's mobile number";
    let picker = this.renderPicker();
    let searchType = (this.state.searchType === 'name') ? 'username' : 'mobile number';
    let keyboardType = (this.state.searchType === 'name') ? 'default' : 'phone-pad';
    let radio_props = [{label: 'Username', value: 'name' }, {label: 'Mobile Number', value: 'mobile' }];

    if (_.isEmpty(this.state.searchedMember)) {
      if (this.state.noResultSearchMember) {
        content = this.renderUserNotFound();
      } else if (this.state.isSearching) {
        content = this.renderSearching();
      }  else {
        content = this.renderSearchButton();
      }
    } else {
      if (this.state.searchText == null || this.state.searchText == '') {
        content = this.renderSearchButton();
      } else {
        content = this.renderResult();
      }
    }

    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderTopBar()}

        <RadioForm
          formHorizontal={true}
          animation={true}
          initial={0}
          style={{paddingTop: Platform.OS === 'ios' ? 15 : 10, paddingLeft: 10}}>
          {radio_props.map((obj, i) => {
            var is_selected = this.state.newContactSearchOptionIndex == i;
            return (
              <View key={i}>
                <RadioButton
                  isSelected={is_selected}
                  obj={obj}
                  index={i}
                  labelHorizontal={true}
                  buttonSize={16}
                  buttonOuterSize={16}
                  borderWidth={1.5}
                  buttonColor={color.INACTIVE_BUTTON}
                  labelColor={color.TEXT_DARK}
                  labelStyle={{paddingLeft:0}}
                  buttonWrapStyle={{marginRight: 5}}
                  style={{marginRight:25}}
                  onPress={(value, index) => {
                    SoundEffects.playSound('tick');
                    this.setState({
                      newContactSearchOptionIndex: index,
                      searchType: value,
                      noResultSearchMember: false,
                      searchText: '',
                      searchedMember: []
                    });
                  }}
                />
              </View>
            )
          })}
        </RadioForm>
        <View style={{marginTop: 10}}>
          {picker}
          <View style={[styles.searchBoxContainer, {backgroundColor: color.HIGHLIGHT, width: SCREEN.width - 20, marginLeft: 10, marginRight: 10, flexDirection: 'row', alignItems: 'center'}]}>
            <TextInput ref="inputSearch"
              value={this.state.searchText}
              onSubmitEditing={(event) => this.searchOtherMember()}
              placeholder={searchPlaceholder}
              placeholderTextColor={color.TEXT_LIGHT}
              onChangeText={(text) => this.onChangeSearchText(text)}
              onFocus={() => SoundEffects.playSound('tick')}
              keyboardType={keyboardType}
              style={[styles.input, {backgroundColor: 'transparent', textAlign: 'left', flex: 1}]}
              autoCapitalize={'none'} />
            {this.renderClose()}
          </View>
          {content}
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderTopBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <Text style={[styles.pageTitle, {color: color.TEXT_LIGHT}]}>Search by ID / Phone Number</Text>;
  }

  renderModal() {
    if (this.state.isShowContactProfile) {
      return (
        <RenderModal
          theme={this.props.theme}
          profileData={this.state.contact}
          setIsShowModal={() => this.setIsShowContactProfile(null)}
          goToGroupDetails={this.goToGroupDetails}
          modalType={'viewProfile'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    }
  }

  renderPicker() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.searchType === 'name') {
      return <View />;
    } else {
      return (
        <TouchableHighlight onPress={() => {this.onPressCountry(); SoundEffects.playSound('tick')}} underlayColor={color.HIGHLIGHT}
          style={[styles.searchBoxContainer, {backgroundColor: color.HIGHLIGHT, width: SCREEN.width - 20, marginLeft: 10, marginRight: 10}]}>
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK, textAlignVertical: 'center'}]} >
              {this.state.selectedCountry}
            </Text>
            <Text style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER, textAlignVertical: 'center'}]} >
              {this.state.selectedCode}
            </Text>
          </View>
        </TouchableHighlight>
      );
    }
  }

  renderClose() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.searchText) {
      return (
        <View style={{borderRadius: 50, width: 15, height: 15, backgroundColor: color.TEXT_LIGHT, margin: 7, justifyContent: 'center'}}>
          <KlikFonts name="close" style={{borderRadius: 10, fontSize: 15, color: '#fff', alignSelf: 'center', backgroundColor: 'transparent'}}
            onPress={() => this.clearSearchText()}
          />
        </View>
      );
    }
  }

  renderUserNotFound() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{margin: 10, flexDirection: 'row', alignItems: 'center'}}>
        <ScreenNoItemFound isLoading={false} text={`User not found.`} color={color} />
      </View>
    );
  }

  renderSearching() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{margin: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator styleAttr="Inverse" color={color.THEME} />
        <Text style={{color: color.TEXT_LIGHT}}>Searching...</Text>
      </View>
    );
  }

  renderSearchButton() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Button text='Search'
       color={color.THEME}
       underlayColor={color.HIGHLIGHT}
       onPress={() => {this.searchOtherMember()}}
       style={{alignSelf:'center', marginTop:30, width: SCREEN.width*0.2}}
       isDefaultWidth={false}/>
    );
  }

  renderResult() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let searchedMember = this.state.searchedMember;
    let width = 100;
    return (
      <View style={{margin: 10, alignItems: 'center'}}>
        <ImageWithLoader image={getMediaURL(searchedMember.user.image)} color={color} style={{width:width, height:width, borderRadius: 50}} />

        <View style={{margin: 10, flex: 1, alignItems: 'center'}}>
          <Text style={{fontWeight: '700', color: color.TEXT_DARK}}>{searchedMember.user.name}</Text>
          <Text style={{color: color.TEXT_LIGHT}}>{searchedMember.user.statusMessage}</Text>
          <Text style={{color: color.TEXT_LIGHT}}>{searchedMember.action.description}</Text>
        </View>
        {this.renderResultButton()}
      </View>
    );
  }

  renderResultButton() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let user = this.state.searchedMember.user;
    let searchAction = this.state.searchedMember.action;
    let isContact = (searchAction.description == 'Already your friend');
    let hasAlreadySentFR = (searchAction.description == 'Friend request sent');
    let hasAlreadyReceivedFR = (searchAction.description == 'Respond to friend request');
    let action;

    if (isContact || hasAlreadySentFR) {
      action = () => this.goToMessageChat(user);
    } else if (user._id == this.props.loggedInUser()._id) {
      action = null;
    } else {
      if (hasAlreadyReceivedFR) {
        action = () => this.goToMessageChat(user);
      } else {
        action = () => this.setIsShowContactProfile(user);
      }
    }

    return (
      <Button text={searchAction.name}
        color={color.THEME}
        underlayColor={color.HIGHLIGHT}
        onPress={action}
        style={{width: SCREEN.width * 0.3}}
        isDefaultWidth={false}
      />
    )
  }

  onPressCountry() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'inputcountrycode',
        name:'InputCountryCode',
        theme: this.props.theme,
        selectedCountry: this.state.selectedCountry,
        setSelectedCountry: this.setSelectedCountry
      });
    });
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setSelectedCountry(country) {
    this.setState({
      selectedCountry: country.name,
      selectedCode: country.dial_code
    });
  }

  setIsShowContactProfile(contact) {
    let profileData = null;
    if (contact) {
      let actions = [{icon: 'close', iconLabel: 'Cancel', onPressIcon: () => this.setIsShowContactProfile(null)},
                    {icon: 'check', iconLabel: 'Send Request', onPressIcon: () => this.inviteSearchedMember(contact._id)}];

      profileData = {
        data: contact,
        actions: actions,
        relStatus: null
      }
    }

    if (this._isMounted) {
      this.setState({
        isShowContactProfile: !this.state.isShowContactProfile,
        contact: profileData
      });
    }
  }

  inviteSearchedMember(contactId) {
    this.setIsShowContactProfile(null);
    if (this.props.hasInternet()) {
      ddp.call('addFriendRequest', [contactId]).then(result => {
        // console.log(result);
        if (this._isMounted) {
          this.setState({
            searchedMember: result
          })
        }
      }).catch(err => {
        console.log('SearchOtherMember [addFriendRequest] Error:', err);
        this.setIsShowMessageModal('Something went wrong, please try again later.');
      });
    } else {
      this.setIsShowCheckConnection();      
    }
  }

  goToMessageChat(contact) {
    // requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'messagesgiftedchat',
        name:'MessagesGiftedChat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        group: null,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        changeBgImage: this.props.changeBgImage,
        timeFormat: this.props.timeFormat,
        user: contact,
        users: null,
        renderBackgroundImage: this.props.renderBackgroundImage
      });
      this.clearResult();
      this.setState({searchText: ''});
    // })
  }

  searchOtherMember() {
    let searchText;
    this.refs.inputSearch.blur();

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else if (!(_.isEmpty(this.state.searchText))) {
      if (this._isMounted) {
        this.setState({isSearching: true})
      }
      if (this.state.searchType == 'mobile') {
        let countryCode = this.state.selectedCode.replace('+', '');
        searchText = countryCode + this.state.searchText;
      } else {
        searchText = this.state.searchText.toLowerCase();
      }

      // ddp.call('searchUser', [searchText, this.state.searchType]).then((contacts) => {
        ddp.call('searchForUser', [this.state.searchType, searchText]).then(contact => {
        // console.log("contact: ",contact);
        //let contacts = Users.searchUser(searchText, this.state.searchType, this.props.loggedInUser());
          if (_.isEmpty(contact)) {
            if (this._isMounted) {
              this.setState({
                searchedMember: contact,
                noResultSearchMember: true,
                isSearching: false
              })
            }
          } else {
            if (this._isMounted) {
              this.setState({
                searchedMember: contact,
                noResultSearchMember: false,
                isSearching: false
              })
            }
          }
      }).catch((error) => {
        this.setState({
          noResultSearchMember: true,
          isSearching: false
        });
      });
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  onChangeSearchText(text) {
    if (!this.state.isSearching) {
      this.setState({searchText: text});
      this.clearResult();
    }
  }

  clearResult() {
    if (this._isMounted) {
      this.setState({
        searchedMember: [],
        isSearching: false,
        noResultSearchMember: false,
      })
    }
  }

  clearSearchText() {
    if (this._isMounted) {
      this.setState({
        searchedMember: [],
        isSearching: false,
        searchText: '',
        noResultSearchMember: false,
      })
    }
  }

}

module.exports = SearchOtherMember;
