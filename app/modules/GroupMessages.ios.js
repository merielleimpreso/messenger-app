'use strict';

import React, {
  Component,
} from 'react';
import ReactNative, {
  Alert,
  Dimensions,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {logTime} from './Helpers';
import Chat from './Chat';
import GroupChat from './config/db/GroupChat';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
import TypingArea from './TypingArea';

const styles = StyleSheet.create(require('./../styles/styles_common'));
const GLOBAL = require('./config/Globals.js');
const ADD_TO_LIMIT = 10;
const NAVBAR_HEIGHT = 64;
const SCREEN_SIZE = Dimensions.get('window');

class GroupMessages extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      connectionStatusHeight: 0,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isFirstLoad: true,
      isLoading: true,
      isViewVisible: true,
      limit: 10,
      messages: null,
      typeAreaHeight: 0,
      shouldReloadData: this.props.shouldReloadData(),
    }
    this.renderChat = this.renderChat.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.onPressedBack = this.onPressedBack.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  // Re-subscribe if limit or shouldReloadData values are updated
  componentDidUpdate(prevProps, prevState) {
    if (this.state.limit != prevState.limit) {
      InteractionManager.runAfterInteractions(() => {
        this.subscribe();
      });
    }

    if (this.props.shouldReloadData() != this.state.shouldReloadData) {
      if (this._isMounted) {
        this.setState({
          shouldReloadData: this.props.shouldReloadData()
        })
      }
      if (this.props.shouldReloadData()) {
        InteractionManager.runAfterInteractions(() => {
          console.log('GroupMessages ['+ logTime() +']: Reloading...');
          this.subscribe();
          if (this.state.messages) {
            if (this._isMounted) {
              this.setState({
                limit: this.state.messages.length + ADD_TO_LIMIT
              });
            }
          }
        })
      }
    }

    if (prevState.messages != this.state.messages) {
      if (this.state.isFirstLoad) {
        this.state.isFirstLoad = false;
        if (this.state.limit < this.state.messages.length) {
          this.setState({
            isFirstLoad: false,
            limit: this.state.messages.length
          });
        }
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  subscribe() {
    console.log('GroupChat ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    if (this.props.hasInternet()) {
      GroupChat.subscribeByGroup(this.props.group._id, this.state.limit).then(() => {
        this.observe();
      });
    } else {
      this.observe();
    }
  }

  observe() {
    GroupChat.observeByGroup(this.props.group._id, (messages) => {
      GroupChat.checkTempAndDeleteSentFiles(messages, (tempMsgs, newMessages) => {
        if(JSON.stringify(newMessages) != JSON.stringify(this.state.messages)) {
          if (this._isMounted) {
            console.log('GroupChat ['+ logTime() +']: Loaded ' + newMessages.length + ' groupchat item/s');
            this.updateSeen(newMessages);
            this.setState({
              dataSource: this.state.dataSource.cloneWithRows(newMessages),
              isLoading: false,
              messages: newMessages
            });
          }
          if (tempMsgs.length > 0) {
            console.log('GroupChat ['+ logTime() +']: To be deleted ' + tempMsgs.length + ' item/s from temporary groupchat');
          }
        }
      });
    });
  }

  // Update seen property
  updateSeen(messages) {
    for (var i = 0; i < messages.length; i++) {
      var chat = messages[i];
      Send.updateSeenGroup(chat, this.props.loggedInUser()._id);
    }
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={styles.containerNoResults}>
        <NavigationBar
          tintColor={color.THEME}
          title={{ title: this.props.group.Name, tintColor: color.BUTTON_TEXT }}
          leftButton={
            <KlikFonts
            name="back"
            color={color.BUTTON_TEXT}
            size={35}
            onPress={() => this.onPressedBack()}
           />
          }
           rightButton={
             <KlikFonts name='theme'
              color={color.BUTTON_TEXT}
              size={35}
              onPress={() => this.onPressButtonTheme()}
             />
           }
        />
        <View onLayout={(event) => {this.setState({connectionStatusHeight: event.nativeEvent.layout.height});}}>
          {this.props.renderConnectionStatus()}
        </View>
        {this.renderChat()}
        {this.renderTypingArea()}
      </View>
    );
  }

  renderChat() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var chatHeight = SCREEN_SIZE.height - this.state.typeAreaHeight - NAVBAR_HEIGHT - this.state.connectionStatusHeight;
    if (chatHeight < 165) {
      chatHeight = 165;
    }
    return (
      <Chat height={chatHeight}
       navigator={this.props.navigator}
       loggedInUser={this.props.loggedInUser}
       users={this.props.users}
       chatType={this.props.chatType}
       color={color}
       dataSource={this.state.dataSource}
       isLoading={this.state.isLoading}
       isAllLoaded={this.state.isAllLoaded}
       loadMore={this.loadMore}
       hasInternet={this.props.hasInternet}
       theme={this.props.theme}
       bgImage={this.props.bgImage}
      />
    );
  }

  renderTypingArea() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View onLayout={(event) => {
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <TypingArea navigator={navigator}
         color={color}
         isViewVisible={this.state.isViewVisible}
         loggedInUser={this.props.loggedInUser}
         recipientId={this.props.group._id}
         chatType={GLOBAL.CHAT.GROUP}
        />
      </View>
    )
  }

  // Check if there are recent messages need to be loaded.
  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('GroupMessages ['+ logTime() +']: Checking if there are more messages to load...');
          ddp.call('shouldLoadGroupChat', [this.props.group._id, this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('GroupMessages ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              console.log(error);
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  // Go back
  onPressedBack() {
    if (this._isMounted) {
      this.setState({
        isViewVisible: false
      });
    }
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPressButtonTheme() {
    Alert.alert(
      'Set Background Image',
      null,
      [{text: 'Clear Background', onPress: () => this.clearBackground()},
       {text: 'Choose Image from Gallery', onPress: () => this.props.changeBgImage()},
       {text: 'CANCEL', onPress: () => {}}]
    )
  }

  clearBackground() {
    this.props.setImage(null);
  }

}

// Export module.
module.exports = GroupMessages;
