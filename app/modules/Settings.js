/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-06
 * Date Updated: N/A
 * Description: Component used for Settings
 */
'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Image,
  ListView,
  Platform,
  StyleSheet,
  ToolbarAndroid,
  TouchableHighlight,
  Text,
  View,
} from 'react-native';
import {logTime} from './Helpers';
import ButtonIcon from './common/ButtonIcon';
import NavigationBar from 'react-native-navbar';
import Users from './config/db/Users';
import Message from './config/db/Message';
import Post from './config/db/Post';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './common/ListRow';
import RenderModal from './common/RenderModal';
import RNRestart from 'react-native-restart';

const ImagePickerManager = require('NativeModules').ImagePickerManager;
const GLOBAL = require('./config/Globals.js');
const menuSettings = [{name: 'Change Password'}, {name: 'Chat Wallpaper'}, {name: 'Data Usage'}, {name: 'Sign Out'}];

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalSignOutVisible: false,
      isShowCheckConnection: false,
      isLoggingOut: false,
      backgroundImageSrc: '',
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    }
    this.renderModal = this.renderModal.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setModalSignOutVisible = this.setModalSignOutVisible.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(menuSettings)
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderTopBar()}
        {this.props.renderConnectionStatus()}
        <ListView ref="listview" dataSource={this.state.dataSource} renderRow={this.renderRow} />
        {this.renderModal()}
      </View>
    );
  }

  renderTopBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (Platform.OS == 'ios') {
      return (
        <NavigationBar tintColor={color.THEME}
         title={{title: 'Settings', tintColor: color.BUTTON_TEXT}}
         rightButton={<ButtonIcon icon='close' color={color} onPress={() => this.onPressButtonClose()}/>} />
      );
    } else {
      return (
        <KlikFonts.ToolbarAndroid
         elevation={5}
         title={'Settings'}
         titleColor={color.BUTTON_TEXT}
         style={[styles.topContainer, {backgroundColor: color.THEME}]} />
      );
    }
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <ListRow onPress={() => this.onPress(data)} labelText={data.name} color={color} addSeparator={true}/>;
  }

  renderModal() {
    if (this.state.modalSignOutVisible) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setModalSignOutVisible}
          onPress={this.signOut}
          modalType={'signOutOptions'}
        />
      );
    } else if (this.state.isLoggingOut) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loggingOut'}
        />
      )
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPress(data) {
    switch (data.name) {
      case 'Change Password':
        return this.goToChangePassword();
      case 'Chat Wallpaper':
        return this.goToChangeWallpaper();
      case 'Sign Out':
        return this.setModalSignOutVisible(true);
      case 'Data Usage':
        return this.goToPhotosAndMedia();
    }
  }

  goToChangeWallpaper() {
    this.props.navigator.push({
      name: 'ChangeWallpaper',
      id: 'changewallpaper',
      theme: this.props.theme,
      getMessageWallpaper: this.props.getMessageWallpaper,
      renderConnectionStatus: this.props.renderConnectionStatus
    });
  }

  goToChangePassword() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.props.navigator.push({
        name: 'ChangePassword',
        id: 'changepassword',
        theme: this.props.theme,
        hasInternet: this.props.hasInternet,
        renderConnectionStatus: this.props.renderConnectionStatus
      });
    }
  }

  goToPhotosAndMedia() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.props.navigator.push({
        name: 'DataUsage',
        id: 'datausage',
        theme: this.props.theme,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setIsAutoSaveMedia: this.props.setIsAutoSaveMedia
      });
    }
  }

  setModalSignOutVisible(visible) {
    if (this._isMounted) {
      this.setState({modalSignOutVisible: visible});
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  signOut() {
    let theme = this.props.theme();
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this._isMounted) {
      this.setState({modalSignOutVisible: false});
    }

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (this._isMounted) {
        this.props.setIsLoggingOut(true);
        this.setState({
          isLoggingOut: true
        });
      }
      console.log('Settings ['+ logTime() +']: Logging out...');
      requestAnimationFrame(() => {
        ddp.logout((err, res) => {
          // console.log("Logout result: ", res);
          // console.log("Logout err: ", err);
          this.props.logout();
          ddp.isConnected = false;
          ddp.isLoggedIn = false;
          
          if (this._isMounted) {
            this.setState({isLoggingOut: false});
          }
          setTimeout(()=>{
            RNRestart.Restart();
          }, 500);

          // this.props.navigator.resetTo({
          //   name: 'KliklabsPortal',
          //   id: 'kliklabsportal',
          //   addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          //   backgroundImageSrc: this.props.backgroundImageSrc,
          //   debugText: this.props.debugText,
          //   changeTheme: this.props.changeTheme,
          //   getMessageWallpaper: this.props.getMessageWallpaper,
          //   hasInternet: this.props.hasInternet,
          //   isDebug: this.props.isDebug,
          //   isSafeToBack: this.props.isSafeToBack,
          //   loggedInUser: this.props.loggedInUser,
          //   logout: this.props.logout,
          //   renderConnectionStatus: this.props.renderConnectionStatus,
          //   setIsSafeToBack: this.props.setIsSafeToBack,
          //   setLoggedInUser: this.props.setLoggedInUser,
          //   setSignUpCallback: this.props.setSignUpCallback,
          //   shouldReloadData: this.props.shouldReloadData,
          //   subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          //   theme: this.props.theme,
          //   timeFormat: this.props.timeFormat,
          //   checkLoginToken: this.props.checkLoginToken
          // });
        });
      });
    }
  }
};

let styles = StyleSheet.create({
  topContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonUpload: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
    borderRadius: 5,
    margin: 5,
  },
  menu: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    position: 'relative',
    borderBottomWidth: 1,
  }
});

module.exports = Settings;
