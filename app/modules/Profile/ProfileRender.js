'use strict'

import React, { Component } from 'react';

import {
  ActivityIndicator,
  AsyncStorage,
  Image,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions,
  BackAndroid,
  ToastAndroid
} from 'react-native';
import ContentWrapper from '../common/ContentWrapper';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';
import ProfileDetailsRow from '../common/ProfileDetailsRow';
import ProfilePic from '../common/ProfilePic';
import RenderModal from '../common/RenderModal';
import Toolbar from '../common/Toolbar';

const GLOBAL = require('../config/Globals.js');
const styles = StyleSheet.create(require('../../styles/styles.main'));


class ProfileRender extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let color = this.props.color;
    return (
      <ContentWrapper color={color}>
        <Toolbar title={'Profile'} withBackButton={true} {...this.props}/>
        <ProfilePic source={this.props.image} color={color} size={150} changeAvatar={this.props.setIsOpenImagePicker} style={{margin: 20}}/>

        {/*<View  style={{paddingTop: 5, paddingRight: 15, paddingBottom: 5, paddingLeft: 15, flexDirection: 'row'}}>
          <Text style={{fontSize: 15, fontWeight: '800',color: color.TEXT_DARK, flex: 1}}>Share updated profile on timeline</Text>
          <View style={[styles.changeAvatarIcon, {backgroundColor: color.CHAT_BG, borderColor: color.TEXT_LIGHT, marginLeft: -20}]}>
            {this.renderShareUpdateIcon()}
          </View>
        </View>*/}

        <ProfileDetailsRow title='Display Name' profileDetail={this.props.name} color={color} action={() => {
          this.props.setIsChangingName(true);
        }}/>
        <ProfileDetailsRow title='Status Message' profileDetail={this.props.statusMessage} color={color} action={() => {this.props.setIsChangingStatus(true);}}/>
        <ProfileDetailsRow action={() => console.log('check in')} title='Checked in' profileDetail={this.props.location} color={color}
          onSwitch={value => this.props.saveAndSetLocate(value)}
          switchValue={this.props.isAutoLocate}
        />
        <ProfileDetailsRow action={() => console.log('user id')} title='User ID' profileDetail={this.props.userId} color={color}/>
        <ProfileDetailsRow action={() => console.log('phone number')}
          onPressButton={() => this.props.onPressMobileNumber()}
          title='Phone Number' profileDetail={'+' + this.props.mobile}
          color={color} buttonLabel=' Change '
        />

        {this.renderModal()}
      </ContentWrapper>
    );
  }

  renderShareUpdateIcon() {
    let theme = this.props.theme();
    let color = GLOBAL.COLOR_THEME[theme];
    let isShareUpdate = this.props.isShareUpdate;
    let iconColor = (isShareUpdate) ? color.BUTTON : color.CHAT_BG;

    return <KlikFonts
            name="check"
            style={{color: iconColor, fontSize: 22, backgroundColor: 'transparent'}}
            onPress={() => Navigation.onPress(this.props.changeShareUpdate)}/>;
  }

  renderModal() {
    if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isOpenImagePicker) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => this.props.setIsOpenImagePicker()}
          onPressOpenGallery={this.props.launchGallery}
          onPressOpenCamera={this.props.launchCamera}
          modalType={'uploadImage'}
        />
      );
    } else if (this.props.isUploading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('uploading')}
          modalType={'uploading'}
        />
      );
    } else if (this.props.isChangingDisplayName) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.props.name}
          maxLength={30}
          title={'Display Name'}
          setIsShowModal={this.props.setIsChangingName}
          setChangeProfileDetail={this.props.setChangeProfileDetail}
          modalType={'changeProfileDetail'}
        />
      );
    } else if (this.props.isChangingStatus) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.props.statusMessage}
          title={'Status Message'}
          maxLength={120}
          setIsShowModal={this.props.setIsChangingStatus}
          setChangeProfileDetail={this.props.setChangeProfileDetail}
          modalType={'changeProfileDetail'}
        />
      );
    } else if(this.props.isLoading){
      return (<RenderModal
        theme={this.props.theme}
        setIsShowModal={() => console.log('loading')}
        modalType={'loading'}
      />);
    } else if (this.props.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.props.setIsShowMessageModal("")}
          message={this.props.alertMessage}
          modalType={'alert'}
        />
      );
    }
  }

}

export default ProfileRender;
