'use strict';

import React, {
  Component,
} from 'react';

import {
  View,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  TouchableWithoutFeedback,
  Text,
  Image,
  ToolbarAndroid,
  ListView
} from 'react-native';
const windowSize = Dimensions.get('window');
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
import _ from 'underscore';
import {toNameCase, getMediaURL} from './Helpers';
import Button from './common/Button';
import FacebookTabBar from './FacebookTabBar';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './common/ListRow';
import Send from './config/db/Send';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Users from './config/db/Users';

export default class ShareMedia extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactsDataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      selectedContacts: [],
      friendList: [],
      buttonHeight: 0
    }
    this.renderTabs = this.renderTabs.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.getAllContacts = this.getAllContacts.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
    this.getAllContacts();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllContacts() {
    let friends = this.props.loggedInUser().contacts;
    let friendList = [];
    if (friends.length > 0) {
      for (let i = 0; i < friends.length; i++) {
        let user = Users.getItemById(friends[i]);
        let selected = (_.contains(this.state.selectedContacts, user._id)) ? 'selected' : 'unselected';

        let userData = {
          _id: user._id,
          name: `${toNameCase(user.profile.firstName)} ${toNameCase(user.profile.lastName)}`,
          image: getMediaURL(user.profile.photoUrl),
          selected: selected
        }
        friendList.push(userData);
      }
    }

    if (this._isMounted) {
      this.setState({
        friendList: friendList,
        contactsDataSource: this.state.contactsDataSource.cloneWithRows(friendList)
      })
    }
  }

  render() {
    const color = this.props.color;
    return (
      <View style={{flex: 1}}>
        <ToolbarAndroid
          title={'Share with...'}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
        />
        {this.renderTabs()}
        <View onLayout={(event) => {
          this.setState({buttonHeight: event.nativeEvent.layout.height});
        }}>
          {this.renderButton()}
        </View>
      </View>
    );
  }

  renderButton() {
    const color = this.props.color;
    if (this.state.selectedContacts) {
      return (
        <Button text={`Send(${this.state.selectedContacts.length})`} color={color.BUTTON}
          underlayColor={color.HIGHLIGHT}
          onPress={() => this.onPressSend()} style={{width: windowSize.width, borderRadius: 0}}
          isDefaultWidth={false}/>
      );
    }
  }

  renderTabs() {
    const color = this.props.color;
    let height = (windowSize.height * 0.805) - this.state.buttonHeight;
    return (
      <ScrollableTabView
        ref="scrollableTabView"
        initialPage={0}
        renderTabBar={() => <FacebookTabBar color={color} tabFor="ShareMedia"/>}
        style={{backgroundColor: color.CHAT_BG}}>

          <View style={[{backgroundColor: color.CHAT_BG, height: height}]} tabLabel="Friends">
            <ListView 
              dataSource={this.state.contactsDataSource}
              renderRow={this.renderRow}
            />
          </View>

          {/*<View style={[{backgroundColor: color.CHAT_BG}]} tabLabel="Groups">
            <Text>Groups</Text>
          </View>*/}

      </ScrollableTabView>
    );
  }

  renderRow(data) {
    return (
      <ListRow iconPress={() => this.selectContact(data._id)}
        icon={data.selected}
        labelText={data.name}
        image={data.image}
        color={this.props.color}
        addSeparator={false}
      />
    );
  }

  onPressSend() {
    if (this.state.selectedContacts) {
      let sender = this.props.loggedInUser()._id;
      let selectedContacts = this.state.selectedContacts;
      let image = this.props.image;
      image.URL = image.URL.replace(ddp.mediaServerUrl, '');
      image.original = image.original.replace(ddp.mediaServerUrl, '');
      image.small = image.small.replace(ddp.mediaServerUrl, '');
      image.medium = image.medium.replace(ddp.mediaServerUrl, '');
      image.large = image.large.replace(ddp.mediaServerUrl, '');
      
      for (let x = 0; x < selectedContacts.length; x++) {
        Send.image(sender, selectedContacts[x], null, image);
      }
      // Send.media(this.props.loggedInUser()._id, this.state.selectedContacts, null, photoUrl, 'photo', 0, photoUrl, fileSize);
    }
  }

  selectContact(id) {
    let selectedContacts = this.state.selectedContacts;
    if (_.contains(selectedContacts, id)) {
      let index = selectedContacts.indexOf(id);
      selectedContacts.splice(index, 1);
    } else {
      selectedContacts.push(id);
    }
    
    if (this._isMounted) {
      this.setState({ selectedContacts: selectedContacts })
      this.getAllContacts();
    }
  }
}