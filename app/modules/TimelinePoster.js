'use strict';

import React, { Component } from 'react';
import {
  BackAndroid,
  Dimensions,
  Image,
  InteractionManager,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableHighlight,
  View,
} from 'react-native';
import {launchCamera, launchImageLibrary, getMediaOptions} from './Helpers';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import LinkPreview from './LinkPreview';
import NavigationBar from 'react-native-navbar';
import Modal from './common/RenderModal';
import Send from './config/db/Send';
import Stickers from './InputStickers';
import SoundEffects from './actions/SoundEffects';
import Validator from 'validator';
import Video from 'react-native-video';
const GLOBAL = require('./../modules/config/Globals.js');
const NAVBAR_HEIGHT = (Platform.OS === 'ios') ? 64 : 73;
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');

export default class TimelinePoster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: {},
      active: 'keyboard',
      connectionStatusHeight: 0,
      keyboardSpace: 0,
      linkText:'',
      isLinkModalOpen: false,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isUploading: false,
      shouldPlaySound: false
    };
    this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.onAddLinkPress = this.onAddLinkPress.bind(this);
    this.clearImage = this.clearImage.bind(this);
    this.clearLink = this.clearLink.bind(this);
    this.clearVideo = this.clearVideo.bind(this);
    this.onPressSticker = this.onPressSticker.bind(this);
    this.post = this.post.bind(this);
    this.renderLinkOption = this.renderLinkOption.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.setLinkContent = this.setLinkContent.bind(this);
    this.goBackToTimeline = this.goBackToTimeline.bind(this);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      switch (this.props.commentActive()) {
        case 'stickers':
          return this.onEmojiPress();
        case 'image':
          return this.onPressChooseMedia('photo');
        case 'link':
          return this.onLinkPress();
        default:
          return this.refs.textInput.focus();
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
    BackAndroid.removeEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderNavigator()}
        {this.renderTextContainer()}
        {this.renderOptionsBar()}
      </View>
    );
  }

  renderNavigator() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (Platform.OS === 'ios') {
      return (
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Write', tintColor: color.BUTTON_TEXT}}
         rightButton={<ButtonIcon icon='check' color={color} onPress={this.post} size={25}/>}
         leftButton={<ButtonIcon icon='back' color={color} onPress={this.goBackToTimeline} size={25}/>}
        />
      );
    } else {
      return (
        <KlikFonts.ToolbarAndroid
          elevation={5}
          title={'Write'}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
          actions={[{title: 'Post', iconName: 'check', show: 'always'}]}
          onActionSelected={() => this.post()}
        />
      );
    }
  }

  shouldPlaySound() {
    if (this.state.shouldPlaySound) {
      SoundEffects.playSound('tick');
    } else {
      this.setState({shouldPlaySound:true});
    }
  }

  renderTextContainer() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let textAreaHeight = windowSize.height - this.state.keyboardSpace - NAVBAR_HEIGHT;

    return (
      <View style={{height: textAreaHeight}}>
        {this.props.renderConnectionStatus()}
        <TextInput
          ref='textInput'
          placeholder="What's new today?"
          placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
          value={this.state.content.Text}
          multiline={true}
          onFocus={() => {
            this.shouldPlaySound();
            this.setState({ active: 'keyboard'})}}
          // onBlur={()=> this.closeKeyboardSpace()}
          onChangeText={(text) => {
            text = (text.trim() == '') ? '' : text;
            this.changeText(text)
          }}
          style={{flex:1, fontSize:13, padding:10, backgroundColor:'#fff', textAlignVertical: 'top'}}
        />
        {this.renderCheckConnection()}
        {this.renderModalMessage()}
        {this.renderUploading()}
      </View>
    );
  }

  renderOptionsBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View elevation={10} onLayout={(event) => {
        this.setState({keyboardSpace: event.nativeEvent.layout.height});
      }}>
        <View style={{flexDirection:'row', backgroundColor: color.BUTTON, height: 50}}>
          <KlikFonts name="emoticon" size={25}
            color={this.state.content.Stickers ? color.THEME :color.BUTTON_TEXT }
            style={{marginLeft:20, alignSelf:'center'}}
            onPress={() => this.onEmojiPress()}
          />
          <KlikFonts name="photo" size={32}
            color={this.state.content.Image ? color.THEME :color.BUTTON_TEXT }
            style={{marginLeft:20, alignSelf:'center'}}
            onPress={() => this.onPressChooseMedia('photo')}
          />
          <KlikFonts name="play" size={32}
            color={this.state.content.Video ? color.THEME :color.BUTTON_TEXT }
            style={{marginLeft:20, alignSelf:'center'}}
            onPress={() => this.onPressChooseMedia('video')}
          />
          <KlikFonts name="link" size={32}
            color={this.state.content.Link ? color.THEME :color.BUTTON_TEXT }
            style={{marginLeft:20, alignSelf:'center'}}
            onPress={() => this.onLinkPress()}
          />
        </View>
        {this.renderOptions()}
        {this.renderLinkModal()}
        <KeyboardSpacer />
      </View>
    );
  }

  renderOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    switch(this.state.active) {
      case 'stickers':
        return <Stickers
                height={windowSize.height * 0.35}
                color={color}
                onStickerPress={this.onPressSticker}
                hasInternet={this.props.hasInternet}
              />
      case 'chooseMedia':
        return this.renderMediaOptions();
      case 'link':
        return this.renderLinkOption();
    }
  }

  renderMediaOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let mediaContent = null;
    if (this.state.media == 'photo') {
      mediaContent = (this.state.content.Image)
        ? <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', width: windowSize.height*0.28}}>
            <Close color={color} onPress={this.clearImage}/>
            <Image style={{height: windowSize.height*0.25, width: windowSize.height*0.25}}
              source={{uri:this.state.content.Image ? this.state.content.Image.uri: null}}
            />
          </View>
        : <KlikFonts name="gallery" size={55} color={color.HIGHLIGHT}/>
    } else {
      mediaContent = (this.state.content.Video)
        ? <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', width: windowSize.height*0.28}}>
            <Close color={color} onPress={this.clearVideo}/>
            <Video ref={ref => this._video = ref}
              source={{ uri: this.state.content.Video.path }}
              style={{height: windowSize.height*0.25, width: windowSize.height*0.25}}
              resizeMode={'cover'}
              repeat={true}
              onLoad={() => { this._video.seek(0); }}
            />
          </View>
        : <View style={{borderColor: color.HIGHLIGHT, borderWidth: 5, borderRadius: 50,
                      height: windowSize.height*0.08, width: windowSize.height*0.08,
                      justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
            <KlikFonts name="play" size={38} color={color.HIGHLIGHT}/>
          </View>
    }

    return (
      <View style={{height: ddp.keyboardSpace}}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Button text={'Camera'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.onPressCamera()}
            style={{width: windowSize.width*0.4, marginTop: 20, marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
          <Button text={'Gallery'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.onPressGallery()}
            style={{width: windowSize.width*0.4, marginTop: 20, marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {mediaContent}
        </View>
      </View>
    );
  }

  renderLinkOption() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let linkObj = this.state.content.Link;
    let image
    if (linkObj) {
      let imageArray = JSON.stringify(linkObj.data).match(/(https?:\/\/.*\.(?:png|jpg))/i);
      image = linkObj.data.ogImage ? linkObj.data.ogImage[0].url : imageArray ? imageArray[0]: null;
    }
    let width = windowSize.width * 0.9,
        height = windowSize.height * 0.25
    return (
      <View style={{height: ddp.keyboardSpace, justifyContent: 'center', alignItems: 'center'}}>
        <Button text={'Add Link'}
          color={color.THEME}
          underlayColor={color.HIGHLIGHT}
          onPress={() => this.onAddLinkPress()}
          style={{width: windowSize.width*0.5, marginTop: 20, alignSelf: 'center'}}
          isDefaultWidth={false}/>
        {
          (linkObj)
          ? <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', width: windowSize.width * 0.955}}>
              <Close color={color} onPress={this.clearLink}/>
              <LinkPreview width={width} height={height} image={image} link={linkObj} color={color}/>
            </View>
          : <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <KlikFonts name="link" size={55} color={color.HIGHLIGHT}/>
            </View>
        }
      </View>
    );
  }

  renderLinkModal() {
    if (this.state.isLinkModalOpen) {
      return (
        <Modal
          theme={this.props.theme}
          setIsShowModal={this.onAddLinkPress}
          setLinkContent={this.setLinkContent}
          modalType={'link'}
        />
      );
    }
  }

  renderModalMessage() {
    if (this.state.isShowMessageModal) {
      return (
        <Modal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    }
  }

  renderCheckConnection() {
    if (this.state.isShowCheckConnection) {
      return (
        <Modal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  renderUploading() {
    if (this.state.isUploading) {
      return (
        <Modal
          theme={this.props.theme}
          setIsShowModal={() => console.log('uploading')}
          modalType={'uploading'}
        />
      );
    }
  }

  backAndroidHandler() {
    if ((!this.state.active && this.props.isSafeToBack) || this.state.active == 'keyboard') {
      this.goBackToTimeline();
    } else {
      if (this._isMounted) {
        this.setState({active: null});
        // this.props.setIsSafeToBack(true);
      }
    }
  }

  setLinkContent(link) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (link) {
        let contentObj = this.state.content;
        if (Validator.isURL(link)) {
          ddp.call('extractMetaData', [link]).then((result)=>{
            contentObj.Link = result;
            contentObj.Link.data.ogUrl = this.state.linkText;
            this.setState({
              content: contentObj,
              isLinkModalOpen: false,
              active: 'link'
            });
          }).catch(err => {
            console.log('[extractMetaData] error', err);
            contentObj.Link = null;
            this.setState({
              content: contentObj,
            });
            this.setIsShowMessageModal('URL does not exist');
          });
        } else{
          this.setIsShowMessageModal('Not a URL');
        }
      } else {
        this.setIsShowMessageModal('No URL')
      }
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  goBackToTimeline() {
    this.refs.textInput.blur();
    this.props.navigator.pop();
  }

  clearImage() {
    let contentObj = this.state.content;
    contentObj.Image = null;
    this.setState({
      content: contentObj
    });
  }

  clearVideo() {
    let contentObj = this.state.content;
    contentObj.Video = null;
    this.setState({
      content: contentObj
    });
  }

  clearLink() {
    let contentObj = this.state.content;
    contentObj.Link = null;
    this.setState({
      content: contentObj
    });
  }

  changeText(text){
    let contentObj = this.state.content;
    contentObj.Text = text;
    this.setState({
      content: contentObj
    });
  }

  onPressChooseMedia(media) {
    console.log("Open media");
    if (this._isMounted) {
      SoundEffects.playSound('tick');
      this.refs.textInput.blur();
      this.setState({
        active: 'chooseMedia',
        media: media
      });
      this.props.setIsSafeToBack(false);
    }
  }

  onPressSticker(data){
    let contentObj = this.state.content;
    contentObj.Stickers = data.Image;
    this.setState({
      content: contentObj
    });
  }

  onAddLinkPress(){
    if (this.state.content.Link) {
      if (this._isMounted) {
        this.refs.textInput.blur();
        this.setState({
          isLinkModalOpen: false,
          active: 'link'
        })
      }
      this.setIsShowMessageModal('You can attach 1 URL only.');
    } else {
      if (this._isMounted) {
        this.refs.textInput.blur();
        this.setState({
          isLinkModalOpen: !this.state.isLinkModalOpen,
          active: null
        })
      }
    }
  }

  onEmojiPress() {
    if (this._isMounted) {
      SoundEffects.playSound('tick');
      this.refs.textInput.blur();
      this.setState({active: 'stickers'});
      this.props.setIsSafeToBack(false);
    }
  }

  onLinkPress(){
    if (this._isMounted) {
      SoundEffects.playSound('tick');
      this.refs.textInput.blur();
      this.setState({active: 'link'});
      this.props.setIsSafeToBack(false);
    }
  }

  onPressGallery() {
    launchImageLibrary(this.state.media, (response) => {
      this.setMediaContent(response);
    });
  }

  setMediaContent(response) {
    if (typeof response == 'string') {
      this.setIsShowMessageModal(response);
    } else {
      if (this._isMounted) {
        let contentObj = this.state.content;
        if (this.state.media == 'photo') {
          contentObj.Image = response;
        } else {
          contentObj.Video = response;
        }

        this.setState({
          content: contentObj
        });
      }
    }
  }

  onPressCamera() {
    launchCamera(this.state.media, (response) => {
      this.setMediaContent(response);
    });
  }

  post() {
    SoundEffects.playSound('tick');
    if (!this.props.hasInternet() || !this.props.isLoggedIn()) {
      this.setIsShowCheckConnection();
    } else {
      let content = this.state.content;
      if ( (content.hasOwnProperty('Text') && content.Text != "")
          || (content.hasOwnProperty('Image') && content.Image)
          || (content.hasOwnProperty('Stickers') && content.Stickers)
          || (content.hasOwnProperty('Link') && content.Link)
          || (content.hasOwnProperty('Video') && content.Video)) {
        if (this._isMounted) {
          this.setState({
            active: null,
            isUploading: true,
          });
        }
        Send.post(content, this.props.loggedInUser()._id, () => {
          if (this._isMounted) {
            this.setState({isUploading: false});
          }
          InteractionManager.runAfterInteractions(() => {
            if (Platform.OS == 'android') {
              ToastAndroid.show('Successfully posted on your timeline.', ToastAndroid.SHORT)
            }
          });
          this.goBackToTimeline();
        });
      } else {
        this.setIsShowMessageModal('You do not have something to post')
      }
    }
  }
}

const Close = ({color, onPress}) => (
  <View style={{height:25, width:25, position: 'absolute', top: 12, right: 0, zIndex: 1,
                borderRadius: 50, backgroundColor: color.HIGHLIGHT, borderColor: color.TEXT_LIGHT, borderWidth: 0.5}}>
    <KlikFonts name='close' size={24} onPress={() => onPress()} style={{color: color.TEXT_LIGHT, backgroundColor: 'transparent'}}/>
  </View>
)