/*
Author: Terence John Lampasa
Created: Aug 16, 2016

update: 2016-08-22 (ida)
- Added styles

*/
import {
  View,
  StyleSheet,
  TouchableNativeFeedback,
  Text,
  TextInput,
  Navigator,
  Platform,
  Alert,
  Dimensions,
  ListView,
  RecyclerViewBackedScrollView,
} from 'react-native';
import React, { Component } from 'react';
import _ from 'underscore';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
var Helpers = require('./Helpers.js');
var GLOBAL = require('./../modules/config/Globals.js');
var RNFS = require('react-native-fs');
var styles = StyleSheet.create(require('./../styles/styles.main'));

class DirectoryExplorer extends Component
{
  constructor(props) {
    super(props);

    this.state = {
        currentDirectory: '/storage/emulated/0/',
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        previousDirectories: [],
    }
    this.updateCurrentDirectory = this.updateCurrentDirectory.bind(this);
    this.onPressRow = this.onPressRow.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }

  componentDidMount()
  {
    this.updateCurrentDirectory();
  }

  render() {

    return(
      <View style={{height: this.props.keyboardSpace}}>
        <ListView
          ref='listview'
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          initialListSize={1}
          onEndReached={this.onEndReached}/>
      </View>
    );
  }

  renderRow(rowData) { 
    let color =  this.props.color;
    //console.log(rowData)
      //RowData Format:
      // name: 'obb',
      // path: '/storage/emulated/0/obb',
      // size: 4096,
      // isFile: [Function: isFile],
      // isDirectory: [Function: isDirectory]

      var name = rowData.name;
      let isDirectory = rowData.isDirectory();
      let borderBottomWidth = (isDirectory) ? 1 : 0;

      return(
        <View style={{height: 40}}>
          <TouchableNativeFeedback
            onPress={() => this.onPressRow(rowData)}>
              <View 
                style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5, marginRight: 5,
                        borderBottomWidth: borderBottomWidth, borderBottomColor: color.BUTTON}}>
                  {this.renderIcon(rowData)}
                  <Text numberOfLines={1} style={{color: color.BUTTON, flex: 1}}>{name}</Text>
              </View>
            </TouchableNativeFeedback>
          </View>
      )


  }

  renderIcon(rowData) {
    let color =  this.props.color;
    let name = rowData.name;
    let fileExtension = name.split('.').pop();
    let isFile = rowData.isFile();
    let isDirectory = rowData.isDirectory();
    
    if (rowData.isBackButton) {
      return <KlikFonts name="openfolder" style={{fontSize: 25, marginLeft: 5, marginRight: 5, color: color.BUTTON}}/>;
    } else if(isDirectory) {
      return <KlikFonts name="closefolder" style={{fontSize: 25, marginLeft: 5, marginRight: 5, color: color.BUTTON}}/>;
    } else {
      switch (fileExtension) {
        case "MOV":
        case "mp4":
        case "3gp":
          return <KlikFonts name="videofile" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
        case "pdf":
          return <KlikFonts name="pdf" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
        case "doc":
        case "docx":
        case "rft":
        case "pptx":
          return <KlikFonts name="doc" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
        case "aac":
        case "mp3":
        case "ogg":
          return <KlikFonts name="audio" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
        case "jpg":
        case "png":
         return <KlikFonts name="image" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
        default:
          return <KlikFonts name="file" style={{fontSize: 25, marginRight: 5, marginLeft: 20, color: color.BUTTON}}/>;
      }
    }
  }

  renderFooter()
  {
    return <View/>
  }

  onPressRow(rowData)
  {
    if(!rowData.isDirectory()) {
      //console.warn("Clicked File: "+rowData.name);
      this.props.sendAttachment(rowData);
    } else {
      if(rowData.isBackButton)
      {
        if(this.state.previousDirectories.length > 0)
          this.state.previousDirectories.splice(this.state.previousDirectories.length-1, 1);
      }
      else {
        this.state.previousDirectories.push(this.state.currentDirectory);
      }

      this.state.currentDirectory = rowData.path + "/";

      this.updateCurrentDirectory();
    }
  }

  updateCurrentDirectory()
  {
    //RowData Format:
    // name: 'obb',
    // path: '/storage/emulated/0/obb',
    // size: 4096,
    // isFile: [Function: isFile],
    // isDirectory: [Function: isDirectory]

    var backButton ={
            name: 'Back',
            path: this.state.previousDirectories[this.state.previousDirectories.length-1],
            size: 0,
            isFile: function(){return false},
            isDirectory: function(){return true},
            isBackButton: true}
            ;

    RNFS.readDir(this.state.currentDirectory)
    .then((result)=>{

      var folders = [];
      var files = [];
      var newResult = [];

      //Sort alphabetically
      result = _.sortBy(result, 'name');

      //segregate according to file/folder
      _.each(_.values(result), function(r) {
          if(r.isDirectory())
          {
            folders.push(r);
          }
          else {
            files.push(r);
          }
      });

      if(this.state.previousDirectories.length > 0)
      newResult.push(backButton);

      _.each(folders, function (r){
        newResult.push(r);
      });

      _.each(files, function (r){
        newResult.push(r);
      });


      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(newResult)
      })
    })
    .catch(error=>{
      console.warn("RNFS: "+error);
    })
  }

}

module.exports = DirectoryExplorer;
