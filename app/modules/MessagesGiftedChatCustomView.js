import React from 'react';
import {
  ActivityIndicator,
  Image,
  Linking,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import _ from 'underscore';
import {getDisplayName, getMediaURL, toNameCase} from './Helpers';
import AudioPlayerView from './AudioPlayerView';
import Download from './config/db/Download';
import ImageWithLoader from './common/ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Message from './config/db/Message';
import RenderModal from './common/RenderModal';
import Send from './config/db/Send';
import MapView from 'react-native-maps';
import Users from './config/db/Users';
const GLOBAL = require('./config/Globals.js');

export default class MessagesGiftedChatCustomView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isShowContactModal: false,
      contact: null,
      noConnection: false
    }
    this.acceptFriendRequest = this.acceptFriendRequest.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.render = this.render.bind(this);
    this.renderContactModal = this.renderContactModal.bind(this);
    this.resendMedia = this.resendMedia.bind(this);
    this.sendFriendRequest = this.sendFriendRequest.bind(this);
    this.setIsShowContactModal = this.setIsShowContactModal.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    var color = this.props.color;
    let isLoggedInUser = this.props.currentMessage.user._id == this.props.loggedInUser()._id;

    if (this.props.currentMessage.audio) {
      if (this.props.currentMessage.seen != undefined) {
        let audio = this.props.currentMessage;
        let isCurrentUser = this.props.currentMessage.isCurrentUser;
        return (
          <View style={{borderRadius: 13, margin: 3}}>
            <AudioPlayerView
              data={audio}
              color={color}
              isCurrentUser={isCurrentUser}
              otherUserId={this.props.otherUser._id}
              {...this.props}
            />
          </View>
        );
      } else if (this.props.currentMessage.status == 'Failed' && isLoggedInUser) {
        return (
          <TouchableOpacity style={[styles.audioPlayer, {margin: 3}]} onPress={() => this.resendMedia(this.props.currentMessage._id, 'audio', Download.mediaTypePath.audioSent)}>
            <KlikFonts name="refresh" color={'#fff'} size={40} />
          </TouchableOpacity>
        )
      } else if (this.props.currentMessage.status == 'Sending' && isLoggedInUser) {
        return (
          <View style={[styles.audioPlayer, {margin: 3}]}>
            <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
            <Text style={{color:color.BUTTON, fontSize:10, alignSelf:'center'}}> Uploading...</Text>{/* {Math.round(100 * this.props.currentMessage.progress)}%</Text> */}
          </View>
        );
      }
    } else if (this.props.currentMessage.contact) {
      let contact = this.props.currentMessage.contact;
      let image = (contact.image) ? contact.image : GLOBAL.DEFAULT_PROFILE_IMAGE;
      return (
        <TouchableOpacity onPress={() => this.setIsShowContactModal(contact)}
         style={{margin:10, marginLeft:30, marginRight:30, alignItems:'center'}}>
          <ImageWithLoader style={{width:80, height:80, borderRadius:40}} color={color} image={getMediaURL(image)} />
          <Text style={{color: color.TEXT_DARK, margin:5, textAlign:'center', textDecorationLine:'underline', fontWeight:'700'}} >{contact.name}</Text>
          {this.renderContactModal(contact._id)}
        </TouchableOpacity>
      );
    } else if (this.props.currentMessage.like) {
      return <KlikFonts name="emoticon" style={[styles.messageIcon, {color: color.CHAT_SENT}]}/>
    } else if (this.props.currentMessage.location) {
      return (
        <TouchableOpacity style={[styles.container, this.props.containerStyle]} onPress={() => {
          const url = Platform.select({
            ios: `http://maps.apple.com/?ll=${this.props.currentMessage.location.latitude},${this.props.currentMessage.location.longitude}&z=18`,
            android: `http://maps.google.com/?q=${this.props.currentMessage.location.latitude},${this.props.currentMessage.location.longitude}&z=18`
          });
          Linking.canOpenURL(url).then(supported => {
            if (supported) {
              return Linking.openURL(url);
            }
          }).catch(err => {
            console.error('An error occurred', err);
          });
        }}>
          <MapView
            style={[styles.mapView, this.props.mapViewStyle]}
            region={{
              latitude: this.props.currentMessage.location.latitude,
              longitude: this.props.currentMessage.location.longitude,
              latitudeDelta: 0.001,
              longitudeDelta: 0.001
            }}
            annotations={[{
              latitude: this.props.currentMessage.location.latitude,
              longitude: this.props.currentMessage.location.longitude,
            }]}
            scrollEnabled={false}
            zoomEnabled={false}
          />
        </TouchableOpacity>
      );
    } else if (this.props.currentMessage.video) {
      let id = this.props.currentMessage._id;
      let video = (this.props.currentMessage.mediaCache) ? this.props.currentMessage.mediaCache : this.props.currentMessage.video;
      let urlParts = this.props.currentMessage.video.split('/');
      let thumb = urlParts[4].split('.');

      // http://elb-dev-763284714.ap-southeast-1.elb.amazonaws.com:5001/vt/YXzgFTFnGPKgrmMyn/5120621390105326-big_buck_bunny.mp4
      // let videoThumbUri = 'http://' + GLOBAL.SERVER_HOST + ':' + GLOBAL.SERVER_PORT + '/vt/' + urlParts[3] + '/' + urlParts[4];
      // let videoThumbUri = ddp.mediaServerUrl + '/' + urlParts[3] + '/' + thumb[0] + '.png';
      let videoThumbUri = 'http://elb-dev-763284714.ap-southeast-1.elb.amazonaws.com:5001' + '/vt/' + urlParts[3] + '/' + urlParts[4];
      console.log('----- videoThumbUri = ', videoThumbUri);

      if (this.props.currentMessage.seen != undefined) {
        let data = this.props.currentMessage;
        return (
          <TouchableOpacity style={[styles.container, this.props.containerStyle]}
            onPress={() => this.onPressVideo()}>
            <Image
            style={[styles.image, this.props.imageStyle, {backgroundColor:color.HIGHLIGHT}]}
            borderRadius={13}
            source={{uri: videoThumbUri}}>
            {(data.video && this.props.hasInternet() || data.mediaCache) ?
                <View style={styles.videoOverlayView}>
                  <KlikFonts name="play" color={'#fff'} size={30} />
                  <Text style={{fontSize:20, fontWeight:'600', color:'#fff', marginLeft:10}}>PLAY</Text>
                </View>
              : <View style={styles.videoOverlayView}>
                  <Text style={{fontSize:20, fontWeight:'600', color:'#fff'}}>Cannot Play</Text>
                </View>
            }
            </Image>
          </TouchableOpacity>
        );
      } else if (this.props.currentMessage.status == 'Failed' && isLoggedInUser) {
        return (
          <TouchableOpacity style={[styles.container, this.props.containerStyle, {margin: 3}]} onPress={() => this.resendMedia(this.props.currentMessage._id, 'video', Download.mediaTypePath.videoSent)}>
            <View style={styles.videoOverlayView}>
              <KlikFonts name="refresh" color={'#fff'} size={40} />
            </View>
          </TouchableOpacity>
        )
      } else if (this.props.currentMessage.status == 'Sending' && isLoggedInUser) {
        return (
          <View style={[styles.container, this.props.containerStyle, {margin: 3}]}>
            <View style={styles.videoOverlayView}>
              <ActivityIndicator styleAttr="Inverse" color={'#FFF'} />
              <Text style={{color:'#FFF'}}>Uploading...</Text>
              {/* <Text style={{color:'#FFF'}}>{Math.round(100 * this.props.currentMessage.progress)}%</Text> */}
            </View>
          </View>
        )
      }
    }

    return null;
  }

  renderContactModal(contactId) {
    if (this.state.isShowContactModal) {
      if (contactId !== this.props.loggedInUser()._id) {
        let contact = this.props.currentMessage.contact;
        let isContact = _.contains(_.pluck(Users.getAllFriends(), '_id'), contactId);
        let hasAlreadySentFR = _.contains(_.pluck(this.props.loggedInUser().friendRequestSent, 'userId'), contactId);
        let hasAlreadyReceivedFR = _.contains(_.pluck(Users.getAllFriendRequests(), '_id'), contactId);
        let actions = [];
        let relStatus = null;

        if (isContact || hasAlreadySentFR) {
          if (hasAlreadySentFR) {
            relStatus = 'Friend request sent';
          } else {
            relStatus = 'Already your friend';
          }
          actions = [{icon: 'recent', iconLabel: 'Chat', onPressIcon: () => this.goToMessageChat(contact)}];
        } else {
          if (hasAlreadyReceivedFR) {
            relStatus = 'Respond to friend request';
            actions = [{icon: 'block', iconLabel: 'Block', onPressIcon: () => this.setIsShowContactModal(null)},
                      {icon: 'check', iconLabel: 'Accept Request', onPressIcon: () => this.acceptFriendRequest(contactId)}];
          } else {
            actions = [{icon: 'block', iconLabel: 'Block', onPressIcon: () => this.setIsShowContactModal(null)},
                      {icon: 'check', iconLabel: 'Send Friend Request', onPressIcon: () => this.sendFriendRequest(contactId)}];
          }
        }

        let contactInfo = {
          data: contact,
          actions: actions,
          relStatus: relStatus
        }
        return (
          <RenderModal
            color={this.props.color}
            profileData={contactInfo}
            setIsShowModal={() => this.setIsShowContactModal(null)}
            modalType={'viewProfile'}
          />
        );
      }
    }
  }

  onPressVideo() {
    let data = this.props.currentMessage;
    if (data.video && this.props.hasInternet() || data.mediaCache) {
      this.props.goToVideoPlayer(this.props.otherUser._id, this.props.currentMessage)
    } else {
      this.setState({
        noConnection: true
      })
    }
  }

  setIsShowContactModal() {
    if (this._isMounted) {
      this.setState({
        isShowContactModal: !this.state.isShowContactModal
      });
    }
  }

  resendMedia(id, mediaType, mediaPath) {
    let message = Message.getItemById(id);
    console.log('---', message.status);
    if (message.status == 'Failed') {
      Send.resend(message, mediaType, Send.uploadURL.mediaChat, mediaPath, this.props.isAutoSaveMedia());;
      this.props.setReuploadMedia();
    }
  }

  acceptFriendRequest(id) {
    this.setIsShowContactModal(null);
    ddp.call('addToContacts', [id]).then(res => {
      if (res) {
        console.log('Successfully accepted friend request.')
        ddp.collections.contacts.upsert(res);
      }
    });
  }

  sendFriendRequest(id) {
    this.setIsShowContactModal(null);
    ddp.call('addFriendRequest', [id]).then(result => {
      if (result) {
        console.log('Successfully sent friend request.');
      }
    });
  }

  goToMessageChat(contact) {
    this.setIsShowContactModal(null);
    this.props.goToMessageChat(contact);
  }

}

const styles = StyleSheet.create({
  container: {
  },
  mapView: {
    width: 150,
    height: 100,
    borderRadius: 13,
    margin: 3,
  },
  image: {
    width: 150,
    height: 100,
    // borderRadius: 13,
    margin: 3,
    // resizeMode: 'cover',
  },
  videoOverlayView: {
    width: 150,
    height: 100,
    borderRadius: 13,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor:'#000000AA',
  },
  audioPlayer: {
    width: 150,
    height: 40,
    borderRadius: 13,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor:'#000000AA',
  },
});
