/**
Author:           Terence John Lampasa
Date Created:     2016-05-04
Date Updated:     2016-05-19
Descrption:       This file is used to edit a group chat.
Return:
File Dependency:  ddp.js, Helpers.js
Changelog:
  update: 2016-05-06 (by Ida)
    - Modified UI color scheme
  update: 2016-05-19 (by Ida)
  - added keyboardShouldPersistTaps={true} in <ListView />
  update: 2016-06-20 (ida)
  - Implement color themes
  update: 2016-06-27 (ida)
  - changed icons to klikfonts
  upDATE: 2016-07-27 (Terence)
    -Fixed Checkboxes not toggling between checked/unchecked states
*/

'use strict';

import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableHighlight,
  View,
  Dimensions
} from 'react-native';

import {jsonToArray, toNameCase, toPhoneNumber, getDisplayName, getMediaURL} from './Helpers.js';
import _ from 'underscore';
//import ContactRetriever from './ContactRetriever';
import ddp from './config/ddp';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './common/ListRow';
import Modal from 'react-native-simple-modal';
import RenderModal from './common/RenderModal';
import Users from './config/db/Users';
let ImagePickerManager = require('NativeModules').ImagePickerManager;

const GLOBAL = require('./../modules/config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
var windowSize = Dimensions.get('window');
class AddUserToGroup extends Component {

  // Initial state
  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      contactsChosen: [],//user object list of chosen list
      text: "",
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isShowModalLoading: false,
      loading: true,
      isLoaded: false,
      buttonText: 'Add',
      loggedInUser: this.props.loggedInUser(),
      users: this.props.groupUsers
    }
    this.setData = this.setData.bind(this);
    this.getGroupUsers = this.getGroupUsers.bind(this);
    this.renderContacts = this.renderContacts.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowModalLoading = this.setIsShowModalLoading.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
  }

  //get the data before rendering
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getGroupUsers();
    });
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  getGroupUsers() {
    if (this.props.addOrRemove == 'Add') {
      ddp.call('getUsersToBeAddedToGroup', [this.props.group._id]).then(result => {
        if (this._isMounted) {
          this.setState({users: result});
        }
        this.setData();
      });
    } else {
      this.setData();
    }
  }

  setData() {
    let users = this.state.users;
    let loggedInUserId = this.state.loggedInUser._id;
    let groupUsers = _.reject(users, function(user){ return user._id == loggedInUserId; });

    if (this._isMounted) {
      this.setState({
        buttonText: this.props.addOrRemove,
        users: groupUsers,
        dataSource: this.state.dataSource.cloneWithRows(groupUsers)
      })
    }
  }

  //render each contact
  renderContacts(contact) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let icon = (this.state.contactsChosen.indexOf(contact._id) == -1) ? 'unselected' : 'selected';

    return (
      <ListRow labelText={contact.name}
        detailText={contact.statusMessage}
        image={getMediaURL(contact.image)}
        color={color}
        addSeparator={true}
        icon={icon}
        iconPress={() => this.processContact(contact)}
      />
    );
  }

  // Search contact
  filterWithSearchText(text) {
    text = (text.trim() == '') ? '' : text;
    var contact;
    var tempRawDataSource = this.state.users.slice();//Makes a copy of the rawDataSource so we don't have to getContacts over n over

    for(var index =0; index < tempRawDataSource.length; index++) {
      // search contact by name
      contact = tempRawDataSource[index].name;

      if( (contact).toLowerCase().indexOf((text).toLowerCase())===-1 ) {
        tempRawDataSource.splice(index, 1);
        index=-1;
      }
    }
    if (this._isMounted) {
      this.setState({
        text: text,
        dataSource: this.state.dataSource.cloneWithRows(tempRawDataSource)
      });
    }
  }

  // Add or Remove user from the group
  processContact(contact) {
    let tempContactsChosen = this.state.contactsChosen.slice();

    if( tempContactsChosen.indexOf(contact._id)==-1 ) {
      tempContactsChosen.push(contact._id);
    } else {
      tempContactsChosen.splice(tempContactsChosen.indexOf(contact._id), 1);
    }

    if (this._isMounted) {
      this.state.dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setState({
        users: this.state.users,
        dataSource: this.state.dataSource.cloneWithRows(this.state.users),
        contactsChosen: tempContactsChosen
      });
    }
    console.log('contactsChosen', tempContactsChosen);
    this.filterWithSearchText(this.state.text);
  }

  // Create the group and redirect to GroupMessages.android.js
  submit() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else if(this.state.contactsChosen.length <= 0) {
      this.props.navigator.pop();
    } else {
      if (this.props.addOrRemove == 'Remove') {
        if ((this.state.users.length - this.state.contactsChosen.length) <= 1) {
          this.setIsShowMessageModal('Cannot remove member. There should be atleast 3 members in a group.');
        } else {
          this.editGroupMembers();
        }
      } else {
        this.editGroupMembers();
      }
    }
  }

  editGroupMembers() {
    let action = (this.props.addOrRemove == 'Add') ? 'add' : 'remove';
    let options = [this.props.group._id, this.state.contactsChosen, action]
    this.setIsShowModalLoading();
    ddp.call('editGroupUsers', options).then(result => {
      if (result) {
        if (this._isMounted) {
          this.setState({users: result});
        }
        this.props.setGroupUsers(result, action);
        this.setIsShowModalLoading();
        this.props.navigator.pop();
      }
    }).catch(err => {
      console.log('Remove user from group Error: ', err);
      this.setIsShowModalLoading();
      this.setIsShowMessageModal('Something went wrong, please try again later.');
    });
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let content = <View/>;

    if (this.state.dataSource.getRowCount() === 0) {
      content = <NoContacts
                  filter={this.state.filter}
                  isLoading={this.state.isLoading}
                  theme={this.props.theme}
                />;
    } else {
      content = <ListView
                  ref="listview"
                  style={{height: windowSize.height* 0.68}}
                  keyboardShouldPersistTaps={true}
                  dataSource={this.state.dataSource}
                  renderRow={this.renderContacts}
                  onEndReached={this.onEndReached}
                  removeClippedSubviews={false}
                />;
    }

    return (
      <View style={styles.container}>
        <View style={[styles.addContainer, {backgroundColor: color.THEME}]}>
          <Text style={{color: color.BUTTON_TEXT, flex: 1, flexDirection: 'row', textAlign: 'left', marginLeft: 20, fontSize: 23,justifyContent: 'flex-start'}}>{this.props.group.Name}</Text>
          <TouchableHighlight
            onPress={() => this.submit()}
            underlayColor={color.THEME}
            style={{width: 150}}>
            <View style={styles.buttonCreate}>
              <KlikFonts name='check' style={[styles.createIcon, {color: color.BUTTON_TEXT}]}/>
              <View style={styles.txtCreateContainer}>
                <Text style={{color: color.BUTTON_TEXT}}>{this.state.buttonText}</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
        <View style={[styles.mainContainer, {backgroundColor: color.CHAT_BG, paddingTop: 20}]}>

          <View style={styles.searchContainer}>
            <KlikFonts name="addcontact" style={[styles.personIcon, {color: color.THEME}]}/>
            <View style={styles.addPeople}>
              <TextInput
                placeholder={`${this.state.buttonText} people to the group`}
                placeholderTextColor={color.TEXT_LIGHT}
                style={[styles.txtName, {color: color.TEXT_DARK}]}
                onChangeText={(text) => {
                  // this.setState({searchText: text});
                  this.filterWithSearchText(text)
                }}
                value={this.state.text}
                underlineColorAndroid={color.THEME} />
              {/*<View style={[styles.underline, {borderColor: '#dbdbdb'}]}></View>*/}
            </View>
          </View>
          {content}
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderModal() {
    if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.state.isShowModalLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loading'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsShowModalLoading() {
    if (this._isMounted) {
      this.setState({isShowModalLoading: !this.state.isShowModalLoading});
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  goToGroupDetails(data) {


    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    console.log("goToGroupDetails data: ", data);
    let groupData = {
      _id: data._id,
      Name: this.state.groupName,
      image: getMediaURL(data.GroupImage),
    };

    //console.log("Data.GroupImage: ", groupData.image);
    data["userGroups"] = UserGroups.getItemByGroupId(data._id);
    // console.log("group data: ", data);
    // console.log("Group users: ", data.userGroups);
    if (navigatorStack[currentRoute].name !== 'GroupDetails') {
      requestAnimationFrame(() => {
        setTimeout(()=>{
          this.props.navigator.replacePreviousAndPop({
            name: 'GroupDetails',
            backgroundImageSrc: this.props.backgroundImageSrc,
            groupUsers: data.userGroups,
            theme: this.props.theme,
            group: groupData,//this.props.group,
            loggedInUser: this.props.loggedInUser,
            shouldReloadData: this.props.shouldReloadData,
            renderConnectionStatus: this.props.renderConnectionStatus,
            hasInternet: this.props.hasInternet,
            refreshGroups: this.props.refreshGroups,
            timeFormat: this.props.timeFormat,
            setIsSafeToBack: this.props.isSafeToBack
          });
        }, 1500);

      });
    }
  }

  replaceMessageThread(data) {

    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    let groupData = {
      _id: data._id,
      Name: this.state.groupName,
      image: data.GroupImage,
    };
    data["userGroups"] = UserGroups.getItemByGroupId(data._id);
    console.log("Replaced route at ",   navigatorStack[navigatorStack.length - 3].name);
    console.log("Message Thread name: ", groupData);
    if (navigatorStack[currentRoute].name !== 'GroupDetails') {
      requestAnimationFrame(() => {

          this.props.navigator.replaceAtIndex(
            {
              id:'messagesgiftedchat',
              name:'MessagesGiftedChat',
              addContactToLoggedInUser: this.props.addContactToLoggedInUser,
              backgroundImageSrc: this.props.backgroundImageSrc,
              changeBgImage: this.props.changeBgImage,
              changeTheme: this.props.changeTheme,
              getMessageWallpaper: this.props.getMessageWallpaper,
              group: groupData,
              hasInternet: this.props.hasInternet,
              isSafeToBack: this.props.isSafeToBack,
              loggedInUser: this.props.loggedInUser,
              logout: this.props.logout,
              renderConnectionStatus: this.props.renderConnectionStatus,
              setLoggedInUser: this.props.setLoggedInUser,
              setIsViewVisible: this.props.setIsViewVisible,
              setIsSafeToBack: this.props.setIsSafeToBack,
              shouldReloadData: this.props.shouldReloadData,
              subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
              theme: this.props.theme,
              timeFormat: this.props.timeFormat,
              user: this.props.user,
              users: data.userGroups
            },
            (navigatorStack.length - 3),
            null
          );
        });
      }
  }
};

// Display for empty records
var NoContacts = React.createClass({
  render: function() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var text = '';
    if (this.props.filter) {
      text = `No results for "${this.props.filter}"`;
    } else if (!this.props.isLoading) {
      text = 'No contacts found';
    }

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={{marginTop: 20, color: color.TEXT_LIGHT}}>{text}</Text>
      </View>
    );
  }
});

module.exports = AddUserToGroup;
