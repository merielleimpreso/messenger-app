'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import _ from 'underscore';
import {logTime, toNameCase, getMediaURL} from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import ScreenNoItemFound from './ScreenNoItemFound';
import SearchBar from './SearchBar';

const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const GLOBAL = require('./config/Globals.js');

class ContactsGroup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      members: [],
      dataSource: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2}),
      isLoading: true,
      theme: this.props.theme(),
      loading: true,
      isLoaded: false
    };
    this.getMembers = this.getMembers.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getMembers();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getMembers() {
    let usergroups = _.where(this.props.usergroups(), {'GroupFID': this.props.groupId});
    var members = [];
    for (var i = 0; i < usergroups.length; i++) {
      let user = _.findWhere(this.props.users(), {'_id': usergroups[i].UserFID});
      let contact = {
        _id: user._id,
        name: toNameCase(user.profile.firstName) + ' ' + toNameCase(user.profile.lastName),
        image: (user.profile.photoUrl) ? user.profile.photoUrl : GLOBAL.DEFAULT_IMAGE
      };
      members.push(contact);
    }
    if (this._isMounted) {
      this.setState({
        members: members,
        dataSource: this.state.dataSource.cloneWithRows(members),
        isLoading: false
      });
    }
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Members', tintColor: color.BUTTON_TEXT}}
         leftButton={
           <TouchableHighlight onPress={() => this.onPressButtonBack()} underlayColor='transparent' style={{alignSelf:'center'}}>
             <KlikFonts name='back' color={color.BUTTON_TEXT} style={styles.navbarButton} size={30}/>
           </TouchableHighlight>
         }
        />
        <SearchBar
         theme={this.props.theme}
         onSearchChange={this.onSearchChange}
         isLoading={false}
         placeholder='Search by Name'
         onFocus={() =>
          this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
       />
       {this.renderContent()}
      </View>
    );
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (!this.state.isLoading && this.state.dataSource.getRowCount() == 0) {
      return <ScreenNoItemFound isLoading={false} text={'No contact found'} color={color}/>;
    } else {
      return (
        <ListView ref="listview"
         dataSource={this.state.dataSource}
         renderRow={this.renderRow}
         automaticallyAdjustContentInsets={false}
         keyboardDismissMode="on-drag"
         keyboardShouldPersistTaps={true}
         showsVerticalScrollIndicator={false}
        />
      );
    }
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let checked = _.contains(this.state.chosenContactIds, data._id);
    return (
      <View>
        <View style={styles.contactsListViewRow}>
          <Image source={{uri:getMediaURL(data.image)}} style={styles.listViewImage}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
            {(this.state.loading && !this.state.isLoaded) 
              ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:40, width:40}}>
                  <ActivityIndicator size="small" color={color.BUTTON} />
                </View>
              : null
            }
          </Image>
          <View style={styles.listViewRowContainer}>
            <Text numberOfLines={1} style={[styles.chatRecentTextName, {color:color.TEXT_DARK}]}>{data.name}</Text>
          </View>
        </View>
        <View style={{height:1, marginLeft:50, marginRight:10, backgroundColor:color.CHAT_SENT}}/>
      </View>
    );
  }

  onSearchChange(searchText) {
    var contacts = _.filter(this.state.members, function(contact){
      return (contact.name).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });

    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(contacts)
      });
    }
  }

  onPressButtonBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

}

module.exports = ContactsGroup;
