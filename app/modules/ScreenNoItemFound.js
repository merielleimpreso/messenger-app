/*

Author: Merielle Impreso
Date Created: 2016-04-19
Date Updated: N/A
Description: Display when no item is found in search.
  Used in Contacts.ios.

*/

'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

// var styles = StyleSheet.create(require('./../styles/styles_common.js'));
const styles = StyleSheet.create(require('./../styles/styles_common.js'));

var ScreenNoItemFound = React.createClass({
  render: function() {
    var color = this.props.color;
    var text = '';
    var isLoading = this.props.isLoading;
    if (!isLoading) {
      text = this.props.text;
    }

    return (
      <View style={[styles.containerNoResults]}>
          <Text style={[styles.textNoResults, {color:color.TEXT_LIGHT}]}> {text} </Text>
      </View>
    );
  }
});

module.exports = ScreenNoItemFound;
