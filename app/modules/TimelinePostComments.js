'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  BackAndroid,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  ToolbarAndroid,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import _ from 'underscore';
import {launchCamera, launchImageLibrary, renderFooter} from './Helpers';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import Comment from './Comment';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from './common/RenderModal';
import NavigationBar from 'react-native-navbar';
import Post from './config/db/Post';
import Reaction from './Reaction';
import Send from './config/db/Send';
import Stickers from './InputStickers';
import SystemAccessor from './SystemAccessor';
import TimelinePost from './TimelinePost';
import TimelineReact from './TimelineReact';
import SoundEffects from '../modules/actions/SoundEffects';
const GLOBAL = require('./../modules/config/Globals.js');
const NAVBAR_HEIGHT = (Platform.OS === 'ios') ? 64 : 75;
const RCTUIManager = require('NativeModules').UIManager;
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');

export default class TimelinePostComments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: null,
      comments: [],
      commentsDataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      connectionStatusHeight: 0,
      keyboardSpace: 0,
      isLoading: true,
      isUploading: false,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      postHeight: 0,
      post: this.props.post(),
      postData: null,
      reactions: [],
      reactionsDataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      reactionsHeight: 0,
      reactToggled: false,
      selectedPositionY: 0
    }
    this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.clearActive = this.clearActive.bind(this);
    this.closeReact = this.closeReact.bind(this);
    this.comments = this.comments.bind(this);
    this.commentOnPost = this.commentOnPost.bind(this);
    this.onPressCamera = this.onPressCamera.bind(this);
    this.onPressGallery = this.onPressGallery.bind(this);
    this.onStickerPress = this.onStickerPress.bind(this);
    this.postImage = this.postImage.bind(this);
    this.reactions = this.reactions.bind(this);
    this.renderContents = this.renderContents.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderReactions = this.renderReactions.bind(this);
    this.renderReactionsRow = this.renderReactionsRow.bind(this);
    this.renderComments = this.renderComments.bind(this);
    this.renderCommentsRow = this.renderCommentsRow.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowReactions = this.setIsShowReactions.bind(this);
    this.goBackToTimeline = this.goBackToTimeline.bind(this);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.makeSubscription();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    // if (this.state.comments.length != prevState.comments.length) {
    //   this.goToBottom();
    // }
    if (JSON.stringify(this.state.reactions) != JSON.stringify(prevState.reactions)) {
      if (this._isMounted) {
        this.setState({
          reactions: this.state.reactions,
          reactionsDataSource: this.state.reactionsDataSource.cloneWithRows(this.state.reactions)
        });
      }
    }
  }

  componentWillUnmount() {
    // this.clearActive();
    this._isMounted = false;
    BackAndroid.removeEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  backAndroidHandler() {
    if ((!this.state.active && this.props.isSafeToBack) || this.state.active == 'keyboard') {
      this.goBackToTimeline();
    } else {
      if (this._isMounted) {
        this.setState({active: null});
      }
    }
  }

  // Subscribe and observe directchat collection.
  makeSubscription() {
    let postId = this.state.post.id;
    Post.observeByPostId(postId, (result) => {
      if (JSON.stringify(result) != JSON.stringify(this.state.postData)) {

        if (this._isMounted) {
          
          let comments = result[0].Comments;
          let reactions = result[0].Reaction;

          if (comments.length > 0 || reactions.length > 0) {
            comments = _.sortBy(comments, 'CreateDate');
            this.setState({
              postData: result[0],
              comments: comments,
              reactions: reactions,
              reactionsDataSource: this.state.reactionsDataSource.cloneWithRows(reactions),
              commentsDataSource: this.state.commentsDataSource.cloneWithRows(comments),
              isLoading: false
            });
          } else {
            this.setState({
              postData: result[0],
              isLoading: false
            });
          }
        }
      }
      this.goToBottom();
    });
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return(
      <View style={{flex: 1, backgroundColor: '#f4f1f1'}}>
        {this.renderNavigator()}
        {this.renderContents()}
        {this.renderOptionsBar()}
        {this.renderModal()}
      </View>
    );
  }

  renderNavigator() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (Platform.OS === 'ios') {
      return (
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Comments', tintColor: color.BUTTON_TEXT}}
         leftButton={<ButtonIcon icon='back' color={color} onPress={this.goBackToTimeline} size={25}/>}
        />
      );
    } else {
      return (
        <ToolbarAndroid
          elevation={5}
          title={'Comments'}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
        />
      );
    }
  }

  renderContents() {
    return (
      <View>
        <View style={{top: 0, left: 0, right: 0, position: 'absolute', alignSelf: 'flex-start', zIndex: 1}}>
          <View onLayout={(event) => {
            this.setState({connectionStatusHeight: event.nativeEvent.layout.height});
          }}>
            {this.props.renderConnectionStatus()}
          </View>
        </View>
        {this.renderReactions()}
        {this.renderComments()}
      </View>
    );
  }

  renderPost() {
    let data = this.props.post();
    return (
      <TimelinePost
        key={data.id}
        data={data}
        navigator={this.props.navigator}
        commentOnPost={this.commentOnPost}
        isOnComment={true}
        setIsShowReactions={this.setIsShowReactions}
        theme={this.props.theme}
        comments={this.comments}
        reactions={this.reactions}
      />
    );
  }

  renderReactions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View onLayout={(event) => {
          this.setState({reactionsHeight: event.nativeEvent.layout.height});
        }}>
        {
          (this.state.reactions.length === 0)
          ? null
          : <View style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: color.HIGHLIGHT, paddingTop: 5, paddingBottom: 5, justifyContent: 'center'}}>
              <ListView ref='reactionslistview'
                removeClippedSubviews={true}
                automaticallyAdjustContentInsets={false}
                dataSource={this.state.reactionsDataSource}
                renderRow={this.renderReactionsRow}
                initialListSize={10}
                contentContainerStyle={{justifyContent: 'flex-start', flexDirection: 'row', flexWrap: 'wrap'}}
              />
            </View>
        }
      </View>
    );
  }

  renderComments() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let contentHeight = windowSize.height - this.state.keyboardSpace - NAVBAR_HEIGHT - this.state.reactionsHeight;

    return (
      <View style={{height: contentHeight}}>
          <ListView ref='commentslistview'
            automaticallyAdjustContentInsets={false}
            enableEmptySections={true}
            dataSource={this.state.commentsDataSource}
            removeClippedSubviews={false}
            renderFooter={this.renderFooter}
            renderRow={this.renderCommentsRow}
            initialListSize={10}
            style={{flex: 1}}
          />
      </View>
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.state.commentsDataSource.getRowCount(0) == 0) ? 'No comments yet. Write one.' : null;
    return renderFooter(!this.state.isLoading, color, this.props.hasInternet(), notFoundText);
  }

  renderOptionsBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let inputFieldStyle = styles.messageInput;
    (Platform.OS == 'ios') && (inputFieldStyle = styles.messageInputIOS);

    return (
      <View elevation={10} onLayout={(event) => {
        this.setState({keyboardSpace: event.nativeEvent.layout.height});
      }}>
        <View style={[styles.inputContainer, {backgroundColor: color.CHAT_BG}]}>
          <TouchableHighlight onPress={() => {SoundEffects.playSound('tick'); this.setActive('image')}}
              underlayColor='#eeeeee' style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <KlikFonts name='photo' color={color.THEME} size={25}/>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => {SoundEffects.playSound('tick'); this.setActive('stickers')}}
                underlayColor='#eeeeee' style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <KlikFonts name='emoticon' color={color.THEME} size={20}/>
            </TouchableHighlight>
          <View style={styles.messageInputContainer}>
            <TextInput
              ref="commentInput"
              placeholder='Enter your comment.'
              placeholderTextColor='lightgray'
              multiline={true}
              autoFocus={false}
              onFocus={() => {
                        this.setState({ active: 'keyboard'});
                            this.goToBottom();
                        }}
              keyboardAppearance="dark"
              style={[[inputFieldStyle], {color: color.TEXT_DARK}]}
              onChangeText={(text) => this.setState({newMessage: text ? text: false})}
            />
          </View>
          <TouchableHighlight
            onPress={() => {SoundEffects.playSound('tick'); this.onCommentPress()}}
            underlayColor='#eeeeee' style={{flex:1.3, alignItems: 'center', justifyContent: 'center', backgroundColor: color.BUTTON}}>
              <KlikFonts name='send' color={color.BUTTON_TEXT} size={32}/>
          </TouchableHighlight>
        </View>
        {this.renderOptions()}
        <KeyboardSpacer />
      </View>
    );
  }

  renderReactionsRow(reaction) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <Reaction reaction={reaction} key={reaction.UserFID} color={color} />
  }

  renderCommentsRow(comment, sectionId, rowId) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let contentHeight = windowSize.height - this.state.keyboardSpace - NAVBAR_HEIGHT - this.state.reactionsHeight;
    return (
      <View onLayout={(event) => {
        if ((this.state.comments.length - 1) == rowId) {
          if (this._isMounted) {
            this.setState({
              selectedPositionY: event.nativeEvent.layout.y
            });
          }
      }}}>
        <Comment comment={comment} color={color} />
      </View>
    );
  }

  renderOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.active == 'stickers') {
      return (
        <View style={{height: windowSize.height * 0.35, backgroundColor: color.CHAT_BG,
          borderTopWidth: 0.5, borderTopColor: color.HIGHLIGHT}}>
          <Stickers
            height={windowSize.height * 0.35}
            color={color}
            onStickerPress={this.onStickerPress}
            hasInternet={this.props.hasInternet}
          />
        </View>
      );
    } else if (this.state.active == 'image') {
      return this.renderMediaOptions();
    }
  }

  renderMediaOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{height: ddp.keyboardSpace, backgroundColor: color.CHAT_BG,
                    borderTopWidth: 0.5, borderTopColor: color.HIGHLIGHT}}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Button text={'Camera'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.onPressCamera()}
            style={{width: windowSize.width*0.4, marginTop: 20, marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
          <Button text={'Gallery'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.onPressGallery()}
            style={{width: windowSize.width*0.4, marginTop: 20, marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {/*
            (this.state.image)
            ? <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', width: windowSize.height*0.28}}>
                <Close color={color} onPress={this.clearImage}/>
                <Image style={{height: windowSize.height*0.25, width: windowSize.height*0.25}}
                  source={{uri:this.state.image ? this.state.image.uri: null}}
                />
              </View>
            : <KlikFonts name="gallery" size={55} color={color.HIGHLIGHT}/>
          */}
        </View>
      </View>
    );
  }

  connectionStatusHeight() {
    return this.state.connectionStatusHeight;
  }

  comments() {
    return this.state.comments;
  }

  reactions() {
    return this.state.reactions;
  }

  renderModal() {
    if (this.state.isShowMessageModal) {
      return (
        <Modal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    } else if (this.state.isUploading) {
      return (
        <Modal
          theme={this.props.theme}
          setIsShowModal={() => console.log('uploading')}
          modalType={'uploading'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <Modal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  onCommentPress(){
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (!_.isEmpty(this.state.newMessage)) {
        if (this.state.newMessage.trim().length > 0) {
          let newMessage = this.state.newMessage.trim();
          this.setState({newMessage: null});
          this.postComment(newMessage);
        }
      } else{
        this.setIsShowMessageModal('Field is empty.');
      }
    }
  }

  onStickerPress(data){
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      ddp.call('commentOnPost', [this.props.post().id, {'Sticker': data.Image}]);
      this.clearActive();
    }
  }

  postComment(newMessage) {
    let postId = this.props.post().id;
    if (newMessage) {
      ddp.call('commentOnPost', [postId, {'Text': newMessage}]).then(result => {
        if (result) {
          if (this._isMounted) {
            this.setState({newMessage: null});
          }
          this.refs.commentInput.setNativeProps({text:''});
        }
      });
    }
  }

  clearActive() {
    if (this._isMounted) {
      this.refs.commentInput.blur();
      this.setState({active: null});
    }
  }

  commentOnPost(){
    this.goToBottom();
    this.refs.commentInput.focus();
  }

  goToBottom() {
    if (this._isMounted) {
      setTimeout(() => {
        this.refs.commentslistview.scrollTo({y:this.state.selectedPositionY, animated:true});
        // RCTUIManager.measure(this.commentslistview.getInnerViewNode(), (...data) => {
        //   console.log('data', data)
        //   this.commentslistview.scrollTo({y: (data[3]), animated:true})
        // });
      }, 500);
    }
  }

  goBackToTimeline() {
    this.refs.commentInput.blur();
    this.props.navigator.pop();
  }

  closeReact(){
    if(this._isMounted) {
      this.setState({
        reactToggled: false,
        reactHeight: 0,
        reactTopPosition: 0
      });
    }
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    }
  }

  setIsShowReactions(postInfo) {
    if (this._isMounted) {
      this.setState({
        postId: postInfo.id,
        reactHeight: 50,
        reactToggled: !this.state.reactToggled,
        reactTopPosition: (postInfo.height),
      });
    }
  }

  setActive(active){
    this.refs.commentInput.blur();
    this.goToBottom();
    if (this._isMounted) {
      this.setState({active: active});
      this.props.setIsSafeToBack(false);
    }
  }

  onPressCamera() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      launchCamera('photo', (response) => {
        this.checkFileSize(response);
      });
    }
  }

  onPressGallery() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      launchImageLibrary('photo', (response) => {
        this.checkFileSize(response);
      });
    }
  }

  checkFileSize(response) {
    if (typeof response == "string") {
      this.setIsShowMessageModal(response);
    } else {
      this.postImage(response);
      // TODO: use http request
      // let data = response;
      // data['duration'] = 0;
      // data['data'] = '';
      // Send.uploadMedia(data, 'photo', Send.uploadType.mediaComment);
    }
  }

  postImage(response) {
    this.clearActive();

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (this._isMounted) {
        this.setState({isUploading: true});
      }
      let data = response;
      data['sender'] = ddp.user._id;
      data['postId'] = this.props.post().id;
      Send.uploadMedia(data, 'photo', Send.uploadURL.mediaComment, null, null, (result) => {
        if (result == 'Uploading failed.') {
          this.setIsShowMessageModal("Something went wrong. Please try again later.");
        }

        if (this._isMounted) {
          this.setState({isUploading: false});
        }
      });
      // var formData = new FormData();
      // var filename = response.uri.replace(/^.*[\\\/]/, '');

      // formData.append('file', {uri: response.uri, name: filename, type: "image/jpeg"});
      // formData.append('postId', this.props.post().id);
      // formData.append('sender', ddp.user._id);

      // var request = new XMLHttpRequest();
      // var chatURL = ddp.urlUploadComment;

      // request.open("POST", chatURL, true);
      // request.setRequestHeader('sender', ddp.user._id);
      // request.onreadystatechange = function(status){
      //   if (request.readyState !== 4) {
      //     return;
      //   }
      // }
      // if (request.upload) {
      //   request.upload.onprogress = (event) => {
      //     if (event.lengthComputable) {
      //       let uploadProgress = event.loaded / event.total;
      //       if (uploadProgress == 1) {
      //         if (this._isMounted) {
      //           this.setState({isUploading: false});
      //         }
      //       }
      //     }
      //   };
      //   request.upload.onerror = (event) => {
      //     console.log('Upload failed');
      //   };
      //   request.error = (err) => {
      //     console.log('error', err)
      //   }
      // }
      // request.send(formData);
    }
  }
}

const Close = ({color, onPress}) => (
  <View style={{height:25, width:25, position: 'absolute', top: 12, right: 0, zIndex: 1,
                borderRadius: 50, backgroundColor: color.HIGHLIGHT, borderColor: color.TEXT_LIGHT, borderWidth: 0.5}}>
    <KlikFonts name='close' size={24} onPress={() => onPress()} color={color.TEXT_LIGHT}/>
  </View>
);
