'use strict';

import React, {
  Component
} from 'react';
import ReactNative, {
  StyleSheet,
  TouchableHighlight,
  Text,
  View,
} from 'react-native';
import {secondsToTime} from './Helpers';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import FileReader from 'react-native-fs';

const styles = StyleSheet.create(require('./../styles/styles_common'));
class InputAudio extends Component {

  constructor(props) {
    super(props);
    this.state = {
      buttonText: 'Record',
      currentTime: 0.0,
      hasStoppedRecording: false,
      isRecording: false,
      isFinished: false,
    }
    this.prepareRecording = this.prepareRecording.bind(this);
    this.record = this.record.bind(this);
  }

  //Put a flag when component is mounted.
  componentWillMount() {
    this._isMounted = true;
    this.prepareRecording();
  }

  // Put a flag when component will unmount.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Prepare a path to save the audio file
  prepareRecording() {
    let audioPath = AudioUtils.DocumentDirectoryPath + '/test.aac';
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac"
    });
    AudioRecorder.onProgress = (data) => {
      var currentTime = data.currentTime;
      var currentTimeStr = secondsToTime(currentTime);
      this.setState({
        currentTime: currentTime,
        buttonText: currentTimeStr
      });
    };
    AudioRecorder.onFinished = (data) => {
      this.setState({
        isFinished: data.finished,
        buttonText: 'Record'
      });
      this.props.onPressButtonRecordSend(audioPath, 'audio', this.state.currentTime);
    };
  }

  // Render record display
  render() {
    let color = this.props.color;
    let height = (this.props.height > 0) ? this.props.height : 220;

    return (
      <View style={[{height: height, left: 0, right: 0, bottom: 0}]} >
        <View style={styles.container}>
          <TouchableHighlight style={[styles.recordButton, {backgroundColor:color.BUTTON}]}
           onPress={() => this.record()}
           underlayColor={color.HIGHLIGHT}>
            <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}> {this.state.buttonText} </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  // On record button pressed
  record() {
    if (this.state.buttonText == 'Record') {
      AudioRecorder.startRecording();
      if (this._isMounted) {
        this.setState({
          isRecording: true
        });
      }
    } else {
      AudioRecorder.stopRecording();
      if (this._isMounted) {
        this.setState({
          buttonText: 'Record',
          hasStoppedRecording: true,
          isRecording: false
        });
      }
    }
  }
}

module.exports = InputAudio;
