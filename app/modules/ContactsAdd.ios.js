'use strict';
import React, {
  Component,
} from 'react';
import {
  Alert,
  InteractionManager,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import _ from 'underscore';
import ButtonIcon from './common/ButtonIcon';
import ContactsManager from 'react-native-contacts';
import FriendRequest from './FriendRequest';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
import Users from './config/db/Users';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const WINDOW_SIZE = Dimensions.get('window');

class ContactsAdd extends Component {

  // Initial state and bind functions
  constructor(props) {
    super(props);
    this.onPressButtonInvite = this.onPressButtonInvite.bind(this);
    this.state = {
      contactsOnKlikChat: []
    }
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.onPressButtonInvite = this.onPressButtonInvite.bind(this);
    this.onPressButtonSearch = this.onPressButtonSearch.bind(this);
    this.onPressButtonNearby = this.onPressButtonNearby.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getContactsFromPhone();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // Get contacts from phone, only select one number per person
  getContactsFromPhone() {
    var contacts = [];
    ContactsManager.getAll((err, results) => {
      if(err && err.type === 'permissionDenied') {
        consoleLog('Contacts: contacts denied');
      } else {
        for (var i = 0; i < results.length; i++) {
          var isContact = false;
          let phoneNumbers = results[i].phoneNumbers;
          var selectedIndex = 0;
          for (var j = 0; j < phoneNumbers.length; j++) {
            isContact = _.contains(this.props.loggedInUser().contacts, phoneNumbers[j].number);
            if (isContact) {
              selectedIndex = j;
              break;
            }
          }
          let number = phoneNumbers[selectedIndex].number;
          var isOnKlikChat = false;
          Users.subscribeToUsername(number).then(() => {
            let user = Users.getItemByUsername(number);
            let isOnKlikChat = (user != null);
            if (user != null) {
              this.state.contactsOnKlikChat.push(user);
            }
          });
        }
      }
    });
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let numButtons = 3;
    let buttonMargin = 10;
    let buttonWidth = (WINDOW_SIZE.width - (buttonMargin*numButtons*2)) / numButtons;
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Add Contact', tintColor: color.BUTTON_TEXT}}
         rightButton={<ButtonIcon icon='close' color={color} onPress={this.onPressButtonClose}/>} />
        <View style={{height: 1, backgroundColor:color.CHAT_BG}} />
        <View style={{backgroundColor:color.THEME, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
          <TouchableHighlight onPress={this.onPressButtonInvite} underlayColor='transparent'>
            <View style={{flexDirection:'column', alignItems:'center', margin:buttonMargin, width:buttonWidth}}>
              <KlikFonts name='addcontact' color={color.BUTTON_TEXT} size={40} />
              <Text style={{color:color.BUTTON_TEXT,fontSize:11}}>Contact</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.onPressButtonSearch} underlayColor='transparent'>
            <View style={{flexDirection:'column', alignItems:'center', margin:buttonMargin, width:buttonWidth}}>
              <KlikFonts name='searchidphone' color={color.BUTTON_TEXT} size={40}/>
              <Text style={{color:color.BUTTON_TEXT,fontSize:11}}>ID/Phone No.</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.onPressButtonNearby} underlayColor='transparent'>
            <View style={{flexDirection:'column', alignItems:'center', margin:buttonMargin, width:buttonWidth}}>
              <KlikFonts name='nearby' color={color.BUTTON_TEXT} size={40} />
              <Text style={{color:color.BUTTON_TEXT,fontSize:11}}>Nearby</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style={{backgroundColor:color.HIGHLIGHT}}>
          <TouchableHighlight onPress={() => this.onPressButtonAutoAdd()} underlayColor='transparent'>
            <View style={{flexDirection:'row', margin:5, marginLeft:10, marginRight:10}}>
              <View style={{flexDirection:'column', justifyContent:'center'}}>
                <View style={{width:40, height:40, borderRadius:20, alignItems:'center', justifyContent:'center', backgroundColor:color.TEXT_LIGHT}}>
                  <KlikFonts name='addcontact' color={color.BUTTON_TEXT} size={25} />
                </View>
              </View>
              <View style={{flexDirection:'column', justifyContent:'center', margin:10, marginLeft:10}}>
                <Text style={[styles.chatRecentTextNameBold, {fontWeight:'600',color:color.TEXT_DARK}]}>Auto Add Friends</Text>
                <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_LIGHT, fontSize:10}]}>Invite contacts already on KlikChat as friends</Text>
              </View>
            </View>
          </TouchableHighlight>
          <View style={{backgroundColor:color.INACTIVE_BUTTON, height:1}}/>
          <TouchableHighlight onPress={() => this.onPressButtonCreateGroup()} underlayColor='transparent'>
            <View style={{flexDirection:'row', margin:5, marginLeft:10, marginRight:0}}>
              <View style={{flexDirection:'column', justifyContent:'center'}}>
                <View style={{width:40, height:40, borderRadius:20, alignItems:'center', justifyContent:'center', backgroundColor:color.TEXT_LIGHT}}>
                  <KlikFonts name='addgroup' color={color.BUTTON_TEXT} size={25} />
                </View>
              </View>
              <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'space-between',marginTop:10, marginBottom:10, marginLeft:10 }}>
                <View>
                  <Text style={[styles.chatRecentTextNameBold, {fontWeight:'600',color:color.TEXT_DARK}]}>Create a Group</Text>
                  <Text style={[styles.chatRecentTextMessage, {color:color.TEXT_LIGHT, fontSize:10}]}>Create a group with friends</Text>
                </View>
                <KlikFonts name='next' color={color.TEXT_LIGHT} size={20} style={{backgroundColor:'transparent'}}/>
              </View>
            </View>
          </TouchableHighlight>
          <View style={{backgroundColor:color.INACTIVE_BUTTON, height:0.5}}/>
        </View>
        <View style={{height: 50}} />
      
      <FriendRequest navigator={this.props.navigator}
       loggedInUser={this.props.loggedInUser}
       theme={this.props.theme}
       renderConnectionStatus={this.props.renderConnectionStatus}
       users={this.props.users}
       hasInternet={this.props.hasInternet}
       setIsContactsChanged={this.props.setIsContactsChanged}
       setLoggedInUser={this.props.setLoggedInUser} />
      </View>
    );
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPressButtonInvite() {
    this.props.navigator.push({
      id: 'contactsinvite',
      hasInternet: this.props.hasInternet,
      loggedInUser: this.props.loggedInUser,
      renderConnectionStatus: this.props.renderConnectionStatus,
      theme: this.props.theme,
      users: this.props.users
    });
  }

  onPressButtonSearch() {
    this.props.navigator.push({
      // id: 'contactsaddsearch',
      id: 'searchothermember',
      backgroundImageSrc:this.props.backgroundImageSrc,
      getMessageWallpaper: this.props.getMessageWallpaper,
      hasInternet: this.props.hasInternet,
      loggedInUser: this.props.loggedInUser,
      renderConnectionStatus: this.props.renderConnectionStatus,
      shouldReloadData: this.props.shouldReloadData,
      theme: this.props.theme,
      timeFormat: this.props.timeFormat
    });
  }

  onPressButtonAutoAdd() {
    var message = 'Invite ';
    if (this.state.contactsOnKlikChat.length > 0) {
      if (this.state.contactsOnKlikChat.length > 1) {
        message += 'Invite all ' + this.state.contactsOnKlikChat.length + ' friends already on KlikChat?';
      } else {
        message += 'a friend already on KlikChat?'
      }
      Alert.alert(
        'Send Invite',
        message,
        [{text: 'Cancel', null},
         {text: 'OK', onPress: () => this.onPressInviteAllOk()}]
      );
    } else {
      Alert.alert('KlikChat', 'There is no one to be invited.');
    }
  }

  onPressInviteAllOk() {
    console.log(this.state.contactsOnKlikChat);
    for (var i = 0; i < this.state.contactsOnKlikChat.length; i++) {
      var contact = this.state.contactsOnKlikChat[i];

      let sender = this.props.loggedInUser()._id;
      let recipient = contact._id;
      let chatType = GLOBAL.CHAT.DIRECT;
      let message = 'Hi ' + contact.profile.firstName + '! I want to connect with you on KlikChat.'
      Send.textMessage(sender, recipient, chatType, message);
    }
    var message = 'You sent ';
    if (this.state.contactsOnKlikChat.length > 1) {
      message += 'invites to all of your friends already on KlikChat.';
    } else {
      message += 'an invite to a friend already on KlikChat.'
    }
    Alert.alert('Success', message);
  }

  onPressButtonCreateGroup() {
    this.props.navigator.push({
      id: 'contactscreategroup',
      hasInternet: this.props.hasInternet,
      loggedInUser: this.props.loggedInUser,
      renderConnectionStatus: this.props.renderConnectionStatus,
      theme: this.props.theme,
      users: this.props.users
    });
  }

  onPressButtonNearby() {
    this.props.navigator.push({
      id: 'contactsnearby',
      hasInternet: this.props.hasInternet,
      loggedInUser: this.props.loggedInUser,
      renderConnectionStatus: this.props.renderConnectionStatus,
      theme: this.props.theme,
      users: this.props.users
    });
  }
}

module.exports = ContactsAdd;
