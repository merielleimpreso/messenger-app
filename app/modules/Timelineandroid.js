/*

Author: John Rezan Baguna
Date Created: 2016-06-29
Date Updated:
Description: Display posts by friends and writes posts

update: 2016-06-30 by Merielle I.
  - Get data from server
  - Added TimelinePost for displaying of post
update: 2016-07-05 by Merielle I.
  - Added limit in subscribe
update: 2016-07-12 by Merielle I.
  - Loading indicator now disappears when all messages are loaded

*/

'use strict';

import React, {
  Component,
} from 'react';
import {
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
  Dimensions
} from 'react-native';
import _ from 'underscore';
import {toNameCase, timelineTimeOrDate} from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import Post from './config/db/Post';
import TimelinePost from './TimelinePost';
import TimelineReact from './TimelineReact';
import Users from './config/db/Users';
const windowSize = Dimensions.get('window');

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common'));

class Timeline extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isMakingNewStatus: false,
      messages: null,
      theme: this.props.theme(),
      loggedInUser: this.props.loggedInUser(),
      reactToggled: false
    }
    this.renderStatusBox = this.renderStatusBox.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setIsShowReactions = this.setIsShowReactions.bind(this);
    this.commentOnPost = this.commentOnPost.bind(this);
    this.closeReact = this.closeReact.bind(this);
  }

  // Put a flag to check if the component is mounted
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.props.theme() != this.state.theme) {
      //this.state.dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,});
    //  let data = this.getDataToBeDisplayed(this.state.messages);
      if (this._isMounted) {
        console.log("TimeLine didUpdate");
        this.setState({
          //dataSource: this.state.dataSource.cloneWithRows(data),
          theme: this.props.theme()
        });
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  subscribe() {
    if (this.props.hasInternet()) {
      Post.subscribe().then(() => {
        this.observe();
      });
    } else {
      Post.useCache(() => {
        this.observe();
      })
    }
  }

  observe() {
    Post.observe(results => {
      console.log("Timeline observed");
      if (JSON.stringify(results) != JSON.stringify(this.state.messages)) {
        let data = this.getDataToBeDisplayed(results);
        if (this._isMounted) {
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(data),
            messages: results,
          });
        }
      }
    });
  }

  getDataToBeDisplayed(posts) {
    var data = [];
    for (var i = 0; i < posts.length; i++) {
      let post = posts[i];
      let user = Users.getItemById(post.UserFID);

      let postImagesRaw = _.compact(_.pluck(post.Content.Media, 'image'));
      let postVideosRaw = _.compact(_.pluck(post.Content.Media, 'video'));

      let id = post._id;
      let media = _.union(postVideosRaw, postImagesRaw);
      let link = post.Content.Link;
      let sticker = post.Content.Sticker;
      let text = post.Content.Text;
      let isProfilePicture = ( typeof post.Content.isProfilePicture != 'undefined' ) ? post.Content.isProfilePicture : null;
      let userImage = (user.profile.photoUrl) ? user.profile.photoUrl : GLOBAL.DEFAULT_IMAGE;
      let userName = toNameCase(user.profile.firstName) + ' ' + toNameCase(user.profile.lastName);
      let CreateDate = post.CreateDate;
      let reactions = post.Reaction;
      let comments = post.Comments;

      //console.log('getDataToBeDisplayed : ' + isProfilePicture);

      let p = {
        id: id,
        isProfilePicture: isProfilePicture,
        link: link,
        media: media,
        sticker: sticker,
        text: text,
        userImage: userImage,
        userName: userName,
        CreateDate: CreateDate,
        reactions: reactions,
        comments: comments,
      };
      data.push(p);
    }
    return data;
  }

  render() {

    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor: '#f4f1f1'}]}>
        {this.props.renderConnectionStatus()}
        {this.renderStatusBox()}
        {this.renderContent()}
        {this.renderReactions()}
      </View>
    );
  }

  renderStatusBox() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.state.loggedInUser;
    let image = (loggedInUser.profile.photoUrl == '') ? GLOBAL.DEFAULT_IMAGE : loggedInUser.profile.photoUrl;
    let width = windowSize.width;
    return (
      <TouchableHighlight onPress={() => this.goToTimelinePost()} underlayColor='transparent'>
        <View style={[styles.inputLoginContainer, {flexDirection: 'row', backgroundColor: color.CHAT_BG, width: width, marginBottom: 3, paddingTop: 10, paddingBottom: 10, alignItems: 'center'}]}>
          <Text style={ {backgroundColor: 'transparent', width: windowSize.width * 0.6, marginLeft: 15}}>{"What's new today?"}</Text>
          <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
            <KlikFonts
              name="emoticon"
              color={color.LIGHT_TEXT}
              style={{marginLeft:15, alignSelf:'center'}}
              size={20}
              onPress={this.onEmojiPress}
            />
            <KlikFonts
              name="photo"
              color={color.LIGHT_TEXT }
              style={{marginLeft:15, alignSelf:'center'}}
              size={25}
              onPress={this.launchCamera}
            />
            <KlikFonts
              name="gallery"
              color={color.LIGHT_TEXT }
              style={{marginLeft:15, alignSelf:'center'}}
              size={25}
              onPress={this.launchImageLibrary}
            />
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  renderContent() {
    return (
      <ListView ref='listview'
        removeClippedSubviews={true}
        automaticallyAdjustContentInsets={false}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        initialListSize={20}
        enableEmptySections={true}
        onPress={() => {this.setState({reactToggled:false})}}
        onScroll={() => {this.setState({
              reactHeight:0,
              reactToggled: false
            });
        }}
      />
    );
  }

  renderRow(data) {
    return (
      <TimelinePost key={data.id} data={data} 
        theme={this.props.theme} 
        navigator={this.props.navigator}
        commentOnPost={this.commentOnPost}
        setIsShowReactions={this.setIsShowReactions}
      />
    );
  }

  renderReactions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.reactToggled) {
      return <TimelineReact 
                postId={this.state.postId} 
                style={{height:this.state.reactHeight}} 
                closeReact={this.closeReact} 
                reactTopPosition={this.state.reactTopPosition} 
                color={color} />;
    } else { 
      return <View/>;
    }
  }

  commentOnPost(post){
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name:'TimelinePostComments',
        color: color,
        post: post
      })
    })
  }

  closeReact() {
    if (this._isMounted) {
      this.setState({
        postId: null,
        reactHeight: 0,
        reactToggled: !this.state.reactToggled,
        reactTopPosition: 0,
      });
    }
  }

  setIsShowReactions(postInfo) {
    if (this._isMounted) {
      this.setState({
        postId: postInfo.id,
        reactHeight: 50,
        reactToggled: !this.state.reactToggled,
        reactTopPosition: (postInfo.y + postInfo.height),
      });
    }
  }

  goToTimelinePost() {
    this.props.navigator.push({
      name: 'TimelinePoster',
      theme: this.props.theme,
      hasInternet: this.props.hasInternet
    });
  }
}

// Export module
module.exports = Timeline;
