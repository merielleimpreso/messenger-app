'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  InteractionManager,
  Text,
  View
} from 'react-native';
import {getMediaURL} from './Helpers';
import ImageWithLoader from './common/ImageWithLoader';
import Users from './config/db/Users';
const GLOBAL = require('./config/Globals.js');

export default class Reaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getUser();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getUser() {
    let data = this.props.reaction;
    let id = data.UserFID;
    let user = Users.getItemById(id);
    let image = getMediaURL(GLOBAL.DEFAULT_PROFILE_IMAGE);
    if (user) {
      image = getMediaURL(user.image);
    } 
    // else {
    //   Users.subscribeById(id);
    // }
    if (this._isMounted) {
      this.setState({
        image: image,
      });
    }
  }

  getReactionPath(reaction) {
    switch(reaction) {
      case ':love':
        return require('./../images/reactions/love.png');
      case ':happy':
        return require('./../images/reactions/happy.png');
      case ':confused':
        return require('./../images/reactions/confused.png');
      case ':supportive':
        return require('./../images/reactions/supportive.png');
      case ':blank':
        return require('./../images/reactions/blank.png');
      case ':sad':
        return require('./../images/reactions/sad.png');
    }
  }

  render() {
    let imgDim = 34,
        imgRadius = imgDim/2;
    let reaction = this.getReactionPath(this.props.reaction.Reaction);
    let data = this.props.reaction;
    let id = data.UserFID;
    let user = Users.getItemById(id);
    let image = getMediaURL(GLOBAL.DEFAULT_PROFILE_IMAGE);
    if (user) {
      image = getMediaURL(user.image);
    } 
    // else {
    //   Users.subscribeById(id);
    // }
    return (
      <View style={{marginTop: 5, marginLeft: 10, marginBottom: 5, width:imgDim, height:imgDim}}>
        <Image source={{uri: image}} style={{width:imgDim, height:imgDim, borderRadius:imgRadius}} key={this.props.reaction.UserFID}/>
        <Image style={{width:25, height:25, alignSelf: 'flex-end', marginTop: -20, marginRight: -4, zIndex: 1}} source={reaction}/>
      </View>
    );
  }
}
