import React, {
  Component,
} from 'react';
import {
  Alert,
  NavigatorIOS,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  View,
} from 'react-native';
import {
  randomString,
  logTime
} from './Helpers.js';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import ScreenLoading from './ScreenLoading';

var styles = StyleSheet.create(require('./../styles/styles_common.js'));
var GLOBAL = require('./config/Globals.js');

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      number: '',
      password: '',
      confirmPassword: '',
      error: '',
      isSigningup: false
    };
    this.signup = this.signup.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    var color = this.props.color;

    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
          tintColor={color.THEME}
          title={{ title: 'Signup', tintColor: color.BUTTON_TEXT}}
          leftButton={
            <KlikFonts
            name="back"
            color={color.BUTTON_TEXT}
            size={35}
            onPress={() => this.onPressedBack()}
           />
          }
        />
        {this.renderSignupForm()}
        {this.renderSigningup()}

        <KeyboardSpacer getKeyboardStatus={this.setKeyboardStatus} />
      </View>
    );
  }

  renderSignupForm() {
    let color = this.props.color;
    return (
      <ScrollView keyboardDismissMode='interactive'
        keyboardShouldPersistTaps={false}
        style={{flex: 1}}
        automaticallyAdjustContentInsets={false} >
      <View style={styles.containerNoResults}>
        <View style={{height:10}} />
        <Text style={[styles.loginError, {color:color.ERROR}]}>
          {this.state.error}
        </Text>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='firstName'
            autoCapitalize='words'
            autoCorrect={false}
            autoFocus={true}
            onChangeText={(firstName) => this.setState({firstName})}
            onSubmitEditing={(event) => {this.refs.lastName.focus();}}
            placeholder='First Name'
            returnKeyType='next'
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='lastName'
            autoCapitalize='words'
            autoCorrect={false}
            onChangeText={(lastName) => this.setState({lastName})}
            onSubmitEditing={(event) => {this.refs.email.focus();}}
            placeholder='Last Name'
            returnKeyType='next'
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='email'
            autoCapitalize='none'
            autoCorrect={false}
            keyboardType='email-address'
            onChangeText={(email) => this.setState({email})}
            onSubmitEditing={(event) => {this.refs.password.focus();}}
            placeholder='Email'
            returnKeyType='next'
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='number'
            autoCapitalize='none'
            autoCorrect={false}
            keyboardType='number-pad'
            onChangeText={(number) => this.setState({number})}
            onSubmitEditing={(event) => {this.refs.password.focus();}}
            placeholder='Mobile Number'
            returnKeyType='next'
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='password'
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={(password) => this.setState({password})}
            onSubmitEditing={(event) => {this.refs.confirmPassword.focus();}}
            placeholder='Password'
            returnKeyType='next'
            secureTextEntry
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginTop:5, marginRight:40, marginLeft:40}}>
          <TextInput ref='confirmPassword'
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={(confirmPassword) => this.setState({confirmPassword})}
            secureTextEntry
            selectionColor={color.THEME}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            placeholder='Confirm Password'
            rejectResponderTermination={false}
          />
          <View style={[styles.profilePasswordTextBoxSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{height:20}} />
        <TouchableHighlight onPress={() => this.onPressedSignup()}
          style={[styles.loginButtonSignup, {backgroundColor:color.BUTTON}]} underlayColor={color.HIGHLIGHT}>
          <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}>
            Signup
          </Text>
        </TouchableHighlight>
      </View>
      </ScrollView>
    );
  }

  renderSigningup() {
    let color = this.props.color;
    return (
      <Modal open={this.state.isSigningup}
       closeOnTouchOutside={false}
       containerStyle={{
         justifyContent: 'center'
       }}
       modalStyle={{
         borderRadius: 2,
         margin: 20,
         padding: 10,
       }
     }>
       <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
       <Text style={{textAlign: 'center', color: color.BUTTON}}>Signing you up...</Text>
     </Modal>
    )
  }

  setKeyboardStatus(state){
    if (this.isMounted) {
      this.setState({
        keyBoardStatus: state
      });
    }
  }

  onPressedBack() {
    requestAnimationFrame(() => {
      this.refs.firstName.blur();
      this.refs.lastName.blur();
      this.refs.email.blur();
      this.refs.number.blur();
      this.refs.password.blur();
      this.refs.confirmPassword.blur();

      this.props.navigator.pop();
    })
  }

  onPressedSignup() {
    this.setState({
      isSigningup: true
    });
    let emailPattern = /\S+@\S+\.\S+/;
    console.log('Signup ['+ logTime() +']: Signup button was clicked. Checking for errors...');

    if (this.state.firstName == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, first name is blank');
      this.refs.firstName.focus();
      this.setState({
        error: 'Please enter your first name',
        isSigningup: false
      });
    } else if (this.state.lastName == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, last name is blank');
      this.refs.lastName.focus();
      this.setState({
        error: 'Please enter your last name',
        isSigningup: false
      });
    } else if (this.state.email == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, email is blank');
      this.refs.email.focus();
      this.setState({
        error: 'Please enter your email',
        isSigningup: false
      });
    } else if (!emailPattern.test(this.state.email)) {
      console.log('Signup ['+ logTime() +']: Signup failed, email is invalid');
      this.refs.email.focus();
      this.setState({
        error: 'Your email is invalid',
        isSigningup: false
      });
    } else if (this.state.number == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, number is blank');
      this.refs.number.focus();
      this.setState({
        error: 'Please enter your mobile number',
        isSigningup: false
      });
    } else if(this.state.number.length < 6){
      console.log('Signup ['+ logTime() +']: Signup failed, number is less than the required number of characters');
      this.refs.number.focus();
      this.setState({
        error: 'Number should be at least 6 digits.',
        isSigningup: false
      });
    }else if (this.state.password == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, password is blank');
      this.refs.password.focus();
      this.refs.password.clear();
      this.refs.confirmPassword.clear();
      this.setState({
        error: 'Please enter your password',
        isSigningup: false
      });
    } else if (this.state.password.length < 6) {
      console.log('Signup ['+ logTime() +']: Signup failed, password is less than the required number of characters');
      this.refs.password.focus();
      this.refs.password.clear();
      this.refs.confirmPassword.clear();
      this.setState({
        error: 'Password should be at least 6 characters long.',
        isSigningup: false
      });
    } else if (this.state.confirmPassword == '') {
      console.log('Signup ['+ logTime() +']: Signup failed, confirm password is blank');
      this.refs.confirmPassword.focus();
      this.refs.password.clear();
      this.refs.confirmPassword.clear();
      this.setState({ error: 'Please confirm your password',
        isSigningup: false });
    } else {
      console.log('Signup ['+ logTime() +']: Signup failed, password does not match');
      if (this.state.password != this.state.confirmPassword) {
        this.refs.password.focus();
        this.refs.password.clear();
        this.refs.confirmPassword.clear();
        this.setState({ error: 'Your password does not match',
          isSigningup: false });
      } else {
        this.signup();
      }
    }
  }

  signup() {
    console.log('Signup ['+ logTime() +']: Signing up...');
    let {firstName, lastName, email, number, password } = this.state;
    ddp.signUpWithUsername(number, password, email, firstName, lastName, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        console.log('Signup ['+ logTime() +']: Sign up successful');
        Alert.alert(
          'Congratulations!',
          'You can now login to your newly created account and verify it.',
          [{text: 'OK', onPress: () => this.onPressedBack()}]
        )
        if (this._isMounted) {
          this.setState({
            isSigningup: false
          });
        }
      } else {
        console.log('Signup ['+ logTime() +']: Sign up failed: ' + err.reason);
        if (this._isMounted) {
          this.setState({
            isSigningup: false,
            error:  err.reason
          });
        }
      }
    });
  }
}

module.exports = Signup;
