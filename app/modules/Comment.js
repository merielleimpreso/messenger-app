'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  InteractionManager,
  Text,
  View
} from 'react-native';
import {getDisplayName, timelineTimeOrDate, getMediaURL, toNameCase} from './Helpers';
import ImageWithLoader from './common/ImageWithLoader';
import Users from './config/db/Users';
const GLOBAL = require('./../modules/config/Globals.js');
const windowSize = Dimensions.get('window');
const IMAGE_WIDTH = 150;

export default class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false,
      height: 150,
      width: 150
    };
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getUser();
      this.getImageSize();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getUser() {
    let comment = this.props.comment;
    let id = comment.UserFID;
    let user = Users.getItemById(id);
    let image = getMediaURL(GLOBAL.DEFAULT_PROFILE_IMAGE);
    let name;
    if (user) {
      image = getMediaURL(user.image);
      name = user.name;//getDisplayName(user);
    } 
    // else {
    //   Users.subscribeById(id);
    // }
    if (this._isMounted) {
      this.setState({
        name: name,
        image: image
      })
    }
  }

  getImageSize() {
    let comment = this.props.comment;

    if (comment.Content.Image) {
      let image = getMediaURL(comment.Content.Image.medium)
      Image.getSize(image, (actualWidth, actualHeight) => {
        var height = IMAGE_WIDTH * (actualHeight / actualWidth);
        if (this._isMounted) {
          this.setState({
            height: height,
          });
        }
      }, (error) => {
          console.log(error);
        }
      );
    }
  }

  render(){
    let comment = this.props.comment;
    let color = this.props.color;
    // console.log("CreateDate: ", this.props.comment.CreateDate);
    let time = timelineTimeOrDate(this.props.comment.CreateDate, 'MM/DD HH:mm');
    let stickerUri = comment.Content.Sticker ? getMediaURL(comment.Content.Sticker) : '';
    let imgUri = comment.Content.Image ? getMediaURL(comment.Content.Image.medium) : '';
    let imgDim = 32,
        imgRadius = imgDim/2;

    return (
      <View style={{width: windowSize.width * 0.98, borderWidth:0, marginTop: 8, marginBottom:10, paddingLeft:10, backgroundColor: '#f4f1f1' }}>
        <View style={{flexDirection:'row', marginBottom:0}}>
          <ImageWithLoader image={this.state.image} color={color} style={{width:imgDim, height:imgDim, borderRadius:imgRadius}} />
          <View style={{flex:1,paddingLeft:8}}>
            <View style={{flexDirection:'row', alignItems:'center', marginBottom:2}}>
              <Text style={{color:color.TEXT_DARK}}>{this.state.name}</Text>
              <Text style={{fontSize:9, color:color.TEXT_LIGHT, marginLeft:10}}>{time}</Text>
            </View>
            {(() => {
              if (comment.Content.Text) {
                return <Text style={{fontSize:16, color:color.TEXT_LIGHT}}>{comment.Content.Text}</Text>
              }
            })()}
            {(() => {
              if (stickerUri) {
                return (
                  <ImageWithLoader image={stickerUri} color={color}
                    style={{width:100, height:100, borderRadius:50}}
                    sticker={true} />
                )
              }
            })()}
            {(() => {
              if (imgUri) {
                return (
                  <ImageWithLoader image={imgUri} color={color} style={{width: this.state.width, height: this.state.height}} />
                )
              }
            })()}
          </View>
        </View>
      </View>
    )
  }

}
