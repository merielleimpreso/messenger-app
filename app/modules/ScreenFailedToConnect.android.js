/*
Author: Merielle Impreso
Date Created: 2016-05-10
Date Updated: NA
Description: This module is displayed when the app cannot connect to server.
  Used index.ios.js, LoggedOut.ios.js
update: 2016-05-17 (Merielle I.)
  - Added text as props to have flexible messages
update: 2016-06-09 (Merielle I.)
  - Added color theme
*/

'use strict';

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// var styles = StyleSheet.create(require('./../styles/styles_common.js'));
const styles = StyleSheet.create(require('./../styles/styles_common.js'));

var theme = 'NORMAL';
var GLOBAL = require('./config/Globals.js');
var COLOR = GLOBAL.COLOR_THEME[theme];

var ScreenFailedToConnect = React.createClass({
  render: function() {
    var renderReloadButton = <View />;
    if (this.props.reload) {
      renderReloadButton = <Icon name="ios-loop-strong"
        color={COLOR.THEME}
        size={30}
        style={styles.reload}
        onPress={() => this.props.reload()}/>
    }
    return (
      <View style={styles.container}>
        <Text style={styles.loginButtonText}> {this.props.text} </Text>
        {renderReloadButton}
      </View>
    )
  }
});

module.exports = ScreenFailedToConnect;
