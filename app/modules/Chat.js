'use strict';

import React, {
  Component
} from 'react';
import ReactNative, {
  Clipboard,
  ListView,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import {renderFooter} from './Helpers';
import ChatRow from './ChatRow';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import ScreenNoItemFound from './ScreenNoItemFound';
import RenderModal from './common/RenderModal';

const GLOBAL = require('./config/Globals.js');

class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      textToCopy: "",
      showModal: false,
    }
    this.render = this.render.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderMessageOptions = this.renderMessageOptions.bind(this);
    this.setShowModal = this.setShowModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.copyText = this.copyText.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  render() {
    let color = this.props.color;
    return (
      <View style={{height:this.props.height}}>

        {this.renderContent()}
        {this.renderMessageOptions()}
      </View>
    )
  }

  renderContent() {
    if (!this.props.isLoading && this.props.dataSource.getRowCount() == 0) {
      let color = this.props.color;
      return <ScreenNoItemFound isLoading={false} text={'No messages'} color={color}/>;
    } else {
      return (
        <ListView ref='listview'
         removeClippedSubviews={true}
         automaticallyAdjustContentInsets={false}
         renderScrollComponent={props => <InvertibleScrollView {...props} inverted />}
         dataSource={this.props.dataSource}
         renderRow={this.renderRow}
         renderFooter={this.renderFooter}
         initialListSize={20}
         onEndReached={this.loadMore}
         />
      );
    }
  }

  loadMore() {
    this.props.loadMore();
  }

  renderRow(chat) {
    return (
      <ChatRow chat={chat}
        key={chat._id}
        myID={this.props.loggedInUser()._id}
        users={this.props.users}
        chatType={this.props.chatType}
        navigator={this.props.navigator}
        color={this.props.color}
        imageGallery={this.props.imageGallery}
        hasInternet={this.props.hasInternet}
        timeFormat={this.props.timeFormat}
        loggedInUser={this.props.loggedInUser}
        renderConnectionStatus={this.props.renderConnectionStatus}
        shouldReloadData={this.props.shouldReloadData}
        acceptRequest={this.props.acceptRequest}
        theme={this.props.theme}
        changeTheme={this.props.changeTheme}
        getMessageWallpaper={this.props.getMessageWallpaper}
        backgroundImageSrc={this.props.backgroundImageSrc}
        setShowModal={this.setShowModal}
      />
    );
  }

  renderMessageOptions(messageData) {
    let color = this.props.color;
    if(this.state.showModal) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.hideModal}
          modalType={'messageOptions'}
          copyText={this.copyText}
        />
      );
    }
  }

  renderFooter() {
    if (this.props.isAllLoaded) {
      return renderFooter(this.props.isAllLoaded, this.props.color, this.props.hasInternet());
    } else {
      return renderFooter(this.props.isLoading, this.props.color, this.props.hasInternet());
    }
  }

  setShowModal(show,text) {
    this.setState({
      showModal: show,
      textToCopy: text
    });
  }

  hideModal() {
    this.setState({
      showModal: false,
      textToCopy: ""
    });
  }

  copyText() {
    Clipboard.setString(this.state.textToCopy);
    this.setState({ showModal: false});
  }
}

module.exports = Chat;
