'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import _ from 'underscore';
import {toNameCase} from './Helpers';
import ButtonIcon from './common/ButtonIcon';
import Countries from './../modules/config/db/Countries';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import SearchBar from './SearchBar';
import Send from './config/db/Send';
import Users from './config/db/Users';

const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const GLOBAL = require('./config/Globals.js');

class ContactsAddSearch extends Component {

  // Initial state and bind functions
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      searchText: '',
      searchOption: 0,
      selectedCountry: 'Philippines',
      isLoadingSearch: false,
      shouldDisplayResult: false,
      loading: true,
      isLoaded: false
    }
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onPressButtonSearch = this.onPressButtonSearch.bind(this);
    this.setSelectedCountry = this.setSelectedCountry.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let radioProps = [ {label: 'ID', value: 0 }, {label: 'Phone Number', value: 1 }];

    let keyboardType = (this.state.searchOption == 0) ? 'default' : 'phone-pad';
    var placeholder = "Enter your friend's ";
    placeholder += (this.state.searchOption == 0) ? "ID" : "phone number";

    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Search by ID/Phone No.', tintColor: color.BUTTON_TEXT}}
         leftButton={<ButtonIcon icon='back' color={color} onPress={() => this.onPressButtonBack()} size={25}/>} />
        <View style={{marginRight:10, marginLeft:10, marginTop:15}}>
          <RadioForm formHorizontal={true} animation={true}>
            {radioProps.map((obj, i) => {
              var that = this;
              var is_selected = this.state.searchOption == i;
              return (
                <View key={i} style={styles.radioButtonWrap}>
                  <RadioButton
                    isSelected={is_selected}
                    obj={obj}
                    index={i}
                    labelHorizontal={true}
                    buttonColor={color.THEME}
                    buttonSize={12}
                    borderWidth={1.5}
                    labelColor={color.THEME}
                    style={[i !== radioProps.length-1 && styles.radioStyle]}
                    onPress={(value, index) => {
                      this.refs.search.blur();
                      this.setState({
                        searchOption: index
                      });
                    }}
                  />
                </View>
              )
            })}
          </RadioForm>
        </View>
        <View style={{height:10}}/>
        {this.renderCountrySelection()}
        <View style={[styles.searchBar, {backgroundColor:color.HIGHLIGHT}]}>
          <TextInput
            ref='search'
            autoCapitalize="none"
            autoCorrect={false}
            placeholder={placeholder}
            placeholderTextColor={color.TEXT_LIGHT}
            color={color.TEXT_DARK}
            onFocus={this.props.onFocus}
            style={styles.searchBarInput}
            selectionColor={color.TEXT_DARK}
            onChangeText={(search) => this.onSearchChange(search)}
            keyboardType={keyboardType}
            returnKeyType='search'
          />
          {this.renderRightMostButton()}
        </View>
        {this.renderResult()}
      </View>
    );
  }

  renderCountrySelection() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let countries = Countries.country;
    let selectedCountry = _.findWhere(countries, {name: this.state.selectedCountry});
    let value = selectedCountry.name + ' (' + selectedCountry.dial_code + ')';

    if (this.state.searchOption == 1) {
      return (
        <TouchableHighlight style={{marginBottom:5}} onPress={() => this.onPressCountry()} underlayColor='transparent'>
          <View style={[styles.searchBar, {backgroundColor:color.HIGHLIGHT}]}>
            <TextInput
              refs="country"
              autoCapitalize="none"
              autoCorrect={false}
              value={value}
              placeholderTextColor={color.TEXT_LIGHT}
              color={color.TEXT_DARK}
              onFocus={this.props.onFocus}
              style={styles.searchBarInput}
              selectionColor={color.TEXT_DARK}
              onChangeText={(search) => this.onSearchChange(search)}
              editable={false}
            />
            <KlikFonts name='next' color={color.THEME} size={30}/>
          </View>
        </TouchableHighlight>
      );
    } else {
      return null;
    }
  }

  renderRightMostButton() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.isLoadingSearch) {
      return <ActivityIndicator animating={this.props.isLoadingSearch} style={styles.searchBarSpinner} color={color.BUTTON}/>;
    } else {
      return (
        <TouchableHighlight onPress={() => this.onPressButtonSearch()} underlayColor='transparent'>
          <KlikFonts name='search' color={color.THEME} size={30}/>
        </TouchableHighlight>
      );
    }
  }

  renderResult() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.result) {
      let image = this.state.result.profile.photoUrl;
      let name = toNameCase(this.state.result.profile.firstName) + ' ' + toNameCase(this.state.result.profile.lastName);
      return (
        <View style={{flex:1, margin:10, alignItems:'center'}}>
          <Image source={{uri:image}} style={{width:80, height:80, borderRadius: 40}}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
            {(this.state.loading && !this.state.isLoaded) 
              ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:80, width:80}}>
                  <ActivityIndicator size="small" color={color.BUTTON} />
                </View>
              : null
            }
          </Image>
          <Text style={{color:color.TEXT_DARK, margin:5}}>{name}</Text>
          <TouchableHighlight onPress={() => this.onPressButtonAdd()}
           style={{backgroundColor:color.BUTTON, width:120, alignItems:'center'}}
           underlayColor={color.HIGHLIGHT}>
            <Text style={{color:color.BUTTON_TEXT, margin:7, fontWeight:'500'}}>Add</Text>
          </TouchableHighlight>
        </View>
      );
    } else {
      if (this.state.shouldDisplayResult) {
        return (
          <View style={[styles.containerNoResults]}>
            <Text style={[styles.textNoResults, {color:color.TEXT_LIGHT}]}>No user found</Text>
          </View>
        );
      } else {
        return null;
      }
    }
  }

  onPressButtonBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onSearchChange(searchText) {
    if (this._isMounted) {
      this.setState({
        searchText: searchText,
        shouldDisplayResult: false
      })
    }
  }

  onPressCountry() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'inputcountrycode',
        theme: this.props.theme,
        selectedCountry: this.state.selectedCountry,
        setSelectedCountry: this.setSelectedCountry
      });
    });
  }

  onPressButtonSearch() {
    this.refs.search.blur();
    let searchText = this.state.searchText;
    if (this._isMounted) {
      this.setState({
        isLoadingSearch: true,
      });
    }
    var searchType = this.state.searchOption == 0 ? 'id' : 'number';
    Users.subscribeSearchUser(searchText, searchType).then(() => {
      let search = Users.searchUser(searchText, searchType);
      var result = null;
      if (search) {
        if (!_.contains(this.props.loggedInUser().contacts, search._id)) {
          result = search;
        }
      }
      if (this._isMounted) {
        this.setState({
          isLoadingSearch: false,
          result: result,
          shouldDisplayResult: true
        });
      }
    });
  }

  onPressButtonAdd() {
    Alert.alert(
      'KlikChat Invitation',
      'Send an invitation to ' + toNameCase(this.state.result.profile.firstName) + '?',
      [{text: 'Cancel', onPress: () => {}},
      {text: 'Invite', onPress: () => this.onPressButtonSendInvite()}]
    );
  }

  onPressButtonSendInvite() {
    let sender = this.props.loggedInUser()._id;
    let recipient = this.state.result._id;
    let chatType = GLOBAL.CHAT.DIRECT;
    let message = 'Hi ' + toNameCase(this.state.result.profile.firstName) + '! I want to connect with you on KlikChat.'
    Send.textMessage(sender, recipient, chatType, message);
    Alert.alert(
      'Success',
      'You successfully send an invite to ' + toNameCase(this.state.result.profile.firstName) + '.',
      [{text: 'OK', onPress: () => {}}]
    );
  }

  setSelectedCountry(country) {
    this.setState({
      selectedCountry: country.name
    });
  }
}

module.exports = ContactsAddSearch;
