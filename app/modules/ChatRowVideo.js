/*

Author: Ida Aurea Jimenez
Date Created: 2016-08-17
Date Updated: NA
Description: Display video depending on its actual size.
Used In: ChatRow.js

*/

import React, {
  Component
} from 'react';
import ReactNative, {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Platform
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ImageWithLoader from './common/ImageWithLoader';

const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const GLOBAL = require('./config/Globals.js');

class ChatRowVideo extends Component {

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoading: false,
      progress: 0,
    }
  }

  // Display image.
  render() {
    var color = this.props.color;

    var loader = this.state.loading
      ? null
      : <View style={{alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
          <KlikFonts name="play" color={'#fff'} size={30} />
          <Text style={{fontSize:20, fontWeight:'600', color:'#fff', marginLeft:10}}>PLAY</Text>
        </View> ;

    if (this.state.error) {
      return (
        <View style={[styles.chatImageContainer, {backgroundColor: '#000'}]}>
          <View style={[styles.chatImage, {width:200, height: 200}]}>
            <Text style={{color:color.BUTTON, alignSelf:'center', textAlign:'center', fontSize:12}}>Ooopss!</Text>
            <Text style={{color:color.BUTTON, alignSelf:'center', textAlign:'center', fontSize:10}}>{this.state.error}</Text>
          </View>
        </View>
      );
    } else {
      if (this.props.chat.isTemporary) {
        if (this.props.chat.isFailed) {
          return (
            <View style={[styles.chatImage, {width:200, height: 200, backgroundColor:'#000000'}]}>
              <Text style={{color:color.BUTTON, fontSize:10, textAlign: 'center'}}>Failed uploading video.{'\n'}Please try again later.</Text>
            </View>
          );
        } else {
          return (
            <View style={[styles.chatImage, {width:200, height: 200, backgroundColor:'#000000'}]}>
              <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
              <Text style={{color:color.BUTTON, fontSize:10}}>Uploading...</Text>
              <Text style={{color:color.BUTTON}}>{Math.round(100 * this.props.chat.progress)}%</Text>
            </View>
          );
        }
      } else {
        let video = this.props.video;
        let urlParts = video.split('/');
        let videoThumbUri = 'http://' + GLOBAL.SERVER_HOST + ':' + GLOBAL.SERVER_PORT + '/vt/' + urlParts[3] + '/' + urlParts[4];

        return (
          <TouchableHighlight onPress={() => this.props.playVideo(video)}
          style={[styles.chatImageContainer, {backgroundColor: color.HIGHLIGHT}]}
          underlayColor={color.HIGHLIGHT}>
            <Image source={{uri: videoThumbUri}} borderRadius={10} key={videoThumbUri} style={[styles.chatImage, {width:200, height: 200}]}>
                {loader}
            </Image>
          </TouchableHighlight>
        );
      }
    }
  }
}

// Export module
module.exports = ChatRowVideo;
