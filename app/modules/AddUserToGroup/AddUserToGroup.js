'use strict';

import React, { Component } from 'react';
import {
  InteractionManager,
  ListView,
  Platform,
  ToastAndroid
} from 'react-native';
import _ from 'underscore';
import AddUserToGroupRender from './AddUserToGroupRender';
import Users from './../config/db/Users';

export default class AddUserToGroup extends Component {
  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 }),
      contactsChosen: [],//user object list of chosen list
      text: "",
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isShowModalLoading: false,
      loading: true,
      isAllLoaded: false,
      buttonText: 'Add',
      loggedInUser: this.props.loggedInUser(),
      users: this.props.groupUsers
    }
  }

  //get the data before rendering
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getGroupUsers();
    });
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  getGroupUsers() {
    if (this.props.addOrRemove == 'Add') {
      ddp.call('getUsersToBeAddedToGroup', [this.props.group._id]).then(result => {
        if (this._isMounted) {
          this.setState({users: result});
        }
        this.setData();
      });
    } else {
      this.setData();
    }
  }

  setData() {
    let users = this.state.users;
    let loggedInUserId = this.state.loggedInUser._id;
    let groupUsers = _.reject(users, function(user){ return user._id == loggedInUserId; });

    if (this._isMounted) {
      this.setState({
        isAllLoaded: true,
        buttonText: this.props.addOrRemove,
        users: groupUsers,
        dataSource: this.state.dataSource.cloneWithRows(groupUsers)
      })
    }
  }

  // Search contact
  filterWithSearchText(text) {
    text = (text.trim() == '') ? '' : text;
    var contact;
    var tempRawDataSource = this.state.users.slice();//Makes a copy of the rawDataSource so we don't have to getContacts over n over

    for(var index =0; index < tempRawDataSource.length; index++) {
      // search contact by name
      contact = tempRawDataSource[index].name;

      if( (contact).toLowerCase().indexOf((text).toLowerCase())===-1 ) {
        tempRawDataSource.splice(index, 1);
        index=-1;
      }
    }
    if (this._isMounted) {
      this.setState({
        text: text,
        dataSource: this.state.dataSource.cloneWithRows(tempRawDataSource)
      });
    }
  }

  // Add or Remove user from the group
  processContact(contact) {
    let tempContactsChosen = this.state.contactsChosen.slice();

    if( tempContactsChosen.indexOf(contact._id)==-1 ) {
      tempContactsChosen.push(contact._id);
    } else {
      tempContactsChosen.splice(tempContactsChosen.indexOf(contact._id), 1);
    }

    if (this._isMounted) {
      this.state.dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setState({
        users: this.state.users,
        dataSource: this.state.dataSource.cloneWithRows(this.state.users),
        contactsChosen: tempContactsChosen
      });
    }
    console.log('contactsChosen', tempContactsChosen);
    this.filterWithSearchText(this.state.text);
  }

  // Create the group and redirect to GroupMessages.android.js
  submit() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else if (this.state.contactsChosen.length <= 0) {
      this.props.navigator.pop();
      let action = (this.props.addOrRemove == 'Add') ? 'add' : 'remove';
      if (Platform.OS === 'ios') {
        this.setIsShowMessageModal(`Nothing to ${action}`);
      } else {
        ToastAndroid.show(`Nothing to ${action}`, ToastAndroid.LONG);
      }
    } else {
      if (this.props.addOrRemove == 'Remove') {
        if ((this.state.users.length - this.state.contactsChosen.length) <= 1) {
          this.setIsShowMessageModal('Cannot remove member. There should be atleast 3 members in a group.');
        } else {
          this.editGroupMembers();
        }
      } else {
        this.editGroupMembers();
      }
    }
  }

  editGroupMembers() {
    let action = (this.props.addOrRemove == 'Add') ? 'add' : 'remove';
    let options = [this.props.group._id, this.state.contactsChosen, action]
    this.setIsShowModalLoading();
    ddp.call('editGroupUsers', options).then(result => {
      if (result) {
        if (this._isMounted) {
          this.setState({users: result});
          this.setIsShowModalLoading(true);
        }
        this.props.setGroupUsers(result, action);
      }
    }).catch(err => {
      console.log(`${this.props.addOrRemove} user from group Error: `, err);
      this.setIsShowModalLoading();
      this.setIsShowMessageModal('Something went wrong, please try again later.');
    });
  }

  render() {
    return (
      <AddUserToGroupRender 
        alertMessage={this.state.alertMessage}
        buttonText={this.state.buttonText}
        contactsChosen={this.state.contactsChosen}
        dataSource={this.state.dataSource}
        filterWithSearchText={this.filterWithSearchText.bind(this)}
        isAllLoaded={this.state.isAllLoaded}
        isLoading={this.state.isLoading}
        isShowCheckConnection={this.state.isShowCheckConnection}
        isShowModalLoading={this.state.isShowModalLoading}
        isShowMessageModal={this.state.isShowMessageModal}
        onActionSelected={this.submit.bind(this)}
        processContact={this.processContact.bind(this)}
        setIsShowCheckConnection={this.setIsShowCheckConnection.bind(this)}
        setIsShowMessageModal={this.setIsShowMessageModal.bind(this)}
        text={this.state.text}
        toolbarActions={[{title: 'Submit', iconName: 'check', show: 'always'}]}
        {...this.props}
      />
    );
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsShowModalLoading(shouldGoBack = false) {
    if (this._isMounted) {
      this.setState({isShowModalLoading: !this.state.isShowModalLoading});
      shouldGoBack && this.props.navigator.pop();
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }
};

// Display for empty records
var NoContacts = React.createClass({
  render: function() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var text = '';
    if (this.props.filter) {
      text = `No results for "${this.props.filter}"`;
    } else if (!this.props.isLoading) {
      text = 'No contacts found';
    }

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={{marginTop: 20, color: color.TEXT_LIGHT}}>{text}</Text>
      </View>
    );
  }
});

