import React, { Component } from 'react';
import {
  Dimensions,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import { getMediaURL, renderFooter } from './../Helpers.js';
import GLOBAL from './../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './../common/ListRow';
import RenderModal from './../common/RenderModal';
import styles from './styles';
import Toolbar from './../common/Toolbar';
const windowSize = Dimensions.get('window');

export default class AddUserToGroup extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        <Toolbar title={this.props.buttonText} 
          withBackButton={true} 
          rightIcon={'check'}
          onPressRightButton={this.props.onActionSelected} 
          {...this.props} 
        />
        <View style={styles.searchContainer}>
          <KlikFonts name="addcontact" style={[styles.contactIcon, {color: color.THEME}]}/>
          <TextInput
            placeholder={`${this.props.buttonText} people to the group`}
            placeholderTextColor={color.TEXT_LIGHT}
            style={[styles.input, {color: color.TEXT_DARK}]}
            onChangeText={(text) => this.props.filterWithSearchText(text)}
            value={this.props.text}
            underlineColorAndroid={color.THEME} />
        </View>
        {this.renderListView()}
        {this.renderModal()}
      </View>
    );
  }

  renderListView() {
    return (
      <ListView
        ref="listview"
        keyboardShouldPersistTaps={true}
        dataSource={this.props.dataSource}
        enableEmptySections={true}
        renderFooter={this.renderFooter.bind(this)}
        renderRow={this.renderRow}
        removeClippedSubviews={false}
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.props.dataSource.getRowCount(0) == 0) ? 'No contact found' : null;
    return renderFooter(this.props.isAllLoaded, color, this.props.hasInternet(), notFoundText);
  }

  renderRow(contact) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let icon = (this.props.contactsChosen.indexOf(contact._id) == -1) ? 'unselected' : 'selected';

    return (
      <ListRow labelText={contact.name}
        detailText={contact.statusMessage}
        image={getMediaURL(contact.image)}
        color={color}
        addSeparator={true}
        icon={icon}
        onPress={() => this.props.processContact(contact)}
        iconPress={() => this.props.processContact(contact)}
      />
    );
  }

  renderModal() {
    if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowModalLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loading'}
        />
      );
    } else if (this.props.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.props.setIsShowMessageModal("")}
         message={this.props.alertMessage}
         modalType={'alert'}
        />
      );
    }
  }
}