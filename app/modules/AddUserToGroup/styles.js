import { Dimensions, StyleSheet } from 'react-native';
const windowSize = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  searchContainer: {
    flexDirection: 'row',
    width: windowSize.width - 20,
    margin: 10, 
    alignItems: 'center'
  },
  input: {
    flex: 1,
    height: 40,
    padding: 5,
  },
  contactIcon: {
    fontSize: 30,
  },
});