/**
 * Author: John
 * TypingArea to be used in chatItems
 * params as of now:
 * from - user ID of the sender
 * to - user ID of the recipient
 * onSendCallback - callback when message is successfully sent
 * chatType - GLOBAL.CHAT.DIRECT or GLOBAL.CHAT.GROUP [updated by Merielle]
 *
 * update: 2016/05/03 (by Ida)
 *  - Added ProgressBarAndroid for when sending
   update: 2016-05-03 (by Tere)
    -Fixed double declaration of noData state
   update: 2016-05-04 (by Ida)
    - Hide 'Sending...' for ios
   update: 2016-05-05 (by Ida)
    - Modified UI color scheme
   update: 2016-05-12 (by Ida)
   - Enhance on TypingArea, Stickers, Keyboard display handling
   update: 2016-05-13 (by Ida)
   - Fixed issue on not sending messages
   - Removed loader
   update: 2016-05-23 (by Ida)
   - added function for attaching video, will wait for the server's function to insert video
   update: 2016-05-27 (by Ida)
   - fix stickers not scrolling that leads to stickers not rendered completely
   update: 2016-06-09 (by Ida)
   - Added function to send video via xhr
   update: 2016-05-15 (by Tere)
   -replaced images for buttons with version 2 of each. (send2, text2 etc)
  update: 2016-06-16 (Ida)
  - Changed color scheme similar to whatsapp
  update: 2016-06-28 (ida)
  - Implement sending like
  update: 2016-07-01 (ida)
  - Implement message indicator
  update: 2016-07-13 (ida)
  - Fixed sending of spaces only
  update: 2016-07-21 (tere)
  -Applied fix to large white space.
  update: 2016-07-22
  -Fixed white space
  update: 2016-08-01 (ida)
  - Added onPressSticker()
  update: 2016-08-16 (ida)
  - Added store to cache for sending of sticker and like
  update: 2016-08-22 (ida)
  - Moved DirectoryExplorer to another js file
 */

'use strict';

import _ from 'underscore';
import file from './config/data.json';
import AudioRecorder from './AudioRecording';
import DirectoryExplorer from './DirectoryExplorer';
import FileReader from 'react-native-fs';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import Modal from 'react-native-simple-modal';
import React, { Component } from 'react';
import Send from './config/db/Send';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  Text,
  TextInput,
  Image,
  InteractionManager,
  Dimensions,
  BackAndroid,
  ActivityIndicator,
  ScrollView
} from 'react-native';
var Helpers = require('./Helpers.js');
var GLOBAL = require('./../modules/config/Globals.js');
var ImagePickerManager = require('NativeModules').ImagePickerManager;

var CryptoJS = require("crypto-js");
var styles = StyleSheet.create(require('./../styles/styles.main'));
var windowSize = Dimensions.get('window');
var watchID;
var RNFS = require('react-native-fs');

class TypingArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowRecording: false,
      isShowKeyboard: false,
      canEditTextInput: true,
      newMessage: '',
      addKeyboardSpace:  false,
      isEmojiPress: false, //local. this.props.setIsEmojiPress does not reflect
      isOptionsPress: false,// this.props.isEmojiPress() in this frame, only at
      isRecordPress: false, //next frame. these are reset to false every setIsShowKeyboard
      isSendingText: false,
      inputFieldHeight: 30,
    }
    this.setIsShowRecording = this.setIsShowRecording.bind(this);
    this.setIsShowKeyboard = this.setIsShowKeyboard.bind(this);
    this.sendAudio = this.sendAudio.bind(this);
    this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onPressSendIcon = this.onPressSendIcon.bind(this);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({isShowRecording: this.props.isRecordPress()});
    // this.props.setTypingAreaCallBack(this.backButtonPressed());
  }

  componentWillUnmount() {
    this._isMounted = false;
    BackAndroid.removeEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  backAndroidHandler() {
    if (this.state.isShowRecording) {
      this.setIsShowRecording();
      this.props.setIsSafeToBack(true);
      return true;
    } else if (this.props.isEmojiPress()) {
      this.props.setIsEmojiPress();
      // this.props.setIsSafeToBack(true);
      return true;
    } else if (this.props.isOptionsPress()) {
      this.props.setIsOptionsPress();
      // this.props.setIsSafeToBack(true);
      return true;
    } else if (this.props.isRecordPress()) {
      this.props.setIsRecordPress('typingArea');
      this.props.setIsSafeToBack(true);
      return true;
    }
    // return false;
  }

  // Make unique ID
  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 13; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  onSendPress() {
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;

    if ((this.state.newMessage!=null && this.state.newMessage.trim().length != 0)) {
      let message = this.state.newMessage;
      this.state.newMessage = null;
      requestAnimationFrame(() => {
        Send.textMessage(sender, recipient, chatType, message, true, () => {
          this.setState({
                    isSendingText: false,
                    inputFieldHeight: 30
                    });
        });
      });
    }
  }

  sendAudio(audio, duration) {
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    let type = 'audio';

    Send.media(sender, recipient, chatType, audio, type, duration, null, 0, !this.props.hasInternet());
    this.setIsShowRecording();
  }

  // send(mediaDetails) {

  //   let sender = this.props.loggedInUser._id;
  //   let recipient = this.props.to;
  //   let chatType = this.props.chatType;
  //   let message = this.state.newMessage;
  //   this.state.newMessage = null;
  //   Send.textMessage(sender, recipient, chatType, message);
  // }

  setIsShowRecording() {
    // console.warn("setIsShowRecording: ", this.props.isRecordPress());
    // if (( this.state.newMessage !== null && this.state.newMessage.trim().length != 0 )) {
    //   this.onSendPress();
    // } else
    console.log('setIsShowRecording')
    if (this.props.isRecordPress()) {
      this.props.setIsRecordPress()
      this.props.setIsSafeToBack(true);
    } else {
      if (this._isMounted) {
        this.setState({
          isShowRecording: !this.state.isShowRecording,
          inputFieldHeight: 30
        })
        if (this.state.isShowRecording == false) {
          this.props.setIsSafeToBack(false);
        }
        if (this.state.isShowKeyboard) {
          this.refs.messageInput.blur();
          // this.props.setIsSafeToBack(true);
        }
      }
    }
  }

  setIsShowKeyboard(isShown) {
    if (this._isMounted) {
      console.log("called setIsShowKeyboard. isShowRecording: "+ this.state.isShowRecording
                    + " \nisShown: "+ isShown
                    + " \nisOptionsPress: " + this.props.isOptionsPress()
                    + " \nisEmojiPress: " + this.props.isEmojiPress()
                    + " \nisRecordPress: " + this.props.isRecordPress()
                  );
      this.state.isShowKeyboard = isShown;
      var toggleOption = false,
          toggleEmoji = false,
          toggleRecord = false;
      if(this.props.isOptionsPress() && (isShown || this.state.isRecordPress || this.state.isEmojiPress))
      {
        console.log("toggled Options");
        toggleOption = true;

      }
      if(this.props.isEmojiPress() && (isShown || this.state.isOptionsPress || this.state.isRecordPress))
      {
        toggleEmoji = true;
        console.log("toggled Emoji");

      }
      if(this.props.isRecordPress() && (isShown || this.state.isEmojiPress || this.state.isOptionsPress))
      {
        toggleRecord = true;
        console.log("toggled Record");

      }

      if(toggleOption) {
        this.props.setIsOptionsPress();
      }
      if(toggleEmoji) {
        this.props.setIsEmojiPress();
      }
      if(toggleRecord) {
        this.props.setIsRecordPress();
      }
      this.state.isEmojiPress = false;
      this.state.isRecordPress = false;
      this.state.isOptionsPress = false;
      if(!isShown && this.refs.messageInput.isFocused()) {
        this.refs.messageInput.blur();
      }
    }
  }

  renderRecording() {
    if  (this.props.isRecordPress()) {
      return (
        <AudioRecorder
          chatType={this.props.chatType}
          keyboardSpace={ddp.keyboardSpace}
          from={this.props.from}
          to={this.props.to}
          color={this.props.color}
          sendAudio={this.sendAudio}
        />
      );
    }
  }

  render() {
    let color = this.props.color;
    let placeholder = (this.state.canEditTextInput) ? 'Type a message...' : '';
    let sendIcon = (this.state.newMessage) ? 'send' : 'mic';
    let scrollFieldHeight;

    if(this.state.inputFieldHeight > windowSize.height * 0.10)
    {
      scrollFieldHeight = windowSize.height * 0.10;
    }
    else {
      scrollFieldHeight = this.state.inputFieldHeight;
    }
    return (
      <View style={styles.typingArea}>
        <View style={[styles.inputContainer, {marginLeft: 3, marginRight: 3, marginBottom: 5, marginTop: 5, height: scrollFieldHeight + 10}]}>
          <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: color.CHAT_RCVD, marginRight: -5, borderRadius: 5}}>
            <View style={{backgroundColor: color.CHAT_RCVD, flexDirection: 'row', alignItems: 'flex-end', height: scrollFieldHeight + 10, borderRadius: 5, marginBottom: 6}}>
              <TouchableOpacity underlayColor={color.HIGHLIGHT}
                onPress={() => {this.props.setIsOptionsPress();
                                this.state.isOptionsPress = true;
                                this.setIsShowKeyboard(false);}}
                style={{alignItems: 'center', justifyContent: 'center', marginLeft: 3}}>
                <KlikFonts name='plus' style={[styles.sendButton, {color: color.TEXT_LIGHT}]}/>
              </TouchableOpacity>
              <TouchableOpacity underlayColor={color.HIGHLIGHT}
                onPress={() => {this.props.setIsEmojiPress();
                                this.state.isEmojiPress = true;
                                this.setIsShowKeyboard(false);}}
                style={{alignItems: 'center', justifyContent: 'center'}}>
                <KlikFonts name="emoticon" style={[styles.sendButton, {color: color.TEXT_LIGHT}]}/>
              </TouchableOpacity>
            </View>
            <ScrollView
            ref="scrollField"
            horizontal={false}
            style={{height: scrollFieldHeight,
            width: windowSize.width * 0.68,}}
            showsVerticalScrollIndicator={false}
            >
                <View style={{width: windowSize.width * 0.68,
                             height: this.state.inputFieldHeight,
                             alignSelf: 'center',
                             flexDirection: 'column',
                             flex: 1,}}>
                  {
                    // (this.state.isSendingText) ? null
                    // :
                      <TextInput
                        ref="messageInput"
                        value={(this.state.isSendingText) ? '' : this.state.newMessage}
                        controlled={true}
                        placeholder={placeholder}
                        placeholderTextColor={color.TEXT_LIGHT}
                        keyboardType="default"
                        multiline={true}
                        autoFocus={false}
                        contentOffset={{x: 0,y: this.state.inputFieldHeight}}
                        style={{color: color.TEXT_DARK, backgroundColor: 'transparent', textAlign: 'left', height: this.state.inputFieldHeight, padding: 5}}
                        // editable={!this.state.isSendingText}
                        onChangeText={(text) => {this.onChangeText(text)}}
                        onFocus={() => this.setIsShowKeyboard(true)}
                        onBlur={() => this.setIsShowKeyboard(false)}

                        onChange={(event) => {

                          if(event.nativeEvent.contentSize!=null )
                          {
                            if((event.nativeEvent.contentSize.height > 0 || event.nativeEvent.contentSize.height > this.state.inputFieldHeight))
                            {

                              this.refs.scrollField.scrollTo({y:this.state.inputFieldHeight});
                              this.setState({
                                inputFieldHeight: event.nativeEvent.contentSize.height,
                              });
                            }


                          }
                          }}
                        onContentSizeChange={(nativeEvent) => {
                          if(nativeEvent.contentSize!=null )
                          {
                            if((event.nativeEvent.contentSize.height > 0 || event.nativeEvent.contentSize.height > this.state.inputFieldHeight))
                            {
                              this.refs.scrollField.scrollTo({y:this.state.inputFieldHeight});
                              this.setState({
                                inputFieldHeight: nativeEvent.contentSize.height,
                              });
                            }


                          }
                        }
                          }
                      />
                  }
                </View>
            </ScrollView>

          </View>
        <View style={[styles.triangleSent, {borderTopColor: color.CHAT_RCVD, marginRight: -7, zIndex: -1,}]}/>
        <View style={{flexDirection: 'row', alignItems: 'flex-end', height: scrollFieldHeight + 10, borderRadius: 5}}>
          <TouchableOpacity
            onPress={() => {this.onPressSendIcon()}}
            disabled={this.state.isSendingText}>
              <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: color.INACTIVE_BUTTON, borderRadius: 20, height: 40, width: 40}}>
                <KlikFonts name={sendIcon} color={color.BUTTON_TEXT} size={32}/>
              </View>
          </TouchableOpacity>
          </View>
      </View>
      {this.renderRecording()}
      <KeyboardSpacer />
    </View>
    );
  }

  onChangeText(text) {
    if (this.state.isSendingText) {
      this.refs.messageInput.setNativeProps({text: ''});
      this.refs.messageInput.clear(0);
    } else {
      this.setState({newMessage: text});
    }
  }

  onPressSendIcon() {
    if (this.state.newMessage != null && this.state.newMessage != '') {
      this.refs.messageInput.clear(0);
      this.refs.messageInput.setNativeProps({text: ''});
      this.setState({isSendingText: true});
      this.onSendPress();
    } else {
      this.props.setIsRecordPress();
      this.state.isRecordPress = true;
      this.setIsShowKeyboard(false)
    }
  }

}

module.exports = TypingArea;
