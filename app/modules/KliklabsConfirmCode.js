'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from 'react-native';

import Button from './common/Button';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import Navigation from './actions/Navigation';
import RenderModal from './common/RenderModal';
import SystemAccessor from './SystemAccessor';
import Toolbar from './common/Toolbar';

var GLOBAL = require('./../modules/config/Globals.js');
var styles = StyleSheet.create(require('./../styles/styles.main.js'));
var windowSize = Dimensions.get("window");

class KliklabsSignUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      code: '',
      isLoading: false,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      loading: true,
      isLoaded: false
    };

    this.renderModal = this.renderModal.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.showSmsConfirmCode = this.showSmsConfirmCode.bind(this);
    this.goToRegisterPage = this.goToRegisterPage.bind(this);
    this.goToLogin = this.goToLogin.bind(this);
    this.registerUsingKliklabs = this.registerUsingKliklabs.bind(this);
    this.callLoginWithUsername = this.callLoginWithUsername.bind(this);
  }

  componentDidMount() {
    console.log("ConfirmCode");
    this._isMounted = true;
    this.props.setSignUpCallback();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1}}>
        {this.renderNavBar()}
        {this.props.renderConnectionStatus()}
        <View style={{flex:1, alignItems:'center', justifyContent:'center', flexDirection:'row', backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG}}>
          <ScrollView keyboardDismissMode='interactive' keyboardShouldPersistTaps={true} style={{flex: 1}}>
            <View style={[styles.loginContainer, {backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG}]}>
              <Image style={{height: 155, width: 190}} source={require('./../images/code_signup.png')} />
              <Text style={{color: GLOBAL.COLOR_CONSTANT.TEXT_LIGHT, fontSize: 16, marginTop: 50}}>Enter Confirmation Code</Text>
              <Text style={{color: GLOBAL.COLOR_CONSTANT.HIGHLIGHT, width: 200, textAlign: 'center', fontSize: 13}}>
                Enter the 4-digit code we sent to your phone number
              </Text>
              {/*<TouchableWithoutFeedback onPress={() => Navigation.onPress(() => this.showSmsConfirmCode())}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: GLOBAL.COLOR_CONSTANT.BUTTON, fontSize: 13}}>
                    Show Code (FOR TESTING ONLY!)
                  </Text>
                </View>
              </TouchableWithoutFeedback>*/}
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON, marginTop: 20}]}>
                <TextInput ref="confirm"
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER}
                  placeholder='Confirmation code'
                  value={this.state.code}
                  onChangeText={(code) => this.setState({code})}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.confirmCode()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <Button text="Next"
              color={GLOBAL.COLOR_CONSTANT.THEME}
              underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
              onPress={() => this.confirmCode()}
              style={{marginTop:15}}
              isDefaultWidth={true}/>
            </View>
          <KeyboardSpacer />
        </ScrollView>
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderNavBar() {
    return (
      <Toolbar
        title={'Confirm Code'}
        withBackButton={true}
        onIconClicked={this.props.goToSignUp}
        {...this.props}
      />
    );
  }

  showSmsConfirmCode(){
    var urlString = this.props.urlstring;
    console.log('pressed')
    this.refs.confirm.blur();
    if(urlString.indexOf('SMSText') > -1) {
      console.log("urlString: ", urlString);
    	this.setIsShowMessageModal('SMS Confirmation Code is: ' + urlString.substring(urlString.indexOf('enter+')+6, urlString.indexOf('+to')));
    } else {
      console.log(2)
    	this.setIsShowMessageModal('SMS text not found');
    }
  }

  confirmCode(){
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.refs.confirm.blur();
      if (this.state.code) {
        this.setIsLoading(true);
        var toEncrypt = {
          username: this.props.username,
          mobileCode: this.state.code
        };

        SystemAccessor.encrypt(JSON.stringify(toEncrypt), this.props.token.substring(0, 16), (err, credentials) => {
          console.log("credentials: ", credentials);
          if (credentials) {
            ddp.call('apiVerifyCode', [credentials, this.props.token]).then((toDecrypt) => {
              SystemAccessor.decrypt(toDecrypt, this.props.token.substring(0, 16), (err, response) => {
                if (response) {
                  console.log("response: ", response);
                  response = JSON.parse(response);

                  if(GLOBAL.TEST_MODE) {
                    response.sukses = true;
                    console.log('--- TEST MODE KliklabsConfirmCode - apiVerifyCode response=', response);
                  }

                  if (response.sukses) {
                    if (this.props.apiLoginOptions) {
                      if(this.props.register)
                        this.registerUsingKliklabs();
                      else {
                        this.setIsLoading(false);
                        this.goToLogin();
                      }
                    } else {
                      this.goToRegisterPage();
                      this.setIsLoading(false);
                    }
                  } else {
                    this.setIsLoading(false);
                    this.setIsShowMessageModal('Incorrect Code');
                  }
                } else {
                  this.setIsLoading(false);
                  this.setIsShowMessageModal('Something went wrong, please try again later.');
                }
              });
            });
          } else {
            this.setIsLoading(false);
            this.setIsShowMessageModal('Something went wrong, please try again later.');
          }
        });
      } else{
        this.setIsShowMessageModal('Invalid code')
      }
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsLoading(isLoading) {
    if (this._isMounted) {
      this.setState({isLoading: isLoading});
    }
  }

  renderModal() {
    if (this.state.isLoading) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  onPressedBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  registerUsingKliklabs() {
    console.log('registerUsingKliklabs');
    let mobile = this.props.mobile;
    let apiLoginOptions = this.props.apiLoginOptions;
    ddp.call('apiGetProfile', [this.props.token]).then(response => {
      console.log('apiGetProfile: ', response);

      let fullName = response.fullname.split(" ");
      let lastName = fullName[fullName.length-1];
      let firstName = '';
      for (let i = 0; i < fullName.length-1; i++) {
        firstName += fullName[i];
        if (i < fullName.length-1) {
          firstName += ' ';
        }
      }

      let toEncrypt = {
        mobile: mobile,
        password: apiLoginOptions.password,
        email: response.email,
        username: apiLoginOptions.username,
        firstName: firstName,
        lastName: lastName,
        photoUrl: null,
        isOnKlikLabs: true
      }

      SystemAccessor.encrypt(JSON.stringify(toEncrypt), this.props.token.substring(0, 16), (err, credentials) => {
        if (credentials) {
          console.log('Register kliklab user into klikchat database...');
          ddp.call('apiRegisterUser', [credentials, apiLoginOptions.accessToken]).then(toDecrypt => {

            SystemAccessor.decrypt(toDecrypt, this.props.token.substring(0, 16), (err, response) => {
              response = JSON.parse(response);

              if(GLOBAL.TEST_MODE) {
                response.sukses = true;
                console.log('--- TEST MODE KliklabsConfirmCode - apiGetProfile response=', response);
              }

              console.log('apiRegisterUser', response);
              if (response == '1') {
                ddp.call('apiCheckMobileVer', [apiLoginOptions.accessToken]).then((response) => {
                  console.log('apiCheckMobileVer', response);
                  this.setIsLoading(false);
                  this.props.navigator.push({
                    name:'KliklabsCongratulations',
                    id: 'kliklabscongratulations',
                    apiMobile: response.mobile,
                    audio: this.props.audio,
                    username: apiLoginOptions.username,
                    password: apiLoginOptions.password,
                    addContactToLoggedInUser: this.props.addContactToLoggedInUser,
                    backgroundImageSrc: this.props.backgroundImageSrc,
                    changeTheme: this.props.changeTheme,
                    debugText: this.props.debugText,
                    getMessageWallpaper: this.props.getMessageWallpaper,
                    goToSignUp: this.props.goToSignUp,
                    hasInternet: this.props.hasInternet,
                    hasNewFriendRequest: this.props.hasNewFriendRequest,
                    isDebug: this.props.isDebug,
                    isLoggedIn: this.props.isLoggedIn,
                    isSafeToBack: this.props.isSafeToBack,
                    loggedInUser: this.props.loggedInUser,
                    logout: this.props.logout,
                    renderConnectionStatus: this.props.renderConnectionStatus,
                    setAudio: this.props.setAudio,
                    setIsSafeToBack: this.props.setIsSafeToBack,
                    setLoggedInUser: this.props.setLoggedInUser,
                    setSignUpCallback: this.props.setSignUpCallback,
                    shouldReloadData: this.props.shouldReloadData,
                    subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
                    theme: this.props.theme,
                    timeFormat: this.props.timeFormat,
                  });
                });
              } else {
                this.setIsLoading(false);
                this.setIsShowMessageModal('Something went wrong. Please try again.');
              }
            });
          });
        } else {
          this.setIsLoading(false);
          this.setIsShowMessageModal('Something went wrong. Please try again.');
        }
      });
    });
  }

  callLoginWithUsername(mobile, password) {
    ddp.loginWithUsername(mobile, password, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        this.props.subscribeToLoggedInUser(res.id);
        this.props.navigator.push({
          name:'KliklabsCongratulations',
          id: 'kliklabscongratulations',
          audio: this.props.audio,
          username: this.props.username,
          password: this.state.klikPass,
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          debugText: this.props.debugText,
          getMessageWallpaper: this.props.getMessageWallpaper,
          goToSignUp: this.props.goToSignUp,
          hasInternet: this.props.hasInternet,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          isDebug: this.props.isDebug,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setLoggedInUser: this.props.setLoggedInUser,
          setSignUpCallback: this.props.setSignUpCallback,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
        });
      } else {
        console.log('Login ['+ logTime() +']: Login failed: ' + err.reason);
        this.refs.number.focus();
        if (this._isMounted) {
          this.setState({
            isLoggedIn: false,
            isLoggingIn: false,
            error: err.reason
          })
        }
      }
    });
  }

  goToRegisterPage() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'kliklabsregister',
        name:'KliklabsRegister',
        audio: this.props.audio,
        username: this.props.username,
        mobile: this.props.mobile,
        token: this.props.token,
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        debugText: this.props.debugText,
        getMessageWallpaper: this.props.getMessageWallpaper,
        goToSignUp: this.props.goToSignUp,
        hasInternet: this.props.hasInternet,
        hasNewFriendRequest: this.props.hasNewFriendRequest,
        isDebug: this.props.isDebug,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setLoggedInUser: this.props.setLoggedInUser,
        setSignUpCallback: this.props.setSignUpCallback,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
      });
    });
  }

  goToLogin() {
    if (this.props.hasInternet()) {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name: 'Login',
          id: 'login',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          debugText: this.props.debugText,
          getMessageWallpaper: this.props.getMessageWallpaper,
          goToSignUp: this.props.goToSignUp,
          hasInternet: this.props.hasInternet,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          isDebug: this.props.isDebug,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setLoggedInUser: this.props.setLoggedInUser,
          setSignUpCallback: this.props.setSignUpCallback,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          checkLoginToken: this.props.checkLoginToken
        });
      });
    } else {
      this.setIsNotConnected(true);
    }
  }

}


module.exports = KliklabsSignUp;
