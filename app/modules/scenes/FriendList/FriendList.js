'use strict';
import React, { Component } from 'react';
import {
  InteractionManager,
  ListView
} from 'react-native';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import FriendListRender from './FriendListRender';
import Users from '../../config/db/Users';

export default class FriendList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 }),
      friendList: [],
      filter: '',
      isAllLoaded: false
    }
    this.filterWithSearchText = this.filterWithSearchText.bind(this);
    this.getContacts = this.getContacts.bind(this);
    this.onPressContact = this.onPressContact.bind(this);
    this.sendContact = this.sendContact.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(()=>{
      this.getContacts();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getContacts() {
    let friendList = Users.getAllFriends();
    if (this._isMounted) {
      this.setState({
        friendList: friendList,
        dataSource: this.state.dataSource.cloneWithRows(friendList),
        isAllLoaded: true
      });
    }
  }

  render() {
    return (
      <FriendListRender
        dataSource={this.state.dataSource}
        filter={this.state.filter}
        filterWithSearchText={this.filterWithSearchText}
        isAllLoaded={this.state.isAllLoaded}
        onPressContact={this.onPressContact}
        {...this.props}
      />
    );
  }

  onPressContact(contact) {
    dismissKeyboard();
    if (this.props.action == 'sendContact') {
      return this.sendContact(contact);
    } else {
      return this.sendNewMessage(contact);
    }
  }

  sendNewMessage(contact) {
    this.props.navigator.replace({
      name:'MessagesGiftedChat',
      addContactToLoggedInUser: this.props.addContactToLoggedInUser,
      audio: this.props.audio,
      backgroundImageSrc: this.props.backgroundImageSrc,
      changeTheme: this.props.changeTheme,
      getMessageWallpaper: this.props.getMessageWallpaper,
      hasInternet: this.props.hasInternet,
      isAutoSaveMedia: this.props.isAutoSaveMedia,
      isLoggedIn: this.props.isLoggedIn,
      isSafeToBack: this.props.isSafeToBack,
      loggedInUser: this.props.loggedInUser,
      logout: this.props.logout,
      renderConnectionStatus: this.props.renderConnectionStatus,
      setAudio: this.props.setAudio,
      setIsViewVisible: this.props.setIsViewVisible,
      setIsSafeToBack: this.props.setIsSafeToBack,
      shouldReloadData: this.props.shouldReloadData,
      subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
      theme: this.props.theme,
      changeBgImage: this.props.changeBgImage,
      timeFormat: this.props.timeFormat,
      user: contact,
    });
  }

  sendContact(contact) {
    this.props.navigator.pop();
    this.props.sendContact(contact);
  }

  filterWithSearchText(text) {
    text = (text.trim() == '') ? '' : text;
    this.setState({isLoadingSearch: true});
    var contact;
    var tempRawDataSource = this.state.friendList.slice();
    for(var index = 0; index < tempRawDataSource.length; index++) {
      contact = tempRawDataSource[index].name;
      if( (contact).toLowerCase().indexOf((text).toLowerCase())===-1 ) {
        tempRawDataSource.splice(index, 1);
        index=-1;
      }
    }
    if (this._isMounted) {
      this.setState({
        filter: text,
        dataSource: this.state.dataSource.cloneWithRows(tempRawDataSource)
      });
    }
  }
}
