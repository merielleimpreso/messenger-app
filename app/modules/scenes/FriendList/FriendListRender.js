'use strict';
import React, {
  Component
} from 'react';
import {
  ListView,
  ToolbarAndroid,
  View
} from 'react-native';
import {getMediaURL, renderFooter} from '../../Helpers.js';
import GLOBAL from '../../config/Globals.js';
import ListRow from '../../common/ListRow';
import styles from './styles';
import SearchBar from '../../SearchBar';
import Toolbar from '../../common/Toolbar';

class FriendListRender extends Component {

  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let title = (this.props.action == 'sendContact') ? 'Send Contact' : 'Send New Message';
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        <Toolbar
          title={title}
          withBackButton={true}
          {...this.props}
        />
        <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
          <SearchBar
            theme={this.props.theme}
            onSearchChange={this.props.filterWithSearchText}
            isLoading={false}
            placeholder={"Enter your friend's name"}
            onFocus={() => this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
            value={this.props.filter}
          />
          <ListView
            ref="listview"
            enableEmptySections={true}
            keyboardShouldPersistTaps={true}
            dataSource={this.props.dataSource}
            renderRow={this.renderRow}
            renderFooter={this.renderFooter}
          />
        </View>
      </View>
    )
  }

  renderRow(contact) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ListRow onPress={() => this.props.onPressContact(contact)}
        labelText={contact.name}
        detailText={contact.statusMessage}
        image={getMediaURL(contact.image)}
        color={color}
        addSeparator={false}
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.props.dataSource.getRowCount(0) == 0) ? 'You have no friend in KlikChat yet.' : null;
    return renderFooter(this.props.isAllLoaded, color, true, notFoundText);
  }

}

module.exports = FriendListRender;
