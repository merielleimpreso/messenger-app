'use strict';

import React, { Component } from 'react';
import {
  AsyncStorage,
  ListView
} from 'react-native';
import { launchCamera, launchImageLibrary } from './../../Helpers';
import ChangeWallpaperRender from './ChangeWallpaperRender';
const menuSettings = [{name: 'Choose from Gallery'}, {name: 'Take Photo'}, {name: 'Reset Wallpaper'}];

export default class ChangeWallpaper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowMessageModal: false,
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(menuSettings)
    });
  }

  render() {
    return (
      <ChangeWallpaperRender
        alertMessage={this.state.alertMessage}
        dataSource={this.state.dataSource}
        isShowMessageModal={this.state.isShowMessageModal}
        onPress={this.onPress.bind(this)}
        onPressButtonClose={this.onPressButtonClose.bind(this)}
        setIsShowMessageModal={this.setIsShowMessageModal.bind(this)}
        {...this.props}
      />
    );
  }
  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPress(data) {
    switch (data.name) {
      case 'Choose from Gallery':
        return this.launchImageLibrary();
      case 'Take Photo':
        return this.launchCamera();
      case 'Reset Wallpaper':
        return this.resetWallpaper();
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  resetWallpaper() {
    AsyncStorage.setItem('backgroundImageSrc', "").then(() => {
      this.props.getMessageWallpaper();
      this.setIsShowMessageModal('Successfully reset chat wallpaper!');
    });
  }

  storeWallpaper(uri) {
    AsyncStorage.setItem('backgroundImageSrc', uri).then(()=> {
      this.props.getMessageWallpaper();
      this.setIsShowMessageModal('Successfully changed chat wallpaper!');
    });
  }

  launchImageLibrary() {
    launchImageLibrary('photo', (response) => {
      this.storeWallpaper(response.uri);
    });
  }

  launchCamera() {
    launchCamera('photo', (response) => {
      this.storeWallpaper(response.uri);
    });
  }
}
