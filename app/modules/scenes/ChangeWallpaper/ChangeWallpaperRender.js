import React, { Component } from 'react';
import {
  ListView,
  Platform,
  StyleSheet,
  Text,
  ToolbarAndroid,
  View,
} from 'react-native';
import ButtonIcon from './../../common/ButtonIcon';
import GLOBAL from './../../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './../../common/ListRow';
import NavigationBar from 'react-native-navbar';
import RenderModal from './../../common/RenderModal';
import styles from './styles';
import Toolbar from '../../common/Toolbar';

export default class ChangeWallpaperRender extends Component {
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderTopBar()}
        {this.props.renderConnectionStatus()}
        <ListView ref="listview" dataSource={this.props.dataSource} renderRow={this.renderRow.bind(this)} />
        {this.renderModalMessage()}
      </View>
    );
  }

  renderTopBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Toolbar
        title={'Chat Wallpaper'}
        withBackButton={true}
        {...this.props}
      />
    )
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <ListRow onPress={() => this.props.onPress(data)} labelText={data.name} color={color} addSeparator={true} />;
  }

  renderModalMessage() {
    if (this.props.isShowMessageModal) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => this.props.setIsShowMessageModal("")}
          message={this.props.alertMessage}
          modalType={'alert'}
        />
      );
    }
  }

}