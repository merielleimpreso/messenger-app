import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

export default {
  addPhotoRegister: {
    borderRadius: 50,
    borderWidth: 0.5,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 8,
    right: 8
  },
  inputLoginContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    width: width * 0.80,
    borderWidth: 0.5,
    borderColor: '#bdbdbd',
    borderRadius: 3
  },
  input: {
    height: 35,
    padding: 5,
    marginLeft: 5,
    marginRight: 5
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 20
  },
  socialButton: {
    width: width * 0.38
  },
  createButton: {
    marginTop: 25,
    width: width * 0.80,
    height: 40
  },
  eyeIcon: {
    fontSize: 20,
    padding: 7,
    color: '#bdbdbd'
  }
}