'use strict';

import React, { Component } from 'react';
import {
  Dimensions,
  ListView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {getMediaURL, renderFooter} from '../../Helpers';
import GLOBAL from '../../config/Globals.js';
import ListRow from '../../common/ListRow';
import ProfilePic from '../../common/ProfilePic';
import RenderModal from '../../common/RenderModal';
import styles from './styles';
import Toolbar from '../../common/Toolbar';
const windowSize = Dimensions.get('window');

export default class GroupDetailsRender extends Component {
  render() {
    let color  = GLOBAL.COLOR_THEME[this.props.theme()];

    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        {this.renderToolbar()}
        <View style={[styles.mainContainer, {backgroundColor: color.CHAT_BG}]}>
          <View style={styles.pictureNameContainer}>
            <ProfilePic source={this.props.groupPhoto} color={color} size={75} 
              changeAvatar={this.props.changeAvatar}
            />
            <TouchableOpacity onPress={() => this.props.setIsChangingGroupName(true)}>
              <View style={{borderBottomWidth: 1, borderBottomColor: color.BUTTON, marginLeft: 15, width: windowSize.width * 0.65}}>
                <Text style={[styles.txtName, {color: color.TEXT_DARK}]} underlineColorAndroid={color.HIGHLIGHT}>
                  {this.props.groupName}
                </Text>
              </View>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT, marginLeft: 15}}>EDIT GROUP NAME</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ListView
          ref="listview"
          keyboardShouldPersistTaps={true}
          dataSource={this.props.dataSource}
          renderRow={this.renderContacts.bind(this)}
          renderFooter={this.renderFooter.bind(this)}
          removeClippedSubviews={false}
        />
        {this.renderModal()}
      </View>
    )
  }

  renderToolbar() {
    return (
      <Toolbar title={'Group Details'} 
        withBackButton={true}
        rightIcon={'options1'}
        {...this.props} 
      />
    );
  }

  //render each contact
  renderContacts(contact) {
    let color  = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ListRow
       labelText={contact.name}
       detailText={contact.statusMessage}
       image={getMediaURL(contact.image)}
       color={color}
       addSeparator={true}
       />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return renderFooter(!this.props.isLoading, color, this.props.hasInternet());
  }

  renderModal() {
    if (this.props.isShowChooseToUploadModal) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowChooseToUploadModal}
          onPressOpenGallery={this.props.launchGallery}
          onPressOpenCamera={this.props.launchCamera}
          modalType={'uploadImage'}
        />
      );
    } else if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowModalLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loading'}
        />
      );
    } else if (this.props.isUploading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'uploading'}
        />
      );
    } else if (this.props.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.props.setIsShowMessageModal("")}
         message={this.props.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.props.isShowChangeGroupName) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.props.groupName}
          maxLength={30}
          title={'Display Name'}
          setIsShowModal={this.props.setIsChangingGroupName}
          setChangeProfileDetail={this.props.changeGroupName}
          modalType={'changeProfileDetail'}
        />
      );
    } else if (this.props.isShowModalOption) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowModalOption}
          onPress={this.props.onPressLeave}
          modalType={'options'}
        />
      );
    } else if (this.props.isShowMenu) {
      let actions = this.props.toolbarActions;
      actions[0].action = () => this.props.goToAddNewUser('Add');
      actions[1].action = () => this.props.goToAddNewUser('Remove');
      actions[2].action = this.props.setIsShowModalOption;
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.onPressRightButton}
          actions={actions}
          modalType={'menu'}
        />
      );
    }
  }
}