'use strict';

import React, { Component } from 'react';
import {
  InteractionManager,
  ListView,
  ToastAndroid
} from 'react-native';
import {launchCamera, launchImageLibrary, getMediaURL} from '../../Helpers';
import GLOBAL from '../../config/Globals.js';
import GroupDetailsRender from './GroupDetailsRender';
import Users from '../../config/db/Users';
import Send from '../../config/db/Send';
const TOOLBAR_ACTIONS = {
  Group: [{title: 'Add Member', iconName: 'options1', show: 'never'},
          {title: 'Remove Member', iconName: 'settings', show: 'never'},
          {title: 'Leave Group', iconName: 'settings', show: 'never'}]
};

export default class GroupDetails extends Component {
  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1!==row2,
      }),
      currentUsers: [],
      groupName: '',
      groupPhoto: null,
      isLoading: true,
      isShowCheckConnection: false,
      isShowChooseToUploadModal: false,
      isShowMenu: false,
      isShowModalLoading: false,
      isShowChangeGroupName: false,
      isUploading: false,
      isShowMessageModal: false,
      isShowModalOption: false,
      toolbarActions: (this.props.loggedInUser().groups[this.props.group._id]) ? null : TOOLBAR_ACTIONS.Group
    }
    this.setGroupUsers = this.setGroupUsers.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
    let group = this.props.group;
    let groupPhoto = (group.image) ? group.image : GLOBAL.DEFAULT_PROFILE_IMAGE;
    !group.user && this.getActiveUsers();

    this.setState({
      groupName: this.props.group.name,
      groupPhoto: getMediaURL(groupPhoto),
    })
  }

  getActiveUsers() {
    InteractionManager.runAfterInteractions(() => {
      ddp.call('getGroupUsersById', [this.props.group._id]).then(users => {
        if (this._isMounted) {
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(users),
            groupUsers: users,
            isLoading: false
          })
        }
      });
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <GroupDetailsRender
        alertMessage={this.state.alertMessage}
        changeAvatar={this.changeAvatar.bind(this)}
        changeGroupName={this.changeGroupName.bind(this)}
        dataSource={this.state.dataSource}
        goToAddNewUser={this.goToAddNewUser.bind(this)}
        groupName={this.state.groupName}
        groupPhoto={this.state.groupPhoto}
        groupUsers={this.state.groupUsers}
        isLoading={this.state.isLoading}
        isShowChangeGroupName={this.state.isShowChangeGroupName}
        isShowCheckConnection={this.state.isShowCheckConnection}
        isShowChooseToUploadModal={this.state.isShowChooseToUploadModal}
        isShowMessageModal={this.state.isShowMessageModal}
        isShowMenu={this.state.isShowMenu}
        isShowModalLoading={this.state.isShowModalLoading}
        isShowModalOption={this.state.isShowModalOption}
        isUploading={this.state.isUploading}
        launchCamera={this.launchCamera.bind(this)}
        launchGallery={this.launchGallery.bind(this)}
        onActionSelected={this.onActionSelected.bind(this)}
        onPressLeave={this.onPressLeave.bind(this)}
        onPressRightButton={this.onPressRightButton.bind(this)}
        setIsChangingGroupName={this.setIsChangingGroupName.bind(this)}
        setIsShowCheckConnection={this.setIsShowCheckConnection.bind(this)}
        setIsShowChooseToUploadModal={this.setIsShowChooseToUploadModal.bind(this)}
        setIsShowMessageModal={this.setIsShowMessageModal.bind(this)}
        setIsShowModalLoading={this.setIsShowModalLoading.bind(this)}
        setIsShowModalOption={this.setIsShowModalOption.bind(this)}
        toolbarActions={this.state.toolbarActions}
        {...this.props}
      />
    )
  }

  setGroupUsers(users, action) {
    if (this._isMounted) {
      InteractionManager.runAfterInteractions(() => {
        action = (action == 'add') ? 'added' : 'removed';
        this.setIsShowMessageModal(`Successfully ${action}.`);
      });
      if (this.props.getGroupUsers()) {
        this.props.getGroupUsers();
      }
      this.setState({
        groupUsers: users,
        dataSource: this.state.dataSource.cloneWithRows(users)
      });
    }
  }

  goToAddNewUser(addOrRemove) {
    this.state.isShowMenu && this.onPressRightButton();
    this.props.navigator.push({
      name: 'AddUserToGroup',
      backgroundImageSrc: this.props.backgroundImageSrc,
      group: this.props.group,
      chatType: GLOBAL.CHAT.GROUP,
      theme: this.props.theme,
      currentUsers: this.state.currentUsers,
      groupUsers: this.state.groupUsers,
      loggedInUser: this.props.loggedInUser,
      shouldReloadData: this.props.shouldReloadData,
      renderConnectionStatus: this.props.renderConnectionStatus,
      hasInternet: this.props.hasInternet,
      refreshGroups: this.props.refreshGroups,
      timeFormat: this.props.timeFormat,

      addContactToLoggedInUser: this.props.addContactToLoggedInUser,
      changeBgImage: this.props.changeBgImage,
      changeTheme: this.props.changeTheme,
      getMessageWallpaper: this.props.getMessageWallpaper,
      isSafeToBack: this.props.isSafeToBack,
      logout: this.props.logout,
      setLoggedInUser: this.props.setLoggedInUser,
      setIsViewVisible: this.props.setIsViewVisible,
      setIsSafeToBack: this.props.setIsSafeToBack,
      subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
      user: this.props.user,
      users: this.props.group.users,
      addOrRemove: addOrRemove,
      setGroupUsers: this.setGroupUsers
    });
  }

  onPressRightButton() {
    if (this._isMounted) {
      if (this.state.toolbarActions) {
        this.setState({isShowMenu: !this.state.isShowMenu});
      } else {
        this.setIsShowMessageModal('You are no longer part of this group.');
      }
    }
  }

  setIsShowModalOption() {
    if (this._isMounted) {
      this.setState({isShowModalOption: !this.state.isShowModalOption});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  changeAvatar(){
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if((this.props.loggedInUser().groups[this.props.group._id]) == null){
        this.setIsShowChooseToUploadModal();
      } else {
        this.setState({
          isShowMessageModal: true,
          alertMessage: "You are no longer part of this group."
        });
      }

    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowChooseToUploadModal() {
    if (this._isMounted) {
      this.setState({isShowChooseToUploadModal: !this.state.isShowChooseToUploadModal});
    }
  }

  setIsShowModalLoading() {
    if (this._isMounted) {
      this.setState({isShowModalLoading: !this.state.isShowModalLoading});
    }
  }

  setIsChangingGroupName() {
    if (this._isMounted) {
      if ((this.props.loggedInUser().groups[this.props.group._id]) == null) {
        this.setState({
          isShowChangeGroupName: !this.state.isShowChangeGroupName
        });
      } else if ((this.props.loggedInUser().groups[this.props.group._id]) != null){
        this.setState({
          isShowMessageModal: true,
          alertMessage: "You are no longer part of this group."
        });
      }
    }
  }

  launchGallery(){
    launchImageLibrary('photo', (response) => {
      this.checkFileSize(response);
    });
  }

  launchCamera(){
    launchCamera('photo', (response) => {
      this.checkFileSize(response);
    });
  }

  checkFileSize(response) {
    this.setIsShowChooseToUploadModal();
    if (typeof response == "string") {
      this.setIsShowMessageModal(response);
    } else {
      this.changePhoto(response);
    }
  }

  changePhoto(response) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (this._isMounted) {
        this.setState({
          isUploading: true
        });
      }
      let data = response;
      data['groupId'] = this.props.group._id;
      Send.uploadMedia(data, 'photo', Send.uploadURL.groupImage, null, null, (result) => {
        console.log('--- change group profile photo=', result);
        console.log('--- change group profile photo - typeof result=', (typeof result));

        // if (typeof result == 'string' && (result.includes('/avatar') || result == 'Success')) {
          if (typeof result == 'string' && result != 'Uploading failed.') {
          var jsonObj=JSON.parse(result);

          console.log('--- jsonObj.data.key=',jsonObj.data.key);

          if (this._isMounted) {
            console.log('--- change group profile photo - isMounted');

            this.setState({
              // groupPhoto: (result.includes('/avatar')) ? getMediaURL(result) : response.uri,
              groupPhoto: (jsonObj.data.key.includes('avatar/')) ? getMediaURL(jsonObj.data.key) : response.uri,
              isUploading: false
            });
          }
          this.setIsShowMessageModal('Successfully uploaded image.')
        } else {
          if (this._isMounted) {
            this.setState({isUploading: false});
              this.setIsShowMessageModal('Upload failed, please try again later.')
            console.log('--- Something went wrong, please try again later.');
            // this.setIsShowMessageModal('Something went wrong, please try again later.')
          }
        }
      });
    }
  }

  changeGroupName(groupName) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (groupName.trim()) {
        this.setIsChangingGroupName(false);
        this.setIsShowModalLoading();

        ddp.call('editGroupName', [this.props.group._id, groupName]).then(result => {
          this.setIsShowModalLoading();
          if (this._isMounted) {
            this.setState({
              groupName: result
            });
          }
        }).catch(err => {
          this.setIsShowModalLoading();
          console.log("Edit Group name failed: ", err);
        });
      } else {
        ToastAndroid.show("Display name cannot be empty!", ToastAndroid.SHORT);
      }
    }
  }

  onActionSelected(position) {
    if (position == 0) {
      this.goToAddNewUser('Add');
    } else if (position == 1) {
      this.goToAddNewUser('Remove');
    } else if (position == 2) {
      this.setIsShowModalOption();
    }
  }

  onPressLeave() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.state.isShowMenu && this.onPressRightButton();
      this.setIsShowModalOption();
      this.setIsShowModalLoading();
      ddp.call('leaveGroup',[this.props.group._id]).then((result) => {
        this.setIsShowModalLoading();
        this.props.navigator.pop();
      });
    }
  }

  // go back to previous screen
  onBackPress() {
    this.props.navigator.pop();
  }
}
