'use strict';

import React, { Component } from 'react';
import {
  ListView,
} from 'react-native';
import { logTime } from './../../Helpers';
import RNRestart from 'react-native-restart';
import SettingsRender from './SettingsRender';
import FCM from 'react-native-fcm';

const menuSettings = [{name: 'Change Password'}, {name: 'Chat Wallpaper'}, {name: 'Data Usage'}, {name: 'Sign Out'}];

export default class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalSignOutVisible: false,
      isShowCheckConnection: false,
      isLoggingOut: false,
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(menuSettings)
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <SettingsRender
        dataSource={this.state.dataSource}
        isLoggingOut={this.state.isLoggingOut}
        isShowCheckConnection={this.state.isShowCheckConnection}
        modalSignOutVisible={this.state.modalSignOutVisible}
        onPress={this.onPress.bind(this)}
        onPressButtonClose={this.onPressButtonClose.bind(this)}
        setIsShowCheckConnection={this.setIsShowCheckConnection.bind(this)}
        setModalSignOutVisible={this.setModalSignOutVisible.bind(this)}
        signOut={this.signOut.bind(this)}
        {...this.props}
      />
    );
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPress(data) {
    switch (data.name) {
      case 'Change Password':
        return this.goToChangePassword();
      case 'Chat Wallpaper':
        return this.goToChangeWallpaper();
      case 'Sign Out':
        return this.setModalSignOutVisible(true);
      case 'Data Usage':
        return this.goToPhotosAndMedia();
    }
  }

  goToChangeWallpaper() {
    this.props.navigator.push({
      name: 'ChangeWallpaper',
      id: 'changewallpaper',
      theme: this.props.theme,
      getMessageWallpaper: this.props.getMessageWallpaper,
      renderConnectionStatus: this.props.renderConnectionStatus
    });
  }

  goToChangePassword() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.props.navigator.push({
        name: 'ChangePassword',
        id: 'changepassword',
        theme: this.props.theme,
        hasInternet: this.props.hasInternet,
        renderConnectionStatus: this.props.renderConnectionStatus
      });
    }
  }

  goToPhotosAndMedia() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.props.navigator.push({
        name: 'DataUsage',
        id: 'datausage',
        theme: this.props.theme,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setIsAutoSaveMedia: this.props.setIsAutoSaveMedia
      });
    }
  }

  setModalSignOutVisible(visible) {
    if (this._isMounted) {
      this.setState({modalSignOutVisible: visible});
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  signOut() {
    if (this._isMounted) {
      this.setState({modalSignOutVisible: false});
    }

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (this._isMounted) {
        this.props.setIsLoggingOut(true);
        this.setState({
          isLoggingOut: true
        });
      }
      console.log('Settings ['+ logTime() +']: Logging out...');
      requestAnimationFrame(() => {
        ddp.logout((err, res) => {


            this.props.logout();
            ddp.close();
            if (this._isMounted) {
              this.setState({isLoggingOut: false});
            }
            //
            // FCM.getFCMToken().then(token => {
            //   console.log('Klikchat: FCM got new device token = ' + token);
            //   GLOBAL.FCM_USER_TOKEN = token;

              setTimeout(()=>{
                RNRestart.Restart();
              }, 500);
           //});





        });
      });
    }
  }
};
