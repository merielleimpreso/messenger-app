'use strict';

import React, { Component } from 'react';
import {
  ListView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import GLOBAL from './../../config/Globals.js';
import ListRow from './../../common/ListRow';
import RenderModal from './../../common/RenderModal';
import styles from './styles';
import Toolbar from './../../common/Toolbar';

export default class SettingsRender extends Component {
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        <Toolbar title={'Settings'} withBackButton={true} onPressLeftButton={this.props.onPressButtonClose} {...this.props}/>
        {this.props.renderConnectionStatus()}
        <ListView ref="listview" dataSource={this.props.dataSource} renderRow={this.renderRow.bind(this)}/>
        {this.renderModal()}
      </View>
    );
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <ListRow onPress={() => this.props.onPress(data)} labelText={data.name} color={color} addSeparator={true}/>;
  }

  renderModal() {
    if (this.props.modalSignOutVisible) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setModalSignOutVisible}
          onPress={this.props.signOut}
          modalType={'signOutOptions'}
        />
      );
    } else if (this.props.isLoggingOut) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loggingOut'}
        />
      )
    } else if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }
}
