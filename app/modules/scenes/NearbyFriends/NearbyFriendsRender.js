'use strict';

import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  ListView,
  Platform,
  RefreshControl,
  View,
} from 'react-native';
import _ from 'underscore';
import {getMediaURL, renderFooter, toNameCase, getDisplayName} from '../../Helpers';
import Button from '../../common/Button';
import GLOBAL from '../../config/Globals.js';
import ListRow from '../../common/ListRow';
import RenderModal from '../../common/RenderModal';
import ScreenNoItemFound from '../../ScreenNoItemFound';
import styles from './styles';

export default class NearbyFriendsRender extends Component {

  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var content = <View/>;
    let screenNoItemFound = null;
    let listStyle = {};

    if (!this.props.isLoading && (this.props.dataSource.getRowCount() == 0)) {
      screenNoItemFound = <ScreenNoItemFound isLoading={false} text={'No contact save on this phone'} color={color}/>;
      listStyle = { flex: 1, height: 500 };
    }
    let listView = (
      <ListView ref="listview"
        dataSource={this.props.dataSource}
        renderRow={this.renderRow}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={true}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isLoading}
            onRefresh={this.onRefresh.bind(this)}
            colors={[color.THEME]}
          />
        }
        style={listStyle}
      />
    );

    if (this.props.resultLength <= 0 && !this.props.isLoading) {
      if (this.props.nothingToDisplay) {
        console.log("Rendered No Contact");
        content = <View style={{marginTop: 10}}>
                    <ScreenNoItemFound isLoading={false} text={'No friend is near you'} color={color}/>
                    {listView}
                  </View>;
      } else {
        console.log("Rendered CurrentLocation Not Set");
        content = <View style={styles.searchLocationView}>
                    <ScreenNoItemFound isLoading={false} text={'Location settings are turned off!'} color={color}/>
                    <Button text={this.props.checkInButtonText}
                      color={color.THEME}
                      underlayColor={color.HIGHLIGHT}
                      onPress={() => this.props.identifyCurrentLocation()}
                      style={styles.button}
                      isDefaultWidth={false}
                    />
                  </View>;
      }
    } else if (this.props.isLoading) {
      content = <ActivityIndicator size="small" color={color.BUTTON} style={{marginTop: 10}}/>
    } else {
      content = listView;
    }

    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        {content}
        {this.renderModal()}
      </View>
    )
  }

  onRefresh() {
    this.props.onRefresh();
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let image = getMediaURL(data.image);
    let id = data._id;
    let buttonLabel = (_.contains(this.props.isContact, id)) ? 'Chat'
                        : (_.contains(this.props.isIgnored, id)) ? 'Chat'
                          : (_.contains(this.props.isFriendRequest, id)) ? 'Accept\nRequest'
                            : (_.contains(this.props.isSentRequest, id)) ? 'Chat' : 'Add';

    return (
      <ListRow
        color={color}
        labelText={toNameCase(data.name)}
        detailText={data.location}
        image={image}
        button={buttonLabel}
        iconPress={() => this.props.onIconPress(data, buttonLabel)}
        addSeparator
      />
    );
  }

  renderSearchLocation() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={styles.searchLocationView}>
        <ScreenNoItemFound isLoading={false} text={'Location settings are turned off!'} color={color}/>
        <Button text={this.state.checkInButtonText}
          color={color.THEME}
          underlayColor={color.HIGHLIGHT}
          onPress={() => this.identifyCurrentLocation()}
          style={styles.button}
          isDefaultWidth={false}
        />
      </View>
    )
  }

  renderModal() {
    if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowContactProfile) {
      return (
        <RenderModal
          theme={this.props.theme}
          profileData={this.props.contact}
          setIsShowModal={() => this.props.setIsShowContactProfile(null)}
          modalType={'viewProfile'}
        />
      );
    } else if (this.props.isShowLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    }
  }
}
