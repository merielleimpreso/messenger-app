import { Dimensions, StyleSheet } from 'react-native';
let { width } = Dimensions.get('window'); 

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch'
  },
  searchLocationView: {
    marginTop: 10
  },
  button: {
    alignSelf: 'center', 
    width: width * 0.25
  }
});
