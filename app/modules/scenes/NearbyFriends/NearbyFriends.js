'use strict';

import React, { Component } from 'react';
// import {
//   ActivityIndicator,
//   Dimensions,
//   InteractionManager,
//   ListView,
//   StyleSheet,
//   Text,
//   TouchableHighlight,
//   View,
//   Alert
// } from 'react-native';
// import {getDisplayName, getMediaURL} from './Helpers';
// import Button from './common/Button';
// import Geocoder from 'react-native-geocoder';
// import ListRow from './common/ListRow';
// import ScreenNoItemFound from './ScreenNoItemFound';
// import SystemAccessor from './SystemAccessor';
// const GLOBAL = require('./config/Globals.js');
// const styles = StyleSheet.create(require('./../styles/styles.main'));
// const windowSize = Dimensions.get('window');
import {
  InteractionManager,
  ListView,
  Alert
} from 'react-native';
import _ from 'underscore';
import {getDisplayName, getMediaURL} from '../../Helpers';
import Geocoder from 'react-native-geocoder';
import GLOBAL from '../../config/Globals.js';
import NearbyFriendsRender from './NearbyFriendsRender';
import SystemAccessor from '../../SystemAccessor';

class NearbyFriends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: null,
      contacts: [],
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      resultLength: 0,
      nothingToDisplay: false,
      isLoading: true,
      isOpenModalProfile: false,
      checkInButtonText: "Try again."
    }
    this.acceptRequest = this.acceptRequest.bind(this);
    this.getRelationship = this.getRelationship.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.identifyCurrentLocation = this.identifyCurrentLocation.bind(this);
    this.ignoreRequest = this.ignoreRequest.bind(this);
    this.processContact = this.processContact.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.setIsShowContactProfile = this.setIsShowContactProfile.bind(this);
    this.setIsShowLoading = this.setIsShowLoading.bind(this);
    this.setRelationshipStates = this.setRelationshipStates.bind(this);
    this.updateMyLocationToServer = this.updateMyLocationToServer.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      console.log("Calling findNearbyContacts");
      this.identifyCurrentLocation();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onRefresh() {
    if (this._isMounted) {
      this.setState({
        isLoading: true
      });
    }
    this.identifyCurrentLocation();
  }

  render() {
    return (
      <NearbyFriendsRender
        contact={this.state.contact}
        contacts={this.state.contacts}
        checkInButtonText={this.state.checkInButtonText}
        dataSource={this.state.dataSource}
        goToMessageChat={this.goToMessageChat}
        identifyCurrentLocation={this.identifyCurrentLocation}
        isContact={this.state.isContact}
        isFriendRequest={this.state.isFriendRequest}
        isIgnored={this.state.isIgnored}
        isLoading={this.state.isLoading}
        isMember={this.state.isMember}
        isSentRequest={this.state.isSentRequest}
        isShowLoading={this.state.isShowLoading}
        isShowContactProfile={this.state.isShowContactProfile}
        isOpenModalProfile={this.state.isOpenModalProfile}
        onIconPress={this.onIconPress.bind(this)}
        nothingToDisplay={this.state.nothingToDisplay}
        resultLength={this.state.resultLength}
        setIsShowContactProfile={this.setIsShowContactProfile}
        onRefresh={this.onRefresh}
        {...this.props}
      />
    )
  }

  identifyCurrentLocation() {
    if (this._isMounted) {
      this.setState({
        checkInButtonText: "Finding..."
      });
    }
    console.log("Identifying current location...");
    navigator.geolocation.getCurrentPosition((position) => {
      if (this._isMounted) {
        this.setState({
          checkInButtonText: "Try again"
        });
      }
      //Identify location name using react-native-geocoder
      Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(res => {
        //console.log("Geocoder result: ",res);
        console.log("Identified. Currently in: ", res[0].locality);
        this.updateMyLocationToServer(position, res[0].locality);
      }).catch((error) => {
        this.setState({
          isLoading: false,
          nothingToDisplay: true
        });
          Alert.alert("Oops!", "Unable to identify current location at this time: \n\nReason: "+ error + ". \n\nTry again? ", [
            {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
            {text: "No", onPress: () => {}}
          ]);
      });
    },(error) => {
      if (error === "No available location provider.") {

        Alert.alert("Oops!", "Unable to identify current location at this time: \n\nReason: Location might be turned off. \n\nTry again? ", [
          {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
          {text: "No", onPress: () => {}},
          {text: "Settings", onPress: () => {this.goToLocationSettings()}}
        ]);
      } else {
          Alert.alert("Oops!", "Unable to identify current location at this time. \n\nReason: "+ error + " \n\nTry again? ", [
            {text: "Yes", onPress: () => {this.identifyCurrentLocation()}},
            {text: "No", onPress: () => {}}
         ]);
      }
      if (this._isMounted) {
        this.setState({
          nothingToDisplay: false,
          isLoading: false,
          checkInButtonText: "Try again"
        });
      }
    },{timeout: 50000, maximumAge: 1000});
  }

  updateMyLocationToServer(position, locality) {
    var location = {latitude: position.coords.latitude, longitude: position.coords.longitude, locality: locality};
    console.log("Updating location in server... ");

    ddp.call("updateUserLocation", [location]).then(()=>{
      console.log("Successfully updated location in server. ");
      console.log("Finding nearby contacts... ");
      ddp.call("findNearbyContacts", []).then((result)=>{
        this.state.resultLength = result.length;
        if (result != "No Coordinates found!" && result.length > 0 ) {
          console.log("Found "+ result.length +" nearby contacts.");
          this.getRelationship(result);
        } else {
          if (this._isMounted) {
            this.setState({
              isLoading: false,
              nothingToDisplay: true
            });
          }
        }
      });
    }).catch(() => {
      this.setState({
        isLoading: false,
        nothingToDisplay: true
      });

      Alert.alert("Oops! ", "Failed to check in to "+ locality+"! Try again?",
      [
        {text: "Yes", onPress: () => this.updateMyLocationToServer(position, locality)},
        {text: "No", onPress: () =>{}}
      ]);
   });
  }

  goToLocationSettings() {
    SystemAccessor.goTo("locationSettings", (error, result) => {
      console.log("goToLocationSettings result: "+ result);
    });
  }

  getRelationship(result) {
    let friendRequestList = _.pluck(this.props.loggedInUser().friendRequestReceived, 'userId');
    let ignoredRequestList = _.reject(this.props.loggedInUser().friendRequestReceived, function(i){ return i.date != null; });
    let ignoredRequestIds = _.pluck(ignoredRequestList, 'userId');
    let sentRequestList = _.pluck(this.props.loggedInUser().friendRequestSent, 'userId');
    let contacts = [];

    for (let i = 0; i < result.length; i++) {
      let contact = {
        _id: result[i].id,
        image: result[i].profile.image,
        name: result[i].profile.name,
        location: result[i].location.locality + "  " + result[i].distance
      };
      contact['isContact'] = _.contains(this.props.loggedInUser().contacts, contact._id);
      contact['isFriendRequest'] = _.contains(friendRequestList, contact._id);
      contact['isIgnored'] = _.contains(ignoredRequestIds, contact._id);
      contact['isSentRequest'] = _.contains(sentRequestList, contact._id);

      contacts.push(contact);
    }

    let isContact = _.pluck(_.where(contacts, {'isContact': true}), '_id');
    let isFriendRequest = _.pluck(_.where(contacts, {'isFriendRequest': true}), '_id');
    let isIgnored = _.pluck(_.where(contacts, {'isIgnored': true}), '_id');
    let isSentRequest = _.pluck(_.where(contacts, {'isSentRequest': true}), '_id');
    let isMember = _.pluck(_.where(contacts, {'isMember': true}), '_id');
    let data = {
      contacts: _.sortBy(contacts, 'name'),
      isContact: isContact,
      isFriendRequest: isFriendRequest,
      isIgnored: isIgnored,
      isMember: isMember,
      isSentRequest: isSentRequest
    }
    this.setRelationshipStates(data);
  }

  setRelationshipStates(data) {
    if (this._isMounted) {
      this.setState({
        contacts: data.contacts,
        dataSource: this.state.dataSource.cloneWithRows(data.contacts),
        isContact: data.isContact,
        isFriendRequest: data.isFriendRequest,
        isIgnored: data.isIgnored,
        isSentRequest: data.isSentRequest,
        isMember: data.isMember,
        isLoading: false,
        nothingToDisplay: data.length <= 0
      });
    }
  }

  onIconPress(data, action) {
    if (action == 'Chat') {
      this.goToMessageChat(data);
    } else if (action == 'Accept Request') {
      this.setIsShowContactProfile(data);
    } else if (action == 'Add') {
      this.setIsShowContactProfile(data);
    } else if (action == 'Invite') {
      this.inviteContact(data.mobile, data.name);
    }
  }

  setIsShowContactProfile(data) {
    let profileData = null;
    if (data) {
      let actions = [];
      if (data.isFriendRequest) {
        actions = [{icon: 'block', iconLabel: 'Block', onPressIcon: () => this.ignoreRequest(data._id)},
                   {icon: 'check', iconLabel: 'Accept Request', onPressIcon: () => this.acceptRequest(data._id)}];
      } else {
        actions = [{icon: 'close', iconLabel: 'Cancel', onPressIcon: () => this.setIsShowContactProfile(null)},
                   {icon: 'check', iconLabel: 'Send Request', onPressIcon: () => this.sendRequest(data._id)}];
      }

      profileData = {
        data: data,
        actions: actions,
        relStatus: null
      }
    }

    if (this._isMounted) {
      this.setState({
        isShowContactProfile: !this.state.isShowContactProfile,
        contact: profileData
      });
    }
  }

  setIsShowLoading() {
    if (this._isMounted) {
      this.setState({
        isShowLoading: !this.state.isShowLoading
      });
    }
  }

  ignoreRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('ignoreFriendRequest', [id]).then(() => {
        console.log('Successfully ignored friend request.')
        //ddp.collections.contacts.remove({_id: id});
        this.processContact('ignored', id);
        this.setIsShowLoading();
      });
      this.setIsShowContactProfile(null);
    }
  }

  acceptRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('addToContacts', [id]).then(res => {
        if (res) {
          console.log('Successfully accepted friend request.')
          ddp.collections.contacts.upsert(res);
          this.processContact('accepted', id);
          this.setIsShowLoading();
        }
      });
      this.setIsShowContactProfile(null);
    }
  }

  sendRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('addFriendRequest', [id]).then(result => {
        if (result) {
          console.log('Successfully sent friend request.');
          this.processContact('sent', id);
          this.setIsShowLoading();
        }
      });
      this.setIsShowContactProfile(null);
    }
  }

  processContact(processed, id) {
    let contacts = this.state.contacts;
    let isContact = this.state.isContact;
    let isSentRequest = this.state.isSentRequest;
    let isFriendRequest = this.state.isFriendRequest;
    let isIgnored = this.state.isIgnored;
    let isMember = this.state.isMember;

    if (processed == 'accepted') {
      isContact.splice();
      isContact.push(id);
    } else if (processed == 'sent') {
      isSentRequest.splice();
      isSentRequest.push(id);
    } else if (processed == 'ignored') {
      isIgnored.splice();
      isIgnored.push(id);
    }

    let data = {
      contacts: _.sortBy(contacts, 'name'),
      isContact: isContact,
      isFriendRequest: isFriendRequest,
      isIgnored: isIgnored,
      isMember: isMember,
      isSentRequest: isSentRequest
    }

    if (this._isMounted) {
      this.state.dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setRelationshipStates(data);
    }
  }

  goToMessageChat(data) {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    let groupData = null;
    // console.log("Tried to go to: ", data);
    if (navigatorStack[currentRoute].name !== 'MessagesGiftedChat') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id:'messagesgiftedchat',
          name:'MessagesGiftedChat',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          isAutoSaveMedia: this.props.isAutoSaveMedia,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          changeBgImage: this.props.changeBgImage,
          timeFormat: this.props.timeFormat,
          user: data,
          isLoggedIn: this.props.isLoggedIn
        });
      });
    }
  }

}

// Export module.
module.exports = NearbyFriends;
