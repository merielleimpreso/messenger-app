/*Author: John Rezan B
Date Created: 2016-09-09
Date Updated:
Description: This file is the login modals for the kliklab.
Changelog:
update:
*/

'use strict';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Image,
  InteractionManager,
  ListView,
  Picker,
  Platform,
  ScrollView,
  StyleSheet,
  TextInput,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import Button from '../../common/Button';
import country from '../../config/db/Countries';
import GLOBAL from '../../config/Globals';
import KeyboardSpacer from '../../KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import Navigation from '../../actions/Navigation';
import NavigationBar from 'react-native-navbar';
import RenderModal from '../../common/RenderModal';
import styles from './styles';
import SystemAccessor from '../../SystemAccessor';
import Toolbar from '../../common/Toolbar';

const MINIMUM_MOBILE_CHAR = 10;

class KliklabsSignUp extends Component {

  // Set initial state
  constructor(props) {
    super(props);
    this.state = {
      selectedCountry: 'Indonesia',
      selectedCode: '+62',
      mobile: '',
      theme: GLOBAL.THEMES[0],
      isLoading: false,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      username: (this.props.apiLoginOptions) ? this.props.apiLoginOptions.username : '',
      loading: true,
      isLoaded: false
    };

    this.getToken = this.getToken.bind(this);
    this.onPressCountry = this.onPressCountry.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.renderNavbar = this.renderNavbar.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.setSelectedCountry = this.setSelectedCountry.bind(this);
    this.theme = this.theme.bind(this);
  }

  // Check is logged in
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getToken();
      this.props.setSignUpCallback();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // Render display
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let usernameColor = (this.props.apiLoginOptions != null) ? GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER : GLOBAL.COLOR_CONSTANT.TEXT_DARK;
    let button = (this.props.apiLoginOptions != null) ? 'Submit' : 'Next';
    return (
      <View style={styles.container}>
        {this.renderNavbar()}
        {this.props.renderConnectionStatus()}
        <View style={{flex:1, alignItems:'center', justifyContent:'center', flexDirection:'row', backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG}}>
          <ScrollView keyboardDismissMode='interactive'
            keyboardShouldPersistTaps={true}
            style={{flex: 1}}>
            <View style={styles.loginContainer}>
              <Image style={{height: 175, width: 175}} source={require('../../../images/kclogoicon.png')} />
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON, marginTop: 50}]}>
                {
                  (this.props.apiLoginOptions != null)
                    ? <Text style={{color: GLOBAL.COLOR_CONSTANT.ERROR, fontSize:10, textAlign:'center', marginBottom:10}}>
                        Please provide your mobile number to proceed
                      </Text>
                    : null
                }
                <TextInput ref='username'
                  placeholder='Username'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER}
                  value={this.state.username}
                  onChange={(event) => {
                    let text = event.nativeEvent.text
                    text = (text.trim() == '') ? '' : text;
                    this.setState({
                      username:  text
                    });
                  }}
                  onEndEditing={(event) => {
                    this.setState({
                      username:  event.nativeEvent.text.toLowerCase()
                    })
                  }}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.mobile.focus();}}
                  style={[styles.input, {backgroundColor: 'transparent', color: usernameColor}]}
                  editable={(this.props.apiLoginOptions == null)}
                  autoCapitalize='none'
                />
              </View>
              <TouchableHighlight onPress={() => Navigation.onPress(() => this.onPressCountry())} underlayColor={'#e8e5e6'}
                style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <View style={{flexDirection: 'row', justifyContent: 'center', flex:1}}>
                  <Text
                    style={[styles.input, {color: GLOBAL.COLOR_CONSTANT.TEXT_DARK, textAlignVertical: 'center', textAlign: 'right'}]} >
                    {this.state.selectedCountry}
                  </Text>
                  <Text
                    style={[styles.input, {color: GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER, textAlignVertical: 'center', textAlign: 'left'}]} >
                    {this.state.selectedCode}
                  </Text>
                </View>
              </TouchableHighlight>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput ref='mobile'
                  placeholder='Mobile Number'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER}
                  value={this.state.mobile}
                  onChangeText={(mobile) => {
                    mobile = (mobile.trim() == '') ? '' : mobile;
                    this.setState({mobile: mobile});
                  }}
                  keyboardType='phone-pad'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.signInWithKliklabs()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <Button text={button}
               color={GLOBAL.COLOR_CONSTANT.THEME}
               underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
               onPress={() => this.signInWithKliklabs()}
               style={{marginTop:15}}
               isDefaultWidth={true}/>
            </View>
            <KeyboardSpacer />
          </ScrollView>
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderNavbar() {
    return (
      <Toolbar
        title={'Sign Up'}
        withBackButton={true}
        onIconClicked={this.props.goToSignUp}
        {...this.props}
      />
    );
  }

  getToken() {
    ddp.call('apiGuestCred', ['android']).then((response)=>{
      if (response) {
        if (this._isMounted) {
          this.setState({token: response});
        }
      } else {
        this.setIsShowMessageModal('Connection Problem');
      }
    });
  }

  theme() {
    return this.state.theme;
  }

  setSelectedCountry(country) {
    this.setState({
      selectedCountry: country.name,
      selectedCode: country.dial_code
    });
  }

  onPressCountry() {
    this.refs.username.blur();
    this.refs.mobile.blur();

      requestAnimationFrame(() => {
        this.props.navigator.push({
          name:'InputCountryCode',
          theme: this.theme,
          selectedCountry: this.state.selectedCountry,
          setSelectedCountry: this.setSelectedCountry
        });
      });

  }

  signInWithKliklabs(){
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      let code = this.state.selectedCode.replace('+', '');
      let mobileInput = this.state.mobile;
      if (mobileInput.charAt(0) == '0') {
        mobileInput = mobileInput.substr(1, mobileInput.length-1);
      }
      var mobile = code + mobileInput;
      var numberRegex = /^[0-9]*$/;

      if (this.state.token) {
        if (!this.state.username || !this.state.mobile) {
          this.setIsShowMessageModal('Please enter username or mobile number');
        } else if (!numberRegex.test(this.state.mobile)) {
          this.setIsShowMessageModal('Please enter valid mobile number');
        } else if (this.state.mobile.length < MINIMUM_MOBILE_CHAR) {
          this.setIsShowMessageModal('Minimum mobile number is 10 characters')
        } else {
          this.refs.username.blur();
          this.refs.mobile.blur();

          this.setIsLoading(true);

          if (this.props.apiLoginOptions) {
            var toEncrypt = {
              username: this.state.username,
              mobile: mobile
            };

            SystemAccessor.encrypt(JSON.stringify(toEncrypt), this.props.token.substring(0, 16), (err, options) => {
              if (options) {
                ddp.call('apiUpdateMobile', [options, this.props.token]).then((toDecrypt)=> {
                  if (Object.prototype.toString.call(toDecrypt) != "[object Object]") {
                    SystemAccessor.decrypt(toDecrypt, this.props.token.substring(0, 16), (err, response) => {
                      response = JSON.parse(response);

                      if(GLOBAL.TEST_MODE) {
                        response.sukses = true;
                        console.log('--- TEST MODE KliklabsSignUp - apiUpdateMobile response=', response);
                      }

                      if (response.sukses) {
                        this.goToConfirmCode(response, mobile);
                      } else {
                        this.setIsShowMessageModal(response.msg);
                      }
                      this.setIsLoading(false);
                    });
                  } else {
                    this.setIsShowMessageModal("Unexpected behavior with apiUpdateMobile(). \nMsg: ", toDecrypt.msg);
                    this.setIsLoading(false);
                  }
                });
              } else {
                this.setIsShowMessageModal("Something went wrong. Please try again.");
                this.setIsLoading(false);
              }
            });
          } else {
            var toEncrypt = {
              username: this.state.username,
              mobile: mobile
            };

            SystemAccessor.encrypt(JSON.stringify(toEncrypt), this.state.token.substring(0, 16), (err, credentials) => {
              if (credentials) {
                ddp.call('apiCheckUsername', [credentials, this.state.token]).then((toDecrypt) => {
                  if (Object.prototype.toString.call(toDecrypt) != "[object Object]") {
                    SystemAccessor.decrypt(toDecrypt, this.state.token.substring(0, 16), (err, response) => {
                      response = JSON.parse(response);

                      if(GLOBAL.TEST_MODE) {
                        response.sukses = true;
                        console.log('--- TEST MODE KliklabsSignUp - apiCheckUsername response=', response);
                      }

                      if (response.sukses) {
                        this.goToConfirmCode(response, mobile);
                      } else {
                        let message = response.msg;
                        if (message == 'MobileAlreadyExist') {
                          this.setIsShowMessageModal('Your mobile phone is already registered.');
                        } else if (message == 'UsernameAlreadyExist') {
                          this.setIsShowMessageModal('Username is already taken.');
                        } else {
                          this.setIsShowMessageModal(message);
                        }
                      }
                      this.setIsLoading(false);
                    });
                  } else {//assume failed, username exists. probably
                    let message = toDecrypt.msg;
                    if (message == 'MobileAlreadyExist') {
                      this.setIsShowMessageModal('Your mobile phone is already registered.');
                    } else if (message == 'UsernameAlreadyExist' || message == 'Username Exist Already') {
                      this.setIsShowMessageModal('Username is already taken.');
                    } else {
                      this.setIsShowMessageModal(message);
                    }
                    this.setIsLoading(false);
                  }
                });
              } else {
                this.setIsLoading(false);
                this.setIsShowMessageModal('Something went wrong. Please try again.');
              }
            });
          }
        }
      } else {
        this.setIsShowMessageModal('Problem with token');
      }
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsLoading(isLoading) {
    if (this._isMounted) {
      this.setState({isLoading: isLoading});
    }
  }

  renderModal() {
    if (this.state.isLoading) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  goToConfirmCode(response, mobile) {
    var register = true;
    if (this.props.apiLoginOptions) {
      console.log("Dont register");
      register = false;
    }
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'kliklabsconfirmcode',
        name:'KliklabsConfirmCode',
        username: this.state.username,
        mobile: mobile,
        token: this.state.token,
        urlstring: response.url,
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        debugText: this.props.debugText,
        getMessageWallpaper: this.props.getMessageWallpaper,
        goToSignUp: this.props.goToSignUp,
        hasInternet: this.props.hasInternet,
        hasNewFriendRequest: this.props.hasNewFriendRequest,
        isDebug: this.props.isDebug,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setLoggedInUser: this.props.setLoggedInUser,
        setSignUpCallback: this.props.setSignUpCallback,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        apiLoginOptions: this.props.apiLoginOptions,
        register: register
      });
    });
  }
}

module.exports = KliklabsSignUp;
