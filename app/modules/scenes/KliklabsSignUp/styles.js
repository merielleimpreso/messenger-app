import { StyleSheet } from 'react-native';
import {
  Dimensions
} from 'react-native';
var SCREEN = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 35,
    padding: 5,
    textAlign: 'center',
    flex: 1
  },
  inputLoginContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    width: SCREEN.width * 0.75,
    borderBottomWidth: 0.5
  },
  loginContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  }
});
