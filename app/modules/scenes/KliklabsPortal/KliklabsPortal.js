'use strict';

import React, {
  Component
} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
  Platform,
  StatusBar
} from 'react-native';
import {logTime} from '../../Helpers.js';
import Button from '../../common/Button';
import GLOBAL from '../../config/Globals';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import RenderModal from '../../common/RenderModal';
import styles from './styles';
import Tabs from '../../Tabs';
import Users from '../../config/db/Users';

var SCREEN = Dimensions.get("window");

class KliklabsPortal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isNotConnected: false,
      loading: true,
      isLoaded: false
    }
    this.renderModal = this.renderModal.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    if (this.props.loggedInUser()) {
      return this.renderTabs();
    } else {
      return (
        <View style={[styles.container, {backgroundColor:GLOBAL.COLOR_CONSTANT.CHAT_BG}]}>
          {(Platform.OS == 'android') ? <StatusBar backgroundColor={GLOBAL.COLOR_CONSTANT.STATUS_BAR}/> : null}
          {this.props.renderConnectionStatus()}
          {this.renderLoginForm()}
          {this.renderModal()}
        </View>
      );
    }
  }

  renderTabs() {
    return (
      <Tabs navigator={this.props.navigator}
        loggedInUser={this.props.loggedInUser}
        hasInternet={this.props.hasInternet}
        renderConnectionStatus={this.props.renderConnectionStatus}
        shouldReloadData={this.props.shouldReloadData}
        theme={this.props.theme}
        changeTheme={this.props.changeTheme}
        addContactToLoggedInUser={this.addContactToLoggedInUser}
        changeBgImage={this.props.changeBgImage}
        bgImage={this.props.bgImage}
        setImage={this.props.setImage}
        subscribeToLoggedInUser={this.props.subscribeToLoggedInUser}
        logout={this.props.logout}
      />
    );
  }

  renderLoginForm() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center', flexDirection:'row', backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG}}>
          <View style={styles.loginContainer}>
            <Image style={{width:SCREEN.width*0.55, height:SCREEN.height*0.35, resizeMode:'contain'}}
              source={require('../../../images/klikchatlogo.png')}/>
            <Button text="Log In" color={GLOBAL.COLOR_CONSTANT.THEME}
              underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
              onPress={() => this.goToLogin()} style={{marginTop:30}}
              playSound={this.props.playSound}
              isDefaultWidth={true}/>
            <Button text="Sign Up" color={GLOBAL.COLOR_CONSTANT.THEME}
              underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
              onPress={() => this.goToKliklabs()} style={{marginTop:10}}
              playSound={this.props.playSound}
              isDefaultWidth={true}/>
          </View>
      </View>
    );
  }

  goToLogin() {
    if (this.props.hasInternet()) {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name: 'Login',
          id: 'login',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          debugText: this.props.debugText,
          getMessageWallpaper: this.props.getMessageWallpaper,
          goToSignUp: this.props.goToSignUp,
          hasInternet: this.props.hasInternet,
          isDebug: this.props.isDebug,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setLoggedInUser: this.props.setLoggedInUser,
          setSignUpCallback: this.props.setSignUpCallback,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          checkLoginToken: this.props.checkLoginToken
        });
      });
    } else {
      this.setIsNotConnected(true);
    }
  }

  goToKliklabs(){
    if (this.props.hasInternet()) {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name:'KliklabsSignUp',
          id: 'kliklabssignup',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          debugText: this.props.debugText,
          getMessageWallpaper: this.props.getMessageWallpaper,
          goToSignUp: this.props.goToSignUp,
          hasInternet: this.props.hasInternet,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          isDebug: this.props.isDebug,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setLoggedInUser: this.props.setLoggedInUser,
          setSignUpCallback: this.props.setSignUpCallback,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
        });
      });
    } else {
      this.setIsNotConnected(true);
    }
  }

  setIsNotConnected(isConnected) {
    if (this._isMounted) {
      this.setState({isNotConnected: isConnected});
    }
  }

  renderModal() {
    if (this.state.isNotConnected) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsNotConnected(false)}
          message={this.state.alertMessage}
          modalType={'connection'}
        />
      );
    }
  }
}

module.exports = KliklabsPortal;
