'use strict';
import {
  Actions,
  Bubble,
  GiftedChat,
  LoadEarlier,
  MessageImage,
  MessageText,
  Send,
  Time
} from '../../GiftedChat/GiftedChat';
import React, {
  Component,
} from 'react';
import ReactNative, {
  ActivityIndicator,
  AppState,
  Dimensions,
  Image,
  InteractionManager,
  LayoutAnimation,
  Platform,
  Text,
  TouchableHighlight,
  UIManager,
  View
} from 'react-native';
import { toNameCase } from '../../Helpers';
import GLOBAL from '../../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import MessagesGiftedChatCustomView from '../../MessagesGiftedChatCustomView';
import RenderModal from '../../common/RenderModal';
import Toolbar from '../../common/Toolbar';
import TypingAreaOptions from '../../TypingAreaOptions';
const NAVBAR_HEIGHT = 50;
const SCREEN_SIZE = Dimensions.get('window');

export default class MesssagesGiftedChatRender extends Component {
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Image style={{width: null, height: null, flex: 1, flexDirection: 'column'}}
        resizeMode={'cover'}
        source={this.renderBackgroundImage()}>
        {this.renderToolbar()}
        <GiftedChat
          color={color}
          {...this.props}
          messages={this.props.messageData}
          renderActions={this.renderActions.bind(this)}
          renderBackgroundImage={this.renderBackgroundImage.bind(this)}
          renderBubble={this.renderBubble.bind(this)}
          renderConnectionStatus={this.renderConnectionStatus.bind(this)}
          renderCustomView={this.renderCustomView.bind(this)}
          renderFriendRequest={this.renderFriendRequest.bind(this)}
          renderLoadEarlier={this.renderLoadEarlier.bind(this)}
          renderMessageText={this.renderMessageText.bind(this)}
          renderTailBubble={this.renderTailBubble.bind(this)}
          renderTime={this.renderTime.bind(this)}
          renderTypingAreaOptions={this.renderTypingAreaOptions.bind(this)}
          user={{_id: this.props.loggedInUser()._id}}
          renderNotInParty={this.renderNotInParty.bind(this)}
        />
        {this.renderModal()}
      </Image>
    );
  }

  renderToolbar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let name = (this.props.otherUserDetails) ? toNameCase(this.props.otherUserDetails.name) : toNameCase(this.props.user.name);
    let style = (Platform.OS === 'ios') ? {zIndex: 1} : {};
    return (
        <View style={style}>
          <Toolbar
            rightIcon={'options1'}
            title={name}
            withBackButton={true}
            {...this.props}
          />
        </View>
    );
  }

  renderActions(props) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Actions
        {...props}
        color={color}
        setIsOptionsPress={() => requestAnimationFrame(() => this.props.setActive('options')) }
        setIsEmojiPress={() => requestAnimationFrame(() => this.props.setActive('stickers')) }
      />
    );
  }

  renderBackgroundImage() {
    var bgRetriever = this.props.backgroundImageSrc;
    var bgImage = bgRetriever();
      if (bgImage == null || bgImage == '') {
        if (this.props.theme() == "ORIGINAL") {
          return require('../../../images/chatbgdefault.jpg');
        } else if (this.props.theme() == "BLUE") {
          return require('../../../images/backgroundImage/chatbg1.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('../../../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "LAVENDER") {
          return require('../../../images/backgroundImage/themebg2b.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('../../../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "ORIGINAL") {
          return require('../../../images/backgroundImage/themebg3b.jpg');
        } else {
          return null;
        }
      } else {
        return {uri: bgImage};
      }
    // return require('./../images/chatbgdefault.jpg')
  }

  renderBubble(props) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: color.CHAT_SENT,
          },
          left: {
            backgroundColor: color.CHAT_RCVD,
          }
        }}
      />
    );
  }

  renderCustomView(props) {
    return (
      <MessagesGiftedChatCustomView
        {...props}
        acceptFriendRequest={this.props.acceptRequest}
        // audio={this.props.audio}
        color={GLOBAL.COLOR_THEME[this.props.theme()]}
        // currentTime={this.props.currentTime}
        // goToMessageChat={this.props.goToMessageChat}
        // goToVideoPlayer={this.props.goToVideoPlayer}
        // isPaused={this.props.isPaused}
        // hasInternet={this.props.hasInternet}
        // loggedInUser={this.props.loggedInUser}
        // playAudio={this.props.playAudio}
        // setAudio={this.props.setAudio}
        // setReuploadMedia={this.props.setReuploadMedia}
      />
    );
  }

  renderTailBubble() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{
          width: 0,
          height: 0,
          backgroundColor: 'transparent',
          borderStyle: 'solid',
          borderRightWidth: 10,
          borderTopWidth: 10,
          borderRightColor: 'transparent',
          borderTopColor: color.BUTTON_TEXT,
          marginRight: -7,
          alignSelf: 'flex-start'
        }}
      />
    )
  }

  renderFriendRequest() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View elevation={10} onLayout={(event) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <View style={{backgroundColor: color.CHAT_BG, flex: 1, flexDirection: 'row', height: 40}}>
          <TouchableHighlight style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.onActionPress(this.props.user._id, 'ignore')}>
            <Text style={{color: color.THEME}}>BLOCK</Text>
          </TouchableHighlight>
          <TouchableHighlight style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.onActionPress(this.props.user._id, 'accept')}>
            <Text style={{color: color.THEME}}>ACCEPT</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderNotInParty(msgFor) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let message = (msgFor == 'group') ? 'You are no longer part of this group.' : 'You have blocked this user.'
    return (
      <View style={{backgroundColor: color.CHAT_BG, alignItems: 'center', justifyContent: 'center', flex: 1, height: 40}}>
        <Text style={{color: color.THEME}}>{message}</Text>
      </View>
    );
  }

  renderLoadEarlier(props) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let wrapperStyle = {};

    if (this.props.isLoading) {
      return (
        <ActivityIndicator color={color.BUTTON} size='small' style={{marginBottom: 10}}/>
      )
    } else {
      return null;
    }
  }

  renderMessageText(props) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <MessageText
      {...props}
      textStyle={{
        right: {
          color: color.TEXT_DARK,
        },
        left: {
          color: color.TEXT_DARK,
        }
      }}
      />
    );
  }

  renderTime(props) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Time
      {...props}
      textStyle={{
        right: {
          color: color.TEXT_LIGHT,
        },
        left: {
          color: color.TEXT_LIGHT,
        }
      }}
      />
    );
  }

  renderModal() {
    if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowNotParty) {
      return(
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowNotParty}
          message={"You are no longer part of this group."}
          modalType={'alert'}
        />
      );
    } else if (this.props.isShowModalLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loading'}
        />
      );
    } else if (this.props.isShowModalOption) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowModalOption}
          onPress={this.props.onPressLeave}
          modalType={'options'}
        />
      );
    } else if (this.props.isShowConverting) {
      console.log("showConverting");
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={()=>{ this.props.setisShowConverting}}
          modalType={'converting'}
        />
      );
    } else if(this.props.isShowFileTooBig) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowFileTooBig}
          message={"File greater than 25mb. Reduce quality or length."}
          modalType={'alert'}
        />
      );
    }
    // else if (this.props.isShowChatMenu) {
    //   let actions = this.props.toolbarActions;
    //   if (this.props.user._id.includes('group')) {
    //     actions[0].action = this.props.goToGroupDetails;
    //     actions[1].action = this.props.goToChatWallpaper;
    //     actions[2].action = this.props.onPressLeave;
    //   } else {
    //     actions[0].action = this.props.goToChatWallpaper;
    //   }
    //   return (
    //     <RenderModal
    //       theme={this.props.theme}
    //       setIsShowModal={() => this.props.onPressRightButton()}
    //       actions={actions}
    //       modalType={'menu'}
    //     />
    //   );
    // }
  }

  renderConnectionStatus() {
    return (
      <View style={{top: 0, left: 0, right: 0, position: 'absolute', alignSelf: 'flex-start'}}>
        <View onLayout={(event) => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          // this.setState({connectionStatusHeight: event.nativeEvent.layout.height});
        }}>
          {this.props.renderConnectionStatus()}
        </View>
      </View>
    );
  }

  renderTypingAreaOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let typeAreaHeight = 40;
    let chatHeight = SCREEN_SIZE.height - NAVBAR_HEIGHT - 25 - typeAreaHeight;
    let recipientId = this.props.user._id;
    let top = (this.props.active() == 'stickers' || this.props.active() == 'audio' || this.props.active() == 'options') ? 0 : -chatHeight;
    return (
      <View style={{top: top, left: 0, right: 0, position: 'absolute', alignSelf: 'flex-start'}}>
        <TypingAreaOptions
          // active={this.props.active}
          color={color}
          height={chatHeight}
          // navigator={this.props.navigator}
          // loggedInUser={this.props.loggedInUser}
          recipientId={recipientId}
          chatType={GLOBAL.CHAT.DIRECT}
          // setActive={this.props.setActive}
          // setIsSafeToBack={this.props.setIsSafeToBack}
          // setIsRecordPress={this.props.setIsRecordPress}
          // setIsEmojiPress={this.props.setIsEmojiPress}
          // setIsOptionsPress={this.props.setIsOptionsPress}
          // isEmojiPress={this.props.isEmojiPress}
          // isOptionsPress={this.props.isOptionsPress}
          // isRecordPress={this.props.isRecordPress}
          // theme={this.props.theme}
          // hasInternet={this.props.hasInternet}
          // isPartOfGroup={this.props.isPartOfGroup}
          renderNotInParty={this.renderNotInParty}
          // checkIfPartOfGroup={this.props.checkIfPartOfGroup}
          // setIsShowNotParty={this.props.setIsShowNotParty}
          // setIsShowCompressing={this.props.setIsShowCompressing}
          // setIsShowFileTooBig={this.props.setIsShowFileTooBig}
          {...this.props}
        />
      </View>
    );
  }
}
