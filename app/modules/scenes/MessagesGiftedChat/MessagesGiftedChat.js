
import React, { Component } from 'react';
import { GiftedChat } from '../../GiftedChat/GiftedChat';
import ReactNative, {
  AppState,
  InteractionManager,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import _ from 'underscore';
import { AudioPlayer } from 'react-native-audio';
import { getMediaURL } from '../../Helpers';
import ActionSheet from '@exponent/react-native-action-sheet';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import Download from '../../config/db/Download';
import GLOBAL from '../../config/Globals.js';
import FCM from 'react-native-fcm';
import Message from '../../config/db/Message';
import MessagesGiftedChatRender from './MessagesGiftedChatRender';
import Send from '../../config/db/Send';
import StoreCache from '../../config/db/StoreCache';
import Users from '../../config/db/Users';
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

let waitingToRemoveNotif = false;
let timeInterval;
const TOOLBAR_ACTIONS = {
  Group: [{title: 'Group Details', iconName: 'group', show: 'never'},
          {title: 'Chat Wallpaper', iconName: 'settings', show: 'never'},
          {title: 'Leave Group', iconName: 'sending', show: 'never'}],
  Direct: [{title: 'Chat Wallpaper', iconName: 'settings', show: 'always'}]
};

class MessagesGiftedChat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: null,
      appState: null,
      cachedMessages: [],
      pendingCount: 0,
      connectionStatusHeight: 0,
      loadEarlier: true,
      loggedInUser: this.props.loggedInUser(),
      onProgress: false,
      messages: [],
      groupUsers: [],
      imageGallery: [],
      isAllMessagesLoaded: false,
      isLoading: true,
      isDoneSettingFailedUploads: false,
      isEmojiPress: false,
      isFirstLoad: true,
      isLoggedIn: this.props.isLoggedIn(),
      isOptionsPress: false,
      isPaused: true,
      isRecordPress: false,
      isRemovedFromGroup: this.props.loggedInUser().groups[this.props.user._id] ? true : false,
      isUpdatingMessages: true,
      isShowChatMenu: false,
      isShowCheckConnection: false,
      isShowConverting: false,
      isShowModalLoading: false,
      isShowModalOption: false,
      isShowFileTooBig: false,
      mediaCache: {},
      otherUserDetails: this.props.user,
      reuploadMedia: false,
      shouldCheckIfExist: false,
      shouldSendFailedMsgs:this.props.isLoggedIn(),
      shouldShowKeyboard: false,
      toolbarActions: this.getToolbarActions(),
      isShowNotParty: false,
      isPartOfGroup: true,
      shouldPlaySound: false,
      noConnection: false
    };
    this.audioPlayer = this.audioPlayer.bind(this);
    this.generateMessages = this.generateMessages.bind(this);
    this.getCachedMessages = this.getCachedMessages.bind(this);
    this.getGroupUsers = this.getGroupUsers.bind(this);
    this.getToolbarActions = this.getToolbarActions.bind(this);
    this.goToVideoPlayer = this.goToVideoPlayer.bind(this);
    this.observeById = this.observeById.bind(this);
    this.onPressRightButton = this.onPressRightButton.bind(this);
    this.setMessageStates = this.setMessageStates.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.onActionPress = this.onActionPress.bind(this);
    this.noConnection = this.noConnection.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getCachedMessages();
    AppState.addEventListener('change', this.handleAppState);

    InteractionManager.runAfterInteractions(() => {
      if (this.props.hasInternet()) {
        this.getGroupUsers();
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this._isMounted && waitingToRemoveNotif) {
      this.cancelNotification(this.props.user._id);
      waitingToRemoveNotif = false;
    }

    if (this.state.isLoggedIn != this.props.isLoggedIn()) {
      console.log('MessagesGiftedChat: Detected login change: ' + this.props.isLoggedIn());
      if (this._isMounted) {
        this.setState({
          isLoggedIn: this.props.isLoggedIn()
        });
      }
      if (this.props.isLoggedIn()) {
        this.getGroupUsers();
        if (this.props.setShouldResubscribe) {
          this.props.setShouldResubscribe(true);
        }

        if (this._isMounted) {
          this.setState({
            shouldSendFailedMsgs: true
          });
        }
      } else {
        console.log('MessagesGiftedChat: stop observing');
        // Message.stopObserving();
        if (this._isMounted) {
          this.setState({
            shouldSendFailedMsgs: false
          });
        }
      }
    }
    if (JSON.stringify(this.state.loggedInUser) !== JSON.stringify(this.props.loggedInUser())) {
      if (this.props.user._id.includes('group')) {
        let isRemoved = this.props.loggedInUser().groups[this.props.user._id] ? true : false;
        if (this.state.isRemovedFromGroup != isRemoved) {
          Message.stopObserving();
          this.getGroupUsers();
          if (this._isMounted) {
            this.setState({
              isRemovedFromGroup: isRemoved,
              toolbarActions:  this.getToolbarActions()
            });
          }
        }
      }
      if (this._isMounted) {
        this.setState({
          loggedInUser: this.props.loggedInUser()
        });
      }
    }

    if (this.props.audio() == null && !this.state.isPaused) {
      clearInterval(timeInterval);
      this.setState({
        isPaused: true,
        currentTime: 0
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    console.log('MessagesGiftedChat: will unmount');
    if (this.props.audio() && !this.state.isPaused) {
      AudioPlayer.stop();
      clearInterval(timeInterval);
    }
    this.props.setAudio(null);
    AppState.removeEventListener('change', this.handleAppState);
    Message.stopObserving();
    Users.stopObservingOtherUser();
  }

  getToolbarActions() {
    let toolbarActions = (this.props.user._id.includes('group_')) ?
                          (!this.props.loggedInUser().groups[this.props.user._id]) ? TOOLBAR_ACTIONS.Group : [] : TOOLBAR_ACTIONS.Direct;
    return toolbarActions;
  }

  handleAppState(appState) {
    if (this._isMounted) {
      this.setState((previousState) => {
        if (previousState.appState.match(/inactive|background/) && appState == 'active') {
          this.subscribe();
        }
        return appState
      });
    }
  }

  getCachedMessages() {
    let id = this.props.user._id;
    let userId = this.props.loggedInUser()._id;
    Message.getCache(userId, id, result => {
      StoreCache.getCache(StoreCache.keys.media, (mediaCache) => {
        if (result) {
          if (id.includes('group')) {
            Message.getGroupUsers(id, (groupUsers) => {
              if (groupUsers) {
                if (this._isMounted) {
                  this.setState({groupUsers: groupUsers});
                }
                this.setMessageStates(result, mediaCache);
              }
            });
          } else {
            this.setMessageStates(result, mediaCache);
          }
        }
      });
    });
  }

  setMessageStates(result, mediaCache) {
    let msgs = _.sortBy(this.generateMessages(result, mediaCache), 'createdAt').reverse();
    console.log('Loaded cache');
    this.setState({
      cachedMessages: result,
      count: msgs.length,
      isLoading: (msgs) ? false : true,
      isUpdatingMessages: true,
      mediaCache: mediaCache,
      messages: msgs,
    })
  }

  getGroupUsers() {
    let id = this.props.user._id;
    if (id.includes('group')) {
      ddp.call('getGroupAllUsersById', [id]).then(users => {
        if (users) {
          Message.saveGroupsUsers(id, users);
          if (this._isMounted) {
            this.setState({
              groupUsers: users
            });
            this.subscribe();
          }
        }
      })
    } else {
      this.subscribe();
    }
  }

  subscribe() {
    if (this.props.hasInternet()) {
      if (this._isMounted) {
        this.setState({
          isLoading: true
        });
      }
      let id = this.props.user._id;
      let userId = this.props.loggedInUser()._id;
      // Users.subscribeToOtherUser(id).then(() => {
        Message.subscribeByUserId(this.props.user._id).then(() => {
          Message.upsertFailedMessages(this.state.cachedMessages);
          this.observeById(userId, id);
        }).catch((err) => {

          this.setState({
            isLoading: false
          });

          console.log('error: ' + err);
        });
      // });
    } else {
      if (this._isMounted) {
        this.setState({
          isLoading: false
        });
      }
    }
  }

  observeById(userId, id) {
    requestAnimationFrame(() => {
      Users.observeOtherUser(id, (otherUser) => {
        if (JSON.stringify(otherUser) != JSON.stringify(this.state.otherUserDetails)) {
          if (this._isMounted) {
            this.setState({
              otherUserDetails: otherUser
            });
          }
        }
        Message.observeByUserId(this.props.loggedInUser(), userId, id, (messages) => {
          Message.getFailedMessages(messages, this.state.cachedMessages, (failedMessages, stillOnProgress) => {
            StoreCache.getCache(StoreCache.keys.media, (mediaCache) => {
              let msgs = _.sortBy(this.generateMessages(messages, mediaCache), 'createdAt').reverse();
              if (JSON.stringify(msgs) != JSON.stringify(this.state.messages)) {
                let pendingCount = Message.countPendingMessages(this.props.loggedInUser()._id);
                if (pendingCount < this.state.pendingCount) {
                  Message.playSound();
                }

                Message.storeCache();
                if (this._isMounted) {
                  this.setState({
                    isLoading: false,
                    mediaCache: mediaCache,
                    messages: msgs,
                    pendingCount: pendingCount,
                    imageGallery: this.state.imageGallery,
                    isUpdatingMessages: true
                  })
                }
                else {
                  waitingToRemoveNotif = true;
                }
              } else {
                if (messages.length == 0 && this.state.cachedMessages.length == 0) {
                  if (this._isMounted) {
                    this.setState({
                      isLoading: false,
                      messages: [],
                      isUpdatingMessages: true
                    });
                  }
                } else {
                  if (this._isMounted) {
                    this.setState({
                      isLoading: false,
                    });
                  }
                }
              }
              //-- Updating seen --//
              if (this.state.isUpdatingMessages && AppState.currentState == 'active') {
                if (this.state.isLoggedIn) {

                  ddp.call('editSeenByUserId', [id]).then(result => {
                    this.cancelNotification(id);
                    console.log('editSeenByUserId', result);
                    if (this._isMounted) {
                      this.setState({
                        isUpdatingMessages: false
                      })
                    }
                  }).catch(error => {
                    console.log('editSeenByUserId: ', error);
                  })
                }
              }

              //-- Resending failed messages --//
              if (this.state.isLoggedIn) {
                if (failedMessages.length > 0 || stillOnProgress.length > 0) {
                  if (this.state.shouldSendFailedMsgs) {
                    for (let i = 0; i < failedMessages.length; i++) {
                      if (!failedMessages[i].isMedia) {
                        Send.resend(failedMessages[i]);
                      } else {
                        console.log('setFailed, failedMessages')
                        Send.setFailedUploading(failedMessages[i]);
                      }
                    }
                    for (let i = 0; i < stillOnProgress.length; i++) {
                      console.log('setFailed, stillOnProgress')
                      Send.setFailedUploading(stillOnProgress[i]);
                    }
                    if (this._isMounted) {
                      this.setState({
                        messages: msgs,
                        shouldSendFailedMsgs: false
                      });
                    }
                  }
                }
              }
            });
          });
        });
      });
    });
  }

  generateMessages(messages, mediaCache = {}) {
    let msgs = [];
    let imageGallery = [];
    let loggedInUser = this.props.loggedInUser();
    for (let i = 0; i < messages.length; i++) {
      let m = messages[i];

      if (m.message) {
        let name = (m.fromUserId == loggedInUser._id) ? loggedInUser.profile.name : this.props.user.name;
        let image = (m.fromUserId == loggedInUser._id) ? loggedInUser.profile.image : getMediaURL(this.props.user.image);

        if (this.props.user._id.includes('group')) {
          let groupUser = _.find(this.state.groupUsers, {_id : m.fromUserId});
          name = (groupUser) ? groupUser.name : 'Klikchat User';
          image = (groupUser) ? getMediaURL(groupUser.image) : getMediaURL(GLOBAL.DEFAULT_PROFILE_IMAGE);
        }

        let msg = {
          _id: m._id,
          createdAt: new Date(m.date),
          user: {
            _id: m.fromUserId,
            name: name,
            avatar: image,
          },
          otherUserId: this.props.user._id,
          seen: m.seen,
          status: m.status,
          isSticker: false,
          isGroup: this.props.user._id.includes('group')
        }
        // m.seen && (msg.seen = m.seen);

        if (m.message.audio) {
          msg.audio = m.message.audio;
          msg.isCurrentUser = (m.fromUserId == this.props.loggedInUser()._id);
          if (m.isTemporary) {
            msg.progress = m.progress;
            msg.isFailedUploading = m.isFailedUploading;
          }
        } else if (m.message.contactDetails) {
          msg.contact = m.message.contactDetails;
        } else if (m.message.location) {
          msg.location = m.message.location;
        } else if (m.message.media) {
          var image = this.adaptToImageURL(m.message.media);
          msg.image = (m.status == 'Sent') ? m.image : image[0].mediumUri;
          if (m.isTemporary) {
            msg.progress = m.progress;
            msg.isFailedUploading = m.isFailedUploading;
          }
          msg.fileSize = (image[0].fileSize) ? image[0].fileSize : null;
          imageGallery.push(msg);
        } else if (m.message.sticker) {
          msg.image = getMediaURL(m.message.sticker);
          msg.isSticker = true;
        } else if (m.message.text) {
          msg.text = m.message.text;
        } else if (m.message.video || m.message.videoUri) {
          msg.video = (m.hasOwnProperty('seen')) ? getMediaURL(m.message.video) : m.message.videoUri;
          if (m.isTemporary) {
            msg.progress = m.progress;
            msg.isFailedUploading = m.isFailedUploading;
          }
        }

        if (m.message.video || m.message.media || m.message.audio) {
          if (mediaCache) {
            let usersMediaCache = mediaCache[this.props.user._id];
            if (usersMediaCache) {
              msg.mediaCache = usersMediaCache[m._id];
            }
          }
        }
        // console.log(msg)
        msgs.push(msg);
      }
    }
    this.state.imageGallery = imageGallery;
    return msgs;
  }

  render() {
    return (
      <ActionSheet ref={component => this._actionSheetRef = component}>
        <MessagesGiftedChatRender
          active={this.active.bind(this)}
          onActionPress={this.onActionPress}
          checkIfPartOfGroup={this.checkIfPartOfGroup.bind(this)}
          currentTime={this.currentTime.bind(this)}
          goToChatWallpaper={this.goToChatWallpaper.bind(this)}
          goToGroupDetails={this.goToGroupDetails.bind(this)}
          goToMessageChat={this.goToMessageChat.bind(this)}
          goToMessageInfo={this.goToMessageInfo.bind(this)}
          goToVideoPlayer={this.goToVideoPlayer.bind(this)}
          goToViewImage={this.goToViewImage.bind(this)}
          imageGallery={this.state.imageGallery}
          isAutoSaveMedia={this.state.isAutoSaveMedia}
          isPaused={this.isPaused.bind(this)}
          isAutoSaveMedia={this.state.isAutoSaveMedia}
          isLoading={this.state.isLoading}
          isLoadingEarlier={this.state.isLoadingEarlier}
          isPartOfGroup={this.state.isPartOfGroup}
          isRemovedFromGroup={this.state.isRemovedFromGroup}
          isShowChatMenu={this.state.isShowChatMenu}
          isShowCheckConnection={this.state.isShowCheckConnection}
          isShowConverting={this.state.isShowConverting}
          isShowFileTooBig={this.state.isShowFileTooBig}
          isShowModalLoading={this.state.isShowModalLoading}
          isShowModalOption={this.state.isShowModalOption}
          isShowConverting={this.state.isShowConverting}
          loadEarlier={this.state.loadEarlier}
          messageData={this.state.messages}
          noConnection={this.noConnection}
          onLoadEarlier={this.onLoadEarlier.bind(this)}
          onActionSelected={this.onActionSelected.bind(this)}
          onPressLeave={this.onPressLeave.bind(this)}
          onPressRightButton={this.onPressRightButton.bind(this)}
          onSend={this.onSend.bind(this)}
          otherUser={this.props.user}
          otherUserDetails={this.state.otherUserDetails}
          playAudio={this.playAudio.bind(this)}
          setActive={this.setActive.bind(this)}
          setReuploadMedia={this.setReuploadMedia.bind(this)}
          setReuploadMedia={this.setReuploadMedia.bind(this)}
          setIsEmojiPress={this.setIsEmojiPress.bind(this)}
          setIsOptionsPress={this.setIsOptionsPress.bind(this)}
          setIsRecordPress={this.setIsRecordPress.bind(this)}
          setIsShowCheckConnection={this.setIsShowCheckConnection.bind(this)}
          setIsShowFileTooBig={this.setIsShowFileTooBig.bind(this)}
          setisShowConverting={this.setisShowConverting.bind(this)}
          setIsShowModalOption={this.setIsShowModalOption.bind(this)}
          setIsShowNotParty={this.setIsShowNotParty.bind(this)}
          shouldShowKeyboard={this.state.shouldShowKeyboard}
          toolbarActions={this.state.toolbarActions}
          randomMethod={this.randomMethod.bind(this)}
          {...this.props}
        />
      </ActionSheet>
    );
  }

  cancelNotification(id) {
    if (Platform.OS === 'android') {
      FCM.cancelLocalNotification(id);
    }
  }

  active() {
    return this.state.active;
  }

  isEmojiPress() {
    return this.state.isEmojiPress;
  }

  isOptionsPress() {
    return this.state.isOptionsPress;
  }

  isRecordPress() {
    return this.state.isRecordPress;
  }

  setReuploadMedia() {
    if (this._isMounted) {
      this.setState({
        reuploadMedia: true
      })
    }
  }

  setIsEmojiPress() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      if (this._isMounted) {
        this.setState({
          isEmojiPress: !this.state.isEmojiPress,
          isClickedSetIsEmojiPress: true
        });
      }

      dismissKeyboard();

      if (this.state.isOptionsPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        if (this._isMounted) {
          this.setState({isOptionsPress: !this.state.isOptionsPress});
        }
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  setIsOptionsPress() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      if (this._isMounted) {
        this.setState({
          isOptionsPress: !this.state.isOptionsPress,
          isClickedSetIsOptionsPress: true,
        });
      }
      dismissKeyboard();

      if (this.state.isEmojiPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        if (this._isMounted) {
          this.setState({
            isEmojiPress: !this.state.isEmojiPress,
            shouldShowKeyboard: false
          });
        }
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  setActive(active = null) {
    if (this._isMounted) {
      if (active != 'keyboard') {
        dismissKeyboard();
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({active: active});
      if (active) {
        this.props.setIsSafeToBack(false);
      }
    }
  }

  setIsRecordPress() {
    if (this.state.active == 'audio') {
      this.setActive(null);
    } else {
      this.setActive('audio');
    }
  }

  setIsShowModalOption() {
    if (this._isMounted) {
      this.setState({isShowModalOption: !this.state.isShowModalOption});
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowFileTooBig () {
    if (this._isMounted) {
      this.setState({isShowFileTooBig: !this.state.isShowFileTooBig});
    }
  }

  setIsShowNotParty(){

    if(this._isMounted){
      this.setState({isShowNotParty: !this.state.isShowNotParty})
    }
  }

  setisShowConverting() {
      if(this._isMounted) {
        console.log("Set isShowConverting");
        this.setState({isShowConverting: !this.state.isShowConverting});
      }
  }
  setIsShowModalLoading() {
    if (this._isMounted) {
      this.setState({isShowModalLoading: !this.state.isShowModalLoading});
    }
  }

  setShouldCheckIfExist() {
    if (this._isMounted) {
      this.setState({shouldCheckIfExist: false});
    }
  }

  shouldCheckIfExist() {
    return this.state.shouldCheckIfExist;
  }

  onPressLeave() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      // this.onPressRightButton();
      this.setIsShowModalOption();
      this.setIsShowModalLoading();
      ddp.call('leaveGroup',[this.props.user._id]).then((result)=>{
        this.setIsShowModalLoading();
        this.props.navigator.pop();
      });
    }
  }

  onPressRightButton() {
    if (this._isMounted && (Platform.OS === 'ios') && !_.isEmpty(this.getToolbarActions())) {
      // this.setState({isShowChatMenu: !this.state.isShowChatMenu});
      let options = this.state.toolbarActions.map(actions => actions.title);
      options.push('Cancel');
      const cancelButtonIndex = options.length - 1;
      this._actionSheetRef.showActionSheetWithOptions({
        options,
        cancelButtonIndex,
      },
      (buttonIndex) => {
        if (options.length > 2) {
          switch (buttonIndex) {
            case 0:
              this.goToGroupDetails();
              break;
            case 1:
              this.goToChatWallpaper();
              break;
            case 2:
              if (!this.props.loggedInUser().groups[this.props.user._id]) {
                this.setIsShowModalOption();
              } else {
                this.setIsShowNotParty()
              }
              break;
          }
        } else {
          switch (buttonIndex) {
            case 0:
              this.goToChatWallpaper();
              break;
          }
        }
      });
      return true;
    } else {
      return false;
    }
  }

  goToChatWallpaper() {
    // this.onPressRightButton();
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'ChangeWallpaper') {
      requestAnimationFrame(() => {
        this.setActive('changeWallpaper');
        this.props.navigator.push({
          name: 'ChangeWallpaper',
          theme: this.props.theme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          renderConnectionStatus: this.props.renderConnectionStatus
        });
      });
    }
  }

  goToGroupDetails() {
    // this.onPressRightButton();
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'GroupDetails') {
        let group = (this.state.otherUserDetails) ? this.state.otherUserDetails : this.props.user;
        // group['name'] = otherUserDetails ? otherUserDetails.name : group.name;
        // group['image'] = otherUserDetails ? otherUserDetails.image : group.image;
        // group['users'] = users;
        requestAnimationFrame(() => {
          this.setActive('groupDetails');
          this.props.navigator.push({
            name: 'GroupDetails',
            theme: this.props.theme,
            getGroupUsers: this.getGroupUsers,
            group: this.props.user,
            loggedInUser: this.props.loggedInUser,
            shouldReloadData: this.props.shouldReloadData,
            renderConnectionStatus: this.props.renderConnectionStatus,
            hasInternet: this.props.hasInternet,
            refreshGroups: this.props.refreshGroups,
            timeFormat: this.props.timeFormat,

            addContactToLoggedInUser: this.props.addContactToLoggedInUser,
            backgroundImageSrc: this.props.backgroundImageSrc,
            changeBgImage: this.props.changeBgImage,
            changeTheme: this.props.changeTheme,
            getMessageWallpaper: this.props.getMessageWallpaper,
            isSafeToBack: this.props.isSafeToBack,
            logout: this.props.logout,
            setLoggedInUser: this.props.setLoggedInUser,
            setIsViewVisible: this.props.setIsViewVisible,
            setIsSafeToBack: this.props.setIsSafeToBack,
            subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          });
        });
      // });
    }
  }

  goToMessageChat(contact) {
    requestAnimationFrame(() => {
      this.props.navigator.replace({
        id:'messagesgiftedchat',
        name:'MessagesGiftedChat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeBgImage: this.props.changeBgImage,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setLoggedInUser: this.props.setLoggedInUser,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        user: contact,
      });
    });
  }

  goToMessageInfo(message) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.props.loggedInUser();
    loggedInUser = {
      _id: loggedInUser._id,
      name: loggedInUser.profile.name,
      image: loggedInUser.profile.image
    }
    let users = [this.props.user, loggedInUser]
    let otherUsers = (this.props.user._id.includes('group')) ? this.state.groupUsers : users;
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id: 'messageinfo',
        name: 'MessageInfo',
        otherUsers: otherUsers,
        loggedInUser: this.props.loggedInUser,
        message: message,
        color: color,
        timeFormat: this.props.timeFormat
      });
    });
  }

  downloadMediaFile(mediaTypePath, data) {
    if (this.props.hasInternet()) {
      if (this.props.isAutoSaveMedia() && data.mediaURL.indexOf('http') !== -1) {
        Download.downloadMediaFile(mediaTypePath, data, true);
      }
    }
  }

  goToVideoPlayer(otherUserId, message) {
    let video = message.video;
    let mediaCache = (!_.isEmpty(this.state.mediaCache)) ? this.state.mediaCache[this.props.user._id] : null;
    let usersMediaCache = (mediaCache && mediaCache[message._id]) ? mediaCache[message._id] :
                            (message.mediaCache) ? message.mediaCache : null;
    let data = {
      messageId: message._id,
      otherUserId: otherUserId
    }

    let mediaTypePath = (message.user._id == this.props.loggedInUser()._id) ? Download.mediaTypePath.videoSent : Download.mediaTypePath.video;

    if (usersMediaCache) {
      Download.checkIfFileExist(usersMediaCache, (isExist) => {
        console.log('video', isExist, usersMediaCache);
        video = (isExist) ? usersMediaCache : video;
        if (!isExist) {
          data['mediaURL'] = video;
          this.downloadMediaFile(mediaTypePath, data);
        }
        this.pushToVideoPlayer(video);
      });
    } else {
      this.pushToVideoPlayer(video);
      data['mediaURL'] = video;
      this.downloadMediaFile(mediaTypePath, data);
    }
  }

  pushToVideoPlayer(video) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (video.indexOf('http') !== -1 && this.props.hasInternet()
        || video.indexOf('http') == -1 ) {
      this.setActive('watchVideo');
      this.props.navigator.push({
        id: 'videoplayer',
        name: 'VideoPlayer',
        color: color,
        data: video,
        media: 'Video',
        hasInternet: this.props.hasInternet
      });
    } else {
      this.setState({
        noConnection: true
      })
    }
  }

  goToViewImage(otherUserId, message, imageGallery) {
    let image = message.image;
    let data = {
      messageId: message._id,
      otherUserId: otherUserId
    }
    let mediaTypePath = (otherUserId == this.props.loggedInUser()._id) ? Download.mediaTypePath.imageSent : Download.mediaTypePath.image;

    if (message.mediaCache) {
      Download.checkIfFileExist(message.mediaCache, (isExist) => {
        image = (isExist) ? message.mediaCache : message.image;
        if (!isExist) {
          data['mediaURL'] = image;
          this.downloadMediaFile(mediaTypePath, data);
        }
        this.pushToViewImage(message, imageGallery);
      });
    } else {
      this.pushToViewImage(message, imageGallery);
      data['mediaURL'] = image;
      this.downloadMediaFile(mediaTypePath, data);
    }
  }

  pushToViewImage(message, imageGallery) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    this.setActive('viewImage');
    this.props.navigator.push({
      id: 'viewphoto',
      name: 'ViewPhoto',
      color: color,
      data: message,
      imageGallery: imageGallery,
      loggedInUser: this.props.loggedInUser,
      setIsSafeToBack: this.props.setIsSafeToBack
    });
  }

  onActionPress(id, action) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (action == 'accept') {
        this.acceptRequest(id);
      } else {
        this.ignoreRequest(id);
      }
    }
  }

  acceptRequest(id) {
    ddp.call('addToContacts', [id]).then(res => {
      if (res) {
        ddp.collections.contacts.upsert(res);
      }
    });
  }

  ignoreRequest(id) {
    ddp.call('ignoreFriendRequest', [id]).then(() => {
      //ddp.collections.contacts.remove({_id: id});
    });
  }

  onActionSelected(position) {
    if(!this.checkIfPartOfGroup() && this.state.groupUsers != null) {
      //this.setState({isPartOfGroup: false});
      this.setIsShowNotParty();
    } else {
      if (this.props.user._id.includes('group_') && this.state.groupUsers != null && !this.props.loggedInUser().groups[this.props.user._id]){
        if (position == 0) {
          this.goToGroupDetails();
        } else if(position == 1){
          this.goToChatWallpaper();
        } else if(position == 2) {
          if (!this.props.loggedInUser().groups[this.props.user._id]) {
            this.setIsShowModalOption();
          } else {
            this.setIsShowNotParty()
          }
        }
      } else {
        if (position == 0) {
          this.goToChatWallpaper();
        }
      }
    }

  }

  onLoadEarlier() {
    if (this.state.isLoadingEarlier == false || this.state.isFirstLoad) {
      if (this._isMounted) {
        this.setState({
          isLoadingEarlier: true
        });
      }
    }
  }

  onSend(messages = []) {
    messages[0].isFailed = true;
    if (this._isMounted) {
      this.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, messages),
          limit: this.state.limit + 1
        };
      });
    }
    let fromUserId = this.props.loggedInUser()._id;
    let toUserId = this.props.user._id;
    let message = {
      fromUserId: fromUserId,
      toUserId: toUserId,
      message: {text: messages[0].text},
      status: 'Sending'
    }
    requestAnimationFrame(() => {
      Send.newMessage(message);
    });
  }

  adaptToImageURL(image) {
    if (image[0].hasOwnProperty('url')) {
      image[0].url = getMediaURL(image[0].url);
      image[0].original = getMediaURL(image[0].original);
      image[0].small = getMediaURL(image[0].small);
      image[0].medium = getMediaURL(image[0].medium);
      image[0].large = getMediaURL(image[0].large);
    }
    return image;
  }

  checkIfPartOfGroup() {
    if (this.props.group) {
      var isPartOfGroup = false;
      var groups = this.props.loggedInUser().groups;
      var group;
      if(groups[this.props.group._id] == null )//loggedinuser is member of group with group._id
      {
        isPartOfGroup = true;
      }

      console.log("isPartOfGroup: ", isPartOfGroup);
      return isPartOfGroup;
    } else if (this.props.user) {
      return true;
    }
  }

  // play audio clip
  playAudio(audioUrl, playAudio, duration) {
    if (playAudio.indexOf('http') !== -1 && this.props.hasInternet()
        || playAudio.indexOf('http') == -1 ) {

      if (this.props.audio() == playAudio) {
        if (this.state.isPaused) { // Unpause
          console.log('unpause');
          AudioPlayer.unpause();
          timeInterval = setInterval(() => this.setCurrentTime(), 1000);
        } else {  // Pause
          console.log('pause')
          AudioPlayer.pause();
          clearInterval(timeInterval);
        }
        this.canPlayOtherAudio = true;
        this.setState({isPaused: !this.state.isPaused});
      } else {
        if (this.canPlayOtherAudio) {
          if (this.state.isPaused) {
            console.log('hey')
            clearInterval(timeInterval);
            this.canPlayOtherAudio = false;
            AudioPlayer.playWithUrl(playAudio, null);
            AudioPlayer.stop();
            this.setState({
              currentTime: duration,
              isPaused: false
            });
            this.props.setAudio(playAudio);
            this.audioPlayer(audioUrl, playAudio, duration);
          } else {
            console.log('stop to play')
            clearInterval(timeInterval);
            AudioPlayer.stop();
            this.setState({
              currentTime: duration,
            });
            this.props.setAudio(playAudio);
          }
        } else {
          console.log('PLAY')
          this.setState({
            currentTime: duration,
            isPaused: false,
          });
          this.canPlayOtherAudio = true;
          this.props.setAudio(playAudio);
          this.audioPlayer(audioUrl, playAudio, duration);
        }
      }
    } else {
      console.log('Cannot play')
      this.setState({
        noConnection: true
      });
    }
  }

  audioPlayer(audioUrl, playAudio, duration) {
    if (playAudio.indexOf('http') !== -1 && this.props.hasInternet()
        || playAudio.indexOf('http') == -1 ) {
      timeInterval = setInterval(() => this.setCurrentTime(), 1000);
      AudioPlayer.playWithUrl(playAudio, null).then(result => {
        console.log('Play', result)
      }).catch(err => {
        console.log('ERROR', err)
        clearInterval(timeInterval);
        this.props.setAudio(null);
        AudioPlayer.stop();
        this.audioPlayer(audioUrl, audioUrl, duration)
      });
    } else {
      console.log('Cannot play')
      this.setState({
        noConnection: true
      });
    }
  }

  noConnection() {
    return this.state.noConnection;
  }

  isPaused() {
    return this.state.isPaused;
  }

  currentTime() {
    return this.state.currentTime;
  }

  setCurrentTime() {
    if (this._isMounted) {
      if (this.state.currentTime > 0) {
        this.setState({
          currentTime: this.state.currentTime - 1
        })
      } else {
        this.setState({
          isPaused: true,
          currentTime: 0
        });
        this.canPlayOtherAudio = false;
        this.props.setAudio(null);
        clearInterval(timeInterval);
      }
    }
  }

  randomMethod() {
    if(this._isMounted) {
      console.log("Random method responding");
      this.setisShowConverting();
    }
  }
}

module.exports = MessagesGiftedChat;
