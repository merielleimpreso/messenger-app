/*

Author: John Rezan Leslie Baguna
Date Created: 2016-07-01
Description: Comment area for commenting.
  Parameters passed when called:

Changelogs:
*/

'use strict';

import _ from 'underscore';
import AudioRecorder from './AudioRecording';
import {consoleLog} from './Helpers';
import FileReader from 'react-native-fs';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import NavigationBar from 'react-native-navbar';

import React, {
  Component,
} from 'react';
import {
  Alert,
  Navigator,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import Stickers from './Stickers';
var Helpers = require('./Helpers.js');
var ImagePickerManager = require('NativeModules').ImagePickerManager;
var ProgressBar = require('ActivityIndicator');
var CryptoJS = require("crypto-js");

const styles = StyleSheet.create(require('./../styles/styles_typingArea'));
const theme = 'BLUE';
const GLOBAL = require('./config/Globals.js');

import file from './config/data.json';


const buttonClicked =  Object.freeze({"KEYBOARD":1, "OPTIONS":2, "STICKERS": 3, "RECORD": 4});
class CommentArea extends Component {

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      canEditTextInput: true,
      keyboardSpace:0,
      location: {
        latitude: 0,
        longitude: 0
      },
      newMessage: '',
      shouldSendLocation: false,
      shouldSendLike: false,
      shouldShowAudioRecorder: false,
      shouldShowSticker: false,
      active:buttonClicked.KEYBOARD,
      video: '',
    };
  }

  // Get location when component mounted
  componentDidMount() {
    this._isMounted = true;
    this.getCurrentLocation();
  }

  // Put flag to check if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Focus textbox when focus on componet update.
  componentDidUpdate(prevProps, prevState) {
    if (this.props.focus) {
      this.refs.messageInput.focus();
    }
    return true;
  }

  // Hide loader.
  componentWillReceiveProps(){
  }

  // Get current location
  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        if (this._isMounted) {
          this.setState({
            location: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            }
          });
        }
      },
      (error) => consoleLog('CommentArea: ' + error.message),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
  }

  // Get audio recorder status
  getAudioRecorderStatus(){
    this.setState({
      active: buttonClicked.RECORD
    });
  }

  // Render display
  render() {
    var footer = <View />;
    var color = this.props.color;

    var loader = <View />;
    if(Platform.OS == 'android'){
       var footer = <View style={styles.footer}></View>
       var loader = <View style={[styles.sendingContainer, {opacity: this.state.viewLoader}]} >
                      <ProgressBar styleAttr="SmallInverse"/>
                      {/*<Text style={{color: color.THEME}}>Sending...</Text>*/}
                    </View>;
    }
    var placeholder = (this.state.active === buttonClicked.KEYBOARD) ? 'Type a message...' : '';
    var messageOption = <View/>;
    var icon = 'plus';
    switch (this.state.active) {
    case buttonClicked.KEYBOARD:
        icon = 'plus';
        break;
    case buttonClicked.OPTIONS:
        icon = 'keyboard';
        messageOption = <View style={{height:this.state.keyboardSpace}}>
                          <View style={{flexDirection:'row', flex:1, paddingTop:15}}>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.launchImageLibrary('photo')}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                <View>
                                  <KlikFonts name="gallery" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                    GALLERY
                                  </Text>
                                </View>
                              </TouchableHighlight>
                            </View>
                            <View style={{width:1.2, backgroundColor: color.THEME,}}/>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.onCameraPress()}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                  <View>
                                    <KlikFonts name="camera" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                    <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                      TAKE PHOTO
                                    </Text>
                                  </View>
                              </TouchableHighlight>
                            </View>
                            <View style={{width:1.2, backgroundColor: color.THEME,}}/>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.launchImageLibrary('video')}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                  <View>
                                    <KlikFonts name="video" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                    <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                      TAKE A VIDEO
                                    </Text>
                                  </View>
                              </TouchableHighlight>
                            </View>
                          </View>

                          <View style={{height:1.2, backgroundColor: color.THEME, marginLeft:15, marginRight:15, flexDirection:'row',}}/>
                          <View style={{flexDirection:'row', flex:1, paddingBottom:15}}>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.onRecordPress()}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                  <View>
                                    <KlikFonts name="record" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                    <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                      VOICE
                                    </Text>
                                  </View>
                              </TouchableHighlight>
                            </View>

                            <View style={{width:1.2, backgroundColor: color.THEME,}}/>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.onLocationPress()}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                  <View>
                                    <KlikFonts name="location" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                    <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                      SHARE LOCATION
                                    </Text>
                                  </View>
                              </TouchableHighlight>
                            </View>
                            <View style={{width:1.2, backgroundColor: color.THEME,}}/>
                            <View style={{padding:15, flex:1, alignItems:'center'}}>
                              <TouchableHighlight
                                  onPress={() => this.onOptionPress()}
                                  underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                  <View>
                                    <KlikFonts name="attachment" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                                    <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                                      ATTACHMENT
                                    </Text>
                                  </View>
                              </TouchableHighlight>
                            </View>
                          </View>
                        </View>;
        break;
    case buttonClicked.STICKERS:
        icon = 'keyboard';
        messageOption = <Stickers
                          chatType={this.props.chatType}
                          keyboardSpace={this.state.keyboardSpace}
                          from={this.props.from}
                          color={this.props.color}
                          to={this.props.to}
                        />;
        break;
    case buttonClicked.RECORD:
        icon = 'keyboard';
        messageOption = <AudioRecorder
                          chatType={this.props.chatType}
                          keyboardSpace={this.state.keyboardSpace}
                          from={this.props.from}
                          to={this.props.to}
                          color={this.props.color}
                        />;
    }

    var textInput = this.state.active === buttonClicked.KEYBOARD ? <View style={styles.messageInputContainer}><TextInput
      ref="messageInput"
      placeholder={placeholder}
      placeholderTextColor='lightgray'
      multiline={true}
      autoFocus={true}
      keyboardAppearance="dark"
      style={styles.messageInput}
      editable={this.state.active === buttonClicked.KEYBOARD}
      onChangeText={(text) => this.setState({newMessage: text ? text: false})}
    /><View style={{height:1, backgroundColor: color.THEME, marginLeft:5, marginRight:5, flexDirection:'row',}}/></View> : <View style={{flex:6}}/>

    var sendIcon = (this.state.newMessage)
      ? <TouchableHighlight
        onPress={() => this.onSendPress(this.props.from, this.props.to)}
        underlayColor='#eeeeee' style={{flex:1.3, alignItems: 'center', justifyContent: 'center', backgroundColor: color.BUTTON}}>
          <KlikFonts name='send' color={color.BUTTON_TEXT} size={32}/>
        </TouchableHighlight>
      : <TouchableHighlight
        onPress={() => this.onLikePress()}
        underlayColor='#eeeeee' style={{flex:1.3, alignItems: 'center', justifyContent: 'center', backgroundColor: color.BUTTON}}>
          <KlikFonts name='like' color={color.BUTTON_TEXT} size={32}/>
        </TouchableHighlight>;

    return (
      <View style={styles.typingArea, {backgroundColor:"#fff"}}>
        <View>
        {loader}
          <View style={styles.inputSeparator}/>

            <View style={{height:0.5, backgroundColor: color.TEXT_INPUT_PLACEHOLDER, flexDirection:'row', marginTop:-2}}/>
            <View style={styles.inputContainer}>

              <TouchableHighlight
                  onPress={() => this.onOptionPress()}
                  underlayColor='#eeeeee' style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <KlikFonts name={icon} color={color.THEME} size={32}/>
              </TouchableHighlight>
              <TouchableHighlight
                  onPress={() => this.onStickerPress()}
                  underlayColor='#eeeeee' style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <KlikFonts name="emoji" color={color.THEME} size={32}/>
              </TouchableHighlight>
              {textInput}
              {sendIcon}
          </View>

          <View style={{height:0.5, backgroundColor: color.TEXT_INPUT_PLACEHOLDER, flexDirection:'row',}}/>
        </View>
        <View onLayout={(event) => {
          if (event.nativeEvent.layout.height > this.state.keyboardSpace) {
            this.setState({
              keyboardSpace: event.nativeEvent.layout.height
            });
          }
        }}>
          <KeyboardSpacer/>
        </View>
        {footer}
        {messageOption}
      </View>
    );
  }
  /*
  Called when camera button is pressed.
  The first arg will be the options object for customization, the second is
  your callback which sends object: response.
  */
  onCameraPress() {

    ImagePickerManager.launchCamera(this.state.photoOptions, (response) => {
      if (response.didCancel) {
        consoleLog('CommentArea: User cancelled image picker');
      } else if (response.error) {
        consoleLog('CommentArea: ImagePickerManager Error: ', response.error);
      } else if (response.customButton) {
        consoleLog('CommentArea: User tapped custom button: ', response.customButton);
      } else {
        consoleLog('CommentArea: ');
        consoleLog(response); // uri (on android)
        const source = {uri: 'data:image/jpeg;base64,' + response.data,
                        fileName: response.fileName,
                        isStatic: true};
        this.setState({photoSource: source});
        this.onSendPress(this.props.from, this.props.to);
      }
      this.onOptionPress();
    });
  }

  // Called when photo button is pressed.
  onPhotoPress() {
    this.selectMediaType();
  }

  // Let the user select the media type.
  selectMediaType() {
    Alert.alert(
      'Please choose',
      null,
      [
        {text: 'Photo', onPress: () => this.launchImageLibrary('photo')},
        {text: 'Video', onPress: () => this.launchImageLibrary('video')},
      ]
    )
  }
  onOptionPress(){
    console.log('hey');
    var nextCall = this.state.active === buttonClicked.KEYBOARD ? buttonClicked.OPTIONS :buttonClicked.KEYBOARD;
    this.setState({
      active: nextCall
    });


    if (nextCall === buttonClicked.KEYBOARD){
      consoleLog('CommentArea: this called');
      this.refs.messageInput.focus();
    }

  }
  /*
  Launch image library depending on what media the user selected.
  The first arg will be the options object for customization, the second is
  your callback which sends object: response.
  */
  launchImageLibrary(media) {
    this.setState({
      canEditTextInput: true,
      shouldShowSticker: false
    });
    var options = this.getMediaOptions(media);
    ImagePickerManager.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        consoleLog('CommentArea: User cancelled image picker');
      } else if (response.error) {
        consoleLog('CommentArea: ImagePickerManager Error: ', response.error);
      } else if (response.customButton) {
        consoleLog('CommentArea: User tapped custom button: ', response.customButton);
      } else {
        if (GLOBAL.IS_ACTUAL_SERVER) {
          this.sendMedia(response, media);
        } else {
          if (media == 'photo') {
            const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};
            this.setState({photoSource: source});
            this.onSendPress(this.props.from, this.props.to);
          } else {
              this.onSendPress(this.props.from, this.props.to);
          }
        }
      }
    });
  }

  // Helper method that returns the media options
  getMediaOptions(media) {
    var photoOptions = {
      cameraType: 'back',
      mediaType: 'photo',
      maxWidth: 800,
      maxHeight: 800,
      quality: 1,
      allowsEditing: false,
      noData: false,
      viewLoader: 0
    }
    var videoOptions = {
      cameraType: 'back',
      mediaType: 'video',
      videoQuality: 'low',
      durationLimit: 10,
      allowsEditing: false,
      noData: false
    }
    var options = (media == 'photo') ? photoOptions : videoOptions;
    return options;
  }

  // Called when sticker button is pressed
  onStickerPress() {
    console.log('lol');
    var nextCall = this.state.active === buttonClicked.STICKERS ? buttonClicked.KEYBOARD :  buttonClicked.STICKERS;
    this.setState({
      active: nextCall
    });

    if (nextCall === buttonClicked.KEYBOARD){
      this.refs.messageInput.focus();
    }
  }

  // Send like
  onLikePress() {
    this.setState({shouldSendLike: true});
    this.onSendPress(this.props.from, this.props.to);
  }

  // Send location
  onLocationPress() {
    this.setState({shouldSendLocation: true});
    this.onSendPress(this.props.from, this.props.to);
  }

  // Show record view
  onRecordPress() {
    this.getAudioRecorderStatus();
  }

  // Make unique ID
  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 13; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  /*
  Called when sending.
  Call insertToDirectChat or insertToGroupChat server method.
  */
  onSendPress(post) {
    if ((this.state.newMessage != '' || this.state.photoSource) && this.props.to
      || this.state.shouldSendLike
      || this.state.shouldSendLocation
      || this.state.video != '') {
      this.setState({ viewLoader: 1 });
      var tempId = "temp"+this.makeid();
      let options = {
         sender : from,
         text : this.state.newMessage ? this.state.newMessage : '',
         media: this.state.photoSource ? this.state.photoSource.uri : null,
         fileName: this.state.photoSource ? this.state.photoSource.fileName : null,
         like: this.state.shouldSendLike ? 'like' : null,
         location: this.state.shouldSendLocation ? this.state.location : null,
         video: (this.state.video != '') ? this.state.video : null,
         recipient: to,
         tempId: tempId,
         createDate: new Date(),
      }
      let tempObj = {
        CreateDate: new Date(),
        FromUserFID: from,
        Message: {
          Text: this.state.newMessage ? this.state.newMessage : '',
          Location: this.state.shouldSendLocation ? this.state.location : null,
          Media:this.state.photoURI ? [{URL:this.state.photoURI}] : null,
        },
        isTemporary: true,
        _id: tempId,
      }
      var file = Helpers.getEncyptedFile(from,to);
      FileReader.readFile(file, 'utf8').then(result => {
        var tempMsgs = JSON.parse(result);
        tempMsgs.messages.push(tempObj);
        var stringTemp = JSON.stringify(tempMsgs);
        FileReader.writeFile(file, stringTemp, 'utf8')
        .then((success) => {
          consoleLog('CommentArea: FILE EDITED!');
        })
        .catch((err) => {
          consoleLog('CommentArea: ' +err.message);
        });

      }).catch((e)=>{
        consoleLog('CommentArea: failed to read file before sending');
      });

      if (this.props.chatType == GLOBAL.CHAT.DIRECT) {
        ddp.call('insertToDirectChat', [options]);
        tempObj.ToUserFID = to;
        ddp.collections.directchat.upsert(tempObj);
      } else {
        ddp.call('insertToGroupChat', [options]);
        tempObj.GroupFID = to;
        ddp.collections.groupchat.upsert(tempObj);
      }

      this.setState({
        photoSource: null,
        fileName: null,
        shouldSendLocation: false,
        video: '',
      });
      this.props.onSendCallback();
    }
    else{
      alert('Field is empty.');
    }

    if (this.refs.messageInput) {
      this.refs.messageInput.setNativeProps({text: ''});
    }
    // this.refs.messageInput.setNativeProps({text: ''});
    this.setState({ newMessage: ''});
    // this.refs.messageInput.focus();
  }

  // Send media through XMLHttpRequest
  sendMedia(media, type) {
    var uri = '';
    var fileName = '';

    if (type == 'video' || type == 'photo') {
      uri = media.uri;
      fileName = uri.replace(/^.*[\\\/]/, '');
    }


    var tempId = "temp"+this.makeid();

    var formData = new FormData();
    formData.append('file', {uri: uri, name: fileName, type: 'multipart/form-data'});
    formData.append('sender', this.props.from);
    formData.append('recipient', this.props.to);
    formData.append('tempId', tempId);
    formData.append('type', type);

    var request = new XMLHttpRequest();
    var chatURL = this.props.chatType == GLOBAL.CHAT.DIRECT ? ddp.urlUpload: ddp.urlUploadGroup;
    request.open("POST", chatURL, true);
    request.setRequestHeader('sender', this.props.from);
    request.onreadystatechange = function(status) {
      if (request.readyState !== 4) {
        consoleLog('ready');
        return;
      }
    }

    let tempObj = {
      CreateDate: new Date(),
      FromUserFID: this.props.from,
      Message: {
        Text: this.state.newMessage ? this.state.newMessage : '',
        Location: this.state.shouldSendLocation ? this.state.location : null,
        Media: uri,
      },
      isTemporary: true,
      _id: tempId,
    }


    if (request.upload) {
      request.upload.onprogress = (event) => {
        consoleLog('CommentArea: upload onprogress', event);

        if (event.lengthComputable) {
          var uploadProgress = event.loaded / event.total;
          tempObj.progress = uploadProgress;

          if (this.props.chatType == GLOBAL.CHAT.DIRECT) {
            tempObj.ToUserFID = this.props.to;
            ddp.collections.directchat.upsert(tempObj);
          } else {
            tempObj.GroupFID = this.props.to;
            ddp.collections.groupchat.upsert(tempObj);
          }
        }
      };
    }

    request.send(formData);

  }

}

// Export module
module.exports = CommentArea;
