/*Author: Lito Ang
Date Created: 2016-03-??
Date Updated: 2016-05-17
Description: This file contains the main body of the messenger. Has the tabs.
             Contains props from index.android and passed to all the pages inside
            it's pages.
            props passed:
              -createNotif: createNotif function in index.android that spawns
              notifications.
              -signedIn: index.android state indicating whether a user is logged
              in or not.
              -navigator: index.android navigator being shared with pages, so
              that pages can call on it to load other pages.
              -changedSignedIn: index.android function called by other pages to
              change signedIn state. (props are read only, pages can't change signedIn)
              -passMemberList: called by Contacts page to pass member details
              of user's contact list to other pages.

Used In: index.android.js

Changelog:
update: 2016-04-26 (by Terence John Lampasa)
  -logout function and load bar system merged here instead of Profile.android.
update: 2016-05-03 (by Ida)
  - modified UI mockup color scheme
update: 2016-05-17 (By Terence)
  -Added capability to detect changes in network (connected/disconneted)
update: 2016-05-17 (by Ida)
 - fixing off screen display when keyboard is open(not yet done)
update: 2016-06-15 (Ida)
- Change to whatsapp color scheme
update: 2016-06-27 (ida)
- change icons to klikfonts
update: 2016-06-28 (Tere)
  -changed navigator.push() to navigator.popToTop() during logout to stop infinite stacks
update: 2016-07-01 (ida)
  - Added tintColor for Logo
update: 2016-07-12 (ida)
  - Fixed changing of color theme
update: 2016-08-02 (ida)
  - Updated react-native-scrollable-tab-view
*/

import React, {
  Component
} from 'react';
import {
  StyleSheet,
  View,
  Image,
  ToastAndroid,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  Text,
  TouchableHighlight,
  BackAndroid,
  StatusBar,
  Dimensions,
  Modal,
  InteractionManager,
  LayoutAnimation,
  AsyncStorage,
  Alert
} from 'react-native';
import _ from 'underscore';
import {logTime, getMediaURL, toNameCase} from './Helpers';
import Contacts from './Contacts';
import Messages from './Messages';
import Options from './Options';
import Timeline from './Timeline';
import ScreenLoading from './ScreenLoading';
import FacebookTabBar from './FacebookTabBar';
import Users from './config/db/Users';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import RenderModal from './common/RenderModal';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Geocoder from 'react-native-geocoder';
import SoundEffects from './actions/SoundEffects';
import StoreCache from './config/db/StoreCache';
import Toolbar from './common/Toolbar';

const SCREEN = Dimensions.get("window");
const TOOLBAR_ACTIONS = {
  Contacts: [
    {title: 'Search', iconName: 'search', show: 'always'},
    {title: 'Add Contact', iconName: 'addcontact', show: 'always'}
  ],
  Chat: [
    {title: 'New Message', show: 'never'},
    {title: 'Create Group', show: 'never'}
  ],
};
var styles = StyleSheet.create(require('./../styles/styles.main'));
var GLOBAL = require('./config/Globals.js');

class Messenger extends Component {

  constructor(props) {
    super(props);
    this.state = {
        isRefreshing: false,
        tabName: 'Contacts',
        toolbarActions: TOOLBAR_ACTIONS.Contacts,
        isShowSearchOnContacts: false,
        unseen: 0,
        isAutoLocate: false,
        isAutoSaveMedia: false,
        isOpenModalProfile: false,
        isLoggedIn: false,
        profileData: [],
        category: '',
        locationError: '',
        locality: '',
        friendRequest: this.props.loggedInUser().friendRequestReceived,
        storedFCMToken: false,
        shouldPlaySound: false,
    }
    this.setIsRefreshing = this.setIsRefreshing.bind(this);
    this.goToMessageThreadFromNotif = this.goToMessageThreadFromNotif.bind(this);
    this.renderTabMenu = this.renderTabMenu.bind(this);
    this.onActionSelected = this.onActionSelected.bind(this);
    this.goToAddMessage = this.goToAddMessage.bind(this);
    this.locationError = this.locationError.bind(this);
    this.identifyCurrentLocation = this.identifyCurrentLocation.bind(this);
    this.isAutoLocate = this.isAutoLocate.bind(this);
    this.isAutoSaveMedia = this.isAutoSaveMedia.bind(this);
    this.isShowSearchOnContacts = this.isShowSearchOnContacts.bind(this);
    this.setIsAutoLocate = this.setIsAutoLocate.bind(this);
    this.setIsAutoSaveMedia = this.setIsAutoSaveMedia.bind(this);
    this.setUnseen = this.setUnseen.bind(this);
    this.setShowSearchOnContacts =  this.setShowSearchOnContacts.bind(this);
    this.goToProfile = this.goToProfile.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.goToGroupDetails = this.goToGroupDetails.bind(this);
    this.updateMyLocationToServer = this.updateMyLocationToServer.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getCachedSettings();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isLoggedIn() != this.state.isLoggedIn) {
      if (this._isMounted) {
        if (!this.state.storedFCMToken) {
          if (this.props.isLoggedIn()) {
            ddp.call('storeFCMToken', [GLOBAL.FCM_USER_TOKEN]).then( result => {
              console.log('Tabs ['+ logTime() +']: Successfully stored FCM token');
            }).catch(error=>{
              console.log('Tabs ['+ logTime() +']: Failed to store FCM token', error);
            });
          }
          this.setState({
            isLoggedIn: this.props.isLoggedIn(),
            storedFCMToken: true
          });
        }
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    console.log("Messenger will unmount");
  }

  render() {
    return this.renderContent();
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let tabName = this.state.tabName;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={color.STATUS_BAR}/>
        <KlikFonts.ToolbarAndroid
          title={tabName}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
          actions={this.state.toolbarActions}
          onActionSelected={this.onActionSelected}
        />
        {this.renderTabs()}
      </View>
    );
  }


  renderLoading() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} style={{flex:1, alignSelf:'center'}} />
  }

  renderTabMenu(tabIndex) {
    if (this._isMounted) {
      if (tabIndex.i == 0) {
        this.setState({
          tabName: 'Contacts',
          toolbarActions: TOOLBAR_ACTIONS.Contacts
        });
      } else if (tabIndex.i == 1) {
        this.setState({
          tabName: 'Chat',
          toolbarActions: TOOLBAR_ACTIONS.Chat
        });
      } else if (tabIndex.i == 2) {
        this.setState({
          tabName: 'Timeline',
          toolbarActions: []
        });
      } else if (tabIndex.i == 3) {
        this.setState({
          tabName: 'Options',
          toolbarActions: []
        });
      }
      SoundEffects.playSound('tick');
    }
  }

  renderTabs() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    return (
      <ScrollableTabView
       ref="scrollableTabView"
       initialPage={0}
       renderTabBar={() => <FacebookTabBar color={color} unseen={this.state.unseen} tabFor="MainTab" color={color} debugText={this.props.debugText} isDebug={this.props.isDebug}/>}
       onChangeTab={(i) => this.renderTabMenu(i)}
       style={{backgroundColor: color.CHAT_BG}}>

        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="profile" >
          {this.props.renderConnectionStatus()}
          <Contacts 
            identifyCurrentLocation={this.identifyCurrentLocation}
            isAutoLocate={this.isAutoLocate}
            isAutoSaveMedia={this.isAutoSaveMedia}
            isShowSearchOnContacts={this.isShowSearchOnContacts}
            locationError={this.locationError}
            setShowSearchOnContacts={this.setShowSearchOnContacts}
            {...this.props}

            // navigator={this.props.navigator}
            // addContactToLoggedInUser={this.props.addContactToLoggedInUser}
            // backgroundImageSrc={this.props.backgroundImageSrc}
            // debugText={this.props.debugText}
            // changeTheme={this.props.changeTheme}
            // getMessageWallpaper={this.props.getMessageWallpaper}
            // groups={this.groups}
            // hasInternet={this.props.hasInternet}
            // isAutoSaveMedia={this.isAutoSaveMedia}
            // isContactsChanged={this.isContactsChanged}
            // isDebug={this.props.isDebug}
            // isLoggedIn={this.props.isLoggedIn}
            // identifyCurrentLocation={this.identifyCurrentLocation}
            // isAutoLocate={this.isAutoLocate}
            // isSafeToBack={this.props.isSafeToBack}
            // isShowSearchOnContacts={this.isShowSearchOnContacts}
            // locationError={this.locationError}
            // loggedInUser={this.props.loggedInUser}
            // logout={this.props.logout}
            // renderConnectionStatus={this.props.renderConnectionStatus}
            // setIsAutoLocate={this.setIsAutoLocate}
            // setIsContactsChanged={this.setIsContactsChanged}
            // setIsOpenProfile={this.setIsOpenProfile}
            // setIsSafeToBack={this.props.setIsSafeToBack}
            // setLoggedInUser={this.props.setLoggedInUser}
            // setShowSearchOnContacts={this.setShowSearchOnContacts}
            // shouldReloadData={this.props.shouldReloadData}
            // subscribeToLoggedInUser={this.props.subscribeToLoggedInUser}
            // theme={this.props.theme}
            // timeFormat={this.props.timeFormat}
            // usergroups={this.usergroups}
            // users={this.users}
            // userConnectionStatus={this.props.userConnectionStatus}
          />
        </View>
        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="recent" >
          {this.props.renderConnectionStatus()}
          <Messages 
            setUnseen={this.setUnseen}
            isAutoSaveMedia={this.isAutoSaveMedia}
            {...this.props}
            // navigator={this.props.navigator}
            // addContactToLoggedInUser={this.props.addContactToLoggedInUser}
            // backgroundImageSrc={this.props.backgroundImageSrc}
            // debugText={this.props.debugText}
            // changeTheme={this.props.changeTheme}
            // getMessageWallpaper={this.props.getMessageWallpaper}
            // groups={this.groups}
            // hasInternet={this.props.hasInternet}
            // isAutoSaveMedia={this.isAutoSaveMedia}
            // isDebug={this.props.isDebug}
            // isLoggedIn={this.props.isLoggedIn}
            // isSafeToBack={this.props.isSafeToBack}
            // loggedInUser={this.props.loggedInUser}
            // logout={this.props.logout}
            // renderConnectionStatus={this.props.renderConnectionStatus}
            // setIsSafeToBack={this.props.setIsSafeToBack}
            // setLoggedInUser={this.props.setLoggedInUser}
            // setUnseen={this.setUnseen}
            // shouldReloadData={this.props.shouldReloadData}
            // subscribeToLoggedInUser={this.props.subscribeToLoggedInUser}
            // theme={this.props.theme}
            // timeFormat={this.props.timeFormat}
            // usergroups={this.usergroups}
            // users={this.users}
            // isLoggingOut={this.props.isLoggingOut}
          />
        </View>
        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="timeline" >
          {this.props.renderConnectionStatus()}
          <Timeline 
            {...this.props}
            // navigator={this.props.navigator}
            // changeTheme={this.props.changeTheme}
            // hasInternet={this.props.hasInternet}
            // isSafeToBack={this.props.isSafeToBack}
            // isLoggedIn={this.props.isLoggedIn}
            // loggedInUser={this.props.loggedInUser}
            // setIsSafeToBack={this.props.setIsSafeToBack}
            // theme={this.props.theme}
            // renderConnectionStatus={this.props.renderConnectionStatus}
            // shouldReloadData={this.props.shouldReloadData}
          />
        </View>
        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="options2" >
          {this.props.renderConnectionStatus()}
          <Options 
            isAutoLocate={this.isAutoLocate}
            isAutoSaveMedia={this.isAutoSaveMedia}
            identifyCurrentLocation={this.identifyCurrentLocation}
            locationError={this.locationError}
            setIsAutoLocate={this.setIsAutoLocate}
            setIsAutoSaveMedia={this.setIsAutoSaveMedia}
            {...this.props}
            // navigator={this.props.navigator}
            // addContactToLoggedInUser={this.props.addContactToLoggedInUser}
            // backgroundImageSrc={this.props.backgroundImageSrc}
            // debugText={this.props.debugText}
            // changeTheme={this.props.changeTheme}
            // getMessageWallpaper={this.props.getMessageWallpaper}
            // hasInternet={this.props.hasInternet}
            // isLoggedIn={this.props.isLoggedIn}
            // isDebug={this.props.isDebug}
            // isSafeToBack={this.props.isSafeToBack}
            // loggedInUser={this.props.loggedInUser}
            // logout={this.props.logout}
            // renderConnectionStatus={this.props.renderConnectionStatus}
            // setIsSafeToBack={this.props.setIsSafeToBack}
            // setLoggedInUser={this.props.setLoggedInUser}
            // setSignUpCallback={this.props.setSignUpCallback}
            // shouldReloadData={this.props.shouldReloadData}
            // subscribeToLoggedInUser={this.props.subscribeToLoggedInUser}
            // theme={this.props.theme}
            // timeFormat={this.props.timeFormat}
            // checkLoginToken={this.props.checkLoginToken}
            // setIsLoggingOut={this.props.setIsLoggingOut}
          />
        </View>
      </ScrollableTabView>
    );
  }

  onPressIcon(data) {
    if (data.hasOwnProperty('category')) {
      if (data.category == 'Group') {
        return this.goToMessageChat(data, 'group');
      }
      return this.goToMessageChat(data, 'direct');
    }
    return this.goToProfile();
  }

  onActionSelected(position) {
    SoundEffects.playSound('tick');
    if (this.state.tabName == 'Contacts') {
      if (position == 0) {
        this.setShowSearchOnContacts();
      } else if (position == 1) {
        this.goToAddFriends();
      }
    } else if (this.state.tabName == 'Chat') {
      if (position == 0) {
        this.goToAddMessage();
      } else if (position == 1) {
        this.goToCreateGroup();
      }
    } else if (this.state.tabName == 'Options') {
      // if (position == 0) {
      //   //this.settings();
      // }
    }
  }

  setShowSearchOnContacts() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({isShowSearchOnContacts: !this.state.isShowSearchOnContacts})
    }
  }

  isShowSearchOnContacts() {
    return this.state.isShowSearchOnContacts;
  }

  goToProfile() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    this.setIsOpenProfile(null);
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'Profile') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name: 'Profile',
          color: color,
          renderConnectionStatus: this.props.renderConnectionStatus,
          loggedInUser: this.props.loggedInUser,
          hasInternet: this.props.hasInternet,
          shouldReloadData: this.props.shouldReloadData,
          theme: this.props.theme,
          identifyCurrentLocation: this.identifyCurrentLocation,
        });
      });
    }
  }

  goToMessageChat(data, chatType) {
    if (data == null) {
      console.log("goToMessageChat failed. data was null.");
      return;
    }
    this.setIsOpenProfile(null);
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    let groupData = null;
    let directData = null;
    if (chatType == 'group') {
      groupData = {
        _id: data._id,
        Name: data.name,
        image: data.image
      };
    } else {
      directData = data;
    }

    requestAnimationFrame(() => {
      this.props.navigator.push({
        name:'MessagesGiftedChat',
        id:'messagesgiftedchat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        group: groupData,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        changeBgImage: this.props.changeBgImage,
        timeFormat: this.props.timeFormat,
        user: directData,
        users: data.userGroups,
      });
    });
  }

  goToGroupDetails(data) {
    this.setIsOpenProfile(null);
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    let groupData = {
      _id: data._id,
      Name: data.name,
      image: data.image
    };

    if (navigatorStack[currentRoute].name !== 'GroupDetails') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name: 'GroupDetails',
          backgroundImageSrc: this.props.backgroundImageSrc,
          groupUsers: data.userGroups,
          theme: this.props.theme,
          group: groupData,
          loggedInUser: this.props.loggedInUser,
          shouldReloadData: this.props.shouldReloadData,
          renderConnectionStatus: this.props.renderConnectionStatus,
          hasInternet: this.props.hasInternet,
          refreshGroups: this.props.refreshGroups,
          timeFormat: this.props.timeFormat
        });
      });
    }
  }

  goToCreateGroup() {
    let contacts = this.props.loggedInUser().contacts;
    if (contacts.length < 2) {
      Alert.alert("Oops!", "You need at least 2 friends to create a group.");
    } else {
      if (this.props.hasInternet()) {
        requestAnimationFrame(() => {
          this.props.navigator.push({
            name:'AddGroupChat',
            addContactToLoggedInUser: this.props.addContactToLoggedInUser,
            backgroundImageSrc: this.props.backgroundImageSrc,
            changeTheme: this.props.changeTheme,
            getMessageWallpaper: this.props.getMessageWallpaper,
            hasInternet: this.props.hasInternet,
            isAutoSaveMedia: this.isAutoSaveMedia,
            isLoggedIn: this.props.isLoggedIn,
            isSafeToBack: this.props.isSafeToBack,
            loggedInUser: this.props.loggedInUser,
            logout: this.props.logout,
            renderConnectionStatus: this.props.renderConnectionStatus,
            setIsViewVisible: this.props.setIsViewVisible,
            setIsSafeToBack: this.props.setIsSafeToBack,
            shouldReloadData: this.props.shouldReloadData,
            subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
            theme: this.props.theme,
            changeBgImage: this.props.changeBgImage,
            timeFormat: this.props.timeFormat,
            users: this.users,
          });
        });
      } else {
        Alert.alert("Oops!", "You're not connected to the internet.");
      }
    }
  }

  goToMessageThreadFromNotif() {
    this.props.initializeDDP();//Use temporarily. Can't smoothen code below.
    // if(refreshDirectMessages)
    //   refreshDirectMessages();
    // if(refreshGroupMessages)
    //   refreshGroupMessages();
  }

  goToAddFriends() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'AddFriends') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name:'AddFriends',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeBgImage: this.props.changeBgImage,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          isAutoSaveMedia: this.isAutoSaveMedia,
          isSafeToBack: this.props.isSafeToBack,
          isLoggedIn: this.props.isLoggedIn,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setLoggedInUser: this.props.setLoggedInUser,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          users: this.users,
        });
      });
    }
  }

  goToAddMessage() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'FriendList') {
      if (this.props.hasInternet()) {
        let contacts = this.props.loggedInUser().contacts;
        if (contacts.length > 0) {
          requestAnimationFrame(() => {
            this.props.navigator.push({
              name: 'FriendList',
              action: 'sendNewMessage',
              addContactToLoggedInUser: this.props.addContactToLoggedInUser,
              backgroundImageSrc: this.props.backgroundImageSrc,
              changeBgImage: this.props.changeBgImage,
              changeTheme: this.props.changeTheme,
              getMessageWallpaper: this.props.getMessageWallpaper,
              hasInternet: this.props.hasInternet,
              isAutoSaveMedia: this.isAutoSaveMedia,
              isLoggedIn: this.props.isLoggedIn,
              isSafeToBack: this.props.isSafeToBack,
              loggedInUser: this.props.loggedInUser,
              logout: this.props.logout,
              renderConnectionStatus: this.props.renderConnectionStatus,
              setIsViewVisible: this.props.setIsViewVisible,
              setIsSafeToBack: this.props.setIsSafeToBack,
              shouldReloadData: this.props.shouldReloadData,
              subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
              theme: this.props.theme,
              timeFormat: this.props.timeFormat,
            })
          });
        } else {
          Alert.alert("Oops!", "You have no friend yet to send a message.");
        }
      } else {
        Alert.alert("Oops!", "You're not connected to the internet.");
      }
    }
  }

  // onRefresh() {
  //   if (this._isMounted) {
  //     this.setState({isRefreshing: true});
  //   }
  // }

  setIsRefreshing() {
    if (this._isMounted) {
      this.setState({isRefreshing: false});
    }
  }

  setUnseen(unseen) {
    if (this._isMounted) {
      this.setState({unseen: unseen});
    }
  }

  setIsAutoLocate(isAutoLocate) {
    if (this._isMounted) {
      this.setState({isAutoLocate: isAutoLocate});
      // if (isAutoLocate) {
        this.identifyCurrentLocation(true);
      // }
    }
  }

  isAutoLocate() {
    return this.state.isAutoLocate;
  }

  setIsAutoSaveMedia(isAutoSaveMedia) {
    if (this._isMounted) {
      this.setState({isAutoSaveMedia: isAutoSaveMedia});
    }
  }

  isAutoSaveMedia() {
    return this.state.isAutoSaveMedia;
  }

  locationError() {
    return this.state.locationError;
  }

  getCachedSettings() {
    StoreCache.getCache(StoreCache.keys.isAutoLocate, (result) => {
      if (result) {
        result = JSON.parse(result);
        if (this._isMounted) {
          this.setState({isAutoLocate: result});
        }
        this.identifyCurrentLocation(false);
      }
      console.log('Tabs: StoreCache isAutoLocate', result);
    });
    StoreCache.getCache(StoreCache.keys.isAutoSaveMedia, (result) => {
      if (result) {
        result = JSON.parse(result);
        if (this._isMounted) {
          this.setState({isAutoSaveMedia: result});
        }
      }
      console.log('Tabs: StoreCache isAutoSaveMedia', result);
    });
  }

  identifyCurrentLocation(isTurnedOn) {
    let id = navigator.geolocation.getCurrentPosition((position) => {
      Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(res => {
        console.log('Tabs: Successfully identified current location.');
        this.updateMyLocationToServer(position, res[0].locality)
      }).catch((error) => {
        console.log('Tabs: Failed to identify current location.');
      });
    },(error) => {
      if (this.state.isAutoLocate) {
        if (this._isMounted) {
          this.setState({locationError: error});
        }
        if (error == 'Location request timed out') {
          console.log('Tabs: getCurrentPosition timeout.');
          this.identifyCurrentLocation(false);
        } else if (isTurnedOn && error == 'No available location provider.') {
          this.setIsAutoLocate(false);
        }
      }
    },{timeout: 10000, maximumAge: 1000});
  }

  updateMyLocationToServer(position, locality) {
    var location = {latitude: position.coords.latitude, longitude: position.coords.longitude, locality: locality};

    if (this.props.hasInternet) {
      ddp.call("updateUserLocation", [location]).then(() => {
        console.log('Tabs: Successfully updated user location.');

        setTimeout(() => {
          if (this.state.isAutoLocate) {
            this.identifyCurrentLocation(false);
          }
        }, 900000);
      }).catch(() => {
        console.log('Tabs: Failed to update user location.');
        // this.updateMyLocationToServer(position, locality);
      });
    }
  }
}

module.exports = Messenger;
