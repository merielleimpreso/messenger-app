/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-05
 * Date Updated: N/A 
 * Description: Component used for viewing photo info
 */
'use strict';

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ToolbarAndroid,
  Image
} from 'react-native';

let GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
import {getDisplayName} from './Helpers';
import Users from './config/db/Users';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import moment from "moment";
import Toolbar from './common/Toolbar';
 
class PhotoInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sender: '',
    }
  }

  //Put a flag to know if component is mounted.
  componentWillMount() {
    this._isMounted = true;
    this.getImageSize();
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Compute the image size to be displayed.
  getImageSize() {
    let messagePhoto = this.props.imageInfo.image;
    Image.getSize(messagePhoto, (actualWidth, actualHeight) => {
      if (this._isMounted) {
        this.setState({
          actualHeight: actualHeight,
          actualWidth: actualWidth
        })
      }
    },
    (error) => {
      console.log('[PhotoInfo] getImageSize', error);
    });
  }

  render() {
    let color = this.props.color;
    let imageInfo = this.props.imageInfo;
    let dateTime = moment(imageInfo.CreateDate).format('LLL');

    let fileSizeInfo;
    if (imageInfo.fileSize) {
      let fileSize = imageInfo.fileSize;
      let size = Math.ceil(fileSize / 1000);
      fileSizeInfo = (
        <View style={{flex: 1, flexDirection: 'row', marginRight: 15, marginLeft: 15, marginTop: 10, marginBottom: 10, position: 'relative'}} >
          <Text>File Size</Text>
          <Text style={{position: 'absolute', right: 0}}>{size} kb</Text>
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <Toolbar title={'Photo Info'} withBackButton={true} {...this.props}/>
        <View elevation={3}
          style={{backgroundColor: color.CHAT_BG}}>
          <View style={{flex: 1, flexDirection: 'row', marginRight: 15, marginLeft: 15, marginTop: 10, marginBottom: 10, position: 'relative'}} >
            <Text>Sender</Text>
            <Text style={{position: 'absolute', right: 0}}>{imageInfo.user.name}</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', marginRight: 15, marginLeft: 15, marginTop: 10, marginBottom: 10, position: 'relative'}} >
            <Text>Date sent</Text>
            <Text style={{position: 'absolute', right: 0}}>{dateTime}</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', marginRight: 15, marginLeft: 15, marginTop: 10, marginBottom: 10, position: 'relative'}} >
            <Text>Resolution</Text>
            <Text style={{position: 'absolute', right: 0}}>{this.state.actualWidth} x {this.state.actualHeight}</Text>
          </View>
          {fileSizeInfo}
        </View>
      </View>
    );
  }
};

module.exports = PhotoInfo;