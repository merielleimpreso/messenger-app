'use strict';

import React, {
  Component,
} from 'react';
import ReactNative, {
  ActivityIndicator,
  Dimensions,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  View,
  Image,
  BackAndroid,
  Animated,
  Easing
} from 'react-native';
import {logTime} from './Helpers';
import Chat from './Chat';
import GroupChat from './config/db/GroupChat';
import _ from 'underscore';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
import TypingArea from './TypingArea';
import TypingAreaOptions from './TypingAreaOptions';
import TypingAreaContacts from './TypingAreaContacts';
import UserGroups from './config/db/UserGroups';
import Users from './config/db/Users';
import Modal from 'react-native-simple-modal';
const styles = StyleSheet.create(require('./../styles/styles.main'));
const GLOBAL = require('./config/Globals.js');
const ADD_TO_LIMIT = 10;
const NAVBAR_HEIGHT = 50;
const SCREEN_SIZE = Dimensions.get('window');
let isSafeToBack = true;
const createAnimation = function (value, duration, easing, delay = 0) {
  return Animated.timing(
    value,
    {
      toValue: 1,
      duration,
      easing,
      delay
    }
  )
}

class GroupMessages extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      connectionStatusHeight: 0,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isFirstLoad: true,
      isLoading: true,
      isViewVisible: true,
      limit: 10,
      messages: null,
      typeAreaHeight: 0,
      shouldReloadData: this.props.shouldReloadData(),
      activeOption: '',
      modalIsVisible: false,
      isEmojiPress: false,
      isOptionsPress: false,
      isRecordPress: false,
      loading: true,
      isLoaded: false
    }
    this.renderChat = this.renderChat.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.onPressedBack = this.onPressedBack.bind(this);
    this.onPressDetails = this.onPressDetails.bind(this);
    this.setActive = this.setActive.bind(this);
    // this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.isEmojiPress = this.isEmojiPress.bind(this);
    this.setIsEmojiPress = this.setIsEmojiPress.bind(this);
    this.isOptionsPress = this.isOptionsPress.bind(this);
    this.setIsOptionsPress = this.setIsOptionsPress.bind(this);
    this.isRecordPress = this.isRecordPress.bind(this);
    this.setIsRecordPress = this.setIsRecordPress.bind(this);
    this.animateTypingAreaOptions = new Animated.Value(0);
  }

  // componentWillMount() {
  //   BackAndroid.addEventListener('hardwareBackPress', this.backAndroidHandler);
  // }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  // Re-subscribe if limit or shouldReloadData values are updated
  componentDidUpdate(prevProps, prevState) {
    // if (this.state.limit != prevState.limit) {
    //   InteractionManager.runAfterInteractions(() => {
    //     this.subscribe();
    //   });
    // }
    //
    // if (this.props.shouldReloadData() != this.state.shouldReloadData) {
    //   if (this._isMounted) {
    //     this.setState({
    //       shouldReloadData: this.props.shouldReloadData()
    //     })
    //   }
    //   if (this.props.shouldReloadData()) {
    //     InteractionManager.runAfterInteractions(() => {
    //       console.log('GroupMessages ['+ logTime() +']: Reloading...');
    //       this.subscribe();
    //       if (this.state.messages) {
    //         if (this._isMounted) {
    //           this.setState({
    //             limit: this.state.messages.length + ADD_TO_LIMIT
    //           });
    //         }
    //       }
    //     })
    //   }
    // }
    //
    if (prevState.messages != this.state.messages) {
      if (this.state.isFirstLoad) {
        this.state.isFirstLoad = false;
        if (this.state.limit < this.state.messages.length) {
          this.setState({
            isFirstLoad: false,
            limit: this.state.messages.length
          });
        }
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    // BackAndroid.removeEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  // backAndroidHandler() {
  //   if (isSafeToBack) {
  //     this.props.navigator.pop();
  //     return true;
  //   }
  //   // return false;
  // }

  subscribe() {
    console.log('GroupChat ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    if (this.props.hasInternet()) {
      GroupChat.subscribeByGroup(this.props.group._id, this.state.limit).then(() => {
        requestAnimationFrame(() => {
          this.observe();
        });
      });
    } else {
      requestAnimationFrame(() => {
        this.observe();
      });
    }
  }

  observe() {
    GroupChat.observeByGroup(this.props.group._id, (messages) => {
      GroupChat.checkTempAndDeleteSentFiles(messages, (tempMsgs, newMessages) => {
        if(JSON.stringify(newMessages) != JSON.stringify(this.state.messages)) {
          if (this._isMounted) {
            console.log('GroupChat ['+ logTime() +']: Loaded ' + newMessages.length + ' groupchat item/s');
            this.updateSeen(newMessages);

            var imageGallery = []
            _.each(_.values(newMessages), function(r) {
              if (_.has(r.Message, "Media")) {
                imageGallery.push(r);
              }
            });

            this.setState({
              dataSource: this.state.dataSource.cloneWithRows(newMessages),
              isLoading: false,
              messages: newMessages,
              imageGallery: imageGallery
            });
          }
          if (tempMsgs.length > 0) {
            console.log('GroupChat ['+ logTime() +']: To be deleted ' + tempMsgs.length + ' item/s from temporary groupchat');
          }
        }
      });
    });
  }

  // Update seen property
  updateSeen(messages) {
    var msgsToBeUpDated =[];
    var loggedInUserId = this.props.loggedInUser()._id;
    for (var i = 0; i < messages.length; i++) {
      var chat = messages[i];

      var seenUsers = chat.Seen;
      var seen = false;
      var msgDetails = chat._id;
      _.each(_.values(seenUsers), function(s) {
        if ( loggedInUserId== s.userId) {
          seen = true;
        }
      });

      if(!seen && !chat.isTemporary)
      {
          msgsToBeUpDated.push(msgDetails);
      }

    }
    if(msgsToBeUpDated.length > 0)
    Send.updateSeenGroup(msgsToBeUpDated, this.props.loggedInUser()._id);
  }

  render() {

    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    if(this.state.activeOption=='keyboard')
    {
      this.state.typingAreaOptionsHeight = SCREEN_SIZE.height * 0.30;
    }
    else {
      this.state.typingAreaOptionsHeight = 0;
    }

    return (
      <View style={styles.msgContainer}>
        <KlikFonts.ToolbarAndroid
          elevation={5}
          navIconName="back"
          onIconClicked={() => this.props.navigator.pop()}
          title={this.props.group.name}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
          actions={[{title: 'Group details', show: 'never'}]}
          onActionSelected={this.onPressDetails} />
        <View onLayout={(event) => {this.setState({connectionStatusHeight: event.nativeEvent.layout.height});}}>
          {this.props.renderConnectionStatus()}
        </View>

        <Image style={{width: null, height: null, flex: 1, flexDirection: 'column'}}
          resizeMode={'cover'}
          source={this.renderBackgroundImage()}
          onLoadStart={(e) => this.setState({loading: true})}
          onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
          {(this.state.loading && !this.state.isLoaded) 
            ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:SCREEN_SIZE.height, width:SCREEN_SIZE.width}}>
                <ActivityIndicator size="small" color={color.BUTTON} />
              </View>
            : null
          }
          {this.renderChat()}
          {this.renderTypingArea()}
          {this.renderTypingAreaOptions()}
        </Image>

      </View>
    );
  }

  renderChat() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var chatHeight = SCREEN_SIZE.height - this.state.typeAreaHeight - NAVBAR_HEIGHT - (this.state.connectionStatusHeight) - 25;
    if (chatHeight < 165) {
      chatHeight = 165;
    }

    return (
      <Chat height={chatHeight}
        navigator={this.props.navigator}
        loggedInUser={this.props.loggedInUser}
        users={this.props.users}
        chatType={GLOBAL.CHAT.GROUP}
        color={color}
        dataSource={this.state.dataSource}
        isLoading={this.state.isLoading}
        isAllLoaded={this.state.isAllLoaded}
        loadMore={this.loadMore}
        hasInternet={this.props.hasInternet}
        theme={this.props.theme}
        imageGallery={this.state.imageGallery}
        backgroundImageSrc={this.props.backgroundImageSrc}
        setIsSafeToBack={this.props.setIsSafeToBack}
      />
    );
  }

  renderTypingArea() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View onLayout={(event) => {
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <TypingArea navigator={navigator}
          color={color}
          active={this.state.activeOption}
          isViewVisible={this.state.isViewVisible}
          loggedInUser={this.props.loggedInUser}
          recipientId={this.props.group._id}
          user={this.props.user}
          chatType={GLOBAL.CHAT.GROUP}
          setIsSafeToBack={this.props.setIsSafeToBack}
          setIsEmojiPress={this.setIsEmojiPress}
          isEmojiPress={this.isEmojiPress}
          setIsOptionsPress={this.setIsOptionsPress}
          isOptionsPress={this.isOptionsPress}
          isRecordPress={this.isRecordPress}
          setIsRecordPress={this.setIsRecordPress}
        //  setTypingAreaCallBack={this.props.setTypingAreaCallBack}
        //  isSafeToBack={this.props.isSafeToBack}
        //  setActive={this.setActive}
        />
      </View>
    )
  }

  renderTypingAreaOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let chatHeight = SCREEN_SIZE.height - NAVBAR_HEIGHT - (this.state.connectionStatusHeight) - 25;

    if (this.state.isClickedSetIsOptionsPress || this.state.isClickedSetIsEmojiPress) {
      let outputRange = (this.state.isOptionsPress || this.state.isEmojiPress) ? [-chatHeight, 0] : [0, -chatHeight];
     
      let top = this.animateTypingAreaOptions.interpolate({
                    inputRange: [0, 1],
                    outputRange: outputRange
                  })

      return (
        <Animated.View style={{top: top, position: 'absolute', alignSelf: 'flex-start'}}>
          <TypingAreaOptions color={color}
            height={chatHeight}
            navigator={this.props.navigator}
            loggedInUser={this.props.loggedInUser}
            recipientId={this.props.group._id}
            chatType={GLOBAL.CHAT.GROUP}
            setIsSafeToBack={this.props.setIsSafeToBack}
            setIsRecordPress={this.setIsRecordPress}
            setIsEmojiPress={this.setIsEmojiPress}
            setIsOptionsPress={this.setIsOptionsPress}
            isEmojiPress={this.isEmojiPress}
            isOptionsPress={this.isOptionsPress}
            // active={this.state.activeOption}
            // setTypingAreaCallBack={this.props.setTypingAreaCallBack}
            // setActive={this.setActive}
            theme={this.props.theme}
            hasInternet={this.props.hasInternet}
          />
        </Animated.View>
      );
    }
  }

  // setIsSafeToBack(isSafe) {
  //   isSafeToBack = isSafe;
  //   console.log('isSafeToBack', isSafeToBack);
  // }

  animateOptions() {
    this.animateTypingAreaOptions.setValue(0);
    Animated.parallel([createAnimation(this.animateTypingAreaOptions, 500, Easing.ease)]).start();
  }

  setIsEmojiPress() {
    if (this._isMounted) {
      this.setState({
        isEmojiPress: !this.state.isEmojiPress,
        isClickedSetIsEmojiPress: true
      });
      this.animateOptions();
      if (this.state.isOptionsPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        this.setState({isOptionsPress: !this.state.isOptionsPress});
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  isEmojiPress() {
    return this.state.isEmojiPress;
  }

  setIsOptionsPress() {
    if (this._isMounted) {
      this.setState({
        isOptionsPress: !this.state.isOptionsPress,
        isClickedSetIsOptionsPress: true
      });
      this.animateOptions();
      if (this.state.isEmojiPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        this.setState({isEmojiPress: !this.state.isEmojiPress});
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  isOptionsPress() {
    return this.state.isOptionsPress;
  }

  setIsRecordPress(clickedFrom) {
    if (this._isMounted) {
      this.setState({isRecordPress: !this.state.isRecordPress});
      if (clickedFrom == 'typingAreaOptions') {
        this.setState({isOptionsPress: !this.state.isOptionsPress});
      }
    }
  }

  isRecordPress() {
    return this.state.isRecordPress;
  }

  activeOption()
  {
    return this.state.activeOption;
  }

  setActive(active)
  {
    if(active == 'contact')
    {
      this.state.modalIsVisible = true;
    }
    else if(this.state.modalIsVisible)
    {
      this.state.modalIsVisible = false;
    }
    this.setState({activeOption: active});
  }

  // Check if there are recent messages need to be loaded.
  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('GroupMessages ['+ logTime() +']: Checking if there are more messages to load...');
          ddp.call('shouldLoadGroupChat', [this.props.group._id, this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('GroupMessages ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              console.log(error);
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  // Go back
  onPressedBack() {
    if (this._isMounted) {
      this.setState({
        isViewVisible: false
      });
    }
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPressDetails() {
    requestAnimationFrame(() => {

      var users = [];
      console.log("groupMessages users: ");
      console.log(this.props.users);
      console.log("Props: ");
      console.log(this.props);
      //When creating new  group, requesting the members of the new group
      //returns [], so the props.users that get passed is empty.
      //Groups -> GroupMessages are okay doe.
      if(this.props.users.length <= 0 || this.props.users ==null)
      {
        console.log("Entered checker for: "+this.props.group._id);
        //users = UserGroups.getItemByGroupId(this.props.group._id);

        var tempUser;
        var user;
        for(var userIndex = 0; userIndex < this.props.group.userIds.length; userIndex++)
        {
          tempUser = Users.getItemById(this.props.group.userIds[userIndex]);

          user ={
              UserFID: tempUser._id
          }
          users.push(user);
        }
      }
      else {
        users = this.props.users;
      }


      this.props.navigator.push({
        name: 'GroupDetails',
        groupUsers: users,
        theme: this.props.theme,
        group: this.props.group,
        loggedInUser: this.props.loggedInUser,
        shouldReloadData: this.props.shouldReloadData,
        renderConnectionStatus: this.props.renderConnectionStatus,
        hasInternet: this.props.hasInternet,
        refreshGroups: this.props.refreshGroups,
      });
    });
  }

  renderBackgroundImage() {
    var bgRetriever = this.props.backgroundImageSrc;
    var bgImage = bgRetriever();
      if(bgImage==null || bgImage == '')
      {
        if (this.props.theme() == "ORIGINAL") {
          return require('./../images/chatbgdefault.jpg');
        } else if (this.props.theme() == "BLUE") {
          return require('./../images/backgroundImage/chatbg1.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "LAVENDER") {
          return require('./../images/backgroundImage/themebg2b.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "ORIGINAL") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else {
          return null;
        }
      } else {

        return {uri: bgImage};
      }
  }

}




// Export module.
module.exports = GroupMessages;



// /*
//
// Author: Merielle Impreso
// Date Created: 2016-04-19
// Date Updated: 2016-05-19
// Description: List all the groupchat items of the logged in user with a specific user.
//              Displayed when a row from Groups is clicked.
// Props passed from Groups in ChatRecentRenderRow:
//   data = recent groupchat item of a specific user
//   data.user = logged in user
//   data.groupId = the id of the group
//   data.users = users included in the group
//
// Changelog:
// update: 2016-04-21 (by Merielle I.)
//   - used latest ddp-client
//   - added GroupChat for database subscription and observe
//   - included Chat component that handles the display of chat items
// update: 2016-04-22 (by Merielle I.)
//   - added isMounted flag to fix setState warnings, put it inside the method callback
// update: 2016-04-27 (by John Rezan B.)
//   - Fix KeyboardSpacer added resizing function of chat when keyboard displays.
// update: 2016-05-05 (by Merielle I.)
//   - Implement UI and color scheme
//   - Replace <Back with Icon
// update: 2016-05-19 (by Merielle I.)
//   - Added NetInfo, check for internet connection
// update: 2016-06-07 (by Merielle I.)
//   - Added seen property
// update: 2016-06-09 (by Merielle I.)
//   - Added theme color
// update: 2016-06-13(by John Rezan B.)
//   - Changed GLOBAL.CHAT.DIRECT to GLOBAL.CHAT.GROUP
//   - Added targetId
// update: 2016-06-21 (by Merielle I.)
//   - Change Icon to KlikFonts
// update: 2016-06-29 (by Merielle I.)
//   - Stopping of observer in componentWillUnmount
//   - Added display of date
// update: 2016-07-05 (by Merielle I.)
//   - Added update limit value for loading other messages
//   - Improve subscribe and observe code
//
// */
//
//
// 'use strict';
//
// import _ from 'underscore';
// import Chat from './Chat';
// import {consoleLog} from './Helpers';
// import GroupChat from './config/db/GroupChat';
// import KlikFonts from 'react-native-vector-icons/KlikFonts';
// import NavigationBar from 'react-native-navbar';
// import ScreenFailedToConnect from './ScreenFailedToConnect';
// import TypingArea from './TypingArea';
//
// import React, {
//   Component,
// } from 'react';
//
// import {
//   DeviceEventEmitter,
//   Dimensions,
//   InteractionManager,
//   Platform,
//   LayoutAnimation,
//   NetInfo,
//   StyleSheet,
//   View
// } from 'react-native';
// var styles = StyleSheet.create(require('./../styles/styles_common.js'));
// var windowSize = Dimensions.get('window');
// var GLOBAL = require('./config/Globals.js');
//
// const animations = {
//   layout: {
//     spring: {
//       duration: 500,
//       create: {
//         duration: 300,
//         type: LayoutAnimation.Types.easeInEaseOut,
//         property: LayoutAnimation.Properties.opacity
//       },
//       update: {
//         type: LayoutAnimation.Types.spring,
//         springDamping: 200
//       }
//     },
//     easeInEaseOut: {
//       duration: 300,
//       create: {
//         type: LayoutAnimation.Types.easeInEaseOut,
//         property: LayoutAnimation.Properties.scaleXY
//       },
//       update: {
//         delay: 100,
//         type: LayoutAnimation.Types.easeInEaseOut
//       }
//     }
//   }
// };
// const navbarHeight = 64;
// const ADD_TO_LIMIT = 10;
//
// var GroupMessages = React.createClass({
//
//   // Initialize state.
//   getInitialState: function(){
//     return {
//       conversationName: '',
//       isConnected: true,
//       isAllMessagesLoaded: false,
//       isLoadingMessages: true,
//       messages: '',
//       observer: {},
//       groupId: '',
//       typeAreaHeight: 86,
//       userId: '',
//       limit: 10
//     }
//   },
//
//   /*
//   Put a flag to check if components were mounted.
//   Call observeByGroup method.
//   Call addConnectivityListener method.
//   */
//   componentDidMount() {
//     this.isMounted = true;
//     this.addConnectivityListener();
//     this.initializeGroupDetails();
//
//     InteractionManager.runAfterInteractions(() => {
//       this.observeByGroup();
//     });
//   },
//
//   /*
//   Stop observer when component is unmounted.
//   Put flag when component will unmount.
//   Remove connectivity listener when component is unmounted.
//   */
//   componentWillUnmount() {
//     this.state.observer.stop;
//     this.isMounted = false;
//
//     NetInfo.isConnected.removeEventListener(
//      'change',
//      this.handleConnectivityChange
//     );
//   },
//
//   // Update typeAreaHeight when component update
//   componentWillUpdate(props, state) {
//       if (state.typeAreaHeight !== this.state.typeAreaHeight)
//           LayoutAnimation.configureNext(animations.layout.spring);
//   },
//
//   // Add listener to check for connectivity
//   addConnectivityListener() {
//     NetInfo.isConnected.addEventListener(
//       'change',
//       this.handleConnectivityChange
//     );
//     NetInfo.isConnected.fetch().done(
//       (isConnected) => {
//         if (this.isMounted) {
//           this.setState({
//             isConnected,
//           });
//         }
//       }
//     );
//   },
//
//   // Handle connectivity change
//   handleConnectivityChange(isConnected) {
//     if (this.isMounted) {
//       this.setState({
//         isConnected,
//       });
//     }
//   },
//
//   initializeGroupDetails() {
//     var props = this.props.config;
//     var conversationName = props.groupName;
//     if (this.isMounted) {
//       this.setState({
//         conversationName: conversationName
//       })
//     }
//   },
//
//   /*
//   Get the props value to save userId and groupId.
//   Observe the groupchat collection and get all chat items for this group.
//   Update the date the user seen the message.
//   */
//   observeByGroup() {
//     var data = this.props.config;
//     var options = {
//       weekday: 'long',
//       year: 'numeric',
//       month: 'long',
//       day: '2-digit'
//     };
//     var groupId = data.groupId;
//     var limit = this.state.limit;
//
//     GroupChat.subscribeByGroup(groupId, limit)
//     .then(() => {
//       var observer = GroupChat.observeByGroup(groupId, (result) => {
//         if (this.isMounted) {
//           var createDates = [];
//           _.each(_.values(result), function(r) {
//             var seenUsers = r.Seen;
//             var seen = false;
//             _.each(_.values(seenUsers), function(s) {
//               if (data.user._id == s.userId) {
//                 seen = true;
//               }
//             });
//             if(!seen) {
//               if (!r.isTemporary) {
//                 ddp.call('updateSeenGroup', [r._id, data.user._id])
//                 .then(res => {
//                   consoleLog('GroupMessages: SEEN: GROUP MESSAGE ID #' + r._id);
//                 })
//                 .catch(err => {
//                   console.log('nag err');
//                   console.log('GroupMessages: ' + err.message);
//                 });
//               }
//             }
//             var date = new Date(r.CreateDate);
//             var formattedDate = date.toLocaleDateString([], options);
//             r['FormattedDate'] = formattedDate;
//             createDates.push(formattedDate);
//           });
//           createDates = _.uniq(createDates);
//
//           var messages = [];
//           _.each(_.values(createDates), function(date) {
//             _.each(_.values(result), function(r) {
//               if (r.FormattedDate == date) {
//                 messages.push(r);
//               }
//             });
//             messages.push({date: date});
//           });
//           if (this.isMounted) {
//             this.setState({
//               isLoadingMessages: false,
//               messages: messages
//             });
//           }
//         }
//       });
//
//       if (this.isMounted) {
//         this.setState({
//           groupId: data.groupId,
//           userId: data.user._id,
//           observer: observer
//         });
//       }
//       this.isAllMessagesLoaded(groupId, limit);
//     })
//     .catch((error) => {
//       if (error.reason == 'All are loaded') {
//         if (this.isMounted) {
//           this.setState({isAllMessagesLoaded: true});
//         }
//       } else {
//         consoleLog('GroupMessages: DDP disconnected');
//         if (this.isMounted) {
//           this.setState({canCallMethod: false})
//         }
//       }
//     });
//   },
//
//   isAllMessagesLoaded(groupId, limit) {
//     ddp.call('shouldLoadGroupChat', [groupId, limit])
//     .then(shouldLoadGroupChat => {
//       if (this.isMounted) {
//         this.setState({isAllMessagesLoaded: !shouldLoadGroupChat});
//       }
//     })
//     .catch(error => {
//       if (error.reason == 'All are loaded') {
//         if (this.isMounted) {
//           this.setState({isAllMessagesLoaded: true});
//         }
//       }
//     })
//   },
//
//   // Load more messages by adding limit value
//   loadMoreMessages() {
//     if (!this.state.isLoadingMessages) {
//       var limit = this.state.limit + ADD_TO_LIMIT;
//       if (this.isMounted) {
//         this.setState({
//           limit: limit,
//           isLoadingMessages: true
//         });
//       }
//       consoleLog('GroupMessages: loaded ' + limit + ' posts');
//       this.observeByGroup();
//     }
//   },
//
//   /*
//   Display the chat view.
//   Pass the users and messages as props to Chat.
//   */
//   renderChat() {
//     var color = this.props.color;
//     var chatHeight = windowSize.height - this.state.typeAreaHeight - navbarHeight;
//     if (chatHeight < 165) {
//       chatHeight = 165;
//     }
//
//     return (
//       <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
//         <NavigationBar
//           tintColor={color.THEME}
//           title={{ title: this.state.conversationName ? this.state.conversationName : 'Group', tintColor: color.BUTTON_TEXT}}
//           leftButton={
//             <KlikFonts
//             name="back"
//             color={color.BUTTON_TEXT}
//             size={35}
//             onPress={() => this.props.navigator.pop()}
//            />
//           }
//         />
//         <Chat
//           height={chatHeight}
//           users={this.props.config.users}
//           targetId={this.props.config.groupId}
//           messages={this.state.messages}
//           navigator={this.props.navigator}
//           chatType={GLOBAL.CHAT.GROUP}
//           color={this.props.color}
//           loadMoreMessages={this.loadMoreMessages}
//           isAllMessagesLoaded={this.state.isAllMessagesLoaded}
//           isLoadingMessages={this.state.isLoadingMessages}
//         />
//         <View onLayout={(event) => {this.setState({typeAreaHeight: event.nativeEvent.layout.height});}}>
//           <TypingArea
//            from={this.state.userId}
//            to={this.state.groupId}
//            chatType={GLOBAL.CHAT.GROUP}
//            onSendCallback={()=>{}}
//            color={color}
//            navigator={this.props.navigator} />
//         </View>
//       </View>
//     );
//   },
//
//   // Display the view.
//   render() {
//    var render = (this.state.isConnected)
//     ? this.renderChat()
//     : render = <ScreenFailedToConnect text={GLOBAL.TIMEOUT_MESSAGE_DISCONNECTED} />;
//    return render;
//   },
//
// });
//
// // Export module
// module.exports = GroupMessages;
