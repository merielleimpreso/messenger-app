/**
Author: Ida Jimenez
Date Created: 2016-04-15
Date Updated: 2016-05-19
Description: This file displays the groupchat items from the database.
  Each row display is handled by ChatRecentRenderRow.
  It passes the following to ChatRecentRenderRow as props:
    data = recent chat item
    data.chatType = GLOBAL.CHAT.GLOBAL;
    data.user = logged in user
Used In: Messenger.android.js

Changelog:
update: 2016-04-21 (by Ida J.)
  - used latest ddp-client
  - added GroupChat for database subscription and observe
update: 2016-04-25 (by Ida J.)
  - added comments
update: 2016-04-26 (by Terence)
  -added componentWillUnmount to reset this.isMounted
update: 2016-05-04 (by Ida)
  - Modified UI color scheme

update: 2016-05-04 (by Terence)
  -Changed FailedToConnect text from "Failed to Connect" to "Failed to retrieve your messages :("
update: 2016-05-06 (by Ida)
  - Modified UI color scheme
update: 2016-05-12 (by Terence)
  -Changed timeout time to 30 seconds
  -Fixed setState being called even if user has logged out already.
update: 2016-05-16 (By Terence)
    -Changed AddNewMessage button to only be visible if all threads have been loaded
    -Changed NoMEssages text to be same color as Loading message
update: 2016-05-19 (by Ida)
  - added keyboardShouldPersistTaps={true} in <ListView />
update: 2016-06-16 (Ida)
  - Changed color scheme similar to whatsapp
update: 2016-06-27 (ida)
  - changed icons to klikfonts
update: 2016-08-11 by Terence
-fixed empty headers warning
update: 2016-07-22 (ida)
 - Fixed red screen when changing color themes #695
 */
'use strict';

import React, { Component } from 'react';

import {
  AsyncStorage,
  ListView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  RecyclerViewBackedScrollView,
  InteractionManager
} from 'react-native';

import _ from 'underscore';
import {dateToTime, getMessage, getMessageBadge, logTime, renderFooter, toNameCase} from './Helpers';
import Group from './config/db/Group';
import GroupChat from './config/db/GroupChat';
import Users from './config/db/Users';
import UserGroups from './config/db/UserGroups';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ActionButton from 'react-native-action-button';
import ScreenNoItemFound from './ScreenNoItemFound';
var ProgressBar = require('ActivityIndicator');
var styles = StyleSheet.create(require('./../styles/styles.main.js'));
var GLOBAL = require('./../modules/config/Globals.js');

const ADD_TO_LIMIT = 10;
var firstTimeLaunch = true;
var isLoadingRows = false;
class Groups extends Component {

  // Get initial state
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      hasNoData: true,
      user: {},
      isLoading: true,
      isFirstLoad: true,
      noConnection: false,
      loadedRowsCount: 0,
      observer: {},
      isAllLoaded: false,
      groupName: '',
      limit: 10,
      messages: null,
      shouldReloadData: this.props.shouldReloadData(),
      theme: this.props.theme()
    }
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.addGroupChat = this.addGroupChat.bind(this);
    this.subscribeToGroup = this.subscribeToGroup.bind(this);
    this.refreshMessages = this.refreshMessages.bind(this);
  }

  // Make subscription on componentDidMount
  componentDidMount() {
    this._isMounted = true;
    // this.props.setRefreshGroupMessages(this.refreshMessages)
    InteractionManager.runAfterInteractions(()=>{
      this.subscribeToGroup();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isRefreshing) {
      this.subscribeToGroup();
    }

    if (this.state.limit != prevState.limit) {
      InteractionManager.runAfterInteractions(() => {
        this.subscribeToGroup();
      });
    }

    if (this.props.theme() != this.state.theme) {
      this.state.dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,});
      let data = this.getDataToBeDisplayed(this.state.messages);
      if (this._isMounted) {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(data),
          theme: this.props.theme()
        });
      }
    }

    if (this.props.shouldReloadData() != this.state.shouldReloadData) {
      if (this._isMounted) {
        this.setState({
          shouldReloadData: this.props.shouldReloadData()
        })
      }
      if (this.props.shouldReloadData()) {
        console.log('Groups ['+ logTime() +']: Reloading...');
        InteractionManager.runAfterInteractions(() => {
          this.subscribeToGroup();

          if (this.state.messages) {
            if (this._isMounted) {
              this.setState({
                limit: this.state.messages.length + ADD_TO_LIMIT
              });
            }
          }
        });
      }
    }

    if (prevState.messages != this.state.messages) {
      if (this.state.isFirstLoad) {
        this.state.isFirstLoad = false;
        if (this.state.messages != null && this.state.limit < this.state.messages.length) {
          this.setState({
            isFirstLoad: false,
            limit: this.state.messages.length
          });
        }
      }
    }
  }

  subscribeToGroup() {

    if (this.props.hasInternet()) {
      Group.subscribe().then(() => {
        Group.getAllItems();
        this.subscribeToUserGroups();
      });
    } else {
      Group.useCache(() => {
        this.subscribeToUserGroups();
      });
    }
  }

  subscribeToUserGroups() {
    if (this.props.hasInternet()) {
      UserGroups.subscribe().then(() => {
        UserGroups.getAllItems();
        this.subscribeToGroupChat();
      });
    } else {
      UserGroups.useCache(() => {
        this.subscribeToGroupChat();
      })
    }
  }

  refreshMessages()
  {
    this.setState({
      dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      hasNoData: true,
      user: {},
      isLoading: true,
      isFirstLoad: true,
      noConnection: false,
      loadedRowsCount: 0,
      observer: {},
      isAllLoaded: false,
      groupName: '',
      limit: 10,
      messages: null,
      shouldReloadData: this.props.shouldReloadData(),
      theme: this.props.theme()
  });

    this.subscribeToGroup();
  }
  
  subscribeToGroupChat() {
    console.log('Groups ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    if (this.props.hasInternet()) {

      GroupChat.subscribe().then(() => {
        this.observe();
      });
    } else {
      GroupChat.useCache(() => {
        this.observe();
      });
    }
  }

  observe() {
    GroupChat.observeRecent((recentChat) => {
      if (JSON.stringify(recentChat) != JSON.stringify(this.state.messages)) {
        let data = this.getDataToBeDisplayed(recentChat);
        if (this._isMounted) {
          console.log('Groups ['+ logTime() +']: Loaded ' + data.length + ' groupchat items');



          this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(data),
            messages: recentChat
          })
        }
        if (this.props.isRefreshing) {
          this.props.setIsRefreshing();
        }
      } else {
        if (this.props.isRefreshing) {
          this.props.setIsRefreshing();
        }
      }
    });
  }

  refreshGroups() {
    console.warn("Refreshed Groups");
    this.makeSubscription();
  }

  getDataToBeDisplayed(recentChat) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.props.loggedInUser();
    let recentChatInfo = [];

    for (var i = 0; i < recentChat.length; i++) {
      let group = Group.getItemById(recentChat[i].GroupFID);

      if (group) {
        let r = recentChat[i];
        let user = Users.getItemById(r.FromUserFID);

        let name = group.Name;
        let image = GLOBAL.DEFAULT_IMAGE;
        let createDate = r.CreateDate;
        let nameSender = (loggedInUser._id == r.FromUserFID) ? 'You' : user.profile.firstName;
        let message = getMessage(nameSender, r.Message);
        let loggedInUserIsSender = (loggedInUser._id == r.FromUserFID);
        let isSeen = this.isSeen(loggedInUser._id, r.Seen);
        let badge = getMessageBadge(loggedInUserIsSender, isSeen, color);
        let messageId = r._id;
        //let unseen = r.unseen;
        let recent = {
          group: group,
          name: name,
          image: image,
          createDate: createDate,
          message: message,
          isSeen: isSeen,
          loggedInUserIsSender: loggedInUserIsSender,
          badge: badge,
          messageId: messageId,
          //unseen: unseen
        }
        recentChatInfo.push(recent);
      }
    }
    return recentChatInfo;
  }

  isSeen(loggedInUserId, seenUsers) {
    var seen = false;
    _.each(_.values(seenUsers), function(s) {
      if (loggedInUserId == s.userId) {
        seen = true;
      }
    });
    return seen;
  }

  // Pass the chat array for rendering to GroupsRender
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];;
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        {this.props.renderConnectionStatus()}
        {this.renderContent()}
        {this.renderNewGroupButton()}
      </View>
    );
  }

  // Render the main content
  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (!this.state.isLoading && this.state.dataSource.getRowCount() == 0) {
      return <ScreenNoItemFound isLoading={false} text={'No messages'} color={color}/>;
    } else {
      return (
        <ListView ref='listview'
          removeClippedSubviews={true}
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          renderFooter={this.renderFooter}
          onEndReached={this.loadMore}
          initialListSize={10}
        />
      );
    }
  }

  renderNewGroupButton() {
    var loadingView = <View />;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    // if (this.state.isAllLoaded || this.state.dataSource.getRowCount() == 0) {
      loadingView = <ActionButton
                      buttonColor={color.BUTTON}
                      spacing={10}
                      outRangeScale={.75}
                      offsetX={10}
                      offsetY={10}>
                      <ActionButton.Item  buttonColor={color.BUTTON}
                        title="New Message"
                        onPress={() => this.addGroupChat()}>
                        <KlikFonts name="writenew" style={[styles.actionButtonIcon, {color: color.BUTTON_TEXT}]} />
                      </ActionButton.Item>
                    </ActionButton>
      return loadingView;
  }

  // Display data in the row
  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var textStyle = (!data.loggedInUserIsSender && !data.isSeen) ? styles.chatRecentTextNotSeen : styles.chatRecentText;

    return (
      <TouchableHighlight key={data.messageId} onPress={() => this.onPressedRow(data)} underlayColor={color.HIGHLIGHT}>
        <View>
          <View style={styles.chatRecentRow}>
            <Image source={{uri:data.image}} style={styles.chatRecentImage} />
            <View style={styles.chatRecentDetailsContainer}>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[styles.chatRecentTextBold, {color:color.TEXT_DARK, flex:4}]}>{data.name}</Text>
                <View style={{flexDirection:'column', alignItems:'flex-end'}}>
                  <Text numberOfLines={1} style={[styles.chatRecentText, {color:color.THEME}]}>{dateToTime(data.createDate)}</Text>
                </View>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[textStyle, {color:color.TEXT_LIGHT,flex:4}]}>{data.message}</Text>
                <View style={{flexDirection:'column', alignItems:'flex-end',flex:1}}>{data.badge}</View>
              </View>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  // Render footer display
  renderFooter() {
    let isAllLoaded = this.state.isAllLoaded;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let hasInternet = this.props.hasInternet()
    return renderFooter(isAllLoaded, color, hasInternet);
  }

  // Check if there are recent messages need to be loaded.
  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('Groups ['+ logTime() +']: Checking if there are more messages to load...');
          ddp.call('shouldLoadGroupChatRecent', [this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('Groups ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  // When row was pressed
  onPressedRow(data) {
    console.log('Groups ['+ logTime() +']: Row is pressed, open messages with ' + data.name);
    var group = {
      _id: data.group._id,
      name: data.name
    };
    console.log("Group: ");
    console.log(data);
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name: 'GroupMessages',
        renderConnectionStatus: this.props.renderConnectionStatus,
        group: group,
        users: UserGroups.getItemByGroupId(data.group._id),
        loggedInUser: this.props.loggedInUser,
        hasInternet: this.props.hasInternet,
        shouldReloadData: this.props.shouldReloadData,
        theme: this.props.theme,
        refreshGroups: this.refreshMessages,
      });
    });
  }

  // Function for adding new groups chat
  addGroupChat(){
    var data = {};

    data['chatType'] = GLOBAL.CHAT.GROUP;
    data['user'] = this.state.user;
    this.props.navigator.push({
      name: 'AddGroupChat',
      data: data,
      loggedInUser: this.props.loggedInUser,
      theme: this.props.theme,
      hasInternet: this.props.hasInternet,
      shouldReloadData: this.props.shouldReloadData,
      renderConnectionStatus: this.props.renderConnectionStatus,
      refreshGroups: this.subscribeToGroup,
    });
  }

  refresh()
  {
    this.setState({isLoading: true, noConnection: false});
    //this.makeSubscription();
  }

  setIsLoadedRowsCount() {
    if(this.isMounted)
    {
      if (this.state.loadedRowsCount < this.state.chat.length) {
        this.state.loadedRowsCount++;
      }

      if (this.state.loadedRowsCount == this.state.chat.length) {
        this.setState({loadedRowsCount: this.state.loadedRowsCount});
      }
    }

  }

  // ListView datasource.
  getDataSource(): ListView.DataSource {
    var user = this.state.user;
    var chat = this.state.chat;
    var index = 0;
    _.each(_.values(chat), function(c) {
      c['chatType'] = GLOBAL.CHAT.GROUP;
      c['user'] = user;
      c['index'] = index + 1;
      index = index + 1;
    });
    return this.state.dataSource.cloneWithRows(chat);
  }
};

// Display this class if there's no item in the dataSource
var NoMessages = React.createClass({
  render: function() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var text = 'No messages found';

    return (<Text style={[styles.loaderMessage, {alignSelf: 'center', color: color.TEXT_LIGHT}]}>{text}</Text>);
  }
});

var LoadBar = React.createClass({
  render: function() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var text = 'Retrieving your messages...';
    var progressBar =
      <View>
        <View style={{justifyContent: 'center'}}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
        </View>
        <ProgressBar size={'large'} color={color.BUTTON} />
      </View>;
    return (
      <View style={styles.loaderContainer}>

        {progressBar}
      </View>
    );
  }
});

var FailedToConnect = React.createClass({
  render: function() {
    let color = this.props.color;
    var text = 'Server is taking quite a while to respond...'
    var content =
      <View>
        <View style={{justifyContent: 'center'}}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
        </View>
        <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
        <TouchableHighlight onPress={() => this.props.refresh()}
          underlayColor={color.HIGHLIGHT}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>Reload</Text>
        </TouchableHighlight>
      </View>;
      return (
        <View style={styles.loaderContainer}>

          {content}
        </View>
      );
  }
});

module.exports = Groups;
