/*

Author: John Rezan Leslie Baguna
Date Created: 2016-04
Date Updated: 2016-05-19
Description: Creating new group.
Used in: Groups.ios when add icon is clicked

Changelog:
update: 2016-05-20 (by Merielle I.)
  - Added comments and clean code
  - Added NetInfo, check for internet connection
update: 2016-06-21 (by Merielle I.)
  - Added color customize UI
  - Added KlikFonts
update: 2016-06-22 (by Merielle I.)
  - Check if added at least one contact before creating group
update: 2016-07-21 (by Merielle I.)
  - Separate react from react-native

*/


'use strict';

import _ from 'underscore';
import BEMCheckBox from 'react-native-bem-check-box';
import GroupChat from './config/db/GroupChat';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import ScreenFailedToConnect from './ScreenFailedToConnect';
import React, {
  Component,
} from 'react';
import ReactNative, {
  Alert,
  Dimensions,
  Image,
  ListView,
  Navigator,
  NetInfo,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
var ImagePickerManager = require('NativeModules').ImagePickerManager;
var styles = StyleSheet.create(require('./../styles/styles_common.js'));
var theme = 'BLUE';
var windowSize = Dimensions.get('window');
var GLOBAL = require('./config/Globals.js');
var COLOR = GLOBAL.COLOR_THEME[theme];

class GroupNew extends Component {

  //Get initial state and bind functions.
  constructor(props) {
    super(props);
    var dataSourceContact = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
    })
    this.state ={
      contacts: '',
      chosenContacts:[ddp.user],
      filteredContacts: '',
      isConnected: true,
      checked: false,
      dataSourceContact: dataSourceContact,
    }
    this.addConnectivityListener = this.addConnectivityListener.bind(this);
    this.handleConnectivityChange = this.handleConnectivityChange.bind(this);
    this.renderContact = this.renderContact.bind(this);
    this.renderCreateGroup = this.renderCreateGroup.bind(this);
  }

  // Call getAllContacts function when component will mount.
  componentWillMount() {
    this._isMounted = true;
    this.getAllContacts();
  }

  /*
  Flag to check if component is mounted.
  Call addConnectivityListener method.
  */
  componentDidMount() {
    this._isMounted = true;
    this.addConnectivityListener();
  }

  /*
  Put flag when component will unmount.
  Remove connectivity listener when component is unmounted.
  */
  componentWillUnmount() {
    this._isMounted = false;

    NetInfo.isConnected.removeEventListener(
      'change',
      this.handleConnectivityChange
    );
  }

  // Add listener to check for connectivity
  addConnectivityListener() {
    NetInfo.isConnected.addEventListener(
      'change',
      this.handleConnectivityChange
    );
    NetInfo.isConnected.fetch().done(
      (isConnected) => {
        if (this._isMounted) {
          this.setState({
            isConnected,
          });
        }
      }
    );
  }

  // Handle connectivity change
  handleConnectivityChange(isConnected) {
    if (this._isMounted) {
      this.setState({
        isConnected,
      });
    }
  }

  /*
  Get all contacts of the current user
  by setting the result to the component's state
  */
  getAllContacts(){
    ddp.call('getContacts').then(result => {
      if (result) {
        if (this._isMounted) {
          this.setState({
            contacts: result,
            filteredContacts: result,
            dataSourceContact: this.state.dataSourceContact.cloneWithRows(result)
          });
        }
      }
    });
  }

  // Function in choosing a contact
  chooseContact(contact, value){
    if (this._isMounted) {
      if (value) {
        var array = this.state.chosenContacts;
        // array.push(contact._id);
        array.push(contact);
        this.setState({
          chosenContacts: array,
        });
      }
      else {
        var array = this.state.chosenContacts;
        var contact = _.where(array, {_id: contact._id});
        var  array = _.without(array, contact[0]);
        this.setState({
          chosenContacts: array,
        });
      }
    }
  }

  // Create a group method
  createGroup(){
    var groupName = this.state.groupName;

    if (this.state.chosenContacts.length > 1) {
      var chosenContactsIds = _.pluck(this.state.chosenContacts, '_id');
      ddp.call('createGroup', [groupName, chosenContactsIds])
      .then(result => {
        let config = {
          user: ddp.user,
          groupId: result,
          users: this.state.chosenContacts,
          groupName: groupName
        }
        GroupChat.subscribe()
        .then(() => {
          GroupChat.observeByGroup(result, (result) => {
            console.log(result);
          })
        });
        console.log(config);
        this.props.navigator.push({
          id: 'groupmessages',
          config: config,
          color: this.props.color
        });
      });
    } else {
      Alert.alert(
        GLOBAL.ALERT_CREATE_GROUP_ERROR_TITLE,
        GLOBAL.ALERT_CREATE_GROUP_ERROR_DETAIL,
        [{text: 'OK'}]
      )
    }
  }

  /*
  Use in filtering contacts
  Params: text from TextInput
  Used by recipientInput onChangeText
  */
  filterContacts(text){
    if(this._isMounted){
      var results = _.filter(this.state.contacts, function(contact){
        return (contact.profile.firstName + " " + contact.profile.lastName).toLowerCase().indexOf(text.toLowerCase()) >= 0;
      });
      this.setState({
        dataSourceContact: this.state.dataSourceContact.cloneWithRows(results),
        chosenContact: false
      });
    }
  }

  //Render the contacts for the listview
  renderContact(contact){
    var color = this.props.color;
    var result = _.where(this.state.chosenContacts, {_id: contact._id});
    var checked = result[0] ? true : false;
    return (
     <View style={styles.groupNewContactContainer}>
       <BEMCheckBox style={{height:20,width:20, borderRadius:0}}
         animationDuration={0.3}
         onCheckColor={color.BUTTON_TEXT}
         value={checked}
         onFillColor={color.BUTTON}
         onAnimationType='fill'
         offAnimationType='flat'
         lineWidth={1}
         onValueChange={value => this.chooseContact(contact,value)}
         boxType="square"
         tintColor={color.BUTTON}
         onTintColor={color.BUTTON}
        />
       <View style={{flexDirection:'row', alignItems:'center'}}>
          <Image source={{uri: contact.profile.photoUrl}} style={styles.groupNewContactImage}></Image>
          <Text numberOfLines={1} style={[styles.groupNewContactName, {color:color.THEME}]} > {contact.profile.firstName} {contact.profile.lastName}</Text>
       </View>
    </View>
   )
  }

  // Display create group UI
  renderCreateGroup(){
    var color = this.props.color;
    const  rightButtonConfig =
      <KlikFonts
       name="check"
       color={color.BUTTON_TEXT}
       size={32}
       style={{marginRight:5}}
       onPress={() => this.createGroup()}
      />
    ;

    var chatHeight = windowSize.height - 190;
    var hasContacts = this.state.contacts;
    var photo = <KlikFonts name="camera"
                  color={color.THEME}
                  size={40}
                  style={{alignSelf:'center', marginTop:20}}
                  onPress={() => this.onPhotoPress()
                  }
                 />;
    var contacts = <Text></Text>;
    if (hasContacts) {
      contacts = <ListView
        style={{height:chatHeight, paddingLeft:(windowSize.width * 0.10), paddingRight:(windowSize.width * 0.10)}}
        automaticallyAdjustContentInsets={false}
        ref='listview'
        dataSource={this.state.dataSourceContact}
        renderRow={this.renderContact}/>;
    }
    if (this.state.photoSource) {
      photo = <Image source={{uri: this.state.photoSource.uri}} style={{alignSelf:'stretch', width:79, height:79, borderRadius:39.5}}/>;
    }
    return (
      <View style={{backgroundColor:color.CHAT_BG}}>
          <NavigationBar
            rightButton={rightButtonConfig}
            tintColor={color.THEME}
            title={{ title: 'Create Group' , tintColor: color.BUTTON_TEXT}}
            leftButton={
              <KlikFonts
              name="back"
              color={color.BUTTON_TEXT}
              size={35}
              onPress={() => this.props.navigator.pop()}
             />
            }/>
          <View style={{flexDirection:'row', marginTop:20}}>
            <View style={{width: (windowSize.width / 3)}}>
              <View style={[{borderColor:color.THEME}, styles.groupNewImageContainer]}>
                  {photo}
              </View>
            </View>
            <View style={{flexDirection:'column', width: (windowSize.width - ( windowSize.width / 3)), paddingRight:30, paddingTop:15}}>
              <TextInput
                ref="groupInput"
                placeholder=' Name (Ex. Family)'
                multiline={false}
                autoFocus={true}
                placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
                selectionColor={color.THEME}
                style={[styles.groupNewInputName, {color:color.TEXT_DARK}]}
                onChangeText={(text) => this.setState({
                  groupName: text
                })}
              />
              <View style={{flexDirection:'row', alignSelf:'stretch', marginTop:-5}}>
                <View style={[styles.groupNewInputEdge, {backgroundColor:color.THEME}]}/>
                <View style={[styles.groupNewInputSeparatorName, {backgroundColor:color.THEME}]}/>
                <View style={[styles.groupNewInputEdge, {backgroundColor:color.THEME}]}/>
              </View>
            </View>
          </View>
          <View style={{marginTop:20, paddingLeft:(windowSize.width * 0.10), paddingRight:(windowSize.width * 0.10)}}>
            <TextInput
              ref="contactsInput"
              placeholder='Add people to group'
              placeholderTextColor='#888'
              multiline={false}
              autoFocus={true}
              placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
              selectionColor={color.THEME}
              style={[styles.groupNewInputName, {color:color.TEXT_DARK}]}
              onChangeText={(text) => this.filterContacts(text)}
            />
            <View style={{flexDirection:'row', alignSelf:'stretch', marginTop:-5}}>
              <View style={[styles.groupNewInputEdge, {backgroundColor:color.THEME}]}/>
              <View style={[styles.groupNewInputSeparatorPeople, {backgroundColor:color.THEME}]}/>
              <View style={[styles.groupNewInputEdge, {backgroundColor:color.THEME}]}/>
            </View>
          </View>
          {contacts}

      </View>
    );
  }

  // Render view
  render() {
    var render = (this.state.isConnected)
     ? this.renderCreateGroup()
     : render = <ScreenFailedToConnect text={GLOBAL.TIMEOUT_MESSAGE_DISCONNECTED} />;
    return render;
  }

  /*
  The first arg will be the options object for customization, the second is
  your callback which sends object: response.
  */
  onPhotoPress() {
    ImagePickerManager.launchImageLibrary(this.state.photoOptions, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
        this.onTypePress();
      }
      else if (response.error) {
        console.log('ImagePickerManager Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // uri (on android)
        const source = {uri: 'data:image/jpeg;base64,' + response.data,
                        isStatic: true};
        console.log(source);
        this.setState({photoSource: source});
      }
    });
  }

}

module.exports = GroupNew;
