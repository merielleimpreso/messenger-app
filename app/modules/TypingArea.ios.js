'use strict';

import React, {
  Component
} from 'react';
import ReactNative, {
  Dimensions,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import {logTime} from './Helpers';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import InputAudio from './InputAudio';
import InputOptions from './InputOptions';
import InputStickers from './InputStickers';
import Send from './config/db/Send';
const styles = StyleSheet.create(require('./../styles/styles_common'));
const BUTTON_CLICKED =  Object.freeze({"KEYBOARD":1, "OPTIONS":2, "STICKERS": 3, "RECORD": 4, "TEXTINPUT" : 5});
const SCREEN = Dimensions.get("window");

class TypingArea extends Component {

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      chosenInput: BUTTON_CLICKED.KEYBOARD,
      keyboard: 0,
      location: { latitude: 0, longitude: 0 },
      newMessage: '',
      optionButton: 'plus',
    };
    this.renderChosenInput = this.renderChosenInput.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getCurrentLocation();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isViewVisible != this.props.isViewVisible) {
      this.refs.messageInput.blur();
    }
  }

  // Get current location
  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        if (this._isMounted) {
          this.setState({
            location: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            }
          });
        }
      },
      (error) => console.log(error),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
  }

  render() {
    let color = this.props.color;
    let bgStyle = {margin:5, marginBottom:7, marginTop:7, justifyContent:'center'};
    let bgStyle1 = {backgroundColor:color.THEME};
    let iconStyle = {flex:1, margin:7};

    return (
      <View style={{backgroundColor:'#fff'}}>
        <View style={{flexDirection:'row', justifyContent:'center', flex:1}}>
          <TypingAreaButton onPress={() => this.onPressButtonOption()} icon={this.state.optionButton} iconColor={color.THEME} bgStyle={bgStyle} />
          <TypingAreaButton onPress={() => this.onPressButtonSticker()} icon='emoticon' iconColor={color.THEME} bgStyle={bgStyle} />
          {this.renderTextArea(bgStyle)}
          {(this.state.newMessage)
            ? <TypingAreaButton onPress={() => this.onPressButtonSend()} icon='send' iconColor={color.BUTTON_TEXT} iconStyle={iconStyle} bgStyle={bgStyle1} />
            : <TypingAreaButton onPress={() => this.onPressButtonRecord()} icon='mic' iconColor={color.BUTTON_TEXT} iconStyle={iconStyle} bgStyle={bgStyle1} />
          }
        </View>
        {this.renderChosenInput()}
        <View onLayout={(event) => {
          let keyboard = event.nativeEvent.layout.height;
          if ((keyboard > 0) && keyboard != this.state.keyboard) {
            this.setState({keyboard: keyboard});
          }
        }}>
          <KeyboardSpacer />
        </View>
      </View>
    );
  }

  renderTextArea(marginStyle) {
    let color = this.props.color;
    return (
      <View style={[{flex:1, alignItems:'center', justifyContent:'center'}, marginStyle]}>
        <TextInput ref='messageInput'
          autoCorrect={false}
          autoFocus={true}
          onChangeText={(text) => this.setState({newMessage: text ? text: false})}
          keyboardAppearance='dark'
          multiline={true}
          placeholder='Type a message...'
          returnKeyType='send'
          selectionColor={color.THEME}
          fontSize={13}
          style={{height:SCREEN.width*0.07}}
          placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
          rejectResponderTermination={false}
          onFocus={() => {
            this.setState({
              chosenInput: BUTTON_CLICKED.KEYBOARD,
              optionButton: 'plus'
            })
          }}
        />
      </View>
    );
  }

  renderChosenInput() {
    switch (this.state.chosenInput) {
      case BUTTON_CLICKED.OPTIONS:
        return (
          <InputOptions navigator={this.props.navigator}
            loggedInUser={this.props.loggedInUser}
            color={this.props.color}
            recipientId={this.props.recipientId}
            onPressButtonLocation={this.onPressButtonLocation.bind(this)}
            onPressButtonRecord={this.onPressButtonRecord.bind(this)}
            onSelectMedia={this.onSelectMedia.bind(this)}
            height={this.state.keyboard}
            theme={this.props.theme}
          />
        );
      case BUTTON_CLICKED.STICKERS:
        return (
          <InputStickers color={this.props.color}
           onStickerPress={this.onPressButtonStickerRow.bind(this)}
           height={this.state.keyboard}
          />
        );
      case BUTTON_CLICKED.RECORD:
        return (
          <InputAudio color={this.props.color}
           height={this.state.keybaord}
           onPressButtonRecordSend={this.onPressButtonRecordSend.bind(this)}
          />
        );
      default:
        return;
    }
  }

  onPressButtonOption() {
    let chosenInput = (this.state.optionButton == 'plus') ? BUTTON_CLICKED.OPTIONS : BUTTON_CLICKED.KEYBOARD;
    let optionButton = (this.state.optionButton == 'plus') ? 'keyboard' : 'plus';
    if (this._isMounted) {
      this.setState({
        chosenInput: chosenInput,
        optionButton: optionButton,
      });
    }
    if (chosenInput == BUTTON_CLICKED.KEYBOARD) {
      this.refs.messageInput.focus();
    } else {
      this.refs.messageInput.blur();
    }
  }

  onPressButtonRecord() {
    this.refs.messageInput.blur();
    if (this._isMounted) {
      this.setState({
        chosenInput: BUTTON_CLICKED.RECORD,
      });
    }
  }

  onPressButtonSticker() {
    this.refs.messageInput.blur();
    if (this._isMounted) {
      this.setState({
        chosenInput: BUTTON_CLICKED.STICKERS,
      });
    }
  }

/* ------------------------ SENDING FUNCTIONS ------------------------  */

  onPressButtonLike() {
    console.log('TypingArea ['+ logTime() +']: Sending like... ');
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    Send.like(sender, recipient, chatType);
  }

  onPressButtonLocation() {
    console.log('TypingArea ['+ logTime() +']: Sending current location... ');
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    let location = this.state.location;
    Send.currentLocation(sender, recipient, chatType, location);
  }

  onPressButtonRecordSend(uri, type, duration) {
    console.log('TypingArea ['+ logTime() +']: Sending audio... ');
    this.onSelectMedia(uri, type, duration);
  }

  onPressButtonSend() {
    console.log('TypingArea ['+ logTime() +']: Sending text message... ');
    this.refs.messageInput.setNativeProps({text: ''});
    if(this._isMounted) {
      this.setState({
        newMessage: '',
      });
    }
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    let message = this.state.newMessage;
    Send.textMessage(sender, recipient, chatType, message);
  }

  onPressButtonStickerRow(data) {
    console.log('TypingArea ['+ logTime() +']: Sending sticker...');
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    let sticker = data.Image;
    Send.sticker(sender, recipient, chatType, sticker);
  }

  onSelectMedia(uri, type, duration) {
    console.log('TypingArea ['+ logTime() +']: Sending ' + type + '...');
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    Send.media(sender, recipient, chatType, uri, type, duration);
  }

}

class TypingAreaButton extends Component {
  render() {
    let bgStyle = (this.props.bgStyle) ? this.props.bgStyle : {};
    let iconStyle = (this.props.iconStyle) ? this.props.iconStyle: {};
    let iconColor = this.props.iconColor;
    let icon = this.props.icon;
    let size = (icon == 'send' || icon == 'mic' || icon == 'keyboard') ? SCREEN.width * 0.1 : SCREEN.width * 0.07;

    return (
      <TouchableHighlight onPress={() => this.props.onPress()} underlayColor='transparent' style={bgStyle}>
        <KlikFonts name={icon} color={iconColor} size={size} style={iconStyle}/>
      </TouchableHighlight>
    )
  }
}

module.exports = TypingArea;
