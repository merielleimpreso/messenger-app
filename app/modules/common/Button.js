'use strict';

import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';
import SystemAccessor from '../SystemAccessor';

const SCREEN = Dimensions.get("window");

class Button extends Component {
  render() {
    let text = this.props.text;
    let color = this.props.color;
    let underlayColor = this.props.underlayColor;
    let width = SCREEN.width * 0.5;
    let buttonWidthStyle = (this.props.isDefaultWidth) ? {width:width} : {};
    let style = this.props.style;
    let fontSize = this.props.fontSize ? {fontSize: this.props.fontSize} : {};
    let fontColor = this.props.fontColor ? {color: this.props.fontColor} : {};

    return (
      <TouchableOpacity
       onPress={() => Navigation.onPress(this.props.onPress)}>
        <View style={[style, buttonWidthStyle, styles.row, {backgroundColor:color }]}>
          {this.props.icon ? <Icon name={this.props.icon} style={[styles.text, fontColor, {fontSize: 20}]} /> : null}
          <Text style={[styles.text, fontSize, fontColor]} allowFontScaling={true}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    color: '#FFF',
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 8,
    marginRight: 4,
    marginLeft: 4,
    textAlign: 'center'
  },
  row: {
    flexDirection: 'row',
    borderRadius: 3, 
    justifyContent: 'center',
    alignItems: 'center'
  }
});

// Export module.
module.exports = Button;
