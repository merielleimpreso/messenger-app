'use strict';

import React from 'react';
import {
  View,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  TouchableNativeFeedback
} from 'react-native';
import Navigation from '../actions/Navigation';

const GLOBAL = require('./../config/Globals.js');
const styles = StyleSheet.create({
  detailContainer: {
    paddingTop: 5,
    paddingRight: 15,
    paddingBottom: 5,
    paddingLeft: 15,
    flexDirection: 'row'
  },
  title: {
    fontSize: 15,
    fontWeight: '800',
  },
  details: {
    marginLeft: 10
  }
});

const ProfileDetailsRow = (props) => {
  let { action, color, title, profileDetail, onPressButton, buttonLabel, onSwitch, switchValue } = props;
  return (
    <TouchableOpacity onPress={() => Navigation.onPress(() => action())}>
      <View style={styles.detailContainer}>
        <View style={{flex: 1}}>
          <Text style={[styles.title, {color: color.TEXT_DARK}]}>{title}</Text>
          <Text numberOfLines={1} style={[styles.details, {color: color.TEXT_LIGHT}]}>{profileDetail}</Text>
        </View>
        {(onPressButton) ?
          <TouchableOpacity onPress={() => Navigation.onPress(() => onPressButton())}
           style={{backgroundColor: color.BUTTON, padding: 5, borderRadius: 2, alignSelf: 'center'}}>
            <Text style={{color: color.BUTTON_TEXT, fontSize: 12}}>{buttonLabel}</Text>
          </TouchableOpacity>
          : null
        }
        {(onSwitch) ?
          <Switch value={switchValue} onValueChange={(value) => Navigation.onPress(() => onSwitch(value))}/>
          : null
        }
      </View>
    </TouchableOpacity>
  )
}

export default ProfileDetailsRow;
