'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  PixelRatio,
  StyleSheet,
  Switch,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import Button from './Button';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ImageWithLoader from './ImageWithLoader';
import Navigation from '../actions/Navigation';


const SCREEN = Dimensions.get("window");
var styles = StyleSheet.create({
  row: {
    flex: 0,
    flexDirection: 'row',
    margin: 10,
    alignItems: 'stretch'
  },
  details: {
    flex: 1,
    justifyContent: 'center',
  },
  separator: {
    height: 1,
    marginLeft: (SCREEN.height * 0.075) + ((SCREEN.height * 0.075)/2),
    marginRight: 10,
  },
  image: {
    height: SCREEN.height * 0.075,
    width: SCREEN.height * 0.075,
    borderRadius: (SCREEN.height * 0.075)/2
  }
});

class ListRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      loading: true
    }
    this.renderRow = this.renderRow.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }

  render() {
    let color = this.props.color;
    if (this.props.onPress) {
      return (
        <TouchableHighlight onPress={() => Navigation.onPress(this.props.onPress)} underlayColor={color.HIGHLIGHT}>
          {this.renderRow()}
        </TouchableHighlight>
      );
    } else {
      return (
        <View>{this.renderRow()}</View>
      );
    }
  }

  renderRow () {
    let labelText = this.props.labelText;
    let labelInfoText = this.props.labelInfoText;
    let detailText = this.props.detailText;
    let badge = this.props.badge;
    let addSeparator = this.props.addSeparator;
    let image = this.props.image;
    let color = this.props.color;
    let icon = this.props.icon;
    let button = this.props.button;
    let iconColor = (icon == 'selected') ? {color: color.BUTTON} : {color: color.TEXT_LIGHT};
    let rowStyle = this.props.addSeparator ? {} : {marginTop:5, marginBottom:5};
    let separatorStyle = (image) ? {} : {marginLeft: 10};
    let friendRequestCount = this.props.friendRequestCount;
    let onSwitch = this.props.onSwitch;
    let switchValue = this.props.switchValue;

    return (
      <View>
        <View style={[styles.row, rowStyle]}>
          {(image) ? this.renderImage(image, color) : null}
          <View style={styles.details}>
            <View style={{flexDirection:'row'}}>
              <Text numberOfLines={1} style={{fontSize:16, color:color.TEXT_DARK, flex:4}}>{labelText}</Text>
              {(labelInfoText)
                ? <View style={{flexDirection:'column', alignItems:'flex-end'}}>
                    <Text numberOfLines={1} style={{fontSize:10, color:color.TEXT_LIGHT}}>{labelInfoText}</Text>
                  </View>
                : null
              }
              {(friendRequestCount) ?
                <View style={{flexDirection: 'row', alignSelf: 'center', marginLeft: 5}}>
                  <Text style={{color: color.TEXT_LIGHT, fontSize: 12}}>{friendRequestCount}</Text>
                  <KlikFonts name='next' style={{color: color.HIGHLIGHT, fontSize: 18}} />
                </View> :
                null}
            </View>
            <View style={{flexDirection:'row'}}>
              {(detailText && (detailText != ' ')) ? <Text numberOfLines={1} style={{fontSize:16, color:color.TEXT_LIGHT,flex:4}}>{detailText}</Text> : null}
              {(badge) ? <View style={{flexDirection:'column', alignItems:'flex-end',flex:1, }}>{badge}</View> : null}
            </View>
          </View>
          {(icon)
            ? <TouchableHighlight
               onPress={() => Navigation.onPress(this.props.iconPress)}
               underlayColor={'transparent'}
               style={{justifyContent: 'center'}}>
                <KlikFonts name={icon} style={[iconColor, {fontSize: 25}]}/>
              </TouchableHighlight>
            : null
          }
          {(button)
            ? <View style={{alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                <Button text={button}
                 color={color.THEME}
                 fontSize={12}
                 underlayColor={color.HIGHLIGHT}
                 onPress={() => this.props.iconPress()}
                 style={{width: SCREEN.width * 0.2}}
                 isDefaultWidth={false} />
              </View>
            : null
          }
          {(onSwitch)
            ? <Switch value={switchValue}
               onValueChange={(value) => Navigation.onPress(() => onSwitch(value))}/>
            : null
          }
        </View>
        {(addSeparator) ? <View style={[styles.separator, separatorStyle, {backgroundColor:color.HIGHLIGHT}]}/> :  null}
      </View>
    )
  }

  renderImage(image, color) {
    return (
      <View style={{marginRight:10}}>
        <ImageWithLoader image={image} color={color} style={styles.image} />
      </View>
    );
  }

}

// Export module.
module.exports = ListRow;
