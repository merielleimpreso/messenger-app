import React, {Component} from 'react';
import {
  ActivityIndicator,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
const GLOBAL = require('./../config/Globals.js');

export default class ImageWithLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false
    }
  }

  render() {
    let color = this.props.color;
    let image = this.props.image;
    let style = this.props.style;
    let bgColor = (this.props.sticker) ? 'transparent' : color.HIGHLIGHT;


    return (
      <View style={[style, {backgroundColor:bgColor}]}>
        <Image key={image}
         source={{uri:image}}
         style={[style, styles.image]}
         onLoadStart={(e) => this.setState({loading: true})}
         onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
         onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
         onLoad={() => this.setState({loading: false, error: false, isLoaded: true,})} >
         {(this.state.loading && !this.state.isLoaded && this.props.sticker)
           ? <View style={[style, {backgroundColor:color.HIGHLIGHT}]} />
           : null
         }
        </Image>
      </View>
    );
  }
}

var styles = (Platform.OS === 'android') ?
StyleSheet.create({
  image: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  thumb: {
    width: 90,
    height: 90
  },
}) :
StyleSheet.create({
  thumb: {
    width: 70,
    height: 70
  },
})
