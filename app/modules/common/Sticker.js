import React, {Component} from 'react';
import {
  ActivityIndicator,
  Platform,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native';
import ImageWithLoader from './ImageWithLoader';
import Navigation from '../actions/Navigation';
const size = (Platform.OS === 'android') ? 90 : 80;
const GLOBAL = require('./../config/Globals.js');

export default class Sticker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false
    }
  }

  render() {
    let color = this.props.color;
    let key = this.props.data._id
    let link = `${ddp.mediaServerUrl}${this.props.data.Image}`; // 'http://'+ GLOBAL.SERVER_HOST+':3000'+this.props.data.Image;
    let imgSource = {
      uri: link
    };

    return (
      <TouchableHighlight style={styles.row}
       onPress={() => Navigation.onPress(() => this.props.onPress(this.props.data))}
       underlayColor='rgba(0,0,0,0)'>
        <View>
          <ImageWithLoader key={key} image={link} sticker={true} color={color} style={[styles.thumb]} />
        </View>
      </TouchableHighlight>
    )
  }
}

var styles = StyleSheet.create({
  row: {
    justifyContent: 'center',
    width: size,
    height: size,
    alignItems: 'center',
  },
  thumb: {
    width: size,
    height: size,
    borderRadius: size / 2
  },
})
