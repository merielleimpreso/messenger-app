import React from 'react';
import {
  Dimensions,
  View,
  Text,
  TouchableHighlight
} from 'react-native';
import ImageWithLoader from './ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';

const windowSize = Dimensions.get('window');

let OptionsButton = (props) => {
  let { color, icon, label, image, onPress } = props;
  let iconSize = (image) ? windowSize.width * 0.12 : windowSize.width * 0.15;
  let marginTop = (image) ? {marginTop: 5} : null;
  let numIcons = 3;
  let width = windowSize.width / numIcons;

  return (
    <TouchableHighlight style={{width: width, alignItems:'stretch'}}
      onPress={() => Navigation.onPress(() => onPress())}
      underlayColor={color.HIGHLIGHT}>
      <View style={{marginTop:10, marginBottom:10, flex:1, alignItems:'center', justifyContent: 'flex-end'}}>
        {(image)
          ? <ImageWithLoader image={image} color={color} style={{width:iconSize, height:iconSize, borderRadius: iconSize/2}} />
          : <KlikFonts name={icon} style={{alignSelf:'center', color: color.TEXT_LIGHT, fontSize: iconSize}}/>
        }
        <Text style={[marginTop, {fontSize: 11, color: color.TEXT_LIGHT}]}>{label}</Text>
      </View>
    </TouchableHighlight>
  );
}

export default OptionsButton;
