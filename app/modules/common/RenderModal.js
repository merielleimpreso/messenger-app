'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Modal,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
  ActivityIndicator,
  Image
} from 'react-native';

const styles = StyleSheet.create(require('./../../styles/styles.main'));
const SCREEN = Dimensions.get("window");
import _ from 'underscore';
import { toNameCase, getDisplayName, getMediaURL } from './../Helpers';
import Button from './Button';
import GLOBAL from './../config/Globals.js';
import ImageWithLoader from './ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './ListRow';
import Navigation from '../actions/Navigation';
import ProfilePic from './ProfilePic';

export default class RenderModal extends Component {
  constructor(props) {
    super(props);
    let detail = (this.props.detail) ? this.props.detail : "";
    this.state = {
      email: '',
      detail: detail,
      textHeight: 0
    }
    this.renderChangeProfileDetail = this.renderChangeProfileDetail.bind(this);
    this.renderOptions = this.renderOptions.bind(this);
  }

  render() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    let right = (SCREEN.width * 0.15) / 3.8;
    let top = (SCREEN.width * 0.25) / 1.8;
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={true}
        onRequestClose={() => {this.props.setIsShowModal()}}
        >
        <TouchableWithoutFeedback onPress={() => Navigation.onPress(this.props.setIsShowModal)}>
          <View style={{alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.3)'}}>
          <TouchableWithoutFeedback>
            {this.renderOptions()}
          </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }

  renderOptions() {
    switch (this.props.modalType) {
      case 'connection':
      case 'alert':
        return this.renderMessage();
      case 'link':
        return this.renderInput();
      case 'loading':
        return this.renderLoading();
      case 'loggingIn':
      case 'loggingOut':
      case 'uploading':
        return this.renderLoading();
      case 'uploadImage':
        return this.renderChooseToUpload();
      case 'signOutOptions':
      case 'options':
        return this.renderSignOutOptions();
      case 'resetPassword':
        return this.renderResetPassword();
      case 'viewProfile':
        return this.renderViewProfile();
      case 'messageOptions':
        return this.renderMessageOptions();
      case 'changeProfileDetail':
        return this.renderChangeProfileDetail();
      case 'menu':
        return this.renderMenu();
    }
  }

  renderMenu() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    return (
      <View style={{position: 'absolute', bottom: 0}}>
        <View style={{width: SCREEN.width, backgroundColor: color.CHAT_BG, flex: 1}}>
          {this.props.actions.map((obj, i) => {
            return (
              <ListRow onPress={() => obj.action()}
                key={i}
                labelText={obj.title}
                color={color}
                addSeparator={true}
              />
            );
          })}
        </View>
      </View>
    );
  }

  renderChangeProfileDetail() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let width = SCREEN.width * 0.9;
    return (
      <View style={{width: width, height: (SCREEN.height * 0.15 + this.state.textHeight) ,backgroundColor: color.CHAT_BG, borderRadius: 2}}>
        <View style={{backgroundColor: color.THEME, padding: 10, borderTopLeftRadius: 3, borderTopRightRadius: 3}}>
          <Text style={{color: color.BUTTON_TEXT}}>{this.props.title}</Text>
        </View>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={[styles.searchBoxContainer, {backgroundColor: color.HIGHLIGHT, width: width * 0.75, flexDirection: 'row', alignItems: 'center'}]}>
            <TextInput ref="changedStatus"
              maxLength={this.props.maxLength}
              multiline={true}
              value={this.state.detail}
              onChangeText={(detail) => {
                detail = (detail.trim() == '') ? '' : detail.replace('  ', ' ');
                this.setState({detail: detail})
              }}
              onContentSizeChange={(event) => {
                this.setState({textHeight: event.nativeEvent.contentSize.height});
              }}
              style={[styles.input, {backgroundColor: 'transparent', textAlign: 'left', height: Math.max(35, this.state.textHeight), flex: 1}]}
              placeholder={this.props.title}
              placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
              autoCapitalize='sentences'
            />
            {this.renderClose()}
          </View>
          <TouchableHighlight style={[styles.searchBoxContainer, {backgroundColor: color.BUTTON, marginLeft: 0, justifyContent: 'center', height: 35, width: width * 0.15}]}
            underlayColor={color.HIGHLIGHT}
            onPress={() => Navigation.onPress(() => this.props.setChangeProfileDetail(this.state.detail))}>
            <Text style={{alignSelf: 'center', color: color.BUTTON_TEXT}}>Save</Text>
          </TouchableHighlight>
        </View>
        <Text style={{marginLeft: 10, color: color.TEXT_LIGHT}}> {this.props.maxLength - this.state.detail.length} / {this.props.maxLength} </Text>

      </View>
    );
  }

  renderClose() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.detail) {
      return (
        <View style={{borderRadius: 50, width: 15, height: 15, backgroundColor: color.TEXT_LIGHT, margin: 7, justifyContent: 'center'}}>
          <KlikFonts name="close" style={{borderRadius: 10, fontSize: 15, color: '#fff', alignSelf: 'center', backgroundColor: 'transparent'}}
            onPress={() => this.setState({detail: ''})}
          />
        </View>
      );
    }
  }

  renderInput() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{padding: 15, width: SCREEN.width * 0.75, height: SCREEN.height * 0.22 ,backgroundColor: color.CHAT_BG, borderRadius: 2}}>
        <Text style={{color: color.TEXT_DARK, textAlign: 'left', fontSize: 18}}>Enter URL</Text>
        <TextInput
          placeholder="Enter a URL here"
          onChangeText={(text) => {
            text = (text.trim() == '') ? '' : text;
            this.setState({linkText: text})}
          }
          onSubmitEditing={() => this.props.setLinkContent(this.state.linkText)}
          style={{flex:1, fontSize:13, padding:10, backgroundColor:'#fff', textAlignVertical: 'top'}}
        />
        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
          <Button text={'Cancel'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => {
              this.props.setIsShowModal()
            }}
            style={{width: SCREEN.width*0.25, alignSelf: 'center', marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
          <Button text={'Confirm'}
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => {
              this.props.setLinkContent(this.state.linkText)
            }}
            style={{width: SCREEN.width*0.25, alignSelf: 'center', marginLeft: 10, marginRight: 10}}
            isDefaultWidth={false}/>
        </View>
      </View>
    );
  }

  renderChooseToUpload() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    return (
      <View style={{padding: 15, width: SCREEN.width * 0.75, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        <Text style={{color: color.THEME, marginTop: 5, marginBottom: 10}}>Upload from</Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Button text="Gallery" color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.onPressOpenGallery()}
            isDefaultWidth={false}
            style={{margin: 5, width: SCREEN.width * 0.25}}/>
          <Button text="Camera" color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.onPressOpenCamera()}
            isDefaultWidth={false}
            style={{margin: 5, width: SCREEN.width * 0.25}}/>
        </View>
      </View>
    );
  }

  renderLoading() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    let text = 'Loading...';

    if (this.props.modalType == 'uploading') {
      text = 'Uploading...'
    } else if (this.props.modalType == 'loggingOut') {
      text = 'Logging Out...';
    } else if (this.props.modalType == 'loggingIn') {
      text = 'Logging In...';
    } else if(this.props.modalType == 'converting') {
      text = 'Converting...';
    }

    return (
      <View style={{padding: 15, width: SCREEN.width * 0.75, backgroundColor: color.CHAT_BG, borderRadius: 2,
                    alignItems: 'center', flexDirection: 'row'}}>
        <ActivityIndicator size={'large'} color={color.THEME} />
        <Text style={{color: color.TEXT_LIGHT, marginLeft: 15, textAlign: 'center'}}>{text}</Text>
      </View>
    );
  }

  renderSignOutOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let label = (this.props.modalType == 'options') ? 'leave' : 'sign out';
    return (
      <View style={{padding: 15, width: SCREEN.width * 0.75, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        <Text style={{color: color.THEME, marginTop: 5, marginBottom: 10, textAlign: 'center'}}>Are you sure you want to {label}?</Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Button text="No" color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.setIsShowModal(false)}
            isDefaultWidth={false}
            style={{margin: 5, width: SCREEN.width * 0.2}}/>
          <Button text="Yes" color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.onPress()}
            isDefaultWidth={false}
            style={{margin: 5, width: SCREEN.width * 0.2}}/>
        </View>
      </View>
    );
  }

  renderMessage() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    let message = (this.props.modalType == 'connection') ?
                    'Please make sure that you have network connectivity and try again.' :
                    this.props.message;
    return (
      <View style={{padding: 15, width: SCREEN.width * 0.75, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        <Text style={{color: color.TEXT_LIGHT, paddingBottom: 15, textAlign: 'center'}}>{message}</Text>

        <TouchableHighlight
         onPress={() => Navigation.onPress(this.props.setIsShowModal)}
         underlayColor={color.CHAT_BG}
         style={{width: SCREEN.width * 0.75}}>
          <Text style={{color: color.THEME, textAlign: 'center'}}>OK</Text>
        </TouchableHighlight>

      </View>
    );
  }

  renderResetPassword() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;

    return (
      <View style={{padding: 15, width: SCREEN.width * 0.85, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        <Text style={{color: color.THEME, marginBottom: 15, textAlign: 'center'}}>Please type your email</Text>
        <View style={[styles.inputLoginContainer, {borderBottomColor: color.INACTIVE_BUTTON}]}>
          <TextInput
            placeholder='Email'
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
            keyboardType='email-address'
            style={[styles.input, {backgroundColor: 'transparent', color: color.TEXT_DARK}]}
          />
        </View>
        <Button text='Reset Password' color={color.THEME}
          underlayColor={color.HIGHLIGHT}
          onPress={() => this.props.onPressForgetPassword(this.state.email)}
          isDefaultWidth={false}
          style={{margin: 5, width: SCREEN.width * 0.75}}/>
      </View>
    );
  }

  renderViewProfile() {
    let profileData = this.props.profileData.data;
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    let group = profileData.hasOwnProperty('category') && profileData.category == 'Group';

    return (
      <View style={{width: SCREEN.width * 0.85, height: SCREEN.height * 0.75, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        {(group) ? this.renderGroupProfile() : this.renderProfile()}
        {this.renderActions()}
      </View>
    );
  }

  renderMessageOptions() {
    //let profileData = this.props.profileData.data;
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    //let group = profileData.hasOwnProperty('category') && profileData.category == 'Group';

    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: 'rgba(0, 0, 0, 0)'}}>
        <View style={{width: SCREEN.width * 0.65, height: SCREEN.height * 0.155, backgroundColor: color.CHAT_BG, borderRadius: 5, alignItems: 'center'}}>
            <View style={{
                         width: SCREEN.width * 0.65,
                         height: SCREEN.height * 0.155,
                         flexDirection: 'column',
                         alignSelf: 'center',
                         justifyContent: 'center'
                       }}>
                <TouchableHighlight
                  onPress={() => Navigation.onPress(this.props.copyText)}
                  underlayColor={color.HIGHLIGHT}
                  style={{borderTopLeftRadius: 5, borderTopRightRadius: 5, height: SCREEN.height * 0.0775 }}>
                  <View style={{flex: 1, flexDirection: 'row', borderBottomWidth: 1.5,borderBottomColor: color.HIGHLIGHT, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ marginTop: SCREEN.height * 0.02, marginBottom: SCREEN.height * 0.02}}> Copy</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight
                  onPress={() => Navigation.onPress(this.props.setIsShowModal)}
                  underlayColor={color.HIGHLIGHT}
                  style={{borderBottomLeftRadius: 5, borderBottomRightRadius: 5, height: SCREEN.height * 0.0775 }}>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={{ marginTop: SCREEN.height * 0.02, marginBottom: SCREEN.height * 0.02}}> Cancel</Text>
                </View>

                </TouchableHighlight>
            </View>
        </View>
      </View>
    );
  }

  renderActions() {
    let profileData = this.props.profileData;
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;

    return (
      <View style={{flexDirection: 'row', borderTopWidth: 0.5, borderTopColor: color.HIGHLIGHT}}>
        {(profileData.actions) ?
          profileData.actions.map((obj, i) => {
            return (
              <TouchableHighlight key={i} underlayColor={color.HIGHLIGHT}
                onPress={() => Navigation.onPress(() => obj.onPressIcon())}
                style={{flex: 1, padding: 10}}>
                <View style={{alignItems: 'center'}}>
                  <KlikFonts name={obj.icon} style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
                  <Text style={{fontSize: 10, color: color.TEXT_LIGHT, textAlign: 'center'}}>{obj.iconLabel}</Text>
                </View>
              </TouchableHighlight>
            )})
          : null
        }
      </View>
    );
  }

  renderProfile() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    let profileData = this.props.profileData.data;
    let name = (profileData.name) ? toNameCase(profileData.name) : toNameCase(getDisplayName(profileData));
    let photo = getMediaURL(profileData.image);
    let statusMessage = profileData.statusMessage;

    return (
      <View style={{width: SCREEN.width * 0.85, flex: 1}}>
        <Image source={require('./../../images/backgroundImage/chatbg1.jpg')} style={{height: SCREEN.height * 0.3, width: null}} />
        <ProfilePic source={photo} size={150} color={color} style={{marginTop: -(SCREEN.width * 0.4) / 2}}/>
        {/*<Image source={{uri: photo}} style={{height: SCREEN.width * 0.4, width: SCREEN.width * 0.4, borderRadius: 100, alignSelf: 'center', marginTop: -(SCREEN.width * 0.4) / 2 }} />*/}
        <Text style={{alignSelf: 'center', marginTop: 10, color: color.TEXT_DARK, textAlign: 'center'}}>{name}</Text>
        <Text style={{alignSelf: 'center', marginTop: 10, color: color.TEXT_LIGHT, textAlign: 'center'}}>{statusMessage}</Text>
        {(profileData.hasOwnProperty('category')) ?
          null :
          <Text style={{alignSelf: 'center', color: color.TEXT_LIGHT, textAlign: 'center'}}>{profileData.username}</Text>
        }
        {(this.props.profileData.relStatus) ?
          <Text style={{alignSelf: 'center', color: color.TEXT_LIGHT, textAlign: 'center'}}>{this.props.profileData.relStatus}</Text>
          : null
        }
      </View>
    );
  }

  renderGroupProfile() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let profileData = this.props.profileData.data;
    let groupImage = (profileData.image) ? profileData.image : GLOBAL.DEFAULT_GROUP_IMAGE;;
    // groupImage = getMediaURL(groupImage);
    return (
      <View style={{width: SCREEN.width * 0.85, flex: 1}}>
        <Image source={{uri: getMediaURL(groupImage)}} style={{height: SCREEN.height * 0.3, }} />
        <Text style={{alignSelf: 'center', marginTop: 25, color: color.TEXT_DARK, fontSize: 18, textAlign: 'center'}}>{profileData.name}</Text>
        <TouchableWithoutFeedback
          onPress={() => Navigation.onPress(() => this.props.goToGroupDetails(profileData))}>
          <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 15}}>
            {profileData.users.map((obj, i) => {
              if (i < 4) {
              let image = (obj.image) ? getMediaURL(obj.image) : getMediaURL(GLOBAL.DEFAULT_PROFILE_IMAGE);
              return <View key={i} style={{margin: 2}}>
                      <ImageWithLoader image={image} color={color} style={{height: 40, width: 40, borderRadius: 40 / 2}} />
                     </View>
              }
            })}
            <View style={{height: 40, width: 40, borderRadius: 50, margin: 2, backgroundColor: color.HIGHLIGHT, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{color: color.TEXT_LIGHT, fontSize: 11, marginLeft: 5}}>{profileData.users.length}</Text>
              <KlikFonts name="next" style={{fontSize: 15, color: color.TEXT_LIGHT}}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
