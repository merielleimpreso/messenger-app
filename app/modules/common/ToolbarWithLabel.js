'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  StyleSheet,
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
const GLOBAL = require('./../config/Globals.js');
const Toolbar = (props) => {
  let { color, title, actions, onActionSelected } = props;
  return (
    <View style={[styles.toolbarContainer, {backgroundColor: color.THEME}]} {...props}>
      <Text style={[styles.toolbarMenu, {color: color.BUTTON_TEXT}]}>{title}</Text>
      {(actions) ?
        actions.map(function(action, i) {
          return <Action action={action} key={i} i={i} color={color} onPress={onActionSelected}/>; 
        })
        : null
      }
    </View>
  );
};

const Action = ({action, color, i, onPress}) => (
  <TouchableNativeFeedback onPress={() => {onPress(i)}}>
    <View style={styles.actionContainer}>
      <Text style={[styles.actionTitle, {color: color.BUTTON_TEXT}]}>{action.title}</Text>
      <KlikFonts name={action.iconName} style={[styles.actionIcon, {color: color.BUTTON_TEXT}]} />
    </View>
  </TouchableNativeFeedback>
);

const styles = StyleSheet.create({
  toolbarContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15 
  },
  toolbarMenu: {
    fontSize: 20,
    flex: 1
  },
  actionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 4,
    paddingRight: 4,
    height: 50
  },
  actionTitle: {
    fontSize: 12
  }, 
  actionIcon: {
    fontSize: 20,
    marginLeft: 3
  }
});

export default Toolbar;