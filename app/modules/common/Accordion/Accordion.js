import React, {
  Component,
  PropTypes,
} from 'react';

import {
  View,
  TouchableHighlight,
} from 'react-native';
import _ from 'underscore';
import Collapsible from './Collapsible';
import SoundEffects from '../../actions/SoundEffects';

const COLLAPSIBLE_PROPS = Object.keys(Collapsible.propTypes);
const VIEW_PROPS = Object.keys(View.propTypes);

class Accordion extends Component {
  static propTypes = {
    sections: PropTypes.array.isRequired,
    renderHeader: PropTypes.func.isRequired,
    renderContent: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    align: PropTypes.oneOf(['top', 'center', 'bottom']),
    duration: PropTypes.number,
    easing: PropTypes.string,
    initiallyActiveSection: PropTypes.number,
    // activeSection: PropTypes.oneOfType([
    //   PropTypes.bool, // if false, closes all sections
    //   PropTypes.number, // sets index of section to open
    // ]),
    underlayColor: PropTypes.string,
  };

  static defaultProps = {
    underlayColor: 'black',
  };

  constructor(props) {
    super(props);

    // if activeSection not specified, default to initiallyActiveSection
    this.state = {
      activeSection: props.activeSection //!== undefined ? props.activeSection : props.initiallyActiveSection,
    };
  }

  _toggleSection(category) {
    var activeSection = this.state.activeSection;
    if (_.contains(activeSection, category)) {
      activeSection = _.without(activeSection, category);
    } else {
      activeSection.push(category);
    }

    SoundEffects.playSound("tick")
    this.setState({
      activeSection: activeSection
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeSection !== undefined) {
      this.setState({
        activeSection: nextProps.activeSection,
      });
    }
  }

  render() {
    let viewProps = {};
    let collapsibleProps = {};
    Object.keys(this.props).forEach((key) => {
      if (COLLAPSIBLE_PROPS.indexOf(key) !== -1) {
        collapsibleProps[key] = this.props[key];
      } else if (VIEW_PROPS.indexOf(key) !== -1) {
        viewProps[key] = this.props[key];
      }
    });

    return (
      <View {...viewProps}>
      {this.props.sections.map((section, key) => (
        <View key={key}>
          <TouchableHighlight onPress={() => this._toggleSection(section.category)} underlayColor={this.props.underlayColor}>
            {this.props.renderHeader(section, key, _.contains(this.state.activeSection, section.category))}
          </TouchableHighlight>
          <Collapsible collapsed={!_.contains(this.state.activeSection, section.category)} {...collapsibleProps}>
            {this.props.renderContent(section, key, _.contains(this.state.activeSection, section.category))}
          </Collapsible>
        </View>
      ))}
      </View>
    );
  }
}

module.exports = Accordion;