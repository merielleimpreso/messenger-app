import React, { Component } from 'react';
import {
  Platform,
  View
} from 'react-native';
import ButtonIcon from './../../common/ButtonIcon';
import GLOBAL from './../../config/Globals';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../../actions/Navigation';
import NavigationBar from 'react-native-navbar';
import styles from './styles';

export default class Toolbar extends Component {
  render() {
    let color = (this.props.theme) ? GLOBAL.COLOR_THEME[this.props.theme()] : this.props.color;
    if (Platform.OS == 'ios') {
      let rightIcon = (this.props.rightIcon) ? this.props.rightIcon : 'close';
      let leftIcon = (this.props.leftIcon) ? this.props.leftIcon : 'back';
      let rightButton = (this.props.onPressRightButton) ? ((this.props.isRemovedFromGroup) ? <View/> : <ButtonIcon icon={rightIcon} color={color} onPress={() => this.props.onPressRightButton()} size={25}/>) : <View/>
      let leftButton = (this.props.withBackButton) ? <ButtonIcon icon={leftIcon} color={color} onPress={() => this.onPressIcon()} size={25}/> : <View/>
      return (
        <NavigationBar tintColor={color.THEME}
          title={{title: this.props.title, tintColor: color.BUTTON_TEXT}}
          rightButton={rightButton} 
          leftButton={leftButton} 
        />
      );
    } else {
      let elevation = (this.props.toolbarFrom == 'Tabs') ? 0 : 5;
      let icon = (this.props.toolbarFrom != 'Tabs') ? 'back' : (this.props.title == 'Add Friends') ? 'back' : null;
      return (
        <KlikFonts.ToolbarAndroid
          navIconName={icon}
          onIconClicked={() => this.onPressIcon()}
          elevation={elevation}
          title={this.props.title}
          titleColor={color.BUTTON_TEXT}
          style={[styles.toolbar, {backgroundColor: color.THEME}]}
          actions={this.props.toolbarActions}
          onActionSelected={this.props.onActionSelected} />
      );
    }
  }

  onPressIcon() {
    if (this.props.onIconClicked) {
      this.props.onIconClicked();
    } else {
      Navigation.back(this.props.navigator);
    } 
  }
} 