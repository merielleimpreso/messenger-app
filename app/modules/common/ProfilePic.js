import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import ImageWithLoader from './ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';

const size = 140;

const ProfilePic = (props) => {
  let { source, color, changeAvatar, style, size } = props;
  let radius = size / 2;
  let position = (size > 100) ? 7 : 0;// -(size / 3.5);
  let marginContainer = (size > 100) ? {margin: 20} : {};
  style = (style) ? style : {};
  return (
    <View style={[marginContainer, style, {height: size, width: size, alignSelf: 'center'}]}>
      <TouchableOpacity onPress={() => {(changeAvatar) ? Navigation.onPress(() => changeAvatar()) : console.log('profile picture');}}>
        <ImageWithLoader image={source} color={color} style={[styles.pictureContainer, {width: size, height: size, borderRadius: radius, borderColor: color.TEXT_LIGHT}]} />
      </TouchableOpacity>
      {(changeAvatar) ?
        <View style={[styles.changeAvatarIcon, {backgroundColor: color.CHAT_BG, borderColor: color.TEXT_LIGHT, bottom: position, right: position}]}>
          <KlikFonts name="photo" onPress={() => Navigation.onPress(() => changeAvatar())} style={[styles.icon, {color: color.TEXT_LIGHT}]}/>
        </View>
        : null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  pictureContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.5,
  },
  changeAvatarIcon: {
    // alignSelf: 'flex-end',
    // borderRadius: 50,
    // borderWidth: 0.5,
    // width: 25,
    // height: 25,
    // alignItems: 'center',
    // justifyContent: 'center', 
    position: 'absolute',
    borderRadius: 50,
    borderWidth: 0.5,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    fontSize: 18
  }
});

ProfilePic.propTypes = {
  size: React.PropTypes.number,
};

// ProfilePic.defaultProps = {
//   source: images.tempPic,
// };

export default ProfilePic;
