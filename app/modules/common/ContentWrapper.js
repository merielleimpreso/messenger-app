'use strict';

import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
const GLOBAL = require('./../config/Globals.js');

const ContentWrapper = (props) => {
  let { color, children, style } = props;
  style = (style) ? style : {};
  return (
    <View style={[styles.container, {backgroundColor: color.CHAT_BG}, style]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  }
});


export default ContentWrapper;