'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';
const SCREEN = Dimensions.get("window");

class ButtonIcon extends Component {
  render() {
    let icon = this.props.icon;
    let color = this.props.color;
    let iconSize = !this.props.size ? SCREEN.width * 0.1 : this.props.size;

    return (
      <TouchableHighlight
       onPress={() => Navigation.onPress(this.props.onPress)}
       underlayColor='transparent'
       style={styles.bg}>
        <KlikFonts name={icon} color={color.BUTTON_TEXT} style={styles.icon} size={iconSize}/>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  bg: {
    alignSelf:'center'
  },
  icon: {
    alignSelf:'center',
    margin:5
  }
});

module.exports = ButtonIcon;
