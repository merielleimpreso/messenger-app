import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

class ConnnectionStatus extends Component {
  render() {
    let color = this.props.color;
    let text = this.props.text;
    let bg = (this.props.isError) ? color.INACTIVE_BUTTON : color.BUTTON;

    return (
      <View style={[styles.container, {backgroundColor: bg}]}>
        <Text style={{color:color.BUTTON_TEXT}}>{text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems:'center',
    justifyContent: 'center'
  }
});

module.exports = ConnnectionStatus;
