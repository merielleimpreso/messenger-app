'use strict';

import React, { Component } from 'react';
import { Alert, Linking, ListView, Platform } from 'react-native';
import Ads from './../config/db/Ads';
import Navigation from './../actions/Navigation';
import OptionsRender from './OptionsRender';

const GLOBAL = require('./../config/Globals.js');
var isUpdatingAds = false;//variable so that di na sagad ka setState
var hasInternet = false;

export default class Options extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      position: 1,
      ads: [],
      isLoggedIn: this.props.isLoggedIn(),
      isUpdatingAds: false,
      isLoading: true
    }
    this.incrementAdIndex = this.incrementAdIndex.bind(this)
    this.fetchAds = this.fetchAds.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.incrementAdIndex();
    this.fetchAds();
    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(Ads)
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isLoggedIn() != this.state.isLoggedIn) {
      if (this._isMounted) {
        this.setState({
          isLoggedIn: this.props.isLoggedIn()
        });
      }
      console.log('Fetching ads..')
      setTimeout(() => {
        this.fetchAds();
      }, 2000);
    }
  }

  render() {
    return (
      <OptionsRender
        dataSource={this.state.dataSource}
        goToAddFriends={this.goToAddFriends.bind(this)}
        goToProfile={this.goToProfile.bind(this)}
        goToSettings={this.goToSettings.bind(this)}
        isLoading={this.state.isLoading}
        openLink={this.openLink.bind(this)}
        placeHolderAlert={this.placeHolderAlert.bind(this)}
        position={this.state.position}
        ads={this.state.ads}
        {...this.props}
      />
    );
  }

  openLink(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        consoleLog('Options: Cannot open ad url: ' + url);
      }
    });
  }

  fetchAds() {
    let adsLink = (Platform.OS == 'ios')
      ? "https://kliklabs.com/klikchat/kcbanner.php?apps=ios"
      : "http://kliklabs.com/klikchat/kcbanner.php";
    fetch(adsLink)
      .then((response) => response.json())
      .then((responseJson) => {
        let bannerlist = responseJson.bannerlist;
        this.setState({
          ads: bannerlist,
          isLoading: false
        })
      })
      .catch((error) => {
        console.log("didMount Fetch fail: ", error);
    });
  }

  goToSettings() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'Settings') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id: 'settings',
          name: 'Settings',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeTheme: this.props.changeTheme,
          debugText: this.props.debugText,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          isAutoSaveMedia: this.props.isAutoSaveMedia,
          isDebug: this.props.isDebug,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsAutoSaveMedia: this.props.setIsAutoSaveMedia,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setLoggedInUser: this.props.setLoggedInUser,
          setSignUpCallback: this.props.setSignUpCallback,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          checkLoginToken: this.props.checkLoginToken,
          setIsLoggingOut: this.props.setIsLoggingOut,
        });
      });
    }
  }

  goToProfile() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;
    var loggedInUser = this.props.loggedInUser();

    var user = loggedInUser.profile;
    user["_id"] = loggedInUser._id;
    user['mobile'] = loggedInUser.mobile;
    user['username'] = loggedInUser.username;

    if (navigatorStack[currentRoute].name !== 'Profile') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id: 'profile',
          name: 'Profile',
          theme: this.props.theme,
          hasInternet: this.props.hasInternet,
          isAutoLocate: this.props.isAutoLocate,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setIsAutoLocate: this.props.setIsAutoLocate,
          shouldReloadData: this.props.shouldReloadData,
          locationError: this.props.locationError,
          loggedInUser: this.props.loggedInUser,
          color: GLOBAL.COLOR_THEME[this.props.theme()],
          user: user,
        });
      });
    }
  }

  goToAddFriends() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'AddFriends') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id:'contactsadd',
          name:'AddFriends',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeBgImage: this.props.changeBgImage,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          isAutoSaveMedia: this.props.isAutoSaveMedia,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          subscribeContacts: this.props.subscribeContacts,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          users: this.props.users
        });
      });
    }
  }

  placeHolderAlert() {
    Alert.alert("Oops. Under construction.","¯\\_(ツ)_/¯")
  }

  incrementAdIndex() {
    console.log("Called incrementAdIndex");
    var position = this.state.position;
    position++;
    if(position >= 3) {
      position = 0;
    }

    this.setState({
      position: position
    })

    //setTimeout(()=>{console.log("Called timeout");this.incrementAdIndex()}, 5000);
  }
}
