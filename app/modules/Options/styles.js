import { Dimensions, StyleSheet } from 'react-native';
const windowSize = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  rowContainer: {
    flexDirection: 'row'
  },
  titleSuggestions: {
    fontSize: 12, 
    marginLeft: 15, 
    marginRight: 15, 
    marginTop: 10, 
    marginBottom: 10
  },
  featuredAd : {
    width: windowSize.width * 0.92,
    height: 150, 
    alignSelf: 'center'
  },
  listView: {
    justifyContent: 'center', 
    flexDirection: 'row', 
    flexWrap: 'wrap'
  },
  adContainer: {
    width: windowSize.width * 0.165, 
    marginLeft: 4, 
    marginRight: 4, 
    justifyContent: 'flex-start'
  },
  image: {
    height: windowSize.width * 0.165, 
    width: windowSize.width * 0.165
  },
  text: {
    textAlign: 'center', 
    fontSize: 10, 
    justifyContent: 'flex-start'
  },
  sliderButton: {
    fontSize: 30
  }
});
