import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  ListView,
  Platform,
  Text,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import { getMediaURL } from './../Helpers.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from './../actions/Navigation';
import NavigationBar from 'react-native-navbar';
import OptionsButton from './../common/OptionsButton';
import Swiper from 'react-native-swiper';

const GLOBAL = require('./../config/Globals.js');
import styles from './styles';
const windowSize = Dimensions.get('window');
var hasUpdated = false;
export default class OptionsRender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ads: [{image: "none"}],
      hasInternet: false,
      key: Math.random(),//key is used by Swiper to supposedly force reload it
                         //this is to fix images disappearing on rerender without
                          //changes to state.
    }
    this.renderRow = this.renderRow.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.props.loggedInUser();
    let fontSizeMultiplier = ((windowSize.height * 0.2)/100);
    let fontSize = 30 * fontSizeMultiplier;
    let numberOfSuggestionElements = 10;

    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        {(Platform.OS == 'ios') ?
          <View>
            <NavigationBar
              tintColor={color.THEME}
              title={{title:'Options', tintColor: color.BUTTON_TEXT}}
            />
            {this.props.renderConnectionStatus()}
          </View> : null}
        <View style={styles.rowContainer}>
          <OptionsButton icon='addcontact' label='Add Friend' color={color} onPress={() => this.props.goToAddFriends()} />
          <OptionsButton icon='settings' label='Settings' color={color} onPress={() => this.props.goToSettings()} />
          <OptionsButton image={getMediaURL(loggedInUser.profile.image)} label='Profile' color={color} onPress={() => this.props.goToProfile()} />
        </View>

        <View>
          {this.props.isLoading ?
            (this.props.hasInternet() ?
            <View style={{height: 250,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'
              }}>
              <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} style={{padding:10}} />
              <Text>Loading...</Text>
            </View>
            : <View style={{height: 250,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#afaaaa'
              }}>
              <Text>Cannot load ads</Text>
            </View>)
            :
            <Swiper showsButtons={true}
              nextButton={<Text style={[styles.sliderButton, {color: color.THEME}]}>›</Text>}
              prevButton={<Text style={[styles.sliderButton, {color: color.THEME}]}>‹</Text>}
              height={250}
              autoplay={true}
              autoplayTimeout={4}
              activeDotColor={color.THEME}
              contentContainerStyle={{alignItems: 'center'}}>
              {this.props.ads.map((item, i) => {
                let image = item.image;
                image = (image.indexOf('http') != -1) ? image : 'http://kliklabs.com'+item.image;

                return (
                  <View key={i} style={{height: 250,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                    <TouchableWithoutFeedback onPress={()=> Navigation.onPress(()=> this.props.openLink(item.urltarget))}>
                      <Image style={{height: 200, width: 350}} source={{uri: image}} />
                    </TouchableWithoutFeedback>
                  </View>
                );
              })}
            </Swiper>
          }
        </View>


        <Text style={[styles.titleSuggestions, {color: color.TEXT_DARK}]}>
          Suggestions
        </Text>

        <ListView contentContainerStyle={styles.listView}
          dataSource={this.props.dataSource}
          enableEmptySections={true}
          renderRow={this.renderRow}
        />
      </View>
    );
  }


  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <TouchableWithoutFeedback onPress={() => Navigation.onPress(() => this.props.openLink(data.link))}>
        <View style={styles.adContainer}>
          <Image style={styles.image} source={data.image}/>
          <Text style={[styles.text, {color: color.TEXT_LIGHT}]}>{data.name}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
