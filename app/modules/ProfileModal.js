'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  Image,
  TouchableHighlight,
  StyleSheet,
  Dimensions
} from 'react-native';
import { getMediaURL } from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
const styles = StyleSheet.create(require('./../styles/styles.main'));
const GLOBAL = require('./config/Globals.js');
const SCREEN = Dimensions.get("window");

const ProfileModal = ({profileData, color, setProfileData}) => (
  <Modal
    animationType={"fade"}
    transparent={true}
    visible={true}
    onRequestClose={() => {setProfileData(null)}}
    >
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.3)'}}>
      <View style={{width: SCREEN.width * 0.85, height: SCREEN.height * 0.7, backgroundColor: color.CHAT_BG, borderRadius: 2, alignItems: 'center'}}>
        <View style={{width: SCREEN.width * 0.85, flex: 1}}>
          <Image source={require('./../images/backgroundImage/chatbg1.jpg')} style={{height: SCREEN.height * 0.3, width: null}} />
          <Image source={{uri: getMediaURL(profileData.profile.photoUrl)}} style={{height: SCREEN.height * 0.2, width: SCREEN.height * 0.2, borderRadius: 100, alignSelf: 'center', marginTop: -(SCREEN.height * 0.2) / 2 }} />
          <Text style={{alignSelf: 'center', marginTop: 10, color: color.TEXT_DARK}}>{`${profileData.profile.firstName} ${profileData.profile.lastName}`}</Text>
          
        </View>
        <View style={{flexDirection: 'row', borderTopWidth: 0.5, borderTopColor: color.HIGHLIGHT}}>
          <TouchableHighlight underlayColor={color.HIGHLIGHT}
            onPress={() => this.goToProfile()}
            style={{flex: 1, padding: 10}}>
            <View style={{alignItems: 'center'}}>
              <KlikFonts name="settings" style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT}}>Edit Profile</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    </View>
  </Modal>
);

export default ProfileModal;