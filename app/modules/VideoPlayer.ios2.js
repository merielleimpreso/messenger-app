/*

Author: Merielle Impreso
Date Created: 2016-06-09
Date Updated: NA
Description: Module to play video. Most of the code is from react-native-video
  https://github.com/brentvatne/react-native-video/blob/master/Examples/VideoPlayer/index.ios.js

update: 2016-06-09 (Ida)
- added videoUri condition for direct link
update: 2016-06-27 (ida)
  - changed icons to klikfonts
*/

'use strict';
import React, {
  Component
} from 'react';

import {
  Alert,
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform
} from 'react-native';
import {getMediaURL} from './Helpers';
import FileReader from 'react-native-fs';
import NavigationBar from 'react-native-navbar';
import Validator from 'validator';
import Video from 'react-native-video';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
var GLOBAL = require('./config/Globals.js');

class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.onLoad = this.onLoad.bind(this);
    this.onProgress = this.onProgress.bind(this);
  }
  state = {
    rate: 1,
    volume: 1,
    muted: false,
    resizeMode: 'contain',
    duration: 0.0,
    currentTime: 0.0,
    controls: false,
    paused: false,
    skin: 'custom',
    uri: ''
  };

  componentDidMount() {
    let video = this.props.data;
    this.setState({uri: video});
    console.log(video)
    // if (Validator.isURL(video)) {
    //   this.setState({uri: video});
    // } else {
    //   let videoPath = FileReader.DocumentDirectoryPath + '/video.mp4';
    //   FileReader.writeFile(videoPath, video, 'base64')
    //   .then((success) => {
    //     console.log('FILE WRITTEN!');
    //     this.setState({uri: videoPath});
    //   })
    //   .catch((err) => {
    //     console.log(err.message);
    //   });
    // }
  }

  onLoad(data) {
    this.setState({duration: data.duration});
  }

  onProgress(data) {
    this.setState({currentTime: data.currentTime});
  }

  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) {
      return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
    } else {
      return 0;
    }
  }

  renderSkinControl(skin) {
    const isSelected = this.state.skin == skin;
    const selectControls = skin == 'native' || skin == 'embed';
    return (
      <TouchableOpacity onPress={() => { this.setState({
          controls: selectControls,
          skin: skin
        }) }}>
        <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
          {skin}
        </Text>
      </TouchableOpacity>
    );
  }

  renderRateControl(rate) {
    const isSelected = (this.state.rate == rate);

    return (
      <TouchableOpacity onPress={() => { this.setState({rate: rate}) }}>
        <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
          {rate}x
        </Text>
      </TouchableOpacity>
    )
  }

  renderResizeModeControl(resizeMode) {
    const isSelected = (this.state.resizeMode == resizeMode);

    return (
      <TouchableOpacity onPress={() => { this.setState({resizeMode: resizeMode}) }}>
        <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
          {resizeMode}
        </Text>
      </TouchableOpacity>
    )
  }

  renderVolumeControl(volume) {
    const isSelected = (this.state.volume == volume);

    return (
      <TouchableOpacity onPress={() => { this.setState({volume: volume}) }}>
        <Text style={[styles.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
          {volume * 100}%
        </Text>
      </TouchableOpacity>
    )
  }

  restartVideo() {
    // Alert.alert('Done!');
    this.refs.player.seek(0);
    this.setState({paused: true})
    this.forceUpdate();
  }

  renderCustomSkin() {
    const color = this.props.color;
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
    const renderVideo = (this.state.uri == '') ? <View /> :
      <Video ref="player"
        source={{uri: this.state.uri}}
        style={styles.fullScreen}
        rate={this.state.rate}
        paused={this.state.paused}
        volume={this.state.volume}
        muted={this.state.muted}
        resizeMode={this.state.resizeMode}
        onLoad={this.onLoad}
        onProgress={this.onProgress}
        //onEnd={() => { Alert.alert('Done!!!') }}
        onEnd={this.restartVideo.bind(this)}
        repeat={false}
      />;

    const nav = (Platform.OS === 'ios') ?
        <NavigationBar
          tintColor={color.THEME}
          title={{ title: 'Play Video', tintColor: color.BUTTON_TEXT}}
          leftButton={
            <KlikFonts
            name="back"
            color={color.BUTTON_TEXT}
            size={35}
            onPress={() => this.props.navigator.pop()}
           />
          }
        /> :

        <View style={[styles.topContainer, {backgroundColor: color.THEME}]}
            elevation={5}>
          <TouchableOpacity
            onPress={() => this.props.navigator.pop()}
            style={{padding: 8}}
            underlayColor={color.HIGHLIGHT}>
            <KlikFonts name="back"
              color={color.BUTTON_TEXT}
              size={25}
              style={{marginBottom:9, marginTop:8}}/>
          </TouchableOpacity>
          <View style={styles.header}>
            <Text style={[styles.headerName, {color: color.BUTTON_TEXT}]}>Play {this.props.media}</Text>
          </View>
        </View>


    return (
      <View style={[styles.mainContainer, {backgroundColor: color.CHAT_BG}]}>
        {nav}
        <View style={styles.container}>
          <TouchableOpacity style={styles.fullScreen} onPress={() => {this.setState({paused: !this.state.paused})}}>
            {renderVideo}
          </TouchableOpacity>

          <View style={styles.controls}>
            {/*<View style={styles.generalControls}>
              <View style={styles.skinControl}>
                {this.renderSkinControl('custom')}
                {this.renderSkinControl('native')}
                {this.renderSkinControl('embed')}
              </View>
            </View>*/}

            <View style={styles.generalControls}>
              {/*<View style={styles.rateControl}>
                {this.renderRateControl(0.5)}
                {this.renderRateControl(1.0)}
                {this.renderRateControl(2.0)}
              </View>*/}

              {/*<View style={styles.volumeControl}>
                {this.renderVolumeControl(0.5)}
                {this.renderVolumeControl(1)}
                {this.renderVolumeControl(1.5)}
              </View>*/}

               {/*<View style={styles.resizeModeControl}>
                {this.renderResizeModeControl('cover')}
                {this.renderResizeModeControl('contain')}
                {this.renderResizeModeControl('stretch')}
              </View>*/}

            </View>

            <View style={styles.trackingControls}>
              <View style={styles.progress}>
                <View style={[styles.innerProgressCompleted, {flex: flexCompleted}]} />
                <View style={[styles.innerProgressRemaining, {flex: flexRemaining}]} />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  renderNativeSkin() {
    var color = this.props.color;
    const videoStyle = this.state.skin == 'embed' ? styles.nativeVideoControls : styles.fullScreen;
    const renderVideo = (this.state.uri == '') ? <View /> :
      <Video source={{uri: this.state.uri}}
        style={videoStyle}
        rate={this.state.rate}
        paused={this.state.paused}
        volume={this.state.volume}
        muted={this.state.muted}
        resizeMode={this.state.resizeMode}
        onLoad={this.onLoad}
        onProgress={this.onProgress}
        onEnd={() => { Alert.alert('Done!') }}
        repeat={true}
        controls={this.state.controls}
      />
    return (
      <View style={styles.mainContainer}>
        <NavigationBar
          tintColor={color.THEME}
          title={{ title: ' ', tintColor: color.BUTTON_TEXT}}
          leftButton={
            <Icon
            name="ios-arrow-thin-left"
            color={color.BUTTON_TEXT}
            size={35}
            style={{marginLeft:15, marginBottom:10}}
            onPress={() => this.props.navigator.pop()}
           />
          }
        />
        <View style={styles.container}>
          <View style={styles.fullScreen}>
            {renderVideo}
          </View>
          <View style={styles.controls}>
            <View style={styles.generalControls}>
              <View style={styles.skinControl}>
                {this.renderSkinControl('custom')}
                {this.renderSkinControl('native')}
                {this.renderSkinControl('embed')}
              </View>
            </View>
            <View style={styles.generalControls}>
              <View style={styles.rateControl}>
                {this.renderRateControl(0.5)}
                {this.renderRateControl(1.0)}
                {this.renderRateControl(2.0)}
              </View>

              <View style={styles.volumeControl}>
                {this.renderVolumeControl(0.5)}
                {this.renderVolumeControl(1)}
                {this.renderVolumeControl(1.5)}
              </View>

              <View style={styles.resizeModeControl}>
                {this.renderResizeModeControl('cover')}
                {this.renderResizeModeControl('contain')}
                {this.renderResizeModeControl('stretch')}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    // return this.state.controls ? this.renderNativeSkin() : this.renderCustomSkin();

    return this.renderCustomSkin();
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  controls: {
    backgroundColor: "transparent",
    borderRadius: 5,
    position: 'absolute',
    bottom: 44,
    left: 4,
    right: 4,
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  innerProgressCompleted: {
    height: 20,
    backgroundColor: '#cccccc',
  },
  innerProgressRemaining: {
    height: 20,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    paddingBottom: 10,
  },
  skinControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: "white",
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
  nativeVideoControls: {
    top: 184,
    height: 300
  },

  // for android
  topContainer: {
    height: 55,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 5
  },
  headerName: {
    fontSize: 20
  },
});

// Export module
module.exports = VideoPlayer;
