/*

Author: Merielle Impreso
Date Created: 2016-04-28
Date Updated: 2016-04-30
Description: Module where sticker images are displayed.

Changelog:
update: 2016-04-30 by Merielle I.
  - Send sticker to server
update: 2016-07-26 by John B.
  - Fix view
  - Added prop parameter onStickerPress, transfered pressing a sticker function on parent component
update: 2016-08-17 by Merielle I.
  - Build the sticker url based on the Global.js host and port settings

*/

'use strict';

import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Image,
  ListView,
  TouchableHighlight,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  Platform
} from 'react-native';
import { getMediaURL } from './Helpers';
import ScreenLoading from './ScreenLoading';
import Sticker from './common/Sticker';
const GLOBAL = require('./config/Globals.js');
const HEIGHT = 220;

var InputStickers = React.createClass({

  // Get initial state
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      noConnection: false,
      loading: false
    };
  },

  _pressData: ({}: {[key: number]: boolean}),

  /*
  Check flag if component is mounted.
  Call getStickers.
  */
  componentDidMount: function() {
    this.isMounted = true;
    this.getStickers();
  },

  // Check flag if component is unmounted.
  componentWillUnmount: function() {
    this.isMounted = false;
  },

  /*
  Get stickers collection by subscription to the server.
  Put these to the dataSource.
  */
  getStickers: function() {
    AsyncStorage.getItem('stickers')
    .then(result => {
      if (result) {
        console.log('stickers from AsyncStorage');
        let stickers = JSON.parse(result);
        if (this.isMounted) {
          this.setState({dataSource: this.getDataSource(stickers)});
        }
      } else {
        if (this.props.hasInternet()) {
          ddp.subscribe('stickers', [])
          .then((result) => {
            if (this.isMounted) {
              AsyncStorage.setItem('stickers', JSON.stringify(ddp.collections.stickers.items));
              this.setState({dataSource: this.getDataSource(ddp.collections.stickers.items)});
            }
          });
        } else {
          if (this.isMounted) {
            this.setState({noConnection: true});
          }
        }
      }
    });
  },

  // Clone dataSource to rows
  getDataSource(array: Array<any>): ListView.DataSource {
    return this.state.dataSource.cloneWithRows(array);
  },

  /*
  Render the view.
  ListView wraps ScrollView and so takes on its properties.
  With that in mind you can use the ScrollView's contentContainerStyle prop to style the items.
  */
  render: function() {
    let color = this.props.color;
    let height = (this.props.height > 0) ? this.props.height : 220;
    let content = (this.state.noConnection)
      ? <ScreenLoading isLoading={false} color={color} text={`Please make sure that you have${'\n'}network connectivity and try again.`} />
      : (this.state.dataSource._cachedRowCount === 0)
        ? <ScreenLoading isLoading={true} color={color} text={GLOBAL.TEXT_LOADING_STICKERS} />
        : <ListView contentContainerStyle={styles.list}
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}
            removeClippedSubviews={false}
          /> ;
    return (
      <View style={{height: height, backgroundColor:color.CHAT_BG}} >
      {content}
      </View>
    );
  },

  // Display each sticker and pass the data to pressRow function.
  renderRow: function(data) {
    let color = this.props.color;
    var link = 'http://'+ GLOBAL.SERVER_HOST+':3000'+data.Image;
    var imgSource = {
      uri: link
    };

    return (
      <Sticker data={data}
        source={imgSource}
        onPress={this.pressRow}
        color={color}
      />
    );
  },

  /*
  Populate the options variable.
  The sticker property is passed, the server will have to check for this.
  */
  pressRow: function(data) {
    this.props.onStickerPress(data);
  },
});

// Styles for the view
var styles = (Platform.OS === 'android') ?
StyleSheet.create({
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  row: {
    justifyContent: 'center',
    padding: 2,
    width: 90,
    height: 90,
    alignItems: 'center',
    borderColor: '#CCC'
  },
  thumb: {
    width: 85,
    height: 85
  },
  text: {
    flex: 1,
    marginTop: 5,
    fontWeight: 'bold'
  }
}) :
StyleSheet.create({
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  row: {
    justifyContent: 'center',
    width: 80,
    height: 80,
    alignItems: 'center'
  },
  thumb: {
    width: 70,
    height: 70
  },
})

// Export module
module.exports = InputStickers;
