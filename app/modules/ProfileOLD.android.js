 /*Author: Terence John Lampasa
Date Created: 2016-03-11
Date Updated: 2016-05-17
Description: This file displays the directchat items from the database.
              Each row display is handled by ChatRecentRenderRow.
              - Implementation based from Messages.ios.js
            It passes the following to ChatRecentRenderRow as props:
              data = recent chat item
              data.chatType = GLOBAL.CHAT.DIRECT;
              data.user = logged in user
Used In: Messenger.android.js

Changelog:
update: 2016-04-21 (by Ida J.)
  - Used latest ddp-client.
  - Added DirectChat for database subscription and observe.
update: 2016-04-25 (by Ida J.)
  - added comments
update: 2016-04-26 (by Terence)
  -moved removed some logout lines and moved to Messenger.android
  -now calls Messenger.android logout function.
update: 2016-05-02 (by Terence)
  -Updated ui as per the new requirements by sir lito
update: 2016-05-04 (by Terence)
  -Fixed placeholder profile picture not showing if user still has no picture yet
-Update: 2016-05-04 (by Terence)
  -Password fields included. OldPassword not yet involved in server processing.
 Update: 2016-05-06 (By Terence)
  -Old password now involved in server processing.
 Update: 2016-05-12 (By Ida)
 -Fixed Signout button going off screen on Moto X.
 update: 2016-05-17 (by Ida)
 - fixing off screen display when keyboard is open(not yet done)
 update: 2016-06-16 (Ida)
  - Changed color scheme similar to whatsapp
 update: 2016-06-22 (Ida)
 - Implement color themes
 update: 2016-06-27 (ida)
 - Commented status
 - Implement changing of photo / avatar
 update: 2016-06-28 (ida)
 - Fixed bug when changing photo / avatar via camera
 update: 2016-06-30 (ida)
 - Added light blue color theme
update: 2016-07-12 (ida)
  - Fixed changing of color theme
*/

'use strict'

import React, { Component } from 'react';

import {
  ActivityIndicator,
  AsyncStorage,
  Image,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions,
  BackAndroid,
  ToastAndroid
} from 'react-native';

import _ from 'underscore';
import {launchCamera, launchImageLibrary, toNameCase, getMediaURL, toParagraph} from './Helpers.js';
import ContentWrapper from './common/ContentWrapper';
import ProfileDetailsRow from './common/ProfileDetailsRow';
import ProfilePic from './common/ProfilePic';
import RenderModal from './common/RenderModal';
import Toolbar from './common/Toolbar';

import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Geocoder from 'react-native-geocoder';
import ImageWithLoader from './common/ImageWithLoader';
import Send from './config/db/Send';
import Modal from 'react-native-simple-modal';
import Navigation from './actions/Navigation';
import SystemAccessor from './SystemAccessor';
import StoreCache from './config/db/StoreCache';

var ProgressBar = require('ActivityIndicator');
var ImagePickerManager = require('NativeModules').ImagePickerManager;
var styles = StyleSheet.create(require('./../styles/styles.main'));
var GLOBAL = require('./../modules/config/Globals.js');
var windowSize = Dimensions.get('window');
let isSafeToBack = true;

let timeout;
var timesUploadAttempted = 0;
class Profile extends Component {
  constructor(props) {
    super(props);
    let loggedInUser =  this.props.loggedInUser();
    this.state = {
      isAutoLocate: this.props.isAutoLocate(),
      isChangingDisplayName: false,
      isChangingStatus: false,
      isOpenImagePicker: false,
      isShareUpdate: true,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isUploading: false,
      isLoading: false, //for general waiting for ddp response
      loggedInUser: loggedInUser,
      shouldCheckLocationSettings: false,
      statusMessage: (loggedInUser.profile.statusMessage && loggedInUser.profile.statusMessage != ' ') ? loggedInUser.profile.statusMessage : null,
      name: loggedInUser.profile.name,
      mobile: loggedInUser.mobile,
      checkInButtonText: "Check In",
      location: loggedInUser.location.locality,
      image: loggedInUser.profile.image
    }
    this.checkFileSize = this.checkFileSize.bind(this);
    this.onPressMobileNumber = this.onPressMobileNumber.bind(this);
    this.saveAndSetLocate = this.saveAndSetLocate.bind(this);
    this.setChangeProfileDetail = this.setChangeProfileDetail.bind(this);
    this.setIsChangingName = this.setIsChangingName.bind(this);
    this.setIsChangingStatus = this.setIsChangingStatus.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsUploading = this.setIsUploading.bind(this);
    this.changePhoto = this.changePhoto.bind(this);
    this.changeStatusMessage = this.changeStatusMessage.bind(this);
    this.changeDisplayName =  this.changeDisplayName.bind(this);
    this.launchGallery = this.launchGallery.bind(this);
    this.launchCamera = this.launchCamera.bind(this);
    this.setMobileNumber = this.setMobileNumber.bind(this);
    this.changeShareUpdate = this.changeShareUpdate.bind(this);
    this.changePhoto = this.changePhoto.bind(this);
    this.goToLocationSettings = this.goToLocationSettings.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state.loggedInUser) != JSON.stringify(this.props.loggedInUser())) {
      if (this._isMounted) {
        this.setState({
          loggedInUser: this.props.loggedInUser()
        });
      }
    }
    // console.log(this.state.shouldCheckLocationSettings, prevState.shouldCheckLocationSettings, this.props.locationError());
    if (this.state.shouldCheckLocationSettings != prevState.shouldCheckLocationSettings) {
      if (this.state.isAutoLocate && this.props.locationError() == 'No available location provider.') {
        this.saveAndSetLocate(false);
      }
    }

  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    // TODO: use loggedInUser
    let color = this.props.color;
    let image = getMediaURL(this.state.image);
    let name = this.state.name;
    let statusMessage = (this.state.statusMessage && (this.state.statusMessage != ' ')) ? this.state.statusMessage : 'Not Set';
    let location = (this.state.isAutoLocate) ? (this.state.location) ? this.state.location : 'Not Set' : ' ';
    let userId = this.props.loggedInUser().username;
    let mobile = this.state.mobile;

    return (
      <ContentWrapper color={color}>
        <Toolbar title={'Profile'} {...this.props}/>
        <ProfilePic source={image} color={color} size={150} changeAvatar={this.setIsOpenImagePicker.bind(this)} style={{margin: 20}}/>

        <View  style={{paddingTop: 5, paddingRight: 15, paddingBottom: 5, paddingLeft: 15, flexDirection: 'row'}}>
          <Text style={{fontSize: 15, fontWeight: '800',color: color.TEXT_DARK, flex: 1}}>Share updated profile on timeline</Text>
          <View style={[styles.changeAvatarIcon, {backgroundColor: color.CHAT_BG, borderColor: color.TEXT_LIGHT, marginLeft: -20}]}>
            {this.renderShareUpdateIcon()}
          </View>
        </View>

        <ProfileDetailsRow title='Display Name' profileDetail={name} color={color} action={() => {
          this.setIsChangingName(true);
        }}/>
        <ProfileDetailsRow title='Status Message' profileDetail={statusMessage} color={color} action={() => {this.setIsChangingStatus(true);}}/>
        <ProfileDetailsRow action={() => console.log('check in')} title='Checked in' profileDetail={location} color={color}
          onSwitch={(value) => this.saveAndSetLocate(value)}
          switchValue={this.state.isAutoLocate}
        />
        <ProfileDetailsRow action={() => console.log('user id')} title='User ID' profileDetail={userId} color={color}/>
        <ProfileDetailsRow action={() => console.log('phone number')}
          onPressButton={() => this.onPressMobileNumber()}
          title='Phone Number' profileDetail={mobile}
          color={color} buttonLabel=' Change '
        />

        {this.renderModal()}
      </ContentWrapper>
    )
  }

  renderShareUpdateIcon() {
    let theme = this.props.theme();
    let color = GLOBAL.COLOR_THEME[theme];
    let isShareUpdate = this.state.isShareUpdate;
    let iconColor = (isShareUpdate) ? color.BUTTON : color.CHAT_BG;

    return <KlikFonts
            name="check"
            style={{color: iconColor, fontSize: 22}}
            onPress={() => Navigation.onPress(this.changeShareUpdate)}/>;
  }

  renderModal() {
    if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.state.isOpenImagePicker) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => this.setIsOpenImagePicker()}
          onPressOpenGallery={this.launchGallery}
          onPressOpenCamera={this.launchCamera}
          modalType={'uploadImage'}
        />
      );
    } else if (this.state.isUploading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('uploading')}
          modalType={'uploading'}
        />
      );
    } else if (this.state.isChangingDisplayName) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.state.name}
          maxLength={30}
          title={'Display Name'}
          setIsShowModal={this.setIsChangingName}
          setChangeProfileDetail={this.setChangeProfileDetail}
          modalType={'changeProfileDetail'}
        />
      );
    } else if (this.state.isChangingStatus) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.state.statusMessage}
          title={'Status Message'}
          maxLength={120}
          setIsShowModal={this.setIsChangingStatus}
          setChangeProfileDetail={this.setChangeProfileDetail}
          modalType={'changeProfileDetail'}
        />
      );
    } else if(this.state.isLoading){
      return (<RenderModal
        theme={this.props.theme}
        setIsShowModal={() => console.log('loading')}
        modalType={'loading'}
      />);
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    }
  }

  saveAndSetLocate(isAutoLocate) {
    StoreCache.storeCache(StoreCache.keys.isAutoLocate, JSON.stringify(isAutoLocate));
    this.props.setIsAutoLocate(isAutoLocate);
    if (this._isMounted) {
      this.setState({ isAutoLocate: isAutoLocate });
    }
    if (isAutoLocate) {
      if (this._isMounted) {
        this.setState({
          shouldCheckLocationSettings: true
        });
      }
      if (this.props.locationError() == 'No available location provider.') {
        ToastAndroid.show('Please turn on your location settings. Try again later.', ToastAndroid.LONG);
      } else {
        ToastAndroid.show('Turned on Location.', ToastAndroid.SHORT);
      }
    } else {
      if (this._isMounted) {
        this.setState({
          shouldCheckLocationSettings: false
        });
      }
      ToastAndroid.show('Turned off Location.', ToastAndroid.SHORT);
    }
  }

  launchGallery() {
    this.setIsOpenImagePicker();
    if (!this.props.hasInternet()) {
     this.setIsShowCheckConnection();
    } else {
      launchImageLibrary('photo', (response) => {
        this.checkFileSize(response);
        // this.changePhoto(response);
      });
    }
  }

  launchCamera() {
    this.setIsOpenImagePicker();
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      launchCamera('photo', (response) => {
        this.checkFileSize(response);
        // this.changePhoto(response);
      });
    }
  }

  checkFileSize(response) {
    if (typeof response == "string") {
      this.setIsShowMessageModal(response);
    } else {
      this.setIsUploading();
      let data = response;
      data['userId'] = this.props.loggedInUser()._id;
      Send.uploadMedia(data, 'photo', Send.uploadURL.profilePicture, (result) => {
        if (typeof result == 'string' && result.includes('/avatar')) {
          let loggedInUser = this.state.loggedInUser;
          loggedInUser.profile.image = getMediaURL(result);
          this.setState({
            loggedInUser: loggedInUser,
            image: loggedInUser.profile.image
          });

          if (this.state.isShareUpdate) {
            this.postChangePhoto(response);
          }
        }
        else if (result == 'Uploading failed.'){
          this.setIsShowMessageModal("Uploading interrupted. Try again.");
        }
        this.setIsUploading();
      });

    }
  }

  changePhoto(response) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsUploading();
      const source = {image: 'data:image/jpeg;base64,' + response.data, isStatic: true};
      var loggedInUser = this.state.loggedInUser;
      // console.log("Source.image: ", source.image);
      ddp.call('editImage', [source.image]).then(result => {
        if (result) {

          loggedInUser.profile.image = getMediaURL(result);
          this.setState({
            loggedInUser: loggedInUser,
            image: loggedInUser.profile.image
          });

          if (this.state.isShareUpdate) {
            this.postChangePhoto(response);
          }
          this.setIsUploading();
        }
      })
      .catch((e)=>{
        console.log('[Profile] changeAvatar Error: ', e)
          if(this.state.isUploading){
            //this.setIsShowMessageModal("Uploading Failed");
            //this.setIsUploading();

              // if(timesUploadAttempted < 4) {
              //     this.changePhoto(response);
              //     timesUploadAttempted++;
              //     console.log("Retried upload: ", timesUploadAttempted);
              // }
              // else {
              //   this.setIsShowMessageModal("Uploading Failed");
              //
              //
              // }
                //this.setIsUploading();


                if(timesUploadAttempted < 4) {
                  this.changePhoto(response);
                  timesUploadAttempted++;
                  console.log("Retrying upload. (", timesUploadAttempted, ")");
                } else {
                  timesUploadAttempted = 0;
                  this.setIsShowMessageModal("Uploading Failed");
                  this.setIsUploading();
                }




          }

      });

      // setTimeout(()=>{
      //     if(this.state.isUploading){
      //       this.setIsShowMessageModal("Uploading Failed");
      //       this.setIsUploading();
      //     }
      //   }, 15000);


    }
  }

  setMobileNumber(newMobileNumber) {
    this.setState({
      mobile: newMobileNumber
    });
  }

  onPressMobileNumber() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      console.log("Open press mobilenumber");
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name: 'ChangeMobileNumber',
          hasInternet: this.props.hasInternet,
          loggedInUser: this.props.loggedInUser,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setMobileNumber: this.setMobileNumber,
          theme: this.props.theme,
        });
      });
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setChangeProfileDetail(detail) {

    if (this.state.isChangingDisplayName) {
      this.changeDisplayName(detail);
    }

    if(this.state.isChangingStatus){
      this.changeStatusMessage(detail);
    }

  }

  setIsChangingName() {
    if (this._isMounted) {
      this.setState({
        isChangingDisplayName: !this.state.isChangingDisplayName,
      });
    }
  }

  setIsChangingStatus() {
    if (this._isMounted) {
      this.setState({
        isChangingStatus: !this.state.isChangingStatus,
      });
    }
  }

  setIsOpenImagePicker() {
    if (this._isMounted) {
      this.setState({isOpenImagePicker: !this.state.isOpenImagePicker});
    }
  }

  setIsUploading() {
    if (this._isMounted) {
      this.setState({isUploading: !this.state.isUploading});
    }
  }

  setIsLoading() {
    if (this._isMounted) {
      this.setState({isLoading: !this.state.isLoading});
    }
  }

  changeStatusMessage(statusMessage) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if ((!(_.isEmpty(statusMessage)) && statusMessage !== this.state.statusMessage)
          || (this.state.statusMessage && _.isEmpty(statusMessage))  ) {
        this.setIsChangingStatus();
        this.setIsLoading();
        // this.setState({isLoading: true});

        ddp.call('editStatusMessage', [statusMessage]).then((res)=>{
          console.log("Edit Response: ", res);
          this.setIsLoading();
          if (this._isMounted) {
            this.setState({
              statusMessage: (res && (res != ' ')) ? res : null,
            });
          }
        })
        .catch((err)=>{
          console.log("Caught: ", err);
        });
      } else {
        this.setIsChangingStatus();
        this.setIsShowMessageModal('There is nothing to update.');
      }
    }
  }

  changeDisplayName(displayName) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      if (!(_.isEmpty(displayName)) && displayName !== this.state.name) {
        this.setIsChangingName();
        this.setIsLoading();

        ddp.call('editName', [displayName]).then((res) => {
          this.setIsLoading();
          this.setState({name: res});
        });
      } else {
        this.setIsChangingName();
        this.setIsShowMessageModal('There is nothing to update.');
      }
    }
  }

  changeShareUpdate() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsLoading();
      let isShareUpdate = (this.state.isShareUpdate == true) ? false : true;

      ddp.call('isShareUpdate', [isShareUpdate]).then(result => {
        let isShared = (result == "true") ? true : false;
        if (this._isMounted) {
          this.setState({
            isShareUpdate: isShared
          });
        }
        this.setIsLoading();
      }).catch((e) => {
        console.log('[Profile] isShareUpdate Error: ', e);
      });
    }
  }

  postChangePhoto(image) {
    var content = {
      Image: image,
      Text: 'Changed profile photo',
      isProfilePicture: 1
    };
    Send.post(content, this.props.loggedInUser()._id, () => {});
  }

  goToLocationSettings()
  {
    SystemAccessor.goTo("locationSettings", (error, result) =>
    {

    });
  }
}

module.exports = Profile;
