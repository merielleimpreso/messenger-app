import _ from 'underscore';
import Sound from 'react-native-sound';
import SystemAccessor from '../../modules/SystemAccessor';
import {Platform} from 'react-native';

let SoundEffects = {}

let sfx = {
  tick: {
    file: 'tick.mp3'
  },
  sent: {
    file: 'sent.mp3'
  }
};

SoundEffects.initAudio = () => {
  let keys = _.keys(sfx);

  for (var i = 0; i < keys.length; i++) {
    let file = sfx[keys[i]].file;
    let sound = new Sound(file, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('SoundEffects.js: failed to load the sound', error);
        return;
      }
    });
    sfx[keys[i]].sound = sound;
  }
}

SoundEffects.playSound = (sound) => {

    if(Platform.OS === 'android') {
      SystemAccessor.shouldPlaySound((err, shouldPlaySound)=> {

        if(shouldPlaySound == "true") {
          sfx[sound].sound.play((success) => {
            if (success) {
              console.log('SoundEffects.js: successfully finished playing');
            } else {
              console.log('SoundEffects.js: playback failed due to audio decoding errors');
            }
          });
        }
        else {
          console.log("SoundEffects.js: didn't play cause muted");
        }

      })

    }

}

module.exports = SoundEffects;
