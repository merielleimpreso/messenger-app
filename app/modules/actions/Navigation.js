import SoundEffects from './SoundEffects';

let Navigation = {}

Navigation.back = (navigator) => {
  SoundEffects.playSound('tick');
  requestAnimationFrame(() => {
    navigator.pop(0);
  });
}

Navigation.onPress = (onPress) => {
  SoundEffects.playSound('tick');
  onPress();
}

module.exports = Navigation;
