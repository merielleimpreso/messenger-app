/**
 * Author: Ida Jimenez
 * Date Created: 2016-07-11
 * Date Update: N/A
 * Description: Serves as the component for loading screen
 *
 */

import React, { Component } from 'react';
import { 
  Text, 
  View, 
  StyleSheet, 
  TouchableHighlightl,
  Image,
  StatusBar 
} from 'react-native';

let ProgressBar = require('ActivityIndicator');
let GLOBAL = require('./config/Globals.js');

class ScreenLoading extends React.Component {

  render() {
    var color = this.props.color;
    let refresh = (this.props.text == GLOBAL.TIMEOUT_MESSAGE) ?
      (<TouchableHighlight  onPress={() => this.props.refresh()}
        underlayColor={color.HIGHLIGHT}>
        <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>Reload</Text>
      </TouchableHighlight>) : 
      ((this.props.isLoading) ?
      <ProgressBar color={color.BUTTON} size="large"/> :
      <View />);

    let renderConnection = (this.props.hasOwnProperty('renderConnectionStatus')) ?
      (this.props.renderConnectionStatus()) :
      (<View/>)
 
    let content = (this.props.text == "start screen") ?
      ( 
        <Image style={{width: null, height: null, flex: 1}} resizeMode={'cover'}
          source={require('./../images/klikchat-startimage.jpg')} >
          <StatusBar backgroundColor={color.STATUS_BAR}/>
          {renderConnection}
        </Image>
      ) :
      (
        <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
          <View style={{justifyContent: 'center'}}>
            <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT, textAlign: 'center'}]}>{this.props.text}</Text>
          </View>
          {refresh}
        </View>
      );

    return content;
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  loaderMessage: {
    alignSelf: 'center',
    justifyContent: 'center'
  }
})
module.exports = ScreenLoading;
