/**
 * Author: Ida Jimenez
 * Date Created: 2016-04-29
 * Date Updated: N/A
 * Description: Used to Display Photo
 *
 * update: 2016-07-05 (ida)
 * - Removed underlayColor
 * update: 2016-08-16 (ida)
   - Implement photo viewer with zoom and swipe to pan
 */

'use strict';

import React, {
  Component,
} from 'react';

import {
  View,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  TouchableWithoutFeedback,
  Text,
  Linking,
  Platform,
  ToastAndroid,
  Image,
  BackAndroid
} from 'react-native';

var windowSize = Dimensions.get('window');
import RNFS from 'react-native-fs';
import GLOBAL from './config/Globals.js';
import Download from './config/db/Download';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import Gallery from 'react-native-gallery';
import Send from './config/db/Send';
import Toolbar from './common/Toolbar';

class ViewPhoto extends Component {
  // Initialize state
  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
      modalShareVisible: false,
      imageURL: [],
      actualHeight: 2400,
      actualWidth: 3607
    }
    this.download = this.download.bind(this);
  }

  componentWillMount(){
    this._isMounted = true;
    this.props.setIsSafeToBack(true);
    this.getImageGallery();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getImageGallery() {
    let imageGallery = this.props.imageGallery;
    let imageURL = [];
    let initialPage;

    for (var x = 0; x < imageGallery.length; x++) {
      imageURL.push(imageGallery[x].image);
      if (this.props.data.image == imageGallery[x].image) {
        initialPage = x;
      }
    }
    this.setState({
      imageURL: imageURL,
      initialPage: initialPage
    });
  }

  render(){
    let color = this.props.color
    var loader = this.state.loading
    ? <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)'}}>
        <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
        <Text style={{color:color.BUTTON, fontSize:10}}>Loading image...</Text>
        <Text style={{color:color.BUTTON}}>{this.state.progress}%</Text>
      </View>
    : null;
    return (
        <View style={styles.container}>
          {(Platform.OS === 'ios') ? <Toolbar title={'View Photo'} withBackButton={true} {...this.props}/> : null}
          <Gallery
            style={{flex: 1, backgroundColor: 'black'}}
            images={this.state.imageURL}
            initialPage={this.state.initialPage}
            pixels={{width: this.state.actualWidth, height: this.state.actualHeight}}
            onPageSelected={(page) => this.onPageSelected(page)}
            // onLoadStart={(e) => this.setState({loading: true})}
            // onLoad={() => this.setState({loading: false})}
          />

          {/*<View style={[styles.infoContainer, {backgroundColor: 'rgba(0,0,0,0.4)', top: 0}]}>
            <View style={{width: windowSize.width, flexDirection: 'row', justifyContent: 'flex-end'}}>
              <Text style={{color: color.BUTTON_TEXT, margin: 10}} >
                {this.state.selectedPage + 1}/{this.props.imageGallery.length}
              </Text>
              <KlikFonts name="imagegallery" size={23} style={{color: color.BUTTON_TEXT, margin: 10}} />
            </View>
          </View>*/}
          {/*<View style={[styles.infoContainer, {bottom: 35}]}>
            <View style={{width: windowSize.width, flexDirection: 'row', position: 'relative'}}>
              <KlikFonts name="info" size={23}
                style={{color: color.BUTTON_TEXT, margin: 10}}
                onPress={() => this.photoInfo()} />
            </View>
          </View>*/}
          <View style={[styles.infoContainer, {backgroundColor: 'rgba(0,0,0,0.9)', bottom: 0}]}>
            <View style={{width: windowSize.width, flexDirection: 'row', position: 'relative'}}>
              {/*<KlikFonts name="share" size={23} style={{color: color.BUTTON_TEXT, margin: 10}}
                onPress={() => this.setModalShareVisible(true)} />*/}
              <KlikFonts name="info" size={23} style={{color: color.BUTTON_TEXT, margin: 10}}
                onPress={() => this.photoInfo()} />
              <KlikFonts name="download" size={23}
                style={{color: color.BUTTON_TEXT, margin: 10, position: 'absolute', right: 10}}
                onPress={() => this.download()} />
            </View>
          </View>
          {loader}
          {this.renderShare()}
          
        </View>
    )
  }

  renderShare() {
    if (this._isMounted) {
      return (
        <Modal
          open={this.state.modalShareVisible}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          modalDidOpen={() => console.log('Modal opened')}
          modalDidClose={() => this.setModalShareVisible(false)}
          modalStyle={{
            borderRadius: 2,
            margin: 50,
            backgroundColor: '#F5F5F5'
          }}>
          <View style={{flex: 1, justifyContent: 'center', width: SCREEN.width * 0.7}}>
            <TouchableWithoutFeedback onPress={() => this.forwardToAnother()} >
              <View style={{padding: 10, flex: 1}} >
                <Text>Forward</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => this.shareOnTimeline()} >
              <View style={{padding: 10, flex: 1}} >
                <Text>Share on Timeline</Text>
              </View>
            </TouchableWithoutFeedback>
            {/*<TouchableWithoutFeedback onPress={() => this.saveToKeep()} >
              <View style={{padding: 10, flex: 1}} >
                <Text>Save to keep</Text>
              </View>
            </TouchableWithoutFeedback>*/}
          </View>
        </Modal>
      );
    }
  }

  onPageSelected(page) {
    let imageGallery = this.props.imageGallery;
    let photoURL = imageGallery[page].image;

    this.getImageSize(photoURL);
    this.setState({selectedPage: page})
  }

  // Compute the image size to be displayed.
  getImageSize(messagePhoto) {
    Image.getSize(messagePhoto, (actualWidth, actualHeight) => {
      this.setState({
        actualHeight: actualHeight * 4,
        actualWidth: actualWidth * 4
      });
    },
    (error) => {
      console.log('[PhotoInfo] getImageSize', error);
    });
  }

  setModalShareVisible(visible) {
    this.setState({modalShareVisible: visible});
  }

  // Not yet implemented
  forwardToAnother() {
    let imageGallery = this.props.imageGallery;
    this.props.navigator.push({
      name: "ShareMedia",
      image: imageGallery[this.state.selectedPage].message.media[0],
      loggedInUser: this.props.loggedInUser,
      color: this.props.color,
    });
  }

  // Not yet implemented
  shareOnTimeline() {
    // TODO: change option to post
    let imageGallery = this.props.imageGallery;
    let loggedInUserId = this.props.loggedInUser()._id;
    let content = {
      Stickers: imageGallery[this.state.selectedPage].message.media[0].url
    }
    Send.post(content, loggedInUserId);
  }

  saveToKeep() {
    this.download();
    this.setModalShareVisible(false);
  }

  photoInfo() {
    this.props.setIsSafeToBack(false);
    this.props.navigator.push({
      name: 'PhotoInfo',
      color: this.props.color,
      imageInfo: this.props.imageGallery[this.state.selectedPage]
    })
  }

  download() {
    let currentImage = this.props.imageGallery[this.state.selectedPage];
    let data = {
      messageId: currentImage._id,
      mediaURL: currentImage.image,
      otherUserId: currentImage.otherUserId
    }
    Download.downloadMediaFile(Download.mediaTypePath.image, data, false);
  }
};

// Styles for the view
var SCREEN = Dimensions.get("window");
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#000',
    position: 'relative'
  },
  photo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  infoContainer: {
    height: 35,
    position: 'absolute',
    justifyContent: 'center'
  }
});

module.exports = ViewPhoto;
