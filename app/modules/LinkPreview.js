'user strict';

import React, { Component } from 'react';

import {
  ActivityIndicator,
  Text,
  Image,
  View,
  Platform,
  Linking,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import SystemAccessor from './SystemAccessor';
import SoundEffects from './actions/SoundEffects';
class LinkPreview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:true,
      isLoaded:false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.getImageRatio(this.props.image);
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  getImageRatio(image){
    if (image) {
      if (this._isMounted) {
        Image.getSize(image,
          (actualWidth, actualHeight) => {
            this.setState({
              ratio: (actualWidth / actualHeight)
            });
          },(error)=>{
            this.setState({
              ratio: 0
            });
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getImageRatio(nextProps.image);
  }

  openUrl() {
    var url = this.props.link.data.url;
    console.log(this.props.link);
    if (!url.match(/^[a-zA-Z]+:\/\//)) {
      url = 'http://' + url;
    }
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        consoleLog('ChatRow: Cannot open URI: ' + url);
      }
    });
  }

  render() {
    return (
      <TouchableHighlight onPress={()=> {SoundEffects.playSound('tick'); this.openUrl()}} underlayColor='transparent'>
        {this.rowLayout()}
      </TouchableHighlight>
    );
  }

  rowLayout(){
    let color = this.props.color;
    let font = Platform.OS === 'ios' ? 'AppleSDGothicNeo-UltraLight' : 'sans-serif-light';
    return (
      <View style={{width: this.props.width}}>
        {(() => {
          if (this.props.link && this.props.link.data) {
            return (
            <View style={{flexDirection:'row'}}>
              {(() => {
                if (this.props.link.data.ogImage) {
                 return (
                  <Image style={{justifyContent:'center',alignItems:'center', height:this.props.height, width:this.props.height}}
                    source={{uri: this.props.link.data.ogImage[0].url}}/>
                 )
                }
              })()}
              <View style={{flex:1, justifyContent:'center', paddingLeft:5}}>
                <Text numberOfLines={2} lineBreakMode='tail' style={{color:'#555',fontWeight:'bold', fontSize:14}}>
                  {this.props.link.data.ogTitle}
                </Text>
                <Text numberOfLines={6} lineBreakMode='tail' style={{color:'#888', fontSize:10}}>
                  {this.props.link.data.ogDescription}
                </Text>
                <Text numberOfLines={1} lineBreakMode='tail' style={{color:'#888', fontFamily:font, fontSize:10}}>
                  {this.props.link.data.ogUrl.toUpperCase()}
                </Text>
              </View>
            </View>);
          }
        })()}
       </View>
    );
  }
}

module.exports = LinkPreview;

// ==== OLD METHODS ====
// rowLayout(){
//   var font = Platform.OS === 'ios' ? 'AppleSDGothicNeo-UltraLight' : 'sans-serif-light';
//   return (
//     <View style={{ flex:1, height: this.props.height,  width: this.props.width}}>
//       {(() => {
//         if (this.props.link && this.props.link.data) {
//           return (
//           <View shadowRadius={5} shadowColor='#777' shadowOpacity={0.5} shadowOffset={{width:5,height:5}} style={{backgroundColor:'#fff', flexDirection:'row' , width:this.props.width, height: this.props.height }}>
//             {(() => {
//               if (this.props.image && this.state.ratio) {
//                return  <Image style={{height: this.props.height, width: this.props.height}} source={{uri:this.props.image}}/>
//               }
//             })()}
//             <View style={{height: this.props.height, width:this.props.width-this.props.height, flex:1, justifyContent:'center'}}>
//               <Text numberOfLines={2} lineBreakMode='tail' style={{color:'#555',fontWeight:'bold', fontSize:14}}>
//                 {this.props.link.data.ogTitle}
//               </Text>
//               <Text numberOfLines={6} lineBreakMode='tail' style={{color:'#888', fontSize:10}}>
//                 {this.props.link.data.ogDescription}
//               </Text>
//               <Text numberOfLines={1} lineBreakMode='tail' style={{color:'#888', fontFamily:font, fontSize:10}}>
//                 {this.props.link.data.ogUrl.toUpperCase()}
//               </Text>
//             </View>
//           </View>);
//         }
//       })()}
//      </View>
//   );
// }
