'use strict';
import _ from 'underscore';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  InteractionManager,
  Image,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Dimensions,
  TextInput
} from 'react-native';
import {toNameCase, consoleLog, logTime, getMessage} from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import Users from './config/db/Users';
import DirectChat from './config/db/DirectChat';
import ScreenNoItemFound from './ScreenNoItemFound';
import ScreenLoading from './ScreenLoading';
import RenderModal from './common/RenderModal';
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main.js'));

class Recommendations extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2}),
      messages: null,
      isLoading: true,
      isOpenInvite: false,
      inviteDetails: null
    }
    this.renderContent = this.renderContent.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderInviteModal = this.renderInviteModal.bind(this);
    this.getDataToBeDisplayed = this.getDataToBeDisplayed.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
  }

  // Put a flag to check if the component is unmounted
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  // Put a flag to check if the component is unmounted
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Subscribe recent messages
  subscribe() {
    // console.log('Recommendations ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    if (this.props.hasInternet()) {
      DirectChat.subscribe().then(() => {
        this.observe();
      });
    } else {
      DirectChat.useCache(() => {
        this.observe();
      });
    }
  }

  observe() {
    let loggedInUser = this.props.loggedInUser();
    var data;

    DirectChat.observeInvitation(loggedInUser, (recentChat) => {
        if(recentChat.length > 0)
        {
          data = this.getDataToBeDisplayed(recentChat);
        }
        else {
          data = [];
        }

      if (JSON.stringify(recentChat) != JSON.stringify(this.state.messages)) {

        // console.log(recentChat)
        if (this._isMounted) {
          // console.log('Messages ['+ logTime() +']: Loaded ' + data.length + ' directchat items');

          this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(data),
            messages: data
          })
        }
      }
    });
  }

  // Helper method to get the data to be displayed
  getDataToBeDisplayed(recentChat) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var chatInfo = [];
    let count = 0;
    let loggedInUser = this.props.loggedInUser();
    for (var i = 0; i < recentChat.length; i++) {
      let r = recentChat[i];
      if (r.ToUserFID == loggedInUser._id) {
        count++;
        let user = Users.getItemById(r.FromUserFID);
        
        let name = toNameCase(user.profile.firstName) + ' ' + toNameCase(user.profile.lastName);
        let image = (user.profile.photoUrl) ? user.profile.photoUrl : GLOBAL.DEFAULT_IMAGE;
        let message = getMessage(user.profile.firstName, r.Message);
        let id = user._id;

        let details = {
          id: id,
          name: name,
          image: image,
          message: message
        }
        chatInfo.push(details);
      }
    }
    if (this._isMounted) {
      this.setState({count: count});
    }
    return chatInfo;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let count = (this.state.dataSource.getRowCount() > 0) ? 
          <Text style={[styles.pageTitle, {color: color.TEXT_LIGHT}]}>Friend Recommendations({this.state.count})</Text> : 
          <View />;
    if (this.state.isLoading) {
      return <ScreenLoading isLoading={true} text={''} color={color}/>;
    } else {
      return (
        <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
          {count}
          {this.renderContent()}
          {this.renderInviteModal()}
          {this.renderCheckConnection()}
        </View>
      );
    }
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (!this.state.isLoading && (this.state.dataSource.getRowCount() == 0)) {
      return <ScreenNoItemFound isLoading={false} text={'No Friend Recommendation'} color={color}/>;
    } else {
      return (
        <ListView ref='listview'
          removeClippedSubviews={true}
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          // renderFooter={this.renderFooter}
          // onEndReached={this.loadMore}
          initialListSize={10}
        />
      );
    }
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <TouchableHighlight onPress={() => this.setIsOpenInvite(data)} underlayColor={color.HIGHLIGHT}
        >
          <View style={[styles.contactsListViewRow]}>
            <Image source={{uri:data.image}} style={styles.listViewImage} />
            <View style={[styles.listViewRowContainer, {alignItems: 'center', marginLeft: 10, marginRight: 10, borderBottomWidth: 0.5, borderBottomColor: color.HIGHLIGHT, flexDirection: 'row'}]}>
              <View style={{flex: 1}}>
                <Text numberOfLines={1} style={[styles.listViewTitle, {color:color.TEXT_DARK, fontWeight:'500'}]}>{data.name}</Text>
                <Text numberOfLines={1} style={[styles.listViewTitle, {color:color.TEXT_LIGHT, fontSize:10}]}>{data.message}</Text>
              </View>
              <KlikFonts name="addcontact" color={color.TEXT_LIGHT} size={25}/>
            </View>
          </View>
      </TouchableHighlight>
    );
  }

  renderInviteModal() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this._isMounted) {
      let profileData = this.state.inviteDetails;
      let name = '';
      let image = GLOBAL.DEFAULT_IMAGE;
      let id = 0;
      if (profileData != null) {
        name = profileData.name;
        image = profileData.image;
        id = profileData.id;
      }
      return (
        <Modal open={this.state.isOpenInvite}
          closeOnTouchOutside={true}
          modalDidClose={() => this.setIsOpenInvite(null)}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 3,
            margin: 40,
            padding: 0,
            backgroundColor: color.CHAT_BG
          }}
        >
          <View style={{height: 350}}>
            <View style={{flex: 1}}>
              <Image source={require('./../images/backgroundImage/chatbg1.jpg')} style={{height: 150, width: null, position: 'relative'}} />
              <Image source={{uri: image}} style={{height: 80, width: 80, borderRadius: 50, alignSelf: 'center', marginTop: -40 }} />
              <Text style={{alignSelf: 'center', marginTop: 10, color: color.TEXT_DARK}}>{name}</Text>
            </View>
            <View style={{flexDirection: 'row', borderTopWidth: 0.5, borderTopColor: color.HIGHLIGHT}}>
              <TouchableHighlight underlayColor={color.HIGHLIGHT}
                onPress={() => this.acceptInvite(id)}
                style={{flex: 1, padding: 10}}>
                <View style={{alignItems: 'center'}}>
                  <KlikFonts name="addcontact" style={{fontSize: 35, color: color.TEXT_LIGHT}}/>
                  <Text style={{fontSize: 10, color: color.TEXT_LIGHT}}>Add Contact</Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
      )
    }
  }

  renderCheckConnection() {
    if (this.state.isShowCheckConnection) {
      return (
        <RenderModal 
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  setIsOpenInvite(data) {
    if (this.props.hasInternet()) {
      if (this._isMounted) {
        this.setState({
          isOpenInvite: (data == null) ? false : !this.state.isOpenInvite,
          inviteDetails: data
        });
      }
    } else {
      this.setIsShowCheckConnection();
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  acceptInvite(id) {
    ddp.call("acceptInvitation", [id])
    .then(()=> {
      console.log("acceptInvitation: successful");
      InteractionManager.runAfterInteractions(() => {
        this.subscribe();
        this.props.setIsContactsChanged(true);
        this.setIsOpenInvite(null)
        //this.props.addContactToLoggedInUser(id);
      });
    })
    .catch(function(error) {
      console.log(error);
    });
  }
}

module.exports = Recommendations;
