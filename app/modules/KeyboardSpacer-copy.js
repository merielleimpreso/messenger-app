/**
 * Created by andrewhurst on 10/5/15.
 */
// var React = require('react-native');
import React, {
  Component,
} from 'react';

// var {
//   DeviceEventEmitter,
//   View,
//   Platform
//   LayoutAnimation,
// } = React;

import {
  DeviceEventEmitter,
  View,
  Platform,
  LayoutAnimation
} from 'react-native';

// From: https://medium.com/man-moon/writing-modern-react-native-ui-e317ff956f02
const animations = {
    layout: {
        spring: {
            duration: 500,
            create: {
                duration: 300,
                type: LayoutAnimation.Types.easeInEaseOut,
                property: LayoutAnimation.Properties.opacity
            },
            update: {
                type: LayoutAnimation.Types.spring,
                springDamping: 200
            }
        },
        easeInEaseOut: {
            duration: 300,
            create: {
                type: LayoutAnimation.Types.easeInEaseOut,
                property: LayoutAnimation.Properties.scaleXY
            },
            update: {
                delay: 100,
                type: LayoutAnimation.Types.easeInEaseOut
            }
        }
    }
};

class KeyboardSpacer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            keyboardSpace: 0,
            isKeyboardOpened: false
        };

        this.updateKeyboardSpace = this.updateKeyboardSpace.bind(this);
        this.resetKeyboardSpace = this.resetKeyboardSpace.bind(this);
    }

    componentWillUpdate(props, state) {
        if (state.isKeyboardOpened !== this.state.isKeyboardOpened)
            LayoutAnimation.configureNext(animations.layout.spring);
    }

    updateKeyboardSpace(frames) {
        if (!frames.endCoordinates)
            return;

        var keyboardSpace = frames.endCoordinates.height + ('topSpacing' in this.props ? this.props.topSpacing : 0);
        this.setState({
            keyboardSpace: keyboardSpace,
            isKeyboardOpened: true
        }, () => ('onToggle' in this.props ? this.props.onToggle(true, keyboardSpace) : null));

        this.getKeyboardStatus();
    }

    resetKeyboardSpace() {
        this.setState({
            keyboardSpace: 0,
            isKeyboardOpened: false
        }, () => ('onToggle' in this.props ? this.props.onToggle(false, 0) : null));
        this.getKeyboardStatus();
    }

    componentDidMount() {
        if (Platform.OS == "android") {
            this._listeners = [
                DeviceEventEmitter.addListener('keyboardDidShow', this.updateKeyboardSpace),
                DeviceEventEmitter.addListener('keyboardDidHide', this.resetKeyboardSpace)
            ];
        } else {
            this._listeners = [
                DeviceEventEmitter.addListener('keyboardWillShow', this.updateKeyboardSpace),
                DeviceEventEmitter.addListener('keyboardWillHide', this.resetKeyboardSpace)
            ];
        }
        this.getKeyboardStatus();
    }

    componentWillUnmount() {
        this._listeners.forEach(function(/** EmitterSubscription */listener) {
            listener.remove();
        });
    }
    getKeyboardStatus(){
      if (this.props.getKeyboardStatus) {
          this.props.getKeyboardStatus(this.state);
      }
    }
    render() {
        return (<View style={[{height: this.state.keyboardSpace, left: 0, right: 0, bottom: 0}, this.props.style]}/>);
    }
}

module.exports = KeyboardSpacer;
