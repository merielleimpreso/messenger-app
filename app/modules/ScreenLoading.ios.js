/**
 * Author: Ida Jimenez
 * Date Created: 2016-07-11
 * Date Update: N/A
 * Description: Serves as the component for loading screen
 *
 */

import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native';

let ProgressBar = require('ActivityIndicator');
let GLOBAL = require('./config/Globals.js');

class ScreenLoading extends React.Component {

  render() {
    let color = this.props.color;
    let refresh = (this.props.isLoading) ?
      <ProgressBar styleAttr="Inverse" color={color.BUTTON} /> :
      ((this.props.loaderMessage == GLOBAL.TIMEOUT_MESSAGE) ?
        <TouchableHighlight  onPress={() => this.props.refresh()}
          underlayColor={color.HIGHLIGHT}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>Reload</Text>
        </TouchableHighlight> :
        <View/>);

    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        <View style={{justifyContent: 'center'}}>
          <Text style={[styles.loaderMessage, {textAlign: 'center', color: color.TEXT_LIGHT}]}>{this.props.loaderMessage}</Text>
        </View>
        {refresh}
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  loaderMessage: {
    alignSelf: 'center',
    justifyContent: 'center'
  }
})
module.exports = ScreenLoading;
