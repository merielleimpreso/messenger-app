import { Dimensions, StyleSheet } from 'react-native';
const windowSize = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  toolbar: {
    height: 50
  },
  card: {
    height: windowSize.height * 0.815,
  },
});