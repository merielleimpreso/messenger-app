import React, { Component } from 'react';
import {
  Platform,
  StatusBar,
  TabBarIOS,
  View,
} from 'react-native';
import Contacts from './../Contacts';
import FacebookTabBar from './../FacebookTabBar';
import GLOBAL from './../config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Messages from './../Messages';
import Options from './../Options';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import styles from './styles';
import Timeline from './../Timeline';
import Toolbar from './../common/Toolbar';

export default class TabsRender extends Component {
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let tabName = this.props.tabName;
    return (
      <View style={styles.container}>
        {(Platform.OS === 'android') ?
          <View>
            <StatusBar backgroundColor={color.STATUS_BAR}/>
            <Toolbar
              toolbarFrom={'Tabs'}
              title={tabName}
              {...this.props}
            />
          </View>
          : null
        }
        {this.renderTabs()}
      </View>
    );
  }

  renderTabs() {
    if (Platform.OS === 'android') {
      return this.renderTabsAndroid();
    }
    return this.renderTabsIos();
  }

  renderTabsIos() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <TabBarIOS
        itemPositioning="fill"
        tintColor={color.THEME}
        selectedTab={this.props.tabName}>
        <KlikFonts.TabBarItemIOS
          iconName="profile"
          title=""
          selected={this.props.tabName === 'Contacts'}
          onPress={() => this.props.onPressTabItem('Contacts')}
          >
          <Contacts
            identifyCurrentLocation={this.props.identifyCurrentLocation}
            isAutoLocate={this.props.isAutoLocate}
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            isShowSearchOnContacts={this.props.isShowSearchOnContacts}
            locationError={this.props.locationError}
            setShowSearchOnContacts={this.props.setShowSearchOnContacts}
            {...this.props}
          />
        </KlikFonts.TabBarItemIOS>
        <KlikFonts.TabBarItemIOS
          badge={this.props.unseen ? this.props.unseen : null}
          iconName="recent"
          title=""
          selected={this.props.tabName === 'Chat'}
          onPress={() => this.props.onPressTabItem('Chat')}>
          <Messages
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            setUnseen={this.props.setUnseen}
            {...this.props}
          />
        </KlikFonts.TabBarItemIOS>
        {/*<KlikFonts.TabBarItemIOS
          iconName="timeline"
          title=""
          selected={this.props.tabName === 'Timeline'}
          onPress={() => this.props.onPressTabItem('Timeline')}>
          <Timeline {...this.props} />
        </KlikFonts.TabBarItemIOS>*/}
        <KlikFonts.TabBarItemIOS
          iconName="options2"
          title=""
          selected={this.props.tabName === 'Options'}
          onPress={() => this.props.onPressTabItem('Options')}>
          <Options
            identifyCurrentLocation={this.props.identifyCurrentLocation}
            isAutoLocate={this.props.isAutoLocate}
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            locationError={this.props.locationError}
            setIsAutoLocate={this.props.setIsAutoLocate}
            setIsAutoSaveMedia={this.props.setIsAutoSaveMedia}
            {...this.props}
          />
        </KlikFonts.TabBarItemIOS>
      </TabBarIOS>
    );
  }

  renderTabsAndroid() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    return (
      <ScrollableTabView
        ref="scrollableTabView"
        initialPage={1}
        renderTabBar={() => <FacebookTabBar color={color} unseen={this.props.unseen} tabFor="MainTab" color={color} debugText={this.props.debugText} isDebug={this.props.isDebug}/>}
        onChangeTab={(i) => this.props.renderTabMenu(i)}
        style={{backgroundColor: color.CHAT_BG}}>

        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="profile" >
          {this.props.renderConnectionStatus()}
          <Contacts
            identifyCurrentLocation={this.props.identifyCurrentLocation}
            isAutoLocate={this.props.isAutoLocate}
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            isShowSearchOnContacts={this.props.isShowSearchOnContacts}
            locationError={this.props.locationError}
            setShowSearchOnContacts={this.props.setShowSearchOnContacts}
            {...this.props}
          />
        </View>
        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="recent" >
          {this.props.renderConnectionStatus()}
          <Messages
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            setUnseen={this.props.setUnseen}
            {...this.props}
          />
        </View>
        {/*<View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="timeline" >
          {this.props.renderConnectionStatus()}
          <Timeline
            {...this.props}
          />
        </View>*/}
        <View style={[styles.card, {backgroundColor: color.CHAT_BG}]} tabLabel="options2" >
          {this.props.renderConnectionStatus()}
          <Options
            identifyCurrentLocation={this.props.identifyCurrentLocation}
            isAutoLocate={this.props.isAutoLocate}
            isAutoSaveMedia={this.props.isAutoSaveMedia}
            locationError={this.props.locationError}
            setIsAutoLocate={this.props.setIsAutoLocate}
            setIsAutoSaveMedia={this.props.setIsAutoSaveMedia}
            {...this.props}
          />
        </View>
      </ScrollableTabView>
    );
  }
}
