import React, {
  Component
} from 'react';
import {
  LayoutAnimation,
  Alert,
  Platform,
  NativeModules
} from 'react-native';
import { logTime } from './../Helpers';
import Geocoder from 'react-native-geocoder';
import GLOBAL from './../config/Globals.js';
import SoundEffects from './../actions/SoundEffects';
import StoreCache from './../config/db/StoreCache';
import TabsRender from './TabsRender';
import Users from './../config/db/Users';
var SystemAccessor;

const TOOLBAR_ACTIONS = {
  Contacts: [
    {title: 'Search', iconName: 'search', show: 'always'},
    {title: 'Add Contact', iconName: 'addcontact', show: 'always'}
  ],
  Chat: [
    {title: 'New Message', show: 'never'},
    {title: 'Create Group', show: 'never'}
  ],
};

let subscribeContacts;
export default class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
        tabName: 'Chat',
        toolbarActions: TOOLBAR_ACTIONS.Chat,
        isShowSearchOnContacts: false,
        unseen: 0,
        isAutoLocate: false,
        isAutoSaveMedia: true,
        isLoggedIn: false,
        locationError: '',
        locality: '',
        storedFCMToken: false,
    }
    this.contactsSubscribe = this.contactsSubscribe.bind(this);
    this.isAutoSaveMedia = this.isAutoSaveMedia.bind(this);
    this.setSubscribeContacts = this.setSubscribeContacts.bind(this);
    this.subscribeContacts = this.subscribeContacts.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getCachedSettings();
    this.contactsSubscribe();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isLoggedIn() != this.state.isLoggedIn) {
      if (this._isMounted) {
        if (!this.state.storedFCMToken) {
          if (this.props.isLoggedIn()) {
            this.contactsSubscribe();
            ddp.call('storeFCMToken', [GLOBAL.FCM_USER_TOKEN]).then( result => {
              console.log('Tabs ['+ logTime() +']: Successfully stored FCM token');
            }).catch(error=>{
              console.log('Tabs ['+ logTime() +']: Failed to store FCM token', error);
            });
          }
          this.setState({
            isLoggedIn: this.props.isLoggedIn(),
            storedFCMToken: true
          });
        }
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    console.log("Tabs will unmount");
  }

  contactsSubscribe() {
    if (this.props.isLoggedIn()) {
      // Users.subscribeContacts().then(() => {
      //   console.log('Tabs: contacts subscribed');
      // }).catch((e) => console.log('Tabs: error', e));
    }
  }

  render() {
    return (
      <TabsRender
        goToAddMessage={this.goToAddMessage.bind(this)}
        goToCreateGroup={this.goToCreateGroup.bind(this)}
        isAutoLocate={this.isAutoLocate.bind(this)}
        isAutoSaveMedia={this.isAutoSaveMedia.bind(this)}
        identifyCurrentLocation={this.identifyCurrentLocation.bind(this)}
        isShowSearchOnContacts={this.isShowSearchOnContacts.bind(this)}
        locationError={this.locationError.bind(this)}
        onActionSelected={this.onActionSelected.bind(this)}
        onPressTabItem={this.onPressTabItem.bind(this)}
        renderTabMenu={this.renderTabMenu.bind(this)}
        setIsAutoLocate={this.setIsAutoLocate.bind(this)}
        setIsAutoSaveMedia={this.setIsAutoSaveMedia.bind(this)}
        setShowSearchOnContacts={this.setShowSearchOnContacts.bind(this)}
        setSubscribeContacts={this.setSubscribeContacts.bind(this)}
        setUnseen={this.setUnseen.bind(this)}
        subscribeContacts={this.subscribeContacts.bind(this)}
        tabName={this.state.tabName}
        toolbarActions={this.state.toolbarActions}
        unseen={this.state.unseen}
        {...this.props}
      />
    );
  }

  renderTabMenu(tabIndex) {
    if (this._isMounted) {
      if (tabIndex.i == 0) {
        this.setState({
          tabName: 'Contacts',
          toolbarActions: TOOLBAR_ACTIONS.Contacts
        });
      } else if (tabIndex.i == 1) {
        this.setState({
          tabName: 'Chat',
          toolbarActions: TOOLBAR_ACTIONS.Chat
        });
      } /*else if (tabIndex.i == 2) {
        this.setState({
          tabName: 'Timeline',
          toolbarActions: []
        });
      }*/ else if (tabIndex.i == 2) {
        this.setState({
          tabName: 'Options',
          toolbarActions: []
        });
      }
      SoundEffects.playSound('tick');
    }
  }

  onActionSelected(position) {
    SoundEffects.playSound('tick');
    if (this.state.tabName == 'Contacts') {
      if (position == 0) {
        this.setShowSearchOnContacts();   
      } else if (position == 1) {
        this.goToAddFriends();
      }
    } else if (this.state.tabName == 'Chat') {
      if (position == 0) {
        this.goToAddMessage();
      } else if (position == 1) {
        this.goToCreateGroup();
      }
    } else if (this.state.tabName == 'Options') {
      // if (position == 0) {
      //   //this.settings();
      // }
    }
  }

  setShowSearchOnContacts() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({isShowSearchOnContacts: !this.state.isShowSearchOnContacts})
    }
  }

  isShowSearchOnContacts() {
    return (Platform.OS == 'android') ? this.state.isShowSearchOnContacts : true;
  }

  goToCreateGroup() {
    let contacts = this.props.loggedInUser().contacts;
    if (contacts.length < 2) {
      Alert.alert("Oops!", "You need at least 2 friends to create a group.");
    } else {
      if (this.props.hasInternet()) {
        requestAnimationFrame(() => {
          this.props.navigator.push({
            name:'AddGroupChat',
            addContactToLoggedInUser: this.props.addContactToLoggedInUser,
            audio: this.props.audio,
            backgroundImageSrc: this.props.backgroundImageSrc,
            changeTheme: this.props.changeTheme,
            getMessageWallpaper: this.props.getMessageWallpaper,
            hasInternet: this.props.hasInternet,
            isAutoSaveMedia: this.isAutoSaveMedia,
            isLoggedIn: this.props.isLoggedIn,
            isSafeToBack: this.props.isSafeToBack,
            loggedInUser: this.props.loggedInUser,
            logout: this.props.logout,
            renderConnectionStatus: this.props.renderConnectionStatus,
            setAudio: this.props.setAudio,
            setIsViewVisible: this.props.setIsViewVisible,
            setIsSafeToBack: this.props.setIsSafeToBack,
            shouldReloadData: this.props.shouldReloadData,
            subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
            theme: this.props.theme,
            changeBgImage: this.props.changeBgImage,
            timeFormat: this.props.timeFormat,
            users: this.users,
          });
        });
      } else {
        Alert.alert("Oops!", "You're not connected to the internet.");
      }
    }
  }

  goToAddFriends() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'AddFriends') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          name:'AddFriends',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeBgImage: this.props.changeBgImage,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          hasNewFriendRequest: this.props.hasNewFriendRequest,
          isAutoSaveMedia: this.isAutoSaveMedia,
          isSafeToBack: this.props.isSafeToBack,
          isLoggedIn: this.props.isLoggedIn,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setLoggedInUser: this.props.setLoggedInUser,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          shouldReloadData: this.props.shouldReloadData,
          subscribeContacts: this.subscribeContacts,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          users: this.users,
        });
      });
    }
  }

  goToAddMessage() {
    let navigatorStack = this.props.navigator.getCurrentRoutes();
    let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

    if (navigatorStack[currentRoute].name !== 'FriendList') {
      if (this.props.hasInternet()) {
        let contacts = this.props.loggedInUser().contacts;
        if (contacts.length > 0) {
          requestAnimationFrame(() => {
            this.props.navigator.push({
              name: 'FriendList',
              action: 'sendNewMessage',
              addContactToLoggedInUser: this.props.addContactToLoggedInUser,
              audio: this.props.audio,
              backgroundImageSrc: this.props.backgroundImageSrc,
              changeBgImage: this.props.changeBgImage,
              changeTheme: this.props.changeTheme,
              getMessageWallpaper: this.props.getMessageWallpaper,
              hasInternet: this.props.hasInternet,
              isAutoSaveMedia: this.isAutoSaveMedia,
              isLoggedIn: this.props.isLoggedIn,
              isSafeToBack: this.props.isSafeToBack,
              loggedInUser: this.props.loggedInUser,
              logout: this.props.logout,
              renderConnectionStatus: this.props.renderConnectionStatus,
              setAudio: this.props.setAudio,
              setIsViewVisible: this.props.setIsViewVisible,
              setIsSafeToBack: this.props.setIsSafeToBack,
              shouldReloadData: this.props.shouldReloadData,
              subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
              theme: this.props.theme,
              timeFormat: this.props.timeFormat,
            })
          });
        } else {
          Alert.alert("Oops!", "You have no friend yet to send a message.");
        }
      } else {
        Alert.alert("Oops!", "You're not connected to the internet.");
      }
    }
  }

  setSubscribeContacts(func) {
    subscribeContacts = func;
  }

  subscribeContacts() {
    subscribeContacts();
  }

  setUnseen(unseen) {
    if (this._isMounted) {
      this.setState({unseen: unseen});
    }
  }

  setIsAutoLocate(isAutoLocate) {
    if (this._isMounted) {
      this.setState({isAutoLocate: isAutoLocate});
      // if (isAutoLocate) {
        this.identifyCurrentLocation(true);
      // }
    }
  }

  isAutoLocate() {
    return this.state.isAutoLocate;
  }

  setIsAutoSaveMedia(isAutoSaveMedia) {
    if (this._isMounted) {
      this.setState({isAutoSaveMedia: isAutoSaveMedia});
    }
  }

  isAutoSaveMedia() {
    return this.state.isAutoSaveMedia;
  }

  locationError() {
    return this.state.locationError;
  }

  getCachedSettings() {
    StoreCache.getCache(StoreCache.keys.isAutoLocate, (result) => {
      result = (result == undefined) ? false : JSON.parse(result);
      if (this._isMounted) {
        this.setState({isAutoLocate: result});
      }

      if(Platform.OS == 'android') {
          this.identifyCurrentLocation(false);
      }

      console.log('Tabs: StoreCache isAutoLocate', result);
    });
    StoreCache.getCache(StoreCache.keys.isAutoSaveMedia, (result) => {
      result = (result == undefined) ? false : JSON.parse(result);
      if (this._isMounted) {
        this.setState({isAutoSaveMedia: result});
      }
      console.log('Tabs: StoreCache isAutoSaveMedia', result);
    });
  }

  identifyCurrentLocation(isTurnedOn) {
    let id = navigator.geolocation.getCurrentPosition((position) => {
      Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(res => {
        console.log('Tabs: Successfully identified current location.');
        this.updateMyLocationToServer(position, res[0].locality)
      }).catch((error) => {
        console.log('Tabs: Failed to identify current location.');
      });
    },(error) => {
      if (this.state.isAutoLocate) {
        if (this._isMounted) {
          this.setState({locationError: error});
        }
        if (error == 'Location request timed out') {
          console.log('Tabs: getCurrentPosition timeout.');
          this.identifyCurrentLocation(false);
        } else if (isTurnedOn && error == 'No available location provider.') {
          this.setIsAutoLocate(false);
        }
      }
    },{timeout: 10000, maximumAge: 1000});
  }

  updateMyLocationToServer(position, locality) {
    var location = {latitude: position.coords.latitude, longitude: position.coords.longitude, locality: locality};

    if (this.props.hasInternet()) {
      setTimeout(() => {
        ddp.call("updateUserLocation", [location]).then(() => {
          console.log('Tabs: Successfully updated user location.');

          setTimeout(() => {
            if (this.state.isAutoLocate && Platform.OS == 'android') {
              this.identifyCurrentLocation(false);
            }
          }, 900000);
        }).catch((e) => {
          console.log('Tabs: Failed to update user location.', e);
          // this.updateMyLocationToServer(position, locality);
        });
      }, 2000);
    }
  }

  onPressTabItem(tab) {
    if (this._isMounted) {
      if (tab === this.state.tabName) return;
      this.setState({
        tabName: tab,
        toolbarActions: TOOLBAR_ACTIONS[tab]
        // isShowSearchOnContacts: this.state.searchText ? true : false
      });
    }
  }
}
