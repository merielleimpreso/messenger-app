'use strict';

import React, {
  Component,
} from 'react';
import ReactNative, {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import _ from 'underscore';
import {dateToTime, getMessage, getMessageBadge, logTime, renderFooter, toNameCase} from './Helpers';
import BEMCheckBox from 'react-native-bem-check-box';
import Group from './config/db/Group';
import GroupNew from './GroupNew';
import GroupChat from './config/db/GroupChat';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import ScreenNoItemFound from './ScreenNoItemFound';
import SearchBar from './SearchBar';
import Users from './config/db/Users';
import UserGroups from './config/db/UserGroups';

const ADD_TO_LIMIT = 10;
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common'));
const windowSize = Dimensions.get('window');

class Groups extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      chosenContacts: [],
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      dataSourceForSearch: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isCreatingGroup: false,
      isOpenGroupNew: false,
      isFirstLoad: true,
      isLoading: true,
      groupName: '',
      limit: 10,
      messages: null,
      searchUsers: null,
      shouldReloadData: this.props.shouldReloadData(),
      theme: this.props.theme()
    }
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.renderGroupNew = this.renderGroupNew.bind(this);
    this.onPressButtonPlus = this.onPressButtonPlus.bind(this);
    this.renderRowForSearch = this.renderRowForSearch.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.createGroup = this.createGroup.bind(this);
  }

  // Put a flag to check if the component is mounted
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.subscribeToGroup();
    });
  }

  // Re-subscribe if limit or shouldReloadData values are updated
  componentDidUpdate(prevProps, prevState) {
    if (this.state.limit != prevState.limit) {
      InteractionManager.runAfterInteractions(() => {
        this.subscribeToGroup();
      });
    }

    if (this.props.theme() != this.state.theme) {
      this.state.dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,});
      let data = this.getDataToBeDisplayed(this.state.messages);
      if (this._isMounted) {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(data),
          theme: this.props.theme()
        });
      }
    }

    if (this.props.shouldReloadData() != this.state.shouldReloadData) {
      if (this._isMounted) {
        this.setState({
          shouldReloadData: this.props.shouldReloadData()
        })
      }
      if (this.props.shouldReloadData()) {
        console.log('Groups ['+ logTime() +']: Reloading...');
        InteractionManager.runAfterInteractions(() => {
          this.subscribeToGroup();

          if (this.state.messages) {
            if (this._isMounted) {
              this.setState({
                limit: this.state.messages.length + ADD_TO_LIMIT
              });
            }
          }
        });
      }
    }

    if (prevState.messages != this.state.messages) {
      if (this.state.isFirstLoad) {
        this.state.isFirstLoad = false;
        if (this.state.limit < this.state.messages.length) {
          if (this._isMounted) {
            this.setState({
              isFirstLoad: false,
              limit: this.state.messages.length
            });
          }
        }
      }
    }
  }

  // Put a flag to check if the component is unmounted
  componentWillUnmount() {
    this._isMounted = false;
  }

  subscribeToGroup() {
    if (this.props.hasInternet()) {
      Group.subscribe().then(() => {
        Group.getAllItems();
        this.subscribeToUserGroups();
      });
    } else {
      Group.useCache(() => {
        this.subscribeToUserGroups();
      });
    }
  }

  subscribeToUserGroups() {
    if (this.props.hasInternet()) {
      UserGroups.subscribe().then(() => {
        UserGroups.getAllItems();
        this.subscribeToGroupChat();
      });
    } else {
      UserGroups.useCache(() => {
        this.subscribeToGroupChat();
      })
    }
  }

  subscribeToGroupChat() {
    console.log('Groups ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    if (this.props.hasInternet()) {
      GroupChat.subscribe().then(() => {
        this.observe();
      });
    } else {
      GroupChat.useCache(() => {
        this.observe();
      });
    }
  }

  observe() {
    GroupChat.observeRecent((recentChat) => {
      if (JSON.stringify(recentChat) != JSON.stringify(this.state.messages)) {
        let data = this.getDataToBeDisplayed(recentChat);
        if (this._isMounted) {
          console.log('Groups ['+ logTime() +']: Loaded ' + data.length + ' groupchat items');
          this.setState({
            isLoading: false,
            dataSource: this.state.dataSource.cloneWithRows(data),
            messages: recentChat
          })
        }
      }
    });
  }

  getDataToBeDisplayed(recentChat) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.props.loggedInUser();
    let recentChatInfo = [];

    if (loggedInUser) {
      for (var i = 0; i < recentChat.length; i++) {
        let group = Group.getItemById(recentChat[i].GroupFID);

        if (group) {
          let r = recentChat[i];
          let user = Users.getItemById(r.FromUserFID);

          let name = group.Name;
          let image = GLOBAL.DEFAULT_IMAGE;
          let createDate = r.CreateDate;
          let nameSender = (loggedInUser._id == r.FromUserFID) ? 'You' : user.profile.firstName;
          let message = getMessage(nameSender, r.Message);
          let loggedInUserIsSender = (loggedInUser._id == r.FromUserFID);
          let isSeen = this.isSeen(loggedInUser._id, r.Seen);
          let badge = getMessageBadge(loggedInUserIsSender, isSeen, color);
          let messageId = r._id;

          let recent = {
            group: group,
            name: name,
            image: image,
            createDate: createDate,
            message: message,
            isSeen: isSeen,
            loggedInUserIsSender: loggedInUserIsSender,
            badge: badge,
            messageId: messageId
          }
          recentChatInfo.push(recent);
        }
      }
    }

    return recentChatInfo;
  }

  isSeen(loggedInUserId, seenUsers) {
    var seen = false;
    _.each(_.values(seenUsers), function(s) {
      if (loggedInUserId == s.userId) {
        seen = true;
      }
    });
    return seen;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
        tintColor={color.THEME}
        title={{ title: 'Groups', tintColor: color.BUTTON_TEXT}}
        rightButton={
           <KlikFonts
           name="plus"
           color={color.BUTTON_TEXT}
           size={35}
           onPress={() => this.onPressButtonPlus()}/>
        }/>
        {this.props.renderConnectionStatus()}
        {this.renderContent()}
        {this.renderGroupNew()}
        {this.renderCreatingGroup()}
        <View style={{height: 50}} />
      </View>
    );
  }

  // Render the main content
  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (!this.state.isLoading && this.state.dataSource.getRowCount() == 0) {
      return <ScreenNoItemFound isLoading={false} text={'No messages'} color={color}/>;
    } else {
      return (
        <ListView ref='listview'
          removeClippedSubviews={true}
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          renderFooter={this.renderFooter}
          onEndReached={this.loadMore}
          initialListSize={10}
        />
      );
    }
  }

  // Display data in the row
  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var textStyle = (!data.loggedInUserIsSender && !data.isSeen) ? styles.chatRecentTextNotSeen : styles.chatRecentText;

    return (
      <TouchableHighlight key={data.messageId} onPress={() => this.onPressedRow(data)} underlayColor={color.HIGHLIGHT}>
        <View>
          <View style={styles.chatRecentRow}>
            <Image source={{uri:data.image}} style={styles.chatRecentImage} />
            <View style={styles.chatRecentDetailsContainer}>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[styles.chatRecentTextBold, {color:color.TEXT_DARK, flex:4}]}>{data.name}</Text>
                <View style={{flexDirection:'column', alignItems:'flex-end'}}>
                  <Text numberOfLines={1} style={[styles.chatRecentText, {color:color.THEME}]}>{dateToTime(data.createDate)}</Text>
                </View>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text numberOfLines={1} style={[textStyle, {color:color.TEXT_LIGHT,flex:4}]}>{data.message}</Text>
                <View style={{flexDirection:'column', alignItems:'flex-end',flex:1}}>{data.badge}</View>
              </View>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  // Render footer display
  renderFooter() {
    let isAllLoaded = this.state.isAllLoaded;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let hasInternet = this.props.hasInternet()
    return renderFooter(isAllLoaded, color, hasInternet);
  }

  // Check if there are recent messages need to be loaded.
  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('Groups ['+ logTime() +']: Checking if there are more messages to load...');
          ddp.call('shouldLoadGroupChatRecent', [this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('Groups ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  // When row was pressed
  onPressedRow(data) {
    console.log('Groups ['+ logTime() +']: Row is pressed, open messages with ' + data.name);
    let usersIds = _.pluck(UserGroups.getItemByGroupId(data.group._id), 'UserFID');
    var users = [];
    for (var i=0; i<usersIds.length; i++) {
      users.push(Users.getItemById(usersIds[i]));
    }
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'groupmessages',
        renderConnectionStatus: this.props.renderConnectionStatus,
        group: data.group,
        users: users,
        loggedInUser: this.props.loggedInUser,
        hasInternet: this.props.hasInternet,
        shouldReloadData: this.props.shouldReloadData,
        theme: this.props.theme,
        changeBgImage: this.props.changeBgImage,
        bgImage: this.props.bgImage,
        setImage: this.props.setImage
      });
    });
  }

  onPressButtonPlus() {
    if (this._isMounted) {
      this.setState({
        isOpenGroupNew: true
      });
    }
    let searchUsers = this.getUsers();
    if (JSON.stringify(searchUsers) != JSON.stringify(this.state.searchUsers)) {
      if (this._isMounted) {
        this.setState({
          dataSourceForSearch: this.state.dataSourceForSearch.cloneWithRows(searchUsers),
          searchUsers: searchUsers
        });
      }
    }
  }

  renderGroupNew() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Modal open={this.state.isOpenGroupNew}
       closeOnTouchOutside={false}
       containerStyle={{
         justifyContent: 'center'
       }}
       modalStyle={{
         borderRadius: 2,
         padding:-5,
       }}>

       <View style={[styles.groupModalTitle, {backgroundColor:color.THEME}]}>
        <KlikFonts name='close' color={color.BUTTON_TEXT} size={30} onPress={() => this.onPressedModalX()} />
        <Text style={[styles.groupModalTitleText, {color:color.BUTTON_TEXT}]}>New Group</Text>
        <KlikFonts name='check' color={color.BUTTON_TEXT} size={30} onPress={() => this.onPressedModalCheck()} />
       </View>

       <View style={{flexDirection:'row', marginTop:10, alignItems:'stretch'}}>
        <KlikFonts name="camera" color={color.THEME} size={50} style={{paddingLeft:5, paddingRight:5}}/>
        <TextInput ref="groupInput"
          placeholder='Name this group chat'
          multiline={false}
          autoFocus={true}
          placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
          selectionColor={color.THEME}
          style={{flex:1,fontSize:14}}
          onChangeText={(text) => this.setState({
            groupName: text
          })}
        />
       </View>

       <SearchBar
        theme={this.props.theme}
        onSearchChange={this.onSearchChange}
        isLoading={false}
        placeholder='Add people to the group'
        onFocus={() =>
         this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
       />

       <View style={{height: 280}}>
         <ListView ref='listview'
           removeClippedSubviews={true}
           automaticallyAdjustContentInsets={false}
           dataSource={this.state.dataSourceForSearch}
           renderRow={this.renderRowForSearch}
           initialListSize={10}
         />
       </View>

      </Modal>
    );
  }

  renderCreatingGroup() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <Modal open={this.state.isCreatingGroup}
       closeOnTouchOutside={false}
       containerStyle={{
         justifyContent: 'center'
       }}
       modalStyle={{
         borderRadius: 2,
         margin: 20,
         padding: 10,
       }}
      >
       <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
       <Text style={{textAlign: 'center', color: color.BUTTON}}>Creating new group...</Text>
      </Modal>
    );
  }

  // Helper method to get all or searched users
  getUsers(searchText) {
    let allContacts = this.props.loggedInUser().contacts;
    var users = [];
    for (var i = 0; i < allContacts.length; i++) {
      let user = Users.getItemById(allContacts[i]);
      users.push(user);
    }

    if (searchText == '' || searchText == null) {
      return users;
    } else {
      let searchUsers = _.filter(users, function(user){
        return (user.profile.firstName + " " + user.profile.lastName).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
      });
      return searchUsers;
    }
  }

  renderRowForSearch(contact) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let name = toNameCase(contact.profile.firstName) + ' ' + toNameCase(contact.profile.lastName);
    let image = (contact.profile.photoUrl != null || contact.profile.photoUrl != '') ? contact.profile.photoUrl : GLOBAL.DEFAULT_IMAGE;
    var result = _.where(this.state.chosenContacts, {_id: contact._id});
    var checked = result[0] ? true : false;
    return (
      <View style={styles.groupNewContactContainer}>
        <BEMCheckBox style={{height:20,width:20, borderRadius:0}}
          animationDuration={0.3}
          onCheckColor={color.BUTTON_TEXT}
          value={checked}
          onFillColor={color.BUTTON}
          onAnimationType='fill'
          offAnimationType='flat'
          lineWidth={1}
          onValueChange={value => this.chooseContact(contact,value)}
          boxType="square"
          tintColor={color.BUTTON}
          onTintColor={color.BUTTON}
         />
        <View style={{flexDirection:'row', alignItems:'center'}}>
           <Image source={{uri: image}} style={styles.groupNewContactImage}></Image>
           <Text numberOfLines={1} style={[styles.groupNewContactName, {color:color.THEME}]} >{name}</Text>
        </View>
     </View>
    );
  }

  // Function in choosing a contact
  chooseContact(contact, value){
    if (this._isMounted) {
      if (value) {
        var array = this.state.chosenContacts;
        // array.push(contact._id);
        array.push(contact);
        this.setState({
          chosenContacts: array,
        });
      }
      else {
        var array = this.state.chosenContacts;
        var contact = _.where(array, {_id: contact._id});
        var  array = _.without(array, contact[0]);
        this.setState({
          chosenContacts: array,
        });
      }
    }
  }

  // On search change
  onSearchChange(searchText) {
    this.setState({
      searchText: searchText,
    });
    let searchUsers = this.getUsers(searchText);
    if (JSON.stringify(searchUsers) != JSON.stringify(this.state.searchUsers)) {
      if (this._isMounted) {
        this.setState({
          dataSourceForSearch: this.state.dataSourceForSearch.cloneWithRows(searchUsers),
          searchUsers: searchUsers
        });
      }
    }
  }

  onPressedModalX() {
    if (this._isMounted) {
      this.setState({
        isOpenGroupNew: false
      });
    }
  }

  onPressedModalCheck() {
    let groupName = this.state.groupName;
    if (groupName == '' || groupName == null) {
      Alert.alert(GLOBAL.ALERT_CREATE_GROUP_ERROR_TITLE, GLOBAL.ALERT_CREATE_GROUP_ERROR_DETAIL_NAME, [{text: 'OK'}]);
    } else {
      if (this.state.chosenContacts.length > 1) {
        this.createGroup();
      } else {
        Alert.alert(GLOBAL.ALERT_CREATE_GROUP_ERROR_TITLE, GLOBAL.ALERT_CREATE_GROUP_ERROR_DETAIL, [{text: 'OK'}]);
      }
    }
  }

  createGroup() {
    if (this._isMounted) {
      this.setState({
        isOpenGroupNew: false,
        isCreatingGroup: true
      });
    }
    let groupName = this.state.groupName;
    let chosenContactsIds = _.pluck(this.state.chosenContacts, '_id');
    chosenContactsIds.push(this.props.loggedInUser()._id);
    ddp.call('createGroup', [groupName, chosenContactsIds])
    .then(group => {
      if (this._isMounted) {
        this.setState({
          isCreatingGroup: false
        });
      }
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id:'groupmessages',
          renderConnectionStatus: this.props.renderConnectionStatus,
          group: group,
          users: UserGroups.getItemByGroupId(group._id),
          loggedInUser: this.props.loggedInUser,
          hasInternet: this.props.hasInternet,
          shouldReloadData: this.props.shouldReloadData,
          theme: this.props.theme,
        });
      });
    });
  }
}

// Export module.
module.exports = Groups;



// /*
//
// Author: Merielle Impreso
// Date Created: 2016-04-15
// Date Updated: 2016-05-16
// Description: This file displays the groupchat items from the database.
//   Each row display is handled by ChatRecentRenderRow.
//   It passes the following to ChatRecentRenderRow as props:
//     data = recent chat item
//     data.chatType = GLOBAL.CHAT.GLOBAL;
//     data.user = logged in user
// Used In: Tabs.ios
//
// Changelog:
// update: 2016-04-21 (by Merielle I.)
//   - used latest ddp-client
//   - added GroupChat for database subscription and observe
// update: 2016-04-22 (by Merielle I.)
//   - added isMounted flag to fix setState warnings
// update: 2016-04-26 (by John Rezan B.)
//   - changed default value of hasNoData to null
//   - changed render of NoMessages component to "No group messages"
//   - removed progressbar, make use of ActivityIndicator. Set loading to true when
//     data is still to be fetched
//   - changed condition in getDataSource method
// update: 2016-05-06 (by Merielle I.)
//   - implement color and UI scheme
//   - added loading view after all method calls is returned
//   - added ScreenLoading module
// update: 2016-05-16 (by Merielle I.)
//   - remove ddp.initialize
// update: 2016-05-30 (by Merielle I.)
//   - call initialize function from Tabs when ddp disconnected
// update: 2016-06-09 (by Merielle I.)
//   - added color theme
//   - added color props in ChatRecentRenderRow
// update: 2016-06-29 (by Merielle I.)
//   - added stopping of observer in componentWillUnmount
// update: 2016-07-05 (by Merielle I.)
//   - improve subscribeRecent implementation
//
// */
//
// 'use strict';
//
// import _ from 'underscore';
// import ChatRecentRenderRow from './ChatRecentRenderRow';
// import GroupChat from './config/db/GroupChat';
// import KlikFonts from 'react-native-vector-icons/KlikFonts';
// import NavigationBar from 'react-native-navbar';
// import SearchBar from './SearchBar.ios';
// import ScreenFailedToConnect from './ScreenFailedToConnect';
// import ScreenLoading from './ScreenLoading';
// import ScreenNoItemFound from './ScreenNoItemFound';
// import TimerMixin from 'react-timer-mixin';
//
// import React, {
//   Component,
// } from 'react';
//
// import {
//   AsyncStorage,
//   ListView,
//   StyleSheet,
//   Text,
//   View,
// } from 'react-native';
//
// var styles = StyleSheet.create(require('./../styles/styles_common.js'));
// var GLOBAL = require('./config/Globals.js');
//
// var Groups = React.createClass({
//   mixins: [TimerMixin],
//
//   // Get initial state
//   getInitialState: function(){
//     return {
//       chat: {},
//       dataSource: new ListView.DataSource({
//           rowHasChanged: (row1, row2) => row1 !== row2,
//       }),
//       hasNoData: null,
//       canCallMethod: true,
//       canLoad: true,
//       isLoading: true,
//       loadedRowsCount: 0,
//       observer: {},
//       user: {},
//     }
//   },
//
//   /*
//   Put a flag when component is mounted.
//   Get the logged in user.
//   */
//   componentDidMount: function() {
//     this.isMounted = true;
//     this.getCurrentUser();
//   },
//
//   // Put a flag when component will unmount.
//   componentWillUnmount: function() {
//     this.isMounted = false;
//     this.state.observer.stop;
//   },
//
//   // Get the logged in user and make a subscription.
//   getCurrentUser: function() {
//     this.handleLoadingTimeout();
//     AsyncStorage.getItem('currentUser')
//     .then(result => {
//       if (this.isMounted) {
//         var user = JSON.parse(result);
//         this.setState({ user: user });
//         this.makeSubscription();
//       }
//     });
//   },
//
//   // Subscribe and observe the groupchat collections.
//   makeSubscription: function() {
//     GroupChat.subscribeRecent()
//     .then(() => {
//       var observer = GroupChat.observeRecent((results) => {
//         if (this.isMounted) {
//           AsyncStorage.setItem('groupMessages', JSON.stringify(results));
//           this.setState({
//             chat: results,
//             isLoading: false
//           });
//         }
//       })
//       if (this.isMounted) {
//         this.setState({observer: observer});
//       };
//     })
//     .catch(error => {
//       console.log(error);
//       this.props.initialize();
//     });
//   },
//
//   // Handle loading timeout
//   handleLoadingTimeout() {
//     setTimeout(() => {
//       if (this.state.isLoading) {
//         if (this.isMounted) {
//           this.setState({canLoad: false});
//         }
//       }
//     }, GLOBAL.TIMEOUT);
//   },
//
//   // ListView datasource.
//   getDataSource(): ListView.DataSource {
//     var user = this.state.user;
//     var chat = this.state.chat;
//     var index = 0;
//     _.each(_.values(chat), function(c) {
//       c['chatType'] = GLOBAL.CHAT.GROUP;
//       c['user'] = user;
//       c['index'] = index + 1;
//       index = index + 1;
//     });
//     return this.state.dataSource.cloneWithRows(chat);
//   },
//
//   // Display the list of recent chat.
//   render: function() {
//     var dataSource = this.getDataSource();
//     var content = <View />;
//
//     if (this.state.isLoading) {
//       content = (this.state.canLoad)
//         ? <ScreenLoading isLoading={true} text={'Loading your group messages...'} color={this.props.color} />
//         : <ScreenLoading isLoading={true} text={GLOBAL.TEXT_SERVER_TAKING_A_WHILE} color={this.props.color} />;
//     } else {
//       //var isLoadingRows = (this.state.loadedRowsCount != this.state.chat.length);
//       var isLoadingRows = false;
//       var listViewStyle = (isLoadingRows) ? styles.hidden : styles.listView;
//       content = (dataSource.getRowCount() == 0) ?
//         <ScreenNoItemFound isLoading={false} text={'No messages'} color={this.props.color} /> :
//         <ListView
//           automaticallyAdjustContentInsets={false}
//           ref='listview'
//           dataSource={dataSource}
//           renderRow={this.renderRow}
//           style={listViewStyle}
//           initialListSize={1}/>
//     }
//
//     var color = this.props.color;
//     return (
//       <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
//         <NavigationBar
//           tintColor={color.THEME}
//           title={{ title: 'Groups', tintColor: color.BUTTON_TEXT}}
//           rightButton={
//             <KlikFonts
//             name="plus"
//             color={color.BUTTON_TEXT}
//             size={35}
//             onPress={() => this.goToGroupNew()}
//            />
//           }
//         />
//         {content}
//       </View>
//     );
//   },
//
//   // Go to GroupNew page
//   goToGroupNew() {
//     this.requestAnimationFrame(() => {
//       this.props.navigator.push({
//         id:'groupnew',
//         title: 'Messenger',
//         color: this.props.color
//       })
//     });
//   },
//
//   // Pass this function to ChatRenderRow to check if all rows is loaded
//   setIsLoadedRowsCount() {
//     if (this.state.loadedRowsCount < this.state.chat.length) {
//       this.state.loadedRowsCount++;
//     }
//     if (this.state.loadedRowsCount == this.state.chat.length) {
//       this.setState({loadedRowsCount: this.state.loadedRowsCount});
//     }
//   },
//
//   // Display the info for each row, pass the data to ChatRecentRenderRow.
//   renderRow(data) {
//     return <ChatRecentRenderRow data={data}
//       navigator={this.props.navigator}
//       setIsLoadedRowsCount={this.setIsLoadedRowsCount}
//       color={this.props.color} />;
//   },
//
// });
//
// //styles for groups
// module.exports = Groups;
