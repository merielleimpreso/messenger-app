'use strict';

import React, {
  Component
} from 'react';
import ReactNative, {
  Alert,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  InteractionManager
} from 'react-native';
import {logTime} from './Helpers';
import FileReader from 'react-native-fs';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Send from './config/db/Send';
const DocumentPicker = require('react-native').NativeModules.RNDocumentPicker;
const ImagePickerManager = require('NativeModules').ImagePickerManager;
const styles = StyleSheet.create(require('./../styles/styles_common'));

class InputOptions extends Component
{

  constructor(props) {
    super(props);
    this.onPressContact = this.onPressContact.bind(this);
    this.sendContact = this.sendContact.bind(this);
  }

  render() {
    let color = this.props.color;
    let height = (this.props.height > 0) ? this.props.height : 220;
    return (
      <View style={{height:height, paddingTop:15}}>
        <View style={{flexDirection:'row', flex:1}}>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.onPressButtonGallery()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
              <View>
                <KlikFonts name="gallery" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                  GALLERY
                </Text>
              </View>
            </TouchableHighlight>
          </View>
          <View style={{width:1.2, backgroundColor: color.THEME,}}/>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.onPressTakePhoto()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                <View>
                  <KlikFonts name="photo" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                    TAKE PHOTO
                  </Text>
                </View>
            </TouchableHighlight>
          </View>
          <View style={{width:1.2, backgroundColor: color.THEME,}}/>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.onPressTakeVideo()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                <View>
                  <KlikFonts name="video" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                    TAKE A VIDEO
                  </Text>
                </View>
            </TouchableHighlight>
          </View>
        </View>
        <View style={{height:1.2, backgroundColor: color.THEME, marginLeft:15, marginRight:15, flexDirection:'row',}}/>
        <View style={{flexDirection:'row', flex:1, paddingBottom:15}}>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.props.onPressButtonRecord()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                <View>
                  <KlikFonts name="audio" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                    VOICE
                  </Text>
                </View>
            </TouchableHighlight>
          </View>
          <View style={{width:1.2, backgroundColor: color.THEME,}}/>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.props.onPressButtonLocation()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                <View>
                  <KlikFonts name="location" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                    SHARE LOCATION
                  </Text>
                </View>
            </TouchableHighlight>
          </View>
          <View style={{width:1.2, backgroundColor: color.THEME,}}/>
          <View style={{padding:15, flex:1, alignItems:'center'}}>
            <TouchableHighlight
                onPress={() => this.onPressContact()}
                underlayColor='#eeeeee' style={{borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                <View>
                  <KlikFonts name="sendcontact" color={color.THEME} style={{alignSelf:'center'}} size={55}/>
                  <Text style={{color: color.THEME, fontSize:9, alignSelf:'center'}}>
                    SEND CONTACT
                  </Text>
                </View>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }

  onPressContact() {
    console.log('onPressContact: ', this.props);
    //this.setState({modalIsVisible: true});
    // this.props.setIsSafeToBack(false);
    this.props.navigator.push({
      id: "sendcontact",
      loggedInUser: this.props.loggedInUser,
      theme: this.props.theme,
      sendContact: this.sendContact,
      // setIsSafeToBack: this.props.setIsSafeToBack
    });
  }

  sendContact(contact) {
    InteractionManager.runAfterInteractions(() => {
      // this.props.setIsSafeToBack(true);
      Send.contact(this.props.loggedInUser()._id, this.props.recipientId, this.props.chatType, contact);
    });
  }



  onPressButtonGallery() {
    requestAnimationFrame(() => {
      this.selectMediaType();
    });
  }

  selectMediaType(mode) {
    requestAnimationFrame(() => {
      Alert.alert(
        'Please choose',
        null,
        [{text: 'Photo', onPress: () => this.launchImageLibrary('photo')},
         {text: 'Video', onPress: () => this.launchImageLibrary('video')},
         {text: 'CANCEL', onPress: () => {}}]
      )
    });
  }

  launchImageLibrary(media) {
    requestAnimationFrame(() => {
      var options = this.getMediaOptions(media);
      ImagePickerManager.launchImageLibrary(options, (response) => {
        if (response.didCancel) {
          console.log('InputOptions ['+ logTime() +']: User cancelled image picker');
        } else if (response.error) {
          console.log('InputOptions [' + logTime() +']: ImagePickerManager Error: ', response.error);
        } else if (response.customButton) {
          console.log('InputOptions [' + logTime() +']: User tapped custom button: ', response.customButton);
        } else {
          this.props.onSelectMedia(response.uri, media, 0);
        }
      });
    });
  }

  onPressTakePhoto() {
    this.launchCamera('photo');
  }

  onPressTakeVideo() {
    this.launchCamera('video');
  }

  launchCamera(media) {
    requestAnimationFrame(() => {
    var options = this.getMediaOptions(media);
      ImagePickerManager.launchCamera(options, (response) => {
        if (response.didCancel) {
          console.log('InputOptions ['+ logTime() +']: User cancelled image picker');
        } else if (response.error) {
          console.log('InputOptions [' + logTime() +']: ImagePickerManager Error: ', response.error);
        } else if (response.customButton) {
          console.log('InputOptions [' + logTime() +']: User tapped custom button: ', response.customButton);
        } else {
          this.props.onSelectMedia(response.uri, media, 0);
        }
      });
    });
  }

  // Helper method that returns the media options
  getMediaOptions(media) {
    var photoOptions = {
      cameraType: 'back',
      mediaType: 'photo',
      maxWidth: 800,
      maxHeight: 800,
      quality: 1,
      allowsEditing: false,
      noData: false,
      viewLoader: 0
    }
    var videoOptions = {
      cameraType: 'back',
      mediaType: 'video',
      videoQuality: 'low',
      durationLimit: 10,
      allowsEditing: false,
      noData: false
    }
    var options = (media == 'photo') ? photoOptions : videoOptions;
    return options;
  }

  onPressButtonAttachment() {
    DocumentPicker.show({
      filetype: ['public.data', 'public.composite-​content', 'public.presentation', 'com.apple.package', 'public.archive',
                 'com.adobe.pdf', 'com.microsoft.word.doc', 'com.microsoft.excel.xls', 'com.microsoft.powerpoint.​ppt'],
    }, (error, url) => {
      if (error) {
        alert("Error opening the file.");
      } else {
        const split = url.split('/');
        const name = split.pop();
        const inbox = split.pop();
        const realPath = FileReader.TemporaryDirectoryPath + inbox + '/' + name;
        this.props.onSelectMedia(realPath, 'attachment', 0);
      }
    });
  }

}

module.exports = InputOptions;
