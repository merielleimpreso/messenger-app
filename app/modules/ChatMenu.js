/**
 * Author: Ida Jimenez
 * Date Created: 2016-11-02
 * Date Updated: N/A
 * Description: Component used for ChatMenu
 */
'use strict';

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ToolbarAndroid,
  TouchableWithoutFeedback,
  AsyncStorage,
  TextInput,
  Dimensions
} from 'react-native';

import KlikFonts from 'react-native-vector-icons/KlikFonts';
const SCREEN_SIZE = Dimensions.get('window');
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));

class ChatMenu extends Component {
  render() {
    const color = this.props.color;
    const containerWidth = SCREEN_SIZE.width / 4;
    let height = this.props.height - (SCREEN_SIZE.height * .3);

    return (
      <View>
        <View style={{height: SCREEN_SIZE.height * .3, backgroundColor: color.CHAT_BG}}>
          <View style={{flex: 1, flexDirection: 'row', paddingTop: 15}}>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="sendcontact" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Invite</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="notifications" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Notifications Off</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="block" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Block</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="editname" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Edit Name</Text>
            </View>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="gallery" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Albums</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="notes" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Notes</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="settings" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Settings</Text>
            </View>
            <View style={{width: containerWidth, paddingLeft: 10, paddingRight: 10, alignItems: 'center'}}>
              <KlikFonts name="editmessage" style={{color: color.TEXT_LIGHT, alignSelf: 'center', fontSize: 40}}/>
              <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Edit Message</Text>
            </View>
          </View>
        </View>
        <TouchableWithoutFeedback onPress={() => this.props.setIsOpenChatMenu()}>
          <View style={{height: height, backgroundColor: 'rgba(0, 0, 0, 0.5)'}} />
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

module.exports = ChatMenu;