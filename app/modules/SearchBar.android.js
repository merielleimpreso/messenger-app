/* Author: John
 * This is the searchBar module for IOS
 * To use this:
 * var SearchBar = require('./path/to/SearchBar.js');
 * and  render

 <SearchBar
   onSearchChange={this.onSearchChange}           ------   event when search text is changed
   isLoading={this.state.isLoading}               ------   check if its fetching data
   placeholder="Search for people"                ------   placeholder of the text input
 />

 update: 2016-04-30 (by Merielle I.)
   - onChangeText, pass the text on onSearchChange props

*/

'use strict';

// var React = require('react-native');
// var {
//   ActivityIndicator,
//   TextInput,
//   StyleSheet,
//   View,
// } = React;

import React, {
  Component,
} from 'react';

import {
  ActivityIndicator,
  TextInput,
  StyleSheet,
  View,
  Dimensions
} from 'react-native';
const GLOBAL = require('./config/Globals.js');
var windowSize = Dimensions.get('window');

var SearchBar = React.createClass({

  render: function() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let width = windowSize.width - 20;
    return (
      <View style={[styles.searchBar, {backgroundColor:color.HIGHLIGHT, width: width}]}>
        <TextInput
          name='search'
          autoCapitalize="none"
          autoCorrect={false}
          placeholder={this.props.placeholder}
          placeholderTextColor={color.TEXT_LIGHT}
          onChangeText={(search) => this.props.onSearchChange(search)}
          onFocus={this.props.onFocus}
          style={[styles.searchBarInput, {backgroundColor: 'transparent', color: color.TEXT_DARK}]}
          value={this.props.value}
        />

        <ActivityIndicator
          animating={this.props.isLoading}
          style={styles.spinner}
        />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  searchBar: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 3,
    width: 270,
    height: 35
  },
  searchBarInput: {
    height: 35,
    padding: 5,
    textAlign: 'center',
  },
  spinner: {
    width: 30,
  },
});

module.exports = SearchBar;
