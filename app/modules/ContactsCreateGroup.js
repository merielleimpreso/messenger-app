'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  ToolbarAndroid,
  TouchableHighlight,
  View
} from 'react-native';
import _ from 'underscore';
import {logTime, toNameCase} from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';

const styles = (Platform.OS === 'ios') ?
                StyleSheet.create(require('./../styles/styles_common.js')) :
                StyleSheet.create(require('./../styles/styles.main.js'));
const GLOBAL = require('./config/Globals.js');
const WINDOW_SIZE = Dimensions.get('window');

class ContactsCreateGroup extends Component {

  // Initialize state and bind functions
  constructor(props) {
    super(props);
    this.state = {
      chosenContactIds: [],
      dataSource: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2}),
      groupName: '',
      isCreatingGroup: false,
      loading: true,
      isLoaded: false
    };
    this.renderRow = this.renderRow.bind(this);
    this.setChosenContactIds = this.setChosenContactIds.bind(this);
    this.onPressButtonSave = this.onPressButtonSave.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getChosenContacts();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.chosenContactIds != this.state.chosenContactIds) {
      this.getChosenContacts();
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getChosenContacts() {
    let addButton = {button: 'Add'};
    var array = [addButton];
    array = _.union(array, this.getChosenContactsInfo());
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(array)
    })
  }

  getChosenContactsInfo() {
    var contacts = [];
    for (var i = 0; i < this.state.chosenContactIds.length; i++) {
      let c = _.findWhere(this.props.users(), {_id:this.state.chosenContactIds[i]});
      if (c) {
        var name = '';
        var image = GLOBAL.DEFAULT_IMAGE;

        name = toNameCase(c.profile.firstName);
        image = (c.profile.photoUrl) ? c.profile.photoUrl : GLOBAL.DEFAULT_IMAGE;

        let contact = {
          _id: c._id,
          name: name,
          image: image
        };
        contacts.push(contact);
      }
    }
    return contacts;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let navbar = (Platform.OS === 'ios') ?
      (<NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Create a Group', tintColor: color.BUTTON_TEXT}}
         leftButton={
          <TouchableHighlight onPress={() => this.onPressButtonBack()} underlayColor='transparent' style={{alignSelf:'center'}}>
            <KlikFonts name='back' color={color.BUTTON_TEXT} style={styles.navbarButton} size={25}/>
          </TouchableHighlight>
         }
         rightButton={
           <TouchableHighlight onPress={() => this.onPressButtonSave()} underlayColor='transparent' style={{alignSelf:'center'}}>
             <Text style={{color:color.BUTTON_TEXT, margin:10, fontSize:15, fontWeight:'600'}}>Save</Text>
           </TouchableHighlight>
         }
      />) :
      (
        <KlikFonts.ToolbarAndroid
          title={"Create a Group"}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
          actions={[{title: 'Save', show: 'always', iconName: 'check'}]}
          onActionSelected={() => this.onPressButtonSave()}
        />
      )

    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        {navbar}

        <View style={{flexDirection:'row', margin:10}}>
          <TouchableHighlight onPress={() => this.onPressButtonImage()} underlayColor='transparent'>
            <View style={{flexDirection:'row'}}>
              <Image ref='image' style={{width:80,height:80,borderRadius:40}} source={{uri: GLOBAL.DEFAULT_IMAGE}}
                onLoadStart={(e) => this.setState({loading: true})}
                onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
                onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
                {(this.state.loading && !this.state.isLoaded) 
                  ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:80, width:80}}>
                      <ActivityIndicator size="small" color={color.BUTTON} />
                    </View>
                  : null
                }
              </Image>
              <View style={[styles.contactsCreateGroupImage, {backgroundColor:color.CHAT_BG, borderColor:color.CHAT_SENT}]}>
                <KlikFonts name='photo' color={color.TEXT_LIGHT} size={18}/>
              </View>
            </View>
          </TouchableHighlight>
          <TextInput ref='groupName'
            autoCapitalize='words'
            autoCorrect={false}
            //autoFocus={true}
            onChangeText={(groupName) => this.setState({groupName})}
            onSubmitEditing={(event) => {this.refs.lastName.focus();}}
            placeholder='Group Name'
            returnKeyType='next'
            selectionColor={color.THEME}
            style={[{margin:10, flex:1, color:color.TEXT_DARK, fontWeight:'bold', fontSize:15}]}
            placeholderTextColor={color.TEXT_LIGHT}
            rejectResponderTermination={false}
          />
        </View>

        <View style={[styles.contactsCreateGroupMemberTitle, {borderTopColor:color.INACTIVE_BUTTON, borderBottomColor:color.INACTIVE_BUTTON, backgroundColor:color.CHAT_BG}]}>
          <Text style={{color:color.TEXT_DARK, fontSize:12, margin:3, marginLeft:10, fontWeight:'400'}}>Members</Text>
        </View>

        <ListView
         contentContainerStyle={styles.contactsCreateGroupList}
         dataSource={this.state.dataSource}
         renderRow={this.renderRow}
         initialListSize={10}
         removeClippedSubviews={false}
        />

      </View>
    );

  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let plusSize = (((WINDOW_SIZE.width - 20) - (5*2*4)) / 4) / 1.5;
    let width = Platform.OS === 'ios' ? ((WINDOW_SIZE.width - 20) - (5*2*4)) / 4 : 60;
    let borderRadius = width * 0.5;
    if (data.button) {
      return (
        <TouchableHighlight style={styles.contactsCreateGroupListRow} onPress={() => this.onPressButtonAdd()} underlayColor='transparent'>
          <View style={{alignItems:'center'}}>
            <View style={[{backgroundColor:color.BUTTON}, styles.contactsCreateGroupListRowItem]}>
              <KlikFonts color={color.BUTTON_TEXT} name='plus' size={plusSize}/>
            </View>
            <Text style={{color:color.TEXT_DARK, fontSize:11}}>Add</Text>
          </View>
        </TouchableHighlight>
      );
    } else {
      return (
        <View key={data.id} style={styles.contactsCreateGroupListRow}>
          <View style={{alignItems:'center'}}>
            <View style={{flexDirection:'row'}}>
              <Image source={{uri:data.image}} style={styles.contactsCreateGroupListRowItem}
                onLoadStart={(e) => this.setState({loading: true})}
                onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
                onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
                {(this.state.loading && !this.state.isLoaded) 
                  ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:width, width:width}}>
                      <ActivityIndicator size="small" color={color.BUTTON} />
                    </View>
                  : null
                }
              </Image>
              <TouchableHighlight onPress={() => this.onPressButtonX(data)}
               underlayColor='transparent'
               style={[styles.contactsCreateGroupListRowItemRemove, {backgroundColor:color.CHAT_BG, borderColor:color.CHAT_SENT}]}>
                <KlikFonts name='close' color={color.TEXT_LIGHT} size={15}/>
              </TouchableHighlight>
            </View>
            <Text style={{color:color.TEXT_DARK, fontSize:11}}>{data.name}</Text>
          </View>
        </View>
      )
    }
  }

  renderCreatingGroup() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this._isMounted) {
      return (
        <Modal open={this.state.isCreatingGroup}
        closeOnTouchOutside={false}
        containerStyle={{
          justifyContent: 'center'
        }}
        modalStyle={{
          borderRadius: 2,
          margin: 20,
          padding: 10,
        }}
        >
        <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
        <Text style={{textAlign: 'center', color: color.BUTTON}}>Creating new group...</Text>
        </Modal>
      );
    }
  }

  onPressButtonBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  onPressButtonImage() {

  }

  onPressButtonAdd() {
    if (Platform.OS === 'ios') {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id:'contactscreategroupadd',
          renderConnectionStatus: this.props.renderConnectionStatus,
          hasInternet: this.props.hasInternet,
          theme: this.props.theme,
          loggedInUser: this.props.loggedInUser,
          users: this.props.users,
          chosenContactIds: this.state.chosenContactIds,
          setChosenContactIds: this.setChosenContactIds
        });
      });
    } else {
    requestAnimationFrame(() => {
        this.props.navigator.push({
          name:'ContactsCreateGroupAdd',
          renderConnectionStatus: this.props.renderConnectionStatus,
          hasInternet: this.props.hasInternet,
          theme: this.props.theme,
          loggedInUser: this.props.loggedInUser,
          users: this.props.users,
          chosenContactIds: this.state.chosenContactIds,
          setChosenContactIds: this.setChosenContactIds
        });
      });
    }
  }

  onPressButtonX(contact) {
    if (this._isMounted) {
      this.setState({
        chosenContactIds: _.without(this.state.chosenContactIds, contact._id)
      });
    }
  }

  onPressButtonSave() {
    if (this.state.groupName == '' || this.state.groupName == null) {
      Alert.alert(
        GLOBAL.ALERT_CREATE_GROUP_ERROR_TITLE,
        GLOBAL.ALERT_CREATE_GROUP_ERROR_DETAIL_NAME,
        [{text: 'OK', onPress: () => {}}]
      );
    } else if (this.state.chosenContactIds.length == 0) {
      Alert.alert(
        GLOBAL.ALERT_CREATE_GROUP_ERROR_TITLE,
        GLOBAL.ALERT_CREATE_GROUP_ERROR_DETAIL,
        [{text: 'OK', onPress: () => {}}]
      )
    } else {
      if (this._isMounted) {
        this.setState({
          isCreatingGroup: true
        });
      }
      let groupName = this.state.groupName;
      var chosenContactIds = this.state.chosenContactIds;
      chosenContactIds.push(this.props.loggedInUser()._id);
      ddp.call('createGroup', [groupName, chosenContactIds])
      .then(group => {
        if (this._isMounted) {
          this.setState({
            isCreatingGroup: false
          });
        }
        Alert.alert(
          'Success!',
          'Created a new group.',
          [{text: 'OK', onPress: () => {}}]
        );
        requestAnimationFrame(() => {
          this.props.navigator.popToTop(0);
        });
      });
    }
  }

  setChosenContactIds(ids) {
    if (this._isMounted) {
      this.setState({
        chosenContactIds: ids
      });
    }
  }
}

module.exports = ContactsCreateGroup;
