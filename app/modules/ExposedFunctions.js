var rneval = require('react-native-eval');
import Send from './config/db/Send';
global.ExposedFunctions = {
    hello: function() {
      console.log("ExposedFunctions: HELLO");
    },

    send: function(data) {
      var info = data[0];
      Send.textMessage(info.sender, info.recipient, info.chatType, info.message, true, () => {
        console.log("ExposedFunctions: sent: "+ info.message +"Great Success!");
      });
    }
  }
