//Used by ScrollableTab to render icons onto it's tab buttons.


/*update: 2016-06-15 (Ida)
- Change to whatsapp color scheme
update: 2016-06-27 (ida)
- change icons to klikfonts
update: 2016-08-01 (ida)
- Adjust icon
*/

'use strict';

// var React = require('react-native');

import React, {
  Component,
} from 'react';

// var {
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
//   Animated,
// } = React;

import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Animated,
} from 'react-native';

import KlikFonts from 'react-native-vector-icons/KlikFonts';
var GLOBAL = require('./../modules/config/Globals.js');
var SCREEN = Dimensions.get("window");

var styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    // marginTop: 5,
    // paddingBottom: 10,
  },
  tabs: {
    // height: 50,
    flexDirection: 'row',
    // paddingTop: 5,
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomColor: 'rgba(0,0,0,0.05)',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  badgeContainer: {
    borderRadius: 50, 
    width: 20, 
    height: 20, 
    marginBottom: -19, 
    marginRight: -5,
    alignSelf: 'flex-end', 
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  }
});

var FacebookTabBar = React.createClass({
  selectedTabIcons: [],
  unselectedTabIcons: [],

  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array
  },

  renderTabOption(name, page) {
    let color = this.props.color;
    var isTabActive = this.props.activeTab === page;
    let iconColor = this.props.activeTab === page ? color.BUTTON_TEXT : color.INACTIVE_BUTTON;
    let iconSize = SCREEN.height * 0.06;
    let tabHeight = SCREEN.height * 0.07;
    let label = null;
    let paddingBottom = {paddingBottom: 0};

    if (this.props.tabFor == "AddFriends") {
      label = <Text style={{color: color.BUTTON_TEXT, fontSize: 9}} allowFontScaling={true}>{this.renderTabLabel(name)}</Text>;
      iconSize = SCREEN.height * 0.05;
      tabHeight = SCREEN.height * 0.08;
      paddingBottom = {paddingBottom: 10}
    } else if (this.props.tabFor == "ShareMedia") {
      label = <Text style={{color: color.BUTTON_TEXT}} allowFontScaling={true}>{this.renderTabLabel(name)}</Text>;  
    }

    return (
      <TouchableOpacity key={name} onPress={() => this.props.goToPage(page)} style={[styles.tab, paddingBottom, {height: tabHeight}]}>
        <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
          {this.renderTabNotif(name)}
          {(this.props.tabFor != "ShareMedia") ?
            <KlikFonts name={name} size={iconSize} color={iconColor}
                ref={(icon) => { this.selectedTabIcons[page] = icon }}/> :
            null
          }
          {label}
        </View>
      </TouchableOpacity>
    );
  },

  // Tab label for Add Friends
  renderTabLabel(name) {
    switch (name) {
      case 'recommended':
        return "Request";
      case 'contact':
        return "Contact";
      case 'searchidphone':
        return "ID/Phone No.";
      case 'nearby':
        return "Nearby";
      case 'Friends':
        return "Friends";
      case 'Groups':
        return "Groups";
    }
  },

  renderTabNotif(name) {
    var notif = <View/>;

    if (name == 'recent' && this.props.unseen > 0) {
      notif = (
        <View style={[styles.badgeContainer, {backgroundColor: this.props.color.BUTTON}]}>
          <Text style={{fontSize: 10, textAlign: 'center', color: '#fff'}}>{this.props.unseen}</Text>
        </View>
      );
    }
    return notif;
  },

  componentDidMount() {
    this.setAnimationValue({value: this.props.activeTab});
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue);
  },

  setAnimationValue({value}) {
    var currentPage = this.props.activeTab;

    this.unselectedTabIcons.forEach((icon, i) => {
      var iconRef = icon;

      if (!icon.setNativeProps && icon !== null) {
        iconRef = icon.refs.icon_image
      }

      if (value - i >= 0 && value - i <= 1) {
        iconRef.setNativeProps({ style: {opacity: value - i} });
      }
      if (i - value >= 0 &&  i - value <= 1) {
        iconRef.setNativeProps({ style: {opacity: i - value} });
      }
    });
  },

  render() {
    let color = this.props.color;
    var containerWidth = this.props.containerWidth;
    var numberOfTabs = this.props.tabs.length;
    var tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 2,
      backgroundColor: color.BUTTON_TEXT,
      bottom: 0,
    };

    var debugText = <View/>;

    styles.icon = {
                  position: 'absolute',
                  top: 0,
                  left: (containerWidth / numberOfTabs)/2.8,
                }
    var left = this.props.scrollValue.interpolate({
      inputRange: [0, 1], outputRange: [0, containerWidth / numberOfTabs]
    });

    if (this.props.isDebug != null && this.props.isDebug) {
      debugText = <Text>{this.props.debugText}</Text>
    }

    return (
      <View style={{backgroundColor: color.THEME}} elevation={5}>
        {debugText}
        <View style={{flexDirection:'row', flex:1}}>
        {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
        </View>
        <Animated.View style={[tabUnderlineStyle, {left}]} />
      </View>
    );
  },
});

module.exports = FacebookTabBar;
