/*Author: John Rezan B
Date Created: 2016-09-09
Date Updated:
Description: This file is the login modals for the kliklab.

Changelog:
update:
*/

'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableNativeFeedback,
  View
} from 'react-native';

import {launchCamera, launchImageLibrary} from './Helpers';
import Button from './common/Button';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';
import Navigation from './actions/Navigation';
import SystemAccessor from './SystemAccessor';
import Toolbar from './common/Toolbar';

const GLOBAL = require('./../modules/config/Globals.js');
const ImagePickerManager = require('NativeModules').ImagePickerManager;
const styles = StyleSheet.create(require('./../styles/styles.main.js'));
const SCREEN = Dimensions.get("window");

class KliklabsRegister extends Component {

  // Set initial state
  constructor(props) {
    super(props);

    this.state = {
      klikFirstName: '',
      klikLastName: '',
      klikEmail: '',
      klikConfirmEmail: '',
      klikPass: '',
      klikConfirmPass: '',
      photoUrl: null,
      isLoading: false,
      isShowCheckConnection: false,
      isShowMessageModal: false,
      photoOptions: {
        cameraType: 'back',
        mediaType: 'photo',
        maxWidth: 800,
        maxHeight: 800,
        quality: 1,
        allowsEditing: false,
        noData: false
      },
      loading: true,
      isLoaded: false
    }
    this.setIsOpenImagePicker = this.setIsOpenImagePicker.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.launchCamera = this.launchCamera.bind(this);
    this.launchGallery = this.launchGallery.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.props.setSignUpCallback();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // Render display
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let photo = (this.state.photoUrl == null) ? require('./../images/tempprofpic.jpg') : {uri: this.state.photoUrl},
        photoWidth = 140,
        borderRadius = photoWidth * 0.5;
      return (
        <View style={{backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG, flex:1}}>
          {this.renderNavBar()}
          {this.props.renderConnectionStatus()}
          <ScrollView keyboardDismissMode='interactive' keyboardShouldPersistTaps={true} style={{flex: 1, paddingTop:15}}>
            <View style={{backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG, alignItems: 'center', justifyContent: 'center',}}>
              <View style={{flexDirection: 'row', position: 'relative', marginBottom: 20}}>
                <Image style={{width:photoWidth, height:photoWidth, borderRadius:borderRadius}} source={photo}/>
                <View style={[styles.addPhotoRegister, {position: 'absolute', backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG, borderColor: GLOBAL.COLOR_CONSTANT.TEXT_LIGHT}]}>
                  <KlikFonts name="photo" onPress={() => Navigation.onPress(() => this.setIsOpenImagePicker(true))} style={{color: GLOBAL.COLOR_CONSTANT.TEXT_LIGHT, fontSize: 18}}/>
                </View>
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikFirstName'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  placeholder="First Name"
                  value={this.state.klikFirstName}
                  onChangeText={(klikFirstName) => {
                    klikFirstName = (klikFirstName.trim() == '') ? '' : klikFirstName;
                    this.setState({klikFirstName: klikFirstName})
                  }}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.klikLastName.focus()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                  autoCapitalize='words'
                />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikLastName'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  placeholder="Last Name"
                  value={this.state.klikLastName}
                  onChangeText={(klikLastName) => {
                    klikLastName = (klikLastName.trim() == '') ? '' : klikLastName;
                    this.setState({klikLastName: klikLastName})
                  }}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.klikEmail.focus()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                  autoCapitalize='words'
                />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikEmail'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  value={this.state.klikEmail}
                  placeholder="Email"
                  onChangeText={(klikEmail) => {
                    klikEmail = (klikEmail.trim() == '') ? '' : klikEmail;
                    this.setState({klikEmail: klikEmail})
                  }}
                  keyboardType='email-address'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.klikConfirmEmail.focus()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikConfirmEmail'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  value={this.state.klikConfirmEmail}
                  placeholder="Confirm Email"
                  onChangeText={(klikConfirmEmail) => {
                    klikConfirmEmail = (klikConfirmEmail.trim() == '') ? '' : klikConfirmEmail;
                    this.setState({klikConfirmEmail: klikConfirmEmail})
                  }}
                  keyboardType='email-address'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.klikPass.focus()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikPass'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  placeholder="Password"
                  value={this.state.klikPass}
                  onChangeText={(klikPass) => {
                    klikPass = (klikPass.trim() == '') ? '' : klikPass;
                    this.setState({klikPass: klikPass})
                  }}
                  secureTextEntry={true}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.klikConfirmPass.focus()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput
                  ref='klikConfirmPass'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                  placeholder="Confirm Password"
                  value={this.state.klikConfirmPass}
                  secureTextEntry={true}
                  onChangeText={(klikConfirmPass) => {
                    klikConfirmPass = (klikConfirmPass.trim() == '') ? '' : klikConfirmPass;
                    this.setState({klikConfirmPass: klikConfirmPass})
                  }}
                  keyboardType='default'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.onPressedSignup()}}
                  style={[styles.input, {backgroundColor: 'transparent', color: GLOBAL.COLOR_CONSTANT.TEXT_DARK}]}
                />
              </View>
              <Button text="Create Account"
               color={GLOBAL.COLOR_CONSTANT.THEME}
               underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
               onPress={() => this.onPressedSignup()}
               style={{marginTop:15}}
               isDefaultWidth={true}/>
            </View>
            <KeyboardSpacer />
          </ScrollView>
          {this.renderModal()}
        </View>
      );
  }

  renderNavBar() {
    return (
      <Toolbar
        title={'Registration'}
        withBackButton={true}
        onIconClicked={this.props.goToSignUp}
        {...this.props}
      />
    );
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsOpenImagePicker(isOpen) {
    if (this._isMounted) {
      this.setState({isOpenImagePicker: isOpen});
    }
  }

  setIsLoading(isLoading) {
    if (this._isMounted) {
      this.setState({isLoading: isLoading});
    }
  }

  renderModal() {
    if (this.state.isLoading) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    } else if (this.state.isOpenImagePicker) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsOpenImagePicker(false)}
          onPressOpenGallery={this.launchGallery}
          onPressOpenCamera={this.launchCamera}
          modalType={'uploadImage'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  launchGallery(){
    launchImageLibrary('photoWithData', (response) => {
      this.checkFileSize(response);
    });
  }

  launchCamera(){
    launchCamera('photoWithData', (response) => {
      this.checkFileSize(response);
    });
  }

  checkFileSize(response) {
    if (typeof response == "string") {
      this.setIsShowMessageModal(response);
    } else {
      const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
      if (this._isMounted) {
        this.setState({photoUrl: source.photoUrl})
      }
      this.setIsOpenImagePicker(false);
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  onPressedSignup() {
    this.refs.klikFirstName.blur();
    this.refs.klikLastName.blur();
    this.refs.klikEmail.blur();
    this.refs.klikConfirmEmail.blur();
    this.refs.klikPass.blur();
    this.refs.klikConfirmPass.blur();

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      let emailPattern = /\S+@\S+\.\S+/;

      if (!this.state.klikFirstName) {
        this.refs.klikFirstName.focus();
        this.setIsShowMessageModal('Please enter your first name');
      } else if (!this.state.klikLastName) {
        this.refs.klikLastName.focus();
        this.setIsShowMessageModal('Please enter your last name')
      } else if (!this.state.klikEmail) {
        this.refs.klikEmail.focus();
        this.setIsShowMessageModal('Please enter your email');
      } else if (!emailPattern.test(this.state.klikEmail)) {
        this.refs.klikEmail.focus();
        this.setIsShowMessageModal('Your email is invalid');
      } else if (this.state.klikEmail !== this.state.klikConfirmEmail) {
        this.refs.klikConfirmEmail.focus();
        this.setIsShowMessageModal('Emails does not match');
      } else if (!this.state.klikPass) {
        this.refs.klikPass.focus();
        this.refs.klikPass.clear();
        this.refs.klikConfirmPass.clear();
        this.setIsShowMessageModal('Please enter your password')
      } else if (this.state.klikPass.length < 6) {
        this.refs.klikPass.focus();
        this.refs.klikPass.clear();
        this.refs.klikConfirmPass.clear();
        this.setIsShowMessageModal('Password should be at least 6 characters long.')
      } else if (!this.state.klikConfirmPass) {
        this.refs.klikConfirmPass.focus();
        this.refs.klikPass.clear();
        this.refs.klikConfirmPass.clear();
        this.setIsShowMessageModal('Please confirm your password');
      } else {
        if (this.state.klikPass != this.state.klikConfirmPass) {
          this.refs.klikPass.focus();
          this.refs.klikPass.clear();
          this.refs.klikConfirmPass.clear();
          this.setIsShowMessageModal('Your password does not match')
        } else {
          this.setIsLoading(true);
          let toEncrypt = {
            username:this.props.username,
            password:this.state.klikPass,
            lastName: this.state.klikLastName,
            firstName:this.state.klikFirstName,
            mobile:this.props.mobile,
            email:this.state.klikEmail,
            image: this.state.photoUrl
          }

          // console.log('registering:',credentials);

          SystemAccessor.encrypt(JSON.stringify(toEncrypt), this.props.token.substring(0, 16), (err, credentials) => {
            if (credentials) {
              console.log("credentials: ", credentials);
              ddp.call('apiRegisterUser', [credentials, this.props.token]).then((toDecrypt) => {
                // console.log('toDecrypt', toDecrypt);
                SystemAccessor.decrypt(toDecrypt, this.props.token.substring(0, 16), (err, response) => {
                  response = JSON.parse(response);

                  if(GLOBAL.TEST_MODE) {
                    response.sukses = true;
                    console.log('--- TEST MODE KliklabsRegister - apiRegisterUser response = ', response);

                  } else {
                    console.log('Register kliklab account:', response);

                  }

                  if (response.sukses) {
                    this.setIsLoading(false);
                    this.props.navigator.push({
                      name:'KliklabsCongratulations',
                      id: 'kliklabscongratulations',
                      apiMobile: null,
                      audio: this.props.audio,
                      image: this.state.photoUrl,
                      username: this.props.username,
                      password: this.state.klikPass,
                      addContactToLoggedInUser: this.props.addContactToLoggedInUser,
                      backgroundImageSrc: this.props.backgroundImageSrc,
                      changeTheme: this.props.changeTheme,
                      debugText: this.props.debugText,
                      getMessageWallpaper: this.props.getMessageWallpaper,
                      hasInternet: this.props.hasInternet,
                      isDebug: this.props.isDebug,
                      isSafeToBack: this.props.isSafeToBack,
                      isLoggedIn: this.props.isLoggedIn,
                      loggedInUser: this.props.loggedInUser,
                      logout: this.props.logout,
                      renderConnectionStatus: this.props.renderConnectionStatus,
                      setAudio: this.props.setAudio,
                      setIsSafeToBack: this.props.setIsSafeToBack,
                      setLoggedInUser: this.props.setLoggedInUser,
                      setSignUpCallback: this.props.setSignUpCallback,
                      shouldReloadData: this.props.shouldReloadData,
                      subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
                      theme: this.props.theme,
                      timeFormat: this.props.timeFormat,
                    });
                  } else {
                    this.setIsLoading(false);
                    if (response.msg) {
                      this.setIsShowMessageModal(response.msg);
                    } else {
                      this.setIsShowMessageModal('Something went wrong. Please try again.');
                    }
                  }
                });
              });
            } else {
              this.setIsLoading(false);
              this.setIsShowMessageModal('Something went wrong. Please try again.');
            }
          });
        }
      }
    }
  }
}


module.exports = KliklabsRegister;
