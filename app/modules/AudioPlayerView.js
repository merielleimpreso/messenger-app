/*

Author: Merielle Impreso
Date Created: 2016-05-30
Date Updated: NA
Description: Module to display recording when played.

update: 2016-06-24 by Merielle I.
  - added timer for playing audio
update: 2016-07-05 by Merielle I.
  - added consoleLog from Helpers for performance

*/

'use strict';

import React, {
  Component,
} from 'react';

import {
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import {secondsToTime, consoleLog, getMediaURL} from './Helpers';
import {AudioPlayer, AudioRecorder, AudioUtils} from 'react-native-audio';
import Download from './config/db/Download';
import FileReader from 'react-native-fs';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Validator from 'validator';
import {Video} from 'react-native-media-kit';
// var styles = StyleSheet.create(require('./../styles/styles_common.js'));
const styles = StyleSheet.create(require('./../styles/styles.main.js'));
let timeInterval;
export default class AudioPlayerView extends Component {
  // mixins: [SetIntervalMixin],

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      cannotPlay: false,
      seconds: 0,
      isPaused: true,
      startInterval: true,
      startTime: new Date,
      duration: this.props.data.audio.duration,
      currentTime: 0,
      isInitial: true,
      noConnection: false,
      playAudio: (this.props.data.mediaCache) ? this.props.data.mediaCache : getMediaURL(this.props.data.audio.data),
    }
    this.cannotPlay = this.cannotPlay.bind(this);
    this.checkIfExist = this.checkIfExist.bind(this);
    this.play = this.play.bind(this);
    this.downloadAudio = this.downloadAudio.bind(this);
    this.setCannotPlay = this.setCannotPlay.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(timeInterval);
  }

  // Render display
  render() {
    var color = this.props.color;
    return (
        <View style={[styles.audioPlayer, {backgroundColor:color.THEME, height: 50, width: 160}]}>
          <Video
            style={[styles.audioPlayer, {backgroundColor:color.THEME}]}
            autoplay={false}
            preload='auto'
            loop={false}
            controls={true}
            muted={false}
            src={this.state.playAudio}
            cannotPlay={this.cannotPlay}
            downloadAudio={this.checkIfExist}
            audiocontrols={true}
          />
        </View>
      )
  }

  cannotPlay() {
    return this.state.cannotPlay;
  }

  checkIfExist() {
    this.setCannotPlay(false);
    
    if ((this.state.playAudio.indexOf('http') !== -1 && this.props.hasInternet()) || this.state.playAudio.indexOf('http') == -1) {
      let audioData = this.props.data;
      let mediaCache = audioData.mediaCache;
      let audioUrl = getMediaURL(audioData.audio.data)
      if (mediaCache) {
        Download.checkIfFileExist(mediaCache, (isExist) => {
          let playAudio = (isExist) ? mediaCache : audioUrl;
          console.log('[isExist]', isExist)
          if (!isExist) {
            this.props.isAutoSaveMedia() && this.downloadAudio();
          }
          if (playAudio.indexOf('http') !== -1 && !this.props.hasInternet()) {
            this.setCannotPlay(true);            
          }
          this.setState({ playAudio: playAudio })
        });
      } else {
        this.props.isAutoSaveMedia() && this.downloadAudio();
        this.setState({ playAudio: audioUrl })
      }
    } else {
      this.setCannotPlay(true);
    }
  }

  setCannotPlay(isCannotPlay) {
    if (this._isMounted) {
      this.setState({ cannotPlay: isCannotPlay })      
    }
  }

  play(audio, duration) {
    if (audio.indexOf('http') !== -1 && this.props.hasInternet() || audio.indexOf('http') == -1) {
      let audioUrl = getMediaURL(this.props.data.audio.data)
      this.props.playAudio(audioUrl, audio, duration);
      this.props.isAutoSaveMedia() && this.downloadAudio(audioUrl, true);
      timeInterval = setInterval(() => this.setCurrentTime(), 1000);
    } else {
      this.setState({
        noConnection: true
      })
    }
  }

  setCurrentTime() {
    if (this._isMounted) {
      if (this.props.audio()) {
        this.setState({
          currentTime: this.state.currentTime + 1
        })
      } else {
        clearInterval(timeInterval);
        this.setState({
          currentTime: 0
        })
      }
    }
  }

  downloadAudio() {
    let audioData = this.props.data;
    if (this.props.isAutoSaveMedia() && this.props.hasInternet()) {
      var audio = getMediaURL(audioData.audio.data);
      let data = {
        messageId: this.props.data._id,
        mediaURL: audio,
        otherUserId: this.props.otherUserId
      }
      let path = (audioData.mediaCache && audioData.mediaCache.indexOf('/Sent/') !== -1) ? Download.mediaTypePath.audioSent : Download.mediaTypePath.audio;
      Download.downloadMediaFile(path, data, true);
    }
  }
};

