'use strict';
import React, { Component } from 'react';
import {
  InteractionManager,
  ListView,
  Platform,
  ToastAndroid
} from 'react-native';
import { launchCamera, launchImageLibrary } from '../Helpers.js';
import _ from 'underscore';
import AddGroupChatRender from './AddGroupChatRender';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import Users from '../config/db/Users';
import Send from '../config/db/Send';

export default class AddGroupChat extends Component{
  // Initial state
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      contactsChosen: [],
      groupName: "",
      groupImage: null,//actual image to be uploaded. Uploads empty, separate upload via Send.uploadMedia
      groupImageTemp: "",//for display,
      isAllLoaded: false,
      isCreatingGroup: false,
      isShowCheckConnection: false,
      isShowChangeGroupName: false,
      loading: true,
      showModalMethod: null,
      pickerResponse: null,
      text: "",
      user: null
    }

    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.goToGroup = this.goToGroup.bind(this);
  }

  //get the data before rendering
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        showModalMethod: this.setIsShowMessageModal
      });
      this.getContacts();
    });
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Get list of contacts from phone contacts that are already registered
  getContacts(){
    let friends = Users.getAllFriends();
    this.setState({
      isAllLoaded: true,
      friends: friends,
      dataSource: this.state.dataSource.cloneWithRows(friends)
    });
  }

  // Display
  render() {
    return (
      <AddGroupChatRender
        alertMessage={this.state.alertMessage}
        contactsChosen={this.state.contactsChosen}
        onActionSelected={this.createGroup.bind(this)}
        dataSource={this.state.dataSource}
        filterWithSearchText={this.filterWithSearchText.bind(this)}
        groupImage={this.state.groupImageTemp}
        groupName={this.state.groupName}
        isAllLoaded={this.state.isAllLoaded}
        isCreatingGroup={this.state.isCreatingGroup}
        isShowCheckConnection={this.state.isShowCheckConnection}
        isShowChooseToUploadModal={this.state.isShowChooseToUploadModal}
        isShowMessageModal={this.state.isShowMessageModal}
        isShowChangeGroupName={this.state.isShowChangeGroupName}
        launchCamera={this.launchCamera.bind(this)}
        launchGallery={this.launchGallery.bind(this)}
        onChangeText={this.onChangeText.bind(this)}
        processThisContact={this.processThisContact.bind(this)}
        setIsShowChooseToUploadModal={this.setIsShowChooseToUploadModal.bind(this)}
        setIsShowCheckConnection={this.setIsShowCheckConnection.bind(this)}
        setIsShowMessageModal={this.state.showModalMethod}
        setIsShowChangeGroupName={this.setIsShowChangeGroupName.bind(this)}
        setGroupName={this.setGroupName.bind(this)}
        toolbarActions={[{title: 'Submit', iconName: 'check', show: 'always'}]}
        {...this.props}
      />
    );
  }

  onChangeText(text) {
    text = (text.trim() == '') ? '' : text;
    if (this._isMounted) {
      this.setState({groupName: text})
    }
  }

  // Search contact
  filterWithSearchText(text) {
    text = (text.trim() == '') ? '' : text;
    var contact;
    var tempRawDataSource = this.state.friends.slice();

    for(var index = 0; index < tempRawDataSource.length; index++) {
      contact = tempRawDataSource[index].name;

      if ((contact).toLowerCase().indexOf((text).toLowerCase()) === -1 ) {
        tempRawDataSource.splice(index, 1);
        index=-1;
      }
    }
    if (this._isMounted) {
      this.setState({
        text: text,
        dataSource: this.state.dataSource.cloneWithRows(tempRawDataSource)
      });
    }
  }

  // Add or Remove user from the group
  processThisContact(contact) {
    let tempContactsChosen = this.state.contactsChosen.slice();

    if (tempContactsChosen.indexOf(contact._id) == -1) {
      tempContactsChosen.push(contact._id);
    } else {
      tempContactsChosen.splice(tempContactsChosen.indexOf(contact._id), 1);
    }

    if (this._isMounted) {
      this.state.dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setState({
        friends: this.state.friends,
        dataSource: this.state.dataSource.cloneWithRows(this.state.friends),
        contactsChosen: tempContactsChosen
      });
    }
    this.filterWithSearchText(this.state.text);
  }

  // Create the group and redirect to GroupMessages.android.js
  createGroup() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      dismissKeyboard();
      if (this.state.contactsChosen.length < 2) {
        if (Platform.OS === 'ios') {
          this.setIsShowMessageModal('Please add two or more people to make this a group');
        } else {
          ToastAndroid.show('Please add two or more people to make this a group', ToastAndroid.SHORT);
        }
      } else {

        if (_.isEmpty(this.state.groupName)) {
          if (Platform.OS === 'ios') {
            this.setIsShowMessageModal('Please enter your groupName');
          } else {
            ToastAndroid.show('Please enter your groupName', ToastAndroid.SHORT);
          }
        } else {
          if (this._isMounted) {
            this.setState({ isCreatingGroup: true });
          }
          let groupName = this.state.groupName;
          let groupImage = this.state.groupImage;
          let contactList = this.state.contactsChosen;
          contactList.push(this.props.loggedInUser()._id);
          ddp.call('addGroup', [groupName, contactList, groupImage]).then(result => {
            if (result) {

              this.setState({
                user: result
              });

              if(this.state.pickerResponse != null) {

                var data = this.state.pickerResponse;
                data['groupId'] = result._id;

                console.log("pickerResponse: ", this.state.pickerResponse);
                Send.uploadMedia(data, 'photo', Send.uploadURL.groupImage, null, null, (uploadMediaResult) => {

                    if (typeof uploadMediaResult == 'string' && uploadMediaResult != 'Uploading failed.') {
                        this.goToGroup();
                    } else {
                        this.setState({
                          showModalMethod: this.goToGroup,
                          isCreatingGroup : false
                        });
                        this.setIsShowMessageModal('Group created, but we group image upload failed.');
                    }

                });
              } else {
                console.log("null pickerResponse");
                this.goToGroup();
              }

            }
          }).catch(error=>{
            if (this._isMounted) {
              this.setState({ isCreatingGroup: false });
              this.setIsShowMessageModal('Something went wrong, please try again later.');
            }
            console.log("Failed adding group Error: ", error);
          });



        }
      }
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowChooseToUploadModal() {
    if (this._isMounted) {
      this.setState({isShowChooseToUploadModal: !this.state.isShowChooseToUploadModal});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  launchGallery(){
    //used to be photoWithData
    launchImageLibrary('photo', (response) => {
      this.checkFileSize(response);
    });
  }

  launchCamera(){
    launchCamera('photo', (response) => {
      this.checkFileSize(response);
    });
  }

  checkFileSize(response) {
    console.log('--- AddGroupChat response=',response);

    this.setIsShowChooseToUploadModal();
    if (typeof response == "string") {
      this.setIsShowMessageModal(response);
    } else {
      this.setGroupImage(response);
    }
  }

  setGroupImage(response) {
    //const source = {photoUrl: 'data:image/jpeg;base64,' + response.data, isStatic: true};
    if (this._isMounted) {
      this.setState({
                  //groupImage: source.photoUrl,
                    groupImageTemp: response.uri,
                    pickerResponse: response});
    }
  }

  setIsShowChangeGroupName() {
    console.log("AAAA");
    if(this._isMounted) {
      console.log("setIsShowChangeGroupName");
      this.setState({isShowChangeGroupName: !this.state.isShowChangeGroupName});
    }
  }

  setGroupName(newGroupName) {
    if(this._isMounted) {
      this.setState({groupName: newGroupName,
                     isShowChangeGroupName: false});
    }
  }

  goToGroup() {

    if (this._isMounted) {
      this.setState({ isCreatingGroup: false,
                      isShowMessageModal: false});
    }

    requestAnimationFrame((user) => {
      this.props.navigator.replace({
        id:'messagesgiftedchat',
        name: 'MessagesGiftedChat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeBgImage: this.props.changeBgImage,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        renderConnectionStatus: this.props.renderConnectionStatus,
        user: this.state.user,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        hasInternet: this.props.hasInternet,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        setAudio: this.props.setAudio,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setIsViewVisible: this.props.setIsViewVisible,
        setLoggedInUser: this.props.setLoggedInUser,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        isAutoSaveMedia: this.props.isAutoSaveMedia
      });
    });
  }
};
