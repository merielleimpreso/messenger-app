import { Dimensions, StyleSheet } from 'react-native';
const windowSize = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  searchContainer: {
    flexDirection: 'row',
    width: windowSize.width - 20,
    marginLeft: 10, 
    marginRight: 10,
    alignItems: 'center'
  },
  input: {
    flex: 1,
    height: 40,
    padding: 5
  },
  contactIcon: {
    fontSize: 30,
  },
  pictureNameContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10
  },
  changeAvatarIcon: {
   alignSelf: 'flex-end',
   borderRadius: 50,
   borderWidth: 0.5,
   width: 25,
   height: 25,
   alignItems: 'center',
   justifyContent: 'center'
 },
 txtName: {
    fontSize: 18,
  },
});