'use strict';
import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  ListView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import { getMediaURL, renderFooter } from '../Helpers.js';
import ImageWithLoader from './../common/ImageWithLoader';
import GLOBAL from '../../modules/config/Globals.js';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from '../common/ListRow';
import ProfilePic from '../common/ProfilePic';
import RenderModal from '../common/RenderModal';
import Navigation from '../actions/Navigation';
import styles from './styles';
import Toolbar from './../common/Toolbar';
const windowSize = Dimensions.get('window');

export default class AddGroupChatRender extends Component {
  // Display
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let groupImage = (this.props.groupImage) ? this.props.groupImage : getMediaURL(GLOBAL.DEFAULT_GROUP_IMAGE);
    let groupName = (this.props.groupName) ? this.props.groupName : 'Group Name'
    let textColor = (this.props.groupName) ? color.TEXT_DARK : color.HIGHLIGHT;
    return (
      <View style={[styles.container, {backgroundColor: color.CHAT_BG}]}>
        <Toolbar title={'Create Group'} {...this.props} 
          withBackButton={true} 
          onPressRightButton={this.props.onActionSelected} 
          rightIcon={'check'}
        />

        <View style={[styles.mainContainer, {backgroundColor: color.CHAT_BG}]}>
          <View style={styles.pictureNameContainer}>
            <ProfilePic source={groupImage} color={color} size={75}
              changeAvatar={this.props.setIsShowChooseToUploadModal}
            />
            <View>
              <View style={[styles.searchContainer]}>

               {/* <TextInput
                  ref="inputGroupName"
                  placeholder={'Name (Ex. Family)'}
                  placeholderTextColor={color.TEXT_LIGHT}
                  style={[styles.input, {color: color.TEXT_DARK}]}
                  onChangeText={(text) => this.props.onChangeText(text)}
                  value={this.props.text}
                  underlineColorAndroid={color.THEME} />*/}
                <TouchableOpacity onPress={() => this.props.setIsShowChangeGroupName()}>
                  <View style={{borderBottomWidth: 1, borderBottomColor: color.BUTTON, marginLeft: 5, width: windowSize.width * 0.65}}>
                    <Text style={[styles.txtName, {color: textColor}]} underlineColorAndroid={color.HIGHLIGHT}>
                      {groupName}
                    </Text>
                  </View>
                  <Text style={{fontSize: 10, color: color.TEXT_LIGHT, marginLeft: 5}}>GROUP NAME</Text>
                </TouchableOpacity>

              </View>
              <Text style={{fontSize: 10, color: color.TEXT_LIGHT, marginLeft: 15}}>{this.props.groupName.length}/30</Text>
            </View>
          </View>
          <View style={styles.searchContainer}>
            <KlikFonts name="addcontact" style={[styles.contactIcon, {color: color.THEME}]}/>
            <TextInput
              placeholder={`Enter your friend's name`}
              placeholderTextColor={color.TEXT_LIGHT}
              style={[styles.input, {color: color.TEXT_DARK}]}
              onChangeText={(text) => this.props.filterWithSearchText(text)}
              value={this.props.text}
              underlineColorAndroid={color.THEME} />
          </View>
          {this.renderListView()}
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderListView() {
    return (
      <ListView
        ref="listview"
        keyboardShouldPersistTaps={true}
        dataSource={this.props.dataSource}
        enableEmptySections={true}
        renderFooter={this.renderFooter.bind(this)}
        renderRow={this.renderContacts.bind(this)}
      />
    );
  }

  //render each contact
  renderContacts(contact) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var icon = (this.props.contactsChosen.indexOf(contact._id)==-1) ? 'unselected' : 'selected';

    return (
      <ListRow iconPress={() => this.props.processThisContact(contact)}
        onPress={() => this.props.processThisContact(contact)}
        labelText={contact.name}
        detailText={contact.statusMessage}
        image={getMediaURL(contact.image)}
        color={color}
        addSeparator={true}
        icon={icon}
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.props.dataSource.getRowCount(0) == 0) ? 'No contact found' : null;
    return renderFooter(this.props.isAllLoaded, color, this.props.hasInternet(), notFoundText);
  }

  renderModal() {
    if (this.props.isCreatingGroup) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('pressed back')}
          modalType={'loading'}
        />
      );
    } else if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowChooseToUploadModal) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.setIsShowChooseToUploadModal}
          onPressOpenGallery={this.props.launchGallery}
          onPressOpenCamera={this.props.launchCamera}
          modalType={'uploadImage'}
        />
      );
    } else if (this.props.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.props.setIsShowMessageModal("")}
         message={this.props.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.props.isShowChangeGroupName) {
      return (
        <RenderModal
          theme={this.props.theme}
          detail={this.props.groupName}
          maxLength={30}
          title={'Group Name'}
          setIsShowModal={this.props.setIsShowChangeGroupName}
          setChangeProfileDetail={this.props.setGroupName}
          modalType={'changeProfileDetail'}
        />
      );
    }
  }
}
