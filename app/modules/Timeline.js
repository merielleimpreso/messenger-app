/*

Author: John Rezan Baguna
Date Created: 2016-06-29
Date Updated:
Description: Display posts by friends and writes posts

update: 2016-06-30 by Merielle I.
  - Get data from server
  - Added TimelinePost for displaying of post
update: 2016-07-05 by Merielle I.
  - Added limit in subscribe
update: 2016-07-5 by Merielle I.
  - Loading indicator now disappears when all messages are loaded

*/
'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import _ from 'underscore';
import {getDisplayName, renderFooter, toNameCase, timelineTimeOrDate} from './Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import NavigationBar from 'react-native-navbar';
import Post from './config/db/Post';
import RenderModal from './common/RenderModal';
import StoreCache from './config/db/StoreCache';
import Navigation from './actions/Navigation';
import TimelinePost from './TimelinePost';
import TimelineReact from './TimelineReact';
import Users from './config/db/Users';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common'));
const windowSize = Dimensions.get('window');

class Timeline extends Component {

  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      commentActive: null,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isLoggedIn: this.props.isLoggedIn(),
      isLoadedFromCache: false,
      isMakingNewStatus: false,
      isShowCheckConnection: false,
      posts: null,
      theme: this.props.theme(),
      loggedInUser: this.props.loggedInUser(),
      reactToggled: false,
      currentPostId: 0,
      post: null
    }
    this.commentActive = this.commentActive.bind(this);
    this.getDataToBeDisplayed = this.getDataToBeDisplayed.bind(this);
    this.getTimelineCache = this.getTimelineCache.bind(this);
    this.observe = this.observe.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderStatusBox = this.renderStatusBox.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setCommentActive = this.setCommentActive.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowReactions = this.setIsShowReactions.bind(this);
    this.commentOnPost = this.commentOnPost.bind(this);
    this.closeReact = this.closeReact.bind(this);
    this.post = this.post.bind(this);
  }

  // Put a flag to check if the component is mounted
  componentDidMount() {
    this._isMounted = true;
    this.getTimelineCache();
    InteractionManager.runAfterInteractions(() => {
      this.subscribe();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state.isLoggedIn) != JSON.stringify(this.props.isLoggedIn())) {
      console.log('Timeline: Detected login change: ' + this.props.isLoggedIn());
      if (this._isMounted) {
        this.setState({
          isLoggedIn: this.props.isLoggedIn()
        });
      }
      if (this.props.isLoggedIn()) {
        this.subscribe();
      } else {
        Post.stopObserving();
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getTimelineCache() {
    StoreCache.getCache(StoreCache.keys.posts, (posts) => {
      if (posts) {
        console.log('Posts.js: Loaded ' + posts.length + ' posts from cache');
        if (this._isMounted) {
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(posts),
            posts: posts,
            isAllLoaded: true,
            // isLoadedFromCache: true
          });
        }
        StoreCache.getCache(StoreCache.keys.contacts, (contacts) => {
          if (contacts) {
            if (this._isMounted) {
              this.setState({
                contacts: contacts
              });
            }
          }
        });
      }
    });
  }

  subscribe() {
    if (this.props.isLoggedIn()) {
      Post.subscribe().then(() => {
        this.observe();
      });
    }
  }

  observe() {
    Post.observe(results => {
      console.log("Timeline observed");
      let data = this.getDataToBeDisplayed(results);
      if (JSON.stringify(data) != JSON.stringify(this.state.posts)) {
        StoreCache.storeCache('posts', data);
        if (this._isMounted) {
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(data),
            posts: data,
            isAllLoaded: true,
            isLoadedFromCache: false
          });
        }
      }
    });
  }

  getDataToBeDisplayed(posts) {
    var data = [];
    for (var i = 0, postsLength = posts.length; i < postsLength; i++) {
      let post = posts[i];
      let user = Users.getItemById(post.UserFID);
      user = (user) ? user : _.find(this.state.contacts, function(contact){ return contact._id == post.UserFID; });;

      if (user) {
        let postImagesRaw = _.compact(_.pluck(post.Content.Media, 'image'));
        let postVideosRaw = _.compact(_.pluck(post.Content.Media, 'video'));
        let id = post._id;
        let media = _.union(postVideosRaw, postImagesRaw);
        let link = post.Content.Link;
        let sticker = post.Content.Sticker;
        let text = post.Content.Text;
        let isProfilePicture = ( typeof post.Content.isProfilePicture != 'undefined' ) ? post.Content.isProfilePicture : null;
        let userImage = (user.image) ? user.image : GLOBAL.DEFAULT_IMAGE;
        let userName = user.name;//getDisplayName(user);
        let CreateDate = post.CreateDate;
        let reactions = post.Reaction;
        let comments = post.Comments;

        let p = {
          id: id,
          isProfilePicture: isProfilePicture,
          link: link,
          media: media,
          sticker: sticker,
          text: text,
          userImage: userImage,
          userName: userName,
          CreateDate: CreateDate,
          reactions: reactions,
          comments: comments,
        };
        data.push(p);
      }
    }
    return data;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor: '#f4f1f1'}]}>
        {this.renderTopBar()}
        {this.renderStatusBox()}
        {this.renderContent()}
        {this.renderReactions()}
        {this.renderModal()}
      </View>
    );
  }

  renderTopBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (Platform.OS !== 'ios') return;
    return (
      <View>
        <NavigationBar
          tintColor={color.THEME}
          title={{ title:'Timeline', tintColor:color.BUTTON_TEXT}}
        />
        {this.props.renderConnectionStatus()}
      </View>
    )
  }

  renderStatusBox() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let loggedInUser = this.state.loggedInUser;
    let image = (loggedInUser == null || loggedInUser.profile.image == '') ? GLOBAL.DEFAULT_IMAGE : loggedInUser.profile.image;
    var newToday = 'What\'s new today?';
    return (
      <TouchableHighlight onPress={() => Navigation.onPress(() => this.goToTimelinePost())} underlayColor='transparent'>
        <View style={{flexDirection:'row',backgroundColor: color.CHAT_BG,marginBottom:3, padding:10, alignItems:'center'}}>
          <Text style={{fontSize: 16, color: color.TEXT_LIGHT}}>{newToday}</Text>
          <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end'}}>
            <KlikFonts
              name="emoticon"
              color={color.TEXT_LIGHT}
              style={{marginLeft:0, alignSelf:'center'}}
              size={18}
              onPress={() => Navigation.onPress(() => this.setCommentActive('stickers'))}
            />
            <KlikFonts
              name="photo"
              color={color.TEXT_LIGHT}
              style={{marginLeft:8, alignSelf:'center'}}
              size={22}
              onPress={() => Navigation.onPress(() => this.setCommentActive('image'))}
            />
            <KlikFonts
              name="link"
              color={color.TEXT_LIGHT}
              style={{marginLeft:6, alignSelf:'center'}}
              size={22}
              onPress={() => Navigation.onPress(() => this.setCommentActive('link'))}
            />
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  renderModal() {
    if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ListView ref='listview'
        automaticallyAdjustContentInsets={false}
        contentInset={{bottom:50}}
        dataSource={this.state.dataSource}
        enableEmptySections={true}
        initialListSize={10}
        removeClippedSubviews={false}
        renderFooter={this.renderFooter}
        renderRow={this.renderRow}
        onPress={() => Navigation.onPress(() => this.setState({reactToggled:false}))}
        onScroll={() => {this.setState({
              reactHeight:0,
              reactToggled: false
            });
        }}
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.state.dataSource.getRowCount(0) == 0) ? 'No post yet.' : null;
    return renderFooter(this.state.isAllLoaded, color, this.props.hasInternet(), notFoundText);
  }

  renderRow(data) {
    return (
      <TimelinePost key={data.id} data={data}
        theme={this.props.theme}
        navigator={this.props.navigator}
        isOnComment={false}
        commentOnPost={this.commentOnPost}
        setIsShowReactions={this.setIsShowReactions}
      />
    );
  }

  renderReactions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.reactToggled) {
      return <TimelineReact
                hasInternet={this.props.hasInternet}
                postId={this.state.postId}
                style={{height:this.state.reactHeight}}
                closeReact={this.closeReact}
                reactTopPosition={this.state.reactTopPosition}
                color={color} />;
    } else {
      return <View/>;
    }
  }

  setCommentActive(active) {
    if (this._isMounted) {
      this.setState({commentActive: active});
      this.goToTimelinePost();
    }
  }

  commentActive() {
    return this.state.commentActive;
  }

  commentOnPost(post){
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this._isMounted) {
      this.setState({post: post});
    }
    requestAnimationFrame(() => {
      this.closeReact('comments');
      this.props.navigator.push({
        id: 'timelinepostcomments',
        name:'TimelinePostComments',
        color: color,
        hasInternet: this.props.hasInternet,
        isOnComment: true,
        isSafeToBack: this.props.isSafeToBack,
        post: this.post,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setIsShowReactions: this.setIsShowReactions,
        theme: this.props.theme,
      })
    })
  }

  post() {
    return this.state.post;
  }

  closeReact(goToComments = null) {
    if (this._isMounted) {
      this.setState({
        postId: null,
        currentPostId: 0,
        reactHeight: 0,
        reactToggled: false,
        reactTopPosition: 0,
      });
    }
    if (!this.props.hasInternet() && !goToComments) {
      this.setIsShowCheckConnection();
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowReactions(postInfo) {
    if (this._isMounted) {
      if (this.state.currentPostId != postInfo.id){
        console.log('always show reactions');
        this.setState({
          currentPostId: postInfo.id,
          postId: postInfo.id,
          reactHeight: 50,
          reactToggled: true,
          reactTopPosition: (postInfo.y + postInfo.height),
        });
      }else {
        console.log('toggle reactions');
        this.setState({
          postId: postInfo.id,
          reactHeight: 50,
          reactToggled: !this.state.reactToggled,
          reactTopPosition: (postInfo.y + postInfo.height),
        });
      }

    }
  }

  goToTimelinePost() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      requestAnimationFrame(() => {
        this.props.navigator.push({
          id: 'timelineposter',
          name: 'TimelinePoster',
          commentActive: this.commentActive,
          hasInternet: this.props.hasInternet,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setIsSafeToBack: this.props.setIsSafeToBack,
          theme: this.props.theme
        });
      });
    }
  }
}

// Export module
module.exports = Timeline;
