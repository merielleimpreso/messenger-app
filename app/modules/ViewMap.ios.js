/*

Author: Merielle N. Impreso
Date Created: 2016-06-08
Date Updated: 2016-06-09
Description: Display the location in a map.
Used In: ChatRow.js

*/


import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';

import React, {
  Component,
} from 'react';

import {
  MapView,
  StyleSheet,
  View
} from 'react-native';

var styles = StyleSheet.create(require('./../styles/styles_common.js'));

var ViewMap = React.createClass({

  render() {
    var location = this.props.location;
    var color = this.props.color;
    return (
      <View style={styles.containerNoResults}>
        <NavigationBar
          tintColor={color.THEME}
          title={{ title: 'View Location', tintColor: color.BUTTON_TEXT}}
          leftButton={
            <KlikFonts
            name="back"
            color={color.BUTTON_TEXT}
            size={35}
            onPress={() => this.props.navigator.pop()}
           />
          }
        />
        <MapView style={styles.map}
         region={{
           latitude: location.latitude,
           longitude: location.longitude,
           latitudeDelta: 0.001,
           longitudeDelta: 0.001
         }}
         annotations={[{
           longitude: location.longitude,
           latitude: location.latitude,
           title: 'I am here',
           draggable: false,
         }]}
         pitchEnabled={false}
         rotateEnabled={false}
         scrollEnabled={false}
         zoomEnabled={false}
       />
     </View>
    );
  }

});

module.exports = ViewMap;
