/**
 * Author: Ida Jimenez
 * Date Created: 2016-07-19
 * Date Updated: N/A 
 * Description: Component used for playing video
 */
'use strict';

import React, { Component } from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid
} from 'react-native';

const {width, height} = Dimensions.get('window');
let GLOBAL = require('./config/Globals.js');
import {Video} from 'react-native-media-kit';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Toolbar from './common/Toolbar';

class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      muted: false,
      width: width,
      height: width / (9.7/9),
      controls: true
    }
  }

  render() {
    let color = this.props.color;
    return (
      <View style={{height: height, backgroundColor: 'black' }} >
        <Toolbar
          title={'Play Video'}
          withBackButton={true}
          {...this.props}
        />
        {(this.props.data.indexOf('http') !== -1 && this.props.hasInternet() || this.props.data.indexOf('http') == -1 ) 
          ? <Video
            style={{width: width, height: height - 85, backgroundColor: 'black'}}
            autoplay={false}
            preload='auto'
            loop={false}
            controls={this.state.controls}
            muted={this.state.muted}
            src={this.props.data}
            audiocontrols={false}
          />
          : <Text style={styles.text}>Cannot play video</Text>
        }
      </View>
    );
  }
}

let styles = StyleSheet.create({
  topContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 18
  }
});

module.exports = VideoPlayer;
