'use strict';
import _ from 'underscore';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  InteractionManager,
  Image,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import {dateToTime, getMessage, getMessageBadge, getMediaURL, logTime, renderFooter, toNameCase} from '../Helpers';
import ActivityView from 'react-native-activity-view';
import ButtonIcon from '../common/ButtonIcon';
import ContactsManager from 'react-native-contacts';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from '../common/ListRow';
import NavigationBar from 'react-native-navbar';
import SearchBar from '../SearchBar';
import Users from '../config/db/Users';

const GLOBAL = require('../config/Globals.js');
const styles = StyleSheet.create(require('../../styles/styles_common.js'));

class ContactsInvite extends Component {

  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      }),
      isLoading: true,
      isLoadingSearch: false,
      theme: this.props.theme(),
      loading: true,
      isLoaded: false
    }
    this.getAllContacts = this.getAllContacts.bind(this);
    this.getCategoryMap = this.getCategoryMap.bind(this);
    this.getContactsFromDatabase = this.getContactsFromDatabase.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  // Put a flag to check if component is mounted
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getAllContacts();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllContacts() {
    ContactsManager.getAll((err, results) => {
      if(err && err.type === 'permissionDenied') {
        consoleLog('Contacts: contacts denied');
      } else {
        let contactsFromDB = this.getContactsFromDatabase(this.props.loggedInUser().contacts);
        var contacts = [];

        for (var i = 0; i < results.length; i++) {
          let name = toNameCase(results[i].givenName) + ' ' + toNameCase(results[i].familyName);
          let image = (results[i].thumbnailPath.length > 0) ? results[i].thumbnailPath.length : GLOBAL.DEFAULT_IMAGE;

          var isContact = false;
          let phoneNumbers = results[i].phoneNumbers;
          var selectedIndex = 0;
          for (var j = 0; j < phoneNumbers.length; j++) {
            isContact = this.isContact(phoneNumbers[j].number, contactsFromDB);
            if (isContact) {
              selectedIndex = j;
              break;
            }
          }
          let number = phoneNumbers[selectedIndex].number;
          var isOnKlikChat = false;
          Users.subscribeToUsername(number).then(() => {
            let user = Users.getItemByUsername(number);
            let isOnKlikChat = (user != null);

            let contact = {
              name: name,
              number: number,
              isContact: isContact,
              isOnKlikChat: isOnKlikChat,
              image: image,
              category: isOnKlikChat ? 'Already on KlikChat' : 'On Your Phone',
              user: user
            }
            if (!isContact && !isOnKlikChat) {
              contacts.push(contact)
            }
            contacts = _.sortBy(_.sortBy(contacts, 'name'), 'category');
            if (this._isMounted) {
              this.setState({
                contacts: contacts,
                dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getCategoryMap(contacts)),
                isLoading: false
              });
            }
          });
        }
      }
    });
  }

  getCategoryMap(contacts) {
    _.each(_.values(contacts), function(c) {
      c['category'] = c.name.charAt(0).toUpperCase();
    });
    var categoryMap = {};
    contacts.forEach(function(item) {
      if (!categoryMap[item.category]) {
        categoryMap[item.category] = [];
      }
      categoryMap[item.category].push(item);
    });
    return categoryMap;
  }

  getContactsFromDatabase(contactIds) {
    var contacts = [];

    for (var i = 0; i < contactIds.length; i++) {
      let c = _.findWhere(this.props.users(), {_id:contactIds[i]});
      if (c) {
        let contact = {
          _id: c._id,
          name: toNameCase(c.profile.firstName) + ' ' + toNameCase(c.profile.lastName),
          number: c.username,
          isContact: true,
          isOnKlikChat: true,
          image: (c.profile.photoUrl) ? c.profile.photoUrl : GLOBAL.DEFAULT_IMAGE,
          user: c,
          category: 'Friend'
        };
        contacts.push(contact);
      }
    }
    return contacts;
  }

  isContact(number, contacts) {
    for (var i = 0; i < contacts.length; i++) {
      if (number == contacts[i]._id) {
        return true;
      }
    }
    return false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{title:'Phonebook Contact', tintColor:color.BUTTON_TEXT}}
         leftButton={<ButtonIcon icon='back' color={color} onPress={this.onPressButtonClose}/>} />
        {this.renderSearchBar()}
        {this.renderLoading()}
        <ListView ref="listview"
         dataSource={this.state.dataSource}
         renderRow={this.renderRow}
         renderSectionHeader={this.renderSectionHeader}
         onEndReached={this.onEndReached}
         automaticallyAdjustContentInsets={false}
         keyboardDismissMode="on-drag"
         keyboardShouldPersistTaps={true}
         showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  renderSearchBar() {
    return (
      <SearchBar
       theme={this.props.theme}
       onSearchChange={this.onSearchChange}
       isLoading={this.state.isLoadingSearch}
       placeholder='Search'
       onFocus={() =>
        this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
     />
    );
  }

  renderLoading() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.isLoading) {
      return (
        <ActivityIndicator animating={this.state.isLoading}
         color={color.BUTTON}
         style={[styles.searchBarSpinner, {alignSelf:'center'}]}/>
      );
    } else {
      return null;
    }
  }

  renderSectionHeader(sectionData, category) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.contactsSectionHeader, {borderTopColor:color.INACTIVE_BUTTON, backgroundColor:color.CHAT_BG}]}>
        <Text style={{flex:1, fontSize:13, color:color.TEXT_LIGHT, fontWeight:'500', margin:5, marginRight:10, alignSelf:'center'}}>{category}</Text>
      </View>
    );
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <TouchableHighlight onPress={() => this.inviteContact()} underlayColor={color.HIGHLIGHT}>
        <View>
          <View style={styles.contactsListViewRow}>
            <Image source={{uri:data.image}} style={styles.listViewImage}
              onLoadStart={(e) => this.setState({loading: true})}
              onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
              onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
              onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
              {(this.state.loading && !this.state.isLoaded)
                ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:40, width:40}}>
                    <ActivityIndicator size="small" color={color.BUTTON} />
                  </View>
                : null
              }
            </Image>
            <View style={styles.listViewRowContainer}>
              <Text numberOfLines={1} style={[styles.chatRecentTextName, {color:color.TEXT_DARK}]}>{data.name}</Text>
              <Text numberOfLines={1} style={[styles.chatRecentTextMessage, {color:color.TEXT_LIGHT}]}>{data.number}</Text>
            </View>
            <KlikFonts name="addcontact" color={color.BUTTON} size={25} style={{alignSelf:'center'}} />
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  onSearchChange(searchText) {
    this.setState({isLoadingSearch: true});
    var contacts = _.filter(this.state.contacts, function(contact){
      return (contact.name).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });

    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getCategoryMap(contacts)),
        isLoadingSearch: false
      });
    }
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  inviteContact() {
    ActivityView.show({
      text: 'Join me on Messenger!',
      url: 'https://aeustech.com',
      imageUrl: 'https://facebook.github.io/react/img/logo_og.png'
    });
  }

}

// Export module.
module.exports = ContactsInvite;
