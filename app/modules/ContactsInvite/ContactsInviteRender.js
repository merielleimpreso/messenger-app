'use strict';

import React, {
  Component,
} from 'react';
import {
  ListView,
  Platform,
  RefreshControl,
  View,
} from 'react-native';
import _ from 'underscore';
import {getMediaURL, renderFooter, toNameCase} from '../Helpers';
import GLOBAL from '../config/Globals.js';
import ListRow from '../common/ListRow';
import RenderModal from '../common/RenderModal';
import ScreenNoItemFound from '../ScreenNoItemFound';
import styles from './styles';

class ContactsInviteRender extends Component {

  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let screenNoItemFound = null;
    let listStyle = {};
    if (!this.props.isLoading && (this.props.dataSource.getRowCount() == 0)) {
      screenNoItemFound = <ScreenNoItemFound isLoading={false} text={'No contact save on this phone'} color={color}/>;
      listStyle = { flex: 1, height: 500 };
    }

    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        {screenNoItemFound}
        <ListView ref='listview'
          automaticallyAdjustContentInsets={false}
          dataSource={this.props.dataSource}
          enableEmptySections={true}
          initialListSize={10}
          removeClippedSubviews={false}
          renderRow={this.renderRow}
          refreshControl={
            <RefreshControl
              refreshing={this.props.isLoading}
              onRefresh={this.onRefresh.bind(this)}
              colors={[color.THEME]}
            />
          }
          style={listStyle}
        />
        {this.renderModal()}
      </View>
    )
  }

  onRefresh() {
    this.props.onRefresh();
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let image = (_.isEmpty(data.image)) ? GLOBAL.DEFAULT_PROFILE_IMAGE : data.image;
    if (Platform.OS == 'android') {
      image = (image.indexOf('content') !== -1) ? image : getMediaURL(image);
    } else {
      image = (image.indexOf('/var/mobile/Containers/Data/Application/') !== -1) ? image : getMediaURL(image);
    }

    let id = data._id;
    let buttonLabel = (_.contains(this.props.isContact, id)) ? 'Chat'
                        : (_.contains(this.props.isIgnored, id)) ? 'Chat'
                          : (_.contains(this.props.isFriendRequest, id)) ? 'Accept\nRequest'
                            : (_.contains(this.props.isSentRequest, id)) ? 'Chat'
                              : (_.contains(this.props.isMember, id)) ? 'Add' : 'Invite';

    return (
      <ListRow
        color={color}
        labelText={toNameCase(data.name)}
        detailText={data.mobile}
        image={image}
        button={buttonLabel}
        iconPress={() => this.props.onIconPress(data, buttonLabel)}
        addSeparator
      />
    );
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.props.dataSource.getRowCount(0) == 0) ? 'No contact save on this phone.' : null;
    return renderFooter(!this.props.isLoading, color, this.props.hasInternet(), notFoundText);
  }

  renderModal() {
    if (this.props.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.props.isShowContactProfile) {
      return (
        <RenderModal
          theme={this.props.theme}
          profileData={this.props.contact}
          setIsShowModal={() => this.props.setIsShowContactProfile(null)}
          modalType={'viewProfile'}
        />
      );
    } else if (this.props.isShowLoading) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    }
  }

}

// Export module.
module.exports = ContactsInviteRender;
