'use strict';

import React, {
  Component,
} from 'react';
import {
  InteractionManager,
  ListView,
  Platform
} from 'react-native';
import _ from 'underscore';
import {toPhoneNumber, toNameCase} from '../Helpers';
import ContactsManager from 'react-native-contacts';
import ContactRetriever from './ContactRetriever';
import ContactsInviteRender from './ContactsInviteRender';
import GLOBAL from '../config/Globals.js';
import SendIntentAndroid from 'react-native-send-intent';
import StoreCache from './../config/db/StoreCache';

class ContactsInvite extends Component {

  constructor(props) {
    super(props);
    this.state = {
      contact: null,
      contacts: null,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isLoading: true,
      isShowContactProfile: false,
      isShowLoading: false,
      phoneContacts: null,
      isContact: [],
      isFriendRequest: [],
      isIgnored: [],
      isSentRequest: [],
      isMember: [],
    }
    this.acceptRequest = this.acceptRequest.bind(this);
    this.retrievePhoneContacts = this.retrievePhoneContacts.bind(this);
    this.getPhoneContacts = this.getPhoneContacts.bind(this);
    this.checkIfRegistered = this.checkIfRegistered.bind(this);
    this.onIconPress = this.onIconPress.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.processContact = this.processContact.bind(this);
    this.setEmptyContacts = this.setEmptyContacts.bind(this);
    this.setIsShowContactProfile = this.setIsShowContactProfile.bind(this);
    this.ignoreRequest = this.ignoreRequest.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.setIsShowLoading = this.setIsShowLoading.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
        StoreCache.getCache(StoreCache.keys.phoneContacts, (results) => {
          if (results) {
            this.setRelationshipStates(results);
          }
        });

    InteractionManager.runAfterInteractions(() => {
      this.retrievePhoneContacts();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  retrievePhoneContacts() {
    if (this._isMounted) {
      this.setState({
        isLoading: true
      });
    }

    if (Platform.OS == 'android') {
      ContactRetriever.getAll((err, results) => {
        if (err && err.type === 'permissionDenied') {
          console.warn('Contacts: contacts denied');
          this.setEmptyContacts();
          Alert.alert("Oops!", "You have denied access to your contacts. Kindly check your settings.", [
            {text: "OK", onPress: () => {}}
          ]);
        } else {
          if (results && this.props.hasInternet()) {
            this.checkIfRegistered(this.getPhoneContacts(results));
          } else {
            this.setEmptyContacts();
          }
        }
      });
    } else {
      ContactsManager.getAll((err, results) => {
        if(err && err.type === 'permissionDenied') {
          console.warn('Contacts: contacts denied');
          Alert.alert("Oops!", "You have denied access to your contacts. Kindly check your settings.", [
            {text: "OK", onPress: () => {}}
          ]);
          this.setEmptyContacts();
        } else {
          if (results) {
            let resultsTmp = [];
            for (let i = 0; i < results.length; i++) {
              if (!_.isEmpty(results[i].phoneNumbers)) {
                resultsTmp.push({
                  username: toNameCase(results[i].givenName) + ' ' + toNameCase(results[i].familyName),
                  photo: (results[i].thumbnailPath.length > 0) ? results[i].thumbnailPath : null,
                  phoneNumbers: _.pluck(results[i].phoneNumbers, "number")
                });
              }
            }
            this.checkIfRegistered(this.getPhoneContacts(resultsTmp));
          } else {
            this.setEmptyContacts();
          }
        }
      });
    }
  }

  setEmptyContacts() {
    if (this._isMounted) {
      this.setState({
        contacts: [],
        isLoading: false
      });
    }
  }

  getPhoneContacts(results) {
    let loggedInUserMobile = this.props.loggedInUser().mobile;
    let phoneContacts = [];
    for (let i = 0; i < results.length; i++) {
      let mobile = toPhoneNumber(results[i].phoneNumbers[0].replace("+", ""));
      if (mobile != loggedInUserMobile) {
        results[i].phoneNumbers[0] = mobile;
        phoneContacts.push(results[i]);
      }
    }
    return phoneContacts;
  }

  checkIfRegistered(phoneContacts) {
    ddp.call('checkIfRegistered', [phoneContacts]).then((contacts) => {
      let friendRequestList = _.pluck(this.props.loggedInUser().friendRequestReceived, 'userId');
      let ignoredRequestList = _.reject(this.props.loggedInUser().friendRequestReceived, function(i){ return i.date != null; });
      let ignoredRequestIds = _.pluck(ignoredRequestList, 'userId');
      let sentRequestList = _.pluck(this.props.loggedInUser().friendRequestSent, 'userId');

      for (let i = 0; i < contacts.length; i++) {
        let contact = contacts[i];
        contact['isContact'] = _.contains(this.props.loggedInUser().contacts, contact._id);
        contact['isFriendRequest'] = _.contains(friendRequestList, contact._id);
        contact['isIgnored'] = _.contains(ignoredRequestIds, contact._id);
        contact['isSentRequest'] = _.contains(sentRequestList, contact._id);
      }

      let isContact = _.pluck(_.where(contacts, {'isContact': true}), '_id');
      let isFriendRequest = _.pluck(_.where(contacts, {'isFriendRequest': true}), '_id');
      let isIgnored = _.pluck(_.where(contacts, {'isIgnored': true}), '_id');
      let isSentRequest = _.pluck(_.where(contacts, {'isSentRequest': true}), '_id');
      let isMember = _.pluck(_.where(contacts, {'isMember': true}), '_id');
      let data = {
        contacts: _.sortBy(contacts, 'name'),
        isContact: isContact,
        isFriendRequest: isFriendRequest,
        isIgnored: isIgnored,
        isMember: isMember,
        isSentRequest: isSentRequest
      }
      this.setRelationshipStates(data);
    }).catch(error => {
      console.log('ContactsInvite: checkIfRegistered error = ' + error);
    });
  }

  setRelationshipStates(data) {
    if (this._isMounted) {
      StoreCache.storeCache(StoreCache.keys.phoneContacts, data);
      this.setState({
        contacts: data.contacts,
        dataSource: this.state.dataSource.cloneWithRows(data.contacts),
        isContact: data.isContact,
        isFriendRequest: data.isFriendRequest,
        isIgnored: data.isIgnored,
        isSentRequest: data.isSentRequest,
        isMember: data.isMember,
        isLoading: false
      });
    }
  }

  render() {
    return (
      <ContactsInviteRender
        color={this.props.color}
        contact={this.state.contact}
        contacts={this.state.contacts}
        dataSource={this.state.dataSource}
        hasInternet={this.props.hasInternet}
        isContact={this.state.isContact}
        isFriendRequest={this.state.isFriendRequest}
        isIgnored={this.state.isIgnored}
        isLoading={this.state.isLoading}
        isMember={this.state.isMember}
        isSentRequest={this.state.isSentRequest}
        isShowLoading={this.state.isShowLoading}
        isShowContactProfile={this.state.isShowContactProfile}
        loggedInUser={this.props.loggedInUser}
        onIconPress={this.onIconPress}
        onRefresh={this.onRefresh}
        renderConnectionStatus={this.props.renderConnectionStatus}
        theme={this.props.theme}
      />
    );
  }

  onRefresh() {
    if (this._isMounted) {
      this.setState({
        isLoading: true
      });
    }
    this.retrievePhoneContacts();
  }

  onIconPress(data, action) {
    if (action == 'Chat') {
      this.goToMessageChat(data);
    } else if (action == 'Accept Request') {
      this.setIsShowContactProfile(data);
    } else if (action == 'Add') {
      this.setIsShowContactProfile(data);
    } else if (action == 'Invite') {
      this.inviteContact(data.mobile, data.name);
    }
  }

  goToMessageChat(data) {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'messagesgiftedchat',
        name:'MessagesGiftedChat',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeBgImage: this.props.changeBgImage,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        hasInternet: this.props.hasInternet,
        isAutoSaveMedia: this.props.isAutoSaveMedia,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setLoggedInUser: this.props.setLoggedInUser,
        setIsViewVisible: this.props.setIsViewVisible,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        user: data
      });
    });
  }

  setIsShowContactProfile(data) {
    let profileData = null;
    if (data) {
      let actions = [];
      if (data.isFriendRequest) {
        actions = [{icon: 'block', iconLabel: 'Block', onPressIcon: () => this.ignoreRequest(data._id)},
                   {icon: 'check', iconLabel: 'Accept Request', onPressIcon: () => this.acceptRequest(data._id)}];
      } else {
        actions = [{icon: 'close', iconLabel: 'Cancel', onPressIcon: () => this.setIsShowContactProfile(null)},
                   {icon: 'check', iconLabel: 'Send Request', onPressIcon: () => this.sendRequest(data._id)}];
      }

      profileData = {
        data: data,
        actions: actions,
        relStatus: null
      }
    }

    if (this._isMounted) {
      this.setState({
        isShowContactProfile: !this.state.isShowContactProfile,
        contact: profileData
      });
    }
  }

  setIsShowLoading() {
    if (this._isMounted) {
      this.setState({
        isShowLoading: !this.state.isShowLoading
      });
    }
  }

  ignoreRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('ignoreFriendRequest', [id]).then(() => {
        console.log('Successfully ignored friend request.')
        //ddp.collections.contacts.remove({_id: id});
        this.processContact('ignored', id);
        this.setIsShowLoading();
      });
      this.setIsShowContactProfile(null);
    }
  }

  acceptRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('addToContacts', [id]).then(res => {
        if (res) {
          console.log('Successfully accepted friend request.')
          ddp.collections.contacts.upsert(res);
          this.processContact('accepted', id);
          this.setIsShowLoading();
        }
      });
      this.setIsShowContactProfile(null);
    }
  }

  sendRequest(id) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowLoading();
      ddp.call('addFriendRequest', [id]).then(result => {
        if (result) {
          console.log('Successfully sent friend request.');
          this.processContact('sent', id);
          this.setIsShowLoading();
        }
      });
      this.setIsShowContactProfile(null);
    }
  }

  processContact(processed, id) {
    let contacts = this.state.contacts;
    let isContact = this.state.isContact;
    let isSentRequest = this.state.isSentRequest;
    let isFriendRequest = this.state.isFriendRequest;
    let isIgnored = this.state.isIgnored;
    let isMember = this.state.isMember;

    if (processed == 'accepted') {
      isContact.splice();
      isContact.push(id);
    } else if (processed == 'sent') {
      isSentRequest.splice();
      isSentRequest.push(id);
    } else if (processed == 'ignored') {
      isIgnored.splice();
      isIgnored.push(id);
    }

    let data = {
      contacts: _.sortBy(contacts, 'name'),
      isContact: isContact,
      isFriendRequest: isFriendRequest,
      isIgnored: isIgnored,
      isMember: isMember,
      isSentRequest: isSentRequest
    }

    if (this._isMounted) {
      this.state.dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      });
      this.setRelationshipStates(data);
    }
  }

  inviteContact(contactNumber, contactName) {
    SendIntentAndroid.sendText({
      title: "Invite "+contactName,
      text: "Join me on KlikChat "+contactName+"! <downloadLink>",
      type: SendIntentAndroid.TEXT_PLAIN
    });
  }
}

// Export module.
module.exports = ContactsInvite;
