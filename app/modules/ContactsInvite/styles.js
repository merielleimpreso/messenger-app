import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  containerNoResults: {
    flex: 1,
    alignItems: 'stretch'
  },
});
