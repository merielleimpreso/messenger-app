'use strict';

import React, {
  Component
} from 'react';
import {
  ActivityIndicator,
  Alert,
  AsyncStorage,
  Dimensions,
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {logTime, toLowerCase} from './Helpers';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import Navigation from './actions/Navigation';
import NavigationBar from 'react-native-navbar';
import Tabs from './Tabs';
import Users from './config/db/Users';
import RenderModal from './common/RenderModal';
import SystemAccessor from './SystemAccessor';

var SCREEN = Dimensions.get("window");
var GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: '',
      enteredCode: '',
      number: '',
      password: '',
      isDoneLoadingUsers: false,
      isLoggingIn: false,
      isVerifying: false,
      isShowCheckConnection: false,
      isShowLoading: false,
      verificationCode: '',
      modalEmailVisible: false,
      modalMobileVisible: false,
      modalNotifyVisible: false,
      isLoggedIn: false,
      loading: true,
      isLoaded: false,
    }
    this.callLoginWithUsername = this.callLoginWithUsername.bind(this);
    this.forgetPassword = this.forgetPassword.bind(this);
    this.goToSignup = this.goToSignup.bind(this);
    this.renderLoginForm = this.renderLoginForm.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowLoading = this.setIsShowLoading.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.setModalEmailVisible = this.setModalEmailVisible.bind(this);
    this.setModalNotifyVisible = this.setModalNotifyVisible.bind(this);
    this.signupKliklabUser = this.signupKliklabUser.bind(this);
    this.logInFailure = this.logInFailure.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    console.log('-----unmount login')
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.isLoggedIn !== this.props.isLoggedIn()) {
      this.goToTabs();
    }
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.container, {backgroundColor:GLOBAL.COLOR_CONSTANT.CHAT_BG}]}>
        {(Platform.OS == 'ios')
          ? <NavigationBar tintColor={color.THEME}
           title={{title:'Kliklabs', tintColor: color.BUTTON_TEXT}}
           leftButton={<ButtonIcon icon='back' size={25} color={color} onPress={() => this.onPressButtonClose()}/>} />
          : null
        }
        {this.props.renderConnectionStatus()}
        {this.renderLoginForm()}
        {this.renderModal()}
      </View>
    );
  }

  renderLoginForm() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
          <ScrollView keyboardDismissMode='interactive'
              keyboardShouldPersistTaps={true}
              style={{flex: 1}}>
            <View style={styles.loginContainer}>
              <Image style={{width:SCREEN.width*0.5, height:SCREEN.height*0.2, resizeMode:'contain'}} source={require('./../images/kclogotext.png')} />
              <Text style={[styles.error, {color:GLOBAL.COLOR_CONSTANT.ERROR, marginTop:30}]} allowFontScaling={true}>{this.state.error}</Text>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput ref="number"
                  onChange={(event) => {
                    let text = event.nativeEvent.text;
                    text = (text.trim() == '') ? '' : text;
                    this.setState({
                      number:  text
                    });
                  }}
                  onEndEditing={(event) => {
                    this.setState({
                      number:  event.nativeEvent.text.toLowerCase()
                    })
                  }}
                  style={[styles.input, {backgroundColor: 'transparent'}]}
                  underlineColorAndroid={GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}
                  placeholder='Username or Phone Number'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER}
                  autoCapitalize='none'
                  returnKeyType='next'
                  onSubmitEditing={(event) => {this.refs.password.focus();}}
                  allowFontScaling={true}
                  autoFocus={true}
                  value={this.state.number}
                  />
              </View>
              <View style={[styles.inputLoginContainer, {borderBottomColor: GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}]}>
                <TextInput ref="password"
                  onChangeText={(password) => {
                    password = (password.trim() == '') ? '' : password;
                    this.setState({password: password})
                  }}
                  style={[styles.input, {backgroundColor: 'transparent'}]}
                  underlineColorAndroid={GLOBAL.COLOR_CONSTANT.INACTIVE_BUTTON}
                  placeholder='Password'
                  placeholderTextColor={GLOBAL.COLOR_CONSTANT.TEXT_INPUT_PLACEHOLDER}
                  autoCapitalize='none'
                  onSubmitEditing={(event) => this.refs.password.blur()}
                  secureTextEntry
                  returnKeyType='go'
                  allowFontScaling={true}
                  value={this.state.password}
                />
              </View>

              <Button text="Log In" color={GLOBAL.COLOR_CONSTANT.THEME}
                underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
                onPress={() => this.onPressedLogin()} style={{marginTop:15}}
                isDefaultWidth={true}/>

              {<TouchableWithoutFeedback onPress={() => Navigation.onPress(() => this.setModalEmailVisible(true))}>
                <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
                  <Text style={{color: GLOBAL.COLOR_CONSTANT.BUTTON_TEXT_LIGHT, fontSize: 10}} allowFontScaling={true}>
                    Having trouble?
                  </Text>
                </View>
              </TouchableWithoutFeedback>}
            </View>
            {(Platform.OS == 'ios') ? <KeyboardSpacer/> : null}
        </ScrollView>
      </View>
    );
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.refs.password.blur();
      this.refs.number.blur();
      this.props.navigator.pop(0);
    });
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }

  setIsShowLoading() {
    if (this._isMounted) {
      this.setState({
        isShowLoading: !this.state.isShowLoading
      });
    }
  }

  renderModal() {
    if (this.state.modalEmailVisible) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setModalEmailVisible(false)}
          onPressForgetPassword={this.forgetPassword}
          modalType={'resetPassword'}
        />
      );
    } else if (this.state.modalMobileVisible) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.modalMobileVisible(false)}
          onPressMobileSubmit={this.mobileSubmit}
          modalType={'mobile'}
        />
      );
    } else if (this.state.modalNotifyVisible) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setModalNotifyVisible(!this.state.modalNotifyVisible)}
          message={'Kindly check your email for your temporary password.'}
          modalType={'alert'}
        />
      );
    } else if (this.state.isShowMessageModal) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowMessageModal("")}
          message={this.state.alertMessage}
          modalType={'alert'}
        />
      );
    } else if (this.state.isShowLoading) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => this.setIsShowLoading()}
          modalType={'loading'}
        />
      );
    } else if (this.state.isLoggingIn) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => console.log('logging in')}
          modalType={'loggingIn'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  // Check for blank input.
  onPressedLogin() {
    // if (Platform.OS == 'ios') {
    //     this.callLoginWithUsername(this.state.number, this.state.password);
    // } else {
      this.refs.password.blur();
      this.refs.number.blur();
      if (this.props.hasInternet()) {
        console.log('Login ['+ logTime() +']: Login button was clicked. Checking for blank input...');
        if (this.state.number == '') {
          console.log('Login ['+ logTime() +']: Login failed, number is blank');
          if (this._isMounted) {
            this.setState({
              error: 'Please enter your username or phone number',
              password: ''
            });
          }
          this.refs.number.focus();
          this.refs.password.clear();
        } else if (this.state.password == '') {
          console.log('Login ['+ logTime() +']: Login failed, password is blank');
          if (this._isMounted) {
            this.setState({
              error: 'Please enter your password',
              password: ''
            });
          }
          this.refs.password.focus();
        } else {
          if (this._isMounted) {
            this.setState({ error: '' });
          }
          this.loginWithUsername();
        }
      } else {
        this.setIsShowCheckConnection();
      }
    // }
  }

  // Call loginWithUsername function
  loginWithUsername() {
    console.log('Login ['+ logTime() +']: Logging in with username...');
    if (this._isMounted) {
      this.setState({
        isLoggingIn: true
      });
    }
    var toEncrypt = { username: this.state.number, password: this.state.password};

    SystemAccessor.encrypt(JSON.stringify(toEncrypt), GLOBAL.SECRET_KEY, (err ,apiLoginOptions) => {
      console.log('apiLoginOptions', apiLoginOptions)
      if (apiLoginOptions) {
        ddp.call('apiLogin', [Platform.OS, apiLoginOptions]).then((response) => {
          if(Object.prototype.toString.call(response) != "[object Object]") {

            console.log('apiLogin response: ', response);
            //ddp.call('checkPassword', [this.state.number, this.state.password]).then(() => {

              SystemAccessor.decrypt(response, GLOBAL.SECRET_KEY, (err, response) => {
                if (response) {
                  response = JSON.parse(response);

                  if(GLOBAL.TEST_MODE) {
                    // response.sukses = true;
                    console.log('--- TEST MODE Login - apiLogin response=', response);
                  }

                  let accessToken = response.access_token;
                  let apiLoginUser = response.username;

                  console.log("Access token: ", accessToken);
                  if (accessToken) { // User exists in the Kliklabs database
                    AsyncStorage.setItem('accessToken', accessToken);
                    let username = response.username;

                    ddp.call('apiCheckMobileVer', [accessToken]).then((response) => {
                      // console.log('apiCheckMobileVer: ', response);
                      if (response.mobile != false) {
                        let apiMobile = response.mobile;

                        ddp.call('getMobileByUsername', [username]).then((response) => {
                          // console.log('getMobileByUsername: ', response);
                          let kcMobile = response.mobile;

                          if (kcMobile == '-1') { // User does not exist in Klikchat, sign up user
                            this.signupKliklabUser(toEncrypt, apiMobile, accessToken, apiLoginUser);
                          } else { // User exists in Klikchat, try to login
                            if (apiMobile != kcMobile) { // Kliklabs mobile not equal to Klikchat mobile, update Klikchat mobile
                              ddp.call('updateMobileOnKlikchat', [username, apiMobile]).then(response => {
                                // console.log('updateMobileOnKlikchat', response);
                                if (response) {
                                  this.callLoginWithUsername(apiLoginUser, this.state.password);
                                }
                              })
                              .catch((err)=>{
                                if (this._isMounted) {
                                  this.setState({
                                    isLoggedIn: false,
                                    isLoggingIn: false,
                                    error: err[0]
                                  });
                                }
                                console.log("updateMobileOnKlikchat error: ", err);
                              });
                            } else {
                              this.callLoginWithUsername(apiLoginUser, this.state.password);
                            }
                          }
                        })
                        .catch((err)=>{
                          if (this._isMounted) {
                            this.setState({
                              isLoggedIn: false,
                              isLoggingIn: false,
                              error: err[0]
                            });
                          }
                          console.log("getMobileByUsername error: ", err);
                        });
                      } else {
                        SystemAccessor.decrypt(apiLoginOptions, GLOBAL.SECRET_KEY, (err, dcApiLoginOptions)=>{
                          dcApiLoginOptions = JSON.parse(dcApiLoginOptions);
                          dcApiLoginOptions.accessToken = accessToken;
                          this.goToSignup(dcApiLoginOptions,accessToken);
                          this.setState({
                            isLoggedIn: false,
                            isLoggingIn: false,
                          });
                        });
                      }
                    })
                    .catch((err)=>{
                      console.log("apiCheckMobileVer error: ", err);
                      if (this._isMounted) {
                        this.setState({
                          isLoggedIn: false,
                          isLoggingIn: false,
                          error: err[0]
                        });
                      }
                    });
                  } else { // User does not exist in the Kliklabs database
                    console.log("Decryption Error: ", response);
                    let message = response.message;
                    if (this._isMounted) {
                      this.setState({
                        isLoggedIn: false,
                        isLoggingIn: false,
                        error: 'Invalid username and password combination'
                      });
                    }
                  }
                } else {
                  console.log("Decryption Error: ", err);
                  if (this._isMounted) {
                    this.setState({
                      isLoggedIn: false,
                      isLoggingIn: false,
                      error: 'Something went wrong. Please try again.'
                    });
                  }
                }
              });
            /*}).catch((err) => {
              console.log("checkPassword Error: ", err);
              if (this._isMounted) {
                this.setState({
                  isLoggedIn: false,
                  isLoggingIn: false,
                  error: 'Something went wrong. Please try again.'
                });
              }
            });*/
          } else { // server returns error,wrong combination
            console.log('apiLogin error:', response)
            let message = response.message;
            if (this._isMounted) {
              this.setState({
              isLoggedIn: false,
              isLoggingIn: false,
              error: 'Invalid username and password combination'
              });
            }
          }
        }).catch((err) => {
          console.log("ERror: ", err);
          if (this._isMounted) {
            this.setState({
              isLoggedIn: false,
              isLoggingIn: false,
              error: 'Something went wrong. Please try again.'
            });
          }
        });
      } else {
        console.log("Encyption Error: ", err);
        if (this._isMounted) {
          this.setState({
            isLoggedIn: false,
            isLoggingIn: false,
            error: 'Something went wrong. Please try again.'
          });
        }
      }
    });
  }

  goToTabs() {
    console.log('Login ['+ logTime() +']: Go to tabs');
    this.props.navigator.immediatelyResetRouteStack([{
      name: 'Tabs',
      id: 'tabs',
      addContactToLoggedInUser: this.props.addContactToLoggedInUser,
      audio: this.props.audio,
      backgroundImageSrc: this.props.backgroundImageSrc,
      changeTheme: this.props.changeTheme,
      debugText: this.props.debugText,
      getMessageWallpaper: this.props.getMessageWallpaper,
      hasInternet: this.props.hasInternet,
      isDebug: this.props.isDebug,
      isLoggedIn: this.props.isLoggedIn,
      isSafeToBack: this.props.isSafeToBack,
      loggedInUser: this.props.loggedInUser,
      logout: this.props.logout,
      renderConnectionStatus: this.props.renderConnectionStatus,
      setAudio: this.props.setAudio,
      setIsSafeToBack: this.props.setIsSafeToBack,
      setLoggedInUser: this.props.setLoggedInUser,
      setSignUpCallback: this.props.setSignUpCallback,
      shouldReloadData: this.props.shouldReloadData,
      subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
      hasNewFriendRequest: this.props.hasNewFriendRequest,
      theme: this.props.theme,
      timeFormat: this.props.timeFormat,
      checkLoginToken: this.props.checkLoginToken
    }]);
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setModalEmailVisible(visible) {
    Linking.canOpenURL(GLOBAL.URL_HAVING_TROUBLE).then(supported => {
      if (supported) {
        Linking.openURL(GLOBAL.URL_HAVING_TROUBLE);
      } else {
        console.warn('Don\'t know how to open URI: ' + GLOBAL.URL_HAVING_TROUBLE);
      }
    });

    /*if (this._isMounted) {
      this.setState({modalEmailVisible: visible});
    }*/
  }

  setModalNotifyVisible(visible) {
    if (this._isMounted) {
      this.setState({modalNotifyVisible: visible});
    }
  }

  forgetPassword(email) {
    let emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email != undefined && emailPattern.test(email)) {
      this.setIsShowLoading()
      this.setModalEmailVisible(!this.state.modalEmailVisible);
      ddp.call('resetPass', [email])
      .then(result => {
        this.setIsShowLoading();
        if ( result == 'Password Sent') {
          if (this._isMounted) {
            this.setModalNotifyVisible(!this.state.modalNotifyVisible);
          }
        } else {
          this.setIsShowMessageModal("Email does not exist.")
        }
      })
      .catch(err => {
        this.setIsShowMessageModal('Something went wrong. Please try again.')
        console.log('[resetPass] Error: ', err.reason)
      });
    } else {
      this.setModalEmailVisible(!this.state.modalEmailVisible);
      this.setIsShowMessageModal("Invalid email address.")
    }
  }

  loggedInUser() {
    return this.state.loggedInUser;
  }

  users() {
    return this.state.users;
  }

  addContactToLoggedInUser(userId) {
    var loggedInUser = this.state.loggedInUser;
    loggedInUser.contacts.push(userId);
    if (this._isMounted) {
      this.setState({
        loggedInUser: loggedInUser
      });
    }
  }

  goToSignup(apiLoginOptions, accessToken) {

    requestAnimationFrame(() => {
      this.props.navigator.push({
        name:'KliklabsSignUp',
        id: 'kliklabssignup',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        debugText: this.props.debugText,
        getMessageWallpaper: this.props.getMessageWallpaper,
        hasInternet: this.props.hasInternet,
        isDebug: this.props.isDebug,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setLoggedInUser: this.props.setLoggedInUser,
        setSignUpCallback: this.props.setSignUpCallback,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        hasNewFriendRequest: this.props.hasNewFriendRequest,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        apiLoginOptions: apiLoginOptions,
        signupKliklabUser: this.signupKliklabUser,
        token: accessToken
      });
    });
  }

  signupKliklabUser(apiLoginOptions, mobile, accessToken, apiLoginUser) {
    console.log("apiLoginOptions.username: ", apiLoginOptions.username);
    if (this._isMounted) {
      this.setState({
        isLoggingIn: true
      });
    }

    ddp.call('apiGetProfile', [accessToken]).then(response => {
      console.log('apiGetProfile: ', response);

      let fullName = response.fullname.split(" ");
      let lastName = fullName[fullName.length-1];
      let firstName = '';

      for (let i = 0; i < fullName.length-1; i++) {
        firstName += fullName[i];
        if (i < fullName.length-1) {
          firstName += ' ';
        }
      }

      let toEncrypt = {
        mobile: mobile,
        password: apiLoginOptions.password,
        email: response.email,
        username: apiLoginOptions.username,
        name: fullName,
        photoUrl: null,
        isOnKlikLabs: true
      }

      console.log('Register kliklab user into klikchat database...');

      SystemAccessor.encrypt(JSON.stringify(toEncrypt), accessToken.substring(0,16),(err, options)=>{
        if (options) {
          ddp.call('apiRegisterUser', [options, accessToken]).then((toDecrypt) => {
            console.log("toDecrypt: ", toDecrypt);
            SystemAccessor.decrypt(toDecrypt, accessToken.substring(0,16), (err, response) => {
              response = JSON.parse(response);

              if(GLOBAL.TEST_MODE) {
                response.sukses = true;
                console.log('--- TEST MODE Login - apiRegisterUser response=', response);
              }

              if (response.sukses) {
                this.callLoginWithUsername(apiLoginUser, apiLoginOptions.password);
              } else {
                console.log('Login ['+ logTime() +']: Login failed: \n', response);
                this.setState({
                  isLoggedIn: false,
                  isLoggingIn: false,
                  error: response.msg
                });
                console.warn("Login Failed. Checking Token...");
                this.props.checkLoginToken();
              }
            });
          }).catch((err)=>{
            console.log("apiRegisterUser error: ", err);
          });
        } else {
          this.setState({
            isLoggedIn: false,
            isLoggingIn: false,
            error: 'Something went wrong. Please try again.'
          });
        }
      });
    });
  }

  callLoginWithUsername(username, password) {
    ddp.loginWithUsername(username, password, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        console.log("Login LoggedInWithUsername: ", res);
        this.props.subscribeToLoggedInUser();
        setTimeout(()=>{
          if (this._isMounted && this.state.isLoggingIn) {
            this.setState({
              isLoggedIn: false,
              isLoggingIn: false,
              error: "Invalid response from server. Retry or Restart suggested."
            });
          }
        }, 30000);
      } else {
        console.log('Login ['+ logTime() +']: Login failed: ' + err.reason);
        this.refs.number.focus();
        if (this._isMounted) {
          this.setState({
            isLoggedIn: false,
            isLoggingIn: false,
            error: err.reason
          })
        }
      }
    });
  }

  goToKliklabSignup(kliklabsUsername) {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name:'KliklabsSignUp',
        id: 'kliklabssignup',
        addContactToLoggedInUser: this.props.addContactToLoggedInUser,
        audio: this.props.audio,
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        debugText: this.props.debugText,
        getMessageWallpaper: this.props.getMessageWallpaper,
        hasInternet: this.props.hasInternet,
        isDebug: this.props.isDebug,
        isLoggedIn: this.props.isLoggedIn,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        logout: this.props.logout,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setAudio: this.props.setAudio,
        setIsSafeToBack: this.props.setIsSafeToBack,
        setLoggedInUser: this.props.setLoggedInUser,
        setSignUpCallback: this.props.setSignUpCallback,
        shouldReloadData: this.props.shouldReloadData,
        subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
        hasNewFriendRequest: this.props.hasNewFriendRequest,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        kliklabsUsername: kliklabsUsername
      });
    });
  }

  logInFailure(response){
    console.log('Login ['+ logTime() +']: Login failed: ' + response);
    if (this._isMounted) {
      this.setState({
        isLoggedIn: false,
        isLoggingIn: false,
        error: response
      });
    }
  }

}

module.exports = Login;
