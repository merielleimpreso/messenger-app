import React, {Component} from 'react';

import {
  AppRegistry,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {secondsToTime} from './Helpers';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
const GLOBAL = require('./../modules/config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');
let timeInterval;

class AudioRecording extends Component {
  constructor(props) {
    super(props);
    this.state = {
      audioPath: null,
      currentTime: 0.0,
      recording: false,
      stoppedRecording: false,
      stoppedPlaying: false,
      playing: false,
      finished: false
    };
    this.record = this.record.bind(this);
    this.stop = this.stop.bind(this);
  }

  prepareRecordingPath(){
    let audioPath = `${AudioUtils.DocumentDirectoryPath}/${new Date().getTime()}_${Math.random().toString(10).substr(2, 100)}.aac`;
    if (this._isMounted) {
      this.setState({audioPath: audioPath});
    }
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });
  }

  componentDidMount() {
    this._isMounted = true;
    this.prepareRecordingPath();
    // NOT WORKING ON ANDROID
    // AudioRecorder.onProgress = (data) => {
    //   this.setState({currentTime: Math.floor(data.currentTime)});
    // };
    // AudioRecorder.onFinished = (data) => {
    //   this.setState({finished: data.finished});
    //   console.log(`Finished recording: ${data.finished}`);
    // };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  stop() {
    if (this.state.currentTime > 1) {
      clearInterval(timeInterval);
      AudioRecorder.stopRecording();
      if (this._isMounted) {
        this.setState({
          stoppedRecording: true,
          recording: false,
          currentTime: 0.0
        });
      }
      this.sendAudioClip();
    }
  }

  record() {
    if(this.state.stoppedRecording){
      this.prepareRecordingPath();
    }
    AudioRecorder.startRecording();
    if (this._isMounted) {
      this.setState({
        startTime: new Date(),
        currentTime: 1,
        recording: true,
        playing: false
      });
      timeInterval = setInterval(() => this.setCurrentTime(), 1000);
    }
  }

  setCurrentTime() {
    if (this._isMounted) {
      this.setState({currentTime: this.state.currentTime + 1})
    }
  }

  sendAudioClip() {
    let now = new Date();
    let duration = (now - this.state.startTime) / 1000;
    let uri = 'file://' + this.state.audioPath;
    let filename = uri.replace(/^.*[\\\/]/, '');

    this.props.sendAudio(uri, duration);
  }

  render() {
    let color = this.props.color;
    let recordImage = (!this.state.recording) ? require('./../images/record_idle.png') : require('./../images/recording.gif');

    return (
      <View style={[{height: this.props.keyboardSpace, left: 0, right: 0, bottom: 0, backgroundColor: color.CHAT_BG}]} >
        <View style={styles.recordContainer} >
          <Image style={{height: this.props.keyboardSpace / 1.5, width: windowSize.width / 1.5, justifyContent: 'center'}}
            source={recordImage}
            resizeMode='contain'>
            <TouchableOpacity
              onLongPress={() => this.record()}
              onPressOut={() => this.stop()}>
              <KlikFonts name='mic' color={color.TEXT_INPUT} size={this.props.keyboardSpace / 1.5} style={{alignSelf:'center', backgroundColor: 'transparent'}}/>
            </TouchableOpacity>
          </Image>
          <Text style={{alignSelf:'center', color: color.TEXT_INPUT, fontSize: 20}}>{secondsToTime(this.state.currentTime)}</Text>
        </View>
      </View>
    );
  }
}

export default AudioRecording;

// /*

// Author: Ida Jimenez
// Date Created: 2016-06-09
// Date Updated: NA
// Description: Module to display recording. Implementation based on AudioRecording.js

// update: 2016-06-10 (Ida)
// - Import AudioRecorder
// - Added function to record
// - Added function to send via xhr
// update: 2016-07-01 (ida)
// - Added sending of audio in group chat
// update: 2016-08-03 (ida)
// - Added save to cache
// */

// 'use strict';

// import React, {
//   Component,
// } from 'react';

// import {
//   ActivityIndicator,
//   Dimensions,
//   Image,
//   StyleSheet,
//   Text,
//   TouchableHighlight,
//   TouchableOpacity,
//   Vibration,
//   View
// } from 'react-native';

// import {consoleLog, secondsToTime, getEncyptedFile} from './Helpers';
// import FileReader from 'react-native-fs';
// import KlikFonts from 'react-native-vector-icons/KlikFonts';
// import moment from 'moment';
// let AudioRecorder = require('react-native-audio-android');
// let audioRecorder = new AudioRecorder();

// const GLOBAL = require('./../modules/config/Globals.js');
// const styles = StyleSheet.create(require('./../styles/styles.main'));
// const windowSize = Dimensions.get('window');
// let timeInderval;
// let currentTime;

// class AudioRecording extends Component{

//   // Get initial state
//   constructor(props) {
//     super(props);
//     this.state = {
//       buttonText: 'Record',
//       buttonTextPlay: 'Play',
//       currentTime: 0,
//       startTime: new Date(),
//       loading: true,
//       isLoaded: false
//     }
//     this.setCurrentTime = this.setCurrentTime.bind(this);
//   }

//   //Put a flag when component is mounted.
//   componentDidMount() {
//     this._isMounted = true;
//   }

//   // Put a flag when component will unmount.
//   componentWillUnmount() {
//     this._isMounted = false;
//   }

//   sendAudioClip(audioPath, duration) {
//     var uri = 'file://' + audioPath;
//     var filename = uri.replace(/^.*[\\\/]/, '');

//     this.props.sendAudio(uri, duration);
//   }

//   render() {
//     let color = this.props.color;
//     let recordImage = (this.state.buttonText == 'Record') ? require('./../images/record_idle.png') : require('./../images/recording.gif');

//     return (
//       <View style={[{height: this.props.keyboardSpace, left: 0, right: 0, bottom: 0, backgroundColor: color.CHAT_BG}]} >
//         <View style={styles.recordContainer} >
//           <Image style={{height: this.props.keyboardSpace / 1.5, width: windowSize.width / 1.5, justifyContent: 'center'}}
//            source={recordImage}
//            resizeMode='contain'>
//             <TouchableOpacity
//               onLongPress={() => this.startRecording()}
//               onPressOut={() => this.stopRecording()}>
//               <KlikFonts name='mic' color={color.TEXT_INPUT} size={this.props.keyboardSpace / 1.5} style={{alignSelf:'center'}}/>
//             </TouchableOpacity>
//           </Image>
//           <Text style={{alignSelf:'center', color: color.TEXT_INPUT, fontSize: 20}}>{secondsToTime(this.state.currentTime)}</Text>
//         </View>
//       </View>
//     );
//   }

//   setCurrentTime() {
//     if (this._isMounted) {
//       this.setState({currentTime: this.state.currentTime + 1})
//     }
//   }

//   startRecording() {
//     // TO DO: record function
//    console.log('starting record');

//     if (this._isMounted) {
//       this.setState({
//         buttonText: 'Stop',
//         startTime: new Date(),
//         currentTime: 1
//         //isRecording: true
//       });
//       timeInderval = setInterval(() => this.setCurrentTime(), 1000);

//       audioRecorder.startAudioRecording((success) => {
//         console.log(success);
//         var now =  new Date();
//         var duration = (now - this.state.startTime) / 1000;
//         //this.setState({currentTime: duration});
//         this.sendAudioClip(success, duration);
//       }, (error) => {
//         console.log(error);
//       });
//     }
//   }

//   stopRecording() {
//     console.log('stop record');
//     if (this._isMounted) {
//       clearInterval(timeInderval);
//       this.setState({
//         buttonText: 'Record',
//         currentTime: 0
//         //hasStoppedRecording: true,
//         //isRecording: false
//       });
//       audioRecorder.stopAudioRecording((result) => {
//         console.log(result)
//       });
//     }
//   }

//   play() {
//     // TO DO: record function
//     if (this.state.buttonTextPlay == 'Play') {
//       console.log('starting play');
//       AudioRecorder.playRecording();
//       if (this._isMounted) {
//         this.setState({
//           buttonTextPlay: 'Stop'
//         });
//       }
//     } else {
//       console.log('stop play');
//       AudioRecorder.stopPlaying();
//       if (this._isMounted) {
//         this.setState({
//           buttonTextPlay: 'Play'
//         });
//       }
//     }
//   }

// };

// // Export module
// module.exports = AudioRecording;
