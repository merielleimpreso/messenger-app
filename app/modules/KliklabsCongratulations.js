'use strict';

import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  BackAndroid,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Alert
} from 'react-native';

import Button from './common/Button';
import Download from './config/db/Download';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import RenderModal from './common/RenderModal';
import SystemAccessor from './SystemAccessor';

const GLOBAL = require('./../modules/config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main.js'));
const windowSize = Dimensions.get('window');

var retryCount = 0;

export default class KliklabsCongratulations extends Component {

  // Set initial state
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLoaded: false,
      isLoading: false,
      isShowCheckConnection: false,
      isLoggedIn: false,
      loggedInUser: this.props.loggedInUser()
    }
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.onPressStart = this.onPressStart.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
  }

  // Check is logged in
  componentDidMount() {
    this._isMounted = true;
    this.props.setSignUpCallback(this.onPressStart);

  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.loggedInUser !== this.props.loggedInUser()) {
      this.goToTabs();
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // Render display
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let photo = (this.props.image == null) ? require('./../images/tempprofpic.jpg') : {uri: this.props.image},
        photoWidth = 150,
        borderRadius = photoWidth * 0.5;
    return (
      <View style={[styles.container, {backgroundColor: GLOBAL.COLOR_CONSTANT.CHAT_BG}]}>
        {this.props.renderConnectionStatus()}
        <Image style={{height: null, width: null, flex: 1, marginTop: 50}} resizeMode={'cover'} source={require('./../images/bg_signup.png')}>
          <View style={{width: windowSize.width, alignItems: 'center', justifyContent: 'center', marginTop: 90}}>
            <Image style={{width:photoWidth, height:photoWidth, borderRadius:borderRadius}} source={photo} />
          </View>
        </Image>
        <View style={{alignItems: 'center', flex: 1, marginTop: -80}}>
          <Text style={{color: GLOBAL.COLOR_CONSTANT.TEXT_LIGHT, fontSize: 16, marginTop: 50}}>Congratulations</Text>
          <Text style={{color: GLOBAL.COLOR_CONSTANT.HIGHLIGHT, textAlign: 'center', fontSize: 13}}>
            You have completed your registration.
          </Text>
          <Button text="Start Now"
           color={GLOBAL.COLOR_CONSTANT.THEME}
           underlayColor={GLOBAL.COLOR_CONSTANT.HIGHLIGHT}
           onPress={() => this.onPressStart()}
           style={{marginTop:30}}
           isDefaultWidth={true}/>
        </View>
        {this.renderModal()}
      </View>
    );
  }

  onPressStart() {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsLoading(true);
      let { username, password } = this.props;

      let toEncrypt = { username:username, password:password};

      SystemAccessor.encrypt(JSON.stringify(toEncrypt), GLOBAL.SECRET_KEY, (err, apiLoginOptions) => {
        // call login api
        if (apiLoginOptions) {
          ddp.call('apiLogin', [Platform.OS, apiLoginOptions]).then((response) => {
            // match returned username against entered username
            SystemAccessor.decrypt(response, GLOBAL.SECRET_KEY, (err, response) => {
              if (response) {
                response = JSON.parse(response);

                if(GLOBAL.TEST_MODE) {
                  // response.sukses = true;
                  console.log('--- TEST MODE KliklabsCongratulations - apiLogin response=', response);
                }

                if (response.access_token) {
                  AsyncStorage.setItem('accessToken', response.access_token);
                }
                if (response.username == username) {
                  // get mobile number and use it to log on to klikchat
                  ddp.call('getMobileByUsername', [this.props.username]).then((response) => {
                    let kcMobile = response.mobile;
                    let username = response.username;
                    if (this.props.apiMobile && (this.props.apiMobile != kcMobile)) {
                      ddp.call('updateMobileOnKlikchat', [username, this.props.apiMobile]).then(response => {
                        console.log('updateMobileOnKlikchat', response);
                        this.loginUser(username, password);
                      });
                    } else {
                      this.loginUser(username, password);
                    }
                  });
                }
              } else {
                this.setIsLoading(false);
                Alert.alert("Something went wrong. Please try again.");
              }
            });
          });
        } else {
          this.setIsLoading(false);
          Alert.alert("Something went wrong. Please try again.");
        }
      });
    }
  }

  loginUser(username, password) {
    ddp.loginWithUsername(username, password, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        this.props.subscribeToLoggedInUser(true, () => {
          this.setState({
            isLoggedIn: true
          });
        });
        // Download.createDirectoryForKlikchat();
        this.setIsLoading(false);

      } else {
        console.log('Login failed: ' + err.reason);
        if(retryCount < 5) {
          this.loginUser(username, password);//force re-call.
          retryCount++;
        } else {
          Alert.alert("Problem with server response: ",err + "\nScreenshot and contact developer about this.");
        }
      }
    });
  }

  setIsLoading(isLoading) {
    if (this._isMounted) {
      this.setState({isLoading: isLoading});
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  renderModal() {
    if (this.state.isLoading) {
      return (
        <RenderModal
          color={GLOBAL.COLOR_CONSTANT}
          setIsShowModal={() => console.log('loading')}
          modalType={'loading'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  goToTabs() {
    this.props.navigator.immediatelyResetRouteStack([{
      id: 'tabs',
      name: 'Tabs',
      addContactToLoggedInUser: this.props.addContactToLoggedInUser,
      audio: this.props.audio,
      backgroundImageSrc: this.props.backgroundImageSrc,
      changeTheme: this.props.changeTheme,
      debugText: this.props.debugText,
      getMessageWallpaper: this.props.getMessageWallpaper,
      hasInternet: this.props.hasInternet,
      hasNewFriendRequest: this.props.hasNewFriendRequest,
      isDebug: this.props.isDebug,
      isLoggedIn: this.props.isLoggedIn,
      isSafeToBack: this.props.isSafeToBack,
      loggedInUser: this.props.loggedInUser,
      logout: this.props.logout,
      renderConnectionStatus: this.props.renderConnectionStatus,
      setAudio: this.props.setAudio,
      setIsSafeToBack: this.props.setIsSafeToBack,
      setLoggedInUser: this.props.setLoggedInUser,
      setSignUpCallback: this.props.setSignUpCallback,
      shouldReloadData: this.props.shouldReloadData,
      subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
      theme: this.props.theme,
      timeFormat: this.props.timeFormat,
    }]);
  }

  isLoggedIn() {
    return this.state.isLoggedIn;
  }


}


// module.exports = KliklabsCongratulations;
