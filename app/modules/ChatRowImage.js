/*

Author: Merielle N. Impreso
Date Created: 2016-07-01
Date Updated: NA
Description: Display image depending on its actual size.
Used In: ChatRow.js

update: 2016-07-21 by Merielle I.
  - change code implementation from ES5 to ES6
update: 2016-08-16 (ida)
  - Implement photo viewer with zoom and swipe to pan

*/

import React, {
  Component
} from 'react';
import ReactNative, {
  ActivityIndicator,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Platform
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';

const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const GLOBAL = require('./config/Globals.js');
const IMAGE_WIDTH = Dimensions.get('window').width * 0.55;

class ChatRowImage extends Component {

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      width: 150,
      height: 150,
      image: GLOBAL.DEFAULT_IMAGE,
      error: false,
      isLoading: false,
      progress: 0,
      loading:true
    }
    this.getImageSize = this.getImageSize.bind(this);
  }

  //Put a flag to know if component is mounted.
  componentWillMount() {
    this._isMounted = true;
    this.getImage();
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  getImage() {
    var messagePhoto = this.props.messagePhoto[0].medium;
    var image = (messagePhoto) ? messagePhoto : GLOBAL.DEFAULT_IMAGE;
    if (this._isMounted) {
      this.setState({
        image: image
      });
    }
    this.getImageSize();
  }

  // Compute the image size to be displayed.
  getImageSize() {
    var messagePhoto = this.props.messagePhoto[0].medium;
    var image = (messagePhoto) ? messagePhoto : GLOBAL.DEFAULT_IMAGE;
    Image.getSize(image,
      (actualWidth, actualHeight) => {
        var height = IMAGE_WIDTH * (actualHeight / actualWidth);
        if (this._isMounted) {
          this.setState({
            height: height,
          });
        }
      },
      (error) => {
        console.log(error);
        if (this._isMounted) {
          this.setState({
            image: GLOBAL.DEFAULT_IMAGE,
            width: 150,
            height: 150,
          });
        }
      }
    );
  }

  // Display image.
  render() {
    var color = this.props.color;
    var image = this.state.image;

    var loader = this.state.loading
      ? <View style={[styles.chatImage, {width:IMAGE_WIDTH, height: this.state.height, backgroundColor:color.HIGHLIGHT}]}>
          <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
          <Text style={{color:color.BUTTON, fontSize:10}}>Loading image...</Text>
          <Text style={{color:color.BUTTON}}>{this.state.progress}%</Text>
        </View>
      : null;

    var render = null;
    if (this.state.error) {
      render = (
        <View style={[styles.chatImageContainer, {backgroundColor:color.HIGHLIGHT}]}>
          <View style={[styles.chatImage, {width:IMAGE_WIDTH, height: this.state.height}]}>
            <Text style={{color:color.BUTTON, alignSelf:'center', textAlign:'center', fontSize:12}}>Ooopss!</Text>
            <Text style={{color:color.BUTTON, alignSelf:'center', textAlign:'center', fontSize:10}}>{this.state.error}</Text>
          </View>
        </View>
      );
    } else {
      var viewPhoto = (this.props.chat.isTemporary) ? this.props.messagePhoto[0].medium : this.props.messagePhoto[0].large;
      // console.log('viewPhoto',viewPhoto);

      render =
        (
          <TouchableHighlight onPress={() => this.props.viewImage(viewPhoto, this.props.chat, this.props.imageGallery)}
          style={[styles.chatImageContainer, {backgroundColor:color.HIGHLIGHT}]}
          underlayColor={color.HIGHLIGHT}>
            <Image source={{uri:image}}
            key={image}
            borderRadius={10}
            style={[styles.chatImage, {width:IMAGE_WIDTH, height: this.state.height}]}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false})}>
                {(() => {
                  if (this.props.chat.isTemporary) {
                  if (this.props.chat.isFailed) {
                    return (
                      <View style={[styles.chatImage, {width:IMAGE_WIDTH, height: this.state.height, backgroundColor:'#000000AA'}]}>
                        <Text style={{color:color.BUTTON, fontSize:10, textAlign: 'center'}}>Failed uploading.{'\n'}Please try again later.</Text>
                      </View>
                    );
                  } else {
                    return (
                      <View style={[styles.chatImage, {width:IMAGE_WIDTH, height: this.state.height, backgroundColor:'#000000AA'}]}>
                        <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} />
                        <Text style={{color:color.BUTTON, fontSize:10}}>Uploading...</Text>
                        <Text style={{color:color.BUTTON}}>{Math.round(100 * this.props.chat.progress)}%</Text>
                      </View>
                    );
                  }
                } else if (this.state.loading) {
                  {loader}
                }
                })()}
            </Image>
          </TouchableHighlight>
        );
    }
    return render;


    /*
    <TouchableHighlight onPress={() => this.props.viewImage(viewPhoto)}
     style={[styles.chatImageContainer, {backgroundColor:color.HIGHLIGHT}]}
     underlayColor={color.HIGHLIGHT}>

    var image = this.state.image;
    var viewPhoto = this.props.messagePhoto[0].large;
    var width = this.state.width;
    var height = this.state.height;
    var color = this.props.color;

    var loader = this.state.loading ?
      <View style={[styles.chatImage, {width:this.state.width, height:this.state.height}]}>
        <Text>{this.state.progress}%</Text>
        <ActivityIndicator style={{marginLeft:5}} />
      </View> : null;

    return (
      <TouchableHighlight onPress={() => this.props.viewImage(viewPhoto)}
       style={styles.chatImageContainer}
       underlayColor={color.HIGHLIGHT}>

        <Image source={{uri: image}}
          style={[styles.chatImage, {width:200, height: this.state.height}]}
          onLoadStart={(e) => this.setState({isLoading: true})}
          onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
          onError={(e) => console.log(image + ' :' + e.nativeEvent.error)}
          onLoad={() => this.setState({isLoading: false, isError: false})}>
          {(() => {
            if (this.props.chat.isTemporary) {
              return <Progress.Circle progress={this.props.chat.progress} showsText={true} size={45} color='#eee' />
            }
           })()}
           {loader}
        </Image>
      </TouchableHighlight>
    )*/
  }
}

// Export module
module.exports = ChatRowImage;
