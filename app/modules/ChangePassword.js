/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-06
 * Date Updated: N/A
 * Description: Component used for Changing password
 */
'use strict';

import React, { Component } from 'react';
import {
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  ToolbarAndroid,
  View,
} from 'react-native';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';
import Toolbar from './common/Toolbar';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main.js'));
const MINIMUM_PASSWORD = 6;

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      passWordState: '',
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isLoading: false,
      accessToken: ""
    }
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
  }

  componentDidMount() {
    AsyncStorage.getItem('accessToken').then((res) => {
        console.log("This.props.loggedinUser: ", res );
        this.setState({accessToken: res});
    });

    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderTopBar()}
        {this.props.renderConnectionStatus()}
        <View style={{alignItems: 'center', paddingTop: 20}}>
          <View style={[styles.inputLoginContainer, {borderBottomColor: color.INACTIVE_BUTTON}]}>
            <TextInput ref='oldPassword'
              onChangeText={(oldPassword) => this.setState({oldPassword})}
              placeholder='Old Password'
              placeholderTextColor={color.TEXT_LIGHT}
              style={[styles.input, {backgroundColor: 'transparent', color: color.TEXT_DARK}]}
              underlineColorAndroid={color.THEME}
              onSubmitEditing={(event) => this.refs.newPassword.focus()}
              secureTextEntry
            />
          </View>
          <View style={[styles.inputLoginContainer, {borderBottomColor: color.INACTIVE_BUTTON}]}>
            <TextInput ref='newPassword'
              onChangeText={(newPassword) => this.setState({newPassword})}
              placeholder='New Password'
              placeholderTextColor={color.TEXT_LIGHT}
              style={[styles.input, {backgroundColor: 'transparent', color: color.TEXT_DARK}]}
              underlineColorAndroid={color.THEME}
              onSubmitEditing={(event) => this.refs.confirmPassword.focus()}
              secureTextEntry
            />
          </View>
          <View style={[styles.inputLoginContainer, {borderBottomColor: color.INACTIVE_BUTTON}]}>
            <TextInput ref='confirmPassword'
              onChangeText={(confirmPassword) => this.setState({confirmPassword})}
              placeholder='Confirm Password'
              placeholderTextColor={color.TEXT_LIGHT}
              style={[styles.input, {backgroundColor: 'transparent', color: color.TEXT_DARK}]}
              underlineColorAndroid={color.THEME}
              onSubmitEditing={(event) => this.checkPasswords()}
              secureTextEntry
            />
          </View>
          <Text style={{alignSelf: 'center', color: color.ERROR, fontSize: 12, height: 20}}>
            {this.state.passWordState}
          </Text>
          <Button text="Change"
            color={color.THEME}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.checkPasswords()}
            style={{marginTop:15}}
            isDefaultWidth={true}/>
        </View>
        {this.renderModal()}
      </View>
    );
  }

  renderTopBar() {
    return (
      <Toolbar
        title={'Change Password'}
        withBackButton={true}
        {...this.props}
      />
    )
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  // checkPasswords() {
  //   this.refs.oldPassword.blur();
  //   this.refs.newPassword.blur();
  //   this.refs.confirmPassword.blur();

  //   if ( (this.state.newPassword === this.state.confirmPassword) &&
  //        (this.state.newPassword != "" && this.state.newPassword != "" ) ) {
  //     if (this._isMounted) {
  //       this.setState({isLoading: true});
  //     }
  //     ddp.call('changePassword', [this.state.oldPassword, this.state.newPassword])
  //     .then((value) => {
  //       this.refs.oldPassword.clear();
  //       this.refs.newPassword.clear();
  //       this.refs.confirmPassword.clear();
  //       this.setIsShowMessageModal('Successfully changed password');
  //       if (this._isMounted) {
  //         this.setState({
  //           oldPassword: '',
  //           newPassword: '',
  //           confirmPassword: '',
  //           passWordState: '',
  //           isLoading: false
  //         });
  //       }
  //     }).catch(function(e) {
  //       this.setState({passWordState: e.reason})
  //     }.bind(this));
  //   } else if (this.state.newPassword==="" || this.state.newPassword==="") {
  //     this.setState({passWordState: 'Password Empty'});
  //   } else {
  //     this.setState({passWordState: 'Password Mismatch'});
  //   }
  // }

  checkPasswords() {
    if(!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.refs.oldPassword.blur();
      this.refs.newPassword.blur();
      this.refs.confirmPassword.blur();

      let oldPassword = this.state.oldPassword.trim().replace('/\s+/g',' ');
      let newPassword = this.state.newPassword.trim().replace('/\s+/g',' ');
      let confirmPassword = this.state.confirmPassword.trim().replace('/\s+/g',' ');

      if (!oldPassword || !newPassword || !confirmPassword) {
        this.setState({passWordState: 'Password Empty.'});
        return;
      } else if (newPassword.length < MINIMUM_PASSWORD) {
        this.setState({passWordState: 'Password should be at least 6 characters.'});
        return;
      } else if( newPassword !== confirmPassword ){
        this.setState({passWordState: 'Password Mismatch'});
        return;
      }

      // good to go
      console.log('good to go... x')
      if (this._isMounted) {
        this.setState({isLoading: true});
      }

      ddp.call('changePasswordOfUser', [oldPassword, newPassword, this.state.accessToken])
      .then((response) => {
        console.log("Entered then");
        console.log("change password Response: ", response);
        this.refs.oldPassword.clear();
        this.refs.newPassword.clear();
        this.refs.confirmPassword.clear();
        this.setIsShowMessageModal(response);
        if (this._isMounted) {
          this.setState({
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
            passWordState: '',
            isLoading: false
          });
        }
      })
      .catch( e => {
        this.setState({
          passWordState: e.reason,
          oldPassword: '',
          isLoading: false
        })
        this.setIsShowMessageModal(response);
        this.refs.oldPassword.clear();
      });
    }
  }

  renderModal() {
    if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.state.isLoading) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => console.log('loading')}
         modalType={'loading'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }
};

module.exports = ChangePassword;
