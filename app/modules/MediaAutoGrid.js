'use strict';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

import { getMediaURL } from './Helpers';
import _ from 'underscore';
import ImageWithLoader from './common/ImageWithLoader';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Navigation from './actions/Navigation';

const GLOBAL = require('./config/Globals.js');
const windowSize = Dimensions.get('window');

class MediaAudioGrid extends Component{
  constructor(props) {
    super(props);
    this.viewVideo = this.viewVideo.bind(this);
    this.state = {
      images: [],
      loading: true,
      isLoaded: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillMount() {
    if (this.props.images) {
      if (this.props.images.length) {
        var urls = _.map(this.props.images, url => {
          var imageUrl = getMediaURL(url);
          var isVideo = false;
          if (imageUrl.split('.').pop() === '3gp' ||
              imageUrl.split('.').pop() === 'mp4') {
              let urlParts = imageUrl.split('/');
              imageUrl = 'http://' + GLOBAL.SERVER_HOST + ':' + GLOBAL.SERVER_PORT + '/vt/' + urlParts[3] + '/' + urlParts[4];
              isVideo = true;
          }
          Image.getSize(imageUrl,
            (actualWidth, actualHeight) => {
              this.setState({
                images: [ ...this.state.images, {ratio: (actualHeight / actualWidth), src: imageUrl, videoUrl: isVideo ? url : null} ]
              });
            }, (error)=>{
              //console.log(error);
          });
        });
      }
    }
  }

  render() {
    let color = this.props.color;
    return (
      <View>
        {(() => {
          var i = 0;
          var images = this.state.images.map(image => {
              i++;
              if(image.videoUrl){
                return (
                  <TouchableHighlight key={i}
                   onPress={() => Navigation.onPress(() => this.viewVideo(image.videoUrl))}>
                    <Image source={{uri: image.src}} style={{flex: 1, height: (this.props.width * image.ratio),  width:this.props.width, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginBottom: 10}}
                     onLoadStart={(e) => this.setState({loading:true})}
                     onError={(e) => this.setState({error:e.nativeEvent.error, loading: false})}
                     onProgress={(e) => this.setState({progress:Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                     onLoad={() => this.setState({loading:false, error:false, isLoaded:true})} >
                     {(this.state.loading && !this.state.isLoaded)
                       ? <View style={{flex:1, justifyContent:'center', alignItems:'center',backgroundColor:color.CHAT_BG, width:this.props.width, height: (this.props.width * image.ratio)}}>
                           <ActivityIndicator size="small" color={color.BUTTON} />
                         </View>
                       : <View style={{flexDirection:'row', alignItems:'center', backgroundColor:'transparent'}}>
                          <Ionicons name="ios-play" color={'#fff'} size={40} />
                          <Text style={{fontSize:20, fontWeight:'600', color:'#fff', marginLeft:10}}> PLAY </Text>
                         </View>
                      }
                    </Image>
                  </TouchableHighlight>);
              }
              else
                return <ImageWithLoader key={i} style={{height:(this.props.width*image.ratio), width:this.props.width}} image={image.src} color={color} />;
          })
          return images;
        })()}
      </View>
    )
  }

  // Play video clip.
  viewVideo(video) {
    if (Platform.OS === 'android') {
      this.props.navigator.push({
        name: 'VideoPlayer',
        data: getMediaURL(video),
        media: 'Video',
        color: this.props.color
      });
    }
    else
    this.props.navigator.push({
      id:'videoplayer',
      title: 'Messenger',
      navigationBarHidden:false,
      data: getMediaURL(video),
      color: this.props.color
    });
  }
}

module.exports = MediaAudioGrid;

/** ================ OLD METHODS ====================**/
// render() {
//   return (
//     <View>
//       {(() => {
//         // var length = this.props.images.length;
//         // var url = this.props.images[0];
//         // var ratio = this.state.aspectRatio;
//         // if (!this.state.aspectRatio && url) {
//         //   let urlParts = url.split('/');
//         //   url = 'http://' + GLOBAL.SERVER_HOST + ':' + GLOBAL.SERVER_PORT + '/vt/' + urlParts[3] + '/' + urlParts[4];
//         //   ratio = 1;
//         //   console.log(url);
//         // }
//         // if (length === 1) {
//         //   var content = <Image
//         //     resizeMode = "cover"
//         //     style = {{ flex: 1, width:this.props.width, height: (this.props.width * ratio) }}
//         //     source = {{ uri: url}} />;
//         // }
//         // else if(length === 2){
//         //   var content = <Image
//         //     resizeMode = "cover"
//         //     style = {{ flex: 1, width:this.props.width, height: (this.props.width * ratio) }}
//         //     source = {{ uri: url}} />;
//         // }
//         // else if(length === 3){
//         //
//         // }
//         // else if(length === 4){
//         //
//         // }
//         // else{
//         //
//         // }
//         // console.log('----------------------- props -----------------------');
//         // console.log(this.props.images);
//         // console.log('----------------------- state -----------------------');
//         // console.log(this.state.images);
//         var i = 0;
//         var images = this.state.images.map(function(image) {
//             i++;
//             if(image.videoUrl){
//               return (
//                 <TouchableHighlight key={i} onPress={() => this.viewVideo(image.videoUrl)}>
//                   <Image
//                     style={{height: (this.props.width * image.ratio), flex: 1, width:this.props.width, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginBottom: 10}}
//                      source={{uri: image.src}}>
//                     <Ionicons name="ios-play" color={'#fff'} size={40} />
//                     <Text style={{fontSize:20, fontWeight:'600', color:'#fff', marginLeft:10}}> PLAY </Text>
//                   </Image>
//               </TouchableHighlight>);
//             }
//             else
//               return <Image key={i} style = {{ flex: 1, width:this.props.width, height: (this.props.width * image.ratio), marginBottom: 10 }} source={{uri: image.src}} />;
//         }.bind(this))
//         return images;
//       })()}
//     </View>
//   );
// }
