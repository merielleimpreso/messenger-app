import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';

export default class Send extends React.Component {
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props.text.trim().length === 0 && nextProps.text.trim().length > 0 || this.props.text.trim().length > 0 && nextProps.text.trim().length === 0) {
  //     return true;
  //   }
  //   return false;
  // }
  render() {
    let color = this.props.color;
    if (this.props.text.trim().length > 0) {
      return (
        <TouchableWithoutFeedback
         onPressIn={() => Navigation.onPress(() => this.props.onSend({text: this.props.text.trim()}, true))}
         onLongPress={() => {}}>
          <View style={[styles.container, this.props.containerStyle, {backgroundColor: color.INACTIVE_BUTTON}]}>
            <KlikFonts name={'send'} color={color.BUTTON_TEXT} size={35} style={styles.text}/>
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      return (
        <TouchableWithoutFeedback
          onPressIn={() => Navigation.onPress(() => {console.log(this.props.setIsRecordPress);this.props.setIsRecordPress()})}
        >
          <View style={[styles.container, this.props.containerStyle, {backgroundColor: color.INACTIVE_BUTTON}]}>
            <KlikFonts name={'mic'} color={color.BUTTON_TEXT} size={35} style={styles.text}/>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: 40,
    borderRadius: 50,
    marginRight: 2,
  },
  text: {
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
});

Send.defaultProps = {
  text: '',
  onSend: () => {},
  label: 'Send',
  containerStyle: {},
  textStyle: {},
  setIsRecordPress: () => {}
};

Send.propTypes = {
  text: React.PropTypes.string,
  onSend: React.PropTypes.func,
  label: React.PropTypes.string,
  containerStyle: View.propTypes.style,
  textStyle: Text.propTypes.style,
  setIsRecordPress: React.PropTypes.func
};
