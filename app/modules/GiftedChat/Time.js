import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import _ from 'underscore';
import { dateToTime } from './../Helpers';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import moment from 'moment/min/moment-with-locales.min';

export default class Time extends React.Component {
  render() {
    let createdAt = dateToTime(this.props.currentMessage.createdAt, this.props.timeFormat());//moment(this.props.currentMessage.createdAt).locale(this.context.getLocale()).format('LT');
    let indicator = '';
    let isSending = this.props.currentMessage.seen == undefined; // _.contains(['Sending', 'Failed', undefined], this.props.currentMessage.status);
    if (isSending) {
      indicator = null;
    } else {
      let seen = this.props.currentMessage.seen;
      if (seen) {
        if (seen.length > 0 && this.props.position == 'right') {
          if (this.props.currentMessage.isGroup) {
            indicator = 'Read by ' + seen.length;
          } else {
            indicator = 'Read';
          }
        }
      }
    }

    return (
      <View style={{justifyContent: 'flex-end'}}>
        <Text style={[styles[this.props.position].text, this.props.textStyle[this.props.position]]}>
          {indicator}
        </Text>
        <Text style={[styles[this.props.position].text, this.props.textStyle[this.props.position]]}>
          {(isSending) ? null : createdAt}
        </Text>
      </View>
    );
  }
}

const containerStyle = {
  marginLeft: 10,
  marginRight: 10,
  marginBottom: 5,
};

const textStyle = {
  fontSize: 10,
  backgroundColor: 'transparent',
  textAlign: 'right',
};

const styles = {
  left: StyleSheet.create({
    container: {
      ...containerStyle,
    },
    text: {
      color: '#aaa',
      ...textStyle,
    },
  }),
  right: StyleSheet.create({
    container: {
      ...containerStyle,
    },
    text: {
      color: '#fff',
      ...textStyle,
    },
  }),
};

Time.contextTypes = {
  getLocale: React.PropTypes.func,
};

Time.defaultProps = {
  position: 'right',
  currentMessage: {
    createdAt: null,
  },
  containerStyle: {},
  textStyle: {},
};

Time.propTypes = {
  position: React.PropTypes.oneOf(['left', 'right']),
  currentMessage: React.PropTypes.object,
  containerStyle: React.PropTypes.shape({
    left: View.propTypes.style,
    right: View.propTypes.style,
  }),
  textStyle: React.PropTypes.shape({
    left: Text.propTypes.style,
    right: Text.propTypes.style,
  }),
};
