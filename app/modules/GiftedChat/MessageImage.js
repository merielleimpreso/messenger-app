import React from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Download from './../config/db/Download';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Message from './../config/db/Message';
import Send from './../config/db/Send';
const IMAGE_WIDTH = 150;

const GLOBAL = require('./../config/Globals.js');

export default class MessageImage extends React.Component {

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      width: 150,
      height: 150,
      image: GLOBAL.DEFAULT_IMAGE,
      error: false,
      isLoading: false,
      progress: 0,
      loading:true
    }
    this.getImageSize = this.getImageSize.bind(this);
    this.resendMedia = this.resendMedia.bind(this);
  }

  //Put a flag to know if component is mounted.
  componentWillMount() {
    this._isMounted = true;
    this.getImageSize();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getImageSize() {
    // var messagePhoto = GLOBAL.MEDIA_SERVER_HOST + '/' + this.props.currentMessage.image;
    var messagePhoto = this.props.currentMessage.image;
    var image = (messagePhoto) ? messagePhoto : GLOBAL.DEFAULT_IMAGE;

    // console.log('----- MessageImage image', image);

    Image.getSize(image, (actualWidth, actualHeight) => {
      var height = IMAGE_WIDTH * (actualHeight / actualWidth);
      if (this._isMounted) {
        this.setState({
          height: height,
        });
      }
    },(error) => {
      console.log('[PhotoInfo] getImageSize', error);
    });
  }

  render() {
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <Image
          key={this.props.currentMessage._id}
          style={[styles.image, this.props.imageStyle, {width:IMAGE_WIDTH, height:this.state.height}]}
          source={{uri: this.props.currentMessage.image}}
          borderRadius={13}>
          {this.renderOverlay()}
        </Image>
      </View>
    );
  }

  renderOverlay() {
    let isLoggedInUser = this.props.currentMessage.user._id == this.props.loggedInUser()._id;

    if (this.props.currentMessage.seen != undefined) {
      let image = this.props.currentMessage.mediaCache ? this.props.currentMessage.mediaCache : this.props.currentMessage.image;
      return (
        <TouchableOpacity onPress={() => {
          if(!this.props.currentMessage.isSticker) {
            this.props.goToViewImage(this.props.otherUser._id, this.props.currentMessage, this.props.imageGallery);
          }}}>
          <View style={[this.props.imageStyle, {width:IMAGE_WIDTH, height:this.state.height}]} />
        </TouchableOpacity>
      );
    } else if (this.props.currentMessage.status == 'Failed' && isLoggedInUser) {
      if (!this.props.currentMessage.isSticker) {
        return (
          <TouchableOpacity style={[this.props.imageStyle, styles.imageOverlay, {width:IMAGE_WIDTH, height:this.state.height}]}
            onPress={() => this.resendMedia(this.props.currentMessage._id)}>
            <KlikFonts name="refresh" color={'#fff'} size={40} />
          </TouchableOpacity>
        );
      }
    } else if (this.props.currentMessage.status == 'Sending' && isLoggedInUser) {
      if (!this.props.currentMessage.isSticker) {
        return (
          <View style={[this.props.imageStyle, styles.imageOverlay, {width:IMAGE_WIDTH, height:this.state.height}]}>
            <ActivityIndicator styleAttr="Inverse" color={'#FFF'} />
            <Text style={{color:'#FFF'}}>Uploading...</Text>
            {/* <Text style={{color:'#FFF'}}>{Math.round(100 * this.props.currentMessage.progress)}%</Text> */}
          </View>
        );
      }
    }

    return null;
  }

  resendMedia(id) {
    let message = Message.getItemById(id);
    message.isFailedUploading = false;
    Send.resend(message, 'photo', Send.uploadURL.mediaChat, Download.mediaTypePath.imageSent, this.props.isAutoSaveMedia());
    this.props.setReuploadMedia();
  }
}

const styles = StyleSheet.create({
  container: {
  },
  image: {
    borderRadius: 13,
    margin: 3,
    resizeMode: 'cover',
  },
  imageOverlay: {
    borderRadius: 13,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#000000AA',
    flexDirection: 'row'
  }
});

MessageImage.defaultProps = {
  currentMessage: {
    image: null,
  },
  containerStyle: {},
  imageStyle: {},
};

MessageImage.propTypes = {
  currentMessage: React.PropTypes.object,
  containerStyle: View.propTypes.style,
  imageStyle: Image.propTypes.style,
};
