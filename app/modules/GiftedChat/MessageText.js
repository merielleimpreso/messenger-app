import React from 'react';
import {
  Dimensions,
  LayoutAnimation,
  Linking,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import ParsedText from 'react-native-parsed-text';
import Communications from 'react-native-communications';
const { height, width } = Dimensions.get("window");
const realWidth = height > width ? width : height;
const fontSize = 16;
const scaledFontSize = Math.round(fontSize * realWidth / 375);

export default class MessageText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfLines: 80,
      arrayOfText: [],
      currentIndex: 0,
      displayText: '',
    }
    this.onUrlPress = this.onUrlPress.bind(this);
    this.onPhonePress = this.onPhonePress.bind(this);
    this.onEmailPress = this.onEmailPress.bind(this);
    this.onLoadMorePress = this.onLoadMorePress.bind(this);
  }

  componentDidMount() {
    let text = this.props.currentMessage.text;
    let arrayOfText = [];
    let displayText = text;
    if (text.length > 500) {
      for (let count = -1; text.length > count; count+=500) {
        let remaining = (text.length - count)
        let add = (remaining > 500) ? count + 500 : count + remaining;
        
        arrayOfText.push(text.substring(count + 1, add))
      }
      displayText = arrayOfText[0];
    }
    
    this.setState({
      arrayOfText,
      displayText
    })
  }

  onUrlPress(url) {
    if (!/^[a-zA-Z-_]+:/.test(url)) {
      url = 'http://' + url;
    }
    Linking.openURL(url);
  }

  onPhonePress(phone) {
    const options = [
      'Text',
      'Call',
      'Cancel',
    ];
    const cancelButtonIndex = options.length - 1;
    this.context.actionSheet().showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    },
    (buttonIndex) => {
      switch (buttonIndex) {
        case 0:
          Communications.text(phone);
          break;
        case 1:
          Communications.phonecall(phone, true);
          break;
      }
    });
  }

  onEmailPress(email) {
    Communications.email(email, null, null, null, null);
  }

  onLoadMorePress() {
    // if (!this.props.shouldShowViewContainer) {
      this.props.navigator.push({
        name:'TextView',
        theme: this.props.theme,
        linkStyle: this.props.linkStyle,
        currentMessage: this.props.currentMessage,
        textStyle: this.props.textStyle,
        onEmailPress: this.onEmailPress,
        onPhonePress: this.onPhonePress,
        onUrlPress: this.onUrlPress
      })
    // } else {
    //   let text = this.props.currentMessage.text;

    //   if (text.length >= this.state.displayText.length) {
    //     LayoutAnimation.easeInEaseOut();
    //     let index = this.state.currentIndex + 1;
    //     this.setState({
    //       currentIndex: index,
    //       displayText: this.state.displayText + this.state.arrayOfText[index]
    //     })
    //   }
    // }
  }

  render() {
    let urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    let arrayOfTextLength = this.state.arrayOfText.length;
    let isShowMore = (arrayOfTextLength > 0 && (arrayOfTextLength != this.state.currentIndex + 1))
    
    return (
      <View style={[styles[this.props.position].container, this.props.containerStyle[this.props.position]]}>
        <ParsedText
          style={[styles[this.props.position].text, this.props.textStyle[this.props.position]]}
          parse={[
            {type: 'url', style: StyleSheet.flatten([styles[this.props.position].link, this.props.linkStyle[this.props.position]]), onPress: this.onUrlPress},
            {type: 'phone', style: StyleSheet.flatten([styles[this.props.position].link, this.props.linkStyle[this.props.position]]), onPress: this.onPhonePress},
            {type: 'email', style: StyleSheet.flatten([styles[this.props.position].link, this.props.linkStyle[this.props.position]]), onPress: this.onEmailPress},
            {pattern: urlRegex , style: StyleSheet.flatten([styles[this.props.position].link, this.props.linkStyle[this.props.position]]), onPress: this.onUrlPress},
          ]}
        >
          {this.state.displayText}
        </ParsedText>
        {(this.props.currentMessage.text.length > 500) ?
          <ParsedText
            includeFontPadding={false}          
            style={[styles[this.props.position].text, this.props.textStyle[this.props.position]]}
            parse={[
              {pattern: /Read More/, style: StyleSheet.flatten([styles[this.props.position].loadMore, this.props.linkStyle[this.props.position]]), onPress: () => this.onLoadMorePress()}
            ]}
          >
            {'Read More'}
          </ParsedText>
          : null
        }
      </View>
    );
  }
}

const textStyle = {
  fontSize: scaledFontSize,
  lineHeight: 20,
  marginTop: 5,
  marginBottom: 5,
  marginLeft: 10,
  marginRight: 10,
  textAlignVertical: 'center'
};

const styles = {
  left: StyleSheet.create({
    container: {
      maxHeight: height * 1.5      
    },
    text: {
      color: 'black',
      ...textStyle,
    },
    link: {
      color: 'black',
      textDecorationLine: 'underline',
    },
    loadMore: {
      color: '#22995c',
      textDecorationLine: 'underline',
    },
  }),
  right: StyleSheet.create({
    container: {
      maxHeight: height * 1.5
    },
    text: {
      color: 'white',
      ...textStyle,
    },
    link: {
      color: 'black',
      textDecorationLine: 'underline',
    },
    loadMore: {
      color: '#22995c',
      textDecorationLine: 'underline',
    },
  }),
};

MessageText.contextTypes = {
  actionSheet: React.PropTypes.func,
};

MessageText.defaultProps = {
  position: 'left',
  currentMessage: {
    text: '',
  },
  containerStyle: {},
  textStyle: {},
  linkStyle: {},
};

MessageText.propTypes = {
  position: React.PropTypes.oneOf(['left', 'right']),
  currentMessage: React.PropTypes.object,
  containerStyle: React.PropTypes.shape({
    left: View.propTypes.style,
    right: View.propTypes.style,
  }),
  textStyle: React.PropTypes.shape({
    left: Text.propTypes.style,
    right: Text.propTypes.style,
  }),
  linkStyle: React.PropTypes.shape({
    left: Text.propTypes.style,
    right: Text.propTypes.style,
  }),
};
