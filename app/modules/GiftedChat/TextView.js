import React, { Component } from 'react';
import {
  Clipboard,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native';
import ActionSheet from '@exponent/react-native-action-sheet';
import Communications from 'react-native-communications';
import GLOBAL from '../config/Globals.js';
import MessageText from './MessageText';
import ParsedText from 'react-native-parsed-text';
import Toolbar from '../common/Toolbar';
const { height } = Dimensions.get('window');

export default class TextView extends Component {
  constructor(props) {
    super(props);
    this.onUrlPress = this.onUrlPress.bind(this);
    this.onPhonePress = this.onPhonePress.bind(this);
    this.onEmailPress = this.onEmailPress.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

    return (
      <ActionSheet ref={component => this._actionSheetRef = component}>
        <View style={styles.container}>
          <Toolbar
            title={''}
            withBackButton={true}
            {...this.props}
          />
          <ScrollView style={styles.scroll}>
            <ParsedText
              parse={[
                {type: 'url', style: StyleSheet.flatten([styles.link, this.props.linkStyle, {color: color.TEXT_DARK}]), onPress: this.onUrlPress},
                {type: 'phone', style: StyleSheet.flatten([styles.link, this.props.linkStyle, {color: color.TEXT_DARK}]), onPress: this.onPhonePress},
                {type: 'email', style: StyleSheet.flatten([styles.link, this.props.linkStyle, {color: color.TEXT_DARK}]), onPress: this.onEmailPress},
                {pattern: urlRegex , style: StyleSheet.flatten([styles.link, this.props.linkStyle, {color: color.TEXT_DARK}]), onPress: this.onUrlPress},
              ]}
              selectable={true}
              onLongPress={() => Clipboard.setString(this.props.currentMessage.text)}
              style={[styles.text, {color: color.TEXT_DARK}]}>
                {this.props.currentMessage.text}
            </ParsedText>
          </ScrollView>
        </View>
      </ActionSheet>
    );
  }

  onUrlPress(url) {
    if (!/^[a-zA-Z-_]+:/.test(url)) {
      url = 'http://' + url;
    }
    Linking.openURL(url);
  }

  onPhonePress(phone) {
    const options = [
      'Text',
      'Call',
      'Cancel',
    ];
    const cancelButtonIndex = options.length - 1;
    this._actionSheetRef.showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    },
    (buttonIndex) => {
      switch (buttonIndex) {
        case 0:
          Communications.text(phone);
          break;
        case 1:
          Communications.phonecall(phone, true);
          break;
      }
    });
  }

  onEmailPress(email) {
    Communications.email(email, null, null, null, null);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }, 
  scroll: {
    flex: 1, 
  },
  link: {
    textDecorationLine: 'underline',
  },
  text: {
    fontSize: 16,
    marginBottom: 15, 
    marginLeft: 15, 
    marginRight: 15,
  }
})