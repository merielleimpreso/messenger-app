import React from 'react';
import {
  Platform,
  StyleSheet,
  TextInput,
} from 'react-native';
import _ from 'underscore';

export default class Composer extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     active: props.active(),
  //     isFocused: false
  //   }
  // }

  // componentDidMount() {
  //   this._isMounted = true;
  // }

  // componentDidUpdate(prevProps, prevState) {
  //   if (this.state.active != this.props.active()) {
  //     if (this._isMounted) {
  //       this.setState((prevState, props) => ({active: props.active()}));
  //     }
  //     if (_.contains(['audio', 'options', 'stickers'], this.state.active)) {
  //       this.refs.chatTextInput.focus();
  //     }
  //   }
  // }

  // componentWillUnmount() {
  //   this._isMounted = false;
  // }

  render() {
    return (
      <TextInput 
        ref="chatTextInput"
        placeholder={this.props.placeholder}
        placeholderTextColor={this.props.placeholderTextColor}
        multiline={this.props.multiline}
        onChange={(e) => {
          this.props.onChange(e);
        }}
        onFocus={() => this.props.setActive('keyboard')}
        style={[styles.textInput, this.props.textInputStyle, {
          height: this.props.composerHeight,
        }]}
        value={this.props.text}
        enablesReturnKeyAutomatically={true}
        underlineColorAndroid="transparent"
        {...this.props.textInputProps}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    marginLeft: 5,
    fontSize: 16,
    lineHeight: 16,
    alignSelf:'center',
    alignItems:'center',
    justifyContent:'center',
    textAlignVertical:'center',
    padding: 5,
    marginTop: Platform.select({
      ios: 6,
      android: 0,
    }),
    marginBottom: Platform.select({
      ios: 5,
      android: 0,
    }),
  },
});

Composer.defaultProps = {
  // active: null,
  onChange: () => {},
  composerHeight: Platform.select({
    ios: 33,
    android: 41,
  }), // TODO SHARE with GiftedChat.js and tests
  text: '',
  placeholder: 'Type a message...',
  placeholderTextColor: '#b2b2b2',
  textInputProps: null,
  multiline: true,
  textInputStyle: {},
};

Composer.propTypes = {
  // active: React.PropTypes.func,
  onChange: React.PropTypes.func,
  composerHeight: React.PropTypes.number,
  text: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  placeholderTextColor: React.PropTypes.string,
  textInputProps: React.PropTypes.object,
  multiline: React.PropTypes.bool,
  textInputStyle: TextInput.propTypes.style,
};
