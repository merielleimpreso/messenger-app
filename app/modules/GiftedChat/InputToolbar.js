import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import _ from 'underscore';
import Composer from './Composer';
import Send from './Send';
import Users from './../config/db/Users';

export default class InputToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser: this.props.loggedInUser(),
    }
    this.render = this.render.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state.loggedInUser) != JSON.stringify(this.props.loggedInUser())) {
      // this.subscribe();
      if (this._isMounted) {
        this.setState({
          loggedInUser: this.props.loggedInUser()
        });
      }
    }
  }

  renderActions() {
    if (this.props.renderActions) {
      return this.props.renderActions(this.props);
    }
    return null;
  }

  renderSend() {
    if (this.props.renderSend) {
      return this.props.renderSend(this.props);
    }
    return <Send {...this.props}/>;
  }

  renderTailBubble() {
    /*if (this.props.renderTailBubble) {
      return this.props.renderTailBubble(this.props);
    }*/
    return null;
  }

  renderComposer() {
    if (this.props.renderComposer) {
      return this.props.renderComposer(this.props);
    }

    return (
      <Composer
        {...this.props}
      />
    );
  }

  renderAccessory() {
    if (this.props.renderAccessory) {
      return (
        <View style={[styles.accessory, this.props.accessoryStyle]}>
          {this.props.renderAccessory(this.props)}
        </View>
      );
    }
    return null;
  }

  /*render() {
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <View style={[styles.primary, this.props.primaryStyle]}>
          {this.renderActions()}
          {this.renderComposer()}
          {this.renderSend()}
        </View>
        {this.renderAccessory()}
      </View>
    );
  }*/

  render() {
    let otherUser = this.props.otherUser;
    if (this.props.loggedInUser().groups[otherUser._id]) {
      return this.props.renderNotInParty('group');
    } else if (otherUser) {
      let isContact = _.contains(this.props.loggedInUser().contacts, otherUser._id);
      let friendRequestReceived = this.props.loggedInUser().friendRequestReceived;
      let ignoredRequest = _.find(friendRequestReceived, function(i){ return i.userId == otherUser._id && i.date == null; });
      let friendReceivedIds = _.pluck(friendRequestReceived, "userId");
      let hasFriendRequest = _.contains(friendReceivedIds, otherUser._id);
      if (!isContact) {
        if (ignoredRequest) return this.props.renderNotInParty('ignoredFriend');
        if (hasFriendRequest) return this.props.renderFriendRequest();
      }
    }
    return this.renderTypingArea();
  }

  renderTypingArea() {
    return (
      <View style={[styles.container, //this.props.containerStyle
        ]}>
        <View style={[styles.primary, //this.props.primaryStyle
          ]}>
          <View style={[styles.primary, styles.inputBubble]}>
            {this.renderActions()}
            {this.renderComposer()}
          </View>
          {this.renderTailBubble()}
          {this.renderSend()}
        </View>
        {this.renderAccessory()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // borderTopWidth: StyleSheet.hairlineWidth,
    // borderTopColor: '#b2b2b2',
    // backgroundColor: '#FFFFFF',
  },
  primary: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent:'center',
  },
  inputBubble: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginLeft: 3,
    marginRight: 3,
    borderRadius: 12,
    //borderRadius: 5, // use with renderTailBubble
    //borderTopRightRadius: 0
  },
  accessory: {
    height: 44,
  },
});

InputToolbar.defaultProps = {
  renderAccessory: null,
  renderActions: null,
  renderSend: null,
  renderComposer: null,
  containerStyle: {},
  primaryStyle: {},
  accessoryStyle: {},
};

InputToolbar.propTypes = {
  renderAccessory: React.PropTypes.func,
  renderActions: React.PropTypes.func,
  renderSend: React.PropTypes.func,
  renderComposer: React.PropTypes.func,
  containerStyle: View.propTypes.style,
  primaryStyle: View.propTypes.style,
  accessoryStyle: View.propTypes.style,
};
