import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Navigation from '../actions/Navigation';


export default class Actions extends React.Component {
  constructor(props) {
    super(props);
    this.onActionsPress = this.onActionsPress.bind(this);
  }

  onActionsPress() {
    const options = Object.keys(this.props.options);
    const cancelButtonIndex = Object.keys(this.props.options).length - 1;
    this.context.actionSheet().showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    },
    (buttonIndex) => {
      let i = 0;
      for (let key in this.props.options) {
        if (this.props.options.hasOwnProperty(key)) {
          if (buttonIndex === i) {
            this.props.options[key](this.props);
            return;
          }
          i++;
        }
      }
    })
  }

  renderIcon() {
    if (this.props.icon) {
      return this.props.icon();
    }
    return (
      <View
        style={[styles.wrapper, this.props.wrapperStyle]}
      >

      </View>
    );
  }

  render() {
    let color = this.props.color;
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <TouchableOpacity
          style={styles.container}
          onPress={() => Navigation.onPress(this.props.setIsOptionsPress)}>
            <KlikFonts name='plus' style={[styles.icon, {color: color.TEXT_LIGHT}]}/>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.container}
          onPress={() => Navigation.onPress(this.props.setIsEmojiPress)}>
            <KlikFonts name='emoticon' style={[styles.icon, {color: color.TEXT_LIGHT}]}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row', 
    height: 40
    // marginBottom: 2
  },
  icon: {
    fontSize: 25,
    textAlignVertical: 'top',
    textAlign: 'center',
    margin: 5
  },
  wrapper: {
    borderRadius: 13,
    borderColor: '#b2b2b2',
    borderWidth: 2,
    flex: 1,
  },
  iconText: {
    color: '#b2b2b2',
    fontWeight: 'bold',
    fontSize: 16,
    backgroundColor: 'transparent',
    textAlign: 'center',
  },
});

Actions.contextTypes = {
  actionSheet: React.PropTypes.func,
};

Actions.defaultProps = {
  onSend: () => {},
  options: {},
  icon: null,
  containerStyle: {},
  iconTextStyle: {},
};

Actions.propTypes = {
  onSend: React.PropTypes.func,
  options: React.PropTypes.object,
  icon: React.PropTypes.func,
  containerStyle: View.propTypes.style,
  iconTextStyle: Text.propTypes.style,
};
