/*
Author: Merielle Impreso
Date Created: 2016-04-14
Date Updated: 2016-04-21
Description: This file displays the login page.
  This page has the following action:
    * When Login button is clicked:
      1. Checks if username & password has a match in the database.
      2. If login info is correct, it will check if the user is verified.
      3. If verified, redirect to Tabs page.
      4. If not verified, redirect to SignupVerification page.
    * When Signup button is clicked:
      1. Redirect to Signup page.
Used In: Login.ios

Changelog:
update: 2016-04-22 (by Merielle I.)
  - Added isMounted flag to fix setState warnings.
  - Display verification code (for testing only).
  - Redirect to SignupVerification page if not yet verified.
update: 2016-04-28 (by John Rezan Leslie Baguna)
  - Disables textinput and sign up button when logging in.
update: 2016-04-29 (by John Rezan Leslie Baguna)
  - Fix unable to type after wrong combination.
update: 2016-04-29 (by Merielle I.)
  - Fixed double tapping on login by changing AsyncStorage to dpp.call in getting currentUser.
update: 2016-05-04 (Merielle I.)
  - Add KeyboardSpacer
  - Implement UI color scheme
update: 2016-05-10 (Merielle I.)
  - Added timeout
update: 2016-05-17 (Merielle I.)
  - Added NetInfo to check for network connection
update: 2016-07-12 by Merielle I.
  - removed warning message when scrolling
*/


'use strict';

import React, {
  Component,
} from 'react';

import {
  ActivityIndicator,
  AsyncStorage,
  TouchableWithoutFeedback,
  Image,
  NetInfo,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
// const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const styles = StyleSheet.create(require('./../styles/styles_common.js'));

import Icon from 'react-native-vector-icons/Ionicons';
import KeyboardSpacer from './KeyboardSpacer';
import ScreenFailedToConnect from './ScreenFailedToConnect';
import Modal from 'react-native-simple-modal';
import ScreenLoading from './ScreenLoading';
var GLOBAL = require('./config/Globals.js');

var Progress = require('react-native-progress');
export default React.createClass({

  // Get initial state.
  getInitialState() {
    return {
      number: '',
      password: '',
      error: '',
      user: '',
      isConnected: true,
      isReconnectingToServer: false,
      canSignIn: true,
      willSignIn: false,
      color: GLOBAL.COLOR_THEME['ORIGINAL'],
      modalKliklabsVisible: false,
      modalConfirmCode: false,
      modalKlikSignUp: false
    };
  },

  /*
  Put flag to check if component is mounted.
  Add event listener to check for connection
  */
  componentDidMount() {
    this.isMounted = true;
    this.addConnectivityListener();
    this.getTheme();
  },

  /*
  Put flag to check if component is unmounted.
  Remove event listener for connection when unmounted.
  */
  componentWillUnmount() {
    this.isMounted = false;

    NetInfo.isConnected.removeEventListener(
      'change',
      this.handleConnectivityChange
    );
  },

  getTheme() {
    var color = this.props.color;

    if (color) {
      if (this.isMounted) {
        this.setState({ color: color });
      }
    } else {
      AsyncStorage.getItem('theme')
      .then(theme => {
        if (theme) {
          var color = GLOBAL.COLOR_THEME[theme]
          if (this.isMounted) {
            this.setState({ color: color });
          }
        }
      });
    }
  },

  // Add listener to check for connectivity
  addConnectivityListener() {
    NetInfo.isConnected.addEventListener(
      'change',
      this.handleConnectivityChange
    );
    NetInfo.isConnected.fetch().done(
      (isConnected) => {
        if (this.isMounted) {
          this.setState({isConnected});
        }
      }
    );
  },

  // Set state if there is connectivity
  handleConnectivityChange(isConnected) {
    if (isConnected && this.state.isConnected == false) {
      this.initialize();
    }
    if (this.isMounted) {
      this.setState({
        isConnected,
      });
    }
  },

  // Initialize connection if it is disconnected
  initialize() {
    if (this.isMounted) {
      this.setState({
        isReconnectingToServer: true
      })
    }

    ddp.initialize()
    .then((result) => {
      if (this.isMounted) {
        this.setState({
          isReconnectingToServer: false,
          isConnected: true,
        })
      }
    })
    .catch(error => {
      if (this.isMounted) {
        this.setState({
          willSignIn: false,
          isConnected: false,
          isReconnectingToServer: false,
        })
      }
    })
  },

  // Render login form
  renderLoginForm() {
    var color = this.state.color;
    return (
      <ScrollView contentContainerStyle={[styles.container, {backgroundColor:color.CHAT_BG}]}>
      <Image style={ styles.loginAppName }
        source={require('./../images/title.png')} />

        <View style={{marginRight:40, marginLeft:40}}>
          <TextInput ref="number"
            onChangeText={(number) => this.setState({number: number})}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            selectionColor={color.THEME}
            placeholder='Mobile Number'
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            autoCapitalize='none'
            returnKeyType='next'
            onSubmitEditing={(event) => {this.refs.password.focus();}}
            rejectResponderTermination={false}
          />
          <View style={[styles.loginSeparator, {backgroundColor:color.THEME}]} />
        </View>
        <View style={{marginRight:40, marginLeft:40}}>
          <TextInput ref="password"
            onChangeText={(password) => this.setState({password: password})}
            style={[styles.textInput, {color:color.TEXT_DARK}]}
            selectionColor={color.THEME}
            placeholder='Password'
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            autoCapitalize='none'
            onSubmitEditing={(event) => this.checkInput()}
            secureTextEntry
            rejectResponderTermination={false}
          />
          <View style={[styles.loginSeparator, {backgroundColor:color.THEME}]} />
        </View>

        <Text style={[styles.loginError, {color:color.ERROR}]}>
          {this.state.error}
        </Text>

        <View>
          <TouchableHighlight
            disabled={false}
            onPress={() => this.checkInput()}
            style={[styles.loginButton, {backgroundColor:color.THEME}]}
            underlayColor={color.HIGHLIGHT}>
            <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}>
              Login
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            disabled={false}
            onPress={() => this.goToSignUp()}
            style={[styles.loginButtonSignup, {backgroundColor:color.THEME}]}
            underlayColor={color.HIGHLIGHT}>
            <Text style={[styles.loginButtonText, {color:color.BUTTON_TEXT}]}>
              Sign up
            </Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => this.goToKliklabs()}
            style={[styles.buttonLogin, {backgroundColor: '#fff', borderWidth:1, borderColor:color.THEME}]}
            underlayColor={color.HIGHLIGHT}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <Image source={require('./../images/kliklabs.png')} style={{width:36, alignSelf:'center', height:28}} />
              <Text style={[styles.loginButtonText, {color: color.THEME, flex:1, marginLeft:10}]}>
                Sign up with kliklabs
              </Text>
            </View>
          </TouchableHighlight>
          <TouchableWithoutFeedback onPress={() => this.setState({modalEmailVisible: true})}>
            <View style={{flex:1, alignItems:'center'}}><Text style={{color: color.THEME}}>
              Forget Password?
            </Text></View>
          </TouchableWithoutFeedback>
        </View>
        <Modal
          open={this.state.modalEmailVisible}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          modalDidOpen={() => console.log('Modal opened')}
          modalDidClose={() => this.setState({modalEmailVisible: false, email: ''})}
          modalStyle={{
            borderRadius: 10,
            marginTop: 20,
            marginBottom: 20,
            backgroundColor: '#F5F5F5'
          }}>
        <View style={{flex: 1, padding:20}}>
            <Text style={{color: color.THEME, marginBottom:40, textAlign:'center'}}>Please type your email</Text>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                placeholder='Email'
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.email}
                autoFocus={true}
                onChangeText={(email) => this.setState({email})}
                keyboardType='email-address'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, paddingBottom: 5,marginBottom:10,}}
              />
            </View>
            <TouchableHighlight
              onPress={() => {this.forgetPassword()}}
              underlayColor={color.HIGHLIGHT}
              style={{backgroundColor: color.BUTTON, borderColor: color.BUTTON,
                alignItems:'center', borderWidth: 1, borderRadius: 10, justifyContent: 'center',
                height: 40, flex:1
            }}>
              <Text
                style={{color: color.BUTTON_TEXT}}
              >Reset Password</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <Modal
          open={this.state.modalKliklabsVisible}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          modalDidOpen={() => console.log('Modal opened')}
          modalDidClose={() => this.setState({
            modalKliklabsVisible: false
          })}
          modalStyle={{
            borderRadius: 10,
            marginTop: 20,
            marginBottom: 20,
            backgroundColor: '#F5F5F5'
          }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20}}>
            <Text style={{color: color.THEME}}>Please type your Kliklabs username and number</Text>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                placeholder='Username'
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.username}
                onChangeText={(username) => this.setState({username})}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                placeholder='Mobile Number'
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.mobile}
                onChangeText={(mobile) => this.setState({mobile})}
                keyboardType='numbers-and-punctuation'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <TouchableHighlight
              onPress={() => {this.signInWithKliklabs()}}
              underlayColor={color.HIGHLIGHT}
              style={{backgroundColor: color.BUTTON, borderColor: color.BUTTON,
                alignItems:'center', borderWidth: 1, borderRadius: 10, justifyContent: 'center',
                height: 40, width: 275
            }}>
              <Text
                style={{color: color.BUTTON_TEXT}}
              >Sign up</Text>
            </TouchableHighlight>
          </View>
        </Modal>

        <Modal
          open={this.state.modalConfirmCode}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          modalDidOpen={() => console.log('Modal opened')}
          modalDidClose={() => this.setState({
            modalConfirmCode: false
          })}
          modalStyle={{
            borderRadius: 10,
            marginTop: 20,
            marginBottom: 20,
            backgroundColor: '#F5F5F5'
          }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20}}>
            <Text style={{color: color.THEME}}>Confirmation code</Text>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.code}
                onChangeText={(code) => this.setState({code})}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <TouchableHighlight
              onPress={() => {this.confirmCode()}}
              underlayColor={color.HIGHLIGHT}
              style={{backgroundColor: color.BUTTON, borderColor: color.BUTTON,
                alignItems:'center', borderWidth: 1, borderRadius: 10, justifyContent: 'center',
                height: 40, width: 275
            }}>
              <Text
                style={{color: color.BUTTON_TEXT}}
              >Confirm</Text>
            </TouchableHighlight>
          </View>
        </Modal>

        <Modal
          open={this.state.modalKlikSignUp}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          modalDidOpen={() => console.log('Modal opened')}
          modalDidClose={() => {
          if(this._hasMounted)
          this.setState({
            modalKlikSignUp: false
          })}}
          modalStyle={{
            borderRadius: 10,
            marginTop: 20,
            marginBottom: 20,
            backgroundColor: '#F5F5F5'
          }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20}}>
            <Text style={{color: color.THEME}}>Fill up information</Text>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikFirstName'
                placeholderTextColor={color.TEXT_LIGHT}
                placeholder="First Name"
                value={this.state.klikFirstName}
                onChangeText={(klikFirstName) => this.setState({klikFirstName})}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikLastName'
                placeholderTextColor={color.TEXT_LIGHT}
                placeholder="Last Name"
                value={this.state.klikLastName}
                onChangeText={(klikLastName) => this.setState({klikLastName})}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikEmail'
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.klikEmail}
                placeholder="Email"
                onChangeText={(klikEmail) => this.setState({klikEmail})}
                keyboardType='email-address'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikConfirmEmail'
                placeholderTextColor={color.TEXT_LIGHT}
                value={this.state.klikConfirmEmail}
                placeholder="Confirm Email"
                onChangeText={(klikConfirmEmail) => this.setState({klikConfirmEmail})}
                keyboardType='email-address'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikPass'
                placeholderTextColor={color.TEXT_LIGHT}
                placeholder="Password"
                value={this.state.klikPass}
                onChangeText={(klikPass) => this.setState({klikPass})}
                secureTextEntry={true}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <View style={[styles.borderRounded, {borderColor: color.BUTTON, alignItems:'center'}]}>
              <TextInput
                ref='klikConfirmPass'
                placeholderTextColor={color.TEXT_LIGHT}
                placeholder="Confirm Password"
                value={this.state.klikConfirmPass}
                secureTextEntry={true}
                onChangeText={(klikConfirmPass) => this.setState({klikConfirmPass})}
                keyboardType='default'
                style={{
                  height: 40, backgroundColor: 'transparent',color: color.THEME,
                  width: 275, textAlign: 'center', paddingBottom: 5}}
              />
            </View>
            <TouchableHighlight
              onPress={() => {this.onPressedSignup()}}
              underlayColor={color.HIGHLIGHT}
              style={{backgroundColor: color.BUTTON, borderColor: color.BUTTON,
                alignItems:'center', borderWidth: 1, borderRadius: 10, justifyContent: 'center',
                height: 40, width: 275
            }}>
              <Text
                style={{color: color.BUTTON_TEXT}}
              >Sign Up</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        {(() => {
          if(!this.state.modalEmailVisible)
          return
          <KeyboardSpacer getKeyboardStatus={this.setKeyboardStatus} />;
        })()}

      </ScrollView>
    );
  },
  forgetPassword() {
    let emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (this.state.email != undefined && emailPattern.test(this.state.email)) {
      ddp.call('resetPass', [this.state.email])
      .then(result => {


        if (result === 'Password Sent') {
          this.setState({modalEmailVisible: false, email: ''});
          alert("New password sent in your email address.");
        } else {
          alert("Email does not exist.")
          //console.warn('wrong', result)
        }
      })
      .catch(err => {
        alert("Email does not exist.")
        console.log('[resetPass] Error: ', err.reason)
      });

    } else {
      alert("Invalid email address.")
      //console.warn('invalid')
    }
  },
  // Render views
  render() {
    var render = <View />;
    var color = this.state.color;
    if (this.state.isReconnectingToServer) {
      render = <ScreenLoading isLoading={true} text={GLOBAL.TEXT_CONNECTING_TO_SERVER} color={color}/>
    } else {
      if (this.state.isConnected) {
        if (this.state.canSignIn) {
          if (GLOBAL.SHOULD_SHOW_INTERNET_FAILURE) {
            render = (this.state.willSignIn)
              ? <ScreenLoading isLoading={true} text={GLOBAL.TEXT_LOGGING_IN} color={color} />
              : this.renderLoginForm();
          } else {
            render = this.renderLoginForm();
          }
        } else {
          render = <ScreenLoading isLoading={true} text={GLOBAL.TEXT_SERVER_TAKING_A_WHILE} color={color} />
        }
      } else {
        if (GLOBAL.SHOULD_SHOW_INTERNET_FAILURE) {
          render = <ScreenFailedToConnect navigator={this.props.navigator} text={GLOBAL.TIMEOUT_MESSAGE_DISCONNECTED} reload={this.initialize} />
        } else {
          render = this.renderLoginForm();
        }
      }
    }
    return render;
  },

  // Reloading for signing in
  reload() {
    this.setState({
      canSignIn: true,
      willSignIn: false
    });
  },

  // Check for blank input.
  checkInput() {
    if (this.state.number == '') {
      if (this.isMounted) {
        this.setState({
          error: 'Please enter your username'
        });
      }
      this.reload();
      this.refs.number.focus();
      this.refs.password.clear();
    } else if (this.state.password == '') {
      if (this.isMounted) {
        this.setState({ error: 'Please enter your password' });
      }
      this.reload();
      this.refs.password.focus();
    } else {
      if (this.isMounted) {
        this.setState({ error: '' });
      }
      this.handleSignIn();
    }
  },
  /*
  Handle the login process, call loginWithUsername method.
  If login is successful, check if user is verified.
    * If user is verified, call goToTabs.
    * If user is not veried, call goToSignupVerification.
  If login is not successful, display error message.
  */
  handleSignIn() {
    this.handleSignInTimeout();

    let { number, password } = this.state;
    this.setState({
      willSignIn: true
    });

    ddp.loginWithUsername(number, password, (err, res) => {
      ddp.onAuthResponse(err, res);
      if (res) {
        ddp.call('currentUser', [])
        .then(result => {
          if (result) {
            if (result.profile.isVerified == true) {
              this.goToTabs();
            } else {
              this.goToSignupVerification();
            }
          }
        });
      } else {
        this.reload();
        this.refs.number.focus();
        if (this.isMounted) {
          this.setState({
            error: 'Incorrect login combination',
            willSignIn: false
          });
        }
      }
    });
  },

  // Handle sign in timeout
  handleSignInTimeout() {
    setTimeout(() => {
      if (this.state.willSignIn) {
        this.setState({
          willSignIn: false,
          canSignIn: false,
       });
      }
    }, GLOBAL.TIMEOUT);
  },

  // Clear the views.
  clearView() {
    this.setState({error: ''});
    //This causes error : Cannot read property 'clear' of undefined
    //FIX ME
    // this.refs.number.clear();
    // this.refs.password.clear();
  },

  // Go to Tabs when login is successful and user is verified.
  goToTabs() {
    this.props.navigator.push({
      id: 'tab-bar',
      title: 'Recent',
      navigationBarHidden: false,
      passProps: {myElement: 'text'}
    });
  },

  // Go to SignupVerification when login is successful but the user is not verified.
  goToSignupVerification() {
    this.clearView();
    if (this.isMounted) {
      this.setState({
        willSignIn: false
      });
    }
    this.props.navigator.push({
      id: 'verification',
      title: 'Verify',
      navigationBarHidden: false,
      color: this.props.color,
      passProps: {myElement: 'text'}
    });
  },

  // Go to Signup page when Signup button is clicked.
  goToSignUp() {
    if (!this.state.willSignIn) {
      this.props.navigator.push({
        id:'signup',
        title: 'Sign up',
        navigationBarHidden: false,
        color: this.state.color,
        passProps: {myElement: 'text'}
      });
      this.clearView();
    }
  }


});
