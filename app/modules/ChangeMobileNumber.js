/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-06
 * Date Updated: 2016-11-07
 * Description: Component used for Changing password
 */
'use strict';

import React, { Component } from 'react';
import {
  AsyncStorage,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  ToolbarAndroid,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View,
 } from 'react-native';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KeyboardSpacer from './KeyboardSpacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';
import SystemAccessor from './SystemAccessor';
import Toolbar from './common/Toolbar';

const GLOBAL = require('./config/Globals.js');
// const styles = StyleSheet.create(require('./../styles/styles_common.js'));
var styles = StyleSheet.create(require('./../styles/styles.main.js'));

const WINDOW_SIZE = Dimensions.get('window');
const MINIMUM_MOBILE_CHAR = 10;

class ChangeMobileNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      accessToken: '',
      alertMessage: '',
      verificationCode: '',
      loggedInUser: this.props.loggedInUser(),
      selectedCountry: 'Indonesia',
      selectedCode: '+62',
      showConfirmCodeForm: false,
      oldMobileNumber: '',
      mobile: '',
      isShowCheckConnection: false,
      isShowMessageModal: false,
      isLoading: false
    }

    this.changeMobileNumber = this.changeMobileNumber.bind(this);
    this.confirmCode = this.confirmCode.bind(this);
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.onPressCountry = this.onPressCountry.bind(this);
    this.setIsLoading = this.setIsLoading.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
    this.setSelectedCountry = this.setSelectedCountry.bind(this);
    this.showSmsConfirmCode = this.showSmsConfirmCode.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    AsyncStorage.getItem('accessToken').then((accessToken) => {
      if (accessToken) { // A login token is saved.
        this.setState({accessToken})
        console.log('accessToken: ', accessToken);
      } else { // No login token saved.
        console.log('accessToken: ', accessToken);
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex:1, backgroundColor: color.CHAT_BG}}>
        {this.renderNavBar()}
        {this.props.renderConnectionStatus()}
        {(() => {
          if (this.state.showConfirmCodeForm) {
            return this.confirmCodeForm()
          } else {
            return this.changeMobileForm()
          }
        })()}
        <Text style={[form.warning, {color: color.ERROR}]}>
          {this.state.warningMessage}
        </Text>
        {this.renderModal()}
      </View>
    )
  }

  renderNavBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let navTitle = !this.state.showConfirmCodeForm ? 'Change Mobile Number' : 'Verify Code';
    return <Toolbar title={navTitle} withBackButton={true} {...this.props} />
  }

  changeMobileForm () {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[form.wrapper]}>
        <TouchableHighlight onPress={() => this.onPressCountry()} underlayColor={'#e8e5e6'}
          style={[form.textInput, {borderColor: color.THEME}]}>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{color: color.TEXT_DARK, textAlignVertical: 'center'}}>
              {this.state.selectedCountry}
            </Text>
            <Text style={{color: color.TEXT_INPUT_PLACEHOLDER, textAlignVertical: 'center'}}>
              {this.state.selectedCode}
            </Text>
          </View>
        </TouchableHighlight>
        <View style={[form.textInput, {borderColor: color.THEME, paddingBottom:10}]}>
          <TextInput ref='mobile'
            placeholder='Mobile Number'
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            value={this.state.mobile}
            onChangeText={(mobile) => {
              mobile = (mobile.trim() == '') ? '' : mobile;
              this.setState({mobile: mobile});
            }}
            keyboardType='phone-pad'
            returnKeyType='next'
            onSubmitEditing={(event) => {this.changeMobileNumber()}}
            style={[form.text, {color: color.TEXT_DARK}]}
          />
        </View>
        <Text />
        <Button text="Save" color={color.THEME} isDefaultWidth={true} onPress={this.changeMobileNumber} style={form.button} underlayColor={color.HIGHLIGHT} />
      </View>
    )
  }

  confirmCodeForm () {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center', flexDirection:'row', backgroundColor: color.CHAT_BG}}>
        <ScrollView keyboardDismissMode='interactive' keyboardShouldPersistTaps={true} style={{flex: 1}}>
          <View style={styles.loginContainer}>
            <Image style={{width:190, height:155}} source={require('./../images/code_signup.png')} />
            <Text style={{color: color.TEXT_LIGHT, fontSize: 16, marginTop: 50}}>Enter Confirmation Code</Text>
            <Text style={{color: color.HIGHLIGHT, width: 200, textAlign: 'center', fontSize: 13}}>
              Enter the 4-digit code we sent to your phone number
            </Text>
            {/*<TouchableWithoutFeedback onPress={() => this.showSmsConfirmCode()}>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{color: color.BUTTON, fontSize: 13}}>
                  Show Code (FOR TESTING ONLY!)
                </Text>
              </View>
            </TouchableWithoutFeedback>*/}
            <View style={[form.wrapper]}>
              <View style={[form.textInput, {borderColor: color.THEME}]}>
                <TextInput ref='confirm'
                  placeholder='Confirmation code'
                  value={this.state.code}
                  onChangeText={(code) => this.setState({code})}
                  returnKeyType='next'
                  onSubmitEditing={(event) => this.confirmCode()}
                  style={[form.text, {color: color.TEXT_DARK, marginTop: 10}]}
                />
              </View>
              <Text />
              <Button text="Confirm" color={color.THEME} isDefaultWidth={true} onPress={this.confirmCode} style={form.button} underlayColor={color.HIGHLIGHT} />
            </View>
          </View>
          <KeyboardSpacer />
        </ScrollView>
      </View>
    )
  }



  onPressCountry() {
    this.refs.mobile.blur();
    requestAnimationFrame(() => {
      this.props.navigator.push({
        id:'inputcountrycode',
        name:'InputCountryCode',
        theme: this.props.theme,
        selectedCountry: this.state.selectedCountry,
        setSelectedCountry: this.setSelectedCountry
      });
    });
  }

  setSelectedCountry(country) {
    this.setState({
      selectedCountry: country.name,
      selectedCode: country.dial_code
    });
  }

  onPressButtonClose() {
      // setTimeout(() => {
      //   this.props.navigator.pop(0);
      // }, 2000 );
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  clearText(fieldName) {
    this.refs[fieldName].setNativeProps({text:''});
  }

  changeMobileNumber() {
    let numRegex = /^[0-9]*$/;
    if (!this.state.mobile) {
      this.setIsShowMessageModal('Please enter mobile number');
      return;
    } else if (!numRegex.test(this.state.mobile)) {
      this.setIsShowMessageModal('Please enter valid mobile number');
      return;
    } else if (this.state.mobile.length < MINIMUM_MOBILE_CHAR) {
      this.setIsShowMessageModal('Minimum mobile number is 10 characters');
      return;
    }

    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      let code = this.state.selectedCode.replace('+', '');
      let mobileInput = this.state.mobile;
      if (mobileInput.charAt(0) == '0') {
        mobileInput = mobileInput.substr(1, mobileInput.length-1);
      }
      let mobile = `${code}${mobileInput}`;
      this.refs.mobile.blur();
      this.setIsLoading(true);
      ddp.call('updateMobile', [mobile, this.state.accessToken]).then( toDecrypt => {

        if (toDecrypt.length >= 16) {
          SystemAccessor.decrypt(toDecrypt, this.state.accessToken.substring(0, 16), (err, response) => {
            response = JSON.parse(response);

            console.log("Response: ", response);
            if (response) {
              if( !response.sukses ) {
                this.setIsShowMessageModal(response.msg);
                this.setIsLoading(false);
                return;
              }

              let urlString = response.url;
              // let verificationCode = urlString.substring(urlString.indexOf('enter')+6, urlString.indexOf('to')-1);
              if (this._isMounted) {
                this.setState({
                  showConfirmCodeForm: true,
                  // verificationCode: verificationCode,
                  mobile: mobile
                })
              }
              this.setIsLoading(false);
            } else {
              this.setIsLoading(false);
              console.log('Something went wrong. ', err);
              this.setIsShowMessageModal('Something went wrong, please try again later.');
            }
          });
        } else {
          console.log('ERROR: toDecrypt', toDecrypt);
          this.setIsLoading(false);
          this.setIsShowMessageModal('Something went wrong, please try again later.');
        }
      }).catch( e => {
        this.setIsLoading(false);
        console.log('Something went wrong. ', e);
        this.setIsShowMessageModal('Something went wrong, please try again later.');
        // this.setState({warningMessage: e.reason})
      });
    }
  }

  confirmCode(){
    this.refs.confirm.blur();
    if (this.state.code) {
      if (!this.props.hasInternet()) {
        this.setIsShowCheckConnection();
      } else {
        this.setIsLoading(true);
        ddp.call('updateMobileVerify', [this.state.mobile, this.state.code, this.state.accessToken]).then( toDecrypt => {

          if (toDecrypt.length >= 16) {
            SystemAccessor.decrypt(toDecrypt, this.state.accessToken.substring(0, 16),(err, response)=>{
              response = JSON.parse(response);

              if (response.sukses) {
                this.props.setMobileNumber(this.state.mobile);
                this.setIsShowMessageModal('Your mobile number has been updated.');
                // this.onPressButtonClose();
              } else {
                this.setIsShowMessageModal('Incorrect Code');
              }
              this.setIsLoading(false);
            });
          } else {
            this.setIsLoading(false);
            this.setIsShowMessageModal('Something went wrong, please try again later.');
          }
        }).catch(err => {
          this.setIsLoading(false);
          console.log('Something went wrong. ', err);
          this.setIsShowMessageModal('Something went wrong, please try again later.');
        });
      }
    } else {
      this.setIsShowMessageModal('Invalid code')
    }
  }


  renderModal() {
    if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.state.isLoading) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => console.log('loading')}
         modalType={'loading'}
        />
      );
    } else if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState((previousState) => {
        if (previousState.alertMessage.indexOf('updated') !== -1 && message == "") {
          this.props.navigator.pop();
        }
        return {
          isShowMessageModal: !this.state.isShowMessageModal,
          alertMessage: message
        }
      });
    }
  }

  setIsLoading(isLoading) {
    if (this._isMounted) {
      this.setState({isLoading: isLoading});
    }
  }

  showSmsConfirmCode(){
    this.refs.confirm.blur();
    if(this.state.verificationCode) {
      this.setIsShowMessageModal(`SMS Confirmation Code is: ${this.state.verificationCode}`);
    } else {
      this.setIsShowMessageModal('SMS text not found');
    }
  }

}


let form = StyleSheet.create({
  wrapper: {
    margin: 20
  },
  warning: {
    textAlign: 'center',
    alignItems: 'stretch',
    fontSize: 12,
    height: 20,
    marginTop: 8,
    marginBottom: 5,
  },
  textInput: {
    flex: 1,
    borderBottomWidth: 1,
    marginBottom: 2
  },
  text: {
    height: 40,
    fontSize: 14,
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  button: {
    borderRadius: 5,
    alignItems: 'stretch',
    alignSelf:'center'
  }
});

module.exports = ChangeMobileNumber;
