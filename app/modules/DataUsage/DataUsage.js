'use strict';

import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import GLOBAL from '../config/Globals.js';
import DataUsageRender from './DataUsageRender';
import StoreCache from '../config/db/StoreCache';

export default class DataUsage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAutoSaveMedia: this.props.isAutoSaveMedia()
    }
    this.onSwitchIsAutoSaveMedia = this.onSwitchIsAutoSaveMedia.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <DataUsageRender
        onSwitchIsAutoSaveMedia={this.onSwitchIsAutoSaveMedia}
        {...this.props}
      />
    );
  }

  onSwitchIsAutoSaveMedia(isAutoSaveMedia) {
    StoreCache.storeCache(StoreCache.keys.isAutoSaveMedia, JSON.stringify(isAutoSaveMedia));
    this.props.setIsAutoSaveMedia(isAutoSaveMedia)
    if (this._isMounted) {
      this.setState({ isAutoSaveMedia: isAutoSaveMedia });
    }
  }
}