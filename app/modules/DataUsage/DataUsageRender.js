'use strict';

import React, { Component } from 'react';
import {
  View
} from 'react-native';
import GLOBAL from '../config/Globals.js';
import ListRow from '../common/ListRow';
import styles from './styles';
import Toolbar from '../common/Toolbar';

const DataUsageRender = (props) => {
  let { isAutoSaveMedia, onSwitchIsAutoSaveMedia, theme } = props;
  let color = GLOBAL.COLOR_THEME[theme()];

  return (
    <View style={styles.container}>
      <Toolbar title={'Data Usage'} withBackButton={true} {...props}  />
      <ListRow onSwitch={(value) => onSwitchIsAutoSaveMedia(value)} 
        switchValue={isAutoSaveMedia()}
        labelText={'Auto-save Media'}
        detailText={'Save media to your Gallery'} 
        color={color} 
        addSeparator={true}/>
    </View>
  );
}

export default DataUsageRender;