/*
Author: Merielle N. Impreso
Date Created: 2016-06-30
Date Updated:
Description: Display each row of timeline post.

update: 07 - 01 - 16 by John B.
  - isLiked
  -
update: 07- 22 - 16 by John B.
  - support for reaction

*/
'use strict';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  LayoutAnimation,
  ListView,
  TouchableHighlight,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {consoleLog, timelineTimeOrDate, getMediaURL} from './Helpers';
import NavigationBar from 'react-native-navbar';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinkPreview from './LinkPreview';
import Post from './config/db/Post';
import Video from 'react-native-video';
import ScreenNoItemFound from './ScreenNoItemFound';
import MediaAutoGrid from './MediaAutoGrid';
import ImageWithLoader from './common/ImageWithLoader';
import SoundEffects from './actions/SoundEffects';
import SystemAccessor from './SystemAccessor';
import _ from 'underscore';
import moment from "moment";

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');

const SetIntervalMixin = {
  componentWillMount() {
    this.intervals = [];
  },
  setInterval() {
    this.intervals.push(setInterval.apply(null, arguments));
  },
  clearInterval() {
    this.intervals.forEach(clearInterval);
  },
  componentWillUnmount() {
    this.intervals.forEach(clearInterval);
  }
};

class TimelinePost extends Component {
  // mixins: [SetIntervalMixin]

  constructor(props) {
    super(props);
    this.state = {
      relativeTime:'',
      loading:true,
      isLoaded:false
    }
  }

  componentDidMount() {
    this._isMounted = true;
    // this.setInterval(this.tick, 1000);
    if (this.props.data.image || this.props.data.video) {
      this.getImageSize();
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  tick() {
    var post = this.props.data;
    var relativeTime = timelineTimeOrDate(post.CreateDate);
    if (relativeTime != this.state.relativeTime) {
      if (this._isMounted) {
        this.setState({ relativeTime: relativeTime });
      }
    }
  }

  getImageSize() {
    var post = this.props.post;
    var postImage = this.props.data.image;
    var postVideo = this.props.data.video;
    if (postImage) {
      Image.getSize(postImage.medium,
        (actualWidth, actualHeight) => {
          var height = windowSize.width * actualHeight / actualWidth;
          if (this.isMounted) {
            this.setState({
              imageHeight: height,
            });
          }
        },
        (error) => {
          console.log(error);
          if (this.isMounted) {
            this.setState({
              imageHeight: 150,
            });
          }
        }
      );
    }
    if (postVideo) {
      Image.getSize(postVideo,
        (actualWidth, actualHeight) => {
          console.log(actualHeight);
          var height = windowSize.width * actualHeight / actualWidth;
          // console.log(height);
          if (this.isMounted) {
            this.setState({
              videoHeight: height,
            });
          }
        },
        (error) => {
          console.log(error);
          if (this.isMounted) {
            this.setState({
              videoHeight: 150,
            });
          }
        }
      );
    }
  }

  render() {
    let data = {};
    let post = this.props.data;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let renderMedia = Boolean(post.isProfilePicture) ? this.renderProfilePicture()  : this.renderMedia();
    let postId = post.id;
    let isOnComment = this.props.isOnComment || false;
    let style = {
          marginTop:3,
          marginRight:6,
          marginBottom:3,
          marginLeft:6,
        }

    if (isOnComment) {
      style = {
          margin: 0,
          paddingTop:12
      }
    }

    return (
      <View ref={postId} style={[{backgroundColor: '#fff',alignSelf: 'center',justifyContent: 'center',padding: 10}, style]}>
        {this.renderImageNameDate()}
        {(() => {
          if (post.media.length) {
            return renderMedia
          }
        })()}
        {(this.props.data.link) && this.renderLink()}
        {this.renderFooter()}
      </View>
    );
  }

  renderImageNameDate() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let data = this.props.data;
    let date = moment( data.CreateDate ).format("MM/DD");
    let time = moment( data.CreateDate ).format('HH:mm');
    let userImage = getMediaURL(data.userImage),
        userImageWidth = windowSize.width * 0.1,
        borderRadius = userImageWidth * 0.5;

    return (
      <View style={{flexDirection:'row', alignSelf:'center'}}>
        <View>
          <ImageWithLoader
           style={{width:userImageWidth, height:userImageWidth, borderRadius:borderRadius}}
           color={color}
           image={userImage} />
        </View>
        <View style={{flexDirection:'row', flex: 2}}>
          <View style={{flex: 1, paddingLeft: 5, paddingRight: 5}}>
            <Text style={[styles.timelinePostUserName, {color:color.TEXT_DARK}]}>{data.userName}</Text>
            {this.renderText()}
            {this.renderSticker()}
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <Text style={{fontSize: 9, color:color.TEXT_LIGHT}}>{date}</Text>
            <Text style={{fontSize: 9, color:color.TEXT_LIGHT}}>{time}</Text>
          </View>
        </View>
      </View>
    );
  }

  renderLink() {
    if (this.props.data.link != 'null') {
      let color = GLOBAL.COLOR_THEME[this.props.theme()];
      let linkObj = JSON.parse(this.props.data.link);
      let imageArray = JSON.stringify(linkObj.data).match(/(https?:\/\/.*\.(?:png|jpg))/i);
      let image = linkObj.data.ogImage ? linkObj.data.ogImage[0].url : imageArray ? imageArray[0]: null;
      let width = windowSize.width * 0.8,
          height = windowSize.height * 0.18,
          mB = windowSize.height * 0.02;
      return (
        <View style={{width:width, paddingTop:10, alignSelf:'center', justifyContent:'center', marginBottom:mB}}>
          <LinkPreview width={width} height={height} image={image} link={linkObj} color={color}/>
        </View>
      )
    } else {
      return <View/>;
    }
  }

  renderMedia() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var content = (this.props.data.media)
      ? <View style={{flex: 1, paddingTop: windowSize.height * 0.02, width: windowSize.width * .75, justifyContent: 'center', alignSelf: 'center'}}>
        <MediaAutoGrid
        key={99}
        navigator={this.props.navigator}
        color={color}
        images={this.props.data.media}
        width={windowSize.width * .75}
        />
        </View>
      : <View/>;
    return content;
  }

  renderProfilePicture() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let data = this.props.data;
    let media = getMediaURL(data.media[0]);
    let width = 100;
    return (
      <View style={{ marginTop: 20, marginBottom: 15, alignItems: 'center'}}>
        <ImageWithLoader image={media} style={{width:width, height:width, borderRadius:50}} color={color}/>
        <Text style={{marginTop: 15}}>{data.userName + "'s Profile"}</Text>
      </View>
    );
  }

   renderSticker() {
    if (!this.props.data.sticker) return;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let sticker = this.props.data.sticker;
    let height = 100;
    let link = `${ddp.mediaServerUrl}${sticker}`;

    return (
      <View style={{flex:1, width:windowSize.width*0.9, justifyContent:'center', alignSelf:'center', paddingTop:5}}>
        <ImageWithLoader style={{height:height, width:height, borderRadius:height/2}} color={color} image={link} sticker={true} />
      </View>
    );
  }

  renderText() {
    if (!this.props.data.text) return;
    let text = this.props.data.text;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let fontFamily = Platform.OS === 'ios' ? 'Helvetica' : 'Roboto';
    return (
        <Text style={{fontSize:12,paddingTop:2,color: color.TEXT_LIGHT,fontFamily:fontFamily}}>{text}</Text>
    );
  }

  // Reaction and Comment Section
  renderFooter() {
    let data = this.props.data;
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let reactionsCount;
    let commentsCount;
    if (this.props.isOnComment) {
      commentsCount = (this.props.comments().length > 0) ? this.props.comments().length : ' ';
      reactionsCount = (this.props.reactions().length > 0) ? this.props.reactions().length : ' '
    } else {
      reactionsCount = (data.reactions && data.reactions.length > 0) ? data.reactions.length : ' ';
      commentsCount = (data.comments && data.comments.length > 0) ? data.comments.length : ' ';
    }

    return (
      <View style={{flexDirection:'row', justifyContent:'flex-end', paddingTop:20}}>
        <View style={{flexDirection:'row', alignItems: 'center'}} >
          <KlikFonts name='emoji' style={{ color:color.TEXT_LIGHT }} size={25} onPress={() => this.emojiClick()}/>
          <Text style={{ color: color.TEXT_LIGHT, fontSize: 12, paddingLeft: 1 }}>{reactionsCount}</Text>
        </View>
        <View style={{flexDirection:'row', alignItems: 'center', paddingLeft: 5}}>
          <KlikFonts name='comment' style={{ color:color.TEXT_LIGHT }} size={25} onPress={() => this.commentOnPost(data)} />
          <Text style={{ color:color.TEXT_LIGHT, fontSize: 12, paddingLeft: 1 }}>{commentsCount}</Text>
        </View>
      </View>
    );
  }

  commentOnPost(post) {
    SoundEffects.playSound('tick');
    requestAnimationFrame(() => {
      this.props.commentOnPost(post);
    });
  }

  emojiClick() {
    SoundEffects.playSound('tick');
    let postId = this.props.data.id;
    this.refs[postId].measure((ox, oy, width, height, px, py) => {
      let postInfo = {id:postId,height:height,y:py};
      this.props.setIsShowReactions(postInfo);
    });
  }

  logTLPLayoutMeasure(ox, oy, width, height, px, py) {
    console.log("ox: " + ox);
    console.log("oy: " + oy);
    console.log("width: " + width);
    console.log("height: " + height);
    console.log("px: " + px);
    console.log("py: " + py);
  }
}

module.exports = TimelinePost;
