'use strict';
import React, {
  Component
} from 'react';
import ReactNative, {
  Alert,
  AsyncStorage,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Animated,
  Easing,
  UIManager,
  LayoutAnimation
} from 'react-native';
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

import _ from 'underscore';
import {logTime, toNameCase} from './Helpers';
import Chat from './Chat';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Message from './config/db/Message';
import NavigationBar from 'react-native-navbar';
import Send from './config/db/Send';
import TypingArea from './TypingArea';
import TypingAreaOptions from './TypingAreaOptions';
import TypingAreaContacts from './TypingAreaContacts';
import moment from 'moment';
const styles = StyleSheet.create(require('./../styles/styles.main'));
const GLOBAL = require('./config/Globals.js');
const ADD_TO_LIMIT = 10;
const NAVBAR_HEIGHT = 50;
const SCREEN_SIZE = Dimensions.get('window');

var toolbarActions = {
  Group: [{title: 'Edit Group', iconName: 'options1', show: 'never'},
          {title: 'Chat Wallpaper', iconName: 'settings', show: 'never'}],
  Direct: [{title: 'Chat Wallpaper', iconName: 'settings', show: 'always'}]
};

const createAnimation = function (value, duration, easing, delay = 0) {
  return Animated.timing(
    value,
    {
      toValue: 1,
      duration,
      easing,
      delay
    }
  )
}

class MessagesChat extends Component {
  // Initialize
  constructor(props) {
    super(props);
    this.state = {
      connectionStatusHeight: 0,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isFirstLoad: true,
      isLoading: true,
      isViewVisible: true,
      limit: 20,
      messages: null,
      typeAreaHeight: 0,
      typeAreaOptions: 0,
      shouldReloadData: this.props.shouldReloadData(),
      imageGallery: [],
      active: '',
      activeOption: '',
      isOpenChatMenu: false,
      isEmojiPress: false,
      isOptionsPress: false,
      isRecordPress: false
    }
    this.renderChat = this.renderChat.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.onPressedBack = this.onPressedBack.bind(this);
    this.generateMessages = this.generateMessages.bind(this);
    //this.setActive = this.setActive.bind(this);
    this.activeOption = this.activeOption.bind(this);
    // this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.isEmojiPress = this.isEmojiPress.bind(this);
    this.setIsEmojiPress = this.setIsEmojiPress.bind(this);
    this.isOptionsPress = this.isOptionsPress.bind(this);
    this.setIsOptionsPress = this.setIsOptionsPress.bind(this);
    this.isRecordPress = this.isRecordPress.bind(this);
    this.setIsRecordPress = this.setIsRecordPress.bind(this);
    this.setIsOpenChatMenu = this.setIsOpenChatMenu.bind(this);
    this.animateChatMenu = new Animated.Value(0);
    this.animateTypingAreaOptions = new Animated.Value(0);
    this.onActionSelected = this.onActionSelected.bind(this);
    this.observeById = this.observeById.bind(this);
    this.acceptRequest = this.acceptRequest.bind(this);
    //this.loadMore = this.loadMore.bind(this);
    //this.onPressedBack = this.onPressedBack.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    if (this._isMounted) {
      if (this.props.group) {
        this.setState({toolbarActions: toolbarActions.Group})
      } else if (this.props.user) {
        this.setState({toolbarActions: toolbarActions.Direct})
      }
    }

    InteractionManager.runAfterInteractions(() => {
      Message.useCache((messages) => {
        var id = (this.props.group) ? this.props.group._id : this.props.user._id;
        this.setMessages(Message.getMessageById(this.props.loggedInUser()._id, id, messages));
        if (messages.length > 0) {
          this.setState({
            limit: messages.length
          });
        }
        this.subscribe();
      });
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.shouldReloadData() != this.state.shouldReloadData) {
      if (this._isMounted) {
        this.setState({
          shouldReloadData: this.props.shouldReloadData()
        })
      }
      if (this.props.shouldReloadData()) {
        console.log('MessagesChat ['+ logTime() +']: Reloading...');
        InteractionManager.runAfterInteractions(() => {
          this.subscribe();
        });
      }
    }

    if (prevState.limit != this.state.limit) {
      InteractionManager.runAfterInteractions(() => {
        this.subscribe();
      });
    }
  }

  subscribe() {
    console.log('MessagesChat ['+ logTime() +']: Loading messages with limit of ' + this.state.limit + '...');
    var id = (this.props.group) ? this.props.group._id : this.props.user._id;
    if (this.props.hasInternet()) {
      Message.subscribeByIdWithLimit(id, this.state.limit).then(() => {
        this.observeById();
      }).catch((err) => {
        console.log('error: ' + err);
      });
    } else {
      this.observeById();
    }
  }

  observeById() {
    var id = (this.props.group) ? this.props.group._id : this.props.user._id;
    Message.observeById(this.props.loggedInUser()._id, id, (messages) => {
      Message.checkTempAndDeleteSentFiles(messages, (newMessages, failedMsgs) => {
        if(JSON.stringify(newMessages) != JSON.stringify(this.state.messages)) {
          if (this.props.loggedInUser()) {
            if (this._isMounted) {
              if (this.props.hasInternet()) {
                Send.updateSeenMessages(newMessages, this.props.loggedInUser()._id);
                if (failedMsgs) {
                  for (let i = 0; i < failedMsgs.length; i++) {
                    Send.resend(failedMsgs[i]);
                  }
                }
              }
              this.setMessages(newMessages);
            }
          }
        }
      });
    });
  }

  setMessages(newMessages) {
    var imageGallery = []
    for (let i = 0; i < newMessages.length; i++) {
      let r = newMessages[i];
      if (_.has(r.Message, "Media")) {
        imageGallery.push(r);
      }
    }

    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(this.generateMessages(newMessages)),
        isLoading: false,
        messages: newMessages,
        imageGallery: imageGallery,
      });
    }
  }

  generateMessages(result) {
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: '2-digit'
    };
    var createDates = [];
    _.each(_.values(result), function(r) {
      var date = new Date(r.CreateDate);
      var formattedDate = moment(date).format('LL');
      r['FormattedDate'] = formattedDate;
      createDates.push(formattedDate);
    });
    createDates = _.uniq(createDates);
    var messages = [];
    _.each(_.values(createDates), function(date) {
      _.each(_.values(result), function(r) {
        if (r.FormattedDate == date) {
          messages.push(r);
        }
      });
      messages.push({date: date});
    });
    return messages;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var name = '';
    if (this.props.group) { // Group name
      name = toNameCase(this.props.group.Name);
    } else {
      name = toNameCase(this.props.user.profile.firstName) + ' ' + toNameCase(this.props.user.profile.lastName);
    }
    return (
      <View style={[styles.msgContainer, {backgroundColor: color.CHAT_BG}]}>
        <KlikFonts.ToolbarAndroid
          elevation={5}
          title={name}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
          actions={this.state.toolbarActions}
          onActionSelected={this.onActionSelected}
        />
        <View onLayout={(event) => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          this.setState({connectionStatusHeight: event.nativeEvent.layout.height});
        }}>
          {this.props.renderConnectionStatus()}
        </View>
        <Image style={{width: null, height: null, flex: 1, flexDirection: 'column'}}
          resizeMode={'cover'}
          source={this.renderBackgroundImage()}>
          {this.renderChat()}
          {this.renderBottomView()}
          {this.renderTypingAreaOptions()}
        </Image>
      </View>
    );
  }

  renderBottomView() {
    if (this.props.user) {
      let isContact = _.contains(this.props.loggedInUser().contacts, this.props.user._id);
      let friendReceivedIds = _.pluck(this.props.loggedInUser().friendRequestReceived, "userId");
      let hasFriendRequest = _.contains(friendReceivedIds, this.props.user._id);
      if (!isContact && hasFriendRequest) {
        return this.renderFriendRequest();
      }
    }
    return this.renderTypingArea();
  }

  renderBackgroundImage() {
    var bgRetriever = this.props.backgroundImageSrc;
    var bgImage = bgRetriever();
      if (bgImage == null || bgImage == '') {
        if (this.props.theme() == "ORIGINAL") {
          return require('./../images/chatbgdefault.jpg');
        } else if (this.props.theme() == "BLUE") {
          return require('./../images/backgroundImage/chatbg1.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "LAVENDER") {
          return require('./../images/backgroundImage/themebg2b.jpg');
        } else if (this.props.theme() == "GREEN") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else if (this.props.theme() == "ORIGINAL") {
          return require('./../images/backgroundImage/themebg3b.jpg');
        } else {
          return null;
        }
      } else {
        return {uri: bgImage};
      }
  }

  renderChat() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var chatHeight = SCREEN_SIZE.height - this.state.typeAreaHeight - NAVBAR_HEIGHT - (this.state.connectionStatusHeight) - 25;
    if (chatHeight < 165) {
      chatHeight = 165;
    }

    return (
      <Chat height={chatHeight}
        navigator={this.props.navigator}
        loggedInUser={this.props.loggedInUser}
        users={[this.props.loggedInUser(), this.props.user]}
        chatType={GLOBAL.CHAT.DIRECT}
        color={color}
        dataSource={this.state.dataSource}
        isLoading={this.state.isLoading}
        isAllLoaded={this.state.isAllLoaded}
        loadMore={this.loadMore}
        hasInternet={this.props.hasInternet}
        theme={this.props.theme}
        imageGallery={this.state.imageGallery}
        backgroundImageSrc={this.props.backgroundImageSrc}
        setIsSafeToBack={this.props.setIsSafeToBack}
        timeFormat={this.props.timeFormat}
        renderConnectionStatus={this.props.renderConnectionStatus}
        shouldReloadData={this.props.shouldReloadData}
        acceptRequest={this.acceptRequest}
        changeTheme={this.props.changeTheme}
        getMessageWallpaper={this.props.getMessageWallpaper}
      />
    );
  }

  loadMore() {
    if (!this.state.isAllLoaded) {
      if (!this.state.isLoading) {
        if (this._isMounted) {
          this.setState({
            isLoading: true
          });
        }
        InteractionManager.runAfterInteractions(() => {
          console.log('MessagesChat ['+ logTime() +']: Checking if there are more messages to load...');
          var id = (this.props.group) ? this.props.group._id : this.props.user._id;
          ddp.call('shouldLoadMessageChat', [id, this.state.limit])
          .then(shouldLoad => {
            var limit = this.state.limit + ADD_TO_LIMIT;
            if (this._isMounted) {
              this.setState({
                limit: limit,
              });
            }
          })
          .catch(error => {
            if (error.reason == 'All are loaded') {
              console.log('MessagesChat ['+ logTime() +']: ' + error.reason);
              if (this._isMounted) {
                this.setState({
                  isLoading: false,
                  isAllLoaded: true
                });
              }
            } else {
              console.log(error);
              if (this._isMounted) {
                this.setState({
                  isLoading: false
                });
              }
            }
          });
        });
      }
    }
  }

  renderFriendRequest() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View  elevation={10} onLayout={(event) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <View style={{backgroundColor: color.CHAT_BG, flex: 1, flexDirection: 'row', height: 40}}>
          <TouchableHighlight style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.props.navigator.pop()}>
            <Text style={{color: color.THEME}}>IGNORE</Text>
          </TouchableHighlight>
          <TouchableHighlight style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            underlayColor={color.HIGHLIGHT}
            onPress={() => this.acceptRequest(this.props.user._id)}>
            <Text style={{color: color.THEME}}>ACCEPT</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  renderTypingArea() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let recipientId = (this.props.group) ? this.props.group._id : this.props.user._id;
    return (
      <View onLayout={(event) => {
        if (this.state.isRecordPress) {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }
        this.setState({typeAreaHeight: event.nativeEvent.layout.height});
      }}>
        <TypingArea navigator={navigator}
          color={color}
          active={this.state.activeOption}
          isViewVisible={this.state.isViewVisible}
          loggedInUser={this.props.loggedInUser}
          recipientId={recipientId}
          chatType={GLOBAL.CHAT.DIRECT}
          setIsSafeToBack={this.props.setIsSafeToBack}
          setIsEmojiPress={this.setIsEmojiPress}
          isEmojiPress={this.isEmojiPress}
          setIsOptionsPress={this.setIsOptionsPress}
          isOptionsPress={this.isOptionsPress}
          isRecordPress={this.isRecordPress}
          setIsRecordPress={this.setIsRecordPress}
          hasInternet={this.props.hasInternet}
        //  setTypingAreaCallBack={this.props.setTypingAreaCallBack}
        //  isSafeToBack={this.props.isSafeToBack}
        //  setActive={this.setActive}
        />
      </View>
    )
  }

  renderTypingAreaOptions() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let chatHeight = SCREEN_SIZE.height - NAVBAR_HEIGHT - (this.state.connectionStatusHeight) - 25;
    let recipientId = (this.props.group) ? this.props.group._id : this.props.user._id;
    let top = (this.state.isOptionsPress || this.state.isEmojiPress) ? 0 : -chatHeight;
    return (
      <View style={{top: top, left: 0, right: 0, position: 'absolute', alignSelf: 'flex-start'}}>
        <View onLayout={(event) => {
          this.setState({typeAreaOptions: event.nativeEvent.layout.height});
        }}>
          <TypingAreaOptions color={color}
            height={chatHeight}
            navigator={this.props.navigator}
            loggedInUser={this.props.loggedInUser}
            recipientId={recipientId}
            chatType={GLOBAL.CHAT.DIRECT}
            setIsSafeToBack={this.props.setIsSafeToBack}
            setIsRecordPress={this.setIsRecordPress}
            setIsEmojiPress={this.setIsEmojiPress}
            setIsOptionsPress={this.setIsOptionsPress}
            isEmojiPress={this.isEmojiPress}
            isOptionsPress={this.isOptionsPress}
            // active={this.state.activeOption}
            // setTypingAreaCallBack={this.props.setTypingAreaCallBack}
            setActive={this.setActive}
            theme={this.props.theme}
            hasInternet={this.props.hasInternet}
          />
        </View>
      </View>
    );
  }

  onActionSelected(position) {
    if (this.props.group) {
      if (position == 0) {
        this.goToGroupDetails();
      } else {
        this.goToChatWallpaper();
      }
    } else if (this.props.user) {
      if (position == 0) {
        this.goToChatWallpaper();
      }
    }
  }

  goToChatWallpaper() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name: 'ChangeWallpaper',
        theme: this.props.theme,
        getMessageWallpaper: this.props.getMessageWallpaper
      });
    });
  }

  goToGroupDetails() {
    requestAnimationFrame(() => {
      this.props.navigator.push({
        name: 'GroupDetails',
        groupUsers: this.props.users,
        theme: this.props.theme,
        group: this.props.group,
        loggedInUser: this.props.loggedInUser,
        shouldReloadData: this.props.shouldReloadData,
        renderConnectionStatus: this.props.renderConnectionStatus,
        hasInternet: this.props.hasInternet,
        refreshGroups: this.props.refreshGroups,
        timeFormat: this.props.timeFormat
      });
    });
  }

  acceptRequest(id) {
    ddp.call('acceptFriendRequest', [id])
    .then(res => {
      if (res) {
        this.props.setLoggedInUser(res);
      }
    })
  }

  animateOptions() {
    requestAnimationFrame(() => {
      this.animateTypingAreaOptions.setValue(0);
      Animated.parallel([createAnimation(this.animateTypingAreaOptions, 500, Easing.ease)]).start();
    });
  }

  setIsOpenChatMenu() {
    if (this._isMounted) {
      this.setState({
        isOpenChatMenu: !this.state.isOpenChatMenu,
        isClickedSetIsOpenChatMenu : true
      });
      this.animateMenu();
    }
  }

  setIsEmojiPress() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({
        isEmojiPress: !this.state.isEmojiPress,
        isClickedSetIsEmojiPress: true
      });
      // this.animateOptions();
      if (this.state.isOptionsPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        this.setState({isOptionsPress: !this.state.isOptionsPress});
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  isEmojiPress() {
    return this.state.isEmojiPress;
  }

  setIsOptionsPress() {
    if (this._isMounted) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({
        isOptionsPress: !this.state.isOptionsPress,
        isClickedSetIsOptionsPress: true,
      });
      // this.animateOptions();
      if (this.state.isEmojiPress) {
        this.props.setIsSafeToBack(this.props.isSafeToBack());
        this.setState({isEmojiPress: !this.state.isEmojiPress});
      } else {
        this.props.setIsSafeToBack(!this.props.isSafeToBack());
      }
    }
  }

  isOptionsPress() {
    return this.state.isOptionsPress;
  }

  setIsRecordPress(clickedFrom) {
    if (this._isMounted) {
      this.setState({isRecordPress: !this.state.isRecordPress});
      if (clickedFrom == 'typingAreaOptions') {
        this.setState({isOptionsPress: !this.state.isOptionsPress});
      }
    }
  }

  isRecordPress() {
    return this.state.isRecordPress;
  }

  activeOption() {
    return this.state.activeOption;
  }

  onPressedBack() {
    if (this._isMounted) {
      this.setState({
        isViewVisible: false
      });
    }
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }
}

module.exports = MessagesChat;
