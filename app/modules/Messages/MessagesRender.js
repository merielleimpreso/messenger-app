import React, {
  Component,
} from 'react';
import {
  ListView,
  Platform,
  View
} from 'react-native';
import _ from 'underscore';
import {getChatBadge, getMediaURL, dynamicDateToTime, renderFooter} from '../Helpers';
import ButtonIcon from '../common/ButtonIcon';
import ContentWrapper from '../common/ContentWrapper';
import GLOBAL from '../config/Globals.js';
import ListRow from '../common/ListRow';
import NavigationBar from 'react-native-navbar';
import RenderModal from '../common/RenderModal';
import styles from './styles';
import Toolbar from '../common/Toolbar';

class MessagesRender extends Component {
  constructor(props) {
    super(props);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let rightButton = this.renderRightButton();
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        {(Platform.OS == 'ios') ? 
          <View>
            <NavigationBar tintColor={color.THEME}
              title={{ title:'Messages', tintColor:color.BUTTON_TEXT}}
              rightButton={rightButton}
            />
            {this.props.renderConnectionStatus()}
          </View> : null}
        {this.renderContent()}
        {this.renderModal()}
      </View>
    );
  }

  renderRightButton() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ContentWrapper color={color} style={{flexDirection: 'row', backgroundColor: color.THEME}}>
        <ButtonIcon icon={'addgroup'} color={color} onPress={() => this.props.goToCreateGroup()} size={25}/>
        <ButtonIcon icon={'writenew'} color={color} onPress={() => this.props.goToAddMessage()} size={25}/>
      </ContentWrapper>
    );
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <ListView ref='listview'
        automaticallyAdjustContentInsets={false}
        contentInset={{bottom:50}}
        dataSource={this.props.dataSource}
        enableEmptySections={true}
        initialListSize={10}
        removeClippedSubviews={false}
        renderFooter={this.renderFooter}
        renderRow={this.renderRow}
      />
    );
  }

  renderModal() {
    if (this.props.isShowMenu) {
      let actions = this.props.toolbarActions;
        actions[0].action = this.props.goToAddMessage;
        actions[1].action = this.props.goToCreateGroup;
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.props.onPressRightButton}
          actions={actions}
          modalType={'menu'}
        />
      );
    }
  }

  renderFooter() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let notFoundText = (this.props.dataSource.getRowCount(0) == 0) ? 'No message yet.' : null;
    return renderFooter(this.props.isAllLoaded, color, this.props.hasInternet(), notFoundText);
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let image = data.user.image ? data.user.image 
                  : (data.user._id.includes('group')) ? GLOBAL.DEFAULT_GROUP_IMAGE 
                    : GLOBAL.DEFAULT_PROFILE_IMAGE;
    let badge = getChatBadge(data.isFromLoggedInUser, data.notSeenCount, data.message.seen, color)
    return (
      <ListRow onPress={() => this.props.onPressedRow(data)}
        labelText={data.user.name}
        labelInfoText={dynamicDateToTime(data.message.date, this.props.timeFormat())}
        detailText={data.message.description}
        image={getMediaURL(image)}
        badge={badge}
        color={color}
        addSeparator={true}
      />
    );
  }
}

// Export module.
module.exports = MessagesRender;
