'use strict';

import _ from 'underscore';
import React, {
  Component,
} from 'react';
import ReactNative, {
  InteractionManager,
  ListView,
} from 'react-native';
import Chat from '../config/db/Chat';
import MessagesRender from './MessagesRender';
import StoreCache from '../config/db/StoreCache';

var hasJustUpdated = false;//stop user from pressing thread too fast after reconxn
var hasInternet = false;
class Messages extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chat: null,
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2,}),
      isAllLoaded: false,
      isCacheLoaded: false,
      isLoggedIn: this.props.isLoggedIn(),
      isShowMenu: false,
      shouldResubscribe: false,
      loggedInUser: null
    }
    this.getUnseenCount = this.getUnseenCount.bind(this);
    this.observeChat = this.observeChat.bind(this);
    this.onPressedRow = this.onPressedRow.bind(this);
    this.onPressRightButton = this.onPressRightButton.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.setShouldResubscribe = this.setShouldResubscribe.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      hasInternet = this.props.hasInternet();
      StoreCache.getCache(StoreCache.keys.recent, (chat) => {
        if (!chat) {
          chat = [];
        }
        this.getUnseenCount(chat);
        console.log('Messages.js: Loaded ' + chat.length + ' recent/s from cache');
        if (this._isMounted) {
          this.setState({
            chat: chat,
            dataSource: this.state.dataSource.cloneWithRows(chat),
            isCacheLoaded: true
          });
        }
        this.subscribe();
      });
    });
  }

  componentDidUpdate(prevProps, prevState) {


    if (JSON.stringify(this.state.loggedInUser) != JSON.stringify(this.props.loggedInUser())) {
      this.subscribe();
      if (this._isMounted) {
        this.subscribe();

        this.setState({
          loggedInUser: this.props.loggedInUser()
        });
      }
    }


    if(this.props.hasInternet() != hasInternet) {
      hasInternet = this.props.hasInternet();
      hasJustUpdated = true;

      setTimeout(()=> {
        hasJustUpdated = false;
      }, 2000);
    }
    if (this.state.isLoggedIn != this.props.isLoggedIn()) {
      if (this._isMounted) {
        this.setState({
          isLoggedIn: this.props.isLoggedIn()
        });
      }
      if (this.props.isLoggedIn()) {
        console.log('Messages.js: Re-subscribing...');
        this.subscribe();
      } else {
        console.log('Messages.js: Stop chat observing');
        Chat.stopObserving();
      }
    }

    if (prevState.shouldResubscribe != this.state.shouldResubscribe) {
      if (this.state.shouldResubscribe) {
        if (this._isMounted) {
          this.setState({
            shouldResubscribe: false
          });
        }
        console.log('Messages.js: Should re-subscribe...');
        this.subscribe();
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    if (this.state.isShowMenu) {
      this.onPressRightButton();
    }
  }

  subscribe() {
    if (this.props.isLoggedIn()) {
      console.log('Messages: subscribe');
      Chat.subscribe().then(() => {
        this.observeChat();
      });
    }
  }

  observeChat() {
    requestAnimationFrame(() => {
      Chat.observeRecent(this.props.loggedInUser(), (chat) => {
        if (JSON.stringify(chat) != JSON.stringify(this.state.chat)) {
        this.getUnseenCount(chat);
          StoreCache.save(StoreCache.keys.recent);
          console.log('Messages.js: Loaded ' + chat.length + ' recent/s from observer');
          if (this._isMounted) {
            this.setState({
              chat: chat,
              dataSource: this.state.dataSource.cloneWithRows(chat),
              isAllLoaded: true,
            })
          }
        } else {
          if (this._isMounted) {
            if (this.state.isCacheLoaded) {
              this.setState({
                isAllLoaded: true
              });
            }
          }
        }
        if (this.state.refreshing) {
          if (this._isMounted) {
            this.setState({refreshing: false});
          }
        }
      });
    });
  }

  getUnseenCount(chat) {
    let count = 0;
    for (let i = 0; i < chat.length; i++) {
      let unseen = chat[i].notSeenCount;
      count = count + unseen;
    }
    this.props.setUnseen(count);
  }

  render() {
    return (
      <MessagesRender
        dataSource={this.state.dataSource}
        isAllLoaded={this.state.isAllLoaded}
        isShowMenu={this.state.isShowMenu}
        onPressRightButton={this.onPressRightButton.bind(this)}
        onPressedRow={this.onPressedRow}
        {...this.props}
      />
    )
  }

  onPressRightButton() {
    if (this._isMounted) {
      this.setState({isShowMenu: !this.state.isShowMenu});
    }
  }

  onPressedRow(data) {
    requestAnimationFrame(() => {

      if(!hasJustUpdated) {
        this.props.navigator.push({
          id:'messagesgiftedchat',
          name:'MessagesGiftedChat',
          addContactToLoggedInUser: this.props.addContactToLoggedInUser,
          audio: this.props.audio,
          backgroundImageSrc: this.props.backgroundImageSrc,
          changeBgImage: this.props.changeBgImage,
          changeTheme: this.props.changeTheme,
          getMessageWallpaper: this.props.getMessageWallpaper,
          hasInternet: this.props.hasInternet,
          isAutoSaveMedia: this.props.isAutoSaveMedia,
          isLoggedIn: this.props.isLoggedIn,
          isSafeToBack: this.props.isSafeToBack,
          loggedInUser: this.props.loggedInUser,
          logout: this.props.logout,
          renderConnectionStatus: this.props.renderConnectionStatus,
          setAudio: this.props.setAudio,
          setLoggedInUser: this.props.setLoggedInUser,
          setIsViewVisible: this.props.setIsViewVisible,
          setIsSafeToBack: this.props.setIsSafeToBack,
          setShouldResubscribe: this.setShouldResubscribe,
          shouldReloadData: this.props.shouldReloadData,
          subscribeToLoggedInUser: this.props.subscribeToLoggedInUser,
          theme: this.props.theme,
          timeFormat: this.props.timeFormat,
          user: data.user
        });
      }
      else {
        console.log("Press rejected. Just updated");
      }

    });
  }

  setShouldResubscribe(shouldResubscribe) {
    if (this._isMounted) {
      console.log('set');
      this.setState({
        shouldResubscribe: shouldResubscribe
      });
    }
  }
}

// Export module.
module.exports = Messages;
