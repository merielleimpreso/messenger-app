'use strict';

import _ from 'underscore';
import file from './config/data.json';
import AudioRecorder from './AudioRecording';
import Stickers from './InputStickers';
import TypingAreaContacts from './TypingAreaContacts';
import Download from './config/db/Download';
import DirectoryExplorer from './DirectoryExplorer';
import FileReader from 'react-native-fs';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import {launchCamera, launchImageLibrary} from './Helpers';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import Modal from 'react-native-simple-modal';
import React, { Component } from 'react';
import Send from './config/db/Send';
import Navigation from './actions/Navigation';
import SystemAccessor from './SystemAccessor';
import  MovToMp4 from 'react-native-mov-to-mp4';

import {
  BackAndroid,
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableNativeFeedback,
  Text,
  TextInput,
  Image,
  Dimensions,
  InteractionManager,
  TouchableWithoutFeedback,
  Platform
} from 'react-native';
var GLOBAL = require('./../modules/config/Globals.js');
var ImagePickerManager = require('NativeModules').ImagePickerManager;

var CryptoJS = require("crypto-js");
var styles = StyleSheet.create(require('./../styles/styles.main'));
var windowSize = Dimensions.get('window');
var watchID;
var RNFS = require('react-native-fs');

class TypingAreaOptions extends Component {
  constructor(props) {
    super(props);
    this.backAndroidHandler = this.backAndroidHandler.bind(this);
    this.sendAttachment = this.sendAttachment.bind(this);
    this.sendAudio = this.sendAudio.bind(this);
    this.onLocationPress = this.onLocationPress.bind(this);
    this.sendLocation = this.sendLocation.bind(this);
    this.sendContact = this.sendContact.bind(this);
    this.onAudioPress = this.onAudioPress.bind(this);
    this.convertVideo = this.convertVideo.bind(this);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
    BackAndroid.removeEventListener('hardwareBackPress', this.backAndroidHandler);
  }

  backAndroidHandler() {
    if ((this.props.active() == null) || this.props.active() == 'keyboard') {
      // this.props.navigator.pop();
      if(this._isMounted){
        this.props.setIsSafeToBack(true);
      }

    } else {
      let navigatorStack = this.props.navigator.getCurrentRoutes();
      let currentRoute = this.props.navigator.getCurrentRoutes().length - 1;

      if (!_.contains(['PhotoInfo', 'AddUserToGroup'], navigatorStack[currentRoute].name)) {
        this.props.setActive(null);
      }
    }
  }

  // Called when camera button is pressed.
  onCameraPress() {
    this.props.setActive(null);
    this.launchCamera('photo');
  }

  onPressContact() {
    if(!this.props.checkIfPartOfGroup()){
      this.props.setActive(null);
      return this.props.setIsShowNotParty();
    }

    this.props.setActive('contact');
    this.props.navigator.push({
      name: "FriendList",
      loggedInUser: this.props.loggedInUser,
      newMessage: null,
      theme: this.props.theme,
      sendContact: this.sendContact,
      action: 'sendContact'
    });
  }

  // Called when video button is pressed.
  onVideoPress() {
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    this.launchCamera('video');
  }

  onLocationPress() {
    if(!this.props.checkIfPartOfGroup()){
      this.props.setActive(null);
      return this.props.setIsShowNotParty();
    }
    this.props.setActive('location');
    this.props.navigator.push({
      name: "Map",
      sendLocation: this.sendLocation,
      from: this.props.from,
      to: this.props.to,
      color: this.props.color
    });
  }

  onAudioPress() {
    if(!this.props.checkIfPartOfGroup()){
      this.props.setActive(null);
      return this.props.setIsShowNotParty();
    }
    this.props.setActive('audio')
  }

  // Get audio recorder status
  // getAudioRecorderStatus(){
  //   if (this._isMounted) {
  //     this.setState({
  //       canEditTextInput: !this.state.canEditTextInput,
  //       shouldShowAudioRecorder: !this.state.shouldShowAudioRecorder,
  //       shouldShowSticker: false
  //     });
  //   }
  // }

  launchImageLibrary(media) {
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    launchImageLibrary(media, (response) => {



      if (typeof response == "string") {
        this.props.setIsShowFileTooBig();
      } else {

        console.log("Media: ", media);
        console.log("Response: ", response);
        if(media === "video"){
            this.convertVideo(media, response);
        }
        else {
          this.sendMedia(media, response);
        }

      }
    });
  }

  launchCamera(media) {
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    launchCamera(media, (response) => {
      // if (typeof response == "string") {
      //   this.props.setIsShowFileTooBig();
      // } else {
      //   this.sendMedia(media, response);
      // }

      if (typeof response == "string") {
        this.props.setIsShowFileTooBig();
      } else {

        console.log("Media: ", media);
        if(media === "video" && response.path.indexOf(".mp4") < 0){
            this.convertVideo(media, response);
        }
        else {
          this.sendMedia(media, response);
        }

      }
    });
  }

  sendMedia(mediaType, response) {
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    let mediaPath = (mediaType == 'video') ? Download.mediaTypePath.videoSent : Download.mediaTypePath.imageSent;
    let data = response;
    data['fromUserId'] = this.props.loggedInUser()._id;
    data['toUserId'] = this.props.recipientId;
    data['duration'] = 0;
    data['fileSize'] = (data.fileSize) ? data.fileSize : 0;
    Send.media(data, mediaType, Send.uploadURL.mediaChat, mediaPath, this.props.isAutoSaveMedia());
  }

  sendAudio(audio, duration) {
    console.log('duration', duration);
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    let data = {
      fromUserId: this.props.loggedInUser()._id,
      toUserId: this.props.recipientId,
      fileName: audio.replace(/^.*[\\\/]/, ''),
      duration: duration,
      fileSize: 0,
      uri: audio,
    };
    Send.media(data, 'audio', Send.uploadURL.mediaChat, Download.mediaTypePath.audioSent, this.props.isAutoSaveMedia());
  }

  sendAttachment(mediaDetails) {
    // this.send(mediaDetails, "attachment");
    let sender = this.props.loggedInUser()._id;
    let recipient = this.props.recipientId;
    let chatType = this.props.chatType;
    let type = 'attachment';
    let duration = 0;
    let uri = "file://"+mediaDetails.path;
    Send.media(sender, recipient, chatType, uri, type, duration, !this.props.hasInternet());
  }

  sendLocation(location) {
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    let fromUserId = this.props.loggedInUser()._id;
    let toUserId = this.props.recipientId;
    let message = {
      fromUserId: fromUserId,
      toUserId: toUserId,
      message: {location: location},
      status: 'Sending'
    }
    requestAnimationFrame(() => {
      Send.newMessage(message);
    });
    // Send.currentLocation(toUserId, location, !this.props.hasInternet());
  }

  sendSticker(data){
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    let sticker = data.Image;
    let fromUserId = this.props.loggedInUser()._id;
    let toUserId = this.props.recipientId;
    let message = {
      fromUserId: fromUserId,
      toUserId: toUserId,
      message: {sticker: sticker},
      status: 'Sending'
    }
    requestAnimationFrame(() => {
      Send.newMessage(message);
    });
    // Send.sticker(toUserId, sticker, !this.props.hasInternet());
  }

  sendContact(contact) {
    this.props.setActive(null);
    if(!this.props.checkIfPartOfGroup()){
      return this.props.setIsShowNotParty();
    }
    InteractionManager.runAfterInteractions(() => {
      let fromUserId = this.props.loggedInUser()._id;
      let toUserId = this.props.recipientId;
      let message = {
        fromUserId: fromUserId,
        toUserId: toUserId,
        message: {contactDetails: contact},
        status: 'Sending'
      }
      requestAnimationFrame(() => {
        Send.newMessage(message);
      });
      // Send.contact(toUserId, contact, !this.props.hasInternet());
    });
  }

  convertVideo(media, response) {


    if(Platform.OS == 'android') {

          console.log("android video detected");

          if(response.path.indexOf(".mp4") < 0) {

              FileReader.exists('/storage/emulated/0' + Download.mediaTypePath.video).then((exists)=>{

                  if(exists) {
                        var path = response.path;
                        var fileName = path.replace(/^.*[\\\/]/, '');
                        fileName = fileName.substring(0, fileName.indexOf("."));

                        var outputPath = Download.mediaTypePath.video + fileName + ".mp4";

                        outputPath = '/storage/emulated/0' + outputPath;

                        // this.props.setisShowConverting();
                        SystemAccessor.convertVideo(path, outputPath, (err, conversionResponse)=>{
                          console.log("conversionResponse: ", conversionResponse);
                            if(conversionResponse) {
                              this.sendMedia(media, conversionResponse);
                            }
                        });
                  } else {
                    //console.log("Creating /Videos directory");
                    FileReader.mkdir('/storage/emulated/0' + Download.mediaTypePath.video).then(()=>{
                    //console.log("Created /Videos directory");
                    var path = response.path;
                    var fileName = path.replace(/^.*[\\\/]/, '');
                    fileName = fileName.substring(0, fileName.indexOf("."));

                    var outputPath = Download.mediaTypePath.video + fileName + ".mp4";

                    outputPath = '/storage/emulated/0' + outputPath;

                    SystemAccessor.convertVideo(path, outputPath, (err, conversionResponse)=>{
                      //console.log("conversionResponse: ", conversionResponse);

                          if(conversionResponse != null) {
                            this.sendMedia(media, conversionResponse);
                          }

                    });

                    });
                  }
              });


          } else {
                this.sendMedia(media, response);
          }


    } else {
      console.log("iOS video detected.");

      var path = response.uri;
      var fileName = response.fileName;
      //react-native-mov-to-mp4 apparently goes crazy if path has file:// at the start.
      path = path.substring(path.indexOf("file://")+6, path.length);
      fileName = "/Videos/"+fileName.substring(0, fileName.indexOf(".")) + ".mp4";

      console.log("filename: ", fileName);

      var outputPath = fileName;
      //outputPath = FileReader.DocumentDirectoryPath + outputPath;
      console.log("outputPath: ",outputPath);

      if(path.indexOf(".mp4") < 0) {//Video file is not mp4
        console.log("Video file is not mp4");

            FileReader.exists(FileReader.DocumentDirectoryPath + "/Videos").then((exists)=>{
            console.log("DocumentDirectoryPath: ", FileReader.DocumentDirectoryPath + "/Videos");
            console.log("DocumentDirectoryPath exists: ", exists);


                if(exists) { // /Videos already exist, skip directory creation

                  //check if mp4 version of file already exists in /Videos
                  FileReader.exists(FileReader.DocumentDirectoryPath + outputPath).then((fileExists)=>{

                        if(fileExists) {//MP4 of file exists already. Skip conversion
                              console.log("MP4 of file already exists");
                              outputPath = "file://"+FileReader.DocumentDirectoryPath + outputPath;
                              var savedResponse = {
                                uri: outputPath,
                                path: outputPath.substring(outputPath.indexOf("file://")+7, outputPath.length)
                              };
                              console.log("savedResponse: ", savedResponse);
                              this.sendMedia(media, savedResponse);

                        } else {//MP4 does not exist yet.
                              console.log("MP4 of file does not exist, converting...");
                              MovToMp4.convertMovToMp4(path, outputPath,  (results)=> {
                                     console.log("movToMP4", results);

                                     if(results !== "Failure" && results !== "Cancelled") {
                                       var cPath = results.substring(results.indexOf("file://")+7, results.length);
                                       var conversionResponse ={
                                         uri: results,
                                         path: cPath
                                       };

                                       console.log("conversionResponse: ", conversionResponse);
                                       this.sendMedia(media, conversionResponse);

                                     }
                                     else {
                                       console.log("Conversion Failure: ", results);
                                     }
                              });
                        }

                  });




                } else {
                  console.log("Creating /Videos directory");
                  FileReader.mkdir(FileReader.DocumentDirectoryPath + "/Videos").then(()=>{
                  console.log("Created /Videos directory");

                  MovToMp4.convertMovToMp4(path, outputPath,  (results)=> {

                         console.log("movToMP4", results);

                         if(results !== "Failure" && results !== "Cancelled") {
                           var cPath = results.substring(results.indexOf("file://")+7, results.length);
                           var conversionResponse ={
                             uri: results,
                             path: cPath
                           };

                           console.log("conversionResponse: ", conversionResponse);
                           this.sendMedia(media, conversionResponse);

                         }
                         else {
                           console.log("Conversion Failure: ", results);
                         }
                  });

                  })
                  .catch((err)=>{
                    console.log("Failed to create /Videos: ", err);
                  });
                }
            });

      } else {
          console.log("Video file already mp4. Skipping conversion");
          var libResponse = {
            uri: response.uri,
            path: response.uri.substring(response.uri.indexOf("file://")+7, response.uri.length)
          };

          this.sendMedia(media, libResponse)
      }







    }
  }
  render() {
    let color = this.props.color;
    let height = this.props.height - (windowSize.height * .4);

    return (
      <View style={styles.typingArea}>
        <View style={{height: this.props.height}}>
          {this.renderContent()}
          <TouchableWithoutFeedback onPress={() => this.props.setActive()}>
            <View style={{height: height}} />
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }

  renderContent() {
    switch(this.props.active()) {
      case 'stickers':
        return (
          <Stickers
            chatType={this.props.chatType}
            height={windowSize.height * 0.4}
            from={this.props.from}
            to={this.props.to}
            color={this.props.color}
            onStickerPress={this.sendSticker.bind(this)}
            hasInternet={this.props.hasInternet}
          />
        );
      case 'audio':
        return (
          <AudioRecorder
            chatType={this.props.chatType}
            keyboardSpace={windowSize.height * 0.4}
            from={this.props.from}
            to={this.props.to}
            color={this.props.color}
            sendAudio={this.sendAudio}
          />
        );
      case 'options':
        return this.renderOptions();
    }
  }

  renderOptions() {
    let color = this.props.color;
    return (
      <View style={[styles.buttonsContainer, {backgroundColor: color.CHAT_BG}]}>
        <View style={{flexDirection: 'row'}}>

            <TouchableHighlight
              onPress={() => Navigation.onPress(() => this.onCameraPress())}
              underlayColor={color.HIGHLIGHT}
              style={styles.buttonWrap}>
              <View>
                <KlikFonts name="photo" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Take Photo</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight
              onPress={() => Navigation.onPress(() => this.launchImageLibrary('photo'))}
              underlayColor={color.HIGHLIGHT}
              style={[styles.buttonWrap]}>
              <View>
                <KlikFonts name="gallery" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Choose Photo</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight
              onPress={() => Navigation.onPress(() => this.launchImageLibrary('video'))}
              underlayColor={color.HIGHLIGHT}
              style={styles.buttonWrap}>
              <View>
                <KlikFonts name="play" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Choose Video</Text>
              </View>
            </TouchableHighlight>

        </View>
        <View style={{flexDirection: 'row'}}>

            <TouchableHighlight
              onPress={() => Navigation.onPress(() => this.onVideoPress('video'))}
              underlayColor={color.HIGHLIGHT}
              style={styles.buttonWrap}>
              <View>
                <KlikFonts name="video" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Take Video</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight
              onPress={() => Navigation.onPress(() => this.onPressContact())}
              underlayColor={color.HIGHLIGHT}
              style={[styles.buttonWrap]}>
              <View>
                <KlikFonts name="sendcontact" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Send Contact</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight
              onPress={() => Navigation.onPress(this.onLocationPress)}
              underlayColor={color.HIGHLIGHT}
              style={styles.buttonWrap}>
              <View>
                <KlikFonts name="location" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Send Location</Text>
              </View>
            </TouchableHighlight>

        </View>
        <View style={{flexDirection: 'row'}}>

            <View style={styles.buttonWrap}>
            </View>

            <TouchableHighlight
              onPress={() => Navigation.onPress(this.onAudioPress)}
              underlayColor={color.HIGHLIGHT}
              style={styles.buttonWrap}>
              <View>
                <KlikFonts name="audio" style={[styles.optionButtonIcon, {color: color.TEXT_LIGHT}]}/>
                <Text style={[styles.optionButtonText, {color: color.TEXT_LIGHT}]}>Voice Note</Text>
              </View>
            </TouchableHighlight>

        </View>
      </View>
    );
  }
}

module.exports = TypingAreaOptions;
