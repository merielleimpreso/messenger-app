/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-06
 * Date Updated: 2016-11-07
 * Description: Component used for Changing password
 */
'use strict';

import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TextInput,
  View,
 } from 'react-native';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const WINDOW_SIZE = Dimensions.get('window');

class ChangeDisplayName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser: this.props.loggedInUser(),
      oldDisplayName: '',
      name: '',
      isShowMessageModal: false,
      isLoading: false
    }
    this.changeDisplayName = this.changeDisplayName.bind(this);
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);

  }

  componentDidMount() {
    this._isMounted = true;
    let loggedInUser = this.state.loggedInUser;

    if (!loggedInUser.profile.hasOwnProperty('displayName')) {
      this.setState({
        name: `${toNameCase(loggedInUser.profile.firstName)} ${toNameCase(loggedInUser.profile.lastName)}`
      });
    } else {
      this.setState({
        name: loggedInUser.profile.displayName,
        oldDisplayName: loggedInUser.profile.displayName
      })
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        <NavigationBar tintColor={color.THEME}
          title={{ title: 'Display Name', tintColor: color.BUTTON_TEXT}}
          rightButton={<ButtonIcon icon='close' color={color} onPress={this.onPressButtonClose}/>}
        />
        <View style={[form.wrapper]}>
          <View style={[form.textInput, {borderColor: color.THEME}]}>
            <TextInput
              onChangeText={(name) => this.setState({name})}
              style={[form.text, {color: color.TEXT_DARK, marginTop: 10}]}
              value={this.state.name}
            />
          </View>
          <Text />
          <Button text="Save" color={color.THEME} isDefaultWidth={true} onPress={this.changeDisplayName} style={form.button} underlayColor={color.HIGHLIGHT} />
        </View>
        <Text style={[form.warning, {color: color.ERROR}]}>
          {this.state.warningMessage}
        </Text>
        {this.renderModal()}
      </View>
    )
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  clearText(fieldName) {
    this.refs[fieldName].setNativeProps({text:''});
  }

  changeDisplayName() {
    let displayName = this.state.name.trim().replace('/\s+/g',' ');
    let oldDisplayName = this.state.oldDisplayName;
    
    if (!displayName) {
      this.setState({warningMessage: 'Display Name must have 1 or more characters.'});
      return;
    }

    // status message does not change do nothing
    if (oldDisplayName == displayName) {
      this.setState({warningMessage: 'You have not made any changes.'});
      return
    }

    if (this._isMounted) {
      this.setState({isLoading: true});
    }
    // Update display name
    ddp.call('updateDisplayName', [this.state.name])
    .then((response) => {
      this.setIsShowMessageModal('Successfully changed display name.');
      if (this._isMounted) {
        this.props.setDisplayName(response);
        this.setState({
          oldDisplayName: response,
          warningMessage: null,
          isLoading: false
        });
      }
       //alert('Successfully changed display name');
    }).catch(function(e) {
      console.log('Something went wrong.');
      this.setState({warningMessage: e.reason})
    }.bind(this));
    
  }

  renderModal() {
    if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.state.isLoading) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => console.log('loading')}
         modalType={'loading'}
        />
      );
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }
}


let form = StyleSheet.create({
  wrapper: {
    margin: 20
  },
  warning: {
    textAlign: 'center',
    alignItems: 'stretch',
    fontSize: 12, 
    height: 20,
    marginTop: 8,
    marginBottom: 5,
  },
  textInput: {
    flex: 1,
    borderBottomWidth: 1,
    marginBottom: 2
  },
  text: {
    height: 40,
    fontSize: 14,
    textAlign: 'center',
  },
  button: {
    borderRadius: 5,
    backgroundColor: GLOBAL.COLOR_CONSTANT.BUTTON,
    alignItems: 'stretch',
    alignSelf:'center'
  }
});

module.exports = ChangeDisplayName;
