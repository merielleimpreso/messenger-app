'use strict';
import _ from 'underscore';
import React, {
  Component,
} from 'react';
import {
  View,
  Dimensions,
  TextInput,
  StyleSheet
} from 'react-native';

import KlikFonts from 'react-native-vector-icons/KlikFonts';
const styles = StyleSheet.create(require('./../styles/styles.main.js'));
const GLOBAL = require('./config/Globals.js');

class ChangeStatusMessage extends Component {

  render () {
    return (
      <View style={{flexDirection: 'row', marginTop: 15, marginBottom: 15, marginLeft: 10, marginRight: 10}}>
        <View style={[styles.searchBoxContainer, {backgroundColor: color.HIGHLIGHT}]}>
          <TextInput ref="changedStatus"
            value={this.state.changedStatus}
            onChangeText={(changedStatus) => this.setState({changedStatus: changedStatus})}
            style={[styles.input, {backgroundColor: 'transparent', textAlign: 'left'}]}
            placeholder='Status Message'
            placeholderTextColor={color.TEXT_INPUT_PLACEHOLDER}
            autoCapitalize='none'
            />
        </View>
        <TouchableHighlight style={[styles.inputLoginContainer, {backgroundColor: color.TEXT_LIGHT, width: 40, justifyContent: 'center'}]}
          underlayColor={color.HIGHLIGHT} onPress={() => this.changeStatus()} >
          <Text style={{alignSelf: 'center', color: color.BUTTON_TEXT}}>Save</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

module.exports = ChangeStatusMessage;