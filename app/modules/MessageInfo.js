import React, {
  Component,
} from 'react';
import {
  InteractionManager,
  ListView,
  Text,
  View
} from 'react-native';
import _ from 'underscore';
import {dateToTime, getDisplayName, getMediaURL} from './Helpers';
import ListRow from './common/ListRow';
import Toolbar from './common/Toolbar';
import Users from './config/db/Users';
import GLOBAL from './config/Globals.js';

class MessageInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      }),
    }
    this.getDataMap = this.getDataMap.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      if (this._isMounted) {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getDataMap())
        });
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getDataMap() {
    let categoryMap = [];
    let objects = [];

    console.log(this.props.message);
    let user = this.props.message.user;
    if (user) {
      objects.push({
        category: 'Sent by',
        image: user.image,
        name: user.name,
        time: dateToTime(this.props.message.createdAt, this.props.timeFormat())
      });
    }

    if (this.props.message.seen) {
      let seen = this.props.message.seen;
      for (var i = 0; i < seen.length; i++) {
        let user = _.find(this.props.otherUsers, {_id : seen[i].userId});
        if (user) {
          objects.push({
            category: 'Read by',
            image: getMediaURL(user.image),
            name: user.name,
            time: dateToTime(seen[i].date, this.props.timeFormat())
          });
        } else {
          console.log('User is null');
        }
      }

      objects.forEach(function(item) {
        if (!categoryMap[item.category]) {
          categoryMap[item.category] = [];
        }
        categoryMap[item.category].push(item);
      });
    }

    return categoryMap;
  }

  render() {
    let color = this.props.color;
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        <Toolbar title={'Message Info'} withBackButton={true} {...this.props}/>
          <ListView ref="listview"
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}
            renderSectionHeader={this.renderSectionHeader}
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps={true}
            showsVerticalScrollIndicator={false}
          />
      </View>
    );
  }

  renderRow(data) {
    return (
      <ListRow onPress={() => this.getProfileData(loggedInUser)}
        labelText={data.name}
        detailText={data.time}
        color={this.props.color}
        addSeparator={false}
        image={data.image}
      />
    );
  }

  renderSectionHeader(sectionData, category) {
    let color = this.props.color;
    return (
      <View style={[styles.sectionHeader, {borderTopColor:color.HIGHLIGHT, backgroundColor:color.CHAT_BG}]}>
        <Text style={[styles.sectionHeaderText, {color:color.TEXT_DARK}]}>{category}</Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  navigationbar: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  sectionHeader: {
    borderTopWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
  },
  sectionHeaderText: {
    fontSize:12,
    marginLeft:5,
    marginTop:3,
    marginBottom:3,
    flex: 1
  }
};

module.exports = MessageInfo;
