'use strict';
import _ from 'underscore';
import React, {
  Component,
} from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  Text,
  View,
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const GLOBAL = require('./config/Globals.js');

class ContactsNearby extends Component {

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Find Friends Nearby', tintColor: color.BUTTON_TEXT}}
         leftButton={
          <TouchableHighlight onPress={() => this.onPressButtonBack()} underlayColor='transparent' style={{alignSelf:'center'}}>
            <KlikFonts name='back' color={color.BUTTON_TEXT} style={styles.navbarButton} size={25}/>
          </TouchableHighlight>
         }
        />
        <Text>TO DO: Implement this</Text>
      </View>
    );
  }

  onPressButtonBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

}

// Export module.
module.exports = ContactsNearby;
