/**
Author:           Terence John Lampasa
Date Created:     2016-10-05
Date Updated:
Descrption:       This file is used to send Contact as message.
Return:
File Dependency:  ddp.js, Send.js, Helpers.js, Globals.js

Changelog:
  update:  2016-10-05
    -Created
*/
'use strict';
import React, { Component } from 'react';
import {
  AsyncStorage,
  BackAndroid,
  Dimensions,
  Image,
  InteractionManager,
  ListView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  ToolbarAndroid,
  TouchableHighlight,
  View
} from 'react-native';
import {getDisplayName, jsonToArray, toNameCase, toPhoneNumber, toLowerCase, getMediaURL} from './Helpers.js';
import ButtonIcon from './common/ButtonIcon';
// import ContactRetriever from './ContactRetriever';
import ddp from './config/ddp';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import ListRow from './common/ListRow';
import NavigationBar from 'react-native-navbar';
import SearchBar from './SearchBar';
import Send from './config/db/Send';
import Users from './config/db/Users';

// const GLOBAL = require('./../modules/config/Globals.js');
const GLOBAL = require('./config/Globals.js');
const ProgressBar = require('ActivityIndicator');
const styles = StyleSheet.create(require('./../styles/styles.main'));

class TypingAreaContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      friendList: [],
      filter:"",
      currentUser: '',
      userImage: '',
      isLoading: true,
      isLoadingSearch: false,
      theme: this.props.theme(),
    }
    this.filterWithSearchText = this.filterWithSearchText.bind(this);
    this.getContacts = this.getContacts.bind(this);
    this.renderContacts = this.renderContacts.bind(this);
    this.sendContact = this.sendContact.bind(this);
  }

  //get the data before rendering
  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(()=>{
      this.getContacts();
    });
  }

  // get contact that are already registered
  getContacts() {
    let friendList = Users.getAllFriends();
    if (this._isMounted) {
      this.setState({
        friendList: friendList,
        dataSource: this.state.dataSource.cloneWithRows(friendList),
        isLoading: false
      });
    }
  }

  //render each contact
  renderContacts(contact){
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    return (
      <ListRow onPress={() => this.sendContact(contact)}
        labelText={contact.name}
        detailText={contact.statusMessage}
        image={getMediaURL(contact.image)}
        color={color}
        addSeparator={false}
      />
    );
  }

  // Display
  render() {
    var windowSize = Dimensions.get('window');
    let color =  GLOBAL.COLOR_THEME[this.props.theme()];
    var content;
    if (this.state.dataSource.getRowCount() === 0 && !this.state.isLoading) {
      content = <NoContacts
                  filter={this.state.filter}
                  isLoading={this.state.isLoading}
                  theme={this.props.theme}
                />;
    } else if (this.state.isLoading) {
      var searchText="";
      if (this.state.filter == "") {
        searchText = "Retrieving contact list.";
      }
      content = <LoadBar
                theme={this.props.theme}
                text={searchText}/>
    } else {
      content = <ListView
                  ref="listview"
                  keyboardShouldPersistTaps={true}
                  style={styles.listViewRecent}
                  dataSource={this.state.dataSource}
                  renderRow={this.renderContacts}
                  onEndReached={this.onEndReached}
                />;
    }
    return (
      <View style={[styles.container, {backgroundColor:color.CHAT_BG}]}>
        {this.renderTopBar()}
        <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
          {this.renderSearchBar()}
          {content}
        </View>
      </View>
    );
  }

  // Render search bar
  renderSearchBar() {
    return (
      <SearchBar
        theme={this.props.theme}
        onSearchChange={this.filterWithSearchText}
        isLoading={this.state.isLoadingSearch}
        placeholder={`Enter your friend's name`}
        onFocus={() =>
          this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
        value={this.state.filter}
     />
    );
  }

  renderTopBar() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];

    if (Platform.OS === 'ios') {
      return (
        <NavigationBar tintColor={color.THEME}
          title={{title: 'Send Contact', tintColor: color.BUTTON_TEXT}}
          leftButton={<ButtonIcon icon='back' color={color} onPress={() => this.onBackPress()}/>}
        />
      )
    } else {
      return (
        <ToolbarAndroid
          elevation={5}
          title={'Send Contact'}
          titleColor={color.BUTTON_TEXT}
          style={[styles.topContainer, {backgroundColor: color.THEME}]}
        />
      );
    }
  }

  sendContact(contact) {
    this.props.navigator.pop();
    this.props.sendContact(contact);
  }

  // go back to previous screen
  onBackPress() {
    //console.log('pressed back');
    this.props.navigator.pop();
  }

  // Search contacts
  filterWithSearchText(text) {
    text = (text.trim() == '') ? '' : text;
    this.setState({isLoadingSearch: true});
    var contact;
    var tempRawDataSource = this.state.friendList.slice();
    for(var index = 0; index < tempRawDataSource.length; index++) {
      contact = tempRawDataSource[index].name;
      if( (contact).toLowerCase().indexOf((text).toLowerCase())===-1 ) {
        tempRawDataSource.splice(index, 1);
        index=-1;
      }
    }
    if (this._isMounted) {
      this.setState({
        filter: text,
        dataSource: this.state.dataSource.cloneWithRows(tempRawDataSource),
        isLoadingSearch: false
      });
    }
  }
}

// Display for empty records
class NoContacts extends Component {
  render() {
    let color =  GLOBAL.COLOR_THEME[this.props.theme()];
      var text = '';
      if (this.props.filter) {
        text = `No results for "${this.props.filter}"`;
      } else if (!this.props.isLoading) {

        text = 'No contacts found';
      }

      return (
        <View style={{alignItems: 'center'}}>
          <Text style={{marginTop: 20, color: color.TEXT_LIGHT}}>{text}</Text>
        </View>
      );
  }
}

class LoadBar extends Component {
  render() {
    let color =  GLOBAL.COLOR_THEME[this.props.theme()];
    var text = this.props.text;
    var progressBar =
      <View>
        <View style={{justifyContent: 'center'}}>
          <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
        </View>
        <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
      </View>;
    return (
      <View style={{backgroundColor: color.CHAT_BG}}>

          <View style={{justifyContent: 'center'}}>
            <Text style={[styles.loaderMessage, {color: color.TEXT_LIGHT}]}>{text}</Text>
          </View>
          <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
      </View>
    );
  }
}

module.exports = TypingAreaContacts;
