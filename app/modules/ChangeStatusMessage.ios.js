/**
 * Author: Ida Jimenez
 * Date Created: 2016-09-06
 * Date Updated: 2016-11-07
 * Description: Component used for Changing password
 */
'use strict';

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  AsyncStorage,
  TextInput,
  Dimensions
} from 'react-native';
import Button from './common/Button';
import ButtonIcon from './common/ButtonIcon';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import RenderModal from './common/RenderModal';

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const WINDOW_SIZE = Dimensions.get('window');

class ChangeStatusMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser: this.props.loggedInUser(),
      oldStatusMessage: '',
      statusMessage: '',
      isShowMessageModal: false,
      isLoading: false
    }
    this.changeStatusMessage = this.changeStatusMessage.bind(this);
    this.onPressButtonClose = this.onPressButtonClose.bind(this);
    this.setIsShowMessageModal = this.setIsShowMessageModal.bind(this);
  }

  componentWillMount() {
    console.log('componentWillMount');
  }

  componentWillUpdate(){
    console.log('componentWillUpdate');
  }

  componentDidUpdate(){
    console.log('componentDidUpdate');
  }

  componentDidMount() {
    console.log('componentDidMount');
    this._isMounted = true;
    let loggedInUser = this.state.loggedInUser;
    
    if (loggedInUser.profile.hasOwnProperty('statusMessage')) {
      if (loggedInUser.profile.statusMessage && 
          loggedInUser.profile.statusMessage !== 'empty') {
        this.setState({
          oldStatusMessage: loggedInUser.profile.statusMessage,
          statusMessage: loggedInUser.profile.statusMessage
        })
      }
    } 
  }
  
  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        <NavigationBar tintColor={color.THEME}
          title={{ title: 'Status Message', tintColor: color.BUTTON_TEXT}}
          rightButton={<ButtonIcon icon='close' color={color} onPress={this.onPressButtonClose}/>}
        />
        <View style={[form.wrapper]}>
          <View style={[form.textInput, {borderColor: color.THEME}]}>
            <TextInput
              placeholder='Please enter a status message.'
              onChangeText={(statusMessage) => this.setState({statusMessage})}
              style={[form.text, {color: color.TEXT_DARK, marginTop: 10}]}
              value={this.state.statusMessage}
            />
          </View>
          <Text />
          <Button text="Save" color={color.THEME} isDefaultWidth={true} onPress={this.changeStatusMessage} style={form.button} underlayColor={color.HIGHLIGHT} />
        </View>
        <Text style={[form.warning, {color: color.ERROR}]}>
          {this.state.warningMessage}
        </Text>
        {this.renderModal()}
      </View>
    )
  }

  onPressButtonClose() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }

  clearText(fieldName) {
    this.refs[fieldName].setNativeProps({text:''});
  }

  changeStatusMessage() {
    let statusMessage = this.state.statusMessage.trim().replace('/\s+/g',' ');
    let oldStatusMessage = this.state.oldStatusMessage !== 'empty' ? this.state.oldStatusMessage : '';

    // status message does not change do nothing
    if (oldStatusMessage == statusMessage) {
      this.setState({warningMessage: 'You have not made any changes.'});
      return
    }

    if (this._isMounted) {
      this.setState({isLoading: true});
    }
    // Update status message
    ddp.call('updateStatusMessage', [statusMessage])
    .then((response) => {
      this.setIsShowMessageModal('Successfully changed status message.');
      if (this._isMounted) {
        this.props.setStatusMessage(response);
        this.setState({
          oldStatusMessage: response,
          warningMessage: null,
          isLoading: false
        });
      }
    }).catch(function(e) {
      console.log('Something went wrong.');
      console.log(e);
      this.setState({warningMessage: e.reason})
    }.bind(this));
  }

  renderModal() {
    if (this.state.isShowMessageModal) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => this.setIsShowMessageModal("")}
         message={this.state.alertMessage}
         modalType={'alert'}
        />
      );
    } else if (this.state.isLoading) {
      return (
        <RenderModal
         theme={this.props.theme}
         setIsShowModal={() => console.log('loading')}
         modalType={'loading'}
        />
      );
    }
  }

  setIsShowMessageModal(message) {
    if (this._isMounted) {
      this.setState({
        isShowMessageModal: !this.state.isShowMessageModal,
        alertMessage: message
      });
    }
  }
}

let form = StyleSheet.create({
  wrapper: {
    margin: 20
  },
  warning: {
    textAlign: 'center',
    alignItems: 'stretch',
    fontSize: 12, 
    height: 20,
    marginTop: 8,
    marginBottom: 5,
  },
  textInput: {
    flex: 1,
    borderBottomWidth: 1,
    marginBottom: 2
  },
  text: {
    height: 40,
    fontSize: 14,
    textAlign: 'center',
  },
  button: {
    borderRadius: 5,
    backgroundColor: GLOBAL.COLOR_CONSTANT.BUTTON,
    alignItems: 'stretch',
    alignSelf:'center'
  }
});

module.exports = ChangeStatusMessage;