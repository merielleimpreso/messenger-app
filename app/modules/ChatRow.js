/*

Author: Merielle N. Impreso
Modified for Android by Terence
Date Created: 2016-06-15
Date Updated: 2016-06-15
Description: Display each row of chat.
Used In: Chat.android.js

update logs:

update: 2016-06-13 by John B.
  - added text for failed message.
  - bind _.each and fix undefined props
update: 2016-06-16 (Ida)
  - Changed color scheme similar to whatsapp
update: 2016-06-22 (ida)
  - Implement color themes
update: 2016-06-28 (ida)
  - Implement sending like
update: 2016-07-01 (ida)
  - Implement message indicator
update: 2016-07-20 (ida)
  - Implement UI for playing audio
  - Implement UI for pending invites
update: 2016-07-29 (ida)
  - Use ChatRowImage in rendering image
  - Fixed showing 'Delivered Invalid Date' when tapping date
update: 2016-08-16 (ida)
  - Adjust ProgressBar message indicator
update: 2016-08-17 (ida)
  - Remove toggling to show Seen/Delivered
  - Updated renderVideo
  - Added renderAttachment()
update: 2016-08-18 (ida)
  - Change loading when sending audio and attachment
update: 2016-08-22 (ida)
  - Adjust text container based on text length
*/

'use strict';

import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  Image,
  Linking,
  ListView,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native';

import {consoleLog, getMediaURL, dateToTime, toNameCase} from './Helpers';
import _ from 'underscore';
import AudioPlayerView from './AudioPlayerView';
import ChatRowImage from './ChatRowImage';
import ChatRowVideo from './ChatRowVideo';
import FileReader from 'react-native-fs';
import Icon from 'react-native-vector-icons/Ionicons';
import ImageWithLoader from './common/ImageWithLoader';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Validator from 'validator';
import moment from "moment";
import {Video, Audio} from 'react-native-media-kit';
import MapView from 'react-native-maps';
import RenderModal from './common/RenderModal';
import Send from './config/db/Send';
import Users from './config/db/Users';
let ProgressBar = require('ActivityIndicator');

const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles.main'));
const windowSize = Dimensions.get('window');

class ChatRow extends Component{

  // Get initial state.
  constructor(props) {
    super(props);
    this.state = {
      displaySeen: false,
      isShowContactModal: false,
      contact: null,
      loading: true,
      isLoaded: false
    }
    this.renderAttachment = this.renderAttachment.bind(this);
    this.renderAudio = this.renderAudio.bind(this);
    this.renderDate = this.renderDate.bind(this);
    this.renderDetails = this.renderDetails.bind(this);
    this.renderImage = this.renderImage.bind(this);
    this.renderLike = this.renderLike.bind(this);
    this.renderLocation = this.renderLocation.bind(this);
    this.renderSticker = this.renderSticker.bind(this);
    this.renderText = this.renderText.bind(this);
    this.renderVideo = this.renderVideo.bind(this);
    this.viewImage = this.viewImage.bind(this);
    this.viewMap = this.viewMap.bind(this);
    this.playVideo = this.playVideo.bind(this);
    this.renderProfilePic = this.renderProfilePic.bind(this);
    this.adaptToImageURL = this.adaptToImageURL.bind(this);
    this.setIsShowContactModal = this.setIsShowContactModal.bind(this);
    this.goToMessageChat = this.goToMessageChat.bind(this);
    this.renderContact = this.renderContact.bind(this);
    this.renderContactModal = this.renderContactModal.bind(this);
  }

  //Put a flag to know if component is mounted.
  componentDidMount() {
    this._isMounted = true;
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
  }

  // Display audio.
  renderAudio(audio, isCurrentUser, chat) {

    let color = this.props.color;
    let backgroundColor = color.CHAT_SENT;//isCurrentUser ? color.CHAT_SENT : color.CHAT_RCVD;
    backgroundColor = chat.isFailed ? color.ERROR : backgroundColor;
    var width = windowSize.width * 0.65;

    if (chat.isTemporary) {
      if (chat.isFailed) {
        return (
          <View style={[styles.chatTextContainer, {height: 40, padding:0, backgroundColor: backgroundColor, maxWidth:width}]}>
            <View style={[styles.chatTextContainerOpaque, {padding:10, backgroundColor:'#000000AA'}]}>
              <Text style={{color:color.BUTTON, fontSize:10, alignSelf:'center'}}>Failed to send</Text>
            </View>
          </View>
        );
      } else {
        return (
          <View style={[styles.chatTextContainer, {height: 40, padding:0, backgroundColor: backgroundColor, maxWidth:width}]}>
            <View style={[styles.chatTextContainerOpaque, {padding:10, backgroundColor:'#000000AA'}]}>
              <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
              <Text style={{color:color.BUTTON, fontSize:10, alignSelf:'center'}}> Uploading... {Math.round(100 * chat.progress)}%</Text>
            </View>
          </View>
        );
      }
    } else {
      let audioURL = getMediaURL(audio.data);
      return (
        <AudioPlayerView audio={audio} color={this.props.color} isCurrentUser={isCurrentUser}  hasInternet={this.props.hasInternet} />
      );
    }
  }

  // Display date.
  renderDate(date) {
    var color = this.props.color;
    return (
      <View style={{flex: 1}}>
        <View style={[styles.chatDateContainer, {backgroundColor:'transparent'}]}>
          <Text style={[styles.chatDateText, {color:color.THEME, backgroundColor:'transparent'}]}> {date} </Text>
        </View>
      </View>
    );
  }

  // Display Delivered or Sent details.
  renderDetails(chat, myID) {
    let color = this.props.color;
    let align = 'flex-start';
    var timeView = null;
    var indicatorView = null;

    // Display for CreateDate
    timeView = <Text style={[styles.chatDetailsText, {color:color.TEXT_LIGHT, backgroundColor:'transparent'}]}> {dateToTime(chat.CreateDate, this.props.timeFormat())} </Text>;

    // Display for indicator
    if (myID == chat.FromUserFID) {
      align = 'flex-end';
      if (chat.isTemporary) {
        // To do put arrow
        indicatorView = <KlikFonts name="sending" style={{color: color.TEXT_LIGHT, backgroundColor:'transparent', fontSize: 10}}/>;
      } else {
        var readText = '';
        if (chat.Seen && chat.Seen.length > 0) {
          if (chat.ChatId.includes('group_')) {
            indicatorView = <Text style={[styles.chatDetailsText, {color:color.TEXT_LIGHT, backgroundColor:'transparent'}]}>Read by {chat.Seen.length}</Text>;
          } else {
            indicatorView = <Text style={[styles.chatDetailsText, {color:color.TEXT_LIGHT, backgroundColor:'transparent'}]}>Read</Text>;
          }
        }
      }
    }

    return (
      <View style={{alignItems: align, marginLeft: 5, marginRight: 5}}>
        {indicatorView}
        {timeView}
      </View>
    );
  }

  adaptToImageURL(image) {
    if (image[0].hasOwnProperty('URL')) {
      image[0].URL = getMediaURL(image[0].URL);
      image[0].original = getMediaURL(image[0].original);
      image[0].small = getMediaURL(image[0].small);
      image[0].medium = getMediaURL(image[0].medium);
      image[0].large = getMediaURL(image[0].large);
    } else {
      image[0].medium = image[0].medium;
    }
    return image;
  }

  // Display image.
  renderImage(image, chat) {
    image = this.adaptToImageURL(image);
    let color = this.props.color;
    return (
      <ChatRowImage chat={this.props.chat}
       color={color}
       messagePhoto={image}
       imageGallery={this.props.imageGallery}
       viewImage={this.viewImage}/>
    )
  }

  // Display location.
  renderLocation(location) {
    let color = this.props.color;
    var  map =
    <TouchableOpacity
        onPress={() => this.openLocationInMaps(location)}
        style={{width: 152, height: 152, borderWidth: 0.5, borderColor: color.TEXT_LIGHT,
          marginLeft: 12, marginRight: 12, borderRadius: 5 }}>
      <View>
        <MapView style={{height: 150, borderRadius: 9}}
          region={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
          }}>
          <MapView.Marker
            coordinate={location}
            title={"You"}
            description={"You're here"}
          />
          </MapView>
      </View>
    </TouchableOpacity>;

           return map;
  }

  /*
  Check message if it contains link by separating each word in the message.
  Display text depending if it is a link or not.
  */
  renderText(text, chat, myID) {
    var color = this.props.color;
    var backgroundColor = (chat.FromUserFID == myID) ? color.CHAT_SENT : color.CHAT_RCVD;
    backgroundColor = chat.isFailed ? color.ERROR : backgroundColor;
    var contents = <Text style={{color: color.CHAT_TEXT}}>{text}</Text>;

    if (typeof text === 'string') {
      var words = text.split(/\s/);
      contents = words.map(function(word, i) {
        var separator = i < (words.length - 1) ? ' ' : '';
        if (Validator.isURL(word)) {
          return <Link key={i} url={word} color={color} >{word}{separator}</Link>;
        } else {
          return word + separator;
        }
      });
    }
    var width = windowSize.width * 0.65;
    return (
      // <View style={[styles.chatTextWrapContainer, {backgroundColor: backgroundColor, maxWidth: width}]}>
      //   <Text style={{color: color.CHAT_TEXT}}> {contents} </Text>
      // </View>
      <View style={styles.chatContainer}>
        <View style={[styles.chatTextWrapContainer, { backgroundColor: backgroundColor }]}>
            <Text style={{color: color.CHAT_TEXT, fontSize:16}}>
              {contents}
            </Text>
        </View>
      </View>
    );
  }

  // Display sticker.
  renderSticker(sticker) {
    var color = this.props.color;
    var link = getMediaURL(sticker);
    return <ImageWithLoader image={link} color={color} style={{width:150, height:150, borderRadius: 75}} sticker={true} />;
  }

  // Display Icon.
  renderLike(like) {
    var color = this.props.color;
    return <KlikFonts name="emoticon" style={[styles.messageIcon, {color: color.CHAT_SENT}]}/>
  }

  // Display video.
  renderVideo(video) {
    let videoURL = getMediaURL(video);
    return (
      <ChatRowVideo chat={this.props.chat} color={this.props.color} video={videoURL} playVideo={this.playVideo} />
    )
  }

  renderAttachment(attachment, chat, myID) {
    var color = this.props.color;
    var backgroundColor = (chat.FromUserFID == myID) ? color.CHAT_SENT : color.CHAT_RCVD;
    backgroundColor = chat.isFailed ? color.ERROR : backgroundColor;
    var width = windowSize.width * 0.65;

    if (chat.isTemporary) {
      if (chat.isFailed) {
        var filename = 'Attachment';
        return (
          <View style={[styles.chatTextContainer, {height: 40, padding:0, backgroundColor: backgroundColor, maxWidth:width}]}>
            <View style={[styles.chatTextContainerOpaque, {padding:10, backgroundColor:'#000000AA'}]}>
              <Text style={{color:color.BUTTON}}>{filename}: Failed to send</Text>
            </View>
          </View>
        );
      } else {
        return (
          <View style={[styles.chatTextContainer, {height: 40, padding:0, backgroundColor: backgroundColor, maxWidth:width}]}>
            <View style={[styles.chatTextContainerOpaque, {padding:10, backgroundColor:'#000000AA'}]}>
              <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
              <Text style={{color:color.BUTTON, fontSize:10, alignSelf:'center'}}> Uploading... {Math.round(100 * chat.progress)}%</Text>
            </View>
          </View>
        );
      }
    } else {
      return (
        <View style={{flexWrap: 'wrap', maxWidth: width, paddingLeft: 10, paddingRight: 10}}>
          <View style={[styles.chatTextWrapContainer, {backgroundColor: backgroundColor}]}>
            <Link key={attachment.link} url={attachment.link} name={attachment.filename} color={color} />
          </View>
        </View>
      );
    }
  }

  renderContact(contact, chat, myID) {
    let color = this.props.color;
    let backgroundColor = (chat.FromUserFID == myID) ? color.CHAT_SENT : color.CHAT_RCVD;
    backgroundColor = chat.isFailed ? color.ERROR : backgroundColor;
    let name = `${toNameCase(contact.profile.firstName)} ${toNameCase(contact.profile.lastName)}`;
    let photoUrl = getMediaURL(contact.profile.image);
    let isContact =  _.contains(this.props.loggedInUser().contacts, contact._id);
    let hasAlreadySentFR = _.contains(_.pluck(this.props.loggedInUser().friendRequestSent, 'userId'), contact._id);
    let hasAlreadyReceivedFR = _.contains(_.pluck(this.props.loggedInUser().friendRequestReceived, 'userId'), contact._id);
    let actions = [];
    let relStatus = null;

    // if (this.props.loggedInUser()._id !== contact._id) {
      if (isContact || hasAlreadySentFR) {
        if (hasAlreadySentFR) {
          relStatus = 'Friend request sent';
        } else {
          relStatus = 'Already your friend';
        }
        actions = [{icon: 'recent', iconLabel: 'Chat', onPressIcon: () => this.goToMessageChat(contact)}];
      } else {
        if (hasAlreadyReceivedFR) {
          relStatus = 'Respond to friend request';
          actions = [{icon: 'close', iconLabel: 'Ignore', onPressIcon: () => this.setIsShowContactModal(null)},
                     {icon: 'check', iconLabel: 'Accept Request', onPressIcon: () => this.acceptFriendRequest(contact._id)}];
        } else {
          actions = [{icon: 'close', iconLabel: 'Ignore', onPressIcon: () => this.setIsShowContactModal(null)},
                     {icon: 'check', iconLabel: 'Send Friend Request', onPressIcon: () => this.sendFriendRequest(contact._id)}];
        }
      }
    // }

    let contactInfo = {
      data: contact,
      actions: actions,
      relStatus: relStatus
    }

    return (
      <View style={styles.chatContainer}>
        <TouchableWithoutFeedback onPress={() => this.setIsShowContactModal(contactInfo)}>
          <View style={[styles.chatTextWrapContainer, { backgroundColor: backgroundColor, flexDirection: 'row', alignItems: 'center'}]}>
            <View style={{margin: 5}}>
              <ImageWithLoader style={{width:50, height:50}} color={color} image={photoUrl} />
            </View>
            <Text style={{color: color.CHAT_TEXT}} >{name}</Text>
          </View>
        </TouchableWithoutFeedback>
        {this.renderContactModal(contact._id, myID)}
      </View>
    );
  }

  /*
  Check what data should be displayed: date, audio, image, location, text, sticker or video.
  Render display depending on the data type.
  Render the Delivered or Sent data.
  */
  render() {

    let color = this.props.color;
    let chat = this.props.chat;
    let myID = this.props.myID;
    let users = this.props.users;
    let content = <View />;
    let contentDetails = <View />;

    let date = chat.date;
    let pendingInvite = chat.pendingInvite;
    let profilePic = chat.FromUserFID;
    let userImageView;
    let triangle = <View />;
    if (date) {
      contentDetails = this.renderDate(date);
      userImageView = <View />;
    } else if (pendingInvite) {
      contentDetails = this.renderDate(pendingInvite);
      userImageView = <View />;
    } else {
      userImageView = this.renderProfilePic(profilePic, myID);
      var attachment = chat.Message.Attachment;
      var details = this.renderDetails(chat, myID);
      var justifyContentAttr = (myID == chat.FromUserFID) ? 'flex-end' : 'flex-start';

      var triangleStyleColor = (chat.FromUserFID == myID) ? color.CHAT_SENT : color.CHAT_RCVD;
      triangleStyleColor = chat.isFailed ? color.ERROR : triangleStyleColor;
      var triangleStyle = (myID == chat.FromUserFID) ?
                            [styles.triangleSent, {borderTopColor: triangleStyleColor, marginLeft: -5 }] :
                            [styles.triangleReceived, {borderTopColor: triangleStyleColor, marginRight: -5}];
      var audio = chat.Message.Audio;
      var image = chat.Message.Media;
      var like = chat.Message.Like;
      var location = chat.Message.Location;
      var text = chat.Message.Text;
      var sticker = chat.Message.Sticker;
      var video = chat.Message.Video;
      var contact = chat.Message.ContactDetails;
      var isCurrentUser = (chat.FromUserFID == myID);

      if (attachment) {
        content = this.renderAttachment(attachment, chat, myID);
      } else if (audio) {
        content = this.renderAudio(audio, isCurrentUser, chat);
        triangle = (chat.isTemporary) ? triangle : <View style={triangleStyle}/>;
      } else if (image) {
        content = this.renderImage(image, chat);
      } else if (like) {
        content = this.renderLike(like);
      } else if (location) {
        content = this.renderLocation(location);
      } else if (text) {
        content = this.renderText(text, chat, myID);
        triangle = <View style={triangleStyle}/>
      } else if (sticker) {
        content = this.renderSticker(sticker);
      } else if (video) {
        content = this.renderVideo(video);
      } else if(contact) {
        content = this.renderContact(contact, chat, myID)
        triangle = <View style={triangleStyle}/>
      }
      if (myID == chat.FromUserFID) {
        contentDetails =  <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                            {details}
                            <TouchableHighlight onLongPress={() => this.props.setShowModal(true, text)}
                              underlayColor='transparent'>
                              <View style={{flexDirection: 'row'}}>
                                {content}
                                {triangle}
                              </View>
                             </TouchableHighlight>
                          </View>;
      } else {
        contentDetails =  <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                            {userImageView}
                            <TouchableHighlight onLongPress={() => this.props.setShowModal(true, text)}
                            underlayColor='transparent'>
                            <View style={{flexDirection: 'row'}}>
                              {triangle}
                              {content}
                            </View>
                            </TouchableHighlight>
                            {details}
                          </View>;
      }

    }

    return (

      <View style={[styles.chatMessageContainer, {justifyContent: justifyContentAttr}]}>

          {contentDetails}

      </View>

    );
  }

  renderProfilePic(profilePic, myID) {
    let color = this.props.color;
    let user = Users.getItemById(profilePic);
    let photoUrl = getMediaURL(user.profile.photoUrl)
    let userImageView = <View style={[styles.chatUserImage, {backgroundColor: '#bcbec0'}]}>
                          <Image style={styles.chatUserImage} source={{uri: photoUrl}}
                            onLoadStart={(e) => this.setState({loading: true})}
                            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
                            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})}>
                          </Image>
                        </View>;
    return userImageView;
  }

  renderContactModal(contactId, myID) {
    let contact = this.state.contact;
    if (this.state.isShowContactModal && !(_.isEmpty(contact))) {
      if (contactId !== myID) {
        return (
          <RenderModal
            color={this.props.color}
            profileData={contact}
            setIsShowModal={() => this.setIsShowContactModal(null)}
            modalType={'viewProfile'}
          />
        );
      }
    }
  }

  acceptFriendRequest(id) {
    this.setIsShowContactModal(null);
    this.props.acceptRequest(id);
  }

  sendFriendRequest(id) {
    this.setIsShowContactModal(null);
    Send.sendFriendRequest(id);
  }

  goToMessageChat(contact) {
    this.setIsShowContactModal(null);
    requestAnimationFrame(() => {
      this.props.navigator.replace({
        id:'messagesgiftedchat',
        name:'MessagesGiftedChat',
        backgroundImageSrc: this.props.backgroundImageSrc,
        changeTheme: this.props.changeTheme,
        getMessageWallpaper: this.props.getMessageWallpaper,
        group: null,
        hasInternet: this.props.hasInternet,
        isSafeToBack: this.props.isSafeToBack,
        loggedInUser: this.props.loggedInUser,
        renderConnectionStatus: this.props.renderConnectionStatus,
        setIsSafeToBack: this.props.setIsSafeToBack,
        shouldReloadData: this.props.shouldReloadData,
        theme: this.props.theme,
        timeFormat: this.props.timeFormat,
        user: contact,
        users: null,
      });
    });
  }

  setIsShowContactModal(contact) {
    if (this._isMounted) {
      this.setState({
        isShowContactModal: !this.state.isShowContactModal,
        contact: contact
      });
    }
  }

  // Open location in maps
  openLocationInMaps(location) {
    let url = 'geo:' + location.latitude + ',' + location.longitude + '?z=17&q=' + location.latitude + ',' + location.longitude;

    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Cannot open URI: ' + url);
        alert('Please install Google Maps');
      }
    });

  }

  // View image.
  viewImage(image, chat, imageGallery) {
    // this.props.setIsSafeToBack(false);
    this.props.navigator.push({
      id: 'viewphoto',
      name: 'ViewPhoto',
      chat: chat,
      chatType: this.props.chatType,
      color: this.props.color,
      data: image,
      imageGallery: imageGallery,
      loggedInUser: this.props.loggedInUser,
      users: this.props.users,
      // setIsSafeToBack: this.props.setIsSafeToBack
    });
  }

  // View map.
  viewMap() {
    console.warn('TO DO: View Map');
  }

  // Play video clip
  playVideo(video) {
    this.props.navigator.push({
      id: 'videoplayer',
      name: 'VideoPlayer',
      color: this.props.color,
      data: video,
      media: 'Video'
    });
  }

}

// Class for links
class Link extends Component{
  // Render text display.
  render() {
    let color = this.props.color;
    var name = (this.props.name) ? this.props.name : this.props.url;
    return <Text onPress={this.openUrl.bind(this, this.props.url)}
            style={[styles.hyperlink, {color: color.CHAT_TEXT, fontSize:16}]}>
              {name}
           </Text>;
  }

  /*
  Open the URL link.
  A note from ReactNative: For web URLs, the protocol ("http://", "https://") must be set accordingly.
  */
  openUrl(url) {
    if (!url.match(/^[a-zA-Z]+:\/\//)) {
      url = 'http://' + url;
    }
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Cannot open URI: ' + url);
      }
    });
  }

}

module.exports = ChatRow;
