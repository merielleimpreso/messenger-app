'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  Image,
  InteractionManager,
  ListView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import _ from 'underscore';
import {logTime, toNameCase} from './Helpers';
import BEMCheckBox from 'react-native-bem-check-box';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import Modal from 'react-native-simple-modal';
import ScreenNoItemFound from './ScreenNoItemFound';
import SearchBar from './SearchBar';
import NavigationBar from 'react-native-navbar';
const GLOBAL = require('./config/Globals.js');
const styles = StyleSheet.create(require('./../styles/styles_common.js'));

class ContactsCreateGroupAdd extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chosenContactIds: [],
      contacts: [],
      dataSource: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2}),
      isLoading: true,
      theme: this.props.theme(),
      loading: true,
      isLoaded: false
    };
    this.render = this.render.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.chooseContact = this.chooseContact.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getAllContacts();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllContacts() {
    let contacts = _.sortBy(_.without(this.props.loggedInUser().contacts, this.props.chosenContactIds), 'name');
    if (this._isMounted) {
      this.setState({
        contacts: contacts,
        dataSource: this.state.dataSource.cloneWithRows(this.getContactsInfo(contacts)),
        isLoading: false
      });
    }
  }

  getContactsInfo(ids) {
    var contacts = [];
    for (var i = 0; i < ids.length; i++) {
      let c = _.findWhere(this.props.users(), {_id:ids[i]});
      if (c) {
        var name = '';
        var image = GLOBAL.DEFAULT_IMAGE;

        name = toNameCase(c.profile.firstName) + ' ' + toNameCase(c.profile.lastName);
        image = (c.profile.photoUrl) ? c.profile.photoUrl : GLOBAL.DEFAULT_IMAGE;

        let contact = {
          _id: c._id,
          name: name,
          image: image
        };
        contacts.push(contact);
      }
    }
    return contacts;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Add Members', tintColor: color.BUTTON_TEXT}}
         rightButton={this.renderRightButton()}
         leftButton={
          <TouchableHighlight onPress={() => this.onPressButtonBack()} underlayColor='transparent' style={{alignSelf:'center'}}>
            <KlikFonts name='close' color={color.BUTTON_TEXT} style={styles.navbarButton} size={25}/>
          </TouchableHighlight>
         }
        />
        <SearchBar
         theme={this.props.theme}
         onSearchChange={this.onSearchChange}
         isLoading={false}
         placeholder='Search by Name'
         onFocus={() =>
          this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}
       />
       {this.renderContent()}
      </View>
    )
  }

  renderRightButton() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let chosenCount = this.state.chosenContactIds.length;

    if (chosenCount > 0) {
      var text = 'Add';
      text += (chosenCount > 0) ? '(' + chosenCount + ')' : '';
      return (
        <TouchableHighlight onPress={() => this.onPressAdd()} underlayColor='transparent' style={{alignSelf:'center'}}>
          <Text style={{color:color.BUTTON_TEXT, margin:10, fontSize:15, fontWeight:'600'}}>{text}</Text>
        </TouchableHighlight>
      );
    } else {
      return <View/>;
    }
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.props.loggedInUser().contacts.length == this.props.chosenContactIds.length) {
      return <ScreenNoItemFound isLoading={false} text={'You have no friends to choose from'} color={color}/>;
    } else {
      if (!this.state.isLoading && this.state.dataSource.getRowCount() == 0) {
        return <ScreenNoItemFound isLoading={false} text={'No contact found'} color={color}/>;
      } else {
        return (
          <ListView ref="listview"
           dataSource={this.state.dataSource}
           renderRow={this.renderRow}
           automaticallyAdjustContentInsets={false}
           keyboardDismissMode="on-drag"
           keyboardShouldPersistTaps={true}
           showsVerticalScrollIndicator={false}
          />
        );
      }
    }
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let checked = _.contains(this.state.chosenContactIds, data._id);
    return (
      <View>
        <View style={[styles.contactsListViewRow, {justifyContent:'center', alignItems:'center', margin:5}]}>
          <BEMCheckBox style={{height:20,width:20, borderRadius:0}}
            animationDuration={0.3}
            onCheckColor={color.BUTTON_TEXT}
            value={checked}
            onFillColor={color.BUTTON}
            onAnimationType='fill'
            offAnimationType='fill'
            lineWidth={2}
            onValueChange={value => this.chooseContact(data, value)}
            boxType="circle"
            tintColor={color.BUTTON}
            onTintColor={color.BUTTON}
           />
          <Image source={{uri:data.image}} style={[styles.listViewImage, {marginLeft: 10}]}
            onLoadStart={(e) => this.setState({loading: true})}
            onError={(e) => this.setState({error: e.nativeEvent.error, loading: false})}
            onProgress={(e) => this.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
            onLoad={() => this.setState({loading: false, error: false, isLoaded: true})} >
            {(this.state.loading && !this.state.isLoaded) 
              ? <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color.CHAT_BG, height:40, width:40}}>
                  <ActivityIndicator size="small" color={color.BUTTON} />
                </View>
              : null
            }
          </Image>
          <View style={styles.listViewRowContainer}>
            <Text numberOfLines={1} style={[styles.chatRecentTextName, {color:color.TEXT_DARK}]}>{data.name}</Text>
          </View>
        </View>
        <View style={{height:1, marginLeft:80, marginRight:10, backgroundColor:color.INACTIVE_BUTTON}}/>
      </View>
    );
  }

  onSearchChange(searchText) {
    var contacts = _.filter(this.state.contacts, function(contact){
      return (contact.name).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });

    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(contacts)
      });
    }
  }

  chooseContact(contact, isChecked) {
    if (isChecked) {
      var array = this.state.chosenContactIds;
      array.push(contact._id);
      this.setState({
        chosenContactIds: array,
      });
    } else {
      var array = this.state.chosenContactIds;
      array = _.without(array, contact._id);
      this.setState({
        chosenContactIds: array
      });
    }
  }

  onPressAdd() {
    this.props.setChosenContactIds(this.state.chosenContactIds);
    this.onPressButtonBack();
  }

  onPressButtonBack() {
    requestAnimationFrame(() => {
      this.props.navigator.pop(0);
    });
  }
}

module.exports = ContactsCreateGroupAdd;
