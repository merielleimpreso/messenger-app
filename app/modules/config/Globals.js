/*

Author: Merielle Impreso
Date Created: 2016-06-09
Date Updated: N/A
Description: List all the constant variables to be used
File Dependency: N/A

Changelog:
update: 2016-05-04 (by Merielle I.)
  - colors config
update: 2016-05-10 (Terence)
  -Changed TIMEOUT from 12000 to 20000
update: 2016-06-09 (Ida)
  - Added SERVER_URL_UPLOAD
update: 2016-07-01 (Ida)
  - Added CHAT_TEXT for Color themes: Normal, Lavender and Dracula
*/

module.exports = {
  APP_NAME: 'Messenger',
  APP_SECRET: 'Kjsge2LkopD0',
  ALERT_CREATE_GROUP_ERROR_TITLE: 'Ooooops!',
  ALERT_CREATE_GROUP_ERROR_DETAIL: 'Please add at least one contact.',
  ALERT_CREATE_GROUP_ERROR_DETAIL_NAME: 'Please enter the group name.',
  CHAT: {
    DIRECT: 'chatDirect',
    GROUP: 'chatGroup'
  },
  COLOR_CONSTANT: {
    THEME: '#22995c',
    CHAT_BG: '#ffffff',
    HIGHLIGHT: '#bcbec0', //'#e8e5e6',
    TEXT_DARK: '#231f20',
    TEXT_LIGHT: '#939598',
    TEXT_INPUT_PLACEHOLDER: '#808184',
    BUTTON: '#8ac341',
    BUTTON_TEXT: '#ffffff',
    CHAT_RCVD: '#ffffff',
    CHAT_SENT: '#cee4ad',
    CHAT_TEXT: '#231f20',
    ERROR: '#FF0000',
    INACTIVE_BUTTON: "#94d0ac",
    TRANSPARENT: '#fafafa',
    BORDER: '#95CFAD',
    STATUS_BAR: '#1f8749',
  },
  COLOR_THEME: {
    ORIGINAL:{
      THEME: '#22995c',
      CHAT_BG: '#ffffff',
      HIGHLIGHT: '#e8e5e6',
      TEXT_DARK: '#231f20',
      TEXT_LIGHT: '#939598',
      TEXT_INPUT_PLACEHOLDER: '#808184',
      BUTTON: '#8ac341',
      BUTTON_TEXT: '#ffffff',
      CHAT_RCVD: '#ffffff',
      CHAT_SENT: '#cee4ad',
      CHAT_TEXT: '#231f20',
      ERROR: '#FF0000',
      INACTIVE_BUTTON: "#94d0ac",
      TRANSPARENT: '#fafafa',
      BORDER: '#95CFAD',
      STATUS_BAR: '#1f8749',
    },
    BLUE:{
      THEME: '#58595b',
      CHAT_BG: '#f4f1f1',
      HIGHLIGHT: '#e8e5e6',
      TEXT_DARK: '#231f20',
      TEXT_LIGHT: '#808184',
      TEXT_INPUT_PLACEHOLDER: '#808184',
      BUTTON: '#808184',
      BUTTON_TEXT: '#ffffff',
      CHAT_RCVD: '#ffffff',
      CHAT_SENT: '#cee4ad',
      CHAT_TEXT: '#231f20',
      ERROR: '#FF0000',
      INACTIVE_BUTTON: "#c8c8c8",
      TRANSPARENT: '#fafafa',
      BORDER: '#c8c8c8',
      STATUS_BAR: '#58595b'
    },
    GREEN: {
      THEME: '#08544A',
      HIGHLIGHT: '#9EA3A6',
      TEXT_DARK: '#181818',
      TEXT_LIGHT: '#707070',
      TEXT_INPUT_PLACEHOLDER: '#C8C8C8',
      BUTTON: '#0DCD56',
      BUTTON_TEXT: '#FFFFFF',
      CHAT_TEXT: '#000000',
      CHAT_BG: '#E0E0D2',
      CHAT_RCVD: '#FFFFFF',
      CHAT_SENT: '#E5FFCC',
      ERROR: '#FF0000',
      INACTIVE_BUTTON: '#82A9A4',
      TRANSPARENT: '#00000050',
      BORDER: '#82A9A4',
      STATUS_BAR: '#08544A'
    },
    LAVENDER: {
      THEME: '#916BE3',
      HIGHLIGHT: '#EAEAEA',
      TEXT_DARK: '#000000',
      TEXT_LIGHT: '#787878',
      TEXT_INPUT_PLACEHOLDER: '#C8C8C8',
      BUTTON: '#8C96FA',
      BUTTON_TEXT: '#FFFFFF',
      CHAT_TEXT: '#000000',
      CHAT_BG: '#FFFFFF',
      CHAT_RCVD: '#EAEAEA',
      CHAT_SENT: '#89B0FF',
      ERROR: '#FF0000',
      INACTIVE_BUTTON: '#A0A0A0',
      TRANSPARENT: '#00000050',
      BORDER: '#A0A0A0',
      STATUS_BAR: '#916BE3'
    },
    DARK: {
      THEME: '#911D22',
      HIGHLIGHT: '#3C1414',
      TEXT_DARK: '#FFFFFF',
      TEXT_LIGHT: '#787878',
      TEXT_INPUT_PLACEHOLDER: '#C8C8C850',
      BUTTON: '#C1272D',
      BUTTON_TEXT: '#FFFFFF',
      CHAT_TEXT: '#fafafa',
      CHAT_BG: '#141414',
      CHAT_RCVD: '#3C1414',
      CHAT_SENT: '#302B2B',
      ERROR: '#FF0000',
      INACTIVE_BUTTON: '#787878',
      TRANSPARENT: '#FFFFFF20',
      BORDER: '#787878',
      STATUS_BAR: '#911D22'
    }
  },
  DEFAULT_IMAGE: 'http://www.sessionlogs.com/media/icons/defaultIcon.png',
  DEFAULT_PROFILE_IMAGE: '/assets/avatar/tempprofpic.jpg',
  DEFAULT_GROUP_IMAGE: '/assets/avatar/tempgrouppic.jpg',
  ERROR_NOT_LOGIN: 403,
  IS_ACTUAL_SERVER: true,
  NAVBAR_TITLE: {
    ADD_CONTACT: 'Add Contact'
  },
  SECRET_KEY: 'ZTWlIxZKJHU0ghCf',
  SHOULD_SHOW_INTERNET_FAILURE: false,
  SHOULD_SHOW_LOGS: false,
  SHOULD_SHOW_DDP_WARNINGS: true,
  TIMEOUT: 20000,
  TIMEOUT_MESSAGE: 'Failed to connect.',
  TIMEOUT_MESSAGE_DISCONNECTED: 'You have been disconnected.\nPlease check your internet connection.',
  TEXT_CONNECTING_TO_SERVER: 'Connecting to server...',
  TEXT_LOADING: 'Loading...',
  TEXT_LOGGING_IN: 'Logging you in...',
  TEXT_LOGGING_OUT: 'Logging you out...',
  TEXT_LOADING_STICKERS: 'Loading stickers',
  TEXT_NO_INTERNET_CONNECTION: 'No Internet Connection',
  TEXT_NO_INTERNET_CONNECTION_DETAILS: 'The feature you are trying to use requires that the app is online.',
  TEXT_RETRIEVING_MESSAGES: 'Retrieving your messages...',
  TEXT_NO_RESULTS_FOUND_FOR: 'No results found for ',
  TEXT_SEARCH: 'Search',
  TEXT_SEARCH_CONTACT: 'Search for people on Klikchat\nusing their ',
  TEXT_SERVER_TAKING_A_WHILE: 'Server is taking quite a while to respond.\nPlease wait...',
  THEMES: ['ORIGINAL', 'BLUE', 'GREEN', 'LAVENDER', 'DARK'],
  FCM_USER_TOKEN: '',

  // dev
  // SERVER_HOST: 'elb-dev-763284714.ap-southeast-1.elb.amazonaws.com',
  // SERVER_PORT: 5001,

  // prod
  SERVER_HOST: 'elb-prod-235443716.ap-southeast-1.elb.amazonaws.com',
  SERVER_PORT: 6001,

  //kliklabs
  // SERVER_HOST: 'klikchat-203299124.ap-southeast-1.elb.amazonaws.com',
  // SERVER_PORT: 5002,

  S3_ENDPOINT: 'https://klikchatmediabucket.s3.amazonaws.com',
  URL_HAVING_TROUBLE: 'https://www.kliklabs.com/forgetpassword/',

  TEST_MODE: false
};
