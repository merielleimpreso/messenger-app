import React from 'react';
import ReactNative, {
  AsyncStorage,
  Platform,
} from 'react-native';
import _ from 'underscore';
import RNFetchBlob from 'react-native-fetch-blob'

import {logTime} from '../../Helpers';
import ddp from '../ddp';
import GLOBAL from '../Globals';
import StoreCache from './StoreCache.js';

let Send = {};

Send.uploadURL = {
  mediaChat: ddp.urlUpload,
  mediaComment: ddp.urlUploadComment,
  groupImage: ddp.urlUploadGroupImage,
  postTimeline: ddp.urlUploadPost,
  profilePicture: ddp.urlUploadProfileAvatar
}

/* -------------------------------------- SIMPLE SENDING -------------------------------------- */

Send.newMessage = (message) => {
  console.log("newMessage: ", message);
  message["date"] = new Date();
  message["_id"] = Send.makeUniqueId(message.fromUserId, message.toUserId);
  Send.upsertToCollection(message);
  Send.insert(message);
}

Send.insert = (message, callback) => {
  console.log("trying send message. ddp: \n", ddp.connected);
  // console.log('message to send', message);
  if (ddp.isLoggedIn) {
    ddp.call('newMessage', [message]).then((result) => {
      console.log('Send ['+ logTime() +']: Insert successful message = ', result._id);
      if (callback) callback();
    }).catch((err) => {
      message.status = 'Failed';
      console.log('failed', message)
      Send.upsertToCollection(message);
      console.log('Send ['+ logTime() +']: Insertion failed', err);
    });
  } else {
    message.status = 'Failed';
    console.log('failed', message)
    Send.upsertToCollection(message);
    console.log("sendMessage Failure. DDP wasn't initialized. Probably. Send.insert()" );
  }
};

Send.resend = (message, mediaType, uploadURL, mediaPath, isAutoSaveMedia) => {
  message.status = 'Sending';
  message.date = new Date();
  Send.upsertToCollection(message);
  if (message.isMedia) {
    Send.uploadMedia(message, mediaType, uploadURL, mediaPath, isAutoSaveMedia);
  } else if (message) {
    Send.insert(message);
  } else {
    console.log('delete');
    ddp.collections.message.remove({"_id": message._id});
  }
}

/*Send.failed = (options) => {
  options.failed = true;
  let temp = Send.makeTempObject(options);
  console.log('failed');
  return temp;
}*/

/* -------------------------------------- UPLOAD REQUEST -------------------------------------- */

Send.media = (data, mediaType, uploadType, mediaPath, isAutoSaveMedia) => {
  let messageObj = Send.getMessageObject(data, mediaType);
  Send.upsertToCollection(messageObj);
  Send.uploadMedia(messageObj, mediaType, uploadType, mediaPath, isAutoSaveMedia);
};

Send.getMessageObject = (data, mediaType) => {
  data['_id'] = Send.makeUniqueId(data.fromUserId, data.toUserId);
  data['date'] = new Date();
  data['status'] = 'Sending';
  data['isTemporary'] = true;
  data['isMedia'] = true;
  data['progress'] = 0;

  if (mediaType == 'attachment') {
    // removed implementation
    let fileName = uri.replace(/^.*[\\\/]/, '');
    tempObj.message = {
      attachment: {
        link: uri,
        filename: fileName
      }
    }
  } else if (mediaType == 'audio') {
    data.message = {
      audio: {dataUri: data.uri}
    }
  }  else if (mediaType == 'video') {
    data.message = {
      videoUri: data.uri,
    }
  } else {
    data.message = {
      media: [{mediumUri: data.uri}],
    }
  }
  return data;
};

Send.setFailedUploading = (tempObj) => {
  tempObj.progress = 0;
  tempObj.status = 'Failed';
  tempObj.isFailedUploading = true;
  tempObj.date = new Date();
  // console.log('tempObj', tempObj);
  Send.upsertToCollection(tempObj);
};

/* -------------------------------------- UPLOADING MEDIA -------------------------------------- */

Send.uploadMedia = (data, mediaType, uploadURL, mediaPath, isAutoSaveMedia, callback) => {
  let formData = Send.getFormData(data, mediaType, uploadURL);
  Send.uploadHeaders(data, formData, mediaType, uploadURL, mediaPath, isAutoSaveMedia, (result) => {
    if (callback) {
      callback(result);
    }
  });
};

Send.getFormData = (data, mediaType, uploadURL) => {
  console.log("getFormData");
  let fileName = (data.fileName) ? data.fileName : data.uri.replace(/^.*[\\\/]/, '');
  let fileSize = data.fileSize;
  let mimeType = 'multipart/form-data';
  if ((Platform.OS === 'android')) {
    fileName = (mediaType == 'video') ? data.path.replace(/^.*[\\\/]/, '') : fileName;
    if (mediaType == 'photo') {
      mimeType = "image/jpeg";
    }
  }

  if (Platform.OS === 'android' && uploadURL != Send.uploadURL.mediaComment) {
    var formData = [];
    formData.push({name: 'file', filename: fileName, type: mimeType, data: RNFetchBlob.wrap(data.uri)});

    if (uploadURL == Send.uploadURL.mediaChat) {
      formData.push({name: 'sender', data: data.fromUserId});
      formData.push({name: 'recipient', data: data.toUserId});
      formData.push({name: '_id', data: data._id});
      formData.push({name: 'type', data: mediaType});
      formData.push({name: 'duration', data: `${data.duration}`});
      formData.push({name: 'fileSize', data: `${fileSize}`});
      if (mediaType == 'photo') {
        formData.push({name: 'height', data: `${data.height}`});
        formData.push({name: 'width', data: `${data.width}`});
      }
    }
  } else {
    var formData = new FormData();
    formData.append('file', {uri: data.uri, name: fileName, type: mimeType});
    if (uploadURL == Send.uploadURL.mediaChat) {
      formData.append('sender', data.fromUserId);
      formData.append('recipient', data.toUserId);
      formData.append('_id', data._id);
      formData.append('type', mediaType);
      formData.append('duration', `${data.duration}`);
      formData.append('fileSize', `${fileSize}`);
    } else if (uploadURL == Send.uploadURL.mediaComment) {
      formData.append('postId', data.postId);
      formData.append('sender', data.sender);
    }
  }

  return formData;
};

Send.uploadHeaders = (data, formData, mediaType, uploadURL, mediaPath, isAutoSaveMedia, callback) => {
  console.log("uploadHeaders");
  if (Platform.OS === 'android' && uploadURL != Send.uploadURL.mediaComment) {
    AsyncStorage.getItem('accessToken').then(token => {
      let headers = {
        Authorization : token,
        'Content-Type' : 'multipart/form-data',
      }
      if (uploadURL == Send.uploadURL.mediaChat) {
        headers['sender'] = data.fromUserId
        headers['recipient'] = data.toUserId
        headers['_id'] = data._id
        headers['duration'] = `${data.duration}`
      } else if (uploadURL == Send.uploadURL.profilePicture) {
        headers['userid'] = data.userId
      } else if (uploadURL == Send.uploadURL.groupImage) {
        headers['groupid'] = data.groupId
      }
      Send.requestUpload(headers, formData, mediaType, uploadURL, data, mediaPath, isAutoSaveMedia, (result) => {
        if (callback) {
          callback(result);
        }
      });
    });
  } else {
    Send.requestUploadIOS(data, formData, mediaType, uploadURL, mediaPath, isAutoSaveMedia, (result) => {
      if (callback) {
        callback(result);
      }
    });
  }
};

Send.requestUploadIOS = (data, formData, mediaType, uploadURL, mediaPath, isAutoSaveMedia, callback) => {
  const Download = require('./Download');
  var request = new XMLHttpRequest();
  request.open("POST", uploadURL, true);

  if (uploadURL == Send.uploadURL.mediaChat) {
    request.setRequestHeader('sender', data.fromUserId);
    request.setRequestHeader('recipient', data.toUserId);
    request.setRequestHeader('_id', data._id);
    request.setRequestHeader('duration', `${data.duration}`);
  } else if (uploadURL == Send.uploadURL.profilePicture) {
    request.setRequestHeader('userid', data.userId);
  } else if (uploadURL == Send.uploadURL.groupImage) {
    request.setRequestHeader('groupid', data.groupId);
  } else if (uploadURL == Send.uploadURL.mediaComment) {
    request.setRequestHeader('sender', data.sender);
  }

  request.onreadystatechange = function(status){
    if (request.readyState !== 4) {
      return;
    }

    if (request.readyState === 4) {
      console.log(request.response)
      if (uploadURL == Send.uploadURL.mediaChat) {
        if (isAutoSaveMedia) {
          // let fileName = (data.fileName) ? data.fileName : data.uri.replace(/^.*[\\\/]/, '');
          // Send.saveToKlikchatFolder(data._id, data.uri, fileName, data.toUserId, mediaPath);
          let mediaURL = request.response;
          let media = {
            mediaURL: mediaURL,
            messageId: data._id,
            otherUserId: data.toUserId
          }
          Download.downloadMediaFile(mediaPath, media, true);
        }
      }
      if (callback && (uploadURL != Send.uploadURL.mediaChat)) {
        callback(request.response);
      }
      console.log(`Send [${logTime()}]: Successfully uploaded media type ${mediaType}`);
    }
  }

  if (request.upload) {
    request.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        uploadProgress = event.loaded / event.total;
        if (uploadURL == Send.uploadURL.mediaChat) {
          data.progress = uploadProgress * 1.0;
          Send.upsertToCollection(data);
        }
      }
    };
    request.upload.onerror = (event) => {
      console.log('Upload failed');
      callback('Uploading failed.');
    };
    request.error = (err) => {
      console.log(`Send [${logTime()}]: Uploading error`, err)
      callback('Uploading failed.');
    };
  }
  request.send(formData);
}

Send.requestUpload = (headers, formData, mediaType, uploadURL, data, mediaPath, isAutoSaveMedia, callback) => {
  const Download = require('./Download');
  let responseText = null;
  RNFetchBlob.fetch('POST', uploadURL, headers, formData).uploadProgress({count: -1},(written, total) => {
    var uploadProgress = written / total;
    if (uploadURL == Send.uploadURL.mediaChat) {
      if (!responseText) {
        data.progress = uploadProgress * 1.0;
        // Send.upsertToCollection(data);
      }
    }
  }).then((resp) => {
    if (resp) {
      if (uploadURL == Send.uploadURL.mediaChat) {
        responseText = resp.text();
        if (isAutoSaveMedia) {
          let mediaURL = responseText;
          let media = {
            mediaURL: mediaURL,
            messageId: data._id,
            otherUserId: data.toUserId
          }
          Download.downloadMediaFile(mediaPath, media, true);
          // Send.saveToKlikchatFolder(data._id, data.uri, formData[0].filename, data.toUserId, mediaPath);
        }
      }
      if (callback && (uploadURL != Send.uploadURL.mediaChat)) {
        callback(resp.text());
      }
      console.log(`Send [${logTime()}]: Successfully uploaded media type ${mediaType}: ${resp.text()}`);
    }
  }).catch((err) => {
    if (uploadURL == Send.uploadURL.mediaChat) {
      Send.setFailedUploading(data);
    } else {
      callback('Uploading failed.');
    }
    console.log(`Send [${logTime()}]: Failed to upload media type ${mediaType}: `, err);
  });
};
/* -------------------------------------- HELPER METHODS -------------------------------------- */

Send.makeUniqueId = (fromUserId, toUserId) => {
  return fromUserId + '_' + toUserId + '_' + new Date().getTime() + '_' + Math.random().toString(36).substr(2, 100);
};

Send.upsertToCollection = (data) => {
  ddp.collections.message.upsert(data);
};
/* -------------------------------------- OTHER CALL METHODS -------------------------------------- */

Send.updateSeenMessages = (messages, loggedInUserId, callback) => {
  let msgsRcvd = [];

  for (let x = 0; x < messages.length; x++) {
    if (messages[x].FromUserFID !== loggedInUserId) {
      msgsRcvd.push(messages[x]);
    }
  }

  let unseen = [];
  for (let i = 0; i < msgsRcvd.length; i++) {
    if (_.isEmpty(msgsRcvd[i].Seen)) {
      unseen.push(msgsRcvd[i]._id);
    } else if (!(_.contains(_.pluck(msgsRcvd[i].Seen, 'userId'), loggedInUserId))) {
      unseen.push(msgsRcvd[i]._id);
    }
  }

  if (unseen.length != 0) {
    ddp.call('updateMessageSeen', [unseen]).then(result => {
      callback(result);
      console.log('result updateMessageSeen', result);
    }).catch(err => {
      console.log('updateMessageSeen Error: ', err);
    });
  }
}

Send.sendFriendRequest = (id, cb) => {
  ddp.call('addFriendRequest', [id])
  .then(result => {
    cb(result);
    console.log('Send ['+ logTime() +']: Sent friend request with id: ' + id);
  })
  .catch(err => {
    cb(err);
    console.log('Send ['+ logTime() +']: Failed to send friend request. Error: ',err);
  });
}

Send.acceptRequest = (id, cb) => {
  ddp.call('acceptFriendRequest', [id])
  .then(res => {
    console.log('Send ['+ logTime() +']: Accept friend with id: ' + id);
    cb(res)
  })
  .catch(err => {
    console.log('Send ['+ logTime() +']: Failed to accept friend. Error: ',err);
    cb(err)
  });
}

Send.storeCache = (filePath, mediaData) => {
  StoreCache.getCache(StoreCache.keys.media, (media) => {
    let mediaCache = {};
    let messageIds = {};
    messageIds[mediaData.messageId] = filePath;
    if (media) {
      mediaCache = media;
    }
    if (mediaCache[mediaData.otherUserId]) {
      let cache = mediaCache[mediaData.otherUserId];
      cache[mediaData.messageId] = filePath;
    } else {
      mediaCache[mediaData.otherUserId] = messageIds;
    }

    StoreCache.storeCache(StoreCache.keys.media, mediaCache, () => {

    });
  });
};

/* --------------------------------------  POST -------------------------------------- */

Send.post = (content, loggedInUserId, callback) => {
  // let uploadURL = Send.uploadURL.postTimeline;
  // let formData = Send.getFormData(data, 'post', uploadURL);
  // Send.uploadHeaders(data, formData, 'post', uploadURL, (result) => {
  //   if (callback) {
  //     callback(result);
  //   }
  // });
  var formData = new FormData();
  if (content.Image) {
    var filename = content.Image.uri.replace(/^.*[\\\/]/, '');
    formData.append('file', {uri: content.Image.uri, name: filename, type: "image/jpeg"});
  }
  if (content.Video) {
    var filename = content.Video.path.replace(/^.*[\\\/]/, '');
    var  uri = content.Video.uri;
    formData.append('file', {uri: uri, name: filename, type: 'video/3gp'});
  }
  formData.append('sender', loggedInUserId);
  (content.isProfilePicture) && formData.append('isProfilePicture', content.isProfilePicture);
  (content.Link) && formData.append('link', JSON.stringify(content.Link));
  (content.Text) && formData.append('text', content.Text);
  (content.Stickers) && formData.append('sticker', content.Stickers);

  var request = new XMLHttpRequest();
  var postURL = ddp.urlUploadPost;
  request.open("POST", postURL, true);
  request.setRequestHeader('sender', loggedInUserId);
  request.onreadystatechange = function(status){
    if (request.readyState !== 4) {
      return;
    }
  }

  if (request.upload) {
    request.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        uploadProgress = event.loaded / event.total;
        if (uploadProgress == 1) {
          callback();
        }
      }
    };
  }
  request.send(formData);
};

module.exports = Send;
