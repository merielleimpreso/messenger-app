import _ from 'underscore';
import ReactNative, {
  AsyncStorage
} from 'react-native';
import Send from './Send';
import StoreCache from './StoreCache';
import SoundEffects from '../../actions/SoundEffects';
let Message = {}
let CACHE_LIMIT = 1000;
var observeByUserId;
Message.subscribe = () => {
  return ddp.subscribe('messages', []);
}
Message.subscribeByUserId = (id) => {
  return ddp.subscribe('messagesByUserId', [id]);
}
Message.observeByUserId = (loggedInUser, userId, id, cb) => {
  Message.stopObserving();
  observeByUserId = ddp.collections.observe(() => {
    var collection = ddp.collections.message;
    if (collection) {
      let query = {};
      if (id.includes('group')) {
        query = {"toUserId": id};
        if (loggedInUser.groups[id]) {
          query = {"toUserId": id, "date": {$lte: loggedInUser.groups[id]}};
        }
      }
      else {
        // Check if user is already friends
        let contactIds = loggedInUser.contacts;
        let friendRequestReceivedIds = _.pluck(loggedInUser.friendRequestReceived, 'userId');
        query = {
          $or: [
            {"fromUserId": userId, "toUserId": id},
            {"fromUserId": id, "toUserId": userId, "isBlocked": false}
          ]
        };
      }
      var options = {sort: {dateUpdated: -1}};
      return collection.find(query, options);
    } else {
      return [];
    }
  });
  observeByUserId.subscribe((results) => {
    cb(results);
  });
  return observeByUserId;
}
Message.countPendingMessages = (loggedInUserId) => {
  let query = {
    fromUserId: loggedInUserId,
    status: 'Sending'
  };
  let notSentMessages = ddp.collections.message.find(query);
  return notSentMessages.length;
}
Message.playSound = () => {
  SoundEffects.playSound("sent");
}
Message.getFailedMessages = (messages, cachedMsgs, cb) => {
  let failedMessages = _.filter(messages, function(message){
    return message.status == 'Failed' && !_.has(message, 'seen');
  });
  let stillOnProgress = _.filter(cachedMsgs, function(message){
    return message.status == 'Sending' && message.progress < 1 && !_.has(message, 'seen');
  });
  cb(failedMessages, stillOnProgress);
}
Message.getRecent = (allMessages) => {
  let chatIds = _.unique(_.pluck(allMessages, 'ChatId'));
  var recentMessages = [];
  for (var i = 0; i < chatIds.length; i++) {
    var messages = _.where(allMessages, {ChatId: chatIds[i]});
    if (messages.length > 0) {
      var recentMessage =  _.last(_.sortBy(messages, 'CreateDate'));
      recentMessages.push(recentMessage);
    }
  }
  return recentMessages;
}
Message.upsertFailedMessages = (cached) => {
  // var messages = ddp.collections.message.find({});
  let failedMsgs = _.where(cached, {'status': 'Failed'});// _.difference(cached, messages);
  for (let i = 0; i < failedMsgs.length; i++) {
    ddp.collections.message.upsert(failedMsgs[i]);
  }
}
Message.storeCache = () => {
  let items = ddp.collections.message.find({});
  StoreCache.storeCache('messages', items);
}
Message.storeMediaCache = (filePath, mediaData) => {
  let message = ddp.collections.message.find({_id: mediaData.messageId});
  message[0]['mediaCache'] = filePath;
  ddp.collections.message.upsert(message);
}
Message.getCache = (userId, id, cb) => {
  StoreCache.getCache('messages', (result) => {
    let code1 = id.includes('group') ? id : userId + '_' + id;
    let code2 = id + '_' + userId;
    let items = _.filter(result, function(item){
      return (item._id.indexOf(code1) !== -1) || (item._id.indexOf(code2) !== -1);
    });
    cb(items);
  });
}
Message.saveGroupsUsers = (groupId, users, callback) => {
  StoreCache.getCache(StoreCache.keys.groups, (groups) => {
    let groupCache = {};
    if (groups) {
      groupCache = groups;
    }
    groupCache[groupId] = users;
    StoreCache.storeCache(StoreCache.keys.groups, groupCache, () => {
      if (callback) {
        callback();
      }
    });
  });
}
Message.getGroupUsers = (groupId, callback) => {
  StoreCache.getCache(StoreCache.keys.groups, (groups) => {
    var users = [];
    if (groups) {
      users = groups[groupId];
    }
    callback(users);
  });
}
/*------------------------------ UNUSED FUNCTIONS ------------------------------*/
Message.subscribeRecent = () => {
  return ddp.subscribe('messagesRecent', []);
}
Message.subscribeRecentWithLimit = (limit) => {
  return ddp.subscribe('messagesRecentWithLimit', [limit]);
}
Message.subscribeById = (chatId) => {
  return ddp.subscribe('messagesByChatId', [chatId]);
}
Message.subscribeByIdWithLimit = (chatId, limit) => {
  return ddp.subscribe('messagesByChatIdWithLimit', [chatId, limit]);
}
Message.subscribeByActualId = (id) => {
  return ddp.subscribe('messagesById', [id]);
}
Message.getMessageById = (loggedInUserId, id, allMessages) => {
  var chatId = '';
  if (id.includes('group_')) {
    chatId = id;
  } else {
    var chat = [loggedInUserId, id];
    var sortedChat = chat.sort();
    chatId = sortedChat[0] + '_' + sortedChat[1];
  }
  let messages = _.where(allMessages, {ChatId: chatId});
  return _.sortBy(messages, 'CreateDate').reverse();
}
Message.getItemById = (id) => {
  return ddp.collections.message.findOne({_id: id});
};
Message.stopObserving = () => {
  if (observeByUserId != null) {
    observeByUserId.dispose();
  }
};
module.exports = Message;