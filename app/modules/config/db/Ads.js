module.exports = [
  {
    image: require('./../../../images/ads/klikluckyyou.png'), 
    name: 'Klik Lucky You',
    link: 'https://play.google.com/store/apps/details?id=com.kliklabs.luckyyou2&hl=en', 
  },
  {
    image: require('./../../../images/ads/klikmarket.png'), 
    name: 'Klik Market',
    link: 'https://play.google.com/store/apps/details?id=com.kliklabs.market&hl=en'
  },
  {
    image: require('./../../../images/ads/klikrajavoucher.png'), 
    name: 'Klik Raja Voucher',
    link: 'https://play.google.com/store/apps/details?id=com.kliklabs.rajavoucher&hl=en'
  },
  {
    image: require('./../../../images/ads/kcicon.png'), 
    name: 'Klik Chat',
    link: 'https://play.google.com/store'
  },
  {
    image: require('./../../../images/ads/veggiesfriend.png'), 
    name: `Veggie's Friends`,
    link: 'https://play.google.com/store/apps/details?id=com.kliklabs.veggiefriends&hl=en'
  }
];