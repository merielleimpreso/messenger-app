import StoreCache from './StoreCache';
import _ from 'underscore';

let Chat = {}
var observer;

Chat.subscribe = () => {
  return ddp.subscribe('recent', []);
}

Chat.observeRecent = (user, cb) => {
  observer = ddp.collections.observe(() => {
    let collection = ddp.collections.recent;
    if (collection) {
      var options = {sort: {dateUpdated: -1}};
      return collection.find({}, options);
    } else {
      return [];
    }
  });
  observer.subscribe((results) => {
    cb(results);
  });
  return observer;
}

Chat.stopObserving = () => {
  if(observer != null) {
    observer.dispose();
  }
}

module.exports = Chat;