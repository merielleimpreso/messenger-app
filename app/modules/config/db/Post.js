import _ from 'underscore';
import ReactNative, {
  AsyncStorage
} from 'react-native';
import StoreCache from './StoreCache';

let Post = {};
var observer;

Post.subscribe = () => {
  return ddp.subscribe('post', []);
};

Post.subscribeWithLimit = (limit) => {
  // Post.cache();
  return ddp.subscribe('postWithLimit', [limit]);
}

Post.observe = (cb) => {
  observer = ddp.collections.observe(() => {
    let collection = ddp.collections.post;
    if (collection) {
      var options = {sort: {CreateDate: -1}};
      return collection.find({}, options);
    } else {
      return [];
    }
  });
  observer.subscribe((results) => {
    cb(results);
  });
  return observer;
};

Post.observeByPostId = (postId, cb) => {
  Post.cache();
  let observer = ddp.collections.observe(() => {
    let collection = ddp.collections.post;
    if (collection) {
      var post = collection.find({_id: postId});
      return post;
    } else {
      return [];
    }
  });

  observer.subscribe((results) => {
    cb(results);
  });
  return observer;
}

Post.storeCache = () => {
  let items = ddp.collections.post.find({});
  StoreCache.storeCache('posts', items);
}

Post.getCache = (cb) => {
  StoreCache.getCache('posts', (result) => {
    console.log('ito oh', result)
    cb(result);
  });
}

Post.cache = () => {
  let collection = ddp.collections.post;
  var postItems = collection.find({});
  AsyncStorage.setItem('post', JSON.stringify(postItems));
};

Post.useCache = (cb) => {
  AsyncStorage.getItem('post').then(result => {
    if (result) {
      var postItems = JSON.parse(result);
      for (var i = 0; i < postItems.length; i++) {
        ddp.collections.post.upsert(postItems[i]);
      }
    }
    cb();
  });
};

Post.stopObserving = () => {
  if (observer != null) {
    observer.dispose();
  }
}

module.exports = Post;
