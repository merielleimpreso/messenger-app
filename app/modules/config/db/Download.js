import React from 'react';
import {
  Platform,
  ToastAndroid
} from 'react-native';
import _ from 'underscore';
import { logTime } from '../../Helpers';
import FileReader from 'react-native-fs';
import Message from './Message';
import RNFetchBlob from 'react-native-fetch-blob';
import StoreCache from './StoreCache';

let Download = {};
let dirs = RNFetchBlob.fs.dirs;

Download.mediaTypePath = {
  audio: `/Klikchat/Audio/`,
  audioSent: `/Klikchat/Audio/Sent/`,
  image: `/Klikchat/Images/`,
  imageSent: `/Klikchat/Images/Sent/`,
  video: `/Klikchat/Videos/`,
  videoSent: `/Klikchat/Videos/Sent/`
}

Download.createDirectoryForKlikchat = () => {
  for (let i = 0; i < _.keys(Download.mediaTypePath).length; i++) {
    let keys = _.keys(Download.mediaTypePath);
    let key = keys[i];
    let trueKey = Download.mediaTypePath[key];
    Download.createDirectory(trueKey);
  }
}

Download.makeDirectoryPath = (mediaTypePath) => {
  let directoryPath = (Platform.OS === 'ios') ? FileReader.DocumentDirectoryPath : '/storage/emulated/0';
  return directoryPath + mediaTypePath;
}

Download.checkIfFileExist = (filePath, cb) => {
  FileReader.exists(filePath).then(result => {
    cb(result)
  }).catch(err => {
    cb(false);
    console.log('Download ['+ logTime() +']: Error', err);
  });
}

Download.downloadMediaFile = (mediaTypePath, mediaData, isForCache) => {
  let directoryPath = Download.makeDirectoryPath(mediaTypePath);
  
  Download.getFileOptions(directoryPath, mediaData, isForCache);
  // FileReader.readDir(directoryPath).then(result => {
  //   console.log('Download ['+ logTime() +']: Folder already exist', mediaTypePath);
  // }).catch(err => {
  //   if (err) {
  //     Download.createDirectory(directoryPath, mediaData, isForCache);
  //   }
  // });
};

Download.createDirectory = (mediaTypePath, mediaData, isForCache) => {
  let directoryPath = Download.makeDirectoryPath(mediaTypePath);

  FileReader.mkdir(directoryPath).then(()=> {
    console.log('Download ['+ logTime() +']: Successfully created folder', mediaTypePath)
    if (mediaData) {
      Download.getFileOptions(directoryPath, mediaData, isForCache);
    }
  }).catch(err => console.log('Download ['+ logTime() +'] mkdir Error: ', err));
}

Download.getFileOptions = (directoryPath, mediaData, isForCache) => {
  let fileName = mediaData.mediaURL.replace(/^.*[\\\/]/, '');
  let toFile = directoryPath + fileName;
  let downloadFileOptions = {
    fromUrl: mediaData.mediaURL,   
    toFile: toFile,
    background: true,
    progressDivider: 1,
  };
  Download.download(downloadFileOptions, mediaData, isForCache);
};

Download.storeCache = (filePath, mediaData) => {
  StoreCache.getCache(StoreCache.keys.media, (media) => {
    let mediaCache = {};
    let messageIds = {};
    messageIds[mediaData.messageId] = filePath;
    if (media) {
      mediaCache = media;
    }
    if (mediaCache[mediaData.otherUserId]) {
      let cache = mediaCache[mediaData.otherUserId];
      cache[mediaData.messageId] = filePath;
    } else {
      mediaCache[mediaData.otherUserId] = messageIds;
    }
    StoreCache.storeCache(StoreCache.keys.media, mediaCache, () => {
      
    });
  });
};

Download.download = (downloadFileOptions, mediaData, isForCache) => {
  if (!isForCache && Platform.OS === 'android') {
    ToastAndroid.show('Download started', ToastAndroid.SHORT);
  }
  console.log(`Download [${logTime()}] Started downloading: ${downloadFileOptions.fromUrl}`)

  RNFetchBlob.config({
    path : downloadFileOptions.toFile
  }).fetch('GET', downloadFileOptions.fromUrl, {
    //some headers ..
  }).then((res) => {
    if (res.respInfo.status == 200) {
      console.log(`Download [${logTime()}] Successfully saved to: ${res.path()}`)
      Download.storeCache(downloadFileOptions.toFile, mediaData);
      Message.storeMediaCache(downloadFileOptions.toFile, mediaData);
      if (!isForCache && Platform.OS === 'android') {
        ToastAndroid.show('Saved.', ToastAndroid.SHORT);
      }
    }
  }).catch(err => {
    console.log('Download ['+ logTime() +'] downloadFile Error: ', err)
    if (!isForCache && Platform.OS === 'android') {
      ToastAndroid.show('Something went wrong. Please try again.', ToastAndroid.SHORT);
    } 
  });
  // FileReader.downloadFile(downloadFileOptions)
  // .promise.then((response)=> {
  //   if (response.statusCode == 200) {
  //     console.log(`Download [${logTime()}] Successfully saved to: ${downloadFileOptions.toFile}`)
  //     Message.storeMediaCache(downloadFileOptions.toFile, mediaData);
  //     if (!isForCache && Platform.OS === 'android') {
  //       ToastAndroid.show('Saved.', ToastAndroid.SHORT);
  //     }
  //   }
  // }).catch(err => {
  //   console.log('Download ['+ logTime() +'] downloadFile Error: ', err)
  //   if (!isForCache && Platform.OS === 'android') {
  //     ToastAndroid.show('Something went wrong. Please try again.', ToastAndroid.SHORT);
  //   } 
  // });
};

module.exports = Download;