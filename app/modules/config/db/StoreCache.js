import React from 'react';
import _ from 'underscore';
import { AsyncStorage } from 'react-native';
import { Cache } from './../lib/cache';

let cache = new Cache({
    namespace: 'myapp',
    policy: {
        maxEntries: 50000
    },
    backend: AsyncStorage
});
let CACHE_LIMIT = 1000;
let StoreCache = {};

StoreCache.keys = {
  contacts : 'contacts',
  loggedInUser: 'loggedInUser',
  messages: 'messages',
  posts: 'posts',
  recent : 'recents',
  groups: 'groups',
  phoneContacts: 'phoneContacts',
  media: 'media',
  isAutoLocate: 'isAutoLocate',
  isAutoSaveMedia: 'isAutoSaveMedia'
};

StoreCache.storeCache = (key, value, callback) => {
  cache.setItem(key, value, function(err) {
    console.log('Saved ' + key + ' to cache');

    if (callback) {
      callback();
    }
  });
}

StoreCache.getCache = (key, cb) => {
  cache.getItem(key, function(err, value) {
    cb(value);
  });
}

StoreCache.hasPreviousSession = (cb) => {
  cache.getItem(StoreCache.keys.loggedInUser, function(err, loggedInUser) {
    let hasPreviousSession = false;
    if (loggedInUser) {
      ddp.collections.currentUser.upsert(loggedInUser);
      hasPreviousSession = true;
    }
    cb(hasPreviousSession);
  });
}

StoreCache.save = (key) => {
  let value = null;
  if (key == StoreCache.keys.contacts) {
    value = ddp.collections.contacts.find({}, {limit: CACHE_LIMIT});
  } else if (key == StoreCache.keys.loggedInUser) {
    value = ddp.collections.currentUser.findOne({});
  } else if (key == StoreCache.keys.recent) {
    value = ddp.collections.recent.find({}, {limit: CACHE_LIMIT});
  } else if (key == StoreCache.keys.posts) {
    value = ddp.collections.post.find({}, {limit: CACHE_LIMIT});
  }
  cache.setItem(key, value, function(err) {
    if (key == StoreCache.keys.loggedInUser) {
      console.log('StoreCache: Saved loggedInUser to cache');
    } else {
      if (value) {
        console.log('StoreCache: Saved ' + value.length + ' ' + key + ' to cache');
      }
    }
  });
}

StoreCache.removeCache = () => {
  for (let i = 0; i < _.keys(StoreCache.keys).length; i++) {
    let keys = _.keys(StoreCache.keys);
    let key = keys[i];
    let trueKey = StoreCache.keys[key];
    cache.removeItem(trueKey, function(err) {
      console.log('Remove ' + trueKey + ' from cache');
    });
  }
}

module.exports = StoreCache;
