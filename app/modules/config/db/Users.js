import _ from 'underscore';
import ReactNative, {
  AsyncStorage
} from 'react-native';
import StoreCache from './StoreCache';

let Users = {};
var observerContacts;
var observerCurrentUser;
var observerOtherUser;

Users.subscribeToCurrentUser = () => {
  return ddp.subscribe('currentUser', []);
}

Users.subscribeContacts = () => {
  return ddp.subscribe('contacts', []);
}

Users.subscribeToOtherUser = (userId) => {
  return ddp.subscribe('otherUser', [userId]);
}

Users.observeOtherUser = (userId, cb) => {
  observerOtherUser = ddp.collections.observe(() => {
    let collection = ddp.collections.contacts;
    if (collection) {
      var otherUser = collection.findOne({_id: userId});
      return otherUser;
    } else {
      return [];
    }
  });
  observerOtherUser.subscribe((results) => {
    cb(results);
  });
  return observerOtherUser;
}

Users.observeContacts = (user, cb) => {
  let contactIds = user.contacts;
  //let friendRequestReceived = _.reject(user.friendRequestReceived, function(fr) { return fr.date == null });
  let friendRequestReceived = user.friendRequestReceived;
  let friendRequestReceivedIds =  _.pluck(friendRequestReceived, 'userId');
  let groupIds = Users.getActiveGroupIds(user.groups);
  let allIds = _.union([user._id], contactIds, friendRequestReceivedIds, groupIds);
  observerContacts = ddp.collections.observe(() => {
    let collection = ddp.collections.contacts;
    if (collection) {
      var query = {_id: {$in: allIds}};
      var options = {sort: {categoryOrder: 1, name: 1}};
      var contacts = collection.find(query, options);
      return contacts;
    } else {
      return [];
    }
  });
  observerContacts.subscribe((results) => {
    cb(results);
  });
  return observerContacts;
}

Users.getActiveGroupIds = (userGroups) => {
  let groups = [];
  let groupIds = _.keys(userGroups);
  for (let i = 0; i < groupIds.length; i++) {
    let id = groupIds[i];
    if (!userGroups[id]) {
      groups.push(id)
    }
  }
  return groups;
}

Users.updateChat = (contacts) => {
  for (let i = 0; i < contacts.length; i++) {
    let id = contacts[i]._id;
    let chat = ddp.collections.recent.findOne({"user._id": id});
    if (chat) {
      if (chat.user.name != contacts[i].name) {
        chat.user.name = contacts[i].name;
        ddp.collections.recent.upsert(chat);
      } else if (chat.user.image != contacts[i].image) {
        chat.user.image = contacts[i].image;
        ddp.collections.recent.upsert(chat);
      }
    }
  }
}

Users.observeCurrentUser = (cb) => {
  observerCurrentUser = ddp.collections.observe(() => {
    let collection = ddp.collections.currentUser;
    if (collection) {
      ddp.call('checkForContacts', []).catch((e) => { console.log(e); });
      var currentUser = collection.findOne({});
      return currentUser;
    } else {
      return [];
    }
  });
  observerCurrentUser.subscribe((results) => {
    cb(results);
  });
  return observerCurrentUser;
}

Users.getCurrentUser = () => {
  let collection = ddp.collections.currentUser;
  return collection.findOne({});
}

Users.getAllFriends = () => {
  return ddp.collections.contacts.find({category: 'Friend'});
}

Users.getAllFriendRequests = () => {
  let currentUser = Users.getCurrentUser();
  let ignoredRequestIds = _.pluck(_.reject(currentUser.friendRequestReceived, function(fr) { return fr.date == null }), 'userId');
  let query = {_id: {$in: ignoredRequestIds}, category: 'Friend Request'};
  let friendRequestReceived = ddp.collections.contacts.find(query);
  return friendRequestReceived;
}

Users.getItemById = (id, cb) => {
  return ddp.collections.contacts.findOne({_id: id});
}

Users.stopObserving = () => {
  if(observerContacts != null) {
    observerContacts.dispose();
  }
  if(observerCurrentUser != null) {
    observerCurrentUser.dispose();
  }
  if (observerOtherUser != null) {
    observerOtherUser.dispose();
  }
}

Users.stopObservingOtherUser = () => {
  if (observerOtherUser != null) {
    observerOtherUser.dispose();
  }
}

module.exports = Users;
