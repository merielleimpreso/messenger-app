/*
Author: ???
Date Created: 2016-03-??
Date Updated: 2016-03-??
Description: This connects to the server.
Used In: index.android.js
Changelog:
update: 2016-05-26 (by Terence)
  -autoReconnect to False
  */
import React from 'react-native';
let { AsyncStorage } = React;
import DDPClient from "ddp-client";
import _ from 'underscore';
import { capitalizeWords, toNameCase, getSettings} from './../Helpers.js';
import StoreCache from './db/StoreCache';
let GLOBAL = require('./Globals.js');
let host = GLOBAL.SERVER_HOST;
let port = GLOBAL.SERVER_PORT;
let mediaPort = GLOBAL.MEDIA_SERVER_PORT
var closeCallBack;
//host : "messenger-server-app.herokuapp.com",
//port : 80,
// let host = "216.244.94.98";
// let port = 5002;
let ddpClient = new DDPClient({
  // All properties optional, defaults shown
  // host : "YOUR_IP_ADDRESS",
  //host : "messenger-server-app.herokuapp.com",
  // host : "messenger-server-app.herokuapp.com",
  // port : 80,
  host : host,
  port : port,
  ssl : false,
  autoReconnect : true,
  autoReconnectTimer : 500,
  maintainCollections : true,
  ddpVersion : '1',
  socketConstructor: WebSocket,
  // Use a full url instead of a set of `host`, `port` and `ssl`
  // url: 'wss://example.com/websocket'
  // socketConstructor: WebSocket // Another constructor to create new WebSockets
});
ddp = {};
ddp.host = host;
ddp.port = port;
ddp.urlUpload = `http://${ddp.host}:${ddp.port}/messages/file/upload`;
ddp.urlUploadGroup = 'http://'+ddp.host+':'+ddp.port+'/uploadGroup';
ddp.urlUploadComment = 'http://'+ddp.host+':'+ddp.port+'/timeline/comment/image';
ddp.urlUploadPost = 'http://'+ddp.host+':'+ddp.port+'/timeline/post';
ddp.urlUploadProfileAvatar = `http://${ddp.host}:${ddp.port}/user/upload/avatar`;
ddp.urlUploadGroupImage = `http://${ddp.host}:${ddp.port}/group/upload/avatar`;
ddp.mediaServerUrl = GLOBAL.S3_ENDPOINT;

ddp.collections = ddpClient.collections;
ddp.keyboardSpace = 0;
ddp.connected = true;
ddp.isLoggedIn = false;
ddp.collections.addCollection('stickers');
ddp.collections.addCollection('message');
ddp.collections.addCollection('chat');
ddp.collections.addCollection('contacts');
ddp.collections.addCollection('currentUser');
ddp.collections.addCollection('post');
ddp.collections.addCollection('recent');
// Initialize a connection with the server
ddp.initialize = function () {
  // ddpClient.host = getSettings().host;
  // ddpClient.port = getSettings().port;
  return new Promise(function(resolve, reject) {
  //   getSettings().then((result)=>{
  //     if (result) {
  //       var obj = JSON.parse(result);
  //       ddpClient.host = obj.host ? obj.host : '103.5.50.154';
  //       ddpClient.port = obj.port ? parseInt(obj.port) : 80;
  //       ddp.host = obj.host;
  //       ddp.port = parseInt(obj.port);
  //       ddp.urlUpload = 'http://'+ddp.host+':'+ddp.port+'/upload';
  //       ddp.urlUploadGroup = 'http://'+ddp.host+':'+ddp.port+'/uploadGroup';
  //     }
      ddpClient.connect(function(error, wasReconnect) {
        // console.log('ERR', error, wasReconnect);
        // If autoReconnect is true, this back will be invoked each time
        // a server connection is re-established
        if (error) {
          console.log('DDP connection error!');
          ddp.connected = false;
          reject(error);
        }
        if (wasReconnect)   {
          ddp.connected = true;
          console.log('Reestablishment of a connection.');
        }
        resolve(true);
      });
    // }); //-- getSettings
  });
};
// Method to close the ddp connection
ddp.close = function() {
  if (ddpClient !=null && ddpClient != undefined) {
      return ddpClient.close();
  } else {
    return 0;
  }
};
// Promised based subscription
ddp.subscribe = function(pubName, params) {
  params = params || undefined;
  if (params && !_.isArray(params)) {
    console.warn('Params must be passed as an array to ddp.subscribe');
  }
  return new Promise(function(resolve, reject) {
    console.log("subscribed: ", pubName);
    ddpClient.subscribe(pubName, params, function () {
      resolve(true);
    });
  });
};
// Observe method for collections
ddp.observe = function(name) {
  //return ddpClient.observe(collection);
  var observer = ddpClient.observe(name);
  observer.added = function(id) {
    console.log("[ADDED] to " + observer.name + ":  " + id);
  };
  observer.changed = function(id, oldFields, clearedFields, newFields) {
    console.log("[CHANGED] in " + observer.name + ":  " + id);
    console.log("[CHANGED] old field values: ", oldFields);
    console.log("[CHANGED] cleared fields: ", clearedFields);
    console.log("[CHANGED] new fields: ", newFields);
  };
  observer.removed = function(id, oldValue) {
    console.log("[REMOVED] in " + observer.name + ":  " + id);
    console.log("[REMOVED] previous value: ", oldValue);
  };
  observer.stop();
  return observer;
};
ddp.call = function(methodName, params) {
  //console.warn("Called ddp Method: "+methodName);
  params = params || undefined;
  if (params && !_.isArray(params)) {
    console.warn('Params must be passed as an array to ddp.call');
  }
  return new Promise(function(resolve, reject) {
    ddpClient.call(methodName, params,
      function (err, result) { // callback which returns the method call results
        if (result) {
          resolve(result);
        } else {
          reject(err);
        }
      },
      function () { // callback which fires when server has finished
      }
    );
  });
};
// Signup method
ddp.signUpWithUsername = (username, password, email, firstName, lastName, cb) => {
  let name = capitalizeWords(firstName + " " + lastName);
  let params = {
    username: username,
    password: password,
    email: email,
    profile: {
      name: name,
      verificationCode: '',
      isVerified: false
    }
  };
  return ddpClient.call('createUser', [params], cb);
};
// Login method with username
ddp.loginWithUsername = (username, password, cb) => {
  password = 'ZsNqJu^5K-Hc99fQHFV$JQr+MN@uc5JZ*yLy9FWy';
  let params = {
    user: {
      username: username
    },
    password: password
  };
  ddpClient.call("login", [params], cb)
};
// Login method with token
ddp.loginWithToken = (loginToken, cb) => {
  let params = { resume: loginToken };
  ddpClient.call("login", [params], cb);
};
// Method for saving login token, get result from login methods
ddp.onAuthResponse = (err, res) => {
  if (res) {
    let { id, token, tokenExpires } = res;
    AsyncStorage.setItem('userId', id.toString());
    AsyncStorage.setItem('loginToken', token.toString());
    AsyncStorage.setItem('loginTokenExpires', tokenExpires.toString());
    ddp.call('currentUser', [])
    .then(result => {
        ddp.user = result;
        AsyncStorage.setItem('currentUser', JSON.stringify(result));
    });
  }
  // else {
  //   AsyncStorage.multiRemove(['userId', 'loginToken', 'loginTokenExpires']);
  // }
};
// Method for logging out
ddp.logout = (cb) => {
  ddp.collections.chat.remove();
  ddp.collections.contacts.remove();
  ddp.collections.currentUser.remove();
  ddp.collections.message.remove();
  ddp.collections.post.remove();
  ddp.collections.recent.remove();
  AsyncStorage.getAllKeys((err, keys) => {
    AsyncStorage.multiRemove(keys).then((res) => {
      StoreCache.removeCache();
      ddpClient.call("logout", [], cb);
    });
  });
}
ddp.setCloseCallBack = (cb) => {
  console.log("ddp.js set closeCallBack");
  closeCallBack = cb;
  ddpClient.setCloseCallBack(ddp.closeCallBack);
}
ddp.closeCallBack = () => {
  if(closeCallBack!=null) {
      closeCallBack();
  }
}
ddp.setForceCloseCallback = (cb) => {
  console.log("ddp.js setForceCloseCallback");
  ddpClient.setForceCloseCallback(cb);
}
module.exports = ddp;
