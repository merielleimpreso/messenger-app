/*

Author: Merielle Impreso
Date Created: 2016-05-10
Date Updated: NA
Description: This module is displayed when the app cannot connect to server.
  Used index.ios.js, LoggedOut.ios.js

update: 2016-05-17 (Merielle I.)
  - Added text as props to have flexible messages
update: 2016-06-09 (Merielle I.)
  - Added color theme
update: 2016-07-21 (Merielle I.)
  - Change ES5 code format to ES6

*/

'use strict';

import React, {
  Component,
} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
var styles = StyleSheet.create(require('./../styles/styles_common'));
import { toNameCase, getSettings } from './Helpers.js';
var GLOBAL = require('./config/Globals.js');
var timeout;

class ScreenFailedToConnect extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isDebugMode: false
    }
    this.onButtonPressIn = this.onButtonPressIn.bind(this);
    this.onButtonPressOut = this.onButtonPressOut.bind(this);
    this.goToDebug = this.goToDebug.bind(this);
  }

  componentWillMount(){
    this._isMounted = true;
    /*getSettings().then((result)=>{
      if(this._isMounted){
        this.setState({
          settings: JSON.parse(result)
        });
      }
    });*/
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onButtonPressIn() {
    timeout = setTimeout(() => this.goToDebug(), 5000)
  }

  onButtonPressOut() {
    clearInterval(timeout)
  }

  goToDebug() {
    console.log('going to debug');
    if(this._isMounted){
      this.setState({
        isDebugMode: true
      });
    }
  }

  render() {
    var renderReloadButton = <View />;
    var color = this.props.color;
    if (this.state.isDebugMode) {
      return <Debug color={color} getTheme={this.getTheme} settings={this.state.settings} navigator={false}/>
    }
    if (this.props.reload) {
      renderReloadButton = (
        <KlikFonts
         name="refresh"
         size={35}
         onPress={() => this.props.reload()}
         style={styles.reload}/>
      );
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity
        onPressIn={() => {this.onButtonPressIn()}} onPressOut={() => {this.onButtonPressOut()}}>
          <Text style={styles.loginButtonText}> {this.props.text} </Text>
        </TouchableOpacity>
        {renderReloadButton}
      </View>
    )
  }
}

module.exports = ScreenFailedToConnect;
