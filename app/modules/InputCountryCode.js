'use strict';
import React, {
  Component,
} from 'react';
import {
  ActivityIndicator,
  InteractionManager,
  ListView,
  Platform,
  StyleSheet,
  ToolbarAndroid,
  TouchableHighlight,
  Text,
  View
} from 'react-native';
import _ from 'underscore';
import ButtonIcon from './common/ButtonIcon';
import Countries from './../modules/config/db/Countries';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import NavigationBar from 'react-native-navbar';
import SearchBar from './SearchBar';
import Navigation from './actions/Navigation';
import Toolbar from './common/Toolbar';


const styles = StyleSheet.create(require('./../styles/styles_common.js'));
const toolbarStyle = StyleSheet.create(require('./../styles/styles.main.js'));
const GLOBAL = require('./config/Globals.js');

var hasPopped = false;
class InputCountryCode extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isFirstLoad: true,
      isFinishedScroll: false,
      isLoading: true,
      selectedPositionY: 0,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
      }),
    }
    this.renderLoading = this.renderLoading.bind(this);
    this.renderNavigator = this.renderNavigator.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      if (this._isMounted) {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getCategoryMap(Countries.country)),
          isLoading: false
        });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedPositionY != this.state.selectedPositionY && this.state.isFirstLoad) {
      this.refs.listview.scrollTo({x:0, y:this.state.selectedPositionY, animated:true});
      if (this._isMounted) {
        this.setState({
          isFirstLoad: false,
        })
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getCategoryMap(countries) {
    _.each(_.values(countries), function(c) {
      c['category'] = c.name.charAt(0).toUpperCase();
    });
    var categoryMap = {};
    countries.forEach(function(item) {
      if (!categoryMap[item.category]) {
        categoryMap[item.category] = [];
      }
      categoryMap[item.category].push(item);
    });
    return categoryMap;
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.containerNoResults, {backgroundColor:color.CHAT_BG}]}>
        <Toolbar title={'Select Country'} withBackButton={true} {...this.props}/>
        <SearchBar
         theme={this.props.theme}
         onSearchChange={this.onSearchChange}
         placeholder='Enter country name'
         isLoading={false}
         onFocus={() => this.refs.listview && this.refs.listview.getScrollResponder().scrollTo({ x: 0, y: 0 })}/>
       {this.renderLoading()}
       <ListView ref="listview"
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        renderSectionHeader={this.renderSectionHeader}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={true}
        initialListSize={this.state.dataSource.getRowCount()}
       />
      </View>
    )
  }

  renderNavigator() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (Platform.OS === 'ios') {
      return (
        <NavigationBar
         tintColor={color.THEME}
         title={{ title: 'Select Country', tintColor: color.BUTTON_TEXT}}
         leftButton={<ButtonIcon icon='back' color={color} onPress={this.onPressButtonBack} size={25}/>}
        />
      );
    } else {
      return (
        <ToolbarAndroid
          elevation={5}
          title={'Select Country'}
          titleColor={color.BUTTON_TEXT}
          style={[toolbarStyle.topContainer, {backgroundColor: color.THEME}]}
        />
      );
    }
  }

  renderLoading() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    if (this.state.isLoading) {
      return (
        <ActivityIndicator animating={this.state.isLoading}
         color={color.BUTTON}
         style={[styles.searchBarSpinner, {alignSelf:'center'}]}/>
      );
    } else {
      return null;
    }
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    var selectedView = null;
    if (data.name == this.props.selectedCountry) {
      selectedView = <KlikFonts name='check' color={color.BUTTON} style={[styles.navbarButton, {alignSelf:'flex-end', marginRight:10}]} size={25}/>
    }
    return (
      <TouchableHighlight onPress={() => Navigation.onPress(() => this.onPressRow(data))} underlayColor={color.HIGHLIGHT}
       onLayout={(event) => {
        if (data.name == this.props.selectedCountry) {
          if (this._isMounted) {
            this.setState({
              isFinishedScroll: true,
              selectedPositionY: event.nativeEvent.layout.y - (event.nativeEvent.layout.height/2)
            });
          }
        }
      }}>
        <View>
          <View style={{flexDirection:'row', alignItems:'stretch'}}>
            <Text style={{color:color.TEXT_DARK, margin:10, marginLeft:20, flex:1}}>{data.name}</Text>
            {selectedView}
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  renderSectionHeader(sectionData, category) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={[styles.contactsSectionHeader, {borderTopColor:color.INACTIVE_BUTTON, backgroundColor:color.CHAT_BG}]}>
        <Text style={{flex:1, fontSize:13, color:color.TEXT_LIGHT, fontWeight:'500', margin:5, marginRight:10, alignSelf:'center'}}>{category}</Text>
      </View>
    );
  }

  onPressButtonBack() {
    if(!hasPopped)
    {
      this.hasPopped = true;
      requestAnimationFrame(() => {
        this.props.navigator.pop(0);
      });
    }

  }

  onSearchChange(searchText) {
    var countries = _.filter(Countries.country, function(country){
      return (country.name).toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });
    if (this._isMounted) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRowsAndSections(this.getCategoryMap(countries))
      });
    }
  }

  onPressRow(data) {
    if (this.state.isFinishedScroll && !hasPopped) {
      this.hasPopped = true;
      this.props.setSelectedCountry(data);
      this.props.navigator.pop(0);
    }
  }

}

module.exports = InputCountryCode;
