/**
Author:           Terence John Lampasa
Date Created:     2016-04-29
Date Updated:     2016-05-19
Descrption:       This page shows device's gps position.
Return:


Changelog:

update: 2016-05-27 By Terence
  -Added information to error Alert
  -forced to pop() after error alert.
  -increased location request timeout from 20000 to 50000
update: 2016-06-22 By Terence
  -Fixed Map Loading screen.
*/

'use strict';

import React, {
  Component,
} from 'react';

//Built in packages
import {
  Alert,
  InteractionManager,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

//Local packages
import {jsonToArray, toNameCase, toPhoneNumber, toLowerCase} from './Helpers.js';
import ButtonIcon from './common/ButtonIcon';
import Toolbar from './common/Toolbar';
import MapView from 'react-native-maps';
import NavigationBar from 'react-native-navbar';
var GLOBAL = require('./config/Globals.js');
var ProgressBar = require('ActivityIndicator');

class Map extends Component{
  // watchID: ?number = null;

  // Set initial state
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.001,
        longitudeDelta: 0.001,
      },
      myPosition: {
        latitude: 0,
        longitude: 0
      },
      hasCalibrated: false,
    };
    this.watchID;
    this.goBack = this.goBack.bind(this);
  }

  onRegionChange(region) {
    //console.warn("Region Changed");
    //this.setState({ region: region });
  }


  //get the data before rendering
  componentDidMount(){
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getLocation();
    });
  }

  // Put flag if component is unmounted.
  componentWillUnmount() {
    this._isMounted = false;
    navigator.geolocation.clearWatch(this.watchID);
    navigator.geolocation.stopObserving();
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      //var initialPosition = JSON.stringify(position);
      if (this._isMounted) {
        this.setState({
          hasCalibrated: true,
          region: {latitude: position.coords.latitude, longitude: position.coords.longitude, latitudeDelta: 0.001, longitudeDelta: 0.001},
          myPosition: {latitude: position.coords.latitude, longitude: position.coords.longitude}
        });
      }
    },(error) => {
      if (this._isMounted) {
        Alert.alert("Oops",error + ". Try again later.");
        // this.props.navigator.pop();
      }
    },{enableHighAccuracy: true, timeout: 50000, maximumAge: 1000});

    this.watchID = navigator.geolocation.watchPosition((position) => {
      //var lastPosition = JSON.stringify(position);
      if (this._isMounted) {
        this.setState({
          hasCalibrated: true,
          region: {latitude: position.coords.latitude, longitude: position.coords.longitude, latitudeDelta: 0.001, longitudeDelta: 0.001},
          myPosition: {latitude: position.coords.latitude, longitude: position.coords.longitude}
        });
      }
    });
  }

  // Display
  render() {
    let color = this.props.color;
    let map = null;
    let button = null;

    if (this.state.hasCalibrated) {
      map = (
        <MapView
          region={this.state.region}
          onRegionChange={this.onRegionChange}
          style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}>
          <MapView.Marker
            coordinate={this.state.myPosition}
            title={"You"}
            description={"You're here"}
          />
        </MapView>
      );
      button = (
        <View
        style={{ position: 'absolute',justifyContent: 'center', left: 0, right: 0, bottom: 30, height: 30}}>
          <TouchableHighlight
            onPress={() => this.sendLocation()}
            underlayColor={'#00000000'}
             style={{alignSelf: 'center', height: 30, width: 140}}>
            <Text style={{textAlign: 'center', height: 30}}>
              SEND LOCATION
            </Text>
          </TouchableHighlight>
        </View>

      );
    } else {
      map = <Loading color={color}/>;
    }

    return (
      <View style={styles.container}>
        <Toolbar title={'Maps'} withBackButton={true} {...this.props}/>
        <View style={{flex: 1, alignItems: 'center'}}>
          {map}
          {button}
        </View>
      </View>
    );
  }

  sendLocation() {
    if(this.state.hasCalibrated) {
      this.props.sendLocation(this.state.myPosition);
      this.props.navigator.pop();
    } else {//For testing purposes
      this.props.setMap({latitude: 37.785834, longitude: -122.406417});
      this.props.navigator.pop();
    }
  }

  goBack() {
    this.props.navigator.pop()
  }
};

// Display for empty records
class Loading extends Component{
  render() {
    let color = this.props.color;
    var text = 'LOADING';
    if (this.props.filter) {
      text = `No results for "${this.props.filter}"`;
    } else if (!this.props.isLoading) {

      text = 'Connecting to Google Maps..';
    }

    return (
      <View style={{alignItems: 'center', marginBottom: 80, flex: 1, justifyContent: 'center'}}>
      <ProgressBar styleAttr="Inverse" color={color.BUTTON} />
        <Text style={{marginTop: 20, color: color.TEXT_DARK}}>{text}</Text>
      </View>
    );
  }
};

var styles = StyleSheet.create(require('./../styles/styles.main'));
module.exports = Map;
