/**
Author: John
This js file contains methods that we can use globally.
dont repeat yourself
how to use:
  - import {jsonToArray} from './path/to/Helpers.js';
  - or
  - import {jsonToArray, toNameCase} from './path/to/Helpers.js';
  - then call just call it on your class / component
  Ex: jsonArray(array)

update: 2016-06-28 by Merielle I.
  - added secondsToTime function
update: 2016-06-29 by Merielle I.
  - added dateToTime function

 */

import React, {
  Component,
} from 'react';

import {
  ActivityIndicator,
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import _ from 'underscore';
import FileReader from 'react-native-fs';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import CryptoJS from "crypto-js";
import moment from "moment";
import GLOBAL from './config/Globals.js';
import SystemAccessor from './SystemAccessor';
import Download from './config/db/Download.js';
//  const ImagePickerManager = require('NativeModules').ImagePickerManager;
var ImagePickerManager = require('react-native-image-picker');
const styles = StyleSheet.create(require('./../styles/styles_common'));
const fileSizeLimit = 25165824;

// Convert json collection to javascript array.
export function jsonToArray(jsonArray){
    var array = [];
    for (var index in jsonArray) {
      array.push(jsonArray[index]);
    }
    return array;
}

// Capitalize first letter of each word.
export function toNameCase(str) {
  if (str != null && str != '') {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  } else {
    return '';
  }

}

export function getItemFromStorage(string){

  return AsyncStorage.getItem(string);

}

export function getObjectFromPromise(promise){
  var object;
  promise.then((obj) => {
    if(obj)
    {
      object = JSON.parse(obj);
    }
  }).done();

  return object;
}

export function processSettings(){
  var encyptedFilename = CryptoJS.HmacSHA1('app_settings', GLOBAL.APP_SECRET).toString();
  var path = FileReader.CachesDirectoryPath + '/' + encyptedFilename;
  var settings = FileReader.readFile(path, 'utf8').then(result => {
    return result;
  });
  return settings;
}
export function getSettings() {
  var promise = processSettings();
  return promise;
}

export function getEncyptedFile(from, to){
  var string = from + "-" +  to;
  var encyptedFilename = CryptoJS.HmacSHA1(string, GLOBAL.APP_SECRET).toString();
  var path = FileReader.CachesDirectoryPath + '/' + encyptedFilename;
  return path;
}

// Random string generator
//Usage:
//randomString(10);        // "4Z8iNQag9v"
//randomString(10, "A");   // "aUkZuHNcWw"
//randomString(10, "N");   // "9055739230"
export function randomString(len, an){
    an = an&&an.toLowerCase();
    var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
    // for(;i++<len;){
    //   var r = Math.random()*(max-min)+min <<0;
    //   str += String.fromCharCode(r+=r>9?r<36?55:61:48);
    // }
    return str;
}

export function toPhoneNumber(str)
{
  return str.replace(/\D\s*/g, "");
}

export function toLowerCase(str)
{
  return str.toLowerCase();
}

// Convert seconds to time format
export function secondsToTime(secs) {
  var hr = Math.floor(secs / 3600);
  var min = Math.floor((secs - (hr * 3600))/60);
  var sec = Math.ceil(secs - (hr * 3600) - (min * 60));
  var hour = (hr > 0) ? (hr + ':') : '';
  if (min < 10) min = '0' + min;
  if (sec < 10) sec = '0' + sec;
  return hour + min + ':' + sec;
}

// Convert date to time format
export function dateToTime(date, timeFormat) {
  var date = new Date(date);
  var formattedDate = moment(date).format('LT');
  if (timeFormat != '12') {
    formattedDate = moment(formattedDate, 'hh:mm A').format('HH:mm');
  };
  return formattedDate;
}

export function dynamicDateToTime(date, timeFormat) {
  var date = new Date(date);
  var dateToday = new Date();
  var formattedDate = moment(date).format('LT');
  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


  var dateDifference = (dateToday.getDate() - date.getDate());
  var yearDifference = (dateToday.getFullYear() - date.getFullYear());


  if(dateDifference <= 0 && yearDifference <= 0 )//current day of current year
  {
    if (timeFormat != '12') {
      formattedDate = moment(formattedDate, 'hh:mm A').format('HH:mm');
    }
  }
  else if(dateDifference == 1  && yearDifference <= 0)//yesterday of current year
  {
    formattedDate = 'yesterday';
  }
  else if(yearDifference <= 0)//old message within this year
  {
    var day = date.getDate();
    var month = date.getMonth() + 1;

    if(day < 10)
    {
        day = "0" + day;
    }

    if(month < 10)
    {
      month = "0" + month;
    }

    formattedDate = month + "/" + day;
  }
  else if(yearDifference > 0)//year/s old message
  {
    formattedDate = moment(date).format('L');
  }

  return formattedDate;
}

// Convert date to time format
export function timelineTimeOrDate(dateStr, format='MMMM D YYYY, h:mm a') {
  var date = new Date(dateStr);
  var today = new Date();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var meridiem = ' AM';

  var dateString = date.getFullYear() + '' + date.getMonth() + '' + date.getDay();
  var todayString = today.getFullYear() + '' + today.getMonth() + '' + today.getDay();
  var dateNumber = date.getFullYear() + date.getMonth() + date.getDay();
  var todayNumber = today.getFullYear() + today.getMonth() + today.getDay();
  if (dateString !== todayString) {
    if ((todayNumber-1) === (dateNumber)) {
      return 'Yesterday '+dateToTime(dateStr);
    }
    else{
      return moment(dateStr).format(format);
    }
  }
  else{
    return postDateDisplay(dateStr);
  }
}

export function logTime() {
  var date = new Date();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var second = date.getSeconds();
  var meridiem = ' AM';

  if (hour > 12) {
    hour = hour - 12;
    meridiem = ' PM';
  } else if (hour == 0) {
    hour = 12;
  }

  if (minute < 10) {
    minute = '0' + minute.toString();
  }

  if (second < 10) {
    second = '0' + second.toString();
  }
  return hour + ':' + minute + ':' + second + meridiem;
}

// Convert date to post date-time display
export function postDateDisplay(dateStr) {
  return moment(dateStr).fromNow();

}

// Helper method for consoleLog
export function consoleLog(message) {
  console.log(message);
}

// Helper method for adding media server to image path
export function getMediaURL(data) {

  if(data.indexOf('avatar/') >= 0 &&  data.indexOf('/avatar') < 0 ) {
    data = "/"+data;
  }
 if (data.indexOf('http') !== -1 || data.indexOf('data') !== -1 || data.indexOf('://') !== -1) {
   return data;
 } else {
   return `${ddp.mediaServerUrl}${data}`;
 }
}

// Helper method for display name
export function getDisplayName(data) {
  return (data.profile.displayName && data.profile.displayName != ' ')
          ? toNameCase(data.profile.displayName) :
            (!_.isEmpty(data.profile.firstName))
            ? toNameCase(data.profile.name):
              toNameCase(data.username);
}

export function capitalizeWords(str) {
  if (str) {
    var pieces = str.split(" ");
    for ( var i = 0; i < pieces.length; i++ ) {
      var j = pieces[i].charAt(0).toUpperCase();
      pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
  } else {
    return '';
  }
}
/* --------------------------- HELPER METHODS USED BY FOR IMAGE PICKER --------------------------- */

// Helper method that returns the media options
export function getMediaOptions(media) {
  var photoOptions = {
    cameraType: 'back',
    mediaType: 'photo',
    maxWidth: 800,
    maxHeight: 800,
    quality: 1,
    allowsEditing: false,
    noData: (media == 'photoWithData') ? false : true
  }
  var videoOptions = {
    cameraType: 'back',
    mediaType: 'video',
    videoQuality: 'low',
    durationLimit: 10,
    allowsEditing: false,
    storageOptions: {
      skipBackup: true,
      cameraRoll: false,
      waitUntilSaved: true
    }
  }
  var options = (media == 'video') ? videoOptions : photoOptions;
  return options;
}

export function checkFileSize(path, callback) {
  FileReader.stat(path).then((result)=>{
      console.log("Old video size: ", result.size);

    if (result.size <= fileSizeLimit) {
      console.log("File within limits.");
      callback('passed');
    } else {
      console.log("File too big.");
      callback("File too big");
    }
  }).catch((err) => {
    console.log("File does not exist", err);
    callback("Something went wrong. Please try again.");
  });
}

export function launchCamera(media, callback, compressCallBack) {
  let options = getMediaOptions(media);
  ImagePickerManager.launchCamera(options, (response) => {
    //console.log('Response = ', response);
    if (response.didCancel) {
      //console.log('User cancelled image picker');
    } else if (response.error) {
      //console.log('ImagePickerManager Error: ', response.error);
    } else if (response.customButton) {
      //console.log('User tapped custom button: ', response.customButton);
    } else {
      if (response.fileSize && response.fileSize <= fileSizeLimit) {
        callback(response);
      } else {
        let path = `${FileReader.DocumentDirectoryPath}/` + response.uri.replace(/^.*[\\\/]/, '');
        path = (Platform.OS == 'ios') ? path : response.path;
        checkFileSize(path, (result) => {
          if (result == 'passed') {
            callback(response);
          } else {
            callback(result);
          }
        });
      }
    }
  });
}

export function launchImageLibrary(media, callback, compressCallBack) {
  let options = getMediaOptions(media);
  ImagePickerManager.launchImageLibrary(options, (response) => {
    //console.log('Response = ', response);
    if (response.didCancel) {
      //console.log('User cancelled image picker');
    } else if (response.error) {
      //console.log('ImagePickerManager Error: ', response.error);
      // this.props.setActive('options');
    } else if (response.customButton) {
      //console.log('User tapped custom button: ', response.customButton);
    } else {
      if (response.fileSize && response.fileSize <= fileSizeLimit) {
        callback(response);
      } else {
        let path = `${FileReader.DocumentDirectoryPath}/` + response.uri.replace(/^.*[\\\/]/, '');
        path = (Platform.OS == 'ios') ? path : response.path;
        checkFileSize(path, (result) => {
          if (result == 'passed') {
            callback(response);
          } else {
            callback(result);
          }
        });
      }
    }
  });
}

export function compressVideo(klikVideoPath, response, callback, compressCallBack) {
  console.log("Called compress Video");
  //console.log("Checking if file exists at ", klikVideoPath + getFileNameFromPath(response.path));
  FileReader.stat(klikVideoPath + getFileNameFromPath(response.path, 'video'))
  .then((result)=>{
    //cache exists

    console.log("cache Exists"); //video has been compressed before

    SystemAccessor.pathToUri(klikVideoPath + getFileNameFromPath(response.path), (err, newUri)=>{
      var newResponse = {
        path: klikVideoPath + getFileNameFromPath(response.path),
        uri: newUri
      }
      console.log("compressed cached newVideo: ", newResponse);
      compressCallBack();

      FileReader.stat(newResponse.path)
      .then((result)=>{
        console.log("newVideo size: ", result.size);
        callback(newResponse);
      })
      .catch((err)=>{
          console.log("File missing.");
          callback(response);
      });

    })

  })
  .catch((err)=>{
    console.log("cache does not exist: ", err); //this video has not been compressed before

    SystemAccessor.processVideo(response.path, klikVideoPath, getFileNameFromPath(response.path),"24", (err, newVideo)=>{

      if(err!= "fail") { //compression was success
            console.log("checking newVideoPath: ", newVideo.path);
            FileReader.stat(newVideo.path)
            .then((result)=>{
              console.log("new newVideo size: ", result.size);
              compressCallBack();
              callback(newVideo);//send compressed video
            })
            .catch((err)=>{
              console.log("FileReader.stat failed. ", err);
              compressCallBack();
              callback(response);//could not compress. send uncompressed
            })
      }
      else { //compression was a failure
          compressCallBack();
          console.log("Deleting ",  klikVideoPath + getFileNameFromPath(response.path));
          FileReader.unlink(klikVideoPath + getFileNameFromPath(response.path))
          .then(()=>{
              console.log("Deleted failed compression file: ");
          })
          .catch((err)=>{
              console.log("Deletion error: ", err);
          });
          console.log("JS detects compression failure. Sending: ", response);
          callback(response);//send uncompressed
      }

    })
  });


}


export function getFileNameFromPath(path) {

    path = path.replace(/^.*[\\\/]/, '');
    return path;
}

/* --------------------------- HELPER METHODS USED BY MESSAGES AND GROUPS --------------------------- */

// Render footer
export function renderFooter(isAllLoaded, color, hasInternet, notFoundText) {
  var render = null;

  if (!isAllLoaded) {
    if (hasInternet) {
      render = (
        <View style={{flex:1}}>
          <ActivityIndicator styleAttr="Inverse" color={color.BUTTON} style={{padding:10}} />
        </View>
      );
    }
  } else {
    if (notFoundText) {
      render = (
        <View style={{flex:1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: color.TEXT_LIGHT, margin:10}}>{notFoundText}</Text>
        </View>
      );
    }
  }
  return render;
}

// Helper method to get the message that should be displayed
export function getMessage(nameSender, data) {
  var message = '';
  if (data.Media && data.Media[0] != null) {
    message = nameSender + " sent an image."
  } else if (data.Sticker) {
    message = nameSender + " sent a sticker."
  } else if (data.Like) {
    message = nameSender + " sent a like."
  } else if (data.Location) {
    message = nameSender + " shared a location."
  } else if (data.Audio) {
    message = nameSender + " sent an audio clip."
  } else if (data.Video) {
    message = nameSender + " sent a video clip."
  } else if (data.Text) {
    message = nameSender + ": " + data.Text;
  } else if (data.ContactDetails) {
    message = nameSender + " send a contact.";
  }
  return message;
}

// Helper method to get message badge
export function getMessageBadge(loggedInUserIsSender, unseenCount, seen, color) {
  if (unseenCount > 0) {
    return (
      <View style={{borderRadius: 50, height: 20, width: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: color.BUTTON}}>
        <Text style={[styles.chatRecentText,{fontSize:11, fontWeight:'700', color:color.BUTTON_TEXT, textAlign: 'center'}]}>
          {unseenCount}
        </Text>
      </View>
    );
  } else {
    if (loggedInUserIsSender) {
      if (seen && seen.length > 0) {
        return <KlikFonts name="check" color={color.THEME} size={15}/>;
      }
    }
  }
}

// Helper method to get message badge
export function getChatBadge(isFromLoggedInUser, notSeenCount, seen, color) {
  if (notSeenCount > 0) {
    return (
      <View style={{borderRadius: 50, height: 20, width: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: color.BUTTON}}>
        <Text style={[styles.chatRecentText,{fontSize:11, fontWeight:'700', color:color.BUTTON_TEXT, textAlign: 'center'}]}>
          {notSeenCount}
        </Text>
      </View>
    );
  } else {
    if (isFromLoggedInUser) {
      if (seen && seen.length > 0) {
        return <KlikFonts name="check" color={color.THEME} size={18}/>;
      }
    }
  }


}
