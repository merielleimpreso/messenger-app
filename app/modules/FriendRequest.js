'use strict';

import React, {
  Component,
} from 'react';
import {
  Image,
  ListView,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  InteractionManager
} from 'react-native';
import _ from 'underscore';
import {getDisplayName, toNameCase, getMediaURL} from './Helpers';
import ListRow from './common/ListRow';
import KlikFonts from 'react-native-vector-icons/KlikFonts';
import RenderModal from './common/RenderModal';
import Send from './config/db/Send';
import ScreenNoItemFound from './ScreenNoItemFound';
import Users from './config/db/Users';
import FCM from 'react-native-fcm';
var styles = StyleSheet.create(require('./../styles/styles.main'));
var GLOBAL = require('./config/Globals.js');

class FriendRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2}),
      friendRequest: [],
      isLoading: true,
      isShowCheckConnection: false,
      isShowContactProfile: false,
      hasNewFriendRequest: this.props.hasNewFriendRequest(),
      loggedInUser: this.props.loggedInUser(),
      profileData: null
    };
    this.acceptRequest = this.acceptRequest.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setIsShowCheckConnection = this.setIsShowCheckConnection.bind(this);
    this.setIsShowContactProfile = this.setIsShowContactProfile.bind(this);
  }
  componentDidMount() {
    console.log('----hasNewFR', this.props.hasNewFriendRequest())
    this._isMounted = true;
    InteractionManager.runAfterInteractions(() => {
      this.getData();
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.state.loggedInUser) != JSON.stringify(this.props.loggedInUser())) {
      console.log('[subscribe]', this.props.subscribeContacts());
      if (this.props.subscribeContacts()) {
        this.props.subscribeContacts();
      }
      this.setState({
        loggedInUser: this.props.loggedInUser()
      });
      InteractionManager.runAfterInteractions(() => {
        this.getData();
      });
    }
    
    if (this.state.hasNewFriendRequest != this.props.hasNewFriendRequest()) {
      console.log('---CHANGED---')
      this.setState({ hasNewFriendRequest: this.props.hasNewFriendRequest() });
      this.getData();
    }
  }

  getData() {
    if (this._isMounted) {
      let requestList = Users.getAllFriendRequests();
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(requestList),
        friendRequest: requestList,
        isLoading: false
      });
    }
  }

  render() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    return (
      <View style={{flex: 1, backgroundColor: color.CHAT_BG}}>
        {this.renderContent()}
        {this.renderModal()}
      </View>
    );
  }

  renderContent() {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let screenNoItemFound = null;
    let listStyle = {};
    if (!this.state.isLoading && (this.state.dataSource.getRowCount() == 0)) {
      screenNoItemFound = <ScreenNoItemFound isLoading={false} text={'No Friend Request'} color={color}/>;
      listStyle = { flex: 1, height: 500 };
    }
    return (
      <View>
        {screenNoItemFound}
        <ListView ref='listview'
          removeClippedSubviews={true}
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          initialListSize={10}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isLoading}
              onRefresh={this.onRefresh.bind(this)}
              colors={[color.THEME]}
            />
          }
          style={listStyle}
          enableEmptySections={true}
        />
      </View>
    );
  }

  onRefresh() {
    if (this._isMounted) {
      this.setState({
        isLoading: true
      });
    }
    this.getData();
  }

  renderRow(data) {
    let color = GLOBAL.COLOR_THEME[this.props.theme()];
    let statusView = (data.statusMessage) ?
                        <Text numberOfLines={1} style={[styles.listViewTitle, {color:color.TEXT_DARK}]}>{data.statusMessage}</Text> :
                        null;
    return (
      <ListRow labelText={data.name}
        detailText={data.statusMessage}
        image={getMediaURL(data.image)}
        color={color}
        addSeparator={true}
        icon={'addcontact'}
        iconPress={() => this.setIsShowContactProfile(data)}
      />
    );
  }

  renderModal() {
    if (this.state.isShowCheckConnection) {
      return (
        <RenderModal
          theme={this.props.theme}
          setIsShowModal={this.setIsShowCheckConnection}
          modalType={'connection'}
        />
      );
    } else if (this.state.isShowContactProfile) {
      return (
        <RenderModal
          theme={this.props.theme}
          profileData={this.state.contact}
          setIsShowModal={() => this.setIsShowContactProfile(null)}
          modalType={'viewProfile'}
        />
      );
    }
  }

  acceptRequest(id) {
    ddp.call('addToContacts', [id]).then(res => {
      if (res) {
        ddp.collections.contacts.upsert(res);
        this.getData();
        FCM.cancelLocalNotification(id);
      }
    });
  }

  ignoreRequest(id) {
    ddp.call('ignoreFriendRequest', [id]).then(() => {
      //ddp.collections.contacts.remove({_id: id});
      this.getData();
    });
  }

  onActionPress(id, action) {
    if (!this.props.hasInternet()) {
      this.setIsShowCheckConnection();
    } else {
      this.setIsShowContactProfile(null);
      if (action == 'accept') {
        this.acceptRequest(id);
      } else {
        this.ignoreRequest(id);
      }
    }
  }

  setIsShowContactProfile(contact) {
    let profileData = null;
    if (contact) {
      let actions = [{icon: 'block', iconLabel: 'Block', onPressIcon: () => this.onActionPress(contact._id, 'ignore')},
                    {icon: 'check', iconLabel: 'Accept Request', onPressIcon: () => this.onActionPress(contact._id, 'accept')}];

      profileData = {
        data: contact,
        actions: actions,
        relStatus: null
      }
    }

    if (this._isMounted) {
      this.setState({
        isShowContactProfile: !this.state.isShowContactProfile,
        contact: profileData
      });
    }
  }

  setIsShowCheckConnection() {
    if (this._isMounted) {
      this.setState({isShowCheckConnection: !this.state.isShowCheckConnection});
    }
  }
}

module.exports = FriendRequest;
